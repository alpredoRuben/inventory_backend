## Inventory Backend

Inventory Backend

## Requirements
1. See https://laravel.com/docs/5.7#server-requirements
2. PHP Composer installed

## Installation 

1. Clone repo `git clone ssh://git@git.mindaperdana.com:2200/mindaperdana/inventory.git`
2. Copy .env file `cp .env.example .env`
3. Run `composer install`
4. Generate Key `php artisan key:generate`
5. Link storage Folder `php artisan storage:link`
6. Setup your local database credential to `.env`
7. Run `php artisan migrate`
8. Run `php artisan db:seed`
9. Run `php artisan jwt:secret`

## API
See Postman API Doc
* https://documenter.getpostman.com/view/3866514/RzteTD19