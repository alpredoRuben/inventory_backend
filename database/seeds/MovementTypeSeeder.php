<?php

use Illuminate\Database\Seeder;
use App\Models\MovementType;

class MovementTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 102 & 101
        MovementType::updateOrCreate(
            ['code' => '102'],
            [
                'code' => '102',
                'description' => 'Rev - GR with PO',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 1
            ]
        );

        $m102 = MovementType::where('code', 102)->first();

        MovementType::updateOrCreate(
            ['code' => '101'],
            [
            	'code' => '101',
            	'description' => 'GR with PO',
            	'reason_ind' => 0,
            	'dc_ind' => 'd',
            	'rev_code_id' => $m102 ? $m102->id : null,
            	'batch_ind' => 1,
        		'qi_ind' => 0,
        		'created_by' => null,
        		'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 1
            ]
        );

        // 132 & 131
        MovementType::updateOrCreate(
            ['code' => '132'],
            [
                'code' => '132',
                'description' => 'Rev - GR from Production',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m132 = MovementType::where('code', 132)->first();

        MovementType::updateOrCreate(
            ['code' => '131'],
            [
            	'code' => '131',
            	'description' => 'GR from Production',
            	'reason_ind' => 0,
            	'dc_ind' => 'd',
            	'rev_code_id' => $m132 ? $m132->id : null,
            	'batch_ind' => 1,
        		'qi_ind' => 0,
        		'created_by' => null,
        		'updated_by' => null,
        		'deleted' => 0
            ]
        );

        // 202 & 201
        MovementType::updateOrCreate(
            ['code' => '202'],
            [
                'code' => '202',
                'description' => 'Rev - GI to Cost Center',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 2
            ]
        );

        $m202 = MovementType::where('code', 202)->first();

        MovementType::updateOrCreate(
            ['code' => '201'],
            [
            	'code' => '201',
            	'description' => 'GI to Cost Center',
            	'reason_ind' => 0,
            	'dc_ind' => 'c',
            	'rev_code_id' => $m202 ? $m202->id : null,
            	'batch_ind' => 0,
        		'qi_ind' => 0,
        		'created_by' => null,
        		'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 2
            ]
        );

        // 242 & 241
        MovementType::updateOrCreate(
            ['code' => '242'],
            [
                'code' => '242',
                'description' => 'Rev - GI to Asset',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m242 = MovementType::where('code', 242)->first();

        MovementType::updateOrCreate(
            ['code' => '241'],
            [
                'code' => '241',
                'description' => 'GI to Asset',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => $m242 ? $m242->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 262 & 261
        MovementType::updateOrCreate(
            ['code' => '262'],
            [
                'code' => '262',
                'description' => 'Rev - GI to Order',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m262 = MovementType::where('code', 262)->first();

        MovementType::updateOrCreate(
            ['code' => '261'],
            [
                'code' => '261',
                'description' => 'GI to Order',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => $m262 ? $m262->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 312 & 311
        MovementType::updateOrCreate(
            ['code' => '312'],
            [
                'code' => '312',
                'description' => 'Rev - Stock Transfer 1-step',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m312 = MovementType::where('code', 312)->first();

        MovementType::updateOrCreate(
            ['code' => '311'],
            [
                'code' => '311',
                'description' => 'Stock Transfer 1-step',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => $m312 ? $m312->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );        

        // 322 & 321
        MovementType::updateOrCreate(
            ['code' => '321'],
            [
                'code' => '321',
                'description' => 'Transfer QI to Unrestricted',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        MovementType::updateOrCreate(
            ['code' => '322'],
            [
                'code' => '322',
                'description' => 'Transfer Unrestricted to QI',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 343 & 344
        MovementType::updateOrCreate(
            ['code' => '343'],
            [
                'code' => '343',
                'description' => 'Transfer Blocked to Unrestrict',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        MovementType::updateOrCreate(
            ['code' => '344'],
            [
                'code' => '344',
                'description' => 'Transfer Unrestricted to Block',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 551 & 552
        MovementType::updateOrCreate(
            ['code' => '552'],
            [
                'code' => '552',
                'description' => 'Rev - GI Scrap Unrestricted',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m552 = MovementType::where('code', 552)->first();

        MovementType::updateOrCreate(
            ['code' => '551'],
            [
                'code' => '551',
                'description' => 'Scrapping from Unrestricted',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => $m552 ? $m552->id : null,
                'batch_ind' => 1,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 555 & 556
        MovementType::updateOrCreate(
            ['code' => '556'],
            [
                'code' => '556',
                'description' => 'Rev - GI Scrap Blocked',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => null
            ]
        );

        $m556 = MovementType::where('code', 556)->first();

        MovementType::updateOrCreate(
            ['code' => '555'],
            [
                'code' => '555',
                'description' => 'Scrapping from Blocked Stock',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => $m556 ? $m556->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => null
            ]
        );

        // 561 & 562
        MovementType::updateOrCreate(
            ['code' => '562'],
            [
                'code' => '562',
                'description' => 'Rev - Initial Stock Balance',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 1
            ]
        );

        $m562 = MovementType::where('code', 562)->first();

        MovementType::updateOrCreate(
            ['code' => '561'],
            [
                'code' => '561',
                'description' => 'Initial Stock Balance',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => $m562 ? $m562->id : null,
                'batch_ind' => 1,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0,
                'sub_type_id' => 1
            ]
        );

        // 605 & 606
        MovementType::updateOrCreate(
            ['code' => '606'],
            [
                'code' => '606',
                'description' => 'Rev - GR from STO',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m606 = MovementType::where('code', 606)->first();

        MovementType::updateOrCreate(
            ['code' => '605'],
            [
                'code' => '605',
                'description' => 'GR from STO(Intransit to Unre)',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => $m606 ? $m606->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 641 & 642
        MovementType::updateOrCreate(
            ['code' => '642'],
            [
                'code' => '642',
                'description' => 'Rev - GI for STO (2-step)',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        $m642 = MovementType::where('code', 642)->first();

        MovementType::updateOrCreate(
            ['code' => '641'],
            [
                'code' => '641',
                'description' => 'GI for STO (2-step)',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => $m642 ? $m642->id : null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        // 701 & 702
        MovementType::updateOrCreate(
            ['code' => '701'],
            [
                'code' => '701',
                'description' => 'Inventory Diff (Unrest.)',
                'reason_ind' => 0,
                'dc_ind' => 'c',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );

        MovementType::updateOrCreate(
            ['code' => '702'],
            [
                'code' => '702',
                'description' => 'Rev - Inventory Diff (Unrest.)',
                'reason_ind' => 0,
                'dc_ind' => 'd',
                'rev_code_id' => null,
                'batch_ind' => 0,
                'qi_ind' => 0,
                'created_by' => null,
                'updated_by' => null,
                'deleted' => 0
            ]
        );
    }
}
