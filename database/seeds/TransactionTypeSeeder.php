<?php

use Illuminate\Database\Seeder;
use App\Models\TransactionType;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionType::firstOrCreate(
            ['code' => 'BSX'],
            ['code' => 'BSX', 'description' => 'Inventory Posting']
        );

        TransactionType::firstOrCreate(
            ['code' => 'GBB'],
            ['code' => 'GBB', 'description' => 'Inventory Offsetting']
        );

        TransactionType::firstOrCreate(
            ['code' => 'WRX'],
            ['code' => 'WRX', 'description' => 'GR/IR Clearing']
        );

        TransactionType::firstOrCreate(
            ['code' => 'FR1'],
            ['code' => 'FR1', 'description' => 'Freight Cost']
        );

        TransactionType::firstOrCreate(
            ['code' => 'KBS'],
            ['code' => 'KBS', 'description' => 'Vendor Account']
        );

        TransactionType::firstOrCreate(
            ['code' => 'VST'],
            ['code' => 'VST', 'description' => 'Purchase Input tax']
        );

        TransactionType::firstOrCreate(
            ['code' => 'ANL'],
            ['code' => 'ANL', 'description' => 'Asset']
        );
    }
}
