<?php

use Illuminate\Database\Seeder;
use App\Models\GroupType;

class GroupTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupType::updateOrCreate(
            ['id' => 1],
            [
                'bs_pl' => 'BS',
                'code' => 'Asset'
            ]
        );

        GroupType::updateOrCreate(
            ['id' => 2],
            [
                'bs_pl' => 'BS',
                'code' => 'Liability'
            ]
        );

        GroupType::updateOrCreate(
            ['id' => 3],
            [
                'bs_pl' => 'BS',
                'code' => 'Equity'
            ]
        );

        GroupType::updateOrCreate(
            ['id' => 4],
            [
                'bs_pl' => 'PL',
                'code' => 'Revenue'
            ]
        );

        GroupType::updateOrCreate(
            ['id' => 5],
            [
                'bs_pl' => 'PL',
                'code' => 'Expenses'
            ]
        );
    }
}
