<?php

use Illuminate\Database\Seeder;
use App\Models\WoStatus;

class WoStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WoStatus::updateOrCreate(
            ['code' => '1'],
            ['description' => 'Draft']
        );

        WoStatus::updateOrCreate(
            ['code' => '2'],
            ['description' => 'In Progress']
        );
        
        WoStatus::updateOrCreate(
            ['code' => '3'],
            ['description' => 'Complete']
        );
        
        WoStatus::updateOrCreate(
            ['code' => '4'],
            ['description' => 'Close']
        );
        
        WoStatus::updateOrCreate(
            ['code' => '5'],
            ['description' => 'Cancel']
        );
        
        WoStatus::updateOrCreate(
            ['code' => '0'],
            ['description' => 'Deleted']
        );
    }
}
