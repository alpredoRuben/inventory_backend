<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(ClassificationTypeTableSeeder::class);
        $this->call(TransactionCodesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SubTypeSeeder::class);
        $this->call(MovementTypeSeeder::class);
        $this->call(DocumentTypeSeeder::class);
        $this->call(WoTypesTableSeeder::class);
        $this->call(WoLevelsTableSeeder::class);
        $this->call(WoStatusesTableSeeder::class);
        $this->call(AssetStatusesTableSeeder::class);
        $this->call(GroupTypeSeeder::class);
        $this->call(PostingKeySeeder::class);
        $this->call(AccountTypeTableSeeder::class);
        $this->call(TransactionTypeSeeder::class);
    }
}
