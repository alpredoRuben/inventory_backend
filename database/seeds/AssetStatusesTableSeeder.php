<?php

use Illuminate\Database\Seeder;
use App\Models\AssetStatus;

class AssetStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssetStatus::updateOrCreate(
            ['id' => 1],
            [
                'code' => '001',
                'name' => 'Draft',
                'deleted' => 0
            ]
        );

        AssetStatus::updateOrCreate(
            ['id' => 2],
            [
                'code' => '002',
                'name' => 'Pending activation',
                'deleted' => 0
            ]
        );

        AssetStatus::updateOrCreate(
            ['id' => 3],
            [
                'code' => '003',
                'name' => 'Active',
                'deleted' => 0
            ]
        );

        AssetStatus::updateOrCreate(
            ['id' => 4],
            [
                'code' => '004',
                'name' => 'Pending retired',
                'deleted' => 0
            ]
        );

        AssetStatus::updateOrCreate(
            ['id' => 5],
            [
                'code' => '005',
                'name' => 'Retired',
                'deleted' => 0
            ]
        );
    }
}
