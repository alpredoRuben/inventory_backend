<?php

use Illuminate\Database\Seeder;
use App\Models\AccountType;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// A = Asset, D = Customer, K = Vendor, M = Material, S = G/L account
        AccountType::updateOrCreate(
            ['id' => 1],
            [
                'code' => 'A',
                'description' => 'Assets'
            ]
        );

        AccountType::updateOrCreate(
            ['id' => 2],
            [
                'code' => 'D',
                'description' => 'Customers'
            ]
        );

        AccountType::updateOrCreate(
            ['id' => 3],
            [
                'code' => 'K',
                'description' => 'Vendors'
            ]
        );

        AccountType::updateOrCreate(
            ['id' => 4],
            [
                'code' => 'M',
                'description' => 'Materials'
            ]
        );

        AccountType::updateOrCreate(
            ['id' => 5],
            [
                'code' => 'S',
                'description' => 'G/L Accounts'
            ]
        );

    }
}
