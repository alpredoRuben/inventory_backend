<?php

use Illuminate\Database\Seeder;
use App\Models\PostingKey;

class PostingKeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostingKey::updateOrCreate(
            ['code' => '00'],
            [
                'code' => '00',
                'description' => 'Act assignment model',
                'd_c' => null,
                'account_type' => null,
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '01'],
            [
                'code' => '01',
                'description' => 'Invoice',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '12'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '02'],
            [
                'code' => '02',
                'description' => 'Reverse credit memo',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '11'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '03'],
            [
                'code' => '03',
                'description' => 'Expenses',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '13'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '04'],
            [
                'code' => '04',
                'description' => 'Other receivables',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '14'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '05'],
            [
                'code' => '05',
                'description' => 'Outgoing payment',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '15'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '06'],
            [
                'code' => '06',
                'description' => 'Payment difference',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '16'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '07'],
            [
                'code' => '07',
                'description' => 'Other clearing',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '17'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '08'],
            [
                'code' => '08',
                'description' => 'Payment clearing',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '18'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '09'],
            [
                'code' => '09',
                'description' => 'Special G/L debit',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 1,
                'reversal' => '19'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0A'],
            [
                'code' => '0A',
                'description' => 'CH Bill.doc. Deb',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '1A'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0B'],
            [
                'code' => '0B',
                'description' => 'CH Cancel.Cred.memoD',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '1B'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0C'],
            [
                'code' => '0C',
                'description' => 'CH Clearing Deb',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0X'],
            [
                'code' => '0X',
                'description' => 'CH Clearing Cred',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '1X'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0Y'],
            [
                'code' => '0Y',
                'description' => 'CH Credit memo Cred',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '1Y'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '0Z'],
            [
                'code' => '0Z',
                'description' => 'CH Cancel.BillDocDeb',
                'd_c' => 'D',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '1Z'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '11'],
            [
                'code' => '11',
                'description' => 'Credit memo',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '02'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '12'],
            [
                'code' => '12',
                'description' => 'Reverse invoice',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '01'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '13'],
            [
                'code' => '13',
                'description' => 'Reverse charges',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '03'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '14'],
            [
                'code' => '14',
                'description' => 'Other payables',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '04'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '15'],
            [
                'code' => '15',
                'description' => 'Incoming payment',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '05'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '16'],
            [
                'code' => '16',
                'description' => 'Payment difference',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '06'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '17'],
            [
                'code' => '17',
                'description' => 'Other clearing',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '07'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '18'],
            [
                'code' => '18',
                'description' => 'Payment clearing',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '08'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '19'],
            [
                'code' => '19',
                'description' => 'Special G/L credit',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 1,
                'reversal' => '09'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1A'],
            [
                'code' => '1A',
                'description' => 'CH Cancel.Bill.docDe',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '01'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1B'],
            [
                'code' => '1B',
                'description' => 'CH Credit memo Deb',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '0B'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1C'],
            [
                'code' => '1C',
                'description' => 'CH Credit memo Deb',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '0C'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1X'],
            [
                'code' => '1X',
                'description' => 'CH Clearing Cred',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '0X'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1Y'],
            [
                'code' => '1Y',
                'description' => 'CH Cancel.Cr.memo C',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '0Y'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '1Z'],
            [
                'code' => '1Z',
                'description' => 'CH Bill.doc. Cred',
                'd_c' => 'C',
                'account_type' => 'D',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '0Z'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '21'],
            [
                'code' => '21',
                'description' => 'Credit memo',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '32'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '22'],
            [
                'code' => '22',
                'description' => 'Reverse invoice',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '31'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '24'],
            [
                'code' => '24',
                'description' => 'Other receivables',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '34'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '25'],
            [
                'code' => '25',
                'description' => 'Outgoing payment',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '35'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '26'],
            [
                'code' => '26',
                'description' => 'Payment difference',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '36'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '27'],
            [
                'code' => '27',
                'description' => 'Clearing',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '37'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '28'],
            [
                'code' => '28',
                'description' => 'Payment clearing',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '38'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '29'],
            [
                'code' => '29',
                'description' => 'Special G/L debit',
                'd_c' => 'D',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 1,
                'reversal' => '39'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '31'],
            [
                'code' => '31',
                'description' => 'Invoice',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '22'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '32'],
            [
                'code' => '32',
                'description' => 'Reverse credit memo',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 1,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '21'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '34'],
            [
                'code' => '34',
                'description' => 'Other payables',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '24'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '35'],
            [
                'code' => '35',
                'description' => 'Incoming payment',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '25'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '36'],
            [
                'code' => '36',
                'description' => 'Payment difference',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '26'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '37'],
            [
                'code' => '37',
                'description' => 'Other clearing',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '27'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '38'],
            [
                'code' => '38',
                'description' => 'Payment clearing',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 1,
                'special_gl' => 0,
                'reversal' => '28'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '39'],
            [
                'code' => '39',
                'description' => 'Special G/L credit',
                'd_c' => 'C',
                'account_type' => 'K',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 1,
                'reversal' => '29'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '40'],
            [
                'code' => '40',
                'description' => 'Debit entry',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '50'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '50'],
            [
                'code' => '50',
                'description' => 'Credit entry',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '40'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '70'],
            [
                'code' => '70',
                'description' => 'Debit asset',
                'd_c' => 'D',
                'account_type' => 'A',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '75'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '75'],
            [
                'code' => '75',
                'description' => 'Credit asset',
                'd_c' => 'C',
                'account_type' => 'A',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '70'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '80'],
            [
                'code' => '80',
                'description' => 'Stock initial entry',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '81'],
            [
                'code' => '81',
                'description' => 'Costs',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '91'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '83'],
            [
                'code' => '83',
                'description' => 'Price difference',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '84'],
            [
                'code' => '84',
                'description' => 'Consumption',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '85'],
            [
                'code' => '85',
                'description' => 'Change in stock',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '86'],
            [
                'code' => '86',
                'description' => 'GR/IR debit',
                'd_c' => 'D',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '96'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '89'],
            [
                'code' => '89',
                'description' => 'Stock inwrd movement',
                'd_c' => 'D',
                'account_type' => 'M',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '99'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '90'],
            [
                'code' => '90',
                'description' => 'Stock initial entry',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '91'],
            [
                'code' => '91',
                'description' => 'Costs',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '81'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '93'],
            [
                'code' => '93',
                'description' => 'Price difference',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '94'],
            [
                'code' => '94',
                'description' => 'Consumption',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '95'],
            [
                'code' => '95',
                'description' => 'Change in stock',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => null
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '96'],
            [
                'code' => '96',
                'description' => 'GR/IR credit',
                'd_c' => 'C',
                'account_type' => 'S',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '86'
            ]
        );

        PostingKey::updateOrCreate(
            ['code' => '99'],
            [
                'code' => '99',
                'description' => 'Stock outwd movement',
                'd_c' => 'C',
                'account_type' => 'M',
                'sales_rel' => 0,
                'payment_rel' => 0,
                'special_gl' => 0,
                'reversal' => '89'
            ]
        );
    }
}
