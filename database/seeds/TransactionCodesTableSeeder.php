<?php

use Illuminate\Database\Seeder;
use App\Models\TransactionCode;

class TransactionCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionCode::firstOrCreate(
            ['code' => 'A'],
            ['description' => 'Purchase Requistion']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'B'],
            ['description' => 'Purchase Order']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'C'],
            ['description' => 'Purchase Return']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'D'],
            ['description' => 'Delivery Order']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'R'],
            ['description' => 'Reservation']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'M'],
            ['description' => 'Document Material (Good Receive, Good Issue, Transfer Posting, Adjustment Stock)']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'O'],
            ['description' => 'Production Order']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'I'],
            ['description' => 'Inspection Document']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'BN'],
            ['description' => 'Batch Number']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'F'],
            ['description' => 'Request for Quotation']
        );
        
        TransactionCode::firstOrCreate(
            ['code' => 'E'],
            ['description' => 'Commitment Item']
        );

        TransactionCode::firstOrCreate(
            ['code' => 'VI'],
            ['description' => 'Vendor Invoice']
        );
    }
}
