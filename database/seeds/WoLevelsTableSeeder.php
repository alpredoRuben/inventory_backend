<?php

use Illuminate\Database\Seeder;
use App\Models\WoLevel;
use App\Models\WoType;

class WoLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wo_type_id = WoType::where('code', 'CM')->first()->id;
        WoLevel::updateOrCreate(
            ['code' => '1'],
            ['description' => 'Critical Priority', 'wo_type_id' => $wo_type_id]
        );
        
        WoLevel::updateOrCreate(
            ['code' => '2'],
            ['description' => 'High Priority', 'wo_type_id' => $wo_type_id]
        );

        WoLevel::updateOrCreate(
            ['code' => '3'],
            ['description' => 'Standard Priority', 'wo_type_id' => $wo_type_id]
        );
    }
}
