<?php

use Illuminate\Database\Seeder;
use App\Models\DocumentType;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentType::updateOrCreate(
            ['code' => 'AA'],
            [
                'description' => 'Asset Posting',
                'account_type' => 'A'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'AB'],
            [
                'description' => 'Accounting Document',
                'account_type' => 'A'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'AF'],
            [
                'description' => 'Depreciation Pstngs',
                'account_type' => 'A'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'AN'],
            [
                'description' => 'Net Asset Posting',
                'account_type' => 'A'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DA'],
            [
                'description' => 'Customer Document',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DB'],
            [
                'description' => 'Cust. Bounced Cheque',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DE'],
            [
                'description' => 'Customer Earned Disc',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DG'],
            [
                'description' => 'Customer Credit Memo',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DI'],
            [
                'description' => 'Customer Int Posting',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DR'],
            [
                'description' => 'Customer Invoice',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'DZ'],
            [
                'description' => 'Customer Payment',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KA'],
            [
                'description' => 'Vendor Document',
                'account_type' => 'K'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KG'],
            [
                'description' => 'Vendor Credit Memo',
                'account_type' => 'K'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KN'],
            [
                'description' => 'Net Vendors',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KP'],
            [
                'description' => 'Account Maintenance',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KR'],
            [
                'description' => 'Vendor Invoice',
                'account_type' => 'K'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'KZ'],
            [
                'description' => 'Vendor Payment',
                'account_type' => 'K'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SA'],
            [
                'description' => 'G/L Account Document',
                'account_type' => 'S'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SB'],
            [
                'description' => 'G/L Account Posting',
                'account_type' => 'S'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SK'],
            [
                'description' => 'Cash Document',
                'account_type' => 'S'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SU'],
            [
                'description' => 'Adjustment Document',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SX'],
            [
                'description' => 'Local Led Postings',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'SY'],
            [
                'description' => 'IFRS Led Postings',
                'account_type' => ''
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'WA'],
            [
                'description' => 'Goods Issue',
                'account_type' => 'M'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'WE'],
            [
                'description' => 'Goods Receipt',
                'account_type' => 'M'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'WI'],
            [
                'description' => 'Inventory Document',
                'account_type' => 'M'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'WL'],
            [
                'description' => 'Goods Issue/Delivery',
                'account_type' => 'M'
            ]
        );

        DocumentType::updateOrCreate(
            ['code' => 'WN'],
            [
                'description' => 'Net Goods Receipt',
                'account_type' => 'M'
            ]
        );
    }
}
