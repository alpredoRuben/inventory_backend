<?php

use Illuminate\Database\Seeder;
use App\Models\SubType;

class SubTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubType::firstOrCreate(
            ['id' => 1],
            ['code' => 'A01', 'description' => 'Initial Stock Balance']
        );

        SubType::firstOrCreate(
            ['id' => 2],
            ['code' => 'A02', 'description' => 'GI to Cost Center']
        );
    }
}
