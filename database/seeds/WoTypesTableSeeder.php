<?php

use Illuminate\Database\Seeder;
use App\Models\WoType;

class WoTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WoType::updateOrCreate(
            ['code' => 'CM'],
            ['description' => 'Corrective Maintenance']
        );

        WoType::updateOrCreate(
            ['code' => 'PM'],
            ['description' => 'Preventive Maintenance']
        );
    }
}
