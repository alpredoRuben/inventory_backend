<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeClassificationParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classification_parameters', function (Blueprint $table) {
            $table->dropForeign('classification_parameters_classification_id_foreign');
            $table->dropForeign('classification_parameters_created_by_foreign');
            $table->dropForeign('classification_parameters_updated_by_foreign');
            
            $table->foreign('classification_id')->references('id')->on('classification_materials')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classification_parameters', function (Blueprint $table) {
            //
        });
    }
}
