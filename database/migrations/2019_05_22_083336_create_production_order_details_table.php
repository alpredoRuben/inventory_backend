<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('production_order_id')->unsigned()->nullable();
            $table->integer('bom_material_id')->unsigned()->nullable();
            $table->float('bom_qty')->nullable();
            $table->integer('component_id')->unsigned()->nullable();
            $table->float('quantity')->nullable();
            $table->integer('base_uom_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('production_order_id')->references('id')->on('production_orders')->onDelete('set null');
            $table->foreign('bom_material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('component_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('base_uom_id')->references('id')->on('uoms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_order_details');
    }
}
