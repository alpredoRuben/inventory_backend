<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable();
            $table->string('document_no')->nullable();
            $table->string('year')->nullable();
            $table->integer('document_type_id')->unsigned()->nullable();
            $table->dateTime('doc_date')->nullable();
            $table->dateTime('posting_date')->nullable();
            $table->integer('fiscal_period')->nullable();
            $table->string('currency')->nullable();
            $table->string('reference')->nullable();
            $table->text('header_text')->nullable();
            $table->integer('status')->nullable()->comment('0. Parked, 1. Posted, 2. Cleared');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null');
            $table->foreign('document_type_id')->references('id')->on('document_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_headers');
    }
}
