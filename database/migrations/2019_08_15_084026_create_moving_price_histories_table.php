<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovingPriceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moving_price_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('movement_history_id')->unsigned()->nullable();
            $table->dateTime('gr_date');
            $table->float('gr_price')->nullable();
            $table->float('gr_quantity')->nullable();
            $table->float('moving_price')->nullable();
            $table->timestamps();

            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('movement_history_id')->references('id')->on('movement_histories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moving_price_histories');
    }
}
