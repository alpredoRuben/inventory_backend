<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeMovementTypeReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_type_reasons', function (Blueprint $table) {
            $table->dropForeign('movement_type_reasons_created_by_foreign');
            $table->dropForeign('movement_type_reasons_movement_type_id_foreign');
            $table->dropForeign('movement_type_reasons_updated_by_foreign');
            
            $table->foreign('movement_type_id')->references('id')->on('movement_types')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement_type_reasons', function (Blueprint $table) {
            //
        });
    }
}
