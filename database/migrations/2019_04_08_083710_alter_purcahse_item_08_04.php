<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPurcahseItem0804 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->integer('group_material_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->string('currency')->nullable();

            $table->foreign('group_material_id')->references('id')->on('group_materials')->onDelete('set null');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->dropForeign(['group_material_id']);
            $table->dropColumn('group_material_id');

            $table->dropForeign(['project_id']);
            $table->dropColumn('project_id');

            $table->dropColumn('delivery_date');
            $table->dropColumn('currency');
        });
    }
}
