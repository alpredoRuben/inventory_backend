<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialAccountFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_account_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_code')->nullable();
            $table->string('plant')->nullable();
            $table->string('valuation_type')->nullable();
            $table->string('valuation_class')->nullable();
            $table->string('price_controll')->nullable();
            $table->string('moving_price')->nullable();
            $table->string('standart_price')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_account_failed_uploads');
    }
}
