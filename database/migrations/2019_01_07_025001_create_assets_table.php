<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->text('description')->nullable();
            $table->integer('material_id')->unsigned()->nullable();//material id
            $table->integer('asset_type_id')->unsigned()->nullable();//asset type id
            $table->integer('plant_id')->unsigned();//plant id
            $table->integer('location_id')->unsigned()->nullable();//location id
            $table->integer('supplier_id')->unsigned()->nullable();//supplier id
            $table->integer('status_id')->unsigned()->nullable();//status id
            $table->integer('valuation_group_id')->unsigned()->nullable();//valuation group id
            $table->integer('retired_reason_id')->unsigned()->nullable();//retired reason id
            $table->integer('responsible_person_id')->unsigned()->nullable();//user id
            $table->integer('old_asset')->nullable();
            $table->date('purchase_date')->nullable();
            $table->integer('purchase_cost')->nullable();
            $table->date('waranty_start')->nullable();
            $table->date('waranty_finish')->nullable();
            $table->string('serial_number')->nullable();
            $table->integer('level_asset');
            $table->integer('superior_asset_id')->unsigned()->nullable();
            $table->integer('count_duration')->nullable();
            $table->integer('unit_duration')->nullable(); // 1 = second, 2 = minutes, 3 = hour, 4 = day, 5 = week, 6 = mounth, 7 = year
            $table->integer('cycle_schedule')->nullable(); // 1 = active, 2 = not active
            $table->string('address_location')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('building')->nullable();
            $table->string('unit')->nullable();
            $table->string('curency')->nullable();
            $table->date('retired_date')->nullable();
            $table->string('retired_remarks')->nullable();
            $table->date('last_maintenance')->nullable();
            $table->date('next_maintenance')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('cascade');
            $table->foreign('asset_type_id')->references('id')->on('asset_types')->onDelete('cascade');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('asset_statuses')->onDelete('cascade');
            $table->foreign('valuation_group_id')->references('id')->on('valuation_groups')->onDelete('cascade');
            $table->foreign('retired_reason_id')->references('id')->on('retired_reasons')->onDelete('cascade');
            $table->foreign('responsible_person_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
