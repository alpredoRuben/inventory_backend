<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTable2108 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('responsible_person')->nullable();
            $table->dropForeign('assets_valuation_group_id_foreign');
            $table->foreign('valuation_group_id')->references('id')->on('user_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('responsible_person')->nullable();
            $table->dropForeign('assets_valuation_group_id_foreign');
            $table->foreign('valuation_group_id')->references('id')->on('valuation_groups')->onDelete('cascade');
        });
    }
}
