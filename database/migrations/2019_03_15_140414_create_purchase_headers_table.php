<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('po_doc_no')->nullable();
            $table->string('po_doc_category')->nullable();//seeder: F (Purchase Order); A (RFQ); K (Contract)
            $table->string('po_doc_type')->nullable();
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->integer('term_payment_id')->unsigned()->nullable();
            $table->integer('purc_group_id')->unsigned()->nullable();
            $table->string('doc_currency')->nullable();//currency
            $table->string('exchange_rate')->nullable();//curr_exchange
            $table->dateTime('purc_dot_date')->nullable();
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_fto')->nullable();
            $table->dateTime('deadline_date')->nullable();
            $table->string('bid_number')->nullable();
            $table->string('quotation_number')->nullable();
            $table->string('your_reference')->nullable();
            $table->integer('incoterm_id')->unsigned()->nullable();
            $table->string('incoterm2')->nullable();//free text
            $table->string('collective_number')->nullable();
            $table->string('release_group')->nullable();
            $table->string('release_strategy')->nullable();
            $table->string('release_state')->nullable();
            $table->integer('status')->nullable();//0.Open; 1. Submitted; 2. Approved; 9.Deleted
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('vendor_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('term_payment_id')->references('id')->on('term_payments')->onDelete('set null');
            $table->foreign('purc_group_id')->references('id')->on('procurement_groups')->onDelete('set null');
            $table->foreign('incoterm_id')->references('id')->on('incoterms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_headers');
    }
}
