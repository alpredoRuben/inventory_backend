<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned();
            $table->integer('sales_organization_id')->unsigned()->nullable();
            $table->integer('delivering_plant_id')->unsigned()->nullable();
            $table->boolean('tax_class')->default(0);// 0 = no tax, 1 = ppn 10%
            $table->integer('account_assignment_id')->unsigned()->nullable();
            $table->integer('item_category_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('sales_organization_id')->references('id')->on('sales_organizations')->onDelete('set null');
            $table->foreign('delivering_plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('account_assignment_id')->references('id')->on('account_assignments')->onDelete('set null');
            $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_sales');
    }
}
