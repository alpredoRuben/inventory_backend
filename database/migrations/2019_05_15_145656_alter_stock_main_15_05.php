<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStockMain1505 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_mains', function (Blueprint $table) {
            $table->string('qty_unrestricted')->nullable()->change();
            $table->string('qty_blocked')->nullable()->change();
            $table->string('qty_transit')->nullable()->change();
            $table->string('qty_qa')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_mains', function (Blueprint $table) {
            $table->string('qty_unrestricted')->nullable()->change();
            $table->string('qty_blocked')->nullable()->change();
            $table->string('qty_transit')->nullable()->change();
            $table->string('qty_qa')->nullable()->change();
        });
    }
}
