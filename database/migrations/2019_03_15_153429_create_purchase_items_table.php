<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_header_id')->unsigned()->nullable();
            $table->string('purchasing_item_no')->nullable();
            $table->integer('account_type')->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->string('short_text')->nullable();
            $table->string('long_text')->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->integer('target_qty')->nullable();
            $table->integer('order_qty')->nullable();
            $table->integer('order_unit_id')->unsigned()->nullable()->comment('uom & uom conv');
            $table->string('price_unit')->nullable();
            $table->string('delivery_location')->nullable();
            $table->string('gl_account')->nullable();
            $table->integer('cost_center_id')->unsigned()->nullable();
            $table->boolean('dlv_complete')->default(0);
            $table->string('fund_center')->nullable();
            $table->string('commitment_item')->nullable();
            $table->integer('reservation_id')->unsigned()->nullable();
            $table->string('pr_number')->nullable();
            $table->string('pr_item_number')->nullable();
            $table->string('purc_doc_number')->nullable();
            $table->string('purc_item_number')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('purchase_header_id')->references('id')->on('purchase_headers')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('order_unit_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('cost_center_id')->references('id')->on('cost_centers')->onDelete('set null');
            $table->foreign('reservation_id')->references('id')->on('reservations')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_items');
    }
}
