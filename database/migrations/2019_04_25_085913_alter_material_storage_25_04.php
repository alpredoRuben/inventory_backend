<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialStorage2504 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_storages', function (Blueprint $table) {
            $table->integer('material_id')->unsigned()->nullable()->change();
            $table->integer('plant_id')->unsigned()->nullable()->change();
            $table->integer('storage_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_storages', function (Blueprint $table) {
            $table->integer('material_id')->unsigned()->change();
            $table->integer('plant_id')->unsigned()->change();
            $table->integer('storage_id')->unsigned()->change();
        });
    }
}
