<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeMaterialUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_uoms', function (Blueprint $table) {
            $table->dropForeign('material_uoms_created_by_foreign');
            $table->dropForeign('material_uoms_material_id_foreign');
            $table->dropForeign('material_uoms_uom_id_foreign');
            $table->dropForeign('material_uoms_updated_by_foreign');
            
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_uoms', function (Blueprint $table) {
            //
        });
    }
}
