<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeReleaseStrategyParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_strategy_parameters', function (Blueprint $table) {
            $table->dropForeign('release_strategy_parameters_classification_parameter_id_foreign');
            $table->dropForeign('release_strategy_parameters_created_by_foreign');
            $table->dropForeign('release_strategy_parameters_release_strategy_id_foreign');
            $table->dropForeign('release_strategy_parameters_updated_by_foreign');
            
            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('set null');
            $table->foreign('classification_parameter_id')->references('id')->on('classification_parameters')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_strategy_parameters', function (Blueprint $table) {
            //
        });
    }
}
