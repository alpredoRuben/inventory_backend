<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMovementType2002 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->boolean('reason_ind')->default(0)->change();
            $table->boolean('batch_ind')->default(0)->change();
            $table->boolean('qi_ind')->default(0)->change();

            $table->integer('rev_code_id')->unsigned()->nullable();
            $table->foreign('rev_code_id')->references('id')->on('movement_types')->onDelete('set null');

            $table->renameColumn('move_type_code', 'code');
            $table->renameColumn('move_type_desc', 'description');

            $table->dropColumn('batch_sort');
            $table->dropColumn('rev_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->boolean('reason_ind')->default(0)->change();
            $table->boolean('batch_ind')->default(0)->change();
            $table->boolean('qi_ind')->default(0)->change();

            $table->dropForeign(['rev_code_id']);
            $table->dropColumn('rev_code_id');

            $table->renameColumn('code', 'move_type_code');
            $table->renameColumn('description', 'move_type_desc');

            $table->string('batch_sort', 4)->nullable();
            $table->string('rev_code', 3)->nullable()->change();
        });
    }
}
