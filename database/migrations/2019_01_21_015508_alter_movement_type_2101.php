<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMovementType2101 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->string('move_type_code', 3)->change();
            $table->string('move_type_desc', 30)->change();
            $table->string('dc_ind', 1)->change()->comment('Debit credit indicator');
            $table->string('rev_code', 3)->change()->comment('Reversal movement type');
            $table->dropColumn('stock_det_id');
            $table->string('batch_sort', 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->integer('stock_det_id')->unsigned()->nullable()->comment('Stock Determination');
            $table->dropColumn('batch_sort');
        });
    }
}
