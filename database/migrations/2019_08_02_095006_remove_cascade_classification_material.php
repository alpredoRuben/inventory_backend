<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeClassificationMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classification_materials', function (Blueprint $table) {
            $table->dropForeign('classification_materials_classification_type_id_foreign');
            $table->dropForeign('classification_materials_created_by_foreign');
            $table->dropForeign('classification_materials_updated_by_foreign');
            
            $table->foreign('classification_type_id')->references('id')->on('classification_types')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classification_materials', function (Blueprint $table) {
            //
        });
    }
}
