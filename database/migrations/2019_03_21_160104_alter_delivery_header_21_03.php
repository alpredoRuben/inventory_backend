<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeliveryHeader2103 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->integer('sent_by')->unsigned()->nullable();
            $table->foreign('sent_by')->references('id')->on('users')->onDelete('set null');
            $table->dateTime('sent_date')->nullable();
            $table->integer('recived_by')->unsigned()->nullable();
            $table->foreign('recived_by')->references('id')->on('users')->onDelete('set null');
            $table->dateTime('recived_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropForeign(['sent_by']);
            $table->dropColumn('sent_by');
            $table->dropColumn('sent_date');
            $table->dropForeign(['recived_by']);
            $table->dropColumn('recived_by');
            $table->dropColumn('recived_date');
        });
    }
}
