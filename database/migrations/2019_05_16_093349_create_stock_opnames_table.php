<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opnames', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stock_opname_doc')->nullable();
            $table->integer('item_number')->nullable();
            $table->integer('type')->nullable();
            $table->dateTime('document_date')->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->integer('stock_batch_id')->unsigned()->nullable();
            $table->boolean('zero_count')->default(0);
            $table->string('quantity')->nullable();
            $table->integer('base_uom_id')->unsigned()->nullable();
            $table->string('quantity_entry')->nullable();
            $table->integer('entry_uom_id')->unsigned()->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('stock_batch_id')->references('id')->on('stock_batches')->onDelete('set null');
            $table->foreign('base_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('entry_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opnames');
    }
}
