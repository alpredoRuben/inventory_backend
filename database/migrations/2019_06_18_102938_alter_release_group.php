<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReleaseGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_groups', function (Blueprint $table) {
            $table->dropColumn('object');

            $table->integer('release_object_id')->unsigned()->nullable();
            $table->foreign('release_object_id')->references('id')->on('release_objects')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_groups', function (Blueprint $table) {
            $table->string('object')->nullable();

            $table->dropForeign(['release_object_id']);
            $table->dropColumn('release_object_id');
        });
    }
}
