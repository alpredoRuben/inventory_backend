<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostingKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posting_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('description')->nullable();
            $table->string('d_c')->nullable();
            $table->string('account_type')->nullable()->comment('A=Assets, D=Customers, K=Vendors, M=Material, S=G/L accounts');
            $table->boolean('sales_rel')->default(0);
            $table->boolean('payment_rel')->default(0);
            $table->boolean('special_gl')->default(0);
            $table->string('reversal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posting_keys');
    }
}
