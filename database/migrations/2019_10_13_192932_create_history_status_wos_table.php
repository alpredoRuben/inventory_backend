<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryStatusWosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_status_wos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trx_wo_id')->unsigned()->nullable();
            $table->integer('wo_status_id')->unsigned()->nullable();
            $table->integer('wo_last_status')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->string('duration')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();

            $table->foreign('trx_wo_id')->references('id')->on('trx_wos')->onDelete('set null');
            $table->foreign('wo_status_id')->references('id')->on('wo_statuses')->onDelete('set null');
            $table->foreign('wo_last_status')->references('id')->on('wo_statuses')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_status_wos');
    }
}
