<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReleaseStrategyParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('release_strategy_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('release_strategy_id')->unsigned()->nullable();
            $table->integer('classification_parameter_id')->unsigned()->nullable();
            $table->string('value')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('cascade');
            $table->foreign('classification_parameter_id')->references('id')->on('classification_parameters')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('release_strategy_parameters');
    }
}
