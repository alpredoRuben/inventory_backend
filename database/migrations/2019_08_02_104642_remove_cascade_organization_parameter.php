<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeOrganizationParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organization_parameters', function (Blueprint $table) {
            $table->dropForeign('organization_parameters_created_by_foreign');
            $table->dropForeign('organization_parameters_role_id_foreign');
            $table->dropForeign('organization_parameters_updated_by_foreign');
            
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organization_parameters', function (Blueprint $table) {
            //
        });
    }
}
