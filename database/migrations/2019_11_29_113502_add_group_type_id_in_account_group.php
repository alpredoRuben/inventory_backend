<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupTypeIdInAccountGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_groups', function (Blueprint $table) {
            $table->dropColumn('group_type');
            $table->integer('group_type_id')->unsigned()->nullable();
            $table->foreign('group_type_id')->references('id')->on('group_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_groups', function (Blueprint $table) {
            $table->integer('group_type')->nullable();
            $table->dropForeign(['group_type_id']);
            $table->dropColumn('group_type_id');
        });
    }
}
