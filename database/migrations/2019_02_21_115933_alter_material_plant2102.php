<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialPlant2102 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->dropColumn('quality_profile');            
            $table->integer('quality_profile_id')->unsigned()->nullable();

            $table->foreign('quality_profile_id')->references('id')->on('classification_materials')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->string('quality_profile')->nullable();
            
            $table->dropForeign(['quality_profile_id']);
            $table->dropColumn('quality_profile_id');
        });
    }
}
