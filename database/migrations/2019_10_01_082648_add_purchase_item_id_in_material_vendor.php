<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseItemIdInMaterialVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_vendors', function (Blueprint $table) {
            $table->integer('purchase_item_id')->unsigned()->nullable();
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_vendors', function (Blueprint $table) {
            $table->dropForeign('material_vendors_purchase_item_id_foreign');
            $table->dropColumn('purchase_item_id');
        });
    }
}
