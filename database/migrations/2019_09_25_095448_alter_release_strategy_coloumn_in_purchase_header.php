<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReleaseStrategyColoumnInPurchaseHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->dropColumn('release_group');
            $table->dropColumn('release_strategy');

            $table->integer('release_strategy_id')->unsigned()->nullable();
            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->string('release_group')->nullable();
            $table->string('release_strategy')->nullable();

            $table->dropForeign('purchase_headers_release_strategy_id_foreign');

            $table->dropColumn('release_strategy_id');
        });
    }
}
