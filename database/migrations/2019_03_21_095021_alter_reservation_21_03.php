<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservation2103 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('proc_type')->nullable();
            $table->integer('proc_group_id')->unsigned()->nullable();
            $table->foreign('proc_group_id')->references('id')->on('procurement_groups')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign(['proc_group_id']);
            $table->dropColumn('proc_group_id');
            $table->dropColumn('proc_type');
        });
    }
}
