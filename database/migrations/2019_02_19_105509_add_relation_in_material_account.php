<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationInMaterialAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_accounts', function (Blueprint $table) {
            $table->dropColumn('valuation_type');
            $table->dropColumn('valuation_class');

            $table->integer('valuation_type_id')->unsigned()->nullable();
            $table->integer('valuation_class_id')->unsigned()->nullable();

            $table->foreign('valuation_type_id')->references('id')->on('valuation_types')->onDelete('set null');
            $table->foreign('valuation_class_id')->references('id')->on('valuation_classes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_accounts', function (Blueprint $table) {
            $table->string('valuation_type')->nullable();
            $table->string('valuation_class')->nullable();

            $table->dropForeign(['valuation_type_id']);
            $table->dropColumn('valuation_type_id');

            $table->dropForeign(['valuation_class_id']);
            $table->dropColumn('valuation_class_id');
        });
    }
}
