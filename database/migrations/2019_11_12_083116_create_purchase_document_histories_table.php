<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDocumentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_document_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_header_id')->unsigned()->nullable();
            $table->integer('purchase_item_id')->unsigned()->nullable();
            $table->integer('transaction')->nullable()->comment('1. Goods Reciept 2. Invoice Reciept 3. Down Payment');
            $table->string('material_year')->nullable();
            $table->string('material_doc')->nullable();
            $table->string('material_doc_item')->nullable();
            $table->string('de_ce')->nullable();
            $table->float('quantity')->nullable();
            $table->float('amount_lc')->nullable();
            $table->float('quantity_order')->nullable();
            $table->float('amount')->nullable();
            $table->string('reference_doc')->nullable();
            $table->timestamps();
            
            $table->foreign('purchase_header_id')->references('id')->on('purchase_headers')->onDelete('set null');
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_document_histories');
    }
}
