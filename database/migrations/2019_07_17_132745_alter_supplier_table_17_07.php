<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSupplierTable1707 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->integer('vendor_group_id')->unsigned()->nullable();
            $table->string('vat_no')->nullable();
            $table->integer('tax_indicator')->nullable();
            
            $table->foreign('vendor_group_id')->references('id')->on('vendor_groups')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->dropForeign(['vendor_group_id']);
            $table->dropColumn('vendor_group_id');
            $table->dropColumn('vat_no');
            $table->dropColumn('tax_indicator');
        });
    }
}
