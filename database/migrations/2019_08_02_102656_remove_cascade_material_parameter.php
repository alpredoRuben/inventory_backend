<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeMaterialParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_parameters', function (Blueprint $table) {
            $table->dropForeign('material_parameters_classification_parameter_id_foreign');
            $table->dropForeign('material_parameters_created_by_foreign');
            $table->dropForeign('material_parameters_material_id_foreign');
            $table->dropForeign('material_parameters_updated_by_foreign');
            
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('classification_parameter_id')->references('id')->on('classification_parameters')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_parameters', function (Blueprint $table) {
            //
        });
    }
}
