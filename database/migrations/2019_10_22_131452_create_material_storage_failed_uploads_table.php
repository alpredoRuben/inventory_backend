<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialStorageFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_storage_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_code')->nullable();
            $table->string('plant')->nullable();
            $table->string('storage')->nullable();
            $table->string('minimum_stock')->nullable();
            $table->string('maximum_stock')->nullable();
            $table->string('storage_bin')->nullable();
            $table->string('storage_condition')->nullable();
            $table->string('storage_type')->nullable();
            $table->string('temp_condition')->nullable();
            $table->string('le_quantity')->nullable();
            $table->string('le_unit')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_storage_failed_uploads');
    }
}
