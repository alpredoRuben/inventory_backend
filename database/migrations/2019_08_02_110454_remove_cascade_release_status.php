<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeReleaseStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('release_statuses', function (Blueprint $table) {
            $table->dropForeign('release_statuses_created_by_foreign');
            $table->dropForeign('release_statuses_release_strategy_id_foreign');
            $table->dropForeign('release_statuses_updated_by_foreign');
            
            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('release_statuses', function (Blueprint $table) {
            //
        });
    }
}
