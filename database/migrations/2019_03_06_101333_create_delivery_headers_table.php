<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('delivery_code')->nullable();
            $table->boolean('delivery_type')->default(0);
            $table->integer('sales_organization_id')->unsigned()->nullable();
            $table->dateTime('planned_gi')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->dateTime('picking_date')->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('dlv_to_plant_id')->unsigned()->nullable();
            $table->integer('dlv_to_storage_id')->unsigned()->nullable();
            $table->string('shipping_point_location')->nullable();
            $table->string('delivery_note')->nullable();
            $table->string('transport_type')->nullable();
            $table->string('transport_id')->nullable();
            $table->string('courier_name')->nullable();
            $table->string('status')->nullable();// 0 = open, 1 =pick/pack, 2 = Good Issued
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);

            $table->foreign('sales_organization_id')->references('id')->on('sales_organizations')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('dlv_to_plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('dlv_to_storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_headers');
    }
}
