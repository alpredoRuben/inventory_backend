<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservationTablePrNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('order_number')->nullable()->change();
            $table->string('planned_order')->nullable()->change();
            $table->string('purc_req_number')->nullable()->change();
            $table->string('po_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('order_number')->nullable()->change();
            $table->string('planned_order')->nullable()->change();
            $table->string('purc_req_number')->nullable()->change();
            $table->string('po_number')->nullable()->change();
        });
    }
}
