<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTypoInPoHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->renameColumn('purc_dot_date', 'purc_doc_date');
            $table->renameColumn('valid_fto', 'valid_to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->renameColumn('purc_doc_date', 'purc_dot_date');
            $table->renameColumn('valid_to', 'valid_fto');
        });
    }
}
