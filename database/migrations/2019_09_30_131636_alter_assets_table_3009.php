<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTable3009 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            
            $table->integer('double_decline')->change();
            $table->integer('straight_line')->change();
            $table->integer('salvage_value')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->bigInteger('double_decline')->change();
            $table->bigInteger('straight_line')->change();
            $table->bigInteger('salvage_value')->change();
        });
    }
}
