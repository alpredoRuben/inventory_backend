<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialSalesFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_sales_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_code')->nullable();
            $table->string('sales_organization')->nullable();
            $table->string('delivering_plant')->nullable();
            $table->string('tax_class')->nullable();
            $table->string('account_assignment')->nullable();
            $table->string('item_category')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_sales_failed_uploads');
    }
}
