<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('material_doc')->unique()->nullable();
            $table->string('doc_year')->nullable();
            $table->string('item_no')->nullable();
            $table->dateTime('posting_date')->nullable();
            $table->integer('movement_type_id')->unsigned()->nullable();
            $table->integer('movement_type_reason_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->string('batch_no')->nullable();
            $table->string('quantity')->nullable();
            $table->integer('base_uom_id')->unsigned()->nullable();
            $table->string('quantity_entry')->nullable();
            $table->integer('entry_uom_id')->unsigned()->nullable();
            $table->string('de_ce')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->dateTime('document_date')->nullable();
            $table->string('doc_header_text')->nullable();
            $table->string('material_slip')->nullable();
            $table->integer('supplier_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('cost_center_id')->unsigned()->nullable();
            $table->integer('work_center_id')->unsigned()->nullable();
            $table->string('po_number')->nullable();
            $table->string('po_item')->nullable();
            $table->string('work_order')->nullable();
            $table->string('project')->nullable();
            $table->string('delivery_no')->nullable();
            $table->string('delivery_item')->nullable();
            $table->string('accounting_no')->nullable();
            $table->string('accounting_line')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('movement_type_id')->references('id')->on('movement_types')->onDelete('set null');
            $table->foreign('movement_type_reason_id')->references('id')->on('movement_type_reasons')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('base_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('entry_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('cost_center_id')->references('id')->on('cost_centers')->onDelete('set null');
            $table->foreign('work_center_id')->references('id')->on('work_centers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_histories');
    }
}
