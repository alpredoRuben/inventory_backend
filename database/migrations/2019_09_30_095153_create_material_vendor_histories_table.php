<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialVendorHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_vendor_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_vendor_id')->unsigned()->nullable();
            $table->float('price')->nullable();
            $table->string('currency')->nullable();
            $table->integer('purchase_header_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();

            $table->foreign('material_vendor_id')->references('id')->on('material_vendors')->onDelete('set null');
            $table->foreign('purchase_header_id')->references('id')->on('purchase_headers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_vendor_histories');
    }
}
