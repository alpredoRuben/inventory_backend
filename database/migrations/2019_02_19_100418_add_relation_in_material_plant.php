<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationInMaterialPlant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->dropColumn('profit_center');
            $table->dropColumn('mrp_controller');
            $table->dropColumn('product_champion');

            $table->integer('profit_center_id')->unsigned()->nullable();
            $table->integer('mrp_controller_id')->unsigned()->nullable();
            $table->integer('product_champion_id')->unsigned()->nullable();

            $table->foreign('profit_center_id')->references('id')->on('profit_centers')->onDelete('set null');
            $table->foreign('mrp_controller_id')->references('id')->on('mrps')->onDelete('set null');
            $table->foreign('product_champion_id')->references('id')->on('product_champions')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->string('profit_center')->nullable();
            $table->string('mrp_controller')->nullable();
            $table->string('product_champion')->nullable();

            $table->dropForeign(['profit_center_id']);
            $table->dropColumn('profit_center_id');

            $table->dropForeign(['mrp_controller_id']);
            $table->dropColumn('mrp_controller_id');

            $table->dropForeign(['product_champion_id']);
            $table->dropColumn('product_champion_id');
        });
    }
}
