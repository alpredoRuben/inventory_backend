<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUopIdInMaterialPlant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->integer('unit_of_purchase_id')->unsigned()->nullable();
            $table->foreign('unit_of_purchase_id')->references('id')->on('uoms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->dropForeign('material_plants_unit_of_purchase_id_foreign');
            $table->dropColumn('unit_of_purchase_id');
        });
    }
}
