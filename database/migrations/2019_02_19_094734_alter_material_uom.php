<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_uoms', function (Blueprint $table) {
            $table->renameColumn('value', 'value_conversion');
            $table->string('base_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_uoms', function (Blueprint $table) {
            $table->renameColumn('value_conversion', 'value');
            $table->dropColumn('base_value');
        });
    }
}
