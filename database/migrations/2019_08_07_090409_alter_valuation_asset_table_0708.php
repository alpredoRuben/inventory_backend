<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterValuationAssetTable0708 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('valuation_asset', function (Blueprint $table) {
            $table->dropColumn('is_depreciation');
        });

        Schema::table('valuation_asset', function (Blueprint $table) {
            $table->boolean('is_depreciation')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('valuation_asset', function (Blueprint $table) {
            // $table->tinyInteger('is_depreciation')->default(0)->change();
        });
    }
}
