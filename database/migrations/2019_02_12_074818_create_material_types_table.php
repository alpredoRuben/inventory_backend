<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description');
            $table->boolean('number_type')->default(0);//0 internal, 1 external
            $table->string('number_range_from')->nullable();
            //if number_type internal increment ex from=1800000, if number_type external alpanumeric ex from=MBAAAAAA
            $table->string('number_range_to')->nullable();
            //if number_type internal increment ex to=1899999, if number_type external alpanumeric ex to=MBZZZZZZ
            $table->string('number_range_used')->nullable();
            $table->boolean('basic')->default(0);
            $table->boolean('classification')->default(0);
            $table->boolean('purchasing')->default(0);
            $table->boolean('mrp')->default(0);
            $table->boolean('storage')->default(0);
            $table->boolean('sales')->default(0);
            $table->boolean('accounting')->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_types');
    }
}
