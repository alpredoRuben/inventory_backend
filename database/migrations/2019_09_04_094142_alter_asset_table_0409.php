<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetTable0409 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->float('qty_asset')->nullable();
            $table->dropColumn('responsible_person');
            $table->dropColumn('address_location');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('province');
            $table->dropColumn('city');
            $table->dropColumn('building');
            $table->dropColumn('unit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('qty_asset');
            $table->string('responsible_person')->nullable();
            $table->string('address_location')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('building')->nullable();
            $table->string('unit')->nullable();
        });
    }
}
