<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockDeterminationFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_determination_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plant')->nullable();
            $table->string('storage')->nullable();
            $table->string('sequence')->nullable();
            $table->string('external_purchase')->nullable();
            $table->string('plant_supply')->nullable();
            $table->string('storage_supply')->nullable();
            $table->string('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_determination_failed_uploads');
    }
}
