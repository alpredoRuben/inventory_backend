<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number')->nullable();
            $table->integer('order_type')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('requirement_date')->nullable();
            $table->integer('req_source')->nullable();
            $table->dateTime('production_date')->nullable();
            $table->integer('mrp_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->integer('work_center_id')->unsigned()->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->float('quantity')->nullable();
            $table->integer('prod_uom_id')->unsigned()->nullable();
            $table->float('gr_quantity')->nullable();
            $table->integer('gr_uom_id')->unsigned()->nullable();
            $table->dateTime('gr_date')->nullable();
            $table->string('material_doc')->nullable();
            $table->string('material_doc_year')->nullable();
            $table->string('material_doc_item')->nullable();
            $table->integer('reservation_id')->unsigned()->nullable();
            $table->integer('reservation_item')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('mrp_id')->references('id')->on('mrps')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('work_center_id')->references('id')->on('work_centers')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('prod_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('reservation_id')->references('id')->on('reservations')->onDelete('set null');
            $table->foreign('gr_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_orders');
    }
}
