<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialsPart21902 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->integer('weight_unit')->unsigned()->nullable();//uom, dgn type mass
            $table->integer('volume_uom')->unsigned()->nullable();//uom, dgn type volume

            $table->foreign('weight_unit')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('volume_uom')->references('id')->on('uoms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropForeign(['weight_unit']);
            $table->dropColumn('weight_unit');

            $table->dropForeign(['volume_uom']);
            $table->dropColumn('volume_uom');
        });
    }
}
