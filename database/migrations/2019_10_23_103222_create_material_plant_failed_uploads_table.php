<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPlantFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_plant_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_code')->nullable();
            $table->string('plant')->nullable();
            $table->string('procurement_type')->nullable();
            $table->string('procurement_group')->nullable();
            $table->string('uop')->nullable();
            $table->string('production_storage')->nullable();
            $table->string('batch_indicator')->nullable();
            $table->string('available_check')->nullable();
            $table->string('profit_center')->nullable();
            $table->string('source_list')->nullable();
            $table->string('mrp_group')->nullable();
            $table->string('mrp_controller')->nullable();
            $table->string('planned_delivery_time')->nullable();
            $table->string('product_champion')->nullable();
            $table->string('hazardous_indicator')->nullable();
            $table->string('stock_determination')->nullable();
            $table->string('quality_profile')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_plant_failed_uploads');
    }
}
