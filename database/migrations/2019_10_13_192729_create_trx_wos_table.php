<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxWosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_wos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',15);
            $table->integer('service_id')->unsigned()->nullable();
            $table->integer('wo_type_id')->unsigned()->nullable();
            $table->integer('wo_level_id')->unsigned()->nullable();
            $table->integer('wo_status_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->string('lat',50)->nullable();
            $table->string('long',50)->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->text('reason')->nullable();
            $table->integer('service_point_id')->unsigned()->nullable();
            $table->integer('service_point_transfer')->unsigned()->nullable();
            $table->integer('timserpo_id')->unsigned()->nullable();
            $table->integer('creator_finding')->unsigned()->nullable();
            $table->date('date_finding')->nullable();
            $table->string('title_finding')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('service_id')->references('id')->on('service_requests')->onDelete('set null');
            $table->foreign('wo_type_id')->references('id')->on('wo_types')->onDelete('set null');
            $table->foreign('wo_level_id')->references('id')->on('wo_levels')->onDelete('set null');
            $table->foreign('wo_status_id')->references('id')->on('wo_statuses')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('service_point_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('service_point_transfer')->references('id')->on('users')->onDelete('set null');
            $table->foreign('timserpo_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('creator_finding')->references('id')->on('users')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_wos');
    }
}
