<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_batches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stock_main_id')->unsigned()->nullable();
            $table->string('batch_number')->nullable();
            $table->dateTime('gr_date')->nullable()->comment('good recive date');
            $table->dateTime('sled_date')->nullable()->comment('expiry date');
            $table->dateTime('manuf_date')->nullable()->comment('manufactur date');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->string('vendor_batch')->nullable();
            $table->string('license')->nullable();
            $table->string('unit_cost')->nullable();
            $table->integer('po_number')->unsigned()->nullable()->comment('fk for po table');
            $table->string('po_item_number')->nullable();
            $table->integer('prod_doc_no')->unsigned()->nullable()->comment('fk for prodo rder');
            $table->string('prod_item_no')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('stock_main_id')->references('id')->on('stock_mains')->onDelete('set null');
            $table->foreign('vendor_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_batches');
    }
}
