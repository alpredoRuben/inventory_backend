<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterials2101 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('code', 20)->change();
            $table->string('description', 60)->change();
            $table->string('type', 4)->change();
            $table->string('sku_code', 20)->nullable()->change();
            $table->string('nett_weight', 10)->nullable()->change();
            $table->string('gross_weight', 10)->nullable()->change();
            $table->string('vol', 10)->nullable()->change();
            $table->string('vol_uom', 5)->nullable()->change();
            $table->string('tax_class', 1)->nullable()->change();
            $table->string('account_assign', 2)->nullable()->change();
            $table->string('proc_type', 5)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->string('name');
        });
    }
}
