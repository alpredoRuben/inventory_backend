<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterShippingLocationInDeliveryHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->integer('shipping_point_location_id')->unsigned()->nullable();
            $table->foreign('shipping_point_location_id')->references('id')->on('storages')->onDelete('set null');

            $table->dropColumn('shipping_point_location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropForeign(['shipping_point_location_id']);
            $table->dropColumn('shipping_point_location_id');

            $table->string('shipping_point_location')->nullable();
        });
    }
}
