<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoaFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coa_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coa_code')->nullable();
            $table->string('coa_description')->nullable();
            $table->string('account_group_code')->nullable();
            $table->string('account_group_description')->nullable();
            $table->string('group_type')->nullable();
            $table->string('account_type')->nullable();
            $table->string('gl_account')->nullable();
            $table->string('gl_description')->nullable();
            $table->string('cash')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coa_failed_uploads');
    }
}
