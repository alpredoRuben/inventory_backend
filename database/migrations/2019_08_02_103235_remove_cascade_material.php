<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropForeign('materials_classification_material_id_foreign');
            $table->dropForeign('materials_created_by_foreign');
            $table->dropForeign('materials_group_material_id_foreign');
            $table->dropForeign('materials_updated_by_foreign');
            
            $table->foreign('classification_material_id')->references('id')->on('classification_materials')->onDelete('set null');
            $table->foreign('group_material_id')->references('id')->on('group_materials')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            //
        });
    }
}
