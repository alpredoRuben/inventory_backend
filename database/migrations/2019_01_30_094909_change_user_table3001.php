<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserTable3001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            // Drop FK
            $table->dropForeign(['location_id']);
            $table->dropForeign(['company_id']);
            // remove coloumn
            $table->dropColumn('isactive');
            $table->dropColumn('image');
            $table->dropColumn('address');
            $table->dropColumn('phone');
            $table->dropColumn('location_id');
            $table->dropColumn('company_id');
            // rename coloumn
            $table->renameColumn('name', 'firstname');
            // add new coloumn
            $table->string('lastname')->nullable();
            $table->string('username')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('wrong_pass')->nullable();
            $table->integer('status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // add coloumn from drop up function
            $table->boolean('isactive')->default(1);
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('suppliers')->onDelete('cascade');
            // rename coloumn
            $table->renameColumn('firstname', 'name');
            // drop new coloumn from up function
            $table->dropColumn('lastname');
            $table->dropColumn('username');
            $table->dropColumn('mobile');
            $table->dropColumn('wrong_pass');
            $table->dropColumn('status');
        });
    }
}
