<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetConsumptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_consumptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_id')->unsigned()->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_number')->nullable();
            $table->string('doc_item')->nullable();
            $table->string('currency')->nullable();
            $table->string('amount')->nullable();
            $table->string('posting_date')->nullable();
            $table->integer('reservation_id')->unsigned()->nullable();
            $table->integer('purchase_item_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('budget_id')->references('id')->on('budgets')->onDelete('set null');
            $table->foreign('reservation_id')->references('id')->on('reservations')->onDelete('set null');
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_consumptions');
    }
}
