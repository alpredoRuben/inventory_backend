<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeResponsiblePersonHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('responsible_person_histories', function (Blueprint $table) {
            $table->dropForeign('responsible_person_histories_asset_id_foreign');
            $table->dropForeign('responsible_person_histories_created_by_foreign');
            $table->dropForeign('responsible_person_histories_old_responsible_person_id_foreign');
            
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('old_responsible_person_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responsible_person_histories', function (Blueprint $table) {
            //
        });
    }
}
