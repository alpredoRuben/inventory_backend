<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificationParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classification_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classification_id')->unsigned();
            $table->string('name');
            $table->integer('type')->nullable(); //1=Char,2=Date,3=Time,4=Numeric,5=List
            /** Type detail
            *Char => Length
            *Date => null
            *Time => null
            *Numeric => Length, Decimal
            *List => Values Separate use ,
            */
            $table->integer('length')->nullable();
            $table->integer('decimal')->nullable();
            $table->string('value')->nullable();
            $table->integer('reading')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('classification_id')->references('id')->on('classification_materials')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classification_parameters');
    }
}
