<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_storages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned();
            $table->integer('plant_id')->unsigned();
            $table->integer('storage_id')->unsigned();
            $table->string('min_stock')->unsigned()->nullable();
            $table->string('max_stock')->unsigned()->nullable();
            $table->boolean('bin_indicator')->default(0);
            $table->string('storage_condition')->nullable();
            $table->string('storage_type')->nullable();
            $table->string('temp_condition')->nullable();
            $table->string('le_quantity')->nullable();
            $table->string('le_unit')->nullable();
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_storages');
    }
}
