<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_plants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            // purchasing
            $table->boolean('proc_type')->nullable();//0=internal, 1=external
            $table->integer('proc_group_id')->unsigned()->nullable();
            // end purchasing
            $table->boolean('batch_ind')->nullable();
            $table->boolean('avail_check')->nullable();
            $table->string('profit_center')->nullable();
            $table->string('serial_profile')->nullable();
            $table->boolean('source_list')->nullable();
            $table->string('mrp_group')->comment('make to stock / make to order')->nullable();
            $table->string('mrp_controller')->nullable();
            $table->string('planned_dlv_time')->nullable();
            $table->string('product_champion')->nullable();
            $table->boolean('hazardous_indicator')->nullable();
            $table->integer('stock_determination')->nullable();
            $table->string('quality_profile')->nullable();
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('proc_group_id')->references('id')->on('procurement_groups')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_plants');
    }
}
