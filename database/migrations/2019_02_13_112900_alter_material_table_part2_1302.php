<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialTablePart21302 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->integer('classification_material_id')->unsigned()->nullable()->change();

            $table->integer('material_type_id')->unsigned()->nullable();
            $table->string('sku_code')->nullable();
            $table->integer('uom_id')->unsigned()->nullable();
            $table->integer('old_material')->unsigned()->nullable();//self relation
            $table->integer('base_material')->unsigned()->nullable();//self relation
            $table->string('nett_weight')->nullable();
            $table->string('gross_weight')->nullable();
            $table->string('weight_unit')->nullable();//uom, dgn type mass
            $table->string('volume')->nullable();
            $table->string('volume_uom')->nullable();//uom, dgn type volume

            $table->foreign('material_type_id')->references('id')->on('material_types')->onDelete('set null');
            $table->foreign('uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('old_material')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('base_material')->references('id')->on('materials')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->integer('classification_material_id')->unsigned()->change();

            $table->dropForeign(['material_type_id']);
            $table->dropColumn('material_type_id');
            $table->dropForeign(['uom_id']);
            $table->dropColumn('uom_id');
            $table->dropForeign(['old_material']);
            $table->dropColumn('old_material');
            $table->dropForeign(['base_material']);
            $table->dropColumn('base_material');

            $table->dropColumn('sku_code');
            $table->dropColumn('nett_weight');
            $table->dropColumn('gross_weight');
            $table->dropColumn('weight_unit');
            $table->dropColumn('volume');
            $table->dropColumn('volume_uom');
        });
    }
}
