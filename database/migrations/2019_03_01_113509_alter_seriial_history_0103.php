<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeriialHistory0103 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serial_histories', function (Blueprint $table) {
            $table->renameColumn('movement_hostory_id', 'movement_history_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serial_histories', function (Blueprint $table) {
            $table->renameColumn('movement_history_id', 'movement_hostory_id');
        });
    }
}
