<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->text('description')->nullable()->change();
            $table->integer('group_material_id')->unsigned()->nullable()->change();
            $table->string('type')->nullable()->after('description')->comment('Material Type');
            $table->string('sku_code')->nullable()->after('type')->comment('Material SKU code');
            $table->integer('uom_id')->unsigned()->nullable()->after('sku_code')->comment('Base Unit of Measure');
            $table->string('nett_weight')->nullable()->after('uom_id')->comment('Nett weight in base uom');
            $table->string('gross_weight')->nullable()->after('nett_weight')->comment('Gross weight in base uom');
            $table->string('vol')->nullable()->after('gross_weight')->comment('Volume Material');
            $table->string('vol_uom')->nullable()->after('vol')->comment('Volume Unit');
            $table->string('tax_class')->nullable()->after('vol_uom')->comment('Tax classification');
            $table->string('account_assign')->nullable()->after('tax_class')->comment('Account assignment group');
            $table->string('proc_type')->nullable()->after('account_assign')->comment('0 = Internal, 1 = ExternalP');
            $table->integer('proc_group_id')->unsigned()->nullable()->after('proc_type')->comment('Procurement Group');

            $table->foreign('uom_id')->references('id')->on('uoms')->onDelete('cascade');
            $table->foreign('proc_group_id')->references('id')->on('procurement_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('materials', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->nullable()->after('id');

            $table->dropForeign(['uom_id']);
            $table->dropForeign(['proc_group_id']);

            $table->dropColumn('group_material_id');
            $table->dropColumn('type');
            $table->dropColumn('sku_code');
            $table->dropColumn('uom_id');
            $table->dropColumn('nett_weight');
            $table->dropColumn('gross_weight');
            $table->dropColumn('vol');
            $table->dropColumn('vol_uom');
            $table->dropColumn('tax_class');
            $table->dropColumn('account_assign');
            $table->dropColumn('proc_type');
            $table->dropColumn('proc_group_id');
        });
    }
}
