<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDepreTypePeriodTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('depre_type_period', function (Blueprint $table) {
           
            $table->dropColumn('month_change');
            $table->tinyInteger('softdelete')->default(0);
            $table->date('period');
            $table->boolean('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('depre_type_period', function (Blueprint $table) {
            
            $table->dropColumn('softdelete');
            $table->dropColumn('period');
        });
    }
}
