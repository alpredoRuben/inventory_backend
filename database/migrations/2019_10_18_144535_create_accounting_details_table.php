<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accounting_header_id')->unsigned()->nullable();
            $table->integer('seq_number')->nullable();
            $table->integer('account_type')->nullable();
            $table->integer('posting_key_id')->unsigned()->nullable();
            $table->string('d_c')->nullable();
            $table->dateTime('clearing_date')->nullable();
            $table->dateTime('clearing_created_time')->nullable();
            $table->string('clearning_doc')->nullable();
            $table->string('tax_code')->nullable();
            $table->integer('tax_type')->nullable()->comment('0.Input Tax; 1.Output Tax');
            $table->decimal('amount')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('amount_lc')->nullable();
            $table->string('assignment')->nullable();
            $table->string('co_area')->nullable();
            $table->integer('cost_center_id')->unsigned()->nullable();
            $table->string('purchasing_doc_no')->nullable();
            $table->string('purchasing_doc_item')->nullable();
            $table->string('billing_doc')->nullable();
            $table->string('sales_order')->nullable();
            $table->string('item_no')->nullable();
            $table->integer('asset_id')->unsigned()->nullable();
            $table->integer('account_id')->unsigned()->nullable();
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('bl_pl')->nullable();
            $table->dateTime('baseline_date')->nullable();
            $table->integer('term_payment_id')->unsigned()->nullable();
            $table->dateTime('due_date')->nullable();
            $table->timestamps();

            $table->foreign('accounting_header_id')->references('id')->on('accounting_headers')->onDelete('set null');
            $table->foreign('posting_key_id')->references('id')->on('posting_keys')->onDelete('set null');
            $table->foreign('cost_center_id')->references('id')->on('cost_centers')->onDelete('set null');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('set null');
            $table->foreign('vendor_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('term_payment_id')->references('id')->on('term_payments')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_details');
    }
}
