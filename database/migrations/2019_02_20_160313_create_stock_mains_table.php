<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockMainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_mains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->dateTime('period')->nullable();
            $table->integer('qty_unrestricted')->nullable();
            $table->integer('qty_blocked')->nullable();
            $table->integer('qty_transit')->nullable();
            $table->integer('qty_qa')->nullable();
            $table->timestamps();
            
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_mains');
    }
}
