<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeAssetImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_images', function (Blueprint $table) {
            $table->dropForeign('asset_images_asset_id_foreign');
            $table->dropForeign('asset_images_created_by_foreign');
            $table->dropForeign('asset_images_updated_by_foreign');

            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_images', function (Blueprint $table) {
            //
        });
    }
}
