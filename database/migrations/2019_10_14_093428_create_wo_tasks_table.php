<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWoTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wo_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trx_wo_id')->unsigned()->nullable();
            $table->string('operation_no', 30);
            $table->integer('level')->default('1');
            $table->integer('plant_id')->unsigned()->nullable();
            $table->string('description', 255)->nullable();
            $table->integer('estimated')->nullable();
            $table->integer('estimated_unit')->default('1')->comment('1 = hour, 2 = day');
            $table->integer('asset_id')->unsigned()->nullable();
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->integer('status')->default('0');
            $table->integer('classification_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('trx_wo_id')->references('id')->on('trx_wos')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('classification_id')->references('id')->on('classification_materials')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wo_tasks');
    }
}
