<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockSerialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_serials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_main_id')->unsigned()->nullable();
            $table->integer('stock_batch_id')->unsigned()->nullable();
            $table->string('serial')->nullable();
            $table->integer('stock_type')->nullable();//1=unrest, 2=in-transit, 3=quality, 4=blocked
            $table->integer('customer_id')->unsigned()->nullable();
            $table->integer('status')->nullable();//1=AVLB, 2=ESTO, 3=EQUI
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('stock_main_id')->references('id')->on('stock_mains')->onDelete('set null');
            $table->foreign('stock_batch_id')->references('id')->on('stock_batches')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_serials');
    }
}
