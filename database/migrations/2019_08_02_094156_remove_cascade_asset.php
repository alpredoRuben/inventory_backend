<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeAsset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropForeign('assets_asset_type_id_foreign');
            $table->dropForeign('assets_created_by_foreign');
            $table->dropForeign('assets_location_id_foreign');
            $table->dropForeign('assets_material_id_foreign');
            $table->dropForeign('assets_plant_id_foreign');
            $table->dropForeign('assets_responsible_person_id_foreign');
            $table->dropForeign('assets_retired_reason_id_foreign');
            $table->dropForeign('assets_status_id_foreign');
            $table->dropForeign('assets_supplier_id_foreign');
            $table->dropForeign('assets_updated_by_foreign');
            $table->dropForeign('assets_valuation_group_id_foreign');

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('asset_type_id')->references('id')->on('asset_types')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('status_id')->references('id')->on('asset_statuses')->onDelete('set null');
            $table->foreign('valuation_group_id')->references('id')->on('valuation_groups')->onDelete('set null');
            $table->foreign('retired_reason_id')->references('id')->on('retired_reasons')->onDelete('set null');
            $table->foreign('responsible_person_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
