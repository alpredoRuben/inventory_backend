<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeliveryHeaders1403 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->integer('transport_type_id')->unsigned()->nullable();
            $table->foreign('transport_type_id')->references('id')->on('transport_types')->onDelete('set null');

            $table->dropColumn('transport_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropForeign(['transport_type_id']);
            $table->dropColumn('transport_type_id');

            $table->string('transport_type')->nullable();
        });
    }
}
