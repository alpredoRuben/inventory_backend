<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reservation_code')->nullable()->comment('H');
            $table->integer('reservation_item')->nullable()->comment('D');
            $table->integer('reservation_type')->nullable()->comment('H');//1. Replenishment, 2. Purchase
            $table->text('note_header')->nullable()->comment('H');
            $table->integer('material_id')->unsigned()->nullable()->comment('D');//mat sort text from this relation
            $table->text('note_item')->nullable()->comment('D');
            $table->integer('plant_id')->unsigned()->nullable()->comment('H');
            $table->integer('storage_id')->unsigned()->nullable()->comment('H');
            $table->integer('batch')->nullable()->comment('D');
            $table->dateTime('requirement_date')->nullable()->comment('H');
            $table->string('quantity')->nullable()->comment('D');
            $table->integer('uom_id')->unsigned()->nullable()->comment('D');
            $table->string('dc')->nullable()->comment('H');// D atau C
            $table->integer('movement_type_id')->unsigned()->comment('H');
            $table->string('unit_cost')->nullable()->comment('D');
            $table->integer('inout_plant_id')->unsigned()->nullable()->comment('D');
            $table->integer('inout_storage_id')->unsigned()->nullable()->comment('D');
            $table->integer('status')->nullable()->comment('H');//0. draft, 1. pending_approval, 2. approved, 3. rejected
            $table->integer('approval_by')->unsigned()->nullable()->comment('H');
            $table->dateTime('approval_date')->nullable()->comment('H');
            $table->integer('material_plant_id')->unsigned()->nullable()->comment('D');//for proc type & proc group
            $table->integer('fulfillment_status')->nullable()->comment('');//0.Open, 1.Completed
            $table->integer('cost_center_id')->unsigned()->nullable()->comment('H');
            $table->integer('profit_center_id')->unsigned()->nullable()->comment('H');
            $table->integer('order_number')->nullable()->comment('D');
            $table->integer('planned_order')->nullable()->comment('D');
            $table->integer('planned_order_item')->nullable()->comment('D');
            $table->integer('purc_req_number')->nullable()->comment('D');
            $table->integer('purc_req_item')->nullable()->comment('D');
            $table->integer('po_number')->nullable()->comment('D');
            $table->integer('po_item_no')->nullable()->comment('D');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('movement_type_id')->references('id')->on('movement_types')->onDelete('set null');
            $table->foreign('inout_plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('inout_storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('approval_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('material_plant_id')->references('id')->on('material_plants')->onDelete('set null');
            $table->foreign('cost_center_id')->references('id')->on('cost_centers')->onDelete('set null');
            $table->foreign('profit_center_id')->references('id')->on('profit_centers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
