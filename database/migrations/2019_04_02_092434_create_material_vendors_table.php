<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->integer('proc_group_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->string('lead_time')->nullable();
            $table->string('unit_price')->nullable();
            $table->string('currency')->nullable();
            $table->dateTime('validity_from')->nullable();
            $table->string('std_qty')->nullable();
            $table->integer('std_uom_id')->unsigned()->nullable();
            $table->string('min_qty')->nullable();
            $table->integer('min_uom_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();
            
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('vendor_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('proc_group_id')->references('id')->on('procurement_groups')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('std_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('min_uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_vendors');
    }
}
