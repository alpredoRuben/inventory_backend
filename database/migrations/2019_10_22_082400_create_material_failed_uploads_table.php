<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
    		$table->string('material_type')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->string('group_material')->nullable();
            $table->string('classification')->nullable();
            $table->string('old_material')->nullable();
            $table->string('base_material')->nullable();
            $table->string('sku_code')->nullable();
            $table->string('uom')->nullable();
            $table->string('nett_weight_value')->nullable();
            $table->string('nett_weight_uom')->nullable();
            $table->string('gross_weight_value')->nullable();
            $table->string('volume_value')->nullable();
            $table->string('volume_uom')->nullable();
            $table->string('serial_profile')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_failed_uploads');
    }
}
