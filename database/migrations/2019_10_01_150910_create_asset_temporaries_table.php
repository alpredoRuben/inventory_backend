<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_temporaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_code')->nullable();
            $table->string('asset_number')->nullable();
            $table->string('description')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('master_asset')->nullable();
            $table->string('old_asset')->nullable();
            $table->string('vendor')->nullable();
            $table->string('plant')->nullable();
            $table->string('group_owner')->nullable();
            $table->string('asset_type')->nullable();
            $table->string('responsible_owner')->nullable();
            $table->string('location')->nullable();
            $table->string('asset_status')->nullable();
            $table->string('purchase_date')->nullable();
            $table->string('purchase_price')->nullable();
            $table->string('currency')->nullable();
            $table->string('warranty_end')->nullable();
            $table->string('maintenance_cycle')->nullable();
            $table->string('maintenance_cycle_unit')->nullable();
            $table->string('last_maintenance_date')->nullable();
            $table->string('retired_remark')->nullable();
            $table->string('c_merek')->nullable();
            $table->string('c_spek')->nullable();
            $table->string('valuation_type')->nullable();
            $table->string('straight_line')->nullable();
            $table->string('double_decline')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_temporaries');
    }
}
