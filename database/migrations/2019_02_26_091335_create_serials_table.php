<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('storage_id')->unsigned()->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->integer('supplier_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->string('serial')->nullable();
            $table->string('equipment')->nullable()->comment('Equipment diisi kalau di GI ke WO, movement type 241');
            $table->integer('status')->nullable()->comment('EST0 Unrest,EST1 Blocked,EST2 Intransit,EST3 QA');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serials');
    }
}
