<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationInMaterialStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_storages', function (Blueprint $table) {
            $table->dropColumn('storage_condition');
            $table->dropColumn('storage_type');
            $table->dropColumn('temp_condition');
            $table->dropColumn('le_unit');

            $table->integer('storage_condition_id')->unsigned()->nullable();
            $table->integer('storage_type_id')->unsigned()->nullable();
            $table->integer('temp_condition_id')->unsigned()->nullable();
            $table->integer('le_unit_id')->unsigned()->nullable();

            $table->foreign('storage_condition_id')->references('id')->on('storage_conditions')->onDelete('set null');
            $table->foreign('storage_type_id')->references('id')->on('storage_types')->onDelete('set null');
            $table->foreign('temp_condition_id')->references('id')->on('temp_conditions')->onDelete('set null');
            $table->foreign('le_unit_id')->references('id')->on('uoms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_storages', function (Blueprint $table) {
            $table->string('storage_condition')->nullable();
            $table->string('storage_type')->nullable();
            $table->string('temp_condition')->nullable();
            $table->string('le_unit')->nullable();

            $table->dropForeign(['storage_condition_id']);
            $table->dropColumn('storage_condition_id');

            $table->dropForeign(['storage_type_id']);
            $table->dropColumn('storage_type_id');

            $table->dropForeign(['temp_condition_id']);
            $table->dropColumn('temp_condition_id');

            $table->dropForeign(['le_unit_id']);
            $table->dropColumn('le_unit_id');
        });
    }
}
