<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStockBatch2604 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_batches', function (Blueprint $table) {
            $table->string('qty_unrestricted')->nullable();
            $table->string('qty_blocked')->nullable();
            $table->string('qty_transit')->nullable();
            $table->string('qty_qa')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_batches', function (Blueprint $table) {
            $table->dropColumn('qty_unrestricted');
            $table->dropColumn('qty_blocked');
            $table->dropColumn('qty_transit');
            $table->dropColumn('qty_qa');
        });
    }
}
