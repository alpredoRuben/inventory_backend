<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudget2006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budgets', function (Blueprint $table) {
            $table->integer('release_group_id')->unsigned()->nullable();
            $table->integer('release_strategy_id')->unsigned()->nullable();
            $table->string('release_indicator')->nullable();
            $table->string('release_state')->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->dateTime('approved_at')->nullable();

            $table->foreign('release_group_id')->references('id')->on('release_groups')->onDelete('set null');
            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('set null');
            $table->foreign('approved_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budgets', function (Blueprint $table) {
            $table->dropForeign(['release_group_id']);
            $table->dropColumn('release_group_id');

            $table->dropForeign(['release_strategy_id']);
            $table->dropColumn('release_strategy_id');

            $table->dropForeign(['approved_by']);
            $table->dropColumn('approved_by');

            $table->dropColumn('release_indicator');
            $table->dropColumn('release_state');
            $table->dropColumn('approved_at');
        });
    }
}
