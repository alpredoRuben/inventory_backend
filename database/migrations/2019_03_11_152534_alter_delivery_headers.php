<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeliveryHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropForeign(['sales_organization_id']);
            $table->dropColumn('sales_organization_id');

            $table->string('sales_org')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->integer('sales_organization_id')->unsigned()->nullable();
            $table->foreign('sales_organization_id')->references('id')->on('sales_organizations')->onDelete('set null');

            $table->dropColumn('sales_org');
        });
    }
}
