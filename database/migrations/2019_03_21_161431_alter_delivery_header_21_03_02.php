<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDeliveryHeader210302 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropColumn('delivery_type');
            $table->integer('delivery_types')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_headers', function (Blueprint $table) {
            $table->dropColumn('delivery_types');
            $table->boolean('delivery_type')->default(0)->nullable();
        });
    }
}
