<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMaterialPlant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            $table->boolean('proc_type')->default(0)->change();//0=internal, 1=external
            $table->boolean('batch_ind')->default(0)->change();
            $table->boolean('avail_check')->default(0)->change();
            $table->boolean('source_list')->default(0)->change();
            $table->boolean('hazardous_indicator')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_plants', function (Blueprint $table) {
            // 
        });
    }
}
