<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_id', 50);
            $table->string('prob_id', 50)->nullable();
            $table->string('service_route_destination', 1024)->nullable();
            $table->string('service_request', 4000)->nullable();
            $table->string('service_creator')->nullable();
            $table->date('ftimestap')->nullable();
            $table->string('service_state')->nullable();
            $table->string('destination')->nullable();
            $table->string('plant_id')->nullable();
            $table->string('plant_name')->nullable();
            $table->string('prob_description', 4000)->nullable();
            $table->string('last_reply')->nullable();
            $table->string('target_time')->nullable();
            $table->string('service_duration')->nullable();
            $table->string('category_name')->nullable();
            $table->string('dispatch')->nullable();
            $table->string('service_info_pengecekan', 4000)->nullable();
            $table->string('service_customer_info_rel', 4000)->nullable();
            $table->string('service_info_indikasi', 1024)->nullable();
            $table->string('service_info_progres_noc', 1024)->nullable();
            $table->string('service_info_lokasi', 1024)->nullable();
            $table->string('service_tipe')->nullable();
            $table->string('status_service_create_wo')->default('0')->nullable();
            $table->string('status_close_service')->default('0')->nullable();
            $table->integer('comment_service')->default('0')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_requests');
    }
}
