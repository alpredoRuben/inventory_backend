<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('move_type_code');
            $table->string('move_type_desc');
            $table->boolean('reason_ind')->comment('Reason indicator');
            $table->string('dc_ind')->comment('Debit credit indicator');
            $table->string('rev_code')->comment('Reversal movement type');
            $table->integer('stock_det_id')->unsigned()->nullable()->comment('Reversal movement type');
            $table->boolean('batch_ind')->comment('Created Batch Number');
            $table->boolean('qi_ind')->comment('Created Quality Inspection');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_types');
    }
}
