<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverySerialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_serials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('delivery_header_id')->unsigned()->nullable();
            $table->string('delivery_item')->nullable();
            $table->integer('sequence')->nullable();
            $table->integer('material_id')->unsigned()->nullable();
            $table->string('serial')->nullable();            
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);

            $table->foreign('delivery_header_id')->references('id')->on('delivery_headers')->onDelete('set null');
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_serials');
    }
}
