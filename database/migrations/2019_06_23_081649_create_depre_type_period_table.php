<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepreTypePeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depre_type_period', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('depre_type_name_id');
            $table->year('period');
            $table->decimal('rates', 8, 3);
            $table->integer('eco_lifetime');
            $table->date('month_change');
            $table->smallInteger('deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depre_type_period');
    }
}
