<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_temporaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fiscal_year')->nullable();
            $table->string('description')->nullable();
            $table->string('project_code')->nullable();
            $table->string('cost_center')->nullable();
            $table->string('gl_account')->nullable();
            $table->string('material')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('company')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_temporaries');
    }
}
