<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseItemIdInMaterialVendorHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_vendor_histories', function (Blueprint $table) {
            $table->integer('purchase_item_id')->unsigned()->nullable();
            $table->foreign('purchase_item_id')->references('id')->on('purchase_items')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_vendor_histories', function (Blueprint $table) {
            $table->dropForeign('material_vendor_histories_purchase_item_id_foreign');
            $table->dropColumn('purchase_item_id');
        });
    }
}
