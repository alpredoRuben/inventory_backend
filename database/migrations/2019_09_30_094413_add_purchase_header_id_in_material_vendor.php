<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseHeaderIdInMaterialVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_vendors', function (Blueprint $table) {
            $table->integer('purchase_header_id')->unsigned()->nullable();
            $table->foreign('purchase_header_id')->references('id')->on('purchase_headers')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_vendors', function (Blueprint $table) {
            $table->dropForeign('material_vendors_purchase_header_id_foreign');
            $table->dropColumn('purchase_header_id');
        });
    }
}
