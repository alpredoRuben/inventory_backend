<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservation2003 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('material_short_text')->nullable()->comment('D');
            $table->string('document_type')->nullable()->comment('H');
            $table->string('item_type')->nullable();
            $table->string('currency')->nullable();
            $table->integer('prefered_vendor_id')->unsigned()->nullable()->comment('D');
            $table->string('release_group')->nullable()->comment('D');
            $table->string('release_strategy')->nullable()->comment('D');
            $table->string('approval_level')->nullable()->comment('D');
            $table->integer('project_id')->unsigned()->nullable()->comment('D');
            $table->integer('account_id')->unsigned()->nullable()->comment('D');
            $table->integer('commitment_item_id')->unsigned()->nullable();

            $table->foreign('prefered_vendor_id')->references('id')->on('suppliers')->onDelete('set null');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('set null');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('set null');
            $table->foreign('commitment_item_id')->references('id')->on('budgets')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign(['prefered_vendor_id']);
            $table->dropColumn('prefered_vendor_id');
            $table->dropForeign(['project_id']);
            $table->dropColumn('project_id');
            $table->dropForeign(['account_id']);
            $table->dropColumn('account_id');
            $table->dropForeign(['commitment_item_id']);
            $table->dropColumn('commitment_item_id');

            $table->dropColumn('material_short_text');
            $table->dropColumn('document_type');
            $table->dropColumn('item_type');
            $table->dropColumn('currency');
            $table->dropColumn('release_group');
            $table->dropColumn('release_strategy');
            $table->dropColumn('approval_level');
        });
    }
}
