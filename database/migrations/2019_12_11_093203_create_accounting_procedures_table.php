<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounting_procedures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_type_id')->unsigned()->nullable();
            $table->integer('sub_type_id')->unsigned()->nullable();
            $table->integer('valuation_class_id')->unsigned()->nullable();
            $table->integer('gl_account_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('transaction_type_id')->references('id')->on('transaction_types')->onDelete('set null');
            $table->foreign('sub_type_id')->references('id')->on('sub_types')->onDelete('set null');
            $table->foreign('valuation_class_id')->references('id')->on('valuation_classes')->onDelete('set null');
            $table->foreign('gl_account_id')->references('id')->on('accounts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounting_procedures');
    }
}
