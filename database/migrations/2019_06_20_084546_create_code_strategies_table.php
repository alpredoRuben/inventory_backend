<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodeStrategiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_strategies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('release_code_id')->unsigned();
            $table->integer('release_strategy_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('release_code_id')->references('id')->on('release_codes')->onDelete('cascade');
            $table->foreign('release_strategy_id')->references('id')->on('release_strategies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_strategies');
    }
}
