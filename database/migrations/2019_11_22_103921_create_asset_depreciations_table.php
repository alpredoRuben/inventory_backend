<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetDepreciationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_depreciations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_id')->unsigned()->nullable();
            $table->integer('number');
            $table->integer('index_dd')->nullable();
            $table->string('depre_type')->comment('sl = straight line. dd = double decline');
            $table->string('month');
            $table->string('year');
            $table->decimal('depreciation', 12, 2);
            $table->decimal('total_depreciation', 12, 2);
            $table->decimal('book_value', 12, 2);
            $table->integer('material_group_id')->unsigned()->nullable();
            $table->integer('gl_account_id')->unsigned()->nullable();
            $table->integer('cost_center_id')->unsigned()->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('material_group_id')->references('id')->on('group_materials')->onDelete('set null');
            $table->foreign('gl_account_id')->references('id')->on('accounts')->onDelete('set null');
            $table->foreign('cost_center_id')->references('id')->on('cost_centers')->onDelete('set null');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_depreciations');
    }
}
