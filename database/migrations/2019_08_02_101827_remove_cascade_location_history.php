<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCascadeLocationHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('location_histories', function (Blueprint $table) {
            $table->dropForeign('location_histories_asset_id_foreign');
            $table->dropForeign('location_histories_created_by_foreign');
            $table->dropForeign('location_histories_old_location_id_foreign');
            
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('set null');
            $table->foreign('old_location_id')->references('id')->on('locations')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('location_histories', function (Blueprint $table) {
            //
        });
    }
}
