<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrxTaskListItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trx_task_list_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tasklist_id')->unsigned()->nullable();
            $table->integer('plant_id')->unsigned()->nullable();
            $table->integer('classification_id')->unsigned()->nullable();
            $table->string('operation_no', 30);
            $table->string('description', 255);
            $table->integer('estimated');
            $table->integer('estimated_unit')->comment('1 = minute, 2 = hour, 3 = day');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->foreign('tasklist_id')->references('id')->on('trx_task_lists')->onDelete('cascade');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('cascade');
            $table->foreign('classification_id')->references('id')->on('classification_materials')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trx_task_list_items');
    }
}
