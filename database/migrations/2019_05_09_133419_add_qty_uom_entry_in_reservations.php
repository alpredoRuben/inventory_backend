<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyUomEntryInReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->string('quantity_base_unit')->nullable()->comment('D');
            $table->integer('uom_base_id')->unsigned()->nullable()->comment('D');

            $table->foreign('uom_base_id')->references('id')->on('uoms')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign(['uom_base_id']);
            $table->dropColumn('uom_base_id');
            $table->dropColumn('quantity_base_unit');
        });
    }
}
