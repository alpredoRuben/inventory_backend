<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_id')->unsigned();
            $table->integer('plant_id')->unsigned();
            $table->string('valuation_type')->nullable();
            $table->string('valuation_class');
            $table->integer('price_controll')->nullable();//0. MAP, 1. Standard, 2. LIFO/FIFO
            $table->float('moving_price', 15, 2)->nullable();//required if price controll 0=Map
            $table->float('standart_price', 15, 2)->nullable();//required if price controll 1=standart
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_accounts');
    }
}
