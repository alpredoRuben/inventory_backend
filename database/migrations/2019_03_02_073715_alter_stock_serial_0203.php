<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStockSerial0203 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_serials', function (Blueprint $table) {
            $table->dropForeign(['stock_main_id']);
            $table->dropColumn('stock_main_id');

            $table->integer('material_id')->unsigned()->nullable();
            $table->foreign('material_id')->references('id')->on('materials')->onDelete('set null');

            $table->integer('storage_id')->unsigned()->nullable();
            $table->foreign('storage_id')->references('id')->on('storages')->onDelete('set null');

            $table->integer('plant_id')->unsigned()->nullable();
            $table->foreign('plant_id')->references('id')->on('plants')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_serials', function (Blueprint $table) {
            $table->integer('stock_main_id')->unsigned()->nullable();
            $table->foreign('stock_main_id')->references('id')->on('stock_mains')->onDelete('set null');

            $table->dropForeign(['material_id']);
            $table->dropColumn('material_id');

            $table->dropForeign(['storage_id']);
            $table->dropColumn('storage_id');

            $table->dropForeign(['plant_id']);
            $table->dropColumn('plant_id');
        });
    }
}
