<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBomDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bom_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bom_id')->unsigned()->nullable();
            $table->integer('component_id')->unsigned()->nullable();
            $table->string('quantity')->nullable();
            $table->integer('uom_id')->unsigned()->nullable();
            $table->string('quantity_entry')->nullable();
            $table->integer('uom_entry_id')->unsigned()->nullable();
            $table->dateTime('valid_from')->nullable();
            $table->dateTime('valid_to')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('deleted')->default(0);
            $table->timestamps();

            $table->foreign('bom_id')->references('id')->on('boms')->onDelete('set null');
            $table->foreign('component_id')->references('id')->on('materials')->onDelete('set null');
            $table->foreign('uom_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('uom_entry_id')->references('id')->on('uoms')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bom_details');
    }
}
