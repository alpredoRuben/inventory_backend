<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetFailedUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_failed_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material')->nullable();
            $table->string('description')->nullable();
            $table->string('asset_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('superior_asset')->nullable();
            $table->string('old_asset')->nullable();
            $table->string('straight_line')->nullable();
            $table->string('double_decline')->nullable();
            $table->string('material_group')->nullable();
            $table->string('vendor')->nullable();
            $table->string('plant')->nullable();
            $table->string('location')->nullable();
            $table->string('status')->nullable();
            $table->string('cost_center')->nullable();
            $table->string('group_owner')->nullable();
            $table->string('asset_type')->nullable();
            $table->string('valuation_type')->nullable();
            $table->string('responsible_person')->nullable();
            $table->string('depreciation_date')->nullable();
            $table->string('purchase_price')->nullable();
            $table->string('curency')->nullable();
            $table->string('quantity_asset')->nullable();
            $table->string('salvage_value')->nullable();
            $table->string('warranty_end')->nullable();
            $table->string('maintenance_cycle')->nullable();
            $table->string('maintenance_cycle_unit')->nullable();
            $table->string('last_maintenance')->nullable();
            $table->string('retired_date')->nullable();
            $table->string('retired_reason')->nullable();
            $table->string('retired_remarks')->nullable();
            $table->text('message')->nullable();
            $table->integer('uploaded_by')->unsigned()->nullable();
            $table->timestamps();
            
            $table->foreign('uploaded_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_failed_uploads');
    }
}
