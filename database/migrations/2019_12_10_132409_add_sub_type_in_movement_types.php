<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubTypeInMovementTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->integer('sub_type_id')->unsigned()->nullable();
            $table->foreign('sub_type_id')->references('id')->on('sub_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movement_types', function (Blueprint $table) {
            $table->dropForeign(['sub_type_id']);
            $table->dropColumn('sub_type_id');
        });
    }
}
