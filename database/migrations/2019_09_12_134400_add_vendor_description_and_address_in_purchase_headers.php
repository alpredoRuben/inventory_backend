<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorDescriptionAndAddressInPurchaseHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->string('vendor_description')->nullable();
            $table->string('vendor_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_headers', function (Blueprint $table) {
            $table->dropColumn('vendor_description');
            $table->dropColumn('vendor_address');
        });
    }
}
