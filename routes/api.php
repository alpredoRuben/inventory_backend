<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// cek koneksi
Route::get('/ping', function () {
    return "ok";
});

// clean transaction table
Route::middleware('auth:api')->get('/clint', function () {
    Artisan::call('clean:transactiontable');
});

// Guest User API
Route::group(['prefix' => 'user', 'namespace' => 'Api'], function () {
    Route::post('/login', 'AuthController@authenticate');
    Route::post('/confirm-email/{id}', 'AuthController@confirmEmail');
    Route::post('/forgot-password', 'AuthController@forgotPassword');
});

// Authenticated User API (Dashboard)
Route::group(['middleware' => ['auth:api', 'activity'], 'prefix' => 'user', 'namespace' => 'Api'], function () {
    Route::get('/profile', 'ProfileController@profile');
    Route::post('/update-login-detail', 'ProfileController@updateLoginDetail');
    Route::post('/update-profile', 'ProfileController@updateProfile');
    Route::get('/login-history', 'ProfileController@loginHistory');
    Route::post('/change-photo-profile', 'ProfileController@changePhotoProfile');
    Route::post('/change-password', 'ProfileController@changePassword');
    Route::get('/module-object-list', 'ProfileController@moduleList');
});

// Authenticated User API (Master)
Route::group(['middleware' => ['auth:api', 'activity'], 'prefix' => 'master', 'namespace' => 'Api'], function () {
    // User Group
    Route::get('/user-group', 'Master\UserGroupController@index');
    Route::get('/user-group-list', 'Master\UserGroupController@list');
    Route::post('/user-group', 'Master\UserGroupController@store');
    Route::get('/user-group/{id}', 'Master\UserGroupController@show');
    Route::get('/user-group-log/{id}', 'Master\UserGroupController@log');
    Route::put('/user-group/{id}', 'Master\UserGroupController@update');
    Route::delete('/user-group/{id}', 'Master\UserGroupController@delete');
    Route::post('/multiple-delete-user-group', 'Master\UserGroupController@multipleDelete');

    // Company
    Route::get('/company', 'Master\CompanyController@index');
    Route::get('/company-list', 'Master\CompanyController@list');
    Route::post('/company', 'Master\CompanyController@store');
    Route::get('/company/{id}', 'Master\CompanyController@show');
    Route::get('/company-log/{id}', 'Master\CompanyController@log');
    Route::put('/company/{id}', 'Master\CompanyController@update');
    Route::delete('/company/{id}', 'Master\CompanyController@delete');
    Route::post('/multiple-delete-company', 'Master\CompanyController@multipleDelete');

    // Department
    Route::get('/department', 'Master\DepartmentController@index');
    Route::get('/department-list', 'Master\DepartmentController@list');
    Route::post('/department', 'Master\DepartmentController@store');
    Route::get('/department/{id}', 'Master\DepartmentController@show');
    Route::get('/department-log/{id}', 'Master\DepartmentController@log');
    Route::put('/department/{id}', 'Master\DepartmentController@update');
    Route::delete('/department/{id}', 'Master\DepartmentController@delete');
    Route::post('/multiple-delete-department', 'Master\DepartmentController@multipleDelete');

    // CostCenter
    Route::get('/cost-center', 'Master\CostCenterController@index');
    Route::get('/cost-center-list', 'Master\CostCenterController@list');
    Route::post('/cost-center', 'Master\CostCenterController@store');
    Route::get('/cost-center/{id}', 'Master\CostCenterController@show');
    Route::get('/cost-center-log/{id}', 'Master\CostCenterController@log');
    Route::put('/cost-center/{id}', 'Master\CostCenterController@update');
    Route::delete('/cost-center/{id}', 'Master\CostCenterController@delete');
    Route::post('/multiple-delete-cost-center', 'Master\CostCenterController@multipleDelete');

    // Get Data by company Id
    Route::get('/department-by-company/{id}', 'Master\DepartmentController@departmentCompany');
    Route::get('/cost-center-by-company/{id}', 'Master\CostCenterController@costCenterByCompany');

    // Master User
    Route::get('/user', 'Master\UserController@index');
    Route::get('/user-list', 'Master\UserController@list');
    Route::post('/user', 'Master\UserController@store');
    Route::get('/user/{id}', 'Master\UserController@show');
    Route::get('/user-log/{id}', 'Master\UserController@log');
    Route::put('/user/{id}', 'Master\UserController@update');
    Route::delete('/user/{id}', 'Master\UserController@delete');
    Route::post('/multiple-delete-user', 'Master\UserController@multipleDelete');
    Route::get('/activate-user/{id}', 'Master\UserController@activate');
    Route::get('/block-user/{id}', 'Master\UserController@block');
    Route::get('/download-user-matrix', 'Master\UserController@userMatrixDownload');

    // User Profile
    Route::get('/user-profile-param', 'Master\UserController@profileParam');
    Route::post('/user-profile-param/{id}', 'Master\UserController@storeUserProfileParam');
    Route::post('/user/{id}/profile', 'Master\UserController@storeProfile');
    Route::post('/user/{id}/photo-profile', 'Master\UserController@storePhotoProfile');

    // User Role
    Route::post('/user/{id}/asign-role', 'Master\UserController@assignRole');
    Route::get('/user/{id}/role-list', 'Master\UserController@userRoleList');
    Route::post('/delete-user-role/{id}', 'Master\UserController@deleteUserRole');

    // User Login History
    Route::get('/user/{id}/login-history', 'Master\UserController@userLoginHistory');

    // Role
    Route::get('/role', 'Master\RoleController@index');
    Route::get('/role-list', 'Master\RoleController@list');
    Route::post('/role', 'Master\RoleController@store');
    Route::post('/copy-role', 'Master\RoleController@copy');
    Route::get('/composite-role/{id}', 'Master\RoleController@compositeList');
    Route::post('/composite-role/{id}', 'Master\RoleController@composite');
    Route::post('/composite-role-delete/{id}', 'Master\RoleController@uncomposite');
    Route::get('/role/{id}', 'Master\RoleController@show');
    Route::get('/role-log/{id}', 'Master\RoleController@log');
    Route::put('/role/{id}', 'Master\RoleController@update');
    Route::delete('/role/{id}', 'Master\RoleController@delete');
    Route::post('/multiple-delete-role', 'Master\RoleController@multipleDelete');

    // Role Auth Object
    Route::post('/role/{id}/add-auth-object', 'Master\RoleController@storeRoleAuthObject');
    Route::get('/role/{id}/auth-object-list', 'Master\RoleController@roleAuthObjectList');
    Route::put('/role/{id}/edit-auth-object', 'Master\RoleController@updateRoleAuthObject');

    // Role Organization Parameter
    Route::post('/role/{id}/add-organization-parameter', 'Master\RoleController@storeRoleOrgParam');
    Route::get('/role/{id}/organization-parameter-list', 'Master\RoleController@roleOrgParamList');
    Route::put('/role/{id}/edit-organization-parameter', 'Master\RoleController@updateRoleOrgParam');

    // Role User
    Route::post('/role/{id}/asign-user', 'Master\RoleController@assignUser');
    Route::get('/role/{id}/user-list', 'Master\RoleController@roleUserList');
    Route::post('/delete-role-user/{id}', 'Master\RoleController@deleteRoleUser');

    // Modules (Auth Object)
    Route::get('/all-modules', 'Master\AuthObjectController@allData');
    Route::get('/modules', 'Master\AuthObjectController@index');
    Route::post('/modules', 'Master\AuthObjectController@stores');
    Route::get('/modules/{id}', 'Master\AuthObjectController@show');
    Route::put('/modules/{id}', 'Master\AuthObjectController@update');
    Route::delete('/modules/{id}', 'Master\AuthObjectController@delete');

    // Classification Type
    Route::get('/classification-type', 'Master\ClassificationTypeController@index');
    Route::post('/classification-type', 'Master\ClassificationTypeController@store');
    Route::get('/classification-type/{id}', 'Master\ClassificationTypeController@show');
    Route::put('/classification-type/{id}', 'Master\ClassificationTypeController@update');
    Route::delete('/classification-type/{id}', 'Master\ClassificationTypeController@delete');
    Route::post('/multiple-delete-classification-type', 'Master\ClassificationTypeController@multipleDelete');

    // Classification Material (Classification Master)
    Route::get('/classification-material', 'Master\ClassificationController@index');
    Route::get('/classification-material-list', 'Master\ClassificationController@list');
    Route::post('/classification-material', 'Master\ClassificationController@store');
    Route::get('/classification-material/{id}', 'Master\ClassificationController@show');
    Route::get('/classification-material-log/{id}', 'Master\ClassificationController@log');
    Route::put('/classification-material/{id}', 'Master\ClassificationController@update');
    Route::delete('/classification-material/{id}', 'Master\ClassificationController@delete');
    Route::post('/multiple-delete-classification-mat', 'Master\ClassificationController@multipleDelete');

    // Classification Parameter
    Route::post('/classification-material/{id}/addparam', 'Master\ClassificationController@clasificationStoreParam');
    Route::get('/classification-parameter/{id}', 'Master\ClassificationController@showClassificationParameter');
    Route::put('/classification-parameter/{id}', 'Master\ClassificationController@updateClassificationParameter');
    Route::delete('/classification-parameter/{id}', 'Master\ClassificationController@deleteClassificationParameter');
    Route::post('/multiple-delete-classification-param', 'Master\ClassificationController@multipleDeleteClassificationParam');

    // Material Group
    Route::get('/group-material', 'Master\GroupMaterialController@index');
    Route::get('/group-material-list', 'Master\GroupMaterialController@list');
    Route::post('/group-material', 'Master\GroupMaterialController@store');
    Route::get('/group-material/{id}', 'Master\GroupMaterialController@show');
    Route::get('/group-material-log/{id}', 'Master\GroupMaterialController@log');
    Route::put('/group-material/{id}', 'Master\GroupMaterialController@update');
    Route::delete('/group-material/{id}', 'Master\GroupMaterialController@delete');
    Route::post('/multiple-delete-group-material', 'Master\GroupMaterialController@multipleDelete');

    // Location Type
    Route::get('/location-type', 'Master\LocationTypeController@index');
    Route::get('/location-type-list', 'Master\LocationTypeController@list');
    Route::post('/location-type', 'Master\LocationTypeController@store');
    Route::get('/location-type/{id}', 'Master\LocationTypeController@show');
    Route::get('/location-type-log/{id}', 'Master\LocationTypeController@log');
    Route::post('/update-location-type/{id}', 'Master\LocationTypeController@update');
    Route::delete('/location-type/{id}', 'Master\LocationTypeController@delete');
    Route::post('/multiple-delete-location-type', 'Master\LocationTypeController@multipleDelete');

    // Plant
    Route::get('/plant', 'Master\PlantController@index');
    Route::get('/plant-list', 'Master\PlantController@list');
    Route::post('/plant', 'Master\PlantController@store');
    Route::get('/plant/{id}', 'Master\PlantController@show');
    Route::get('/plant-log/{id}', 'Master\PlantController@log');
    Route::put('/plant/{id}', 'Master\PlantController@update');
    Route::delete('/plant/{id}', 'Master\PlantController@delete');
    Route::post('/multiple-delete-plant', 'Master\PlantController@multipleDelete');

    Route::get('/company-list-by-type/{id}', 'Master\LocationController@getCompanyByType');

    // Location
    Route::get('/location', 'Master\LocationController@index');
    Route::get('/location-list-hash', 'Master\LocationController@list');
    Route::post('/location', 'Master\LocationController@store');
    Route::get('/location/{id}', 'Master\LocationController@show');
    Route::get('/location-log/{id}', 'Master\LocationController@log');
    Route::put('/location/{id}', 'Master\LocationController@update');
    Route::delete('/location/{id}', 'Master\LocationController@delete');
    Route::post('/multiple-delete-location', 'Master\LocationController@multipleDelete');

    Route::get('/location-list-by-plant/{id}', 'Master\LocationController@getLocationByPlant');
    Route::get('/user-list-by-location/{id}', 'Master\LocationController@getUserByLocation');
    Route::get('/location-list', 'Master\LocationController@getLocationList');
    Route::get('/param-list-by-classification/{id}', 'Master\ClassificationController@parameterByClassification');

    // Storage
    Route::get('/storage', 'Master\StorageController@index');
    Route::get('/storage-list', 'Master\StorageController@list');
    Route::post('/storage', 'Master\StorageController@store');
    Route::get('/storage/{id}', 'Master\StorageController@show');
    Route::get('/storage-log/{id}', 'Master\StorageController@log');
    Route::put('/storage/{id}', 'Master\StorageController@update');
    Route::delete('/storage/{id}', 'Master\StorageController@delete');
    Route::post('/multiple-delete-storage', 'Master\StorageController@multipleDelete');
    Route::get('/storage-by-plant/{id}', 'Master\StorageController@getStorageByPlant');

    // Storage Upload
    Route::get('/download-template-storage', 'Master\StorageController@template');
    Route::post('/storage-upload', 'Master\StorageController@upload');
    Route::get('/storage-invalid-data', 'Master\StorageController@getInvalidUpload');
    Route::get('/storage-invalid-download', 'Master\StorageController@downloadInvalidUpload');
    Route::delete('/storage-invalid', 'Master\StorageController@deleteInvalidUpload');

    // Supplier (Vendor)
    Route::get('/supplier', 'Master\SupplierController@index');
    Route::get('/supplier-list', 'Master\SupplierController@list');
    Route::post('/supplier', 'Master\SupplierController@store');
    Route::get('/supplier/{id}', 'Master\SupplierController@show');
    Route::get('/supplier-log/{id}', 'Master\SupplierController@log');
    Route::put('/supplier/{id}', 'Master\SupplierController@update');
    Route::delete('/supplier/{id}', 'Master\SupplierController@delete');
    Route::post('/multiple-delete-supplier', 'Master\SupplierController@multipleDelete');

    // Customer
    Route::get('/customer', 'Master\CustomerController@index');
    Route::get('/customer-list', 'Master\CustomerController@list');
    Route::post('/customer', 'Master\CustomerController@store');
    Route::get('/customer/{id}', 'Master\CustomerController@show');
    Route::get('/customer-log/{id}', 'Master\CustomerController@log');
    Route::put('/customer/{id}', 'Master\CustomerController@update');
    Route::delete('/customer/{id}', 'Master\CustomerController@delete');
    Route::post('/multiple-delete-customer', 'Master\CustomerController@multipleDelete');

    // Uom (Unit of Measure)
    Route::get('/uom', 'Master\UomController@index');
    Route::get('/uom-list', 'Master\UomController@list');
    Route::post('/uom', 'Master\UomController@store');
    Route::get('/uom/{id}', 'Master\UomController@show');
    Route::get('/uom-log/{id}', 'Master\UomController@log');
    Route::get('/uom-hash/{id}', 'Master\UomController@showHash');
    Route::put('/uom/{id}', 'Master\UomController@update');
    Route::delete('/uom/{id}', 'Master\UomController@delete');
    Route::post('/multiple-delete-uom', 'Master\UomController@multipleDelete');

    // Movement Type
    Route::get('/movement-type', 'Master\MovementTypeController@index');
    Route::get('/movement-type-list', 'Master\MovementTypeController@list');
    Route::post('/movement-type', 'Master\MovementTypeController@store');
    Route::get('/movement-type/{id}', 'Master\MovementTypeController@show');
    Route::get('/movement-type-log/{id}', 'Master\MovementTypeController@log');
    Route::put('/movement-type/{id}', 'Master\MovementTypeController@update');
    Route::delete('/movement-type/{id}', 'Master\MovementTypeController@delete');
    Route::post('/multiple-delete-movement-type', 'Master\MovementTypeController@multipleDelete');

    // Movement Type Reason
    Route::get('/movement-type/{id}/reason-list', 'Master\MovementTypeController@movementTypeReasonList');
    Route::post('/movement-type/{id}/add-reason', 'Master\MovementTypeController@storeMovementTypeReason');
    Route::get('/movement-type-reason/{id}', 'Master\MovementTypeController@showMovementTypeReason');
    Route::put('/movement-type-reason/{id}', 'Master\MovementTypeController@updateMovementTypeReason');
    Route::delete('/movement-type-reason/{id}', 'Master\MovementTypeController@deleteMovementTypeReason');

    // Procurement Group
    Route::get('/procurement-group', 'Master\ProcurementGroupController@index');
    Route::get('/procurement-group-list', 'Master\ProcurementGroupController@list');
    Route::post('/procurement-group', 'Master\ProcurementGroupController@store');
    Route::get('/procurement-group/{id}', 'Master\ProcurementGroupController@show');
    Route::get('/procurement-group-log/{id}', 'Master\ProcurementGroupController@log');
    Route::put('/procurement-group/{id}', 'Master\ProcurementGroupController@update');
    Route::delete('/procurement-group/{id}', 'Master\ProcurementGroupController@delete');
    Route::post('/multiple-delete-procurement-group', 'Master\ProcurementGroupController@multipleDelete');

    // Profit Center
    Route::get('/profit-center', 'Master\ProfitCenterController@index');
    Route::get('/profit-center-list', 'Master\ProfitCenterController@list');
    Route::post('/profit-center', 'Master\ProfitCenterController@store');
    Route::get('/profit-center/{id}', 'Master\ProfitCenterController@show');
    Route::put('/profit-center/{id}', 'Master\ProfitCenterController@update');
    Route::delete('/profit-center/{id}', 'Master\ProfitCenterController@delete');
    Route::post('/multiple-delete-profit-center', 'Master\ProfitCenterController@multipleDelete');

    // Mrp controller
    Route::get('/mrp', 'Master\MrpController@index');
    Route::get('/mrp-list', 'Master\MrpController@list');
    Route::post('/mrp', 'Master\MrpController@store');
    Route::get('/mrp/{id}', 'Master\MrpController@show');
    Route::put('/mrp/{id}', 'Master\MrpController@update');
    Route::delete('/mrp/{id}', 'Master\MrpController@delete');
    Route::post('/multiple-delete-mrp', 'Master\MrpController@multipleDelete');

    // Product Champion
    Route::get('/product-champion', 'Master\ProductChampionController@index');
    Route::get('/product-champion-list', 'Master\ProductChampionController@list');
    Route::post('/product-champion', 'Master\ProductChampionController@store');
    Route::get('/product-champion/{id}', 'Master\ProductChampionController@show');
    Route::put('/product-champion/{id}', 'Master\ProductChampionController@update');
    Route::delete('/product-champion/{id}', 'Master\ProductChampionController@delete');
    Route::post('/multiple-delete-product-champion', 'Master\ProductChampionController@multipleDelete');

    // Storage Condition
    Route::get('/storage-condition', 'Master\StorageConditionController@index');
    Route::get('/storage-condition-list', 'Master\StorageConditionController@list');
    Route::post('/storage-condition', 'Master\StorageConditionController@store');
    Route::get('/storage-condition/{id}', 'Master\StorageConditionController@show');
    Route::put('/storage-condition/{id}', 'Master\StorageConditionController@update');
    Route::delete('/storage-condition/{id}', 'Master\StorageConditionController@delete');
    Route::post('/multiple-delete-storage-condition', 'Master\StorageConditionController@multipleDelete');

    // Storage Type
    Route::get('/storage-type', 'Master\StorageTypeController@index');
    Route::get('/storage-type-list', 'Master\StorageTypeController@list');
    Route::post('/storage-type', 'Master\StorageTypeController@store');
    Route::get('/storage-type/{id}', 'Master\StorageTypeController@show');
    Route::put('/storage-type/{id}', 'Master\StorageTypeController@update');
    Route::delete('/storage-type/{id}', 'Master\StorageTypeController@delete');
    Route::post('/multiple-delete-storage-type', 'Master\StorageTypeController@multipleDelete');

    // Temp Condition
    Route::get('/temp-condition', 'Master\TempConditionController@index');
    Route::get('/temp-condition-list', 'Master\TempConditionController@list');
    Route::post('/temp-condition', 'Master\TempConditionController@store');
    Route::get('/temp-condition/{id}', 'Master\TempConditionController@show');
    Route::put('/temp-condition/{id}', 'Master\TempConditionController@update');
    Route::delete('/temp-condition/{id}', 'Master\TempConditionController@delete');
    Route::post('/multiple-delete-temp-condition', 'Master\TempConditionController@multipleDelete');

    // Sales Organization
    Route::get('/sales-organization', 'Master\SalesOrganizationController@index');
    Route::get('/sales-organization-list', 'Master\SalesOrganizationController@list');
    Route::post('/sales-organization', 'Master\SalesOrganizationController@store');
    Route::get('/sales-organization/{id}', 'Master\SalesOrganizationController@show');
    Route::put('/sales-organization/{id}', 'Master\SalesOrganizationController@update');
    Route::delete('/sales-organization/{id}', 'Master\SalesOrganizationController@delete');
    Route::post('/multiple-delete-sales-organization', 'Master\SalesOrganizationController@multipleDelete');

    // Account Assignment
    Route::get('/account-assignment', 'Master\AccountAssignmentController@index');
    Route::get('/account-assignment-list', 'Master\AccountAssignmentController@list');
    Route::post('/account-assignment', 'Master\AccountAssignmentController@store');
    Route::get('/account-assignment/{id}', 'Master\AccountAssignmentController@show');
    Route::put('/account-assignment/{id}', 'Master\AccountAssignmentController@update');
    Route::delete('/account-assignment/{id}', 'Master\AccountAssignmentController@delete');
    Route::post('/multiple-delete-account-assignment', 'Master\AccountAssignmentController@multipleDelete');

    // Item Category
    Route::get('/item-category', 'Master\ItemCategoryController@index');
    Route::get('/item-category-list', 'Master\ItemCategoryController@list');
    Route::post('/item-category', 'Master\ItemCategoryController@store');
    Route::get('/item-category/{id}', 'Master\ItemCategoryController@show');
    Route::put('/item-category/{id}', 'Master\ItemCategoryController@update');
    Route::delete('/item-category/{id}', 'Master\ItemCategoryController@delete');
    Route::post('/multiple-delete-item-category', 'Master\ItemCategoryController@multipleDelete');

    // Valuation Type
    Route::get('/valuation-type', 'Master\ValuationTypeController@index');
    Route::get('/valuation-type-list', 'Master\ValuationTypeController@list');
    Route::post('/valuation-type', 'Master\ValuationTypeController@store');
    Route::get('/valuation-type/{id}', 'Master\ValuationTypeController@show');
    Route::put('/valuation-type/{id}', 'Master\ValuationTypeController@update');
    Route::delete('/valuation-type/{id}', 'Master\ValuationTypeController@delete');
    Route::post('/multiple-delete-valuation-type', 'Master\ValuationTypeController@multipleDelete');

    // Valuation Class
    Route::get('/valuation-class', 'Master\ValuationClassController@index');
    Route::get('/valuation-class-list', 'Master\ValuationClassController@list');
    Route::post('/valuation-class', 'Master\ValuationClassController@store');
    Route::get('/valuation-class/{id}', 'Master\ValuationClassController@show');
    Route::put('/valuation-class/{id}', 'Master\ValuationClassController@update');
    Route::delete('/valuation-class/{id}', 'Master\ValuationClassController@delete');
    Route::post('/multiple-delete-valuation-class', 'Master\ValuationClassController@multipleDelete');

    // WorkCenter
    Route::get('/work-center', 'Master\WorkCenterController@index');
    Route::get('/work-center-list', 'Master\WorkCenterController@list');
    Route::post('/work-center', 'Master\WorkCenterController@store');
    Route::get('/work-center/{id}', 'Master\WorkCenterController@show');
    Route::put('/work-center/{id}', 'Master\WorkCenterController@update');
    Route::delete('/work-center/{id}', 'Master\WorkCenterController@delete');
    Route::post('/multiple-delete-work-center', 'Master\WorkCenterController@multipleDelete');

    Route::get('/material-work-center', 'Master\WorkCenterController@materialBom');
    Route::post('/work-center/{id}/assign-material', 'Master\WorkCenterController@assignMaterial');
    Route::post('/work-center/{id}/delete-material', 'Master\WorkCenterController@deleteMaterial');

    // TransportType
    Route::get('/transport-type', 'Master\TransportTypeController@index');
    Route::get('/transport-type-list', 'Master\TransportTypeController@list');
    Route::post('/transport-type', 'Master\TransportTypeController@store');
    Route::get('/transport-type/{id}', 'Master\TransportTypeController@show');
    Route::put('/transport-type/{id}', 'Master\TransportTypeController@update');
    Route::delete('/transport-type/{id}', 'Master\TransportTypeController@delete');
    Route::post('/multiple-delete-transport-type', 'Master\TransportTypeController@multipleDelete');

    // Incoterm
    Route::get('/incoterm', 'Master\IncotermController@index');
    Route::get('/incoterm-list', 'Master\IncotermController@list');
    Route::post('/incoterm', 'Master\IncotermController@store');
    Route::get('/incoterm/{id}', 'Master\IncotermController@show');
    Route::put('/incoterm/{id}', 'Master\IncotermController@update');
    Route::delete('/incoterm/{id}', 'Master\IncotermController@delete');
    Route::post('/multiple-delete-incoterm', 'Master\IncotermController@multipleDelete');

    // Term of Payment
    Route::get('/term-of-payment', 'Master\TermPaymentController@index');
    Route::get('/term-of-payment-list', 'Master\TermPaymentController@list');
    Route::post('/term-of-payment', 'Master\TermPaymentController@store');
    Route::get('/term-of-payment/{id}', 'Master\TermPaymentController@show');
    Route::put('/term-of-payment/{id}', 'Master\TermPaymentController@update');
    Route::delete('/term-of-payment/{id}', 'Master\TermPaymentController@delete');
    Route::post('/multiple-delete-term-of-payment', 'Master\TermPaymentController@multipleDelete');

    // Material Vedor
    Route::get('/material-vendor', 'Master\MaterialVendorController@index');
    Route::post('/material-vendor', 'Master\MaterialVendorController@store');
    Route::get('/material-vendor/{id}', 'Master\MaterialVendorController@show');
    Route::get('/material-vendor-history/{id}', 'Master\MaterialVendorController@showHistory');
    Route::put('/material-vendor/{id}', 'Master\MaterialVendorController@update');
    Route::delete('/material-vendor/{id}', 'Master\MaterialVendorController@delete');
    Route::post('/multiple-delete-material-vendor', 'Master\MaterialVendorController@multipleDelete');

    // Chart Of Account
    Route::get('/chart-of-account', 'Master\ChartOfAccountController@chartOfAccount');
    Route::get('/chart-of-account-download', 'Master\ChartOfAccountController@download');
    Route::get('/chart-of-account-list', 'Master\ChartOfAccountController@chartOfAccountList');
    Route::post('/chart-of-account', 'Master\ChartOfAccountController@storeChartOfAccount');
    Route::get('/chart-of-account/{id}', 'Master\ChartOfAccountController@showChartOfAccount');
    Route::put('/chart-of-account/{id}', 'Master\ChartOfAccountController@updateChartOfAccount');
    Route::delete('/chart-of-account/{id}', 'Master\ChartOfAccountController@deleteChartOfAccount');
    Route::post('/multiple-delete-chart-of-account', 'Master\ChartOfAccountController@multipleDeleteChartOfAccount');

    // Chart Of Account Upload
    Route::get('/download-template-chart-of-account', 'Master\ChartOfAccountController@downloadTemplate');
    Route::post('/chart-of-account-upload', 'Master\ChartOfAccountController@upload');
    Route::get('/chart-of-account-invalid-data', 'Master\ChartOfAccountController@getInvalidUpload');
    Route::get('/chart-of-account-invalid-download', 'Master\ChartOfAccountController@downloadInvalidUpload');
    Route::delete('/chart-of-account-invalid', 'Master\ChartOfAccountController@deleteInvalidUpload');

    // Account Group
    Route::get('/group-type', 'Master\ChartOfAccountController@groupTypeList');
    Route::get('/account-type', 'Master\ChartOfAccountController@accountTypeList');
    Route::get('/gl-account-type-vendor', 'Master\ChartOfAccountController@glAccountTypeVendorList');
    Route::get('/chart-of-account/{id}/account-group', 'Master\ChartOfAccountController@accountGroupByCoa');
    Route::post('/chart-of-account/{id}/add-account-group', 'Master\ChartOfAccountController@storeAccountGroup');
    Route::get('/account-group/{id}', 'Master\ChartOfAccountController@showAccountGroup');
    Route::put('/account-group/{id}', 'Master\ChartOfAccountController@updateAccountGroup');
    Route::delete('/account-group/{id}', 'Master\ChartOfAccountController@deleteAccountGroup');
    Route::post('/multiple-delete-account-group', 'Master\ChartOfAccountController@multipleDeleteAccountGroup');

    // Account
    Route::get('/account', 'Master\ChartOfAccountController@accountList');
    Route::get('/account-group/{id}/account', 'Master\ChartOfAccountController@accountByGroup');
    Route::post('/account-group/{id}/add-account', 'Master\ChartOfAccountController@storeAccount');
    Route::get('/account/{id}', 'Master\ChartOfAccountController@showAccount');
    Route::put('/account/{id}', 'Master\ChartOfAccountController@updateAccount');
    Route::delete('/account/{id}', 'Master\ChartOfAccountController@deleteAccount');
    Route::post('/multiple-delete-account', 'Master\ChartOfAccountController@multipleDeleteAccount');

    // Material Type
    Route::get('/material-type', 'Master\MaterialTypeController@index');
    Route::get('/material-type-list', 'Master\MaterialTypeController@list');
    Route::post('/material-type', 'Master\MaterialTypeController@store');
    Route::get('/material-type/{id}', 'Master\MaterialTypeController@show');
    Route::put('/material-type/{id}', 'Master\MaterialTypeController@update');
    Route::delete('/material-type/{id}', 'Master\MaterialTypeController@delete');
    Route::post('/multiple-delete-material-type', 'Master\MaterialTypeController@multipleDelete');

    // New Material (Basic Only Type, Code, Description)
    Route::get('/material-code-by-material-type/{id}', 'Master\MaterialTypeController@getCodeByMaterialType');
    Route::get('/classification-for-material', 'Material\MaterialController@getClassificationForMaterial');
    Route::get('/material', 'Material\MaterialController@material');
    Route::post('/material', 'Material\MaterialController@storeMaterial');
    Route::get('/material/{id}', 'Material\MaterialController@showMaterial');
    Route::get('/material-detail/{code}', 'Material\MaterialController@showMaterialByCode');
    Route::put('/material/{id}', 'Material\MaterialController@updateMaterial');
    Route::delete('/material/{id}', 'Material\MaterialController@deleteMaterial');
    Route::post('/multiple-delete-material', 'Material\MaterialController@multipleDeleteMaterial');

    // Material Basic Upload
    Route::get('/download-template-material-basic', 'Material\MaterialBasicController@template');
    Route::post('/material-basic-upload', 'Material\MaterialBasicController@upload');
    Route::get('/material-basic-invalid-data', 'Material\MaterialBasicController@getInvalidUpload');
    Route::get('/material-basic-invalid-download', 'Material\MaterialBasicController@downloadInvalidUpload');
    Route::delete('/material-basic-invalid', 'Material\MaterialBasicController@deleteInvalidUpload');

    // Material Basic
    Route::post('/material/{id}/basic', 'Material\MaterialBasicController@storeMaterialBasic');
    // Material Classification
    Route::post('/material/{id}/classification', 'Material\MaterialBasicController@storeMaterialClassification');

    // Material Uom Conversion
    Route::get('/material/{id}/uom-list', 'Material\MaterialBasicController@materialUomList');
    Route::post('/material/{id}/add-uom', 'Material\MaterialBasicController@storeMaterialUom');
    Route::get('/material-uom/{id}', 'Material\MaterialBasicController@showMaterialUom');
    Route::put('/material-uom/{id}', 'Material\MaterialBasicController@updateMaterialUom');
    Route::delete('/material-uom/{id}', 'Material\MaterialBasicController@deleteMaterialUom');

    // Material Image
    Route::get('/material/{id}/image-list', 'Material\MaterialBasicController@materialImageList');
    Route::post('/material/{id}/add-image', 'Material\MaterialBasicController@storeMaterialImage');
    Route::get('/material-image/{id}', 'Material\MaterialBasicController@showMaterialImage');
    Route::delete('/material-image/{id}', 'Material\MaterialBasicController@deleteMaterialImage');

    // Material Mrp
    Route::get('/material-plant', 'Material\MaterialPlantController@materialPlant');
    Route::post('/multiple-delete-material-plant', 'Material\MaterialPlantController@multipleDeleteMaterialPlant');
    Route::get('/classification-for-quality-profile', 'Material\MaterialController@getClassificationForQualityProfile');
    Route::get('/material/{id}/material-mrp-list', 'Material\MaterialPlantController@materialMrpList');
    Route::post('/material/{id}/add-material-mrp', 'Material\MaterialPlantController@storeMaterialMrp');
    Route::get('/material-mrp/{id}', 'Material\MaterialPlantController@showMaterialMrp');
    Route::delete('/material-mrp/{id}', 'Material\MaterialPlantController@deleteMaterialMrp');

    // Material plant Upload
    Route::get('/download-template-material-plant', 'Material\MaterialPlantController@template');
    Route::post('/material-plant-upload', 'Material\MaterialPlantController@upload');
    Route::get('/material-plant-invalid-data', 'Material\MaterialPlantController@getInvalidUpload');
    Route::get('/material-plant-invalid-download', 'Material\MaterialPlantController@downloadInvalidUpload');
    Route::delete('/material-plant-invalid', 'Material\MaterialPlantController@deleteInvalidUpload');

    // Material Purchasing
    Route::get('/material/{id}/material-purchasing-list', 'Material\MaterialPurchasingController@materialPurchasingList');
    Route::post('/material/{id}/add-material-purchasing', 'Material\MaterialPurchasingController@storeMaterialPurchasing');
    Route::get('/material-purchasing/{id}', 'Material\MaterialPurchasingController@showMaterialPurchasing');
    Route::delete('/material-purchasing/{id}', 'Material\MaterialPurchasingController@deleteMaterialPurchasing');

    // Material Storage
    Route::get('/material-storage', 'Material\MaterialStorageController@materialStorage');
    Route::post('/multiple-delete-material-storage', 'Material\MaterialStorageController@multipleDeleteMaterialStorage');
    Route::get('/material/{id}/material-storage-list', 'Material\MaterialStorageController@materialStorageList');
    Route::post('/material/{id}/add-material-storage', 'Material\MaterialStorageController@storeMaterialStorage');
    Route::get('/material-storage/{id}', 'Material\MaterialStorageController@showMaterialStorage');
    Route::put('/material-storage/{id}', 'Material\MaterialStorageController@updateMaterialStorage');
    Route::delete('/material-storage/{id}', 'Material\MaterialStorageController@deleteMaterialStorage');

    // Material Storage Upload
    Route::get('/download-template-material-storage', 'Material\MaterialStorageController@template');
    Route::post('/material-storage-upload', 'Material\MaterialStorageController@upload');
    Route::get('/material-storage-invalid-data', 'Material\MaterialStorageController@getInvalidUpload');
    Route::get('/material-storage-invalid-download', 'Material\MaterialStorageController@downloadInvalidUpload');
    Route::delete('/material-storage-invalid', 'Material\MaterialStorageController@deleteInvalidUpload');

    // Material Account
    Route::get('/material-account', 'Material\MaterialAccountController@materialAccount');
    Route::post('/multiple-delete-material-account', 'Material\MaterialAccountController@multipleDeleteMaterialAccount');
    Route::get('/material/{id}/material-account-list', 'Material\MaterialAccountController@materialAccountList');
    Route::post('/material/{id}/add-material-account', 'Material\MaterialAccountController@storeMaterialAccount');
    Route::get('/material-account/{id}', 'Material\MaterialAccountController@showMaterialAccount');
    Route::put('/material-account/{id}', 'Material\MaterialAccountController@updateMaterialAccount');
    Route::delete('/material-account/{id}', 'Material\MaterialAccountController@deleteMaterialAccount');

    // Material account Upload
    Route::get('/download-template-material-account', 'Material\MaterialAccountController@template');
    Route::post('/material-account-upload', 'Material\MaterialAccountController@upload');
    Route::get('/material-account-invalid-data', 'Material\MaterialAccountController@getInvalidUpload');
    Route::get('/material-account-invalid-download', 'Material\MaterialAccountController@downloadInvalidUpload');
    Route::delete('/material-account-invalid', 'Material\MaterialAccountController@deleteInvalidUpload');

    // Material Sales
    Route::get('/material-sales', 'Material\MaterialSalesController@materialSales');
    Route::post('/multiple-delete-material-sales', 'Material\MaterialSalesController@multipleDeleteMaterialSales');
    Route::get('/material/{id}/material-sales-list', 'Material\MaterialSalesController@materialSalesList');
    Route::post('/material/{id}/add-material-sales', 'Material\MaterialSalesController@storeMaterialSales');
    Route::get('/material-sales/{id}', 'Material\MaterialSalesController@showMaterialSales');
    Route::put('/material-sales/{id}', 'Material\MaterialSalesController@updateMaterialSales');
    Route::delete('/material-sales/{id}', 'Material\MaterialSalesController@deleteMaterialSales');

    // Material Sales Upload
    Route::get('/download-template-material-sales', 'Material\MaterialSalesController@template');
    Route::post('/material-sales-upload', 'Material\MaterialSalesController@upload');
    Route::get('/material-sales-invalid-data', 'Material\MaterialSalesController@getInvalidUpload');
    Route::get('/material-sales-invalid-download', 'Material\MaterialSalesController@downloadInvalidUpload');
    Route::delete('/material-sales-invalid', 'Material\MaterialSalesController@deleteInvalidUpload');

    // Asset Valuation Group
    Route::get('/valuation-group', 'Master\ValuationGroupController@index');
    Route::post('/valuation-group', 'Master\ValuationGroupController@store');
    Route::get('/valuation-group/{id}', 'Master\ValuationGroupController@show');
    Route::put('/valuation-group/{id}', 'Master\ValuationGroupController@update');
    Route::delete('/valuation-group/{id}', 'Master\ValuationGroupController@delete');
    Route::post('/multiple-delete-valuation-group', 'Master\ValuationGroupController@multipleDelete');

    // Asset Type
    Route::get('/asset-type', 'Master\AssetTypeController@index');
    Route::get('/asset-type-list', 'Master\AssetTypeController@list');
    Route::post('/asset-type', 'Master\AssetTypeController@store');
    Route::get('/asset-type/{id}', 'Master\AssetTypeController@show');
    Route::put('/asset-type/{id}', 'Master\AssetTypeController@update');
    Route::delete('/asset-type/{id}', 'Master\AssetTypeController@delete');
    Route::post('/multiple-delete-asset-type', 'Master\AssetTypeController@multipleDelete');

    // Asset Status
    Route::get('/asset-status', 'Master\AssetStatusController@index');
    Route::get('/asset-status-normal', 'Master\AssetStatusController@listNormal');
    Route::get('/asset-status-list', 'Master\AssetStatusController@list');
    Route::post('/asset-status', 'Master\AssetStatusController@store');
    Route::get('/asset-status/{id}', 'Master\AssetStatusController@show');
    Route::put('/asset-status/{id}', 'Master\AssetStatusController@update');
    Route::delete('/asset-status/{id}', 'Master\AssetStatusController@delete');
    Route::post('/multiple-delete-asset-status', 'Master\AssetStatusController@multipleDelete');

    // Asset Retired Reason
    Route::get('/retired-reason', 'Master\RetiredReasonController@index');
    Route::get('/retired-reason-list', 'Master\RetiredReasonController@list');
    Route::post('/retired-reason', 'Master\RetiredReasonController@store');
    Route::get('/retired-reason/{id}', 'Master\RetiredReasonController@show');
    Route::put('/retired-reason/{id}', 'Master\RetiredReasonController@update');
    Route::delete('/retired-reason/{id}', 'Master\RetiredReasonController@delete');
    Route::post('/multiple-delete-retired-reason', 'Master\RetiredReasonController@multipleDelete');

    // Asset
    Route::get('/asset', 'AssetController@asset');
    Route::get('/asset-list', 'AssetController@list');
    Route::get('/asset-report', 'AssetController@report');
    Route::post('/asset', 'AssetController@storeAsset');
    Route::get('/asset/{id}', 'AssetController@showAsset');
    Route::get('/asset-straightline/{id}', 'AssetController@showAssetStraightLine');
    Route::get('/asset-doubledecline/{id}', 'AssetController@showAssetDoubleDecline');
    Route::put('/asset/{id}', 'AssetController@updateAsset');
    Route::delete('/asset/{id}', 'AssetController@deleteAsset');
    Route::post('/multiple-delete-asset', 'AssetController@multipleDelete');
    Route::get('/asset-barcode', 'AssetController@qrcode_pdf');
    Route::get('/asset-old-number', 'AssetController@assetoldnumber');
    Route::get('/asset-export', 'AssetController@exportAsset');
    Route::get('/asset-number', 'AssetController@assetnumber');

    // Asset Depresiasi and Test Cron Job
    Route::get('/cron-asset-depresiasi', 'AssetController@cronDepresiasi');
    Route::get('/show-asset-depresiasi-sl/{id}', 'AssetController@showAssetDepresiasiSL');
    Route::get('/show-asset-depresiasi-dd/{id}', 'AssetController@showAssetDepresiasiDD');
    Route::get('/show-all-asset-depreciation', 'AssetController@showAllDepreciation');
    Route::delete('/delete-asset-depresiasi/{id}', 'AssetController@deleteAssetDepresiasi');

    // Asset Upload
    Route::get('/asset-template', 'AssetController@template');
    Route::post('/asset-upload', 'AssetController@upload');
    Route::get('/asset-invalid-data', 'AssetController@getInvalidUpload');
    Route::get('/asset-invalid-download', 'AssetController@downloadInvalidUpload');
    Route::delete('/asset-invalid', 'AssetController@deleteInvalidUpload');

    // Asset history, Asset move location & move responsible person
    Route::get('/asset-responsible-person-history-pagination/{id}', 'AssetController@responsiblePersonHistoryPagination');
    Route::get('/asset-location-history-pagination/{id}', 'AssetController@locationHistoryPagination');
    Route::get('/asset-responsible-person-history-list', 'AssetController@responsiblePersonHistoryList');
    Route::get('/asset-responsible-person-history/{id}', 'AssetController@responsiblePersonHistory');
    Route::get('/asset-location-history/{id}', 'AssetController@locationHistory');
    Route::post('/asset-update-empty-responsible', 'AssetController@updateEmptyResponsiblePerson');
    Route::put('/asset/{id}/move-responsible-person', 'AssetController@updateResponsiblePersonHistory');
    Route::put('/asset/{id}/move-location', 'AssetController@updateLocationHistory');

    // Asset classification parameter
    Route::get('/classification-parameters-by-material/{id}', 'AssetController@classificationParametersByMaterial');
    Route::post('/asset/{id}/store-classification-parameters', 'AssetController@storeAssetClassificationParameters');

    // Asset Image
    Route::get('/asset/{id}/image-list', 'AssetController@assetImageList');
    Route::post('/asset/{id}/add-image', 'AssetController@storeAssetImage');
    Route::get('/asset-image/{id}', 'AssetController@showAssetImage');
    Route::post('/asset-image-update/{id}', 'AssetController@updateAssetImage');
    Route::delete('/asset-image/{id}', 'AssetController@deleteAssetImage');

    // Asset Image 360
    Route::get('/asset/{id}/image-360-list', 'AssetController@assetImage360List');
    Route::post('/asset/{id}/add-image-360', 'AssetController@storeAssetImage360');
    Route::get('/asset-image-360/{id}', 'AssetController@showAssetImage360');
    Route::post('/asset-image-360-update/{id}', 'AssetController@updateAssetImage360');
    Route::delete('/asset-image-360/{id}', 'AssetController@deleteAssetImage360');

    // Asset Area
    Route::get('/asset/{id}/area-list', 'AssetController@assetAreaList');
    Route::post('/asset/{id}/add-area', 'AssetController@storeAssetArea');
    Route::get('/asset-area/{id}', 'AssetController@showAssetArea');
    Route::put('/asset-area/{id}', 'AssetController@updateAssetArea');
    Route::delete('/asset-area/{id}', 'AssetController@deleteAssetArea');

    // Asset approval and status change
    Route::post('/asset-request-for-retired/{id}', 'AssetController@assetRequestForRetired');
    Route::post('/asset-status-approval/{id}/{status}', 'AssetController@approvalAssetRetired');
    Route::post('/asset-request-for-activation', 'AssetController@assetRequestForActivation');
    Route::post('/asset-status-activation/{status}', 'AssetController@approvalAssetActivation');

    // Stock Determination
    Route::get('/stock-determination', 'Threepl\StockDeterminationController@index');
    Route::get('/stock-determination-list', 'Threepl\StockDeterminationController@list');
    Route::post('/stock-determination', 'Threepl\StockDeterminationController@store');
    Route::get('/stock-determination/{id}', 'Threepl\StockDeterminationController@show');
    Route::put('/stock-determination/{id}', 'Threepl\StockDeterminationController@update');
    Route::delete('/stock-determination/{id}', 'Threepl\StockDeterminationController@delete');
    Route::post('/multiple-delete-stock-determination', 'Threepl\StockDeterminationController@multipleDelete');

    // Stock Determination Upload
    Route::get('/download-template-stock-determination', 'Threepl\StockDeterminationController@template');
    Route::post('/stock-determination-upload', 'Threepl\StockDeterminationController@upload');
    Route::get('/stock-determination-invalid-data', 'Threepl\StockDeterminationController@getInvalidUpload');
    Route::get('/stock-determination-invalid-download', 'Threepl\StockDeterminationController@downloadInvalidUpload');
    Route::delete('/stock-determination-invalid', 'Threepl\StockDeterminationController@deleteInvalidUpload');

    // Settings
    Route::get('/setting', 'Master\SettingController@index');
    Route::get('/setting/{key}', 'Master\SettingController@show');
    Route::post('/setting/{key}', 'Master\SettingController@update');

    // Bom
    Route::get('/bill-of-material', 'Master\BomController@index');
    Route::post('/bill-of-material', 'Master\BomController@store');
    Route::get('/bill-of-material/{id}', 'Master\BomController@show');
    Route::put('/bill-of-material/{id}', 'Master\BomController@update');
    Route::delete('/bill-of-material/{id}', 'Master\BomController@delete');
    Route::post('/multiple-delete-bill-of-material', 'Master\BomController@multipleDelete');

    // Bom Detail
    Route::post('/bill-of-material/{id}/detail', 'Master\BomController@storeDetail');
    Route::put('/bill-of-material/{id}/detail', 'Master\BomController@updateDetail');
    Route::delete('/bill-of-material/{id}/detail', 'Master\BomController@deleteDetail');
    Route::post('/multiple-delete-bill-of-material-detail', 'Master\BomController@multipleDeleteDetail');

    // Release Strategy
    // Release Object
    Route::get('/release-object', 'Master\ReleaseStrategy\ReleaseObjectController@index');
    Route::get('/release-object-list', 'Master\ReleaseStrategy\ReleaseObjectController@list');
    Route::post('/release-object', 'Master\ReleaseStrategy\ReleaseObjectController@store');
    Route::get('/release-object/{id}', 'Master\ReleaseStrategy\ReleaseObjectController@show');
    Route::put('/release-object/{id}', 'Master\ReleaseStrategy\ReleaseObjectController@update');
    Route::delete('/release-object/{id}', 'Master\ReleaseStrategy\ReleaseObjectController@delete');
    Route::post('/multiple-delete-release-object', 'Master\ReleaseStrategy\ReleaseObjectController@multipleDelete');

    // Release Group
    Route::get('/classification-for-workflow', 'Material\MaterialController@getClassificationForWorkflow');
    Route::get('/release-group', 'Master\ReleaseStrategy\ReleaseGroupController@index');
    Route::get('/release-group-list', 'Master\ReleaseStrategy\ReleaseGroupController@list');
    Route::post('/release-group', 'Master\ReleaseStrategy\ReleaseGroupController@store');
    Route::get('/release-group/{id}', 'Master\ReleaseStrategy\ReleaseGroupController@show');
    Route::put('/release-group/{id}', 'Master\ReleaseStrategy\ReleaseGroupController@update');
    Route::delete('/release-group/{id}', 'Master\ReleaseStrategy\ReleaseGroupController@delete');
    Route::post('/multiple-delete-release-group', 'Master\ReleaseStrategy\ReleaseGroupController@multipleDelete');

    // Release Code
    Route::get('/release-code', 'Master\ReleaseStrategy\ReleaseCodeController@index');
    Route::get('/release-code-list', 'Master\ReleaseStrategy\ReleaseCodeController@list');
    Route::post('/release-code', 'Master\ReleaseStrategy\ReleaseCodeController@store');
    Route::get('/release-code/{id}', 'Master\ReleaseStrategy\ReleaseCodeController@show');
    Route::put('/release-code/{id}', 'Master\ReleaseStrategy\ReleaseCodeController@update');
    Route::delete('/release-code/{id}', 'Master\ReleaseStrategy\ReleaseCodeController@delete');
    Route::post('/multiple-delete-release-code', 'Master\ReleaseStrategy\ReleaseCodeController@multipleDelete');

    // Release Code User
    Route::get('/release-code-user', 'Master\ReleaseStrategy\ReleaseCodeUserController@index');
    Route::get('/release-code-user-list', 'Master\ReleaseStrategy\ReleaseCodeUserController@list');
    Route::post('/release-code-user', 'Master\ReleaseStrategy\ReleaseCodeUserController@store');
    Route::get('/release-code-user/{id}', 'Master\ReleaseStrategy\ReleaseCodeUserController@show');
    Route::put('/release-code-user/{id}', 'Master\ReleaseStrategy\ReleaseCodeUserController@update');
    Route::delete('/release-code-user/{id}', 'Master\ReleaseStrategy\ReleaseCodeUserController@delete');
    Route::post('/multiple-delete-release-code-user', 'Master\ReleaseStrategy\ReleaseCodeUserController@multipleDelete');

    // Release strategy
    Route::get('/release-strategy', 'Master\ReleaseStrategy\ReleaseStrategyController@index');
    Route::get('/release-strategy-list', 'Master\ReleaseStrategy\ReleaseStrategyController@list');
    Route::post('/release-strategy', 'Master\ReleaseStrategy\ReleaseStrategyController@store');
    Route::get('/release-strategy/{id}', 'Master\ReleaseStrategy\ReleaseStrategyController@show');
    Route::get('/release-strategy-activate/{id}', 'Master\ReleaseStrategy\ReleaseStrategyController@activate');
    Route::get('/release-strategy-deactivate/{id}', 'Master\ReleaseStrategy\ReleaseStrategyController@deactivate');
    Route::put('/release-strategy/{id}', 'Master\ReleaseStrategy\ReleaseStrategyController@update');
    Route::delete('/release-strategy/{id}', 'Master\ReleaseStrategy\ReleaseStrategyController@delete');
    Route::post('/multiple-delete-release-strategy', 'Master\ReleaseStrategy\ReleaseStrategyController@multipleDelete');

    // release strategy assign release code
    Route::post('/release-strategy/{id}/assign-code', 'Master\ReleaseStrategy\ReleaseStrategyController@assignCode');
    Route::post('/release-strategy/{id}/add-release-code', 'Master\ReleaseStrategy\ReleaseStrategyController@addCode');
    Route::post('/release-strategy/{id}/delete-release-code', 'Master\ReleaseStrategy\ReleaseStrategyController@deleteCode');

    // release strategy classification
    Route::post('/release-strategy/{id}/classification', 'Master\ReleaseStrategy\ReleaseStrategyController@storeClassification');

    // release status
    Route::get('/release-strategy/{id}/status', 'Master\ReleaseStrategy\ReleaseStrategyController@status');
    Route::post('/release-strategy/{id}/status', 'Master\ReleaseStrategy\ReleaseStrategyController@updateStatus');

    // Project
    Route::get('/project', 'Master\ProjectController@index');
    Route::get('/project-list', 'Master\ProjectController@list');
    Route::post('/project', 'Master\ProjectController@store');
    Route::get('/project/{id}', 'Master\ProjectController@show');
    Route::put('/project/{id}', 'Master\ProjectController@update');
    Route::delete('/project/{id}', 'Master\ProjectController@delete');
    Route::post('/multiple-delete-project', 'Master\ProjectController@multipleDelete');

    // Budget
    Route::get('/budget', 'Master\BudgetController@index');
    Route::get('/budget-list', 'Master\BudgetController@list');
    Route::post('/budget', 'Master\BudgetController@store');
    Route::get('/budget/{id}', 'Master\BudgetController@show');
    Route::get('/budget-log/{id}', 'Master\BudgetController@log');
    Route::put('/budget/{id}', 'Master\BudgetController@update');
    Route::delete('/budget/{id}', 'Master\BudgetController@delete');

    // Budget Approval
    Route::post('/budget-submit/{id}', 'Master\BudgetController@submit');
    Route::post('/budget-approve/{id}', 'Master\BudgetController@approve');

    // Budget Upload
    Route::get('/budget-template', 'Master\BudgetController@template');
    Route::post('/budget-upload', 'Master\BudgetController@upload');
    Route::get('/budget-valid-data', 'Master\BudgetController@getValidData');
    Route::post('/budget-valid-upload', 'Master\BudgetController@uploadValidData');

    // Depreciation Group
    Route::get('/depreciation-group', 'Master\DepreciationGroupController@index');
    Route::get('/depreciation-group-list', 'Master\DepreciationGroupController@list');
    Route::post('/depreciation-group', 'Master\DepreciationGroupController@store');
    Route::get('/depreciation-group/{id}', 'Master\DepreciationGroupController@show');
    Route::put('/depreciation-group/{id}', 'Master\DepreciationGroupController@update');
    Route::delete('/depreciation-group/{id}', 'Master\DepreciationGroupController@delete');
    Route::post('/multiple-delete-depreciation-group', 'Master\DepreciationGroupController@multipleDelete');

    // Depreciation Type (Depreciation Master)
    Route::get('/depreciation-type', 'Master\DepreciationTypeController@index');
    Route::get('/depreciation-type-list', 'Master\DepreciationTypeController@list');
    Route::post('/depreciation-type', 'Master\DepreciationTypeController@store');
    Route::get('/depreciation-type/{id}', 'Master\DepreciationTypeController@show');
    Route::put('/depreciation-type/{id}', 'Master\DepreciationTypeController@update');
    Route::delete('/depreciation-type/{id}', 'Master\DepreciationTypeController@delete');
    Route::post('/multiple-delete-depreciation-type', 'Master\DepreciationTypeController@multipleDelete');
    Route::get('/depreciation-select/{type}', 'Master\DepreciationTypeController@selectlist');

    // Depreciation Parameter
    Route::post('/depreciation-type/{id}/addparam', 'Master\DepreciationTypeController@periodStoreParam');
    Route::get('/depreciation-period/{id}', 'Master\DepreciationTypeController@showPeriodParameter');
    Route::put('/depreciation-period/{id}', 'Master\DepreciationTypeController@updatePeriodParameter');
    Route::delete('/depreciation-period/{id}', 'Master\DepreciationTypeController@deletePeriodParameter');
    Route::post('/multiple-delete-depreciation-period', 'Master\DepreciationTypeController@multipleDeletePeriodParam');

    // Valuation Asset
    Route::get('/valuation-asset', 'Master\ValuationAssetController@index');
    Route::get('/valuation-asset-list', 'Master\ValuationAssetController@list');
    Route::post('/valuation-asset', 'Master\ValuationAssetController@store');
    Route::get('/valuation-asset/{id}', 'Master\ValuationAssetController@show');
    Route::put('/valuation-asset/{id}', 'Master\ValuationAssetController@update');
    Route::delete('/valuation-asset/{id}', 'Master\ValuationAssetController@delete');
    Route::post('/multiple-delete-valuation-asset', 'Master\ValuationAssetController@multipleDelete');

    // Vendor Group
    Route::get('/vendor-group', 'Master\VendorGroupController@index');
    Route::get('/vendor-group-list', 'Master\VendorGroupController@list');
    Route::post('/vendor-group', 'Master\VendorGroupController@store');
    Route::get('/vendor-group/{id}', 'Master\VendorGroupController@show');
    Route::put('/vendor-group/{id}', 'Master\VendorGroupController@update');
    Route::delete('/vendor-group/{id}', 'Master\VendorGroupController@delete');
    Route::post('/multiple-delete-vendor-group', 'Master\VendorGroupController@multipleDelete');
});

// 3pl
Route::group(['middleware' => ['auth:api', 'activity'], 'prefix' => 'threepl', 'namespace' => 'Api'], function () {
    // Stock Overview
    Route::get('/stock-overview', 'Threepl\StockController@index');
    Route::get('/stock-serial/{id}', 'Threepl\StockController@serialStockMain');
    Route::get('/stock-serial-batch/{id}', 'Threepl\StockController@serialStockBatch');
    Route::get('/stock-serial-by-storage-and-material', 'Threepl\StockController@serialStockByStorageMaterial');

    // StockReplenishment
    Route::post('/stock-replenishment', 'Threepl\StockReplenishmentController@replenishment');

    // Document Code
    Route::get('/document-code', 'Threepl\ReferenceListController@documentCode');

    // get material where have material plant only for reservation & goods movements
    Route::get('/material', 'Threepl\ReferenceListController@materialGm');
    Route::get('/material-reservation', 'Threepl\ReferenceListController@materialReservation');
    Route::get('/material-catalog', 'Threepl\ReferenceListController@materialCatalog');
    Route::get('/material-sku', 'Threepl\ReferenceListController@materialSku');
    Route::get('/material-stock', 'Threepl\StockController@materialStock');
    Route::get('/material-by-code/{code}', 'Threepl\ReferenceListController@materialbyCode');

    // Cost Center list
    Route::get('/cost-center', 'Threepl\ReferenceListController@costCenter');

    // get batch list
    Route::get('/batch-list', 'Threepl\GoodsMovementController@batchList');

    // get serial list
    Route::get('/serial-list', 'Threepl\GoodsMovementController@serialList');

    // get uom list for conversion by selected material
    Route::get('/uom-list-by-material/{id}', 'Threepl\ReferenceListController@uomList');

    // get storage & plant sender by stock determination
    Route::get('/plant-receiver-by-stock-determination', 'Threepl\StockDeterminationController@getReceiverPlantStockDetermination');
    Route::get('/storage-receiver-by-stock-determination', 'Threepl\StockDeterminationController@getReceiverStorageStockDetermination');

    // Reservation
    Route::group(['middleware' => 'auth:api', 'prefix' => 'reservation'], function () {
        Route::get('/get-code', 'ReservationController@reservationCode');
        Route::get('/get-pr-code', 'ReservationController@PRCode');
        Route::get('/', 'ReservationController@index');
        Route::get('/pr-list', 'ReservationController@prList');

        // get inout plant/storage from stock determination based from selected plant & storage
        Route::get('/get-supply-stock-determination', 'Threepl\StockDeterminationController@getStockDetermination');
        Route::get('/plant-by-stock-determination', 'Threepl\StockDeterminationController@getSenderPlantStockDetermination');
        Route::get('/storage-by-stock-determination', 'Threepl\StockDeterminationController@getSenderStorageStockDetermination');

        // get draft by login user
        Route::get('/get/draft', 'ReservationController@getDraftReservation');

        // Create Reservation
        Route::post('/', 'ReservationController@create');
        Route::post('/{reservation_code}/item-line', 'ReservationController@addItemLine');
        Route::post('/{reservation_code}/update-item-line', 'ReservationController@updateItemLine');
        Route::post('/account-assignment/{id}', 'ReservationController@reservationAccountAssignment');
        Route::post('/purchasing/{id}', 'ReservationController@reservationPurchasing');
        Route::post('/production/{id}', 'ReservationController@reservationProduction');

        // Rreservation Header
        Route::get('/{reservation_code}', 'ReservationController@show');
        Route::post('/{reservation_code}', 'ReservationController@updateReservationHeader');
        Route::delete('/{reservation_code}', 'ReservationController@delete');

        // Reservation Attachment
        Route::post('/{reservation_code}/add-attachment', 'ReservationController@addAttachment');
        Route::post('/delete-attachment/{reservation_code}', 'ReservationController@deleteAttachment');

        // Reservation item
        Route::get('/show-item/{id}', 'ReservationController@showReservationItem');
        Route::delete('/delete-item/{id}', 'ReservationController@deleteReservationItem');

        // Submit reservation header include reservation item
        Route::post('/submit/{reservation_code}', 'ReservationController@submit');
        Route::post('/cancel/{reservation_code}', 'ReservationController@cancel');

        // Approve or Reject Reservation
        Route::post('/{reservation_code}/approval', 'ReservationController@approval');

        // Convert Approved Reservation (Purchase Requisition) as Purchase Order For External Purchase Type
        Route::post('/convert/purchase-order', 'ReservationController@convertAsPo');

        // list vendor from info record by material id
        Route::get('/vendor-list/{material_id}', 'Threepl\ReferenceListController@vendorList');
    });

    // Purchase Requisition
    Route::group(['middleware' => 'auth:api', 'prefix' => 'purchase-requisition'], function () {
        Route::get('/get-pr-code', 'Threepl\PurchaseRequisitionController@PRCode');
        Route::get('/', 'Threepl\PurchaseRequisitionController@index');
        Route::get('/unit-of-procurement/{material_id}/{plant_id}', 'Threepl\ReferenceListController@uop');

        // Create Purchase Requisition
        Route::post('/', 'Threepl\PurchaseRequisitionController@create');
        Route::post('/{reservation_code}/item-line', 'Threepl\PurchaseRequisitionController@addItemLine');
        Route::post('/{reservation_code}/update-item-line', 'Threepl\PurchaseRequisitionController@updateItemLine');
        Route::post('/account-assignment/{id}', 'Threepl\PurchaseRequisitionController@reservationAccountAssignment');
        Route::post('/purchasing/{id}', 'Threepl\PurchaseRequisitionController@reservationPurchasing');
        Route::post('/production/{id}', 'Threepl\PurchaseRequisitionController@reservationProduction');

        // Purchase Requisition Header
        Route::get('/{reservation_code}', 'Threepl\PurchaseRequisitionController@show');
        Route::post('/{reservation_code}', 'Threepl\PurchaseRequisitionController@updateReservationHeader');
        Route::delete('/{reservation_code}', 'Threepl\PurchaseRequisitionController@delete');

        // Purchase Requisition Attachment
        Route::post('/{reservation_code}/add-attachment', 'Threepl\PurchaseRequisitionController@addAttachment');
        Route::post('/delete-attachment/{reservation_code}', 'Threepl\PurchaseRequisitionController@deleteAttachment');

        // Purchase Requisition item
        Route::get('/show-item/{id}', 'Threepl\PurchaseRequisitionController@showReservationItem');
        Route::delete('/delete-item/{id}', 'Threepl\PurchaseRequisitionController@deleteReservationItem');

        // Submit Purchase Requisition header include Purchase Requisition item
        Route::post('/submit/{reservation_code}', 'Threepl\PurchaseRequisitionController@submit');
        Route::post('/cancel/{reservation_code}', 'Threepl\PurchaseRequisitionController@cancel');

        // Approve or Reject Purchase Requisition
        Route::post('/{reservation_code}/approval', 'Threepl\PurchaseRequisitionController@approval');

        // Convert Approved Purchase Requisition (Purchase Requisition) as Purchase Order For External Purchase Type
        Route::post('/convert/purchase-order', 'Threepl\PurchaseRequisitionController@convertAsPo');
    });

    // Delivery Orders
    Route::group(['middleware' => 'auth:api', 'prefix' => 'delivery-orders'], function () {
        // picking list
        Route::get('/picking-list', 'Threepl\PickingListController@index');
        Route::post('/reject-picking-list', 'Threepl\PickingListController@reject');

        // Convert Approved Reservation as Delivery Order For Internal Purchase Type
        Route::post('/convert-as-delivery-order', 'Threepl\PickingListController@convertAsDo');
        // Convert Approved Reservation as Purchase Requisition For External Purchase Type
        Route::post('/convert-as-purchase-requisition', 'Threepl\PickingListController@convertAsPr');
        // Edit DO allocated QTY from picking list
        Route::post('/edit-allocated-qty/{id}', 'Threepl\PickingListController@editAllocatedQty');
        // PDF picking
        Route::get('/{id}/pdf-picking', 'Threepl\PickingListController@pdf');
        // Auto allocate DO batch
        Route::get('/{id}/allocate-batch', 'Threepl\PickingListController@allocateBatch');

        // Delivery Orders
        Route::get('/', 'Threepl\DeliveryOrderController@index');
        Route::get('/{id}', 'Threepl\DeliveryOrderController@show');
        Route::get('/{delivery_code}/code', 'Threepl\DeliveryOrderController@showByCode');
        Route::put('/{id}', 'Threepl\DeliveryOrderController@editDO');
        Route::post('/send-do/{id}', 'Threepl\DeliveryOrderController@sendDO');
        // pdf DO
        Route::get('/{id}/pdf', 'Threepl\DeliveryOrderController@pdf');
    });

    // Purchase Orders
    Route::group(['middleware' => 'auth:api', 'prefix' => 'purchase-orders'], function () {
        Route::get('/', 'Threepl\PurchaseOrderController@index');
        Route::get('/{id}', 'Threepl\PurchaseOrderController@show');
        Route::put('/{id}/header', 'Threepl\PurchaseOrderController@updateHeader');
        Route::delete('/{id}/header', 'Threepl\PurchaseOrderController@deleteHeader');
        Route::put('/{id}/item', 'Threepl\PurchaseOrderController@updateItem');
        Route::get('/{id}/show-item', 'Threepl\PurchaseOrderController@showItem');
        Route::put('/{id}/account-assignment', 'Threepl\PurchaseOrderController@updateAccountAssignment');
        Route::get('/{po_code}/code', 'Threepl\PurchaseOrderController@showByCode');
        Route::post('/submit', 'Threepl\PurchaseOrderController@submit');
        Route::post('/approve', 'Threepl\PurchaseOrderController@approve');
        Route::post('/reject', 'Threepl\PurchaseOrderController@reject');
        Route::get('/{id}/pdf', 'Threepl\PurchaseOrderController@pdf');

        // complete po item
        Route::post('/{id}/complete-item', 'Threepl\PurchaseOrderController@completeItem');

        // PO Attachment
        Route::post('/{id}/add-attachment', 'Threepl\PurchaseOrderController@addAttachment');
        Route::post('/delete-attachment/{id}', 'Threepl\PurchaseOrderController@deleteAttachment');

        // PO Condition
        Route::post('/{id}/add-condition', 'Threepl\PurchaseOrderController@addCondition');
        Route::put('/{id}/edit-condition', 'Threepl\PurchaseOrderController@editCondition');
        Route::delete('/{id}/delete-condition', 'Threepl\PurchaseOrderController@deleteCondition');
        Route::get('/release-strategy/{amount}/{plant}/{mat_group}/{cost_center}', 'Threepl\PurchaseOrderController@cekReleaseStrategy');
    });

    // Purchase Status Report
    Route::group(['middleware' => 'auth:api', 'prefix' => 'purchase-status-report'], function () {
        Route::get('/', 'Threepl\PurchaseStatusController@index');
    });

    // Request for quotation
    Route::group(['middleware' => 'auth:api', 'prefix' => 'rfq'], function () {
        Route::get('/', 'Threepl\RfqController@index');
        Route::get('/{id}', 'Threepl\RfqController@show');
        Route::get('/{id}/send', 'Threepl\RfqController@send');
        Route::get('/{id}/print', 'Threepl\RfqController@pdf');
        Route::put('/{id}/header', 'Threepl\RfqController@updateHeader');
        Route::put('/{id}/item', 'Threepl\RfqController@updateItem');
        Route::get('/{id}/show-item', 'Threepl\RfqController@showItem');
        Route::put('/{id}/account-assignment', 'Threepl\RfqController@updateAccountAssignment');
        Route::post('/convert-from-pr', 'Threepl\RfqController@convertFromPR');
        Route::post('/{id}/copy', 'Threepl\RfqController@copy');
        Route::post('/submit', 'Threepl\RfqController@submit');

        // evaluate
        Route::get('/evaluate/{bid_number}', 'Threepl\RfqController@evaluate');
        Route::post('/award/{bid_number}', 'Threepl\RfqController@award');

        // convert awarded rfq to PO
        Route::get('/{id}/convert-to-purchase-order', 'Threepl\RfqController@convertToPo');
    });

    // Goods Movement
    Route::group(['middleware' => 'auth:api', 'prefix' => 'goods-movement'], function () {
        // get movement type for 201 & 561 only
        Route::get('/specific-movement-type', 'Threepl\ReferenceListController@getSpecificMovementType');
        Route::get('/reason-by-movement-type/{id}', 'Threepl\ReferenceListController@getMovementTypeReason');

        // get work center by supply storage
        Route::get('/work-center-by-storage/{id}', 'Threepl\ReferenceListController@workCenterByStorage');
        Route::get('/cost-center-by-work-center/{id}', 'Threepl\ReferenceListController@costCenterByWorkCenter');

        // Create Goods Movement
        Route::post('/header', 'Threepl\GoodsMovementController@createHeader');
        Route::post('/{id}/item-serial-batch', 'Threepl\GoodsMovementController@createItemSerialBatch');

        // Goods movement not complete
        Route::get('/incomplete-list', 'Threepl\GoodsMovementController@incompleteList');
        Route::get('/incomplete-production-confirmation', 'Threepl\GoodsMovementController@incompleteGRProduction');
        Route::get('/incomplete/{id}', 'Threepl\GoodsMovementController@incomplete');
    });

    // Reversal
    Route::group(['middleware' => 'auth:api', 'prefix' => 'reverse-movement'], function () {
        // Post Reverse
        Route::post('/{material_doc}', 'Threepl\ReversalController@reverse');
    });

    // History Movement Report
    Route::group(['middleware' => 'auth:api', 'prefix' => 'history-movement'], function () {
        Route::get('/', 'Threepl\GoodsMovementController@index');
        Route::get('/download-excel', 'Threepl\GoodsMovementController@excel');
        Route::get('/serial/{id}', 'Threepl\GoodsMovementController@movementSerial');
        Route::get('/show/{material_doc}', 'Threepl\GoodsMovementController@showByCode');
        Route::get('/pdf/{material_doc}', 'Threepl\GoodsMovementController@pdf');
    });

    // Monthly Consumption Report
    Route::group(['middleware' => 'auth:api', 'prefix' => 'monthly-consumption'], function () {
        Route::get('/', 'Threepl\MonthlyConsumptionController@index');
        Route::get('/download-excel', 'Threepl\MonthlyConsumptionController@excel');
        Route::get('/specific-movement-type', 'Threepl\MonthlyConsumptionController@getSpecificMovementType');
    });

    // Moving Price History
    Route::group(['middleware' => 'auth:api', 'prefix' => 'moving-price-history'], function () {
        Route::get('/', 'Threepl\MovingPriceHistoryController@index');
    });

    // Stock Opname
    Route::group(['middleware' => 'auth:api', 'prefix' => 'stock-opname'], function () {
        Route::get('/', 'Threepl\StockOpnameController@index');
        Route::get('/detail/{stock_opname_doc}', 'Threepl\StockOpnameController@detail');
        Route::get('/draft', 'Threepl\StockOpnameController@draft');
        Route::get('/list', 'Threepl\StockOpnameController@list');
        Route::get('/{id}', 'Threepl\StockOpnameController@show');
        Route::post('/save', 'Threepl\StockOpnameController@create');
        Route::post('/submit/{stock_opname_doc}', 'Threepl\StockOpnameController@submit');
        Route::post('/approve/{stock_opname_doc}', 'Threepl\StockOpnameController@approve');
        Route::post('/update/{id}', 'Threepl\StockOpnameController@update');
        Route::post('/delete', 'Threepl\StockOpnameController@delete');
    });

    // production order
    Route::group(['middleware' => 'auth:api', 'prefix' => 'production-order'], function () {
        Route::get('/template', 'Threepl\ProductionOrderController@template');
        Route::get('/download-invalid', 'Threepl\ProductionOrderController@download_invalid');
        Route::post('/upload', 'Threepl\ProductionOrderController@upload');
        Route::post('/upload-old', 'Threepl\ProductionOrderController@uploadOld');
        Route::post('/upload-valid-data', 'Threepl\ProductionOrderController@uploadValidData');
        Route::get('/valid-data', 'Threepl\ProductionOrderController@getValidData');

        Route::get('/reservation', 'Threepl\ProductionOrderController@reservation');

        Route::get('/schedule', 'Threepl\ProductionOrderController@schedule');
        Route::post('/confirm-schedule', 'Threepl\ProductionOrderController@confirm');

        Route::get('/', 'Threepl\ProductionOrderController@index');
        Route::get('/resource-requirement', 'Threepl\ProductionOrderController@resourceRequirement');
        Route::get('/{order_number}/code', 'Threepl\ProductionOrderController@showByCode');
    });

});

// Work Order
Route::group(['middleware' => ['auth:api', 'activity'], 'prefix' => 'work-order', 'namespace' => 'Api'], function () {
    //Task List
    Route::resource('/task-list', 'WorkOrder\TrxTaskListController');
    Route::get('/task-list-hash', 'WorkOrder\TrxTaskListController@list');
    Route::post('/multiple-delete-task-list', 'WorkOrder\TrxTaskListController@multipleDelete');

    // Task List Item
    Route::get('/task-list-item', 'WorkOrder\TrxTaskListItemController@index');
    Route::post('/task-list/{id}/store-item', 'WorkOrder\TrxTaskListItemController@store');
    Route::get('/task-list-item/{id}', 'WorkOrder\TrxTaskListItemController@show');
    Route::put('/task-list/{id}/update-item', 'WorkOrder\TrxTaskListItemController@update');
    Route::delete('/task-list-item/{id}', 'WorkOrder\TrxTaskListItemController@destroy');
    Route::get('/task-list-item-by-tasklist-id/{task_id}', 'WorkOrder\TrxTaskListItemController@taskListById'); //versi bukan hash
    Route::get('/task-list-item-hash-by-tasklist-id/{task_id}', 'WorkOrder\TrxTaskListItemController@taskListHashById'); //versi hash
    Route::post('/multiple-delete-task-list-item', 'WorkOrder\TrxTaskListItemController@multipleDelete');

    //Service Request
    Route::resource('/service-request', 'WorkOrder\ServiceRequestController');
    Route::get('/service-request-hash', 'WorkOrder\ServiceRequestController@list');
    Route::post('/multiple-delete-service-request', 'WorkOrder\ServiceRequestController@multipleDelete');

    //Work Order
    Route::get('/wo-status', 'WorkOrder\CorrectiveMaintenanceController@woStatus');
    Route::get('/wo-level', 'WorkOrder\CorrectiveMaintenanceController@woLevel');
    Route::resource('/corrective-maintenance', 'WorkOrder\CorrectiveMaintenanceController');
    Route::get('/corrective-maintenance-hash', 'WorkOrder\CorrectiveMaintenanceController@list');
    Route::post('/multiple-delete-corrective-maintenance', 'WorkOrder\CorrectiveMaintenanceController@multipleDelete');

    //WO Task
    Route::post('/add-task-manual/{wo_id}', 'WorkOrder\CorrectiveMaintenanceController@addTaskManual');
    Route::post('/import-task/{wo_id}', 'WorkOrder\CorrectiveMaintenanceController@importTask');
    Route::get('/wo-task/{wo_id}', 'WorkOrder\CorrectiveMaintenanceController@getWOTask');
    Route::get('/wo-task-hash/{wo_id}', 'WorkOrder\CorrectiveMaintenanceController@getWOTaskHash');
    Route::get('/show-wo-task/{id}', 'WorkOrder\CorrectiveMaintenanceController@showWoTask');
    Route::put('/wo-task/{id}', 'WorkOrder\CorrectiveMaintenanceController@updateWoTask');
    Route::delete('/wo-task/{id}', 'WorkOrder\CorrectiveMaintenanceController@destroyWoTask');
    Route::post('/multiple-delete-wo-task', 'WorkOrder\CorrectiveMaintenanceController@multipleDeleteWoTask');

});

// Accounting
Route::group(['middleware' => ['auth:api', 'activity'], 'prefix' => 'accounting', 'namespace' => 'Api'], function () {
    // Document Type
    Route::get('/document-type', 'Accounting\IncomingInvoiceController@documentType');

    // Trasaction Type
    Route::get('/transaction-type', 'Accounting\AccountingProcedureController@transactionType');

    // Sub Type
    Route::get('/sub-type', 'Accounting\AccountingProcedureController@subType');

    // IncomingInvoice
    Route::group(['middleware' => 'auth:api', 'prefix' => 'incoming-invoice'], function () {
        Route::get('/', 'Accounting\IncomingInvoiceController@index');
        Route::get('/{id}', 'Accounting\IncomingInvoiceController@show');
        Route::get('/po-gr-by-vendor/{vendor_id}', 'Accounting\IncomingInvoiceController@getPoGrByVendor');
        Route::get('/po-detail/{po_number}', 'Accounting\IncomingInvoiceController@poDetail');
        Route::post('/save', 'Accounting\IncomingInvoiceController@store');
    });

    // Accounting Procedure
    Route::resource('/accounting-procedure', 'Accounting\AccountingProcedureController');
    Route::get('/accounting-procedure-hash', 'Accounting\AccountingProcedureController@list');
    Route::post('/multiple-delete-accounting-procedure', 'Accounting\AccountingProcedureController@multipleDelete');
});
