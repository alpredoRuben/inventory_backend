<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Models\DepreTypeName;
use App\Models\DepreTypeGroup;
use App\Models\DepreTypePeriod;
use App\Helpers\HashId;
use Auth;
use Session;
use Validator;
use DB;

class DepreciationGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->cekRoleModules(['depre-group-view']);

        $depre_group = (new DepreTypeGroup)->newQuery();

        $depre_group->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $depre_group->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $depre_group->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $depre_group->orderBy('name', 'asc');
        }

        // $depre_group = $depre_group->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
        //     ->appends(Input::except('page'));

        if (request()->has('per_page')) {
            return $depre_group->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $depre_group->paginate(20)->appends(Input::except('page'));
        }

        return $depre_group;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['depre-group-view']);

        $depre_group = (new DepreTypeGroup)->newQuery();

        $depre_group->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $depre_group->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $depre_group->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $depre_group->orderBy('name', 'asc');
        }

        $depre_group = $depre_group->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($depre_group['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $depre_group['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $depre_group;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['depre-group-create']);

        $this->validate(request(), [
            'name'              => 'required',
            'is_straight_line'  => 'required'
        ]);

        $depre_group = DepreTypeGroup::whereRaw('LOWER(name) = ?', strtolower($request->name))->first();

        if ($depre_group) {

            if($depre_group->deleted){
                $save = $depre_group->update([
                    'name'              => $request->name,
                    'is_straight_line'  => $request->is_straight_line,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                return $depre_group;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'name' => ['name already taken']
                ]
            ],422);
        } else {
            $save = DepreTypeGroup::create([
                'name'          => $request->name,
                'is_straight_line'    => $request->is_straight_line,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['depre-group-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return DepreTypeGroup::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['depre-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $depre_group = DepreTypeGroup::findOrFail($id);

        $this->validate(request(), [
            'name'              => 'required|unique:depre_type_group,name,'. $id .'',
            'is_straight_line'  => 'required'
        ]);

        $save = $depre_group->update([
            'name' => $request->name,
            'is_straight_line' => $request->is_straight_line,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $depre_group;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['depre-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = DepreTypeGroup::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['depre-group-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:depre_type_group,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = DepreTypeGroup::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
