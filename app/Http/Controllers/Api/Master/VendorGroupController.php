<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\VendorGroup;
use App\Helpers\HashId;

class VendorGroupController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['vendor-group-view']);

        $vendor = (new VendorGroup)->newQuery();

        $vendor->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $vendor->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $vendor->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $vendor->orderBy('code', 'asc');
        }

        $vendor = $vendor->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $vendor;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['vendor-group-view']);

        $vendor = (new VendorGroup)->newQuery();

        $vendor->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $vendor->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $vendor->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $vendor->orderBy('code', 'asc');
        }

        $vendor = $vendor->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($vendor['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $vendor['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $vendor;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['vendor-group-create']);

        $this->validate(request(), [
            'code' => 'required|max:4',
            'description' => 'required|max:30',
        ]);

        $vendor = VendorGroup::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($vendor) {

            if($vendor->deleted){
                $save = $vendor->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $vendor;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = VendorGroup::create([
                'code' => $request->code,
                'description' => $request->description,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['vendor-group-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return VendorGroup::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['vendor-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $vendor = VendorGroup::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:4|unique:vendor_groups,code,'. $id .'',
            'description' => 'required|max:30',
        ]);

        $save = $vendor->update([
            'code' => $request->code,
            'description' => $request->description,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $vendor;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['vendor-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = VendorGroup::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['vendor-group-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:vendor_groups,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = VendorGroup::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
