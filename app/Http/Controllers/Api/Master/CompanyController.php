<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\Company;
use App\Models\ActivityLog;
use App\Helpers\HashId;

class CompanyController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['company-view']);

        $company = (new Company)->newQuery();

        $company->where('deleted', false)->with(['location', 'createdBy', 'updatedBy']);
        $company->with('location.location_type');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $company->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $company->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $company->orderBy('code', 'asc');
        }

        $company = $company->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $company;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['company-view']);

        $company = (new Company)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $company->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(companies.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(companies.description)"), 'LIKE', "%".$q."%");
            });

            // search location
            $company->where('companies.deleted', false)->orWhereHas('location', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            })
            ->with(['location' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            }]);

            // search chart_of_account
            $company->where('companies.deleted', false)->orWhereHas('chart_of_account', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(chart_of_accounts.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(chart_of_accounts.description)"), 'LIKE', "%".$q."%");
            })
            ->with(['chart_of_account' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(chart_of_accounts.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(chart_of_accounts.description)"), 'LIKE', "%".$q."%");
            }]);
        }

        $company->where('companies.deleted', false);
        $company->with(['location', 'createdBy', 'updatedBy']);
        $company->with('chart_of_account');
        $company->with('location.location_type');

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'location':
                    $company->join('locations','locations.id','=','companies.location_id');
                    $company->select('companies.code','companies.*');
                    $company->orderBy('locations.name',$sort_order);
                break;

                case 'chart_of_account':
                    $company->leftJoin('chart_of_accounts','chart_of_accounts.id','=','companies.chart_of_account_id');
                    $company->select('companies.code','companies.*');
                    $company->orderBy('chart_of_accounts.code',$sort_order);
                break;
                
                default:
                    $company->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $company->orderBy('code', 'asc');
        }

        $company = $company->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($company['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $company['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $company;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['company-create']);

        $this->validate(request(), [
            'code' => 'required|max:10',
            'description' => 'required|max:30',
            'location_id' => 'required|exists:locations,id',
            'chart_of_account_id' => 'nullable|exists:chart_of_accounts,id'
        ]);

        $company = Company::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($company) {

            if($company->deleted){
                $save = $company->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'location_id' => $request->location_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id,
                    'chart_of_account_id' => $request->chart_of_account_id
                ]);

                return $company;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = Company::create([
                'code' => $request->code,
                'description' => $request->description,
                'location_id' => $request->location_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'chart_of_account_id' => $request->chart_of_account_id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['company-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Company::with(['location', 'createdBy', 'updatedBy', 'chart_of_account'])
            ->with('location.location_type')
            ->findOrFail($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['company-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'Company')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['company-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $company = Company::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:10|unique:companies,code,'. $id .'',
            'description' => 'required|max:30',
            'location_id' => 'required|exists:locations,id',
            'chart_of_account_id' => 'nullable|exists:chart_of_accounts,id'
        ]);

        $save = $company->update([
            'code' => $request->code,
            'description' => $request->description,
            'location_id' => $request->location_id,
            'updated_by' => Auth::user()->id,
            'chart_of_account_id' => $request->chart_of_account_id
        ]);

        if ($save) {
            return $company;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['company-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Company::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['company-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:companies,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Company::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
