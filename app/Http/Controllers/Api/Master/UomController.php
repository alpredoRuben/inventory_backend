<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\Uom;
use App\Helpers\HashId;

class UomController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['uom-view']);
 
        $uom = (new Uom)->newQuery();

        $uom->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $uom->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $uom->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $uom->orderBy('code', 'asc');
        }

        $uom = $uom->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $uom;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['uom-view']);
 
        $uom = (new Uom)->newQuery();

        $uom->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $uom->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $uom->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $uom->orderBy('code', 'asc');
        }

        $uom = $uom->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($uom['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $uom['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $uom;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['uom-create']);

        $this->validate(request(), [
            'code'              => 'required|max:3',
            'description'       => 'required|max:30',
            'decimal'           => 'nullable|integer|min:0|max:4'
        ]);

        $uom = Uom::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($uom) {

            if($uom->deleted){
                $save = $uom->update([
                    'code'          => $request->code,
                    'description'   => $request->description,
                    'decimal'       => $request->decimal ? $request->decimal : 0,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $uom;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = Uom::create([
                'code'          => $request->code,
                'description'   => $request->description,
                'decimal'       => $request->decimal ? $request->decimal : 0,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['uom-view']);

        return Uom::with(['createdBy', 'updatedBy'])->find($id);
    }

    public function showHash($id)
    {
        Auth::user()->cekRoleModules(['uom-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Uom::with(['createdBy', 'updatedBy'])->find($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['uom-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new \App\Models\ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'UOM')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['uom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $uom = Uom::findOrFail($id);

        $this->validate(request(), [
            'code'              => 'required|max:3|unique:uoms,code,'. $id .'',
            'description'       => 'required|max:30',
            'decimal'           => 'nullable|integer|min:0|max:4'
        ]);

        $save = $uom->update([
            'code'              => $request->code,
            'description'       => $request->description,
            'decimal'           => $request->decimal ? $request->decimal : 0,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $uom;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['uom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Uom::find($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['uom-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:uoms,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Uom::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
