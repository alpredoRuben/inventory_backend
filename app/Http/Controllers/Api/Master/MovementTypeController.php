<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\MovementType;
use App\Models\MovementTypeReason;
use App\Helpers\HashId;

class MovementTypeController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['movement-type-view']);
  
        $movementType = (new MovementType)->newQuery();

        $movementType->where('deleted', false)->with(['rev_code', 'createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $movementType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('dc_indicator')) {
            $movementType->whereIn('dc_ind', request()->input('dc_indicator'));
        }

        if (request()->has('batch_indicator')) {
            $movementType->whereIn('batch_ind', request()->input('batch_indicator'));
        }

        if (request()->has('qi_indicator')) {
            $movementType->whereIn('qi_ind', request()->input('qi_indicator'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $movementType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $movementType->orderBy('code', 'asc');
        }

        // if have organization parameter
        $movement_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($movement_id) > 0) {
            $movementType->whereIn('id', $movement_id);
        }

        $movementType = $movementType->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $movementType;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['movement-type-view']);
  
        $movementType = (new MovementType)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $movementType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });

            // search reversal
            $movementType->where('deleted', false)->orWhereHas('rev_code', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })
            ->with(['rev_code' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            }]);

            // search sub_type
            $movementType->where('deleted', false)->orWhereHas('sub_type', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['sub_type' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search credit / debit
            $credit = 'credit';
            $debit = 'debit';

            if ($q) {
                if (strpos($credit,$q) !== false) {
                    $c = 'c';
                    $movementType->orWhere(DB::raw("LOWER(dc_ind)"), 'LIKE', "%".$c."%");
                }

                if (strpos($debit,$q) !== false) {
                    $d = 'd';
                    $movementType->orWhere(DB::raw("LOWER(dc_ind)"), 'LIKE', "%".$d."%");
                }
            }
        }

        $movementType->where('deleted', false);
        $movementType->with(['rev_code', 'createdBy', 'updatedBy', 'sub_type']);

        if (request()->has('dc_indicator')) {
            $movementType->whereIn('dc_ind', request()->input('dc_indicator'));
        }

        if (request()->has('batch_indicator')) {
            $movementType->whereIn('batch_ind', request()->input('batch_indicator'));
        }

        if (request()->has('qi_indicator')) {
            $movementType->whereIn('qi_ind', request()->input('qi_indicator'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'rev_code':
                    $movementType->orderBy('rev_code_id', $sort_order);
                break;

                case 'sub_type':
                    $movementType->leftJoin('sub_types','sub_types.id','=','movement_types.sub_type_id');
                    $movementType->select('movement_types.*');
                    $movementType->orderBy('sub_types.code',$sort_order);
                break;
                
                default:
                    $movementType->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $movementType->orderBy('code', 'asc');
        }

        // if have organization parameter
        $movement_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($movement_id) > 0) {
            $movementType->whereIn('id', $movement_id);
        }

        $movementType = $movementType->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($movementType['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $movementType['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $movementType;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['movement-type-create']);

        $this->validate(request(), [
            // 'code'              => 'required|max:3',
            'description'       => 'required|max:30',
            'reason_ind'        => 'nullable|boolean',
            // 'dc_ind'            => 'required',
            // 'rev_code_id'       => 'nullable|exists:movement_types,id',
            // 'batch_ind'         => 'nullable|boolean',
            // 'qi_ind'            => 'nullable|boolean'
        ]);

        $movementType = MovementType::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($movementType) {

            if($movementType->deleted){
                $save = $movementType->update([
                    // 'code'              => $request->code,
                    'description'       => $request->description,
                    'reason_ind'        => $request->reason_ind ? $request->reason_ind : 0,
                    // 'dc_ind'            => $request->dc_ind,
                    // 'rev_code_id'       => $request->rev_code_id,
                    // 'batch_ind'         => $request->batch_ind ? $request->batch_ind : 0,
                    // 'qi_ind'            => $request->qi_ind  ? $request->qi_ind : 0,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                return $movementType;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = MovementType::create([
                // 'code'              => $request->code,
                'description'       => $request->description,
                'reason_ind'        => $request->reason_ind ? $request->reason_ind : 0,
                // 'dc_ind'            => $request->dc_ind,
                // 'rev_code_id'       => $request->rev_code_id,
                // 'batch_ind'         => $request->batch_ind ? $request->batch_ind : 0,
                // 'qi_ind'            => $request->qi_ind  ? $request->qi_ind : 0,
                'created_by'        => Auth::user()->id,
                'updated_by'        => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['movement-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $data = MovementType::with(['rev_code', 'createdBy', 'updatedBy', 'sub_type'])->find($id);
        $reason = MovementTypeReason::where('movement_type_id', $id)->get()->toArray();

        foreach($reason as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $reason[$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        $data->reasons = $reason;

        return $data;
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['movement-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new \App\Models\ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'MovementType')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['movement-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $movementType = MovementType::findOrFail($id);

        $this->validate(request(), [
            // 'code'              => 'required|max:3|unique:movement_types,code,'. $id .'',
            'description'       => 'required|max:30',
            'reason_ind'        => 'nullable|boolean',
            // 'dc_ind'            => 'required',
            // 'rev_code_id'       => 'nullable|exists:movement_types,id',
            // 'batch_ind'         => 'nullable|boolean',
            // 'qi_ind'            => 'nullable|boolean'
        ]);

        if ($request->rev_code_id == $id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'rev_code_id' => ['Rev Code cant same with code']
                ]
            ], 422);
        }

        $save = $movementType->update([
            // 'code'              => $request->code,
            'description'       => $request->description,
            'reason_ind'        => $request->reason_ind ? $request->reason_ind : 0,
            // 'dc_ind'            => $request->dc_ind,
            // 'rev_code_id'       => $request->rev_code_id,
            // 'batch_ind'         => $request->batch_ind ? $request->batch_ind : 0,
            // 'qi_ind'            => $request->qi_ind  ? $request->qi_ind : 0,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $movementType;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['movement-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = MovementType::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['movement-type-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:movement_types,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = MovementType::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function movementTypeReasonList($id)
    {
        Auth::user()->cekRoleModules(['movement-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $reason = MovementTypeReason::where('deleted', false)->where('movement_type_id', $id)
                ->orderBy('id', 'desc')
                ->get()
                ->toArray();

        foreach($reason as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $reason[$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $reason;
    }

    public function storeMovementTypeReason(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['movement-type-create']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'reason'            => 'required|max:15'
        ]);

        $save = MovementTypeReason::create([
            'movement_type_id'  => $id,
            'reason'            => $request->reason,
            'created_by'        => Auth::user()->id,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            $save->movement_type_id = HashId::encode($save->movement_type_id);

            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 400);
        }
    }

    public function showMovementTypeReason($id)
    {
        Auth::user()->cekRoleModules(['movement-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return MovementTypeReason::findOrFail($id);
    }

    public function updateMovementTypeReason($id, Request $request)
    {
        Auth::user()->cekRoleModules(['movement-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $movementTypeReason = MovementTypeReason::findOrFail($id);

        $this->validate(request(), [
            'reason'            => 'required|max:15'
        ]);

        $save = $movementTypeReason->update([
            'reason'            => $request->reason,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            $data = $movementTypeReason;
            $data->movement_type_id = HashId::encode($data->movement_type_id);

            return $data;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function deleteMovementTypeReason($id)
    {
        Auth::user()->cekRoleModules(['movement-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = MovementTypeReason::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }
}
