<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\GroupMaterial;
use App\Helpers\HashId;

class GroupMaterialController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['group-material-view']);

        $groupMat = (new GroupMaterial)->newQuery();

        $groupMat->where('deleted', false)->with(['account', 'createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $groupMat->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(material_group)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $groupMat->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $groupMat->orderBy('material_group', 'asc');
        }

        $groupMat = $groupMat->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $groupMat;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['group-material-view']);

        $groupMat = (new GroupMaterial)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $groupMat->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(material_group)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });

            // search account
            $groupMat->where('deleted', false)->orWhereHas('account', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['account' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);
        }

        $groupMat->where('deleted', false);
        $groupMat->with(['account', 'createdBy', 'updatedBy']);
        $groupMat->with('account.account_group');

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $groupMat->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $groupMat->orderBy('material_group', 'asc');
        }

        $groupMat = $groupMat->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($groupMat['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $groupMat['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $groupMat;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['group-material-create']);

        $this->validate(request(), [
            'material_group' => 'required|max:9',
            'description' => 'required|max:30',
            'account_id' => 'nullable|exists:accounts,id'
        ]);

        $groupMaterial = GroupMaterial::whereRaw('LOWER(material_group) = ?', strtolower($request->material_group))->first();

        if ($groupMaterial) {

            if($groupMaterial->deleted){
                $save = $groupMaterial->update([
                    'material_group' => $request->material_group,
                    'description'   => $request->description,
                    'account_id'   => $request->account_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $groupMaterial;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = GroupMaterial::create([
                'material_group' => $request->material_group,
                'description'   => $request->description,
                'account_id'   => $request->account_id,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['group-material-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return GroupMaterial::with(['account', 'createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['group-material-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new \App\Models\ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'GroupMaterial')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['group-material-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $groupMaterial = GroupMaterial::findOrFail($id);

        $this->validate(request(), [
            'material_group' => 'required|max:9|unique:group_materials,material_group,'. $id .'',
            'description' => 'required|max:30',
            'account_id' => 'nullable|exists:accounts,id',
        ]);

        $save = $groupMaterial->update([
            'material_group' => $request->material_group,
            'description' => $request->description,
            'account_id' => $request->account_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $groupMaterial;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['group-material-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = GroupMaterial::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['group-material-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:group_materials,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = GroupMaterial::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
