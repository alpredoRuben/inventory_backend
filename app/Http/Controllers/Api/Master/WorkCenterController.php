<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\WorkCenter;
use App\Models\Material;
use App\Models\MaterialStorage;
use App\Models\Bom;
use App\Helpers\HashId;

class WorkCenterController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['work-center-view']);

        $work = (new WorkCenter)->newQuery();

        $work->where('deleted', false)->with([
            'cost_center', 'storage_supply', 'storage_result', 'responsible_user', 'plant',
            'createdBy', 'updatedBy'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $work->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $work->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $work->orderBy('code', 'asc');
        }

        $work = $work->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $work;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['work-center-view']);

        $work = (new WorkCenter)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $work->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });            

            // search cost_center
            $work->where('work_centers.deleted', false)->orWhereHas('cost_center', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })
            ->with(['cost_center' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            }]);

            // search plant
            $work->where('work_centers.deleted', false)->orWhereHas('plant', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })
            ->with(['plant' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            }]);

            // search storage_supply
            $work->where('work_centers.deleted', false)->orWhereHas('storage_supply', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })
            ->with(['storage_supply' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            }]);

            // search storage_result
            $work->where('work_centers.deleted', false)->orWhereHas('storage_result', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })
            ->with(['storage_result' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            }]);

            // search responsible
            $work->where('work_centers.deleted', false)->orWhereHas('responsible_user', function ($query) use ($q) {
                $query->where(DB::raw("LOWER( CONCAT_WS(' ', firstname, lastname) )"), 'LIKE', "%".$q."%");
            })
            ->with(['responsible_user' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER( CONCAT_WS(' ', firstname, lastname) )"), 'LIKE', "%".$q."%");
            }]);
        }

        $work->where('work_centers.deleted', false);
        $work->with([
            'cost_center', 'storage_supply', 'storage_result', 'responsible_user', 'plant',
            'createdBy', 'updatedBy'
        ]);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'cost_center':
                    $work->join('cost_centers','cost_centers.id','=','work_centers.cost_center_id');
                    $work->select('work_centers.*');
                    $work->orderBy('work_centers.code',$sort_order);
                break;

                case 'plant':
                    $work->join('plants','plants.id','=','work_centers.plant_id');
                    $work->select('work_centers.*');
                    $work->orderBy('plants.code',$sort_order);
                break;

                case 'storage_supply':
                    $work->join('storages','storages.id','=','work_centers.storage_supply_id');
                    $work->select('work_centers.*');
                    $work->orderBy('storages.code',$sort_order);
                break;

                case 'storage_result':
                    $work->join('storages','storages.id','=','work_centers.storage_supply_id');
                    $work->select('work_centers.*');
                    $work->orderBy('work_centers.storage_result_id',$sort_order);
                break;

                case 'responsible_user':
                    $work->leftJoin('users','users.id','=','work_centers.responsible_user_id');
                    $work->select('work_centers.*');
                    $work->orderBy('users.firstname',$sort_order);
                break;

                default:
                    $work->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $work->orderBy('code', 'asc');
        }

        $work = $work->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($work['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $work['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $work;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['work-center-create']);

        $this->validate(request(), [
            'code' => 'required|max:8',
            'description' => 'required|max:30',
            'cost_center_id' => 'required|exists:cost_centers,id',
            'plant_id' => 'required|exists:plants,id',
            'storage_supply_id' => 'required|exists:storages,id',
            'storage_result_id' => 'required|exists:storages,id',
            'responsible_user_id' => 'nullable|exists:users,id',
        ]);

        $work = WorkCenter::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($work) {

            if($work->deleted){
                $save = $work->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'cost_center_id' => $request->cost_center_id,
                    'storage_supply_id' => $request->storage_supply_id,
                    'storage_result_id' => $request->storage_result_id,
                    'responsible_user_id' => $request->responsible_user_id,
                    'plant_id' => $request->plant_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                $response = WorkCenter::with('materials')->findOrFail($work->id);
                $response->id_hash = HashId::encode($work->id);

                return $response;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = WorkCenter::create([
                'code' => $request->code,
                'description' => $request->description,
                'cost_center_id' => $request->cost_center_id,
                'storage_supply_id' => $request->storage_supply_id,
                'storage_result_id' => $request->storage_result_id,
                'responsible_user_id' => $request->responsible_user_id,
                'plant_id' => $request->plant_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $response = WorkCenter::with('materials')->findOrFail($save->id);
            $response->id_hash = HashId::encode($save->id);

            return $response;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['work-center-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return WorkCenter::with([
            'cost_center', 'storage_supply', 'storage_result', 'responsible_user', 'plant',
            'createdBy', 'updatedBy', 'materials'
        ])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['work-center-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $work = WorkCenter::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:8|unique:work_centers,code,'. $id .'',
            'description' => 'required|max:30',
            'cost_center_id' => 'required|exists:cost_centers,id',
            'storage_supply_id' => 'required|exists:storages,id',
            'storage_result_id' => 'required|exists:storages,id',
            'responsible_user_id' => 'nullable|exists:users,id',
            'plant_id' => 'required|exists:plants,id',
        ]);

        $save = $work->update([
            'code' => $request->code,
            'description' => $request->description,
            'cost_center_id' => $request->cost_center_id,
            'storage_supply_id' => $request->storage_supply_id,
            'storage_result_id' => $request->storage_result_id,
            'responsible_user_id' => $request->responsible_user_id,
            'plant_id' => $request->plant_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return WorkCenter::with('materials')->findOrFail($id);
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['work-center-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = WorkCenter::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['work-center-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:work_centers,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = WorkCenter::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function materialBom(Request $request)
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material_storage = MaterialStorage::where('storage_id', $request->storage_id)
            ->pluck('material_id');

        $bom = Bom::whereIn('material_id', $material_storage)
            ->pluck('material_id');

        $material = (new Material)->newQuery();

        $material->where('deleted', false);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        $material->whereIn('id', $bom);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $material->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $material->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $material->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $material->paginate(20)->appends(Input::except('page'));
        }
    }

    public function assignMaterial(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['work-center-update']);

        $this->validate(request(), [
            'material_id'     => 'nullable|array',
            'material_id.*'   => 'nullable|exists:materials,id',
        ]);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $work = WorkCenter::with('materials')->findOrFail($id);

        if (in_array(null, $request->material_id, true) || in_array('', $request->material_id, true)) {
            $work->materials()->syncWithoutDetaching();
        } else {
            $work->materials()->syncWithoutDetaching($request->material_id);
        }

        return WorkCenter::with('materials')->findOrFail($id);
    }

    public function deleteMaterial($id)
    {
        Auth::user()->cekRoleModules(['work-center-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'material_id'     => 'nullable|array',
            'material_id.*'   => 'nullable|exists:materials,id',
        ]);

        $work = WorkCenter::findOrFail($id);

        $work->materials()->detach(request()->material_id);

        return response()->json([
            'message' => 'success delete material from work center'
        ], 200);
    }
}
