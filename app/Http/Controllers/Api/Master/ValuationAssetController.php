<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Models\ValuationAsset;
use App\Helpers\HashId;
use Auth;
use Session;
use Validator;
use DB;

class ValuationAssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->cekRoleModules(['valuation-asset-view']);

        $valuation_asset = (new ValuationAsset)->newQuery();

        $valuation_asset->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $valuation_asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $valuation_asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $valuation_asset->orderBy('name', 'asc');
        }

        $valuation_asset = $valuation_asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $valuation_asset;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['valuation-asset-view']);

        $valuation_asset = (new ValuationAsset)->newQuery();

        $valuation_asset->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $valuation_asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $valuation_asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $valuation_asset->orderBy('name', 'asc');
        }

        $valuation_asset = $valuation_asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($valuation_asset['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $valuation_asset['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $valuation_asset;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['valuation-asset-create']);

        $this->validate(request(), [
            'name'              => 'required|unique:valuation_asset,name,NULL,NULL,deleted,false',
            // 'is_depreciation'   => 'required'
        ]);

        $valuation_asset = ValuationAsset::whereRaw('LOWER(name) = ?', strtolower($request->name))->first();

        $is_depreciation = 0;

        if(!empty($request->is_depreciation)){
            $is_depreciation = $request->is_depreciation;
        }

        if ($valuation_asset) {

            if($valuation_asset->deleted){
                $save = $valuation_asset->update([
                    'name'              => $request->name,
                    'is_depreciation'   => $is_depreciation,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                return $valuation_asset;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'name' => ['name already taken']
                ]
            ],422);
        } else {
            $save = ValuationAsset::create([
                'name'              => $request->name,
                'is_depreciation'   => $is_depreciation,
                'created_by'        => Auth::user()->id,
                'updated_by'        => Auth::user()->id
            ]);

            return $save;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['valuation-asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return ValuationAsset::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['valuation-asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $valuation_asset = ValuationAsset::findOrFail($id);

        $this->validate(request(), [
            'name'              => 'required|unique:valuation_asset,name,'. $id .'',
            // 'is_depreciation'  => 'required'
        ]);
        
        $is_depreciation = 0;

        if(!empty($request->is_depreciation)){
            $is_depreciation = $request->is_depreciation;
        }

        $save = $valuation_asset->update([
            'name' => $request->name,
            'is_depreciation' => $is_depreciation,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $valuation_asset;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['valuation-asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = ValuationAsset::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['valuation-asset-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:valuation_asset,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = ValuationAsset::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
