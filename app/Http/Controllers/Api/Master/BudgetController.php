<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\BudgetExport;
use App\Imports\BudgetImport;

use App\Jobs\BudgetJob;

use App\Helpers\HashId;
use App\Models\Budget;
use App\Models\BudgetConsumption;
use App\Models\BudgetTemporary;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Material;
use App\Models\Project;
use App\Models\Account;
use App\Models\User;
use App\Models\ActivityLog;
use App\Models\BudgetApproval;
use App\Models\TransactionCode;

class BudgetController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['budget-view']);

        $budget = (new Budget)->newQuery();

        $budget->where('deleted', false)->with([
            'company', 'project', 'cost_center', 'account', 'material', 'responsible', 'consumption', 'approval', 'createdBy', 'updatedBy'
        ]);

        $budget->with('account.account_group');

        // filter type
        if (request()->has('fiscal_year')) {
            $budget->where(DB::raw("LOWER(fiscal_year)"), 'LIKE', "%".strtolower(request()->input('fiscal_year'))."%");
        }

        // filter company
        if (request()->has('company')) {
            $budget->whereIn('company_id', request()->input('company'));
        }

        // filter project
        if (request()->has('project')) {
            $budget->whereIn('project_id', request()->input('project'));
        }

        // filter cost_center
        if (request()->has('cost_center')) {
            $budget->whereIn('cost_center_id', request()->input('cost_center'));
        }

        // filter account
        if (request()->has('account')) {
            $budget->whereIn('account_id', request()->input('account'));
        }

        // filter material
        if (request()->has('material')) {
            $budget->whereIn('material_id', request()->input('material'));
        }

        // filter description
        if (request()->has('description')) {
            $budget->where(DB::raw("LOWER(description)"), 'LIKE', "%".strtolower(request()->input('description'))."%");
        }

        /** Status
        *0.Draft
        *1.Submitted
        *2.Active
        *3.Completed
        *4.Hold
        *5.Cancelled
        **/
        // filter status
        if (request()->has('status')) {
            $budget->whereIn('status', request()->input('status'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $budget->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $budget->orderBy('id', 'desc');
        }

        $budget = $budget->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        return $budget;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['budget-view']);

        $budget = (new Budget)->newQuery();

        $budget->where('budgets.deleted', false)->with([
            'company', 'project', 'cost_center', 'account', 'material', 'responsible', 'consumption', 'approval', 'createdBy', 'updatedBy'
        ]);

        $budget->with('approval.user');
        $budget->with('account.account_group');

        // filter type
        if (request()->has('fiscal_year')) {
            $budget->where(DB::raw("LOWER(fiscal_year)"), 'LIKE', "%".strtolower(request()->input('fiscal_year'))."%");
        }

        // filter company
        if (request()->has('company')) {
            $budget->whereIn('company_id', request()->input('company'));
        }

        // filter project
        if (request()->has('project')) {
            $budget->whereIn('project_id', request()->input('project'));
        }

        // filter cost_center
        if (request()->has('cost_center')) {
            $budget->whereIn('cost_center_id', request()->input('cost_center'));
        }

        // filter account
        if (request()->has('account')) {
            $budget->whereIn('account_id', request()->input('account'));
        }

        // filter material
        if (request()->has('material')) {
            $budget->whereIn('material_id', request()->input('material'));
        }

        // filter description
        if (request()->has('description')) {
            $budget->where(DB::raw("LOWER(description)"), 'LIKE', "%".strtolower(request()->input('description'))."%");
        }

        /** Status
        *0.Draft
        *1.Submitted
        *2.Active
        *3.Completed
        *4.Hold
        *5.Cancelled
        **/
        // filter status
        if (request()->has('status')) {
            $budget->whereIn('status', request()->input('status'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'company':
                    $budget->leftJoin('companies','budgets.company_id','=','companies.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('companies.code',$sort_order);
                break;

                case 'project':
                    $budget->leftJoin('projects','budgets.project_id','=','projects.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('projects.code',$sort_order);
                break;

                case 'project_name':
                    $budget->leftJoin('projects','budgets.project_id','=','projects.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('projects.description',$sort_order);
                break;

                case 'cost_center':
                    $budget->leftJoin('cost_centers','budgets.cost_center_id','=','cost_centers.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('cost_centers.code',$sort_order);
                break;

                case 'cc_description':
                    $budget->leftJoin('cost_centers','budgets.cost_center_id','=','cost_centers.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('cost_centers.description',$sort_order);
                break;

                case 'gl_account':
                    $budget->join('accounts','budgets.account_id','=','accounts.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('accounts.code',$sort_order);
                break;

                case 'gl_description':
                    $budget->join('accounts','budgets.account_id','=','accounts.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('accounts.description',$sort_order);
                break;

                case 'material':
                    $budget->leftJoin('materials','budgets.material_id','=','materials.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('materials.code',$sort_order);
                break;

                case 'material_description':
                    $budget->leftJoin('materials','budgets.material_id','=','materials.id');
                    $budget->select('budgets.*');
                    $budget->orderBy('materials.description',$sort_order);
                break;

                case 'budget_amounce':
                    $budget->orderBy('amount',$sort_order);
                break;

                case 'balance_amounce':
                    $budget->orderBy('amount',$sort_order);
                break;

                case 'responsible':
                    $budget->join('users','budgets.responsible_id','=','users.id');
                    $budget->orderBy('users.firstname',$sort_order);
                break;
                
                default:
                    $budget->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $budget->orderBy('id', 'desc');
        }

        $budget = $budget->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($budget['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $budget['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $budget;
    }

	public function show($id)
    {
        Auth::user()->cekRoleModules(['budget-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Budget::with(['company', 'project', 'cost_center', 'account', 'material', 'responsible'])
            ->with('account.account_group')
            ->findOrFail($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['budget-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $budget = Budget::find($id);

        $q = 'Table "budgets" is updated';

        $log = ActivityLog::where('log_name', 'BUDGET')
            ->where('subject_id', $id)
            ->where('description', $q)
            ->get();

        $map_log = $log->map(function($q) {
            $data = json_decode($q->properties);

            $new = [
                'user' => User::find($q->causer_id),
                'new' => $data->attributes,
                'old' => $data->old
            ];

            return $new;
        });

        $budget->log = $map_log;

        return $budget;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['budget-create']);

        $this->validate(request(), [
            'fiscal_year' => 'required|date_format:Y',
            'material_id' => 'nullable|exists:materials,id',
            'company_id' => 'nullable|exists:companies,id',
            'project_id' => 'nullable|exists:projects,id',
            'cost_center_id' => 'nullable|exists:cost_centers,id',
            'account_id' => 'nullable|exists:accounts,id',
            // 'commitment_item' => '',
            'currency' => 'required',
            'amount' => 'required',
            // 'status' => '',
            'responsible_id' => 'nullable|exists:users,id',
            'release_group_id' => 'nullable|exists:release_groups,id',
            'release_strategy_id' => 'nullable|exists:release_strategies,id',
            'release_indicator' => 'nullable',
            'release_state' => 'nullable',
            'description' => 'required|string|min:10'
        ]);

        // cek material tdk boleh sendiri
        if (!empty($request->material_id)) {
            if (empty($request->company_id) && empty($request->project_id) && empty($request->cost_center_id) && empty($request->account_id)) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'material_id' => ['material must filled with minimum one of company / project / cost center / gl account']
                    ]
                ], 422);
            }
        } else {
            if (empty($request->company_id) && empty($request->project_id) && empty($request->cost_center_id) && empty($request->account_id)) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'company_id' => ['minimum one of company / project / cost center / gl account is required']
                    ]
                ], 422);
            }
        }

        $budget = Budget::create([
            'fiscal_year' => $request->fiscal_year,
            'material_id' => $request->material_id,
            'company_id' => $request->company_id,
            'project_id' => $request->project_id,
            'cost_center_id' => $request->cost_center_id,
            'account_id' => $request->account_id,
            // 'commitment_item' => ,
            'currency' => $request->currency,
            'amount' => $request->amount,
            'status' => 0,
            'responsible_id' => $request->responsible_id,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'deleted' => 0,
            'release_group_id' => $request->release_group_id,
            'release_strategy_id' => $request->release_strategy_id,
            'release_indicator' => $request->release_indicator,
            'release_state' => $request->release_state,
            // 'approved_by' => '',
            // 'approved_at' => '',
            'description' => $request->description
        ]);

        return $budget;
    }

    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['budget-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $budget = Budget::findOrFail($id);

        // Record apa saja yg berubah

        $this->validate(request(), [
            'fiscal_year' => 'required|date_format:Y',
            'material_id' => 'nullable|exists:materials,id',
            'company_id' => 'nullable|exists:companies,id',
            'project_id' => 'nullable|exists:projects,id',
            'cost_center_id' => 'nullable|exists:cost_centers,id',
            'account_id' => 'nullable|exists:accounts,id',
            // 'commitment_item' => '',
            'currency' => 'required',
            'amount' => 'required',
            // 'status' => '',
            'responsible_id' => 'nullable|exists:users,id',
            'release_group_id' => 'nullable|exists:release_groups,id',
            'release_strategy_id' => 'nullable|exists:release_strategies,id',
            'release_indicator' => 'nullable',
            'release_state' => 'nullable',
            'description' => 'required|string|min:10'
        ]);

        // cek material tdk boleh sendiri
        if (!empty($request->material_id)) {
            if (empty($request->company_id) && empty($request->project_id) && empty($request->cost_center_id) && empty($request->account_id)) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'material_id' => ['material must filled with minimum one of company / project / cost center / gl account']
                    ]
                ], 422);
            }
        } else {
            if (empty($request->company_id) && empty($request->project_id) && empty($request->cost_center_id) && empty($request->account_id)) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'company_id' => ['minimum one of company / project / cost center / gl account is required']
                    ]
                ], 422);
            }
        }

        $budget->update([
            'fiscal_year' => $request->fiscal_year,
            'material_id' => $request->material_id,
            'company_id' => $request->company_id,
            'project_id' => $request->project_id,
            'cost_center_id' => $request->cost_center_id,
            'account_id' => $request->account_id,
            // 'commitment_item' => ,
            'currency' => $request->currency,
            'amount' => $request->amount,
            // 'status' => 0,
            'responsible_id' => $request->responsible_id,
            'updated_by' => Auth::user()->id,
            'release_group_id' => $request->release_group_id,
            'release_strategy_id' => $request->release_strategy_id,
            'release_indicator' => $request->release_indicator,
            'release_state' => $request->release_state,
            // 'approved_by' => '',
            // 'approved_at' => '',
            'description' => $request->description
        ]);

        return $budget;
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['budget-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        Budget::findOrFail($id)->update([
            'deleted' => 1,
            'updated_by' => Auth::user()->id
        ]);

        return response()->json([
            'message' => 'success delete data'
        ], 200);
    }

    public function submit($id, Request $request)
    {
        Auth::user()->cekRoleModules(['budget-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'notes' => 'nullable|max:191'
        ]);

        $budget = Budget::findOrFail($id);

        if ($budget->status != 0) {
            return response()->json([
                'message' => 'budget status must draft',
            ], 422);
        }

        try {
            DB::beginTransaction();

            // record approval
            BudgetApproval::create([
                'budget_id' => $id,
                'user_id' => Auth::user()->id,
                'status_from' => $budget->status,
                'status_to' => 1,//submited
                'notes' => $request->notes
            ]);

            // generate commitment item
            $code = TransactionCode::where('code', 'E')->first();

            // if exist
            if ($code->lastcode) {
                // generate if this year is graether than year in code
                $yearcode = substr($code->lastcode, 1, 2);
                $increment = substr($code->lastcode, 3, 7) + 1;
                $year = substr(date('Y'), 2);
                if ($year > $yearcode) {
                    $lastcode = 'E';
                    $lastcode .= substr(date('Y'), 2);
                    $lastcode .= '0000001';
                } else {
                    // increment if year in code is equal to this year
                    $lastcode = 'E';
                    $lastcode .= substr(date('Y'), 2);
                    $lastcode .= sprintf("%07d", $increment);
                }
            } else {//generate if not exist
                $lastcode = 'E';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            }

            $commitment_item = $lastcode;

            // update status & commitment item
            $budget->update([
                'commitment_item' => $commitment_item,
                'status' => 1,//submited
                'updated_by' => Auth::user()->id
            ]);

            // update transaction code
            $code->update([
                'lastcode' => $commitment_item,
            ]);

            DB::commit();

            return response()->json([
                'message' => 'success submit data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error submit data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function approve($id, Request $request)
    {
        Auth::user()->cekRoleModules(['budget-approval']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $budget = Budget::findOrFail($id);

        if ($budget->status != 1) {
            return response()->json([
                'message' => 'budget status must submited before approval',
            ], 422);
        }

        try {
            DB::beginTransaction();

            // record approval
            BudgetApproval::create([
                'budget_id' => $id,
                'user_id' => Auth::user()->id,
                'status_from' => $budget->status,
                'status_to' => 2,//active
                'notes' => $request->notes
            ]);

            // update status & commitment item
            $budget->update([
                'status' =>  2,//active
                'updated_by' => Auth::user()->id
            ]);

            DB::commit();

            return response()->json([
                'message' => 'success approve data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error approve data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['budget-create']);

        return Excel::download(new BudgetExport, 'budget.xlsx');
    }

    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['budget-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new BudgetImport($user);

        Excel::import($import, $file);

        // store session if error is null
        $countError = collect($import->getRow())->whereIn('valid', false)->count();
        if ($countError == 0) {
            $valid = true;
            $code = 200;
            // todo insert to temporary table
            $data = collect($import->getRow())->toArray();
            foreach ($data as $temp) {
                BudgetTemporary::create([
                    'fiscal_year' => $temp['fiscal_year'],
                    'description' => $temp['description'],
                    'project_code' => $temp['project_code'],
                    'cost_center' => $temp['cost_center'],
                    'gl_account' => $temp['gl_account'],
                    'material' => $temp['material'],
                    'amount' => $temp['amount'],
                    'currency' => $temp['currency'],
                    // 'company' => $temp['company'],
                    'created_by' => Auth::user()->id
                ]);
            }
        } else {
            $valid = false;
            $code = 422;
        }

        return response()->json([
            'message' => 'upload validate',
            'valid' => $valid,
            'data' => $import->getRow()
        ], $code);
    }

    public function getValidData()
    {
        Auth::user()->cekRoleModules(['budget-create']);

        $data = BudgetTemporary::where('created_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $new = $data->map(function($row){
            $material = Material::where(DB::raw("LOWER(code)"), strtolower($row->material))->first();
            $project = Project::where(DB::raw("LOWER(code)"), strtolower($row->project_code))->first();
            $cost_center = CostCenter::where(DB::raw("LOWER(code)"), strtolower($row->cost_center))->first();
            $gl_account = Account::where(DB::raw("LOWER(description)"), strtolower($row->gl_account))->first();

            $new_data = [
                'uploaded_by' => $row->created_by,
                'fiscal_year' => $row->fiscal_year,
                'description' => $row->description,
                'project_code' => $row->project_code,
                'cost_center' => $row->cost_center,
                'gl_account' => $row->gl_account,
                'material' => $row->material,
                'amount' => $row->amount,
                'currency' => $row->currency,
                'project_id' => isset($project) ? $project->id : null,
                'cost_center_id' => isset($cost_center) ? $cost_center->id : null,
                'gl_account_id' => isset($gl_account) ? $gl_account->id : null,
                'material_id' => isset($material) ? $material->id : null,
                'valid' => true,
                'message' => null
            ];

            return $new_data;
        });

        return response()->json([
            'message' => 'valid data',
            'valid' => true,
            'data' => $new
        ], 200);
    }

    public function uploadValidData()
    {
        Auth::user()->cekRoleModules(['budget-create']);

        $data = BudgetTemporary::where('created_by', Auth::user()->id)->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],422);
        }

        $user = Auth::user();

        BudgetJob::dispatch($user);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
}
