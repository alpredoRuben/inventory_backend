<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Master\CoaTemplateExport;
use App\Exports\Master\CoaTemplateUpload;
use App\Exports\Master\CoaInvalidExport;
use App\Imports\Master\CoaImport;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\ChartOfAccount;
use App\Models\AccountGroup;
use App\Models\Account;
use App\Models\GroupType;
use App\Models\AccountType;
use App\Models\CoaFailedUpload;

use App\Helpers\HashId;

class ChartOfAccountController extends Controller
{
    public function chartOfAccount()
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        $chart = (new ChartOfAccount)->newQuery();

        $chart->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $chart->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $chart->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $chart->orderBy('code', 'asc');
        }

        $chart = $chart->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($chart['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $chart['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $chart;
    }

    public function chartOfAccountList()
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        $chart = (new ChartOfAccount)->newQuery();

        $chart->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $chart->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $chart->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $chart->orderBy('code', 'asc');
        }

        $chart = $chart->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        return $chart;
    }

    public function storeChartOfAccount(Request $request)
    {
        Auth::user()->cekRoleModules(['chart-of-account-create']);

        $this->validate(request(), [
            'code' => 'required|max:5',
            'description' => 'required|max:30',
        ]);

        $chart = ChartOfAccount::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($chart) {

            if($chart->deleted){
                $save = $chart->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $chart;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = ChartOfAccount::create([
                'code' => $request->code,
                'description' => $request->description,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function showChartOfAccount($id)
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return ChartOfAccount::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function updateChartOfAccount($id, Request $request)
    {
        Auth::user()->cekRoleModules(['chart-of-account-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $chart = ChartOfAccount::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:5|unique:chart_of_accounts,code,'. $id .'',
            'description' => 'required|max:30',
        ]);

        $save = $chart->update([
            'code' => $request->code,
            'description' => $request->description,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $chart;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function deleteChartOfAccount($id)
    {
        Auth::user()->cekRoleModules(['chart-of-account-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = ChartOfAccount::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteChartOfAccount()
    {
        Auth::user()->cekRoleModules(['chart-of-account-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:chart_of_accounts,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = ChartOfAccount::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function accountGroupByCoa($id)
    {
        Auth::user()->cekRoleModules(['account-group-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $group = (new AccountGroup)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $group->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });

            // search group type
            // 1=asset; 2=liability; 3=equity; 4=profit; 5=loss
            $asset = 'asset';
            $liability = 'liability';
            $equity = 'equity';
            $profit = 'profit';
            $loss = 'loss';

            if ($q) {
                if (strpos($asset,$q) !== false) {
                    $group->orWhere('group_type', 1);
                }

                if (strpos($liability,$q) !== false) {
                    $group->orWhere('group_type', 2);
                }

                if (strpos($equity,$q) !== false) {
                    $group->orWhere('group_type', 3);
                }

                if (strpos($profit,$q) !== false) {
                    $group->orWhere('group_type', 4);
                }

                if (strpos($loss,$q) !== false) {
                    $group->orWhere('group_type', 5);
                }
            }
        }

        $group->where('chart_of_account_id', $id);
        $group->where('deleted', false);
        $group->with(['chart_of_account', 'parent', 'createdBy', 'updatedBy', 'group_type', 'account_type']);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $group->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $group->orderBy('code', 'asc');
        }

        $group = $group->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($group['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $v['chart_of_account_id_hash'] = HashId::encode($v['chart_of_account_id']);
                $group['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $group;
    }

    public function storeAccountGroup(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['account-group-create']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'code' => 'required|max:20',
            'description' => 'required|max:30',
            'group_type_id' => 'required|exists:group_types,id',
            'parent_id' => 'nullable|exists:account_groups,id',
            'account_type_id' => 'nullable|exists:account_types,id',
        ]);

        /* group type
        *1=Asset; 2=Liability; 3=Equity; 4=Profit; 5=Loss
        */

        $group = AccountGroup::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($group) {

            if($group->deleted){
                $save = $group->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'group_type_id' => $request->group_type_id,
                    'chart_of_account_id' => $id,
                    'parent_id' => $request->parent_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id,
                    'account_type_id' => $request->account_type_id,
                ]);

                return $group;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = AccountGroup::create([
                'code' => $request->code,
                'description' => $request->description,
                'group_type_id' => $request->group_type_id,
                'chart_of_account_id' => $id,
                'parent_id' => $request->parent_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'account_type_id' => $request->account_type_id,
            ]);

            return $save;
        }
    }

    public function showAccountGroup($id)
    {
        Auth::user()->cekRoleModules(['account-group-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return AccountGroup::with(['chart_of_account', 'parent','createdBy', 'updatedBy', 'group_type', 'account_type'])
        ->findOrFail($id);
    }

    public function updateAccountGroup($id, Request $request)
    {
        Auth::user()->cekRoleModules(['account-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $group = AccountGroup::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:20|unique:account_groups,code,'. $id .'',
            'description' => 'required|max:30',            
            'group_type_id' => 'required|exists:group_types,id',
            'parent_id' => 'nullable|exists:account_groups,id',
            'account_type_id' => 'nullable|exists:account_types,id',
        ]);

        if ($request->parent_id == $id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'parent_id' => ['Parent Account group cant same with Account group']
                ]
            ],422);
        }

        $save = $group->update([
            'code' => $request->code,
            'description' => $request->description,
            'group_type_id' => $request->group_type_id,
            'parent_id' => $request->parent_id,
            'updated_by' => Auth::user()->id,
            'account_type_id' => $request->account_type_id,
        ]);

        if ($save) {
            return $group;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteAccountGroup($id)
    {
        Auth::user()->cekRoleModules(['account-group-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = AccountGroup::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteAccountGroup()
    {
        Auth::user()->cekRoleModules(['account-group-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:account_groups,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = AccountGroup::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function accountByGroup($id)
    {
        Auth::user()->cekRoleModules(['account-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $account = (new Account)->newQuery();

        $account->where('deleted', false)->with(['account_group', 'parent', 'createdBy', 'updatedBy']);

        $account->with('account_group.chart_of_account');
        $account->where('account_group_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $account->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $account->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $account->orderBy('code', 'asc');
        }

        $account = $account->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($account['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $v['account_group_id_hash'] = HashId::encode($v['account_group_id']);
                $v['code_full'] = $v['account_group']['code'] . '.' . $v['code'];
                $account['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $account;
    }

    public function accountList()
    {
        Auth::user()->cekRoleModules(['account-view']);

        $account = (new Account)->newQuery();

        $account->where('accounts.deleted', false)->with(['account_group', 'parent', 'createdBy', 'updatedBy']);

        $account->with('account_group.chart_of_account');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $account->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(accounts.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(accounts.description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'code_full':
                    $account->leftJoin('account_groups','accounts.account_group_id','=','account_groups.id');
                    $account->select('accounts.*');
                    $account->orderBy('account_groups.code',$sort_order);
                break;
                
                default:
                    $account->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $account->orderBy('id', 'asc');
        }

        $account = $account->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();

        $list_data = [];
        foreach ($account['data'] as $a) {
            $a['code_full'] = $a['account_group']['code'] . '.' . $a['code'];
            $list_data[] = $a;
        }
        $account['data'] = $list_data;

        return $account;
    }

    public function storeAccount(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['account-create']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'code' => 'required|max:20',
            'description' => 'required|max:50',
            'is_cash' => 'nullable|boolean',
            'parent_id' => 'nullable|exists:accounts,id',
        ]);

        /* is cash
        *0 not cash, 1 cash
        */

        $account = Account::where('account_group_id', $id)
        ->whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($account) {

            if($account->deleted){
                $save = $account->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'is_cash' => $request->is_cash ? $request->is_cash : 0,
                    'account_group_id' => $id,
                    'parent_id' => $request->parent_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $account;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = Account::create([
                'code' => $request->code,
                'description' => $request->description,
                'is_cash' => $request->is_cash ? $request->is_cash : 0,
                'account_group_id' => $id,
                'parent_id' => $request->parent_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function showAccount($id)
    {
        Auth::user()->cekRoleModules(['account-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Account::with(['account_group', 'parent','createdBy', 'updatedBy'])
        ->with('account_group.chart_of_account')
        ->findOrFail($id);
    }

    public function updateAccount($id, Request $request)
    {
        Auth::user()->cekRoleModules(['account-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $account = Account::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:20|unique:accounts,code,'. $id .'',
            'description' => 'required|max:50',            
            'is_cash' => 'nullable|boolean',
            'parent_id' => 'nullable|exists:accounts,id',
        ]);

        if ($request->parent_id == $id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'parent_id' => ['Parent Account cant same with Account']
                ]
            ],422);
        }

        $save = $account->update([
            'code' => $request->code,
            'description' => $request->description,
            'is_cash' => $request->is_cash ? $request->is_cash : 0,
            'parent_id' => $request->parent_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $account;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteAccount($id)
    {
        Auth::user()->cekRoleModules(['account-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Account::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteAccount()
    {
        Auth::user()->cekRoleModules(['account-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:accounts,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Account::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function groupTypeList()
    {
        $group_type = (new GroupType)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $group_type->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $group_type->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $group_type->orderBy('id', 'asc');
        }

        $group_type = $group_type->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $group_type;
    }

    public function accountTypeList()
    {
        $acc_type = (new AccountType)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $acc_type->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $acc_type->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $acc_type->orderBy('id', 'asc');
        }

        $acc_type = $acc_type->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $acc_type;
    }

    public function glAccountTypeVendorList()
    {
        $akun_vendor = AccountType::whereRaw('LOWER(description) = ?', 'vendors')->first();

        $gl_akun = (new Account)->newQuery();
        $gl_akun->whereHas('account_group', function($query) use ($akun_vendor) {
            $query->where('account_type_id', $akun_vendor->id);
        });

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $gl_akun->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $gl_akun->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $gl_akun->orderBy('id', 'asc');
        }

        $gl_akun = $gl_akun->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $gl_akun;
    }

    public function downloadOld()
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        $chart = (new ChartOfAccount)->newQuery();

        // filter
        if (request()->has('id')) {
            $data = [];
            foreach (request()->id as $key => $ids) {
                try {
                    $ids = HashId::decode($ids);
                } catch(\Exception $ex) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'id.'.$key  => ['id not found']
                        ]
                    ], 422);
                }

                $data[] = $ids;
            }

            request()->merge(['id' => $data]);

            $chart->whereIn('id', request()->input('id'));
        }

        $chart->where('deleted', false)->with('account_groups');

        $chart->with('account_groups.accounts');
        $chart->with('account_groups.group_type');
        $chart->with('account_groups.account_type');

        $chart = $chart->get();

        return Excel::download(new CoaTemplateExport($chart), 'coa.xlsx');
    }

    public function download()
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        // filter
        if (request()->has('id')) {
            $data = [];
            foreach (request()->id as $key => $ids) {
                try {
                    $ids = HashId::decode($ids);
                } catch(\Exception $ex) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'id.'.$key  => ['id not found']
                        ]
                    ], 422);
                }

                $data[] = $ids;
            }

            request()->merge(['id' => $data]);

            $req_id = request()->input('id');
        } else {
            $req_id = [];
        }

        $ag = AccountGroup::where('deleted', false)
            ->whereIn('chart_of_account_id', $req_id)
            ->pluck('id');
        
        $chart = Account::where('deleted', false)
            ->whereIn('account_group_id', $ag)
            ->with(['account_group', 'account_group.group_type', 'account_group.account_type'])
            ->with('account_group.chart_of_account')
            ->get();
        //return $chart;
        return Excel::download(new CoaTemplateExport($chart), 'coa.xlsx');
        //return view('exports.master.coa2', compact("chart"));
    }

    public function downloadTemplate()
    {
        Auth::user()->cekRoleModules(['chart-of-account-view']);

        return Excel::download(new CoaTemplateUpload, 'template_upload_coa.xlsx');
    }

    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['chart-of-account-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new CoaImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }

    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['chart-of-account-create']);

        $data = CoaFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['chart-of-account-create']);

        $data = CoaFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new CoaInvalidExport($data), 'invalid_coa_upload.xlsx');

        // delete failed upload data
        CoaFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['chart-of-account-create']);

        $data = CoaFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        CoaFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
