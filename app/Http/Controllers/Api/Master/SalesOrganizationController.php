<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\SalesOrganization;
use App\Helpers\HashId;

class SalesOrganizationController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['sales-organization-view']);

        $sales = (new SalesOrganization)->newQuery();

        $sales->where('deleted', false)->with(['location', 'createdBy', 'updatedBy']);
        $sales->with('location.location_type');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $sales->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sales->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $sales->orderBy('code', 'asc');
        }

        $sales = $sales->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $sales;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['sales-organization-view']);

        $sales = (new SalesOrganization)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $sales->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });

            // search location
            $sales->where('sales_organizations.deleted', false)->orWhereHas('location', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            })
            ->with(['location' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            }]);
        }

        $sales->where('sales_organizations.deleted', false);
        $sales->with(['location', 'createdBy', 'updatedBy']);
        $sales->with('location.location_type');

        if (request()->has('sort_field')) {
            $sort_field = request()->input('sort_field');
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            switch ($sort_field) {
                case 'location':
                    $sales->join('locations','locations.id','=','sales_organizations.location_id');
                    $sales->select('sales_organizations.*');
                    $sales->orderBy('locations.name',$sort_order);
                break;
                
                default:
                    $sales->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $sales->orderBy('code', 'asc');
        }

        $sales = $sales->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($sales['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $sales['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $sales;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['sales-organization-create']);

        $this->validate(request(), [
            'code' => 'required|max:4',
            'description' => 'required|max:30',
            'location_id' => 'required|exists:locations,id'
        ]);

        $sales = SalesOrganization::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($sales) {

            if($sales->deleted){
                $save = $sales->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'location_id' => $request->location_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $sales;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = SalesOrganization::create([
                'code' => $request->code,
                'description' => $request->description,
                'location_id' => $request->location_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['sales-organization-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return SalesOrganization::with(['location', 'createdBy', 'updatedBy'])
        	->with('location.location_type')
        	->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['sales-organization-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $storage = SalesOrganization::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:4|unique:sales_organizations,code,'. $id .'',
            'description' => 'required|max:30',
            'location_id' => 'required|exists:locations,id'
        ]);

        $save = $storage->update([
            'code' => $request->code,
            'description' => $request->description,
            'location_id' => $request->location_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $storage;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['sales-organization-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = SalesOrganization::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['sales-organization-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:sales_organizations,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = SalesOrganization::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
