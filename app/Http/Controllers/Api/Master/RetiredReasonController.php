<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Helpers\HashId;

use App\Models\RetiredReason;

class RetiredReasonController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['retired-reason-view']);
 
        $retiredReason = (new RetiredReason)->newQuery();

        $retiredReason->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $retiredReason->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $retiredReason->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $retiredReason->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $retiredReason->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $retiredReason->where('deleted', false)->paginate(20)->appends(Input::except('page'));
        }
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['asset-type-view']);

        $retiredReason = (new RetiredReason)->newQuery();

        $retiredReason->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $retiredReason->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $retiredReason->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $retiredReason->orderBy('name', 'asc');
        }

        $retiredReason = $retiredReason->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($retiredReason['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $retiredReason['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $retiredReason;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['retired-reason-create']);

        $this->validate(request(), [
            'code'              => 'required|max:10',
            'name'              => 'required|max:30'
        ]);

        $retiredReason = RetiredReason::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($retiredReason) {

            if($retiredReason->deleted){
                $save = $retiredReason->update([
                    'code'          => $request->code,
                    'name'          => $request->name,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $retiredReason;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = RetiredReason::create([
                'code'          => $request->code,
                'name'          => $request->name,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['retired-reason-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return RetiredReason::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['retired-reason-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $retiredReason = RetiredReason::findOrFail($id);

        $this->validate(request(), [
            'code'              => 'required|max:10|unique:retired_reasons,code,'. $id .'',
            'name'              => 'required|max:30'
        ]);

        $save = $retiredReason->update([
            'code'              => $request->code,
            'name'              => $request->name,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $retiredReason;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['retired-reason-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = RetiredReason::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['retired-reason-update']);

        // $this->validate(request(), [
        //     'id'          => 'required|array',
        //     'id.*'        => 'required|exists:retired_reasons,id',
        // ]);

        $data = [];
        foreach (request()->id as $id) {
            // $delete = RetiredReason::findOrFail($id)->update([
            //     'deleted' => true, 'updated_by' => Auth::user()->id
            // ]);

            try {
                $ids = HashId::decode($id);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:retired_reasons,id',
        ]);

        // if ($delete) {
        //     return response()->json([
        //         'message' => 'Success Delete Data',
        //     ], 200);
        // } else {
        //     return response()->json([
        //         'message' => 'Failed Delete Data',
        //     ], 401);
        // }

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = RetiredReason::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
