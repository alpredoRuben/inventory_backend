<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\User;
use App\Models\Supplier;
use App\Models\Location;
use App\Models\LocationType;
use App\Helpers\HashId;

class SupplierController extends Controller
{
    public function index()
    {
        /* Vendor type
        * 1 = vendor
        * 2 = manufacturer
        * 3 = customer
        * 4 = vendor one time
        */
        Auth::user()->cekRoleModules(['supplier-view']);

        $supplier = (new Supplier)->newQuery();
        $supplier->where('type', '<>', 3)->where('suppliers.deleted', false)
        ->with(['createdBy', 'updatedBy', 'location', 'vendor_group']);

        $supplier->with('location.location_type');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $supplier->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(suppliers.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(suppliers.description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(halal_license_number)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('type')) {
            $supplier->whereIn('type', request()->input('type'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $supplier->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $supplier->orderBy('code', 'asc');
        }

        $supplier = $supplier->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $supplier;
    }

    public function list()
    {
        /* Vendor type
        * 1 = vendor
        * 2 = manufacturer
        * 3 = customer
        * 4 = vendor one time
        */
        Auth::user()->cekRoleModules(['supplier-view']);

        $supplier = (new Supplier)->newQuery();
        $supplier->where('type', '<>', 3)->where('suppliers.deleted', false)
        ->with(['createdBy', 'updatedBy', 'location', 'vendor_group', 'gl_account']);

        $supplier->with('gl_account.account_group');
        $supplier->with('location.location_type');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $supplier->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(suppliers.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(suppliers.description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(halal_license_number)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('type')) {
            $supplier->whereIn('type', request()->input('type'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'vendor_group':
                    $supplier->leftJoin('vendor_groups','suppliers.vendor_group_id','=','vendor_groups.id');
                    $supplier->select('suppliers.*');
                    $supplier->orderBy('vendor_groups.description',$sort_order);
                break;

                case 'gl_account':
                    $supplier->leftJoin('accounts','suppliers.gl_account_id','=','accounts.id');
                    $supplier->leftJoin('account_groups','accounts.account_group_id','=','account_groups.id');
                    $supplier->select('suppliers.*');
                    $supplier->orderBy('account_groups.code',$sort_order);
                break;

                case 'location':
                    $supplier->join('locations','suppliers.location_id','=','locations.id');
                    $supplier->select('suppliers.*');
                    $supplier->orderBy('locations.address',$sort_order);
                break;
                
                default:
                    $supplier->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $supplier->orderBy('code', 'asc');
        }

        $supplier = $supplier->where('suppliers.deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($supplier['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $supplier['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $supplier;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['supplier-create']);

        $this->validate(request(), [
            'type'              => 'required',//1=vendor, 2=manufacturer, 3=customer, 5 = employee
            'code'              => 'required|max:10',
            'description'       => 'required|max:60',
            'halal_certified'   => 'nullable|boolean',
            'halal_license_number'=> 'nullable|max:50',
            'halal_valid_to'    => 'nullable|date',
            'address'           => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'province'          => 'nullable',
            'city'              => 'nullable',
            'building'          => 'nullable',
            'unit'              => 'nullable',
            'contact'           => 'nullable',
            'phone'             => 'nullable',
            'country'           => 'nullable',
            'postal_code'       => 'nullable',
            'email'             => 'nullable|email',
            'vendor_group_id'   => 'nullable|exists:vendor_groups,id',
            'vat_no'            => 'nullable|string|max:30',
            'tax_indicator'     => 'required',
            'gl_account_id'     => 'nullable|exists:accounts,id'
        ]);

        // Chek Location Type where description is vendor or manufacturer
        if ($request->type == 1 | $request->type == 4) {
            $locationType = LocationType::where('code', 'VNDR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for vendor not exists, please create location type with VNDR code first']
                    ]
                ],422);
            }
        } else if ($request->type == 2) {
            $locationType = LocationType::where('code', 'MFNR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for manufacturer not exists, please create location type with MFNR code first']
                    ]
                ],422);
            }
        } else if ($request->type == 5) {
            $locationType = LocationType::where('code', 'VNDR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for vendor not exists, please create location type with VNDR code first']
                    ]
                ],422);
            }

            $user = User::whereRaw('LOWER(username) = ?', strtolower($request->code))->where('deleted', false)->first();

            if (!$user) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'code' => ['User for vendor does not exists, please create user with ' 
                        . $request->code . ' username first']
                    ]
                ],422);
            }
        }

        $supplier = Supplier::whereRaw('LOWER(code) = ?', strtolower($request->code))
        	->where('type', '<>', 3)
        	->first();

        if ($supplier) {

            if($supplier->deleted){
                // cek location code
                $ceklocation = Location::whereRaw('LOWER(code) = ?', strtolower($request->code))
                	->where('location_type_id', $locationType->id)
                	->first();

                if ($ceklocation) {
                    // update if location exist
                    $ceklocation->update([
                        'location_type_id'  => $locationType->id,
                        'code'              => $request->code,
                        'name'              => $request->description,
                        'address'           => $request->address,
                        'latitude'          => $request->latitude,
                        'longitude'         => $request->longitude,
                        'province'          => $request->province,
                        'city'              => $request->city,
                        'building'          => $request->building,
                        'unit'              => $request->unit,
                        'contact'           => $request->contact,
                        'phone'             => $request->phone,
                        'email'             => $request->email,
                        'country'           => $request->country,
                        'postal_code'       => $request->postal_code,
                        'deleted'           => 0,
                        'updated_by'        => Auth::user()->id
                    ]);

                    $location = $ceklocation;
                } else {
                    // create if location not exist
                    $location = Location::create([
                        'location_type_id'  => $locationType->id,
                        'code'              => $request->code,
                        'name'              => $request->description,
                        'address'           => $request->address,
                        'latitude'          => $request->latitude,
                        'longitude'         => $request->longitude,
                        'province'          => $request->province,
                        'city'              => $request->city,
                        'building'          => $request->building,
                        'unit'              => $request->unit,
                        'contact'           => $request->contact,
                        'phone'             => $request->phone,
                        'email'             => $request->email,
                        'country'           => $request->country,
                        'postal_code'       => $request->postal_code,
                        'created_by'        => Auth::user()->id,
                        'updated_by'        => Auth::user()->id,
                    ]);
                }

                $save = $supplier->update([
                    'type'          => $request->type,
                    'code'          => $request->code,
                    'description'   => $request->description,
                    'location_id'   => $location->id,
                    'halal_certified'   => $request->halal_certified ? $request->halal_certified : 0,
                    'halal_license_number'=> $request->halal_license_number,
                    'halal_valid_to'    => $request->halal_valid_to,
                    'deleted'       => 0,                    
                    'vendor_group_id'   => $request->vendor_group_id,
                    'vat_no'            => $request->vat_no,
                    'tax_indicator'     => $request->tax_indicator,
                    'email' => $request->email,
                    'gl_account_id'   => $request->gl_account_id,
                    'updated_by'    => Auth::user()->id
                ]);

                return $supplier;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            // cek location code
            $ceklocation = Location::whereRaw('LOWER(code) = ?', strtolower($request->code))
            	->where('location_type_id', $locationType->id)
            	->first();

            if ($ceklocation) {
                // update if location exist
                $ceklocation->update([
                    'location_type_id'  => $locationType->id,
                    'code'              => $request->code,
                    'name'              => $request->description,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'province'          => $request->province,
                    'city'              => $request->city,
                    'building'          => $request->building,
                    'unit'              => $request->unit,
                    'contact'           => $request->contact,
                    'phone'             => $request->phone,
                    'email'             => $request->email,
                    'country'           => $request->country,
                    'postal_code'       => $request->postal_code,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                $location = $ceklocation;
            } else {
                // create if location not exist
                $location = Location::create([
                    'location_type_id'  => $locationType->id,
                    'code'              => $request->code,
                    'name'              => $request->description,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'province'          => $request->province,
                    'city'              => $request->city,
                    'building'          => $request->building,
                    'unit'              => $request->unit,
                    'contact'           => $request->contact,
                    'phone'             => $request->phone,
                    'email'             => $request->email,
                    'country'           => $request->country,
                    'postal_code'       => $request->postal_code,
                    'gl_account_id'     => $request->gl_account_id,
                    'created_by'        => Auth::user()->id,
                    'updated_by'        => Auth::user()->id
                ]);
            }

            $save = Supplier::create([
                'type'              => $request->type,
                'code'              => $request->code,
                'description'       => $request->description,
                'location_id'       => $location->id,
                'halal_certified'   => $request->halal_certified ? $request->halal_certified : 0,
                'halal_license_number'=> $request->halal_license_number,
                'halal_valid_to'    => $request->halal_valid_to,
                'vendor_group_id'   => $request->vendor_group_id,
                'vat_no'            => $request->vat_no,
                'tax_indicator'     => $request->tax_indicator,
                'email' => $request->email,
                'gl_account_id'   => $request->gl_account_id,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['supplier-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Supplier::with(['createdBy', 'updatedBy', 'location', 'vendor_group', 'gl_account'])
        ->with('location.location_type')->find($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['supplier-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new \App\Models\ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'Supplier')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['supplier-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $supplier = Supplier::findOrFail($id);

        $this->validate(request(), [
            'type'              => 'required',//1=vendor, 2=manufacturer, 3=customer
            'code'              => 'required|max:10',
            'description'       => 'required|max:60',
            'halal_certified'   => 'nullable|boolean',
            'halal_license_number'=> 'nullable|max:50',
            'halal_valid_to'    => 'nullable|date',
            'address'           => 'required',
            'latitude'          => 'nullable',
            'longitude'         => 'nullable',
            'province'          => 'nullable',
            'city'              => 'nullable',
            'building'          => 'nullable',
            'unit'              => 'nullable',
            'contact'           => 'nullable',
            'phone'             => 'nullable',
            'country'           => 'nullable',
            'postal_code'       => 'nullable',
            'email'             => 'nullable|email',            
            'vendor_group_id'   => 'nullable|exists:vendor_groups,id',
            'vat_no'            => 'nullable|string|max:30',
            'tax_indicator'     => 'required',
            'gl_account_id'     => 'nullable|exists:accounts,id'
        ]);

        // Chek Location Type where description is vendor or manufacturer
        if ($request->type == 1 | $request->type == 4) {
            $locationType = LocationType::where('code', 'VNDR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for vendor not exists, please create location type with VNDR code first']
                    ]
                ],422);
            }
        } else if ($request->type == 2) {
            $locationType = LocationType::where('code', 'MFNR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for manufacturer not exists, please create location type with MFNR code first']
                    ]
                ],422);
            }
        } else if ($request->type == 5) {
            $locationType = LocationType::where('code', 'VNDR')->where('deleted', false)->first();

            if (!$locationType) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['Location Type for vendor not exists, please create location type with VNDR code first']
                    ]
                ],422);
            }
            
            $user = User::whereRaw('LOWER(username) = ?', strtolower($request->code))->where('deleted', false)->first();

            if (!$user) {
                return response()->json([
                    'message' => 'Data invalid',
                    'errors' => [
                        'type' => ['User for vendor does not exists, please create user with ' 
                        . $request->code . ' username first']
                    ]
                ],422);
            }
        }

        $ceksupplier = Supplier::whereRaw('LOWER(code) = ?', strtolower($request->code))
        	->where('type', '<>', 3)
        	->where('id', '<>', $id)
        	->first();

        if ($ceksupplier) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            // cek location code
            $ceklocation = Location::whereRaw('LOWER(code) = ?', strtolower($request->code))
            	->where('location_type_id', $locationType->id)
            	->first();

            if ($ceklocation) {
                // update if location exist
                $ceklocation->update([
                    'location_type_id'  => $locationType->id,
                    'code'              => $request->code,
                    'name'              => $request->description,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'province'          => $request->province,
                    'city'              => $request->city,
                    'building'          => $request->building,
                    'unit'              => $request->unit,
                    'contact'           => $request->contact,
                    'phone'             => $request->phone,
                    'email'             => $request->email,
                    'country'           => $request->country,
                    'postal_code'       => $request->postal_code,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                $location = $ceklocation;
            } else {
                // create if location not exist
                $location = Location::create([
                    'location_type_id'  => $locationType->id,
                    'code'              => $request->code,
                    'name'              => $request->description,
                    'address'           => $request->address,
                    'latitude'          => $request->latitude,
                    'longitude'         => $request->longitude,
                    'province'          => $request->province,
                    'city'              => $request->city,
                    'building'          => $request->building,
                    'unit'              => $request->unit,
                    'contact'           => $request->contact,
                    'phone'             => $request->phone,
                    'email'             => $request->email,
                    'country'           => $request->country,
                    'postal_code'       => $request->postal_code,
                    'created_by'        => Auth::user()->id,
                    'updated_by'        => Auth::user()->id
                ]);
            }
            $save = $supplier->update([
                'type'          => $request->type,
                'code'          => $request->code,
                'description'   => $request->description,
                'location_id'   => $location->id,
                'halal_certified'   => $request->halal_certified ? $request->halal_certified : 0,
                'halal_license_number'=> $request->halal_license_number,
                'halal_valid_to'    => $request->halal_valid_to,
                'deleted'       => 0,
                'vendor_group_id'   => $request->vendor_group_id,
                'vat_no'            => $request->vat_no,
                'tax_indicator'     => $request->tax_indicator,
                'email' => $request->email,
                'updated_by'    => Auth::user()->id,
                'gl_account_id'   => $request->gl_account_id,
            ]);

            return $supplier;
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['supplier-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Supplier::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['supplier-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:suppliers,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Supplier::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
