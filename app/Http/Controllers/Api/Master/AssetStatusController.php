<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Helpers\HashId;

use App\Models\AssetStatus;

class AssetStatusController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['asset-status-view']);
 
        $assetStatus = (new AssetStatus)->newQuery();

        $assetStatus->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $assetStatus->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $assetStatus->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $assetStatus->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $assetStatus->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $assetStatus->where('deleted', false)->paginate(20)->appends(Input::except('page'));
        }
    }

    public function listNormal()
    {
        //Untuk mendapatkan aset status selain pending_retired dan retired
        //Mengubah status asset ke pending_retired atau retired harus melalui approval
        Auth::user()->cekRoleModules(['asset-status-view']);
 
        $assetStatus = (new AssetStatus)->newQuery();

        $assetStatus->where('deleted', false)->with(['createdBy', 'updatedBy']);

        $pending_retired = AssetStatus::whereRaw('LOWER(name) = ?', 'pending retired')->first();
        $retired = AssetStatus::whereRaw('LOWER(name) = ?', 'retired')->first();
        if ($pending_retired && $retired) {
            $assetStatus->whereNotIn('id', [$pending_retired->id, $retired->id]);
        }

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $assetStatus->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $assetStatus->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $assetStatus->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $assetStatus->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $assetStatus->where('deleted', false)->paginate(20)->appends(Input::except('page'));
        }
    }
    
    public function list()
    {
        Auth::user()->cekRoleModules(['asset-status-view']);

        $assetStatus = (new AssetStatus)->newQuery();

        $assetStatus->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $assetStatus->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $assetStatus->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $assetStatus->orderBy('name', 'asc');
        }

        $assetStatus = $assetStatus->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($assetStatus['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $assetStatus['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $assetStatus;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-status-create']);

        $this->validate(request(), [
            'code'              => 'required|max:10',
            'name'              => 'required|max:30'
        ]);

        $assetStatus = AssetStatus::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($assetStatus) {

            if($assetStatus->deleted){
                $save = $assetStatus->update([
                    'code'          => $request->code,
                    'name'          => $request->name,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $assetStatus;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = AssetStatus::create([
                'code'          => $request->code,
                'name'          => $request->name,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['asset-status-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return AssetStatus::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-status-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $assetStatus = AssetStatus::findOrFail($id);

        $this->validate(request(), [
            'code'              => 'required|max:10|unique:asset_statuses,code,'. $id .'',
            'name'              => 'required|max:30'
        ]);

        $save = $assetStatus->update([
            'code'              => $request->code,
            'name'              => $request->name,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $assetStatus;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['asset-status-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = AssetStatus::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['asset-status-update']);

        $data = [];
        foreach (request()->id as $ids) {

            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;

            // $delete = AssetType::findOrFail($ids)->update([
            //     'deleted' => true, 'updated_by' => Auth::user()->id
            // ]);
        }

        request()->merge(['id' => $data]);
        
        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:asset_statuses,id',
        ]);

        // foreach (request()->id as $id) {
        //     $delete = AssetStatus::findOrFail($id)->update([
        //         'deleted' => true, 'updated_by' => Auth::user()->id
        //     ]);
        // }

        // if ($delete) {
        //     return response()->json([
        //         'message' => 'Success Delete Data',
        //     ], 200);
        // } else {
        //     return response()->json([
        //         'message' => 'Failed Delete Data',
        //     ], 401);
        // }

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = AssetStatus::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
