<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\ValuationGroup;

class ValuationGroupController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['valuation-group-view']);

        $valuationGroup = (new ValuationGroup)->newQuery();

        $valuationGroup->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $valuationGroup->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $valuationGroup->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $valuationGroup->orderBy('code', 'asc');
        }

        // if have organization parameter
        $valuation_id = Auth::user()->roleOrgParam(['valuation_group']);

        if (count($valuation_id) > 0) {
            $valuationGroup->whereIn('id', $valuation_id);
        }

        if (request()->has('per_page')) {
            return $valuationGroup->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $valuationGroup->where('deleted', false)->paginate(20)->appends(Input::except('page'));
        }
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['valuation-group-create']);

        $this->validate(request(), [
            'code'              => 'required|max:10',
            'name'              => 'required|max:30'
        ]);

        $valuationGroup = ValuationGroup::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($valuationGroup) {

            if($valuationGroup->deleted){
                $save = $valuationGroup->update([
                    'code'          => $request->code,
                    'name'          => $request->name,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $valuationGroup;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = ValuationGroup::create([
                'code'          => $request->code,
                'name'          => $request->name,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['valuation-group-view']);

        return ValuationGroup::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['valuation-group-update']);

        $valuationGroup = ValuationGroup::findOrFail($id);

        $this->validate(request(), [
            'code'              => 'required|max:10|unique:valuation_groups,code,'. $id .'',
            'name'              => 'required|max:30'
        ]);

        $save = $valuationGroup->update([
            'code'              => $request->code,
            'name'              => $request->name,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $valuationGroup;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['valuation-group-update']);

        $delete = ValuationGroup::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['valuation-group-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:valuation_groups,id',
        ]);

        foreach (request()->id as $id) {
            $delete = ValuationGroup::findOrFail($id)->update([
                'deleted' => true, 'updated_by' => Auth::user()->id
            ]);
        }

        if ($delete) {
            return response()->json([
                'message' => 'Success Delete Data',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }
}
