<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\Supplier;
use App\Helpers\HashId;

class CustomerController extends Controller
{
    public function index()
    {
        /* Vendor type
        * 1 = vendor
        * 2 = manufacturer
        * 3 = customer
        */
        Auth::user()->cekRoleModules(['customer-view']);

        $customer = (new Supplier)->newQuery();
        $customer->where('type', 3)->where('deleted', false)
        ->with(['createdBy', 'updatedBy', 'location']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $customer->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $customer->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $customer->orderBy('code', 'asc');
        }

        $customer = $customer->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $customer;
    }

    public function list()
    {
        /* Vendor type
        * 1 = vendor
        * 2 = manufacturer
        * 3 = customer
        */
        Auth::user()->cekRoleModules(['customer-view']);

        $customer = (new Supplier)->newQuery();
        $customer->where('type', 3)->where('deleted', false)
        ->with(['createdBy', 'updatedBy', 'location']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $customer->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $customer->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $customer->orderBy('code', 'asc');
        }

        $customer = $customer->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($customer['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $customer['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $customer;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['customer-create']);

        $this->validate(request(), [
            // 'type'              => 'required',//1=supplier, 2=serpo, 3=customer
            'code'              => 'required|max:10',
            'description'       => 'required|max:50',
            'location_id'       => 'required|exists:locations,id'
        ]);

        $customer = Supplier::whereRaw('LOWER(code) = ?', strtolower($request->code))
            ->where('type', 3)
            ->first();

        if ($customer) {

            if($customer->deleted){
                $save = $customer->update([
                    'code'          => $request->code,
                    'description'   => $request->description,
                    'location_id'   => $request->location_id,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $customer;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = Supplier::create([
                'type'          => 3,
                'code'          => $request->code,
                'description'   => $request->description,
                'location_id'   => $request->location_id,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['customer-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Supplier::with(['createdBy', 'updatedBy', 'location'])->find($id);
    }

    public function log($id)
    {
        Auth::user()->cekRoleModules(['customer-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $log = (new \App\Models\ActivityLog)->newQuery();

        $log->with('user')
            ->where('log_name', 'Supplier')
            ->whereNotNull('causer_id')
            ->where('subject_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $log->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(properties)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $log->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $log->orderBy('id', 'desc');
        }

        $log = $log->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $log->transform(function ($data) {
            $data->properties = json_decode($data->properties);

            return $data;
        });

        return $log;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['customer-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $customer = Supplier::findOrFail($id);

        $this->validate(request(), [
            // 'type'              => 'required',//1=supplier, 2=serpo, 3=customer
            'code'              => 'required|max:10',
            'description'       => 'required|max:50',
            'location_id'       => 'required|exists:locations,id'
        ]);

        $cekcustomer = Supplier::whereRaw('LOWER(code) = ?', strtolower($request->code))
            ->where('type', 3)
            ->where('id', '<>', $id)
            ->first();

        if ($cekcustomer) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = $customer->update([
                'code'          => $request->code,
                'description'   => $request->description,
                'location_id'   => $request->location_id,
                'deleted'       => 0,
                'updated_by'    => Auth::user()->id
            ]);

            return $customer;
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['customer-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Supplier::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['customer-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:suppliers,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Supplier::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
