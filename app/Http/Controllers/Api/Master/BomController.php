<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\Bom;
use App\Models\BomDetail;
use App\Models\Uom;
use App\Models\Material;
use App\Helpers\HashId;

class BomController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['bom-view']);

        $bom = (new BomDetail)->newQuery();

        // if deleted header
        $bom_id = Bom::where('deleted', false)->pluck('id');

        $bom->with(['component', 'uom', 'createdBy', 'updatedBy']);
        $bom->with('bom.material');
        $bom->with('bom.plant');
        $bom->whereIn('bom_id', $bom_id);

        if (request()->has('material_id')) {
            $material_id = request()->input('material_id');
            $bom->whereHas('bom', function ($q) use ($material_id) {
                $q->whereIn('material_id', $material_id);
            });
        }

        if (request()->has('plant_id')) {
            $plant_id = request()->input('plant_id');
            $bom->whereHas('bom', function ($q) use ($plant_id) {
                $q->whereIn('plant_id', $plant_id);
            });
        }

        if (request()->has('type')) {
            $type = request()->input('type');
            $bom->whereHas('bom', function ($q) use ($type) {
                $q->whereIn('type', $type);
            });
        }

        if (request()->has('status')) {
            $status = request()->input('status');
            $bom->whereHas('bom', function ($q) use ($status) {
                $q->whereIn('status', $status);
            });
        }

        if (request()->has('component')) {
            $bom->whereIn('component_id', request()->input('component'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $bom->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $bom->orderBy('id', 'asc');
        }

        $bom = $bom->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($bom['data'] as $k => $v) {
            try {
                $v['bom_id'] = HashId::encode($v['bom_id']);
                $bom['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $bom;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['bom-create']);

        $this->validate(request(), [
            'material_id' => 'required|exists:materials,id',
            'plant_id' => 'required|exists:plants,id',
            'type' => 'required|boolean',
            'valid_from' => 'nullable|date',
            'status' => 'nullable|boolean',
        ]);

        $bom = Bom::where('material_id', $request->material_id)
        	->where('plant_id', $request->plant_id)
        	->first();

        if ($bom) {

            if($bom->deleted){
                $save = $bom->update([
                    'material_id' => $request->material_id,
		            'plant_id' => $request->plant_id,
		            'type' => $request->type ? $request->type : 0,
		            'valid_from' => $request->valid_from,
		            'status' => $request->status ? $request->status : 0,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                $bom->id_hash = HashId::encode($bom->id);

                return $bom;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'material_id' => ['Data already exists']
                ]
            ],422);
        } else {
            $save = Bom::create([
            	'material_id' => $request->material_id,
	            'plant_id' => $request->plant_id,
	            'type' => $request->type ? $request->type : 0,
	            'valid_from' => $request->valid_from,
	            'status' => $request->status ? $request->status : 0,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $save->id_hash = HashId::encode($save->id);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['bom-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return Bom::with(['material', 'plant', 'detail', 'createdBy', 'updatedBy'])
        	->with('detail.component')
        	->with('detail.uom')
        	->with('detail.uom_entry')
        	->find($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['bom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $bom = Bom::find($id);

        $this->validate(request(), [
            'material_id' => 'required|exists:materials,id',
            'plant_id' => 'required|exists:plants,id',
            'type' => 'required|boolean',
            'valid_from' => 'nullable|date',
            'status' => 'nullable|boolean',
        ]);

        $bom_cek = Bom::where('material_id', $request->material_id)
        	->where('plant_id', $request->plant_id)
        	->where('id', '!=', $id)
        	->where('deleted', false)
        	->first();

        if ($bom_cek) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'material_id' => ['Data already exists']
                ]
            ],422);
        } else {
            $save = $bom->update([
            	'material_id' => $request->material_id,
	            'plant_id' => $request->plant_id,
	            'type' => $request->type ? $request->type : 0,
	            'valid_from' => $request->valid_from,
	            'status' => $request->status ? $request->status : 0,
                'updated_by' => Auth::user()->id
            ]);

            $bom->id_hash = HashId::encode($id);

            return $bom;
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['bom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = Bom::findOrFail((int)$id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['bom-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:boms,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Bom::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function storeDetail($id, Request $request)
    {
        Auth::user()->cekRoleModules(['bom-create']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $bom_head = Bom::find($id);

        $this->validate(request(), [
            'item' => 'array',
            'item.*.component_id' => 'required|exists:materials,id',
            'item.*.item' => 'nullable|numeric',
            'item.*.quantity_entry' => 'nullable|numeric',
            'item.*.uom_entry_id' => 'nullable|exists:uoms,id',
            'item.*.valid_from' => 'nullable|date',
            'item.*.valid_to' => 'nullable|date',
            'item.*.remarks' => 'nullable|max:191',
        ]);

        // validate duplicate material
        foreach ($request->item as $current_key => $current_array) {
            foreach ($request->item as $search_key => $search_array) {
                if ($search_array['component_id'] == $current_array['component_id']) {
                    if ($search_key != $current_key) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$search_key.'.component_id'  => ['Can not enter the same material code more than once']
                            ]
                        ], 422);
                    }
                }
            }
        }

        try {
            DB::beginTransaction();

            foreach ($request->item as $key => $val) {
                if ($val['component_id'] && $val['uom_entry_id']) {
                    // uom entry
                    $unit_entry = Uom::find($val['uom_entry_id']);

                    // find material
                    $mat = Material::find($val['component_id']);

                    // check entry uom = base uom material
                    if ($val['uom_entry_id'] == $mat->uom_id) {
                        $save = BomDetail::create([
                            'bom_id' => $id,
                            'component_id' => $val['component_id'],
                            'quantity' => $val['quantity_entry'],
                            'uom_id' => $mat->uom_id,
                            'quantity_entry' => $val['quantity_entry'],
                            'uom_entry_id' => $val['uom_entry_id'],
                            'valid_from' => $val['valid_from'],
                            'valid_to' => $val['valid_to'],
                            'remarks' => $val['remarks'],
                            'item' => $val['item'],
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    } else {
                        // convert uom
                        $convert_qty = uomconversion($val['component_id'], $val['uom_entry_id'], $val['quantity_entry']);                

                        if (!$convert_qty) {
                            return response()->json([
                                'message' => 'Data invalid',
                                'errors' => [
                                    'material_id' => ['Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion']
                                ]
                            ],422);
                        }

                        $save = BomDetail::create([
                            'bom_id' => $id,
                            'component_id' => $val['component_id'],
                            'quantity' => $convert_qty,
                            'uom_id' => $mat->uom_id,
                            'quantity_entry' => $val['quantity_entry'],
                            'uom_entry_id' => $val['uom_entry_id'],
                            'valid_from' => $val['valid_from'],
                            'valid_to' => $val['valid_to'],
                            'remarks' => $val['remarks'],
                            'item' => $val['item'],
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                } else {
                    $save = BomDetail::create([
                        'bom_id' => $id,
                        'component_id' => $val['component_id'],
                        'valid_from' => $val['valid_from'],
                        'valid_to' => $val['valid_to'],
                        'remarks' => $val['remarks'],
                        'item' => $val['item'],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'success add detail'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while Add Detail',
                'detail' => $e->getMessage()
            ], 400);
        }
    }

    public function updateDetail($id, Request $request)
    {
        Auth::user()->cekRoleModules(['bom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $bom_head = Bom::find($id);

        $bom_detail = $bom_head->detail->pluck('component_id')->toArray();

        $this->validate(request(), [
            'item' => 'array',
            'item.*.component_id' => 'required|exists:materials,id',
            'item.*.item' => 'nullable|numeric',
            'item.*.quantity_entry' => 'nullable|numeric',
            'item.*.uom_entry_id' => 'nullable|exists:uoms,id',
            'item.*.valid_from' => 'nullable|date',
            'item.*.valid_to' => 'nullable|date',
            'item.*.remarks' => 'nullable|max:191',
        ]);

        // validate duplicate material
        foreach ($request->item as $current_key => $current_array) {
            foreach ($request->item as $search_key => $search_array) {
                if ($search_array['component_id'] == $current_array['component_id']) {
                    if ($search_key != $current_key) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$search_key.'.component_id'  => ['Can not enter the same material code more than once']
                            ]
                        ], 422);
                    }
                }
            }
        }

        // get material from request
        $req_component = collect($request->item)->pluck('component_id')->toArray();

        // compare material id with material id in this bom_detail
        $unused_mat = array_diff($bom_detail, $req_component);
        $new_mat = array_diff($req_component, $bom_detail);

        try {
            DB::beginTransaction();

            foreach ($request->item as $key => $val) {
                if ($val['component_id'] && $val['uom_entry_id']) {
                    // uom entry
                    $unit_entry = Uom::find($val['uom_entry_id']);

                    // find material
                    $mat = Material::find($val['component_id']);

                    // check entry uom != base uom material
                    if ($val['uom_entry_id'] != $mat->uom_id) {
                        // convert uom
                        $convert_qty = uomconversion($val['component_id'], $val['uom_entry_id'], $val['quantity_entry']);                

                        if (!$convert_qty) {
                            throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                        }
                    } else {
                        $convert_qty = null;
                    }
                }

                $bom_update = BomDetail::updateOrCreate(
                    [
                        'bom_id' => $id,
                        'component_id' => $val['component_id'],   
                    ],
                    [
                        'bom_id' => $id,
                        'component_id' => $val['component_id'],
                        'quantity' => $convert_qty ? $convert_qty : $val['quantity_entry'],
                        'uom_id' => $mat->uom_id,
                        'quantity_entry' => $val['quantity_entry'],
                        'uom_entry_id' => $val['uom_entry_id'] ? $val['uom_entry_id'] : $mat->uom_id,
                        'valid_from' => $val['valid_from'],
                        'valid_to' => $val['valid_to'],
                        'remarks' => $val['remarks'],
                        'item' => $val['item'],
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]
                );
            }

            // delete unsed item based unused material id
            foreach ($unused_mat as $mat_id) {
                BomDetail::where('component_id', $mat_id)
                ->where('bom_id', $id)
                ->first()
                ->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'success edit detail'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while edit Detail',
                'detail' => $e->getMessage()
            ], 400);
        }
    }

    public function deleteDetail($id)
    {
        Auth::user()->cekRoleModules(['bom-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = BomDetail::find($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDeleteDetail()
    {
        Auth::user()->cekRoleModules(['bom-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:bom_details,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = BomDetail::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
