<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Helpers\HashId;

use App\Models\AssetType;

class AssetTypeController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['asset-type-view']);

        $assetType = (new AssetType)->newQuery();

        $assetType->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $assetType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $assetType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $assetType->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $assetType->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $assetType->where('deleted', false)->paginate(20)->appends(Input::except('page'));
        }
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['asset-type-view']);

        $assetType = (new AssetType)->newQuery();

        $assetType->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $assetType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $assetType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $assetType->orderBy('name', 'asc');
        }

        $assetType = $assetType->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($assetType['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $assetType['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $assetType;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-type-create']);

        $this->validate(request(), [
            'code'              => 'required|max:10',
            'name'              => 'required|max:30'
        ]);

        $assetType = AssetType::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($assetType) {

            if($assetType->deleted){
                $save = $assetType->update([
                    'code'          => $request->code,
                    'name'          => $request->name,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $assetType;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = AssetType::create([
                'code'          => $request->code,
                'name'          => $request->name,
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['asset-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return AssetType::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $assetType = AssetType::findOrFail($id);

        $this->validate(request(), [
            'code'              => 'required|max:10|unique:asset_types,code,'. $id .'',
            'name'              => 'required|max:30'
        ]);

        $save = $assetType->update([
            'code'              => $request->code,
            'name'              => $request->name,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $assetType;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['asset-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = AssetType::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['asset-type-update']);

        // $this->validate(request(), [
        //     'id'          => 'required|array',
        //     'id.*'        => 'required|exists:asset_types,id',
        // ]);
        $data = [];
        foreach (request()->id as $ids) {

            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;

            // $delete = AssetType::findOrFail($ids)->update([
            //     'deleted' => true, 'updated_by' => Auth::user()->id
            // ]);
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:asset_types,id',
        ]);

        // if ($delete) {
        //     return response()->json([
        //         'message' => 'Success Delete Data',
        //     ], 200);
        // } else {
        //     return response()->json([
        //         'message' => 'Failed Delete Data',
        //     ], 401);
        // }

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = AssetType::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }
}
