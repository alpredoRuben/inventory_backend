<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\MaterialVendor;
use App\Models\MaterialVendorHistory;
use App\Helpers\HashId;

class MaterialVendorController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['material-vendor-view']);

        $material = (new MaterialVendor)->newQuery();

        $material->where('material_vendors.deleted', false)->with([
            'material', 'vendor', 'proc_group', 'plant', 'storage', 'std_uom', 'min_uom',
            'createdBy', 'updatedBy', 'purchase_header', 'purchase_item'
        ]);

        if (request()->has('vendor')) {
            $material->whereIn('vendor_id', request()->input('vendor'));
        }

        if (request()->has('material')) {
            $material->whereIn('material_id', request()->input('material'));
        }

        if (request()->has('plant')) {
            $material->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('storage')) {
            $material->whereIn('storage_id', request()->input('storage'));
        }

        if (request()->has('proc_group')) {
            $material->whereIn('proc_group_id', request()->input('proc_group'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'vendor':
                    $material->join('suppliers','material_vendors.vendor_id','=','suppliers.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('suppliers.code',$sort_order);
                break;

                case 'vendor_description':
                    $material->join('suppliers','material_vendors.vendor_id','=','suppliers.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('suppliers.description',$sort_order);
                break;

                case 'material':
                    $material->join('materials','material_vendors.material_id','=','materials.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('materials.code',$sort_order);
                break;

                case 'material_description':
                    $material->join('materials','material_vendors.material_id','=','materials.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('materials.description',$sort_order);
                break;

                case 'plant':
                    $material->leftJoin('plants','material_vendors.plant_id','=','plants.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('plants.code',$sort_order);
                break;

                case 'storage':
                    $material->leftJoin('storages','material_vendors.storage_id','=','storages.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('storages.code',$sort_order);
                break;

                case 'proc_group':
                    $material->join('procurement_groups','material_vendors.proc_group_id','=','procurement_groups.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('procurement_groups.code',$sort_order);
                break;

                case 'min_uom':
                    $material->leftJoin('uoms','material_vendors.min_uom_id','=','uoms.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('uoms.code',$sort_order);
                break;

                case 'std_uom':
                    $material->leftJoin('uoms','material_vendors.std_uom_id','=','uoms.id');
                    $material->select('material_vendors.*');
                    $material->orderBy('uoms.code',$sort_order);
                break;

                default:
                    $material->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $material->orderBy('id', 'desc');
        }

        $material = $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($material['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $material['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $material;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['material-vendor-create']);

        $this->validate(request(), [
            'material_id' => 'required|exists:materials,id',
            'vendor_id' => 'required|exists:suppliers,id',
            'proc_group_id' => 'required|exists:procurement_groups,id',
            'plant_id' => 'nullable|exists:plants,id',
            'storage_id' => 'nullable|exists:storages,id',
            'lead_time' => 'nullable|numeric',
            'unit_price' => 'nullable',
            'currency' => 'nullable',
            'validity_from' => 'nullable|date',
            'std_qty' => 'nullable',
            'std_uom_id' => 'nullable|exists:uoms,id',
            'min_qty' => 'nullable',
            'min_uom_id' => 'nullable|exists:uoms,id',
            'vendor_material' => 'nullable|max:40',
        ]);

        $material = MaterialVendor::where('vendor_id', $request->vendor_id)
	        ->where('material_id', $request->material_id)
	        ->where('storage_id', $request->storage_id)
	        ->first();

        if ($material) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'material_id' => ['Data already exists']
                ]
            ],422);
        } else {
            $save = MaterialVendor::create([
                'material_id' => $request->material_id,
                'vendor_id' => $request->vendor_id,
                'proc_group_id' => $request->proc_group_id,
                'plant_id' => $request->plant_id,
                'storage_id' => $request->storage_id,
                'lead_time' => $request->lead_time,
                'unit_price' => $request->unit_price,
                'currency' => $request->currency,
                'validity_from' => $request->validity_from,
                'std_qty' => $request->std_qty,
                'std_uom_id' => $request->std_uom_id,
                'min_qty' => $request->min_qty,
                'min_uom_id' => $request->min_uom_id,
                'vendor_material' => $request->vendor_material,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['material-vendor-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return MaterialVendor::with([
            'material', 'vendor', 'proc_group', 'plant', 'storage', 'std_uom', 'min_uom',
            'createdBy', 'updatedBy', 'purchase_header', 'purchase_item'
        ])->find($id);
    }

    public function showHistory($id)
    {
        Auth::user()->cekRoleModules(['material-vendor-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return MaterialVendorHistory::with('material_vendor')
            ->with([
                'material_vendor.material', 'material_vendor.vendor',
                'createdBy', 'purchase_header', 'purchase_item'
            ])
            ->where('material_vendor_id', $id)
            ->orderBy('id', 'desc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-vendor-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $material = MaterialVendor::find($id);

        $this->validate(request(), [
            'material_id' => 'required|exists:materials,id',
            'vendor_id' => 'required|exists:suppliers,id',
            'proc_group_id' => 'required|exists:procurement_groups,id',
            'plant_id' => 'nullable|exists:plants,id',
            'storage_id' => 'nullable|exists:storages,id',
            'lead_time' => 'nullable|numeric',
            'unit_price' => 'nullable',
            'currency' => 'nullable',
            'validity_from' => 'nullable|date',
            'std_qty' => 'nullable',
            'std_uom_id' => 'nullable|exists:uoms,id',
            'min_qty' => 'nullable',
            'min_uom_id' => 'nullable|exists:uoms,id',
            'vendor_material' => 'nullable|max:40',
        ]);

        $cekmaterial = MaterialVendor::where('vendor_id', $request->vendor_id)
	        ->where('material_id', $request->material_id)
	        ->where('storage_id', $request->storage_id)
	        ->where('id', '!=', $id)
	        ->first();

        if ($cekmaterial) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'material_id' => ['Data already exists']
                ]
            ],422);
        } else {
            // cek if price beda reocord ke tabel history
            if ($material->unit_price != $request->unit_price) {
                MaterialVendorHistory::create([
                    'material_vendor_id' => $material->id,
                    'price' => $material->unit_price,
                    'currency' => $material->currency,
                    'purchase_header_id' => $material->purchase_header_id,
                    'purchase_item_id' => $material->purchase_item_id,
                    'created_by' => Auth::user()->id
                ]);
            }

            $save = $material->update([
                'material_id' => $request->material_id,
                'vendor_id' => $request->vendor_id,
                'proc_group_id' => $request->proc_group_id,
                'plant_id' => $request->plant_id,
                'storage_id' => $request->storage_id,
                'lead_time' => $request->lead_time,
                'unit_price' => $request->unit_price,
                'currency' => $request->currency,
                'validity_from' => $request->validity_from,
                'std_qty' => $request->std_qty,
                'std_uom_id' => $request->std_uom_id,
                'min_qty' => $request->min_qty,
                'min_uom_id' => $request->min_uom_id,
                'vendor_material' => $request->vendor_material,
                'updated_by' => Auth::user()->id
            ]);

            if ($save) {
                return $material;
            } else {
                return response()->json([
                    'message' => 'Failed Update Data',
                ], 422);
            }
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['material-vendor-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $data = MaterialVendor::find($id);

        if (count($data->history) != 0) {
            return response()->json([
                'message' => 'info record have history can\'t delete data',
            ], 422);
        }
        
        $delete = $data->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['material-vendor-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_vendors,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $data = MaterialVendor::find($ids);

                if (count($data->history) != 0) {
                    throw new \Exception('info record have history can\'t delete data');
                }
                
                $data->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
