<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Models\DepreTypeName;
use App\Models\DepreTypeGroup;
use App\Models\DepreTypePeriod;
use App\Helpers\HashId;
use Auth;
use Session;
use Validator;
use DB;
use Carbon\Carbon;

class DepreciationTypeController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['depre-type-view']);

        $depreciationType = (new DepreTypeName)->newQuery();

        $depreciationType->where('deleted', false)->with(['depre_group', 'depre_period', 'createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $depreciationType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('group')) {
            $depreciationType->whereIn('depre_type_group_id', request()->input('group'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $depreciationType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $depreciationType->orderBy('name', 'asc');
        }

        $depreciationType = $depreciationType->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $depreciationType;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['depre-type-view']);

        $depreciationType = (new DepreTypeName)->newQuery();

        $depreciationType->where('deleted', false)->with(['depre_group', 'depre_period', 'createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $depreciationType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('group')) {
            $depreciationType->whereIn('depre_type_group_id', request()->input('group'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $depreciationType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $depreciationType->orderBy('name', 'asc');
        }

        $depreciationType = $depreciationType->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($depreciationType['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $depreciationType['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $depreciationType;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['depre-type-create']);

        $this->validate(request(), [
            'name'                  => 'required|unique:depre_type_name,name,NULL,NULL,deleted,false',
            'depre_type_group_id'   => 'required|exists:depre_type_group,id',
        ]);

        //update if add data already exsist but soft deleted
        $depreciationType = DepreTypeName::where('name', $request->name)->first();

        if ($depreciationType) {
            $save = $depreciationType->update([
                'depre_type_group_id'   => $request->depre_type_group_id,
                'deleted'               => 0,
                'updated_by'            => Auth::user()->id
            ]);

            $depreciationType->id_hash = HashId::encode($depreciationType->id);

            return $depreciationType;
        } else {
            $save = DepreTypeName::create([
                'name'                  => $request->name,
                'depre_type_group_id'   => $request->depre_type_group_id,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id
            ]);

            $save->id_hash = HashId::encode($save->id);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['depre-type-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return DepreTypeName::with(['depre_group', 'depre_period', 'createdBy', 'updatedBy'])
        ->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $depreciationType = DepreTypeName::findOrFail($id);

        $this->validate(request(), [
            'name'                  => 'required|unique:depre_type_name,name,'. $id .'',
            'depre_type_group_id'   => 'required|exists:depre_type_group,id'
        ]);

        $save = $depreciationType->update([
            'name'                  => $request->name,
            'depre_type_group_id'   => $request->depre_type_group_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $depreciationType;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 400);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = DepreTypeName::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:depre_type_name,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = DepreTypeName::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function periodStoreParam($id, Request $request)
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $this->validate(request(), [
            'period'        => 'required',
            'rates'         => 'required',
            'eco_lifetime'  => 'required'
        ]);

        $depreciationType = DepreTypeName::findOrFail($id);

        DB::table('depre_type_period')->where('depre_type_name_id', $depreciationType->id)->update(['softdelete' => 1]);

        $year_period = Carbon::parse($request->period)->format('Y');
        $month_period = Carbon::parse($request->period)->format('m');

        $period = Carbon::createFromDate($year_period, $month_period)->startOfMonth()->format('Y-m-d');

        $save = DepreTypePeriod::create([
            // 'period'                => $request->period."-01", //return tahun & bulan saja dengan format YYYY-MM
            'period'                => $period,
            'rates'                 => $request->rates,
            'eco_lifetime'          => $request->eco_lifetime,
            'depre_type_name_id'    => $depreciationType->id,
            'created_by'             => Auth::user()->id,
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 400);
        }
    }

    public function showPeriodParameter($id)
    {
        Auth::user()->cekRoleModules(['depre-type-view']);

        // try {
        //     $id = HashId::decode($id);
        // } catch(\Exception $ex) {
        //     return response()->json([
        //         'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
        //     ], 400);
        // }

        return DepreTypePeriod::findOrFail($id);
    }

    public function updatePeriodParameter($id, Request $request)
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        $this->validate(request(), [
            'period'        => 'required',
            'rates'         => 'required',
            'eco_lifetime'  => 'required'
        ]);

        // try {
        //     $id = HashId::decode($id);
        // } catch(\Exception $ex) {
        //     return response()->json([
        //         'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
        //     ], 400);
        // }

        $param = DepreTypePeriod::findOrFail($id);

        $year_period = Carbon::parse($request->period)->format('Y');
        $month_period = Carbon::parse($request->period)->format('m');

        $period = Carbon::createFromDate($year_period, $month_period)->startOfMonth()->format('Y-m-d');

        $save = $param->update([
            // 'period'        => $request->period."-01", //return tahun & bulan saja dengan format YYYY-MM
            'period'        => $period,
            'rates'         => $request->rates,
            'eco_lifetime'  => $request->eco_lifetime,
            'updated_by'    => Auth::user()->id
        ]);

        if ($save) {
            $param->depre_type_name_id = HashId::encode($param->depre_type_name_id);
            return $param;
        } else {
            return response()->json([
                'message' => 'Failed Update data',
            ], 400);
        }
    }

    public function deletePeriodParameter($id)
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        // try {
        //     $id = HashId::decode($id);
        // } catch(\Exception $ex) {
        //     return response()->json([
        //         'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
        //     ], 400);
        // }

        $delete = DepreTypePeriod::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 400);
        }
    }

    public function multipleDeletePeriodParam()
    {
        Auth::user()->cekRoleModules(['depre-type-update']);

        $this->validate(request(), [
            'id'    => 'required|array',
            'id.*'  => 'required|exists:depre_type_period,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = DepreTypePeriod::findOrFail($ids)->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function parameterByPeriod($id)
    {
        $depreciation = DepreTypeName::findOrFail($id);

        return DepreTypePeriod::where('depre_type_name_id', $depreciation->id)->where('deleted', false)->get();
    }

    public function selectlist($type)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $depreciation = (new DepreTypeName)->newQuery();

        if($type == "straightline"){
            
            $depreciation->whereHas('depre_group', function ($query){
                                $query->where('is_straight_line', '=', 1);
                            })->with(['depre_group' => function ($query) {
                                $query->where('is_straight_line', '=', 1);
                            }])->where('deleted', '=', 0);
        }else{
            

            $depreciation->whereHas('depre_group', function ($query){
                                $query->where('is_straight_line', '=', 0);
                            })->with(['depre_group' => function ($query) {
                                $query->where('is_straight_line', '=', 0);
                            }])->where('deleted', '=', 0);
        }

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $depreciation->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $depreciation->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $depreciation->orderBy('name', 'asc');
        }

        if (request()->has('per_page')) {
            return $depreciation->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $depreciation->paginate(20)->appends(Input::except('page'));
        }
    }
}
