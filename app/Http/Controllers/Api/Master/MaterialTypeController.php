<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\MaterialType;
use App\Models\Material;
use App\Helpers\HashId;

class MaterialTypeController extends Controller
{    
    public function index()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $materialType = (new MaterialType)->newQuery();

        $materialType->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $materialType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(number_range_from)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(number_range_to)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $materialType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $materialType->orderBy('code', 'asc');
        }

        $materialType = $materialType->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $materialType;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $materialType = (new MaterialType)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));
            $materialType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(number_range_from)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(number_range_to)"), 'LIKE', "%".$q."%");
            });

            // search internal / external
            $internal = 'internal';
            $external = 'external';

            if ($q) {
                if (strpos($internal,$q) !== false) {
                    $materialType->orWhere('number_type', false);
                }

                if (strpos($external,$q) !== false) {
                    $materialType->orWhere('number_type', true);
                }
            }
        }

        $materialType->where('deleted', false)->with(['createdBy', 'updatedBy']);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $materialType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $materialType->orderBy('code', 'asc');
        }

        $materialType = $materialType->where('deleted', false)
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($materialType['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $materialType['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $materialType;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate(request(), [
            'code' => 'required|max:4',
            'description' => 'required|max:30',
            'number_type' => 'required|boolean',
            'number_range_from' => 'required',
            'number_range_to' => 'required',
			'basic' => 'nullable|boolean',
			'classification' => 'nullable|boolean',
			'purchasing' => 'nullable|boolean',
			'mrp' => 'nullable|boolean',
			'storage' => 'nullable|boolean',
			'sales' => 'nullable|boolean',
			'accounting' => 'nullable|boolean',
        ]);

        if (request()->input('number_type')) {
            // check lenght
            if (strlen($request->number_range_to) < strlen($request->number_range_from)) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'number_range_to'  => ['number range to must greater than number range from']
                    ]
                ], 422);
            }

            // check must greater then number range from
            if ($request->number_range_to < $request->number_range_from) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'number_range_to'  => ['number range to must greater than number range from']
                    ]
                ], 422);
            }
        } else {
            // internal
            $this->validate(request(), [
                'number_range_from' => 'numeric',
                'number_range_to' => 'numeric|gt:number_range_from'
            ]);
        }

        $materialType = MaterialType::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($materialType) {

            if($materialType->deleted){
                $save = $materialType->update([
                    'code' => $request->code,
                    'description' => $request->description,
                    'number_type' => $request->number_type,
                    'number_range_from' => $request->number_range_from,
                    'number_range_to' => $request->number_range_to,
                    'basic' => $request->basic ? $request->basic : 0,
                    'classification' => $request->classification ? $request->classification : 0,
                    'purchasing' => $request->purchasing ? $request->purchasing : 0,
                    'mrp' => $request->mrp ? $request->mrp : 0,
                    'storage' => $request->storage ? $request->storage : 0,
                    'sales' => $request->sales ? $request->sales : 0,
                    'accounting' => $request->accounting ? $request->accounting : 0,
                    'deleted'       => 0,
                    'updated_by'    => Auth::user()->id
                ]);

                return $materialType;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = MaterialType::create([
                'code' => $request->code,
                'description' => $request->description,
                'number_type' => $request->number_type,
                'number_range_from' => $request->number_range_from,
                'number_range_to' => $request->number_range_to,
                'basic' => $request->basic ? $request->basic : 0,
                'classification' => $request->classification ? $request->classification : 0,
                'purchasing' => $request->purchasing ? $request->purchasing : 0,
                'mrp' => $request->mrp ? $request->mrp : 0,
                'storage' => $request->storage ? $request->storage : 0,
                'sales' => $request->sales ? $request->sales : 0,
                'accounting' => $request->accounting ? $request->accounting : 0,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            return $save;
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return MaterialType::with(['createdBy', 'updatedBy'])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $materialType = MaterialType::findOrFail($id);

        $this->validate(request(), [
            'code' => 'required|max:4|unique:material_types,code,'. $id .'',
            'description' => 'required|max:30',
            'number_type' => 'required|boolean',
            'number_range_from' => 'required',
            'number_range_to' => 'required',
            'basic' => 'nullable|boolean',
            'classification' => 'nullable|boolean',
            'purchasing' => 'nullable|boolean',
            'mrp' => 'nullable|boolean',
            'storage' => 'nullable|boolean',
            'sales' => 'nullable|boolean',
            'accounting' => 'nullable|boolean',
        ]);

        if (request()->input('number_type')) {
            // check lenght
            if (strlen($request->number_range_to) < strlen($request->number_range_from)) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'number_range_to'  => ['number range to must greater than number range from']
                    ]
                ], 422);
            }

            // check must greater then number range from
            if ($request->number_range_to < $request->number_range_from) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'number_range_to'  => ['number range to must greater than number range from']
                    ]
                ], 422);
            }
        } else {
            // internal
            $this->validate(request(), [
                'number_range_from' => 'numeric',
                'number_range_to' => 'numeric|gt:number_range_from'
            ]);
        }

        $save = $materialType->update([
            'code' => $request->code,
            'description' => $request->description,
            'number_type' => $request->number_type,
            'number_range_from' => $request->number_range_from,
            'number_range_to' => $request->number_range_to,
            'basic' => $request->basic ? $request->basic : 0,
            'classification' => $request->classification ? $request->classification : 0,
            'purchasing' => $request->purchasing ? $request->purchasing : 0,
            'mrp' => $request->mrp ? $request->mrp : 0,
            'storage' => $request->storage ? $request->storage : 0,
            'sales' => $request->sales ? $request->sales : 0,
            'accounting' => $request->accounting ? $request->accounting : 0,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $materialType;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = MaterialType::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_types,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = MaterialType::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function getCodeByMaterialType($id)
    {
        $type = MaterialType::find($id);

        // if type eternal code freetext
        if ($type->number_type) {
            // https://stackoverflow.com/questions/3567180/how-to-increment-letters-like-numbers-in-php
            // $lastcode = Material::where('material_type_id', $id)->orderBy('id', 'desc')->first();

            // $code = $lastcode ? $lastcode->code++ : $type->number_range_from;

            // $data = [
            //     'message' => 'Success get data',
            //     'type' => $type->number_type ? 'external' : 'internal',
            //     'new_code'  => $code,
            //     'material_type' => $type,
            //     'rule'  => 'add new code in first char & plus with 5 alphanumeric uppercase',
            // ];

            // return $data;
        } else {//generate code if  type internal
            $lastcode = Material::where('material_type_id', $id)->orderBy('id', 'desc')->first();

            $code = $lastcode ? $lastcode->code + 1 : $type->number_range_from;

            if ($code > $type->number_range_to) {
                return response()->json([
                    'message' => 'material code from this material type has execed max'
                ]);
            }

            $data = [
                'message' => 'Success get data',
                'type' => $type->number_type ? 'external' : 'internal',
                'new_code'  => $code,
                'material_type' => $type,
                'rule'  => 'canot change code',
            ];

            return $data;
        }
    }
}
