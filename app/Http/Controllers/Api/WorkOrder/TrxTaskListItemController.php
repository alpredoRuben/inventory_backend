<?php

namespace App\Http\Controllers\Api\WorkOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\TrxTaskListItem;
use App\Models\TrxTaskList;
use App\Models\Material;


class TrxTaskListItemController extends Controller
{
    public function index()
    {   
        Auth::user()->cekRoleModules(['task-list-view']);

        $task_list = (new TrxTaskListItem)->newQuery();

        $task_list->with('tasklist', 'plant', 'classification');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $task_list->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(operation_no)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(estimated)"), 'LIKE', "%".$q."%");
            })->orWhereHas('plant', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })->orWhereHas('classification', function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            if (strtolower(request()->input('sort_field')) == 'plant') {
                $task_list->orderBy('plant_id', $sort_order);
            } else if (strtolower(request()->input('sort_field')) == 'classification') {
                $task_list->orderBy('classification_id', $sort_order);
            } else {
                $task_list->orderBy(request()->input('sort_field'), $sort_order);
            }
        } else {
            $task_list->orderBy('operation_no', 'asc');
        }

        $task_list = $task_list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();


        foreach($task_list['data'] as $k => $v) {
            try {
                $v['id'] = \App\Helpers\HashId::encode($v['id']);
                if ($v['created_by']['id']) {
                    $v['created_by']['id'] = \App\Helpers\HashId::encode($v['created_by']['id']);
                }
                if ($v['updated_by']['id']) {
                    $v['updated_by']['id'] = \App\Helpers\HashId::encode($v['updated_by']['id']);
                }
                $task_list['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 401);
            }
        }

        return $task_list;

    }

    public function taskListById($task_id)
    {   
        Auth::user()->cekRoleModules(['task-list-view']);

        $task_list = (new TrxTaskListItem)->newQuery();

        $task_list->where('tasklist_id', $task_id);

        $task_list->with('tasklist', 'plant', 'classification');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $task_list->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(operation_no)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(estimated)"), 'LIKE', "%".$q."%");
            })->orWhereHas('plant', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })->orWhereHas('classification', function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            if (strtolower(request()->input('sort_field')) == 'plant') {
                $task_list->orderBy('plant_id', $sort_order);
            } else if (strtolower(request()->input('sort_field')) == 'classification') {
                $task_list->orderBy('classification_id', $sort_order);
            } else {
                $task_list->orderBy(request()->input('sort_field'), $sort_order);
            }
        } else {
            $task_list->orderBy('operation_no', 'asc');
        }

        $task_list = $task_list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();

        return $task_list;

    }
    
    public function taskListHashById($task_id)
    {   
        Auth::user()->cekRoleModules(['task-list-view']);

        try {
            $task_id = \App\Helpers\HashId::decode($task_id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Task List ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $task_list = (new TrxTaskListItem)->newQuery();

        $task_list->where('tasklist_id', $task_id);

        $task_list->with('tasklist', 'plant', 'classification');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $task_list->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(operation_no)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(estimated)"), 'LIKE', "%".$q."%");
            })->orWhereHas('plant', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
            })->orWhereHas('classification', function($query) use ($q) {
                $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'plant':
                    $task_list->leftJoin('plants','trx_task_list_items.plant_id','=','plants.id');
                    $task_list->select('trx_task_list_items.*');
                    $task_list->orderBy('plants.code',$sort_order);
                break;

                case 'classification':
                    $task_list->leftJoin('classification_materials','trx_task_list_items.classification_id','=','classification_materials.id');
                    $task_list->select('trx_task_list_items.*');
                    $task_list->orderBy('classification_materials.name',$sort_order);
                break;
                
                default:
                    $task_list->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $task_list->orderBy('operation_no', 'asc');
        }

        $task_list = $task_list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();

        foreach($task_list['data'] as $k => $v) {
            try {
                $v['id'] = \App\Helpers\HashId::encode($v['id']);
                if ($v['created_by']['id']) {
                    $v['created_by']['id'] = \App\Helpers\HashId::encode($v['created_by']['id']);
                }
                if ($v['updated_by']['id']) {
                    $v['updated_by']['id'] = \App\Helpers\HashId::encode($v['updated_by']['id']);
                }
                $task_list['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 401);
            }
        }

        return $task_list;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        Auth::user()->cekRoleModules(['task-list-create']);
                
        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Task List ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }
        
        $this->validate(request(), [
            'operation_number' => 'required|integer',
            'description' => 'required|max:255',
            'estimated_work' => 'required|integer',
            'estimated_unit' => 'required|integer|min:1|max:3', //1 = menit, 2 = jam, 3 = hari
            'plant_id' => 'required|exists:plants,id',
            'classification_material_id' => 'required|exists:classification_materials,id',
        ]);
        
        $where = [
                'operation_no' => $request->operation_number,
                'tasklist_id' => $id
            ];
            
        $cekOpNo = TrxTaskListItem::where($where)->first();

        if($cekOpNo)
        {
            return response()->json([
                'message' => 'Operation number is already in used',
                'errors' => ['operation_number' => ['The opeartion number is already in used.']]
            ], 422);
        }else{
            $save = TrxTaskListItem::create([
                'tasklist_id' => $id,
                'plant_id' => $request->plant_id,
                'classification_id' => $request->classification_material_id,
                'operation_no' => $request->operation_number,
                'description' => $request->description,
                'estimated' => $request->estimated_work,
                'estimated_unit' => $request->estimated_unit,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            
            return $save;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['task-list-view']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $task_list = TrxTaskListItem::with('tasklist', 'plant', 'classification')
        ->findOrFail($id);

        $task_list->id_hash = \App\Helpers\HashId::encode($task_list->id);

        return $task_list;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $task_list = TrxTaskListItem::with('tasklist', 'plant', 'classification')->findOrFail($id);

        $this->validate(request(), [
            'operation_number' => 'required|integer',
            'description' => 'required|max:255',
            'estimated_work' => 'required|integer',
            'estimated_unit' => 'required|integer|min:1|max:3', //1 = menit, 2 = jam, 3 = hari
            'plant_id' => 'required|exists:plants,id',
            'classification_material_id' => 'required|exists:classification_materials,id',
        ]);

        $where = [
                'operation_no' => $request->operation_number,
                'tasklist_id' => $id
            ];
            
        $cekOpNo = TrxTaskListItem::where($where)->where('id', '!=', $task_list->id)->first();

        if($cekOpNo)
        {
            return response()->json([
                'message' => 'Operation number is already in used',
                'errors' => ['operation_number' => ['The opeartion number is already in used.']]
            ], 422);
        } else {
            $save = $task_list->update([
                'plant_id' => $request->plant_id,
                'classification_id' => $request->classification_material_id,
                'operation_no' => $request->operation_number,
                'description' => $request->description,
                'estimated' => $request->estimated_work,
                'estimated_unit' => $request->estimated_unit,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);   
        }

        if ($save) {
            $task_list->id_hash = \App\Helpers\HashId::encode($task_list->id);
            // $task_list->createdBy->id_hash = \App\Helpers\HashId::encode($task_list->createdBy->id);
            // $task_list->updatedBy->id_hash = \App\Helpers\HashId::encode($task_list->updatedBy->id);
            // $task_list->id = null;
            // $task_list->createdBy->id = null;
            // $task_list->updatedBy->id = null;

            return $task_list;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $delete = TrxTaskListItem::findOrFail($id);

        $delete->update([
            'updated_by' => Auth::user()->id
        ]);
        $delete = $delete->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete(Request $request)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = \App\Helpers\HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:trx_task_list_items,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = TrxTaskListItem::findOrFail($id);
                $delete->update([
                    'updated_by' => Auth::user()->id
                ]);
                $delete = $delete->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }

    }
}
