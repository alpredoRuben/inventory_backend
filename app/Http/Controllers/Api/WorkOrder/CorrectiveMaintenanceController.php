<?php

namespace App\Http\Controllers\Api\WorkOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use Sentry;
use Storage;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Plant;
use App\Models\WoAsset;
use App\Helpers\HashId;
use App\Models\WoStatus;
use App\Models\WoLevel;
use App\Models\TrxWo;
use App\Models\TrxTaskList;
use App\Models\TrxTaskListItem;
use App\Models\WoTask;
use App\Models\ServiceRequest;
use App\Models\HistoryStatusWo;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class CorrectiveMaintenanceController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $wo = (new TrxWo)->newQuery();
        
        $wo->with(["service", "plant", "wo_level", "wo_status"]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
			$wo->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('wo_status')) {
            $wo->whereIn('wo_status_id', request()->input('wo_status'));
        } else {
            $wo->whereNotIn('wo_status_id',[6]);
        }

        if (request()->has('wo_level')) {
            $wo->whereIn('wo_level_id', request()->input('wo_level'));
        }

        if (request()->has('created_at')) {
            $start = trim(request()->created_at[0],'"');
            $start = Carbon::parse($start)->toDateString();
            $end = trim(request()->created_at[1],'"');
            $end = Carbon::parse($end)->toDateString();
            $wo->whereBetween('created_at', [$start, $end]);
            // $wo->whereBetween('created_at', [request()->created_at[0], request()->created_at[1]]);
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $wo->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $wo->orderBy('created_at', 'desc');
        }

        if (request()->has('per_page')) {
            $wo = $wo->paginate(request()->input('per_page'))
                ->appends(Input::except('page'))->withPath('')->toArray();
        } else {
            $wo = $wo->paginate(20)->appends(Input::except('page'))
                ->withPath('')->toArray();
        }

        return $wo;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $wo = (new TrxWo)->newQuery();
        
        $wo->with(["service", "plant", "wo_level", "wo_status", "created_by", "updated_by"]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
			$wo->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('wo_status')) {
            $wo->whereIn('wo_status_id', request()->input('wo_status'));
        } else {
            $wo->whereNotIn('wo_status_id',[6]);
        }

        if (request()->has('wo_level')) {
            $wo->whereIn('wo_level_id', request()->input('wo_level'));
        }

        if (request()->has('plant')) {
            $wo->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('created_at')) {
            // $start = trim(request()->created_at[0],'"');
            // $start = Carbon::parse($start)->toDateString();
            // $end = trim(request()->created_at[1],'"');
            // $end = Carbon::parse($end)->toDateString();
            // $wo->whereBetween('created_at', [$start, $end]);
            // $wo->whereBetween('created_at', [request()->created_at[0], request()->created_at[1]]);
            
            $from = str_replace('"', '', request()->created_at[0]);
            $to = str_replace('"', '', request()->created_at[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            $wo->where('created_at', '>=', $f.' 00:00:00');
            $wo->where('created_at', '<=', $t.' 00:00:00');
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $wo->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $wo->orderBy('created_at', 'desc');
        }

        if (request()->has('per_page')) {
            $wo = $wo->paginate(request()->input('per_page'))
                ->appends(Input::except('page'))->withPath('')->toArray();
        } else {
            $wo = $wo->paginate(20)->appends(Input::except('page'))
                ->withPath('')->toArray();
        }

        
        foreach($wo['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $wo['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 422);
            }
        }

        return $wo;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-create']);

        $this->validate($request, [
            'service_request_id' => 'required',
            'wo_level_id' => 'required|exists:wo_levels,id',
            'plant_id' => 'required|exists:plants,id',
            'description' => 'required',
            'address' => 'required',
            'latitude' => 'nullable',
            'longitude' => 'nullable',
            'asset_id' => 'nullable|array',
            'asset_id.*' => 'nullable|exists:assets,id',
        ]);

        $code = $this->code();

        try {
            $request->service_request_id = HashId::decode($request->service_request_id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'Service Request ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        DB::beginTransaction();
        try {
            // save wo
            $wo = TrxWo::create([
                'code' => $code,
                'service_id' => $request->service_request_id,
                'wo_type_id' => 1, //corrective maintenance
                'wo_level_id' => $request->wo_level_id,
                'wo_status_id' => 1, //Draft
                'description' => $request->description,
                'address' => $request->address,
                'lat' => $request->latitude,
                'long' => $request->longitude,
                'plant_id' => $request->plant_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);

            // update status ar
            ServiceRequest::findOrFail($request->service_request_id)->update(['status_service_create_wo' => '1']);

            // save history status wo
            $this->insertToHistoryStatusWo($wo->id, 1);

            if(request()->has('asset_id')) { //set value for method insertToWoAsset
                $this->insertToWoAsset($wo->id, $request->asset_id);
            }

            DB::commit();
            
            return response()->json([
                'message' => "Work Order " . $code . " have been created",
            ], 200);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed to create work order and update service request',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $wo = TrxWo::with(["service", "plant", "wo_level", "wo_status", "wo_type", "history_status_wos", "wo_asset",])
        ->where('code', $id)->first();

        $array = array();
        foreach ($wo->wo_asset as $w) {
            array_push($array, $w->asset_id);
        }

        $wo->wo_asset_array = $array;

        return $wo;
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        $this->validate($request, [
            'wo_status_id'  => 'required|exists:wo_statuses,id',
            'wo_level_id'   => 'required|exists:wo_levels,id',
            'description'   => 'nullable',
            'address'       => 'nullable',
        ]);

        $updateWo = TrxWo::where('code', $id)->first();

        try {

            $update = $updateWo->update([
                'wo_status_id' => $request->wo_status_id,
                'wo_level_id' => $request->wo_level_id,
                'description' => $request->description,
                'address' => $request->address,
                'updated_by' => Auth::user()->id,
            ]);

            // insert to history status wo
            $this->insertToHistoryStatusWo($updateWo->id, $request->wo_status_id);

            DB::commit();

            return response()->json([
                'success' => ['message' => 'WO updated Successfully']
            ], 200);

        } catch (\Throwable $th) {
            DB::rollback();
            
            return response()->json([
                'message' => 'Update WO Failed',
                'detail' => $th->getMessage(),
                'trace' => $th->getTrace()
            ], 422);
        }

    }

    public function destroy($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $delete = TrxWo::findOrFail($id);

        try {
            DB::beginTransaction();

            $delete->update([
                'wo_status_id' => 6,
                'updated_by' => Auth::user()->id
            ]);
            $delete = $delete->delete();

            $this->insertToHistoryStatusWo($id, 6); //6 = deleted

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }

    }

    public function multipleDelete(Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = \App\Helpers\HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:trx_wos,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = TrxWo::findOrFail($id);
                $delete->update([
                    'wo_status_id' => 6,
                    'updated_by' => Auth::user()->id
                ]);
                $delete = $delete->delete();
                $this->insertToHistoryStatusWo($id, 6); //6 = deleted
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }

    }

    public function insertToWoAsset($code, $asset_id)
    {
        $data = [];

        foreach($asset_id as $value) {
            $data[] = [
                'trx_wo_id' => $code,
                'asset_id' => $value,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }

        WoAsset::insert($data);
    }

    public function updateStatusTrxWo($woId, $statusId)
    {
        $work_order = TrxWo::where('id', $woId)->where('wo_type_id', 1)->first();

        $update = $work_order->update([
            'wo_status_id' => $statusId,
            'updated_by' => Auth::user()->id
        ]);
    }

    public function insertToHistoryStatusWo($woId, $statusId)
    {
        $historyStatusWo = HistoryStatusWo::where('trx_wo_id', $woId)->orderBy('id','desc')->first();

        $lastStatus     = isset($historyStatusWo->wo_status_id) ? (int)$historyStatusWo->wo_status_id : 1;
        $lastStatusTime = isset($historyStatusWo->created_at) ? $historyStatusWo->created_at : Carbon::now();
        $nowTime        = Carbon::now();
        $duration       = $nowTime->getTimestamp() - $lastStatusTime->getTimestamp();

        HistoryStatusWo::create([
            'trx_wo_id'      => $woId,
            'wo_status_id'   => $statusId,
            'wo_last_status' => $lastStatus,
            'duration'       => $duration,
            'created_by'     => Auth::user()->id,
            'updated_by'     => Auth::user()->id
        ]);
    }

    public function code()
    {
        // code CMYYYYMMDDnomorurut3digit
        $wo = TrxWo::whereDate('created_at', Carbon::today())->orderBy('code', 'desc')->first();

        if (!$wo) {
            $code_wo = 'CM' . date('Ymd') . '001';
            $temp = 1;
        } else {
            $latest_code = $wo->code;
            $len = strlen($latest_code);
            $temp = substr($latest_code, $len - 3, $len);
            $temp = $temp + 1;
            $code_wo = 'CM' . date('Ymd') . sprintf("%03d", $temp);
        }

        return $code_wo;
    }

    public function WoStatus()
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $plant = (new WoStatus)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $plant->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $plant->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $plant->orderBy('id', 'asc');
        }

        $plant = $plant->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $plant;
    }

    public function woLevel() {
        return WoLevel::all();
    }

    public function importTask($id, Request $request) {
        $this->validate($request, [
            'task_list_id'  => 'required|exists:trx_task_lists,id',
        ]);

        $task_id = TrxTaskList::findOrFail($request->task_list_id)->id;
        $wo = TrxWo::findOrFail($id);
        $task_item = TrxTaskListItem::where('tasklist_id', $task_id)->orderBy('operation_no', 'asc')->get();
        $wo_status = WoStatus::where('description', 'ilike', 'draft')->first();

        try {
            DB::beginTransaction();

            foreach ($task_item as $t) {
                WoTask::create([
                    'trx_wo_id' => $id,
                    'level' => $wo->wo_level_id,
                    'operation_no' => $t->operation_no,
                    'description' => $t->description,
                    'estimated' => $t->estimated,
                    'estimated_unit' => $t->estimated_unit,
                    'plant_id' => $t->plant_id,
                    'status' => $wo_status->id,
                    'classification_id' => $t->classification_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Insert WO task success',
                'wo' => $wo,
                'task_item' => $task_item,
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Fail insert to WO Task',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }
    }

    public function addTaskManual($id, Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-create']);

        $this->validate(request(), [
            'operation_no' => 'required|integer',
            'description' => 'required|max:30',
            'estimated_work' => 'required|integer',
            'estimated_unit' => 'required|integer|min:1|max:3', //1 = menit, 2 = jam, 3 = hari
            'plant_id' => 'nullable|exists:plants,id',
            'classification_material_id' => 'nullable|exists:classification_materials,id',
        ]);

        $wo = TrxWo::findOrFail($id);
        $wo_status = WoStatus::where('description', 'ilike', 'draft')->first();

        $where = [
                'operation_no' => $request->operation_no,
                'trx_wo_id' => $id
            ];
            
        $cekOpNo = WoTask::where($where)->first();

        if($cekOpNo) {
            return response()->json([
                'message' => 'Operation number is already in used',
                'errors' => ['operation_number' => ['The opeartion number is already in used.']]
            ], 422);
        } else {
            $save = WoTask::create([
                'trx_wo_id' => $id,
                'level' => $wo->wo_level_id,
                'operation_no' => $request->operation_no,
                'description' => $request->description,
                'estimated' => $request->estimated_work,
                'estimated_unit' => $request->estimated_unit,
                'plant_id' => $request->plant_id,
                'status' => $wo_status->id,
                'classification_id' => $request->classification_material_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);

            $save->id_hash = \App\Helpers\HashId::encode($save->id);
            $save->created_by = \App\Helpers\HashId::encode($save->created_by);
            $save->updated_by = \App\Helpers\HashId::encode($save->updated_by);
            return $save;            
        }

    }

    public function getWOTask($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $wo = (new WoTask)->newQuery();

        $wo->where('trx_wo_id', $id);

        $wo->with(["plant", "classification"]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $wo->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('plant')) {
            $wo->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('classification')) {
            $wo->whereIn('classification_id', request()->input('classification'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $wo->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $wo->orderBy('operation_no', 'asc');
        }

        if (request()->has('per_page')) {
            $wo = $wo->paginate(request()->input('per_page'))
                ->appends(Input::except('page'))->withPath('')->toArray();
        } else {
            $wo = $wo->paginate(20)->appends(Input::except('page'))
                ->withPath('')->toArray();
        }

        return $wo;
    }

    public function getWOTaskHash($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        $wo = (new WoTask)->newQuery();

        $wo->with(["plant", "classification"]);

        $wo->where('trx_wo_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $wo->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(address)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('plant')) {
            $wo->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('classification')) {
            $wo->whereIn('classification_id', request()->input('classification'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'plant':
                    $wo->leftJoin('plants','wo_tasks.plant_id','=','plants.id');
                    $wo->select('wo_tasks.*');
                    $wo->orderBy('plants.code',$sort_order);
                break;

                case 'classification':
                    $wo->leftJoin('classification_materials','wo_tasks.classification_id','=','classification_materials.id');
                    $wo->select('wo_tasks.*');
                    $wo->orderBy('classification_materials.name',$sort_order);
                break;
                
                default:
                    $task_list->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $wo->orderBy('operation_no', 'asc');
        }

        if (request()->has('per_page')) {
            $wo = $wo->paginate(request()->input('per_page'))
                ->appends(Input::except('page'))->withPath('')->toArray();
        } else {
            $wo = $wo->paginate(20)->appends(Input::except('page'))
                ->withPath('')->toArray();
        }

        foreach($wo['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $wo['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 422);
            }
        }

        return $wo;
    }

    public function showWoTask($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-view']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $wo = WoTask::with(["assetWoTask"])->findOrFail($id);

        return $wo;
    }

    public function updateWoTask($id, Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate($request, [
            'operation_no' => 'required|integer',
            'description' => 'required|max:30',
            'estimated_work' => 'required|integer',
            'estimated_unit' => 'required|integer|min:1|max:3', //1 = menit, 2 = jam, 3 = hari
            'plant_id' => 'nullable|exists:plants,id',
            'classification_material_id' => 'nullable|exists:classification_materials,id',
        ]);

        $updateWo = WoTask::findOrFail($id);

        $save = $updateWo->update([
            'operation_no' => $request->operation_no,
            'description' => $request->description,
            'estimated' => $request->estimated_work,
            'estimated_unit' => $request->estimated_unit,
            'plant_id' => $request->plant_id,
            'classification_id' => $request->classification_material_id,
            'updated_by' => Auth::user()->id,
        ]);

        if ($save) {
            $updateWo->id_hash = \App\Helpers\HashId::encode($updateWo->id);

            return $updateWo;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }

    }

    public function destroyWoTask($id)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = WoTask::findOrFail($id);

        $delete->update([
            'updated_by' => Auth::user()->id
        ]);
        $delete = $delete->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }

    }

    public function multipleDeleteWoTask(Request $request)
    {
        Auth::user()->cekRoleModules(['corrective-maintenance-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = \App\Helpers\HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:wo_tasks,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = WoTask::findOrFail($id);
                $delete->update([
                    'updated_by' => Auth::user()->id
                ]);
                $delete = $delete->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }

    }


    

}
