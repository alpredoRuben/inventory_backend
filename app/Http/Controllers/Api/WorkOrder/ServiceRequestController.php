<?php

namespace App\Http\Controllers\Api\WorkOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\ServiceRequest;
use App\Models\Plant;

class ServiceRequestController extends Controller
{
    public function index()
    {   
        Auth::user()->cekRoleModules(['service-request-view']);

        $serv_req = (new ServiceRequest)->newQuery()->with('plant');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            if ($q != "") {
                $serv_req->where(function($query) use ($q) {
                    $query->orWhere(DB::raw("LOWER(service_id)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(service_request)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(prob_description)"), 'LIKE', "%".$q."%");
                });
            }
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $serv_req->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $serv_req->orderBy('created_at', 'asc');
        }

        $serv_req = $serv_req->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $serv_req->toArray();

    }
    
    public function list()
    {
        Auth::user()->cekRoleModules(['service-request-view']);

        $serv_req = (new ServiceRequest)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            if ($q != "") {
                $serv_req->where(function($query) use ($q) {
                    $query->orWhere(DB::raw("LOWER(service_id)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(service_request)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(prob_description)"), 'LIKE', "%".$q."%");
                });

                // search work_order
                $serv_req->orWhereHas('work_order', function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                })
                ->with(['work_order' => function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                }]);

                // search plant
                $serv_req->orWhereHas('plant', function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                })
                ->with(['plant' => function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                }]);
            }
        }

        $serv_req->with(['plant', 'work_order', 'work_order.wo_status']);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'work_order':
                    $serv_req->leftJoin('trx_wos','service_requests.id','=','trx_wos.service_id');
                    $serv_req->select('service_requests.*');
                    $serv_req->orderBy('trx_wos.code',$sort_order);
                break;

                case 'plant':
                    $serv_req->leftJoin('plants','service_requests.plant_id','=','plants.id');
                    $serv_req->select('service_requests.*');
                    $serv_req->orderBy('plants.code',$sort_order);
                break;
                
                default:
                    $serv_req->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $serv_req->orderBy('created_at', 'asc');
        }

        $serv_req = $serv_req->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();

        foreach($serv_req['data'] as $k => $v) {
            try {
                $v['id'] = \App\Helpers\HashId::encode($v['id']);
                $serv_req['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 401);
            }
        }

        return $serv_req;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['service-request-create']);

        $this->validate(request(), [
            'description' => 'required|max:30',
            'plant_id' => 'required|exists:plants,id',
            'request_detail' => 'required|max:300',
        ]);

        $plant = Plant::findOrFail($request->plant_id);

        $service = ServiceRequest::whereDate('created_at', Carbon::today())->orderBy('service_id', 'desc')->first();

        if (!$service) {
            $service_id = 'SR' . date('Ymd') . '001';
        } else {
            $latest_code = $service->service_id;
            $len = strlen($latest_code);
            $temp = substr($latest_code, $len - 3, $len);
            $temp = $temp + 1;
            $service_id = 'SR' . date('Ymd') . sprintf("%03d", $temp);
        }

        $save = ServiceRequest::create([
            'service_id' => $service_id,
            'plant_id' => $request->plant_id,
            'prob_description' => $request->description,
            'service_request' => $request->request_detail,
            'created_by'    => Auth::user()->id,
            'updated_by'    => Auth::user()->id
        ]);

        $save->id_hash = \App\Helpers\HashId::encode($save->id);
        $save->created_by = \App\Helpers\HashId::encode($save->created_by);
        $save->updated_by = \App\Helpers\HashId::encode($save->updated_by);
        // $save->id = null;
        return $save;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['service-request-view']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $serv_req = ServiceRequest::with('plant')->findOrFail($id);

        $serv_req->id_hash = \App\Helpers\HashId::encode($serv_req->id);

        return $serv_req;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['service-request-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $serv_req = ServiceRequest::findOrFail($id);

        $this->validate(request(), [
            'description' => 'required|max:30',
            'request_detail' => 'required|max:300',
            'plant_id' => 'required|exists:plants,id',
        ]);

        $save = $serv_req->update([
            'prob_description' => $request->description,
            'plant_id' => $request->plant_id,
            'service_request' => $request->request_detail,
            'created_by'    => Auth::user()->id,
            'updated_by'    => Auth::user()->id
        ]);

        if ($save) {
            $serv_req->id_hash = \App\Helpers\HashId::encode($serv_req->id);

            return $serv_req;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->cekRoleModules(['service-request-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $delete = ServiceRequest::findOrFail($id);

        $delete->update([
            'updated_by' => Auth::user()->id
        ]);
        $delete = $delete->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete(Request $request)
    {
        Auth::user()->cekRoleModules(['service-request-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = \App\Helpers\HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:service_requests,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = ServiceRequest::findOrFail($id);
                $delete->update([
                    'updated_by' => Auth::user()->id
                ]);
                $delete = $delete->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }

    }
}
