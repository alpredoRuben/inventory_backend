<?php

namespace App\Http\Controllers\Api\WorkOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

use App\Models\TrxTaskList;
use App\Models\Material;

class TrxTaskListController extends Controller
{

    public function index()
    {   
        Auth::user()->cekRoleModules(['task-list-view']);

        $task_list = (new TrxTaskList)->newQuery();

        $task_list->with('material')->with('material.classification')->withCount('item_list');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $task_list->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(tasklist_id)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
            })->orWhereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $task_list->orderBy('material_id', $sort_order);
                break;

                case 'material_classification':
                    $task_list->join('materials','trx_task_lists.material_id','=','materials.id');
                    $task_list->join('classification_materials','materials.classification_material_id','=','classification_materials.id');
                    $task_list->orderBy('classification_materials.name',$sort_order);
                break;
                
                default:
                    $task_list->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $task_list->orderBy('tasklist_id', 'asc');
        }

        $task_list = $task_list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $task_list->toArray();

    }
    
    public function list()
    {
        Auth::user()->cekRoleModules(['task-list-view']);

        $task_list = (new TrxTaskList)->newQuery();

        $task_list->with('material')->with('material.classification')->withCount('item_list');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $task_list->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(trx_task_lists.tasklist_id)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(trx_task_lists.name)"), 'LIKE', "%".$q."%");
            })->orWhereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(materials.code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(materials.description)"), 'LIKE', "%".$q."%");
                $query->orWhereHas('classification', function($query) use ($q) {
                    $query->where(DB::raw("LOWER(classification_materials.name)"), 'LIKE', "%".$q."%");
                });
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $task_list->join('materials','trx_task_lists.material_id','=','materials.id');
                    $task_list->select('trx_task_lists.*');
                    $task_list->orderBy('materials.code',$sort_order);
                break;

                case 'material_classification':
                    $task_list->join('materials','trx_task_lists.material_id','=','materials.id');
                    $task_list->leftJoin('classification_materials','materials.classification_material_id','=','classification_materials.id');
                    $task_list->select('trx_task_lists.*');
                    $task_list->orderBy('classification_materials.name',$sort_order);
                break;
                
                default:
                    $task_list->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $task_list->orderBy('tasklist_id', 'asc');
        }

        $task_list = $task_list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))->toArray();

        foreach($task_list['data'] as $k => $v) {
            try {
                $v['id'] = \App\Helpers\HashId::encode($v['id']);
                $task_list['data'][$k] = $v;
            }
            catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 401);
            }
        }

        return $task_list;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['task-list-create']);

        $this->validate(request(), [
            'name' => 'required|max:30',
            'material_id' => 'required|exists:materials,id',
        ]);

        $material = Material::findOrFail($request->material_id);
        if (!$material) {
            return response()->json([
                'message' => 'Material not found',
            ], 401);
        }

        $task = TrxTaskList::where('material_id', $request->material_id)->orderBy('tasklist_id', 'desc')->first();

        if (!$task) {
            $tasklist_id = $material->code . '-' . '001';
        } else {
            $latest_code = $task->tasklist_id;
            $len = strlen($latest_code);
            $temp = substr($latest_code, $len - 3, $len);
            $temp = $temp + 1;
            $tasklist_id = $material->code . '-' . sprintf("%03d", $temp);
        }

        $task_list = TrxTaskList::withTrashed()->whereRaw('LOWER(tasklist_id) = ?', strtolower($tasklist_id))->first();

        if ($task_list) {

            if($task_list->deleted_at){
                $task_list->restore();
                $save = $task_list->update([
                    'name' => $request->name,
                    'material_id' => $request->material_id,
                    'updated_by'    => Auth::user()->id
                ]);

                $task_list->id_hash = \App\Helpers\HashId::encode($task_list->id);
                $task_list->material_id = \App\Helpers\HashId::encode($task_list->material_id);
                $task_list->createdBy->id_hash = \App\Helpers\HashId::encode($task_list->createdBy->id);
                $task_list->updatedBy->id_hash = \App\Helpers\HashId::encode($task_list->updatedBy->id);
                // $task_list->createdBy->id = null;
                // $task_list->updatedBy->id = null;
                // $task_list->id = null;

                return $task_list;
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Task List ID already taken']
                ]
            ],422);
        } else {
            $save = TrxTaskList::create([
                'tasklist_id' => $tasklist_id,
                'name' => $request->name,
                'material_id' => $request->material_id,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);

            $save->id_hash = \App\Helpers\HashId::encode($save->id);
            $save->material_id = \App\Helpers\HashId::encode($save->material_id);
            $save->created_by = \App\Helpers\HashId::encode($save->created_by);
            $save->updated_by = \App\Helpers\HashId::encode($save->updated_by);
            // $save->id = null;
            return $save;
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['task-list-view']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $task_list = TrxTaskList::with(['material'])->with('material.classification')
        ->findOrFail($id);

        $task_list->id_hash = \App\Helpers\HashId::encode($task_list->id);
        $task_list->material_id_hash = \App\Helpers\HashId::encode($task_list->material_id);

        return $task_list;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $task_list = TrxTaskList::findOrFail($id);

        $this->validate(request(), [
            'name' => 'required|max:30',
            'material_id' => 'required|exists:materials,id',
        ]);

        $save = $task_list->update([
            'name' => $request->name,
            'material_id' => $request->material_id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            $task_list->id_hash = \App\Helpers\HashId::encode($task_list->id);
            // $task_list->createdBy->id_hash = \App\Helpers\HashId::encode($task_list->createdBy->id);
            // $task_list->updatedBy->id_hash = \App\Helpers\HashId::encode($task_list->updatedBy->id);
            // $task_list->id = null;
            // $task_list->createdBy->id = null;
            // $task_list->updatedBy->id = null;

            return $task_list;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $id = \App\Helpers\HashId::decode($id);
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $delete = TrxTaskList::findOrFail($id);

        $delete->update([
            'updated_by' => Auth::user()->id
        ]);
        $delete = $delete->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 401);
        }
    }

    public function multipleDelete(Request $request)
    {
        Auth::user()->cekRoleModules(['task-list-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = \App\Helpers\HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 401);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:trx_task_lists,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = TrxTaskList::findOrFail($id);
                $delete->update([
                    'updated_by' => Auth::user()->id
                ]);
                $delete = $delete->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 400);
        }

    }
}
