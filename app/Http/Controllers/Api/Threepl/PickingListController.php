<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\Storage as MasterStorage;
use App\Models\Reservation;
use App\Models\ReservationApproval;
use App\Models\DeliveryApproval;
use App\Models\DeliveryHeader;
use App\Models\DeliveryItem;
use App\Models\DeliverySerial;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\MovementHistory;
use App\Models\MaterialStorage;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;

class PickingListController extends Controller
{
	/**
     * Get New Code For Delivery Order.
     *
     * @return new DO Code
     */
    public function getMaterialDoc()
    {
        // get reservation code 
        $code = TransactionCode::where('code', 'D')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'D';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'D';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'D';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    /**
     * Get New Code For Purchase Requisition.
     *
     * @return new PR Code
     */
    public function getPrCode()
    {
        // get reservation code 
        $code = TransactionCode::where('code', 'A')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'A';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    /**
     * Get New Code For Purchase Order.
     *
     * @return new PO Code
     */
    public function getPoCode()
    {
        // get reservation code 
        $code = TransactionCode::where('code', 'B')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'B';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    /**
     * Show the Approved Reservation.
     *
     * @return \App\Models\Reservation
     */
    public function index()
    {
    	Auth::user()->cekRoleModules(['picking-list-view']);

        $reservation = (new Reservation)->newQuery();

        $reservation->with([
        	'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
        	'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy',
            'procurement_group'
        ]);

        $reservation->with('material_plant');
        $reservation->with('material.group_material');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $reservation->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_header)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_item)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(unit_cost)"), 'LIKE', "%".$q."%");
            });
        }

        // filter type 1 = Replenishment, 2 = Purchase
        if (request()->has('reservation_type')) {
            $reservation->whereIn('reservation_type', request()->input('reservation_type'));
        }

        // filter plant
        if (request()->has('plant')) {
            $reservation->whereIn('inout_plant_id', request()->input('plant'));
        }

        // filter storage
        if (request()->has('storage')) {
            $reservation->whereIn('inout_storage_id', request()->input('storage'));
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $reservation->whereIn('inout_plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $reservation->whereIn('inout_storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material')) {
            $reservation->whereIn('material_id', request()->input('material'));
        }

        // filter cost_center
        if (request()->has('cost_center')) {
            $reservation->whereIn('cost_center_id', request()->input('cost_center'));
        }

        // filter reservation
        if (request()->has('reservation_code')) {
            $reservation_code = request()->input('reservation_code');
            $reservation->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".strtolower($reservation_code)."%");
        }

        // filter purchase type
        if (request()->has('purchase_type')) {
            $reservation->whereIn('proc_type', request()->input('purchase_type'));
        }

        // filter requirement_date
        if (request()->has('requirement_date')) {
            $reservation->whereBetween('requirement_date', [request()->requirement_date[0], request()->requirement_date[1]]);
        }

        $reservation->whereIn('status', [2, 5]);

        // filter status
        /* status
        * 2 = Allocation
        * 5 = Picking
        */
        if (request()->has('status')) {
            $reservation->whereIn('status', request()->input('status'));
        }

        $reservation->where('document_type', 'R');

        /* fullfilment status
        * 0 = Open
        * 1 = Completed
        */
        $reservation->whereNotNull('material_id');
        $reservation->whereNull('fulfillment_status');
        $reservation->orWhere('fulfillment_status', 0);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'plant':
                    $reservation->orderBy('plant_id', $sort_order);
                    break;
                case 'storage':
                    $reservation->orderBy('storage_id', $sort_order);
                    break;
                default:
                    $reservation->orderBy('id', 'desc');
            }
        } else {
            $reservation->orderBy('id', 'desc');
        }

        $reservation = $reservation->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $reservation->transform(function($data) {
            // find DO status open/picking
            $do_header = DeliveryHeader::whereIn('status', [0,1])->pluck('id');

            // find DO item by reservation if DO status open/picking
            $do_item = DeliveryItem::where('reservation_id', $data->id)
                ->whereIn('delivery_header_id', $do_header)
                ->orderBy('id', 'asc')
                ->first();

            // get allocated qty, DO
            $data->qty_allocated = $do_item ? (float)$do_item->delivery_qty : null;
            $data->do_number = $do_item ? DeliveryHeader::find($do_item->delivery_header_id)->delivery_code : null;
            $data->do_item = $do_item ? $do_item->delivery_item : null;
            $data->do_id = $do_item ? $do_item->delivery_header_id : null;

            // find stock material from inout storage
            $stock = StockMain::where('material_id', $data->material_id)
                ->where('storage_id', $data->inout_storage_id)
                ->first();

            $data->stock_on_hand = $stock ? $stock->qty_unrestricted : 0;

            return $data;
        });

        return $reservation;
    }

    /**
     * Print PDF picking list
     *
     * @param  string  $do_code
     * @return \PDF
     */
    public function pdf($id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-pdf']);

        $do = DeliveryHeader::find($id);

        if (!$do) {
            return response()->json([
                'message' => 'Delivery Orders not Found'
            ], 404);
        }

        if ($do->status != 1) {
            return response()->json([
                'message' => 'Delivery Orders status must picking'
            ], 422);
        }

        $do->item->map(function ($item){
            $material_storage = MaterialStorage::where('storage_id', $item->storage_id)
                ->where('material_id', $item->material_id)
                ->first();

            if ($material_storage) {
                $item->storage_bin = $material_storage->storage_bin;
            } else {
                $item->storage_bin = '';
            }

            return $item;
        });

        $pdf = PDF::loadview('pdf.picking', compact('do'))->setPaper('a4', 'potrait');
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 10, array(0, 0, 0));
        
        return $pdf->download('Picking_delivery_Orders.pdf');
    }

    /**
     * Convert Approved Reservation as Delivery Order For Internal Purchase Type
     *
     * @param  Request  $request
     * @return \App\Models\Delivery Headers
     */
    public function convertAsDo(Request $request)
    {
        Auth::user()->cekRoleModules(['picking-list-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservations,id'
        ]);

        $reservation = Reservation::whereIn('id', $request->id)->get();

        // cek selected reservation are in one inout_plant_id
        $plant1 = $reservation[0]['inout_plant_id'];
        $cekplant = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            if ($reservation[$i]['inout_plant_id'] !== $plant1)
            {
                $cekplant = false;
            }
        }

        if (!$cekplant) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are Not in same supply plant',
            ], 422);
        }

        // cek selected reservation are in one inout_storage_id
        $storage1 = $reservation[0]['inout_storage_id'];
        $cekstorage = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            if ($reservation[$i]['inout_storage_id'] !== $storage1)
            {
                $cekstorage = false;
            }
        }

        if (!$cekstorage) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are Not in same supply storage',
            ], 422);
        }

        // cek selected reservation are in one requirement_date
        $requirement_date1 = Carbon::parse($reservation[0]['requirement_date'])->format('d M Y');
        $cekrequirement_date = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            $date = Carbon::parse($reservation[$i]['requirement_date'])->format('d M Y');
            if ($date !== $requirement_date1)
            {
                $cekrequirement_date = false;
            }
        }

        if (!$cekrequirement_date) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are not in one requirement date',
            ], 422);
        }

        /* cek proc type
        * 0 = internal
        * 1 = external
        */
        $proc_type1 = $reservation[0]['proc_type'];
        $cekrproc_type = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            $mrp = $reservation[$i]['proc_type'];
            if ($mrp !== $proc_type1 | $mrp)
            {
                $cekrproc_type = false;
            }
        }

        if (!$cekrproc_type) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are not in internal procurement type',
            ], 422);
        }

        // all reservation
        $all_res = Reservation::whereIn('id', $request->id)->get();
        
        $storage_grouping = $all_res->groupBy(['storage_id', 'material_id']);
        $material_grouping = $all_res->map(function ($data){
            // delivery qty = Reservasi Qty - DO reservasi
            $reservasi = Reservation::find($data->id);
            $qty_reservasi = $reservasi->quantity_base_unit;
            $do_reservation = $reservasi->delivery_orders->sum('delivery_qty');

            $data->quantity_base_unit = (int)($qty_reservasi - $do_reservation);

            return $data;
        })->groupBy(['material_id']);

        try {
            DB::beginTransaction();
            
            // insert DO header
            $h = [];
            foreach ($storage_grouping as $key => $value) {
                // get new do code
                $docode = $this->getMaterialDoc();

                // get storage tujuan
                $res_h = Reservation::whereIn('id', $request->id)
                    ->where('storage_id', $key)
                    ->first();
                
                // create do header
                $header = DeliveryHeader::create([
                    'delivery_code' => $docode,
                    'delivery_types' => 0,//Stock Transfer Order
                    'sales_org' => 0, //0. Internal
                    'planned_gi' => $res_h->requirement_date,//sesuai dgn tgl requirement date reservation (bisa diedit di form edit DO)
                    'delivery_date' => $res_h->requirement_date,//sesuai dgn tgl requirement date reservation
                    'picking_date' => now(),//sesuai dgn tgl skrg
                    // 'customer_id' => null,//blank
                    'dlv_to_plant_id' => $res_h->plant_id,//plant tujuan
                    'dlv_to_storage_id' => $res_h->storage_id,//storage tujuan
                    'shipping_point_location_id' => $res_h->inout_storage_id,//lokasi storage pengirim
                    // 'delivery_note' => null,//kosong dulu, nanti diisi pas edit DO
                    // 'transport_type_id' => null,//ini perlu CRUD contoh Truck, Van, Train, Tiki, dkk (diisi dgn edit DO)
                    // 'transport_id' => null,//no plat truck (edit di DO)
                    // 'courier_name' => null,//edit di DO
                    'status' => 0,// 0. Open, 1. Picking/Packing, 2. Sent, 3. Received (perubahas status ini agar di rekam di history)
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $h[] = $header->id;

                // update last reservation code
                $code = TransactionCode::where('code', 'D')->first();

                $code->update(['lastcode' => $docode]);
            }

            // insert do item use auto allocation
            foreach ($material_grouping as $key => $value) {
                // count total storage yg berbeda
                $total_storage = count($value->groupBy('storage_id'));
                $item_by_storage = $value->groupBy('storage_id')->values();

                // get Stock
                $st = StockMain::where('material_id', $key)
                    ->where('storage_id', $value[0]->inout_storage_id)
                    ->first();

                // get material
                $mt = Material::find($key);

                // strg supply
                $strg_supply = MasterStorage::find($value[0]->inout_storage_id);

                if (!$st) {
                    return response()->json([
                        'message' => 'Stock Material '.$mt->description.' in storage '.$strg_supply->description.' not sufficient'
                    ], 422);
                }

                $st_unres = $st->qty_unrestricted;

                // get total do qty status not sent
                $do_not_sent = DeliveryHeader::whereIn('status', [0,1])->pluck('id');

                $st_do_not_sent = DeliveryItem::whereIn('delivery_header_id', $do_not_sent)
                    ->where('material_id', $key)
                    ->get();

                $st_do = $st_do_not_sent->sum('delivery_qty');

                // get total stock can allocated for do (qty unres - total do not sent yet)
                $stock_unres_do = $st_unres - $st_do;

                // cek if stock can alocated < total storage
                if ($stock_unres_do < $total_storage) {
                    return response()->json([
                        'message' => 'Stock material '.$mt->description.' is not sufficient'
                    ], 422);
                }

                // pembagian pertama total stock / total storage
                $hasil_bagi1 = (int)($stock_unres_do / $total_storage);
                $bagi1 = (int)($hasil_bagi1 * $total_storage);
                $sisa_bagi1 = (int)($stock_unres_do - $bagi1);

                for ($st=0; $st < $total_storage; $st++) { 
                    $total_item_by_storage[$st] = $item_by_storage[$st]->count();

                    if ($total_item_by_storage[$st] <= 1) {
                        foreach ($item_by_storage[$st] as $j => $item) {
                            if ($item->quantity_base_unit < $hasil_bagi1) {
                                $qty = $item->quantity_base_unit;
                                $sisa_bagi1 += (int)($hasil_bagi1 - $qty);
                            } else {
                                $qty = (int)$hasil_bagi1;
                            }

                            // get item no
                            $do_head = DeliveryHeader::whereIn('id', $h)
                                ->where('dlv_to_storage_id', $item->storage_id)
                                ->first();

                            $item_no = $do_head->item->count() + 1;

                            // get reservation
                            $reservation = Reservation::find($item->id);

                            // insert do item
                            $item = DeliveryItem::create([
                                'delivery_header_id' => $do_head->id,
                                'delivery_item' => $item_no,
                                'sequence' => 1,
                                'plant_id' => $reservation->inout_plant_id,
                                'storage_id' => $reservation->inout_storage_id,
                                'reservation_id' => $reservation->id,
                                'material_id' => $reservation->material_id,
                                'uom_id' => $reservation->uom_base_id,
                                'delivery_qty' => $qty, //calculated allocation
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // update converted reservation status
                            $reservation->update([
                                'status' => 6, //DO created
                            ]);

                            Reservation::where('reservation_code', $reservation->reservation_code)
                                ->where('reservation_item', null)
                                ->first()
                                ->update([
                                    'status' => 6, //DO created
                                ]);
                        }
                    } else {
                        $qty_total = 0;
                        foreach ($item_by_storage[$st] as $i => $l) {
                            $qty_total += $l->quantity_base_unit;
                        }

                        $total_hasil_bagi2 = [];
                        foreach ($item_by_storage[$st] as $a => $b) {
                            // pembagian ke2
                            $hasil_bagi2 = (int)(($b->quantity_base_unit / $qty_total) * $hasil_bagi1);

                            $total_hasil_bagi2[] += $hasil_bagi2;
                        }

                        if ((array_sum($total_hasil_bagi2)) == 0) {
                            return response()->json([
                                'message' => 'Stock material '.$mt->description.' is not sufficient'
                            ], 422);
                        }                      

                        $sisa_bagi1 += (int)($hasil_bagi1 - array_sum($total_hasil_bagi2));
                        
                        // step 3
                        $hasil_bagi3 = (int)($sisa_bagi1 / $total_item_by_storage[$st]);
                        $bagi3 = (int)($hasil_bagi3 * $total_item_by_storage[$st]);
                        $sisa_bagi3 = (int)($sisa_bagi1 - $bagi3);

                        $hasil = [];
                        foreach ($total_hasil_bagi2 as $c => $d) {
                            $hasil[] = $d + $hasil_bagi3;
                        }

                        $sisa_bagi3 = 0;

                        $sisa_bagi3 += (int)($sisa_bagi1 - $bagi3);

                        if ($sisa_bagi3 < $total_item_by_storage[$st]) {
                            foreach ($item_by_storage[$st] as $j => $item) {
                                $qty = $hasil[$j];

                                // get item no
                                $do_head = DeliveryHeader::whereIn('id', $h)
                                    ->where('dlv_to_storage_id', $item->storage_id)
                                    ->first();

                                $item_no = $do_head->item->count() + 1;

                                // get reservation
                                $reservation = Reservation::find($item->id);

                                // insert do item
                                $item = DeliveryItem::create([
                                    'delivery_header_id' => $do_head->id,
                                    'delivery_item' => $item_no,
                                    'sequence' => 1,
                                    'plant_id' => $reservation->inout_plant_id,
                                    'storage_id' => $reservation->inout_storage_id,
                                    'reservation_id' => $reservation->id,
                                    'material_id' => $reservation->material_id,
                                    'uom_id' => $reservation->uom_base_id,
                                    'delivery_qty' => $qty, //calculated allocation
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // update converted reservation status
                                $reservation->update([
                                    'status' => 6, //DO created
                                ]);

                                Reservation::where('reservation_code', $reservation->reservation_code)
                                    ->where('reservation_item', null)
                                    ->first()
                                    ->update([
                                        'status' => 6, //DO created
                                    ]);
                            }
                        }
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'success converting Reservation as DO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while converting Reservation as DO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Convert Approved Reservation as Purchase Requisition For External Purchase Type
     *
     * @param  Request  $request
     * @return \App\Models\Reservation
     */
    public function convertAsPr(Request $request)
    {
        Auth::user()->cekRoleModules(['picking-list-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservations,id'
        ]);

        $reservation = Reservation::whereIn('id', $request->id)->get();

        // cek selected reservation are in one plant_id
        $plant1 = $reservation[0]['plant_id'];
        $cekplant = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            if ($reservation[$i]['plant_id'] !== $plant1)
            {
                $cekplant = false;
            }
        }

        if (!$cekplant) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are Not in same plant',
            ], 422);
        }

        // cek selected reservation are in one storage_id
        $storage1 = $reservation[0]['storage_id'];
        $cekstorage = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            if ($reservation[$i]['storage_id'] !== $storage1)
            {
                $cekstorage = false;
            }
        }

        if (!$cekstorage) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are Not in same storage',
            ], 422);
        }

        // cek selected reservation are in one requirement_date
        $requirement_date1 = Carbon::parse($reservation[0]['requirement_date'])->format('d M Y');
        $cekrequirement_date = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            $date = Carbon::parse($reservation[$i]['requirement_date'])->format('d M Y');
            if ($date !== $requirement_date1)
            {
                $cekrequirement_date = false;
            }
        }

        if (!$cekrequirement_date) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are not in one requirement date',
            ], 422);
        }

        /* cek proc type
        * 0 = internal
        * 1 = external
        */
        $proc_type1 = $reservation[0]['proc_type'];
        $cekrproc_type = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            $mrp = $reservation[$i]['proc_type'];
            if ($mrp !== $proc_type1 | !$mrp)
            {
                $cekrproc_type = false;
            }
        }

        if (!$cekrproc_type) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are not in external procurement type',
            ], 422);
        }

        /* cek proc type
        * 0 = internal
        * 1 = external
        */
        $proc_group_id1 = $reservation[0]['proc_group_id'];
        $cekproc_group_id = true;

        for ($i = 0; $i < count($reservation); $i++)
        {
            $proc_group_id = $reservation[$i]['proc_group_id'];
            if ($proc_group_id !== $proc_group_id1)
            {
                $cekproc_group_id = false;
            }
        }

        if (!$cekproc_group_id) {
            return response()->json([
                'message'   => 'Data invalid, the selected reservation are not in one procurement group',
            ], 422);
        }

        // get new pr code
        $prcode = $this->getPrCode();

        // choose first selected material for header
        $first_reservation = Reservation::whereIn('id', $request->id)->first();

        try {
            DB::beginTransaction();

            // create pr header
            $reservation_header = new Reservation;
            $reservation_header->reservation_code = $prcode;
            $reservation_header->reservation_type = 3;
            $reservation_header->document_type = 'A';
            $reservation_header->plant_id = $first_reservation->plant_id;
            $reservation_header->storage_id = $first_reservation->storage_id;
            $reservation_header->requirement_date = $first_reservation->requirement_date;
            $reservation_header->note_header = $first_reservation->note_header;
            $reservation_header->status = 0;
            $reservation_header->created_by = Auth::user()->id;
            $reservation_header->updated_by = Auth::user()->id;
            $reservation_header->save();

            // insert reservation as pr item
            $item_no = 1;
            foreach ($request->id as $id) {
                /** Reservation Status
                *0 = draft
                *1 = pending_approval
                *2 = approved
                *3 = rejected
                *4 = canceled
                *5 = processed
                **/
                $reservation_item = Reservation::find($id);

                // create pr item
                $pr = new Reservation;
                $pr->reservation_code = $reservation_header->reservation_code;
                $pr->reservation_item = $item_no++;
                $pr->reservation_type = $reservation_header->reservation_type;
                $pr->document_type = $reservation_header->document_type;
                $pr->note_header = $reservation_header->note_header;
                $pr->status = $reservation_header->status;
                $pr->plant_id = $reservation_header->plant_id;
                $pr->storage_id = $reservation_header->storage_id;
                $pr->item_type = $reservation_header->item_type;
                $pr->material_id = $reservation_item->material_id;
                $pr->material_short_text = $reservation_item->material_short_text;
                $pr->note_item = $reservation_item->note_item;
                $pr->batch = $reservation_item->batch;
                $pr->requirement_date = $reservation_header->requirement_date;
                $pr->quantity = $reservation_item->quantity;
                $pr->uom_id = $reservation_item->uom_id;
                $pr->quantity_base_unit = $reservation_item->quantity_base_unit;
                $pr->uom_base_id = $reservation_item->uom_base_id;
                $pr->group_material_id = $reservation_item->group_material_id;
                $pr->account_id = $reservation_item->account_id;
                $pr->unit_cost = $reservation_item->unit_cost;
                $pr->inout_plant_id = $reservation_item->inout_plant_id;
                $pr->inout_storage_id = $reservation_item->inout_storage_id;
                $pr->material_plant_id = $reservation_item->material_plant_id;
                $pr->proc_type = $reservation_item->proc_type;
                $pr->proc_group_id = $reservation_item->proc_group_id;
                $pr->created_by = Auth::user()->id;
                $pr->updated_by = Auth::user()->id;
                $pr->save();

                // update converted reservation status & pr number, pr item
                $reservation_item->update([
                    'purc_req_number' => $reservation_header->reservation_code,
                    'purc_req_item' => $pr->reservation_item,
                    'status' => 7, //PR created
                    'fulfillment_status' => 1,//0.Open, 1.Completed
                    'updated_by' => Auth::user()->id
                ]);

                Reservation::where('reservation_code', $reservation_item->reservation_code)
                    ->where('reservation_item', null)
                    ->first()
                    ->update([
                        'status' => 7, //PR created
                    ]);
            }

            // update last pr code
            $code = TransactionCode::where('code', 'A')->first();

            $code->update(['lastcode' => $prcode]);

            DB::commit();

            $reservation_saved = Reservation::with([
                'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
            ])
            ->where('reservation_code', $reservation_header->reservation_code)
            ->whereNull('material_short_text')
            ->first();

            $reservation_item_saved = Reservation::with([
                'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
            ])
            ->with('material.images')
            ->with('material.base_uom')
            ->whereNotNull('material_short_text')
            ->where('reservation_code', $reservation_header->reservation_code)
            ->get();

            $saveitem = [
                'reservation' => $reservation_saved,
                'reservation_item' => $reservation_item_saved
            ];

            return $saveitem;
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while converting reservation as PR',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function editAllocatedQty(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-update']);

        $this->validate(request(), [
            'qty' => 'required|numeric|min:1',
        ]);

        $reservation = Reservation::find($id);

        if (!$reservation) {
            return response()->json(['message' => 'Reservation not found'], 404);
        }

        if ($request->qty > $reservation->quantity_base_unit) {
            return response()->json([
                'message'   => 'Qty cant greather than '.$reservation->quantity_base_unit.' (Reservation Qty)'
            ], 422);
        }

        // find do header status open / picking
        $do_header = DeliveryHeader::whereIn('status', [0,1])->pluck('id');

        // find do item based on header & reservation
        $do_item = DeliveryItem::where('reservation_id', $id)
            ->whereIn('delivery_header_id', $do_header)
            ->orderBy('id', 'asc')
            ->first();

        if (!$do_item) {
            return response()->json([
                'message' => 'Reservation must convert to DO for allocated qty'
            ], 404);
        }

        // get on hand stock
        $st = StockMain::where('material_id', $reservation->material_id)
            ->where('storage_id', $reservation->inout_storage_id)
            ->first();

        // get material
        $mt = Material::find($reservation->material_id);

        // strg supply
        $strg_supply = MasterStorage::find($reservation->inout_storage_id);

        if (!$st) {
            return response()->json([
                'message' => 'Stock Material '.$mt->description.' in storage '.$strg_supply->description.' not sufficient'
            ], 422);
        }

        $st_unres = $st->qty_unrestricted;

        // get total do qty status not sent
        $do_not_sent = DeliveryHeader::whereIn('status', [0,1])->pluck('id');

        $st_do_not_sent = DeliveryItem::whereIn('delivery_header_id', $do_not_sent)
            ->where('material_id', $reservation->material_id)
            ->where('id', '!=', $do_item->id)
            ->get();

        $st_do = $st_do_not_sent->sum('delivery_qty');

        // get total stock can allocated for do (qty unres - total do not sent yet)
        $stock_unres_do = $st_unres - $st_do;

        // new sisa stock allocated (qty unrest do - qty input)
        $sisa_allo = $stock_unres_do - $request->qty;

        // cek if stock can allocated < 0
        if ($sisa_allo < 0) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'qty'  => ['Stock Material '.$mt->description.' in storage '.$strg_supply->description.' not sufficient. stock can be allocated just '.$stock_unres_do.'']
                ]
            ], 422);
        }

        // update
        $do_item->update([
            'delivery_qty' => $request->qty,
            'updated_by' => Auth::user()->id
        ]);

        return response()->json(['message' => 'success update allocated DO qty'], 200);
    }

    public function allocateBatch($id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-update']);

        $header = DeliveryHeader::find($id);

        if (!$header) {
            return response()->json([
                'message'   => 'Data Not Found',
            ], 404);
        }

        if ($header->status == 2) {
            return response()->json([
                'message' => 'Delivery Orders already Sent, Cant Allocate anymore'
            ], 422);
        }

        if ($header->status == 3) {
            return response()->json([
                'message' => 'Delivery Orders already Received, Cant Allocate anymore'
            ], 422);
        }

        // validate if already split item cant allocate annymore
        foreach ($header->item as $current_key => $current_array) {
            foreach ($header->item as $search_key => $search_array) {
                if ($search_array['material_id'] == $current_array['material_id']) {
                    if ($search_key != $current_key) {
                        return response()->json([
                            'message' => 'Delivery Orders already Allocate, Cant Allocate anymore'
                        ], 422);
                    }
                }
            }
        }

        try {
            DB::beginTransaction();

            foreach ($header->item as $item) {
                // get stock
                $stock = StockMain::where('storage_id', $item->storage_id)
                    ->where('material_id', $item->material_id)
                    ->first();

                    // cek batch
                if (count($stock->batch_stock) > 0) {
                    $batch = (new StockBatch)->newQuery();

                    $batch->where('stock_main_id', $stock->id)
                    ->where('qty_unrestricted', '<>', 0);

                    // cek global setting
                    if (appsetting('SORT_BATCH')) {
                        $batch->orderBy('gr_date', 'asc');//FIFO (by GR)
                    } else {
                        $batch->orderBy('sled_date', 'asc');//FEFO (by expiry)
                    }

                    $batch->orderBy('batch_number', 'asc');

                    // get batch
                    $batchs = $batch->get();

                    // count total batch
                    $total_batch = count($batchs) - 1;

                    // get all do item
                    $do_all_not_id = DeliveryHeader::where('status', 1)->where('id', '<>', $id)->pluck('id');
                    $do_all = DeliveryHeader::where('status', 1)->where('id', '<>', $id)->pluck('id');

                    // do with batch 1
                    $do_item_batch1 = DeliveryItem::whereIn('delivery_header_id', $do_all_not_id)
                        ->where('stock_batch_id', $batchs[0]->id)
                        ->pluck('pick_qty')
                        ->toArray();

                    if (count($do_item_batch1) > 0){
                        $total_batch_allocated = array_sum($do_item_batch1);
                    } else {
                        $total_batch_allocated = 0;
                    }

                    // sisa batch 1
                    $sisa_batch1 = $batchs[0]->qty_unrestricted - $total_batch_allocated;

                    if ($sisa_batch1 > 0) {
                        if ($sisa_batch1 > $item->delivery_qty) {
                            $qty_pick = $item->delivery_qty;
                        } else {
                            $qty_pick = $sisa_batch1;
                        }

                        // update do item
                        $item->update([
                            'batch' => $batchs[0]->batch_number,
                            'stock_batch_id' => $batchs[0]->id,
                            'pick_qty' => $qty_pick
                        ]);
                        
                        // create new do row if batch lebih dari 1
                        if ($item->delivery_qty != $item->pick_qty) {
                            if ($total_batch >= 1) {
                                $do_item_batch2 = DeliveryItem::whereIn('delivery_header_id', $do_all)
                                    ->where('stock_batch_id', $batchs[1]->id)
                                    ->pluck('pick_qty')
                                    ->toArray();
    
                                if (count($do_item_batch2) > 0){
                                    $total_batch2_allocated = array_sum($do_item_batch1);
                                } else {
                                    $total_batch2_allocated = 0;
                                }
    
                                $sisa_batch2 = $batchs[1]->qty_unrestricted - $total_batch2_allocated;
    
                                if ($sisa_batch2 > 0) {
                                    // get kekurangan qty
                                    $kekurangan1 = $item->delivery_qty - $item->pick_qty;
                                    if ($sisa_batch2 > $kekurangan1) {
                                        $qty_pick2 = $kekurangan1;
                                    } else {
                                        $qty_pick2 = $sisa_batch2;
                                    }
            
                                    // create new row DO
                                    $created_item = DeliveryItem::create([
                                        'delivery_header_id' => $header->id,
                                        'delivery_item' => $item->delivery_item,
                                        'sequence' => $item->sequence + 1,
                                        'plant_id' => $item->plant_id,
                                        'storage_id' => $item->storage_id,
                                        'batch' => $batchs[1]->batch_number,
                                        'stock_batch_id' => $batchs[1]->id,
                                        'reservation_id' => $item->reservation_id,
                                        'material_id' => $item->material_id,
                                        'uom_id' => $item->uom_id,
                                        'delivery_qty' => 0,
                                        'pick_qty' => $qty_pick2,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }
                    } else {
                        if ($total_batch >= 1) {
                            $do_item_batch2 = DeliveryItem::whereIn('delivery_header_id', $do_all_not_id)
                                ->where('stock_batch_id', $batchs[1]->id)
                                ->pluck('pick_qty')
                                ->toArray();

                            if (count($do_item_batch2) > 0){
                                $total_batch2_allocated = array_sum($do_item_batch2);
                            } else {
                                $total_batch2_allocated = 0;
                            }

                            $sisa_batch2 = $batchs[1]->qty_unrestricted - $total_batch2_allocated;

                            if ($sisa_batch2 > 0) {
                                if ($sisa_batch2 > $item->delivery_qty) {
                                    $qty_pick = $item->delivery_qty;
                                } else {
                                    $qty_pick = $sisa_batch2;
                                }
                                
                                // update do item
                                $item->update([
                                    'batch' => $batchs[1]->batch_number,
                                    'stock_batch_id' => $batchs[1]->id,
                                    'pick_qty' => $qty_pick
                                ]);

                                // create new do row if batch lebih dari 2
                                if ($item->delivery_qty != $item->pick_qty) {
                                    if ($total_batch >= 2) {
                                        $do_item_batch3 = DeliveryItem::whereIn('delivery_header_id', $do_all)
                                            ->where('stock_batch_id', $batchs[2]->id)
                                            ->pluck('pick_qty')
                                            ->toArray();
            
                                        if (count($do_item_batch3) > 0){
                                            $total_batch3_allocated = array_sum($do_item_batch3);
                                        } else {
                                            $total_batch3_allocated = 0;
                                        }
            
                                        $sisa_batch3 = $batchs[2]->qty_unrestricted - $total_batch3_allocated;
            
                                        if ($sisa_batch3 > 0) {
                                            // get kekurangan qty
                                            $kekurangan2 = $item->delivery_qty - $item->pick_qty;
                                            if ($sisa_batch3 > $kekurangan2) {
                                                $qty_pick3 = $kekurangan2;
                                            } else {
                                                $qty_pick3 = $sisa_batch3;
                                            }
                    
                                            // create new row DO
                                            $created_item = DeliveryItem::create([
                                                'delivery_header_id' => $header->id,
                                                'delivery_item' => $item->delivery_item,
                                                'sequence' => $item->sequence + 1,
                                                'plant_id' => $item->plant_id,
                                                'storage_id' => $item->storage_id,
                                                'batch' => $batchs[2]->batch_number,
                                                'stock_batch_id' => $batchs[2]->id,
                                                'reservation_id' => $item->reservation_id,
                                                'material_id' => $item->material_id,
                                                'uom_id' => $item->uom_id,
                                                'delivery_qty' => 0,
                                                'pick_qty' => $qty_pick3,
                                                'created_by' => Auth::user()->id,
                                                'updated_by' => Auth::user()->id
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $do_all = DeliveryHeader::where('status', 1)->pluck('id');

                    $do_item_all[] = DeliveryItem::whereIn('delivery_header_id', $do_all)
                        ->where('material_id', $item->material_id)
                        ->where('id', '<>', $item->id)
                        ->pluck('pick_qty');

                    if (count($do_item_all) > 0){
                        $total_stock_allocated = array_sum($do_item_all);
                    } else {
                        $total_stock_allocated = 0;
                    }

                    $sisa_stock = $stock->qty_unrestricted - $total_stock_allocated;

                    if ($sisa_stock > $item->delivery_qty) {
                        $qtyyy = $item->delivery_qty;
                    } else {
                        $qtyyy = $sisa_stock;
                    }

                    $item->update([
                        'batch' => null,
                        'stock_batch_id' => null,
                        'pick_qty' => $qtyyy,
                    ]);

                    // handel delivery serial
                }
            }

            $header->update([
                'status' => 1
            ]);

            DB::commit();

            return response()->json([
                'message' => 'success allocate batch DO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while allocate batch DO',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace(),
            ], 422);
        }
    }

    public function reject(Request $request)
    {
        Auth::user()->cekRoleModules(['picking-list-reject']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservations,id',
            'notes'       => 'required'
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $res = Reservation::find($id);

                // record approval reject
                ReservationApproval::create([
                    'reservation_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $res->status,
                    'status_to' => 3,
                    'notes' => $request->notes
                ]);

                // update reservation reject
                $res->update([
                    'status'    => 3,
                    'approval_note' => $request->notes,
                    'approval_date' => now(),
                    'approval_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'success reject picking list'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while reject picking list',
                'detail' => $e->getMessage()
            ], 422);
        }
    }
}
