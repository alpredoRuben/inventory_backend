<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\Storage as MasterStorage;
use App\Models\Reservation;
use App\Models\ReservationApproval;
use App\Models\PurchaseApproval;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\MovementHistory;
use App\Models\MaterialStorage;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\Supplier;
use App\Models\PurchaseAttachment;

class RfqController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['rfq-view']);

        $rfq = (new PurchaseHeader)->newQuery();

        $rfq->with([
            'term_payment', 'purc_group', 'incoterm', 'vendor', 'createdBy'
        ]);

        // for RFQ
        $rfq->where('po_doc_category', 'F');

        // filter
        if (request()->has('po_doc_no')) {
            $rfq->where(DB::raw("LOWER(po_doc_no)"), 'LIKE', "%".strtolower(request()->input('po_doc_no'))."%");
        }

        if (request()->has('plant')) {
            $plant = request()->input('plant');
            $rfq->whereHas('item', function ($data) use ($plant){
                $data->whereIn('plant_id', $plant);
                return $data;
            });
        }

        if (request()->has('storage')) {
            $storage = request()->input('storage');
            $rfq->whereHas('item', function ($data) use ($storage){
                $data->whereIn('storage_id', $storage);
                return $data;
            });
        }

        /** RFQ status
        * 0. Open
        * 1. Sent
        * 2. Submitted
        * 3. Awarded
        * 4. Rejected
        * 5. Processed
        * 9. Deleted
        **/
        
        if (request()->has('status')) {
            $rfq->where('status', request()->input('status'));
        } else {
            $rfq->where('status', '!=', 9);
        }

        if (request()->has('purc_doc_date')) {
            $rfq->whereBetween('purc_doc_date', 
                [request()->purc_doc_date[0], request()->purc_doc_date[1]]
            );
        }

        if (request()->has('deadline_date')) {
            $rfq->whereBetween('deadline_date', 
                [request()->deadline_date[0], request()->deadline_date[1]]
            );
        }

        if (request()->has('vendor')) {
            $rfq->whereIn('vendor_id', request()->input('vendor'));
        }

        if (request()->has('bid_number')) {
            $rfq->where(DB::raw("LOWER(bid_number)"), 'LIKE', "%".strtolower(request()->input('bid_number'))."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $rfq->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $rfq->orderBy('id', 'desc');
        }

        $rfq = $rfq->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $rfq->transform(function($data){
            // get delivery date, plant, storage from 1st item
            $data->delivery_date = $data->item->first()->delivery_date;
            $data->plant = $data->item->first()->plant;
            $data->storage = $data->item->first()->storage;

            // get last rfq approval
            $last_approval = $data->approval->last();
            $data->approved_by = $last_approval ? $last_approval->user : null;
            $data->approved_time = $last_approval ? Carbon::parse($last_approval->created_at)->format('Y-m-d H:i:s') : null;

            // get rfq ammount from rfq item
            $ammount = [];
            foreach ($data->item as $value) {
                $getpo = PurchaseItem::find($value->id);
                $ammount[] = $getpo->price_unit * $getpo->order_qty;
            }

            $ammount = collect($ammount)->sum();

            $data->ammount = $ammount;

            return $data;
        });

        return $rfq;
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['rfq-view']);

        $rfq = PurchaseHeader::with(['item', 'term_payment', 'purc_group', 'incoterm', 'vendor', 'attachment'])
	        ->with('vendor.location')
	        ->with('item.material')
	        ->with('item.material.images')
	        ->with('item.group_material')
	        ->with('item.gl_account')
	        ->with('item.plant')
	        ->with('item.storage')
	        ->with('item.project')
	        ->with('item.order_unit')
	        ->with('item.cost_center')
	        ->find($id);

        $ammount = [];
        foreach ($rfq->item as $value) {
            $getpo = PurchaseItem::find($value->id);
            $ammount[] = $getpo->price_unit * $getpo->order_qty;
        }

        $ammount = collect($ammount)->sum();

        $rfq->ammount = $ammount;

        if (!$rfq->vendor_id) {
            $first_reservation = Reservation::find($rfq->item[0]->reservation_id);
            $rfq->vendor_reservation = Supplier::find($first_reservation->prefered_vendor_id);
        }

        $awarded = PurchaseApproval::where('purchase_header_id', $id)
            ->where('status_to', 3)
            ->first();

        $rfq->awarded_at = $awarded ? Carbon::parse($awarded->created_at)->format('Y-m-d H:i:s') : null;
        $rfq->awarded_by = $awarded ? User::find($awarded->user_id) : null;

        return $rfq;
    }

    public function showItem($id)
    {
        Auth::user()->cekRoleModules(['rfq-view']);

        $item = PurchaseItem::with([
            'material', 'group_material', 'gl_account', 'plant', 'storage', 'project', 'order_unit', 'cost_center', 'reservation'
        ])
        ->with('material.images')
        ->find($id);

        return $item;
    }

    public function rfqCode()
    {
        Auth::user()->cekRoleModules(['rfq-create']);
        
        // get rfq code 
        $code = TransactionCode::where('code', 'F')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'F';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'F';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'F';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function poCode()
    {
        Auth::user()->cekRoleModules(['rfq-update']);
        
        // get po code 
        $code = TransactionCode::where('code', 'B')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'B';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function convertFromPR(Request $request)
    {
    	Auth::user()->cekRoleModules(['rfq-create']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservations,id'
        ]);

        $pr_code = Reservation::whereIn('id', $request->id)->pluck('reservation_code');

        // validate pr header status
        foreach ($request->id as $key => $value) {
            $pr_headers = Reservation::find($value);
            if (!in_array($pr_headers->status, [2,5])) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key.''  => ['the selected PR status must approved']
                    ]
                ], 422);
            }
        }

        $pr_item = Reservation::whereIn('reservation_code', $pr_code)
            ->whereNotNull('material_short_text')
            ->get();

        // validate pr item status
        foreach ($pr_item as $key => $value) {
            $pr_items = Reservation::find($value->id);
            if ($pr_items->status == 5) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id'  => ['the selected PR Already processed to PO or RFQ']
                    ]
                ], 422);
            }
        }

        // validate proc type external
        /* cek proc type
        * 0 = internal
        * 1 = external
        */
        $proc_type1 = $pr_item[0]['proc_type'];
        $cekrproc_type = true;

        for ($i = 0; $i < count($pr_item); $i++) {
            $mrp = $pr_item[$i]['proc_type'];
            if ($mrp !== $proc_type1 | !$mrp) {
                $cekrproc_type = false;
            }
        }

        if (!$cekrproc_type) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'id'  => ['the selected PR item are not in external procurement group']
                ]
            ], 422);
        }

        try {
            DB::beginTransaction();

            $code = $this->rfqCode();

            // create new RFQ header
            $rfq = PurchaseHeader::create([
                'po_doc_no' => $code,
                'po_doc_category' => 'F',
                'po_doc_type' => 0,
                'purc_doc_date' => now(),
                'purc_group_id' => $pr_item[0]['proc_group_id'],
                'status' => 0,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);

            foreach ($pr_item as $item) {
                // insert PR item as PO item
                $po_item = PurchaseItem::create([
                    'purchase_header_id' => $rfq->id,
                    'purchasing_item_no' => null,
                    'account_type' => $item->material_id ? 0 : 1,//0. Material; 1. Text
                    'material_id' => $item->material_id,
                    'short_text' => $item->material_short_text,
                    'long_text' => null,
                    'plant_id' => $item->plant_id,
                    'storage_id' => $item->storage_id,
                    'target_qty' => null,
                    'order_qty' => $item->quantity_base_unit,
                    'order_unit_id' => $item->uom_base_id,
                    'price_unit' => $item->unit_cost,
                    'delivery_location' => null,
                    'gl_account_id' => $item->account_id,
                    'cost_center_id' => $item->cost_center_id,
                    'dlv_complete' => 0,
                    'fund_center' => null,
                    'commitment_item' => $item->commitment_item_id,
                    'reservation_id' => $item->id,
                    'pr_number' => null,
                    'pr_item_number' => null,
                    'purc_doc_number' => null,
                    'purc_item_number' => null,
                    'group_material_id' => $item->group_material_id,
                    'project_id' => $item->project_id,
                    'delivery_date' => $item->requirement_date,
                    'currency' => $item->currency,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);

                // find pr
                $res_pr_item = Reservation::find($item->id);

                // record pr approval
                ReservationApproval::create([
                    'reservation_id' => $item->id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $res_pr_item->status,
                    'status_to' => 5,
                    'notes' => 'Convert to RFQ'
                ]);

                // update pr status
                $res_pr_item->timestamps = false;
                $res_pr_item->status = 9;//RFQ created
                $res_pr_item->fulfillment_status = 1;//0.Open, 1.Completed
                $res_pr_item->save();

                Reservation::where('reservation_code', $res_pr_item->reservation_code)
                    ->where('reservation_item', null)
                    ->first()
                    ->update([
                        'status' => 9, //RFQ created
                    ]);
            }

            // update rfq code
            TransactionCode::where('code', 'F')
                ->first()
                ->update(['lastcode' => $code]);

            DB::commit();

            return PurchaseHeader::with('item')
                ->with('item.material')
                ->find($rfq->id);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while converting PR as RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function updateHeader($id, Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        $this->validate(request(), [
            // validate header
            'doc_currency' => 'required',
            // 'ammount' => 'required',
            // 'status' => 'required',
            'vendor_id' => 'required|exists:suppliers,id',
            'term_payment_id' => 'nullable|exists:term_payments,id',
            'purc_group_id' => 'nullable|exists:procurement_groups,id',
            'incoterm_id' => 'nullable|exists:incoterms,id',
            'incoterm2' => 'nullable',
            'notes' => 'nullable',
            'your_reference' => 'nullable',
            'bid_number' => 'nullable|string|max:15',
            'quotation_number' => 'nullable|string|max:15',
            'deadline_date' => 'nullable|date'
        ]);

        $rfq = PurchaseHeader::find($id);

        if (!$rfq) {
            return response()->json([
                'message' => 'RFQ Not Found'
            ], 404);
        }

        if ($rfq->status != 0) {
            return response()->json([
                'message' => 'RFQ status not Open, Cant Edit again'
            ], 422);
        }

        // validate awarded Bid Number can't used
        $used_bid_number = PurchaseHeader::where('id', '!=', $id)
            ->where('po_doc_category', 'F')
            ->where('bid_number', $request->bid_number)
            ->whereIn('status', [3,4,5])
            ->first();
        
        if ($used_bid_number) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'bid_number'  => ['Bid Number Already used in another RFQ']
                ]
            ], 422);
        }

        $req_vendor = Supplier::find($request->vendor_id);

        $vendor_rfq = PurchaseHeader::where('vendor_id', $request->vendor_id)
            ->where('bid_number', $request->bid_number)
            ->where('id', '!=', $id)
            ->first();

        if ($vendor_rfq) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'vendor_id'  => ['RFQ for vendor '.$req_vendor->description.' and bid number '.$rfq->bid_number.' already exist']
                ]
            ], 422);
        }

        try {
            DB::beginTransaction();

            // update rfq header
            $rfq->update([
                'purc_group_id' => $request->purc_group_id,
                'vendor_id' => $request->vendor_id,
                'term_payment_id' => $request->term_payment_id,
                'doc_currency' => $request->doc_currency,
                'incoterm_id' => $request->incoterm_id,
                'incoterm2'  => $request->incoterm2,
                'notes' => $request->notes,
                'your_reference' => $request->your_reference,
                'bid_number' => $request->bid_number,
                'quotation_number' => $request->quotation_number,
                'deadline_date' => $request->deadline_date,
                'po_doc_category' => 'F',
                'updated_by' => Auth::user()->id,
            ]);

            DB::commit();

            return PurchaseHeader::with('item')->find($id);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error edit RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function updateAccountAssignment($id, Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        // validation
        $this->validate(request(), [
            'cost_center_id' => 'nullable|exists:cost_centers,id',
            'project_id' => 'nullable|exists:projects,id',
            'commitment_item' => 'nullable',
            'gl_account_id' => 'nullable|exists:accounts,id',
            'fund_center' => 'nullable',
        ]);

        $rfq_item = PurchaseItem::find($id);
        $rfq_item->cost_center_id = $request->cost_center_id;
        $rfq_item->project_id = $request->project_id;
        $rfq_item->commitment_item = $request->commitment_item;
        $rfq_item->gl_account_id = $request->gl_account_id;
        $rfq_item->fund_center = $request->fund_center;
        $rfq_item->updated_by = Auth::user()->id;
        $save = $rfq_item->save();

        if ($save) {
            return $rfq_item;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function updateItem($id, Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        $this->validate(request(), [
            // validate item
            'item' => 'array',
            'item.*.account_type' => 'nullable|numeric|min:0|max:2',
            'item.*.long_text' => 'nullable',
            'item.*.price_unit' => 'required|numeric|min:0',
            'item.*.delivery_date' => 'required|date',
            'item.*.order_qty' => 'required|numeric|min:1',
            'item.*.order_unit_id' => 'required|exists:uoms,id',
            'item.*.group_material_id' => 'required|exists:group_materials,id',
        ]);

        $mat_text = [];

        foreach ($request->item as $key => $val) {
            // validate free material
            if ($val['account_type'] == 1) {
                if (!isset($val['material_id'])) {
                    $this->validate(request(), [
                        'item.'.$key.'.short_text' => 'required',
                    ]);
                }

                $mat_text[] = isset($val['short_text']) ? $val['short_text'] : Material::find($val['material_id'])->description;
            }

            if ($val['account_type'] == 0 | $val['account_type'] == '' | $val['account_type'] == 2) {
                $this->validate(request(), [
                    'item.'.$key.'.material_id' => 'required|exists:materials,id',
                ]);

                $mat_text[] = isset($val['short_text']) ? $val['short_text'] : Material::find($val['material_id'])->description;
            }
        }

        // get material from po item
        $po_item_mat = PurchaseItem::where('purchase_header_id', $id)
            ->whereNotNull('short_text')
            ->pluck('short_text')
            ->toArray();

        // get material from request
        $req_mat = collect($mat_text)->toArray();

        // validate duplicate material
        if (count($req_mat) != count(array_unique($req_mat))) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'material_id'  => ['Can not enter the same material description more than once']
                ]
            ], 422);
        }

        // compare material id with material id in this reservation
        $unused_mat = array_diff($po_item_mat, $req_mat);
        $new_mat = array_diff($req_mat, $po_item_mat);

        $rfq = PurchaseHeader::find($id);

        // cek rfq status
        if ($rfq->status != 0) {
            return response()->json([
                'message' => 'Purchase Orders status not Open, Cant Edit again'
            ], 422);
        }

        try {
            DB::beginTransaction();
            // update rfq item
            $item = 1;
            foreach ($request->item as $key => $val) {
                $first_pi = PurchaseItem::where('purchase_header_id', $id)->first();

                if (isset($val['material_id'])) {
                    $mat = Material::find($val['material_id']);

                    $pi = PurchaseItem::where('short_text', $val['short_text'])
                        ->where('purchase_header_id', $id)
                        ->first();

                    if ($pi) {
                        $pi->update([
                            'purchasing_item_no' => $item++,
                            'long_text' => $val['long_text'],
                            'price_unit' => $val['price_unit'],
                            'delivery_date' => $val['delivery_date'],
                            'order_qty' => $val['order_qty'],
                            'updated_by' => Auth::user()->id,
                        ]);
                    } else {
                        PurchaseItem::create([
                            'purchase_header_id' => $id,
                            'purchasing_item_no' => $item++,
                            'account_type' => isset($val['account_type']) ? $val['account_type'] : null,
                            'material_id' => $val['material_id'],
                            'short_text' => isset($val['short_text']) ? $val['short_text'] : $mat->description,
                            'long_text' => $val['long_text'],
                            'plant_id' => $first_pi->plant_id,
                            'storage_id' => $first_pi->storage_id,
                            'target_qty' => null,
                            'order_qty' => $val['order_qty'],
                            'order_unit_id' => $val['order_unit_id'],
                            'price_unit' => $val['price_unit'],
                            'delivery_location' => null,
                            'gl_account_id' => null,
                            'cost_center_id' => null,
                            'dlv_complete' => false,
                            'fund_center' => null,
                            'commitment_item' => null,
                            'reservation_id' => null,
                            'pr_number' => null,
                            'pr_item_number' => null,
                            'purc_doc_number' => null,
                            'purc_item_number' => null,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id,
                            'deleted' => false,
                            'group_material_id' => $val['group_material_id'],
                            'project_id' => null,
                            'delivery_date' => $val['delivery_date'],
                            'currency' => 'IDR',
                            'completed' => false,
                            'tax' => null,
                            'tax_item' => null
                        ]);
                    }
                } else {
                    $pi = PurchaseItem::where('short_text', $val['short_text'])
                        ->where('purchase_header_id', $id)
                        ->first();

                    if ($pi) {
                        $pi->update([
                            'purchasing_item_no' => $item++,
                            'long_text' => $val['long_text'],
                            'price_unit' => $val['price_unit'],
                            'delivery_date' => $val['delivery_date'],
                            'order_qty' => $val['order_qty'],
                            'updated_by' => Auth::user()->id,
                        ]);
                    } else {
                        PurchaseItem::create([
                            'purchase_header_id' => $id,
                            'purchasing_item_no' => $item++,
                            'account_type' => isset($val['account_type']) ? $val['account_type'] : null,
                            'material_id' => $val['material_id'],
                            'short_text' => isset($val['short_text']) ? $val['short_text'] : $mat->description,
                            'long_text' => $val['long_text'],
                            'plant_id' => $first_pi->plant_id,
                            'storage_id' => $first_pi->storage_id,
                            'target_qty' => null,
                            'order_qty' => $val['order_qty'],
                            'order_unit_id' => $val['order_unit_id'],
                            'price_unit' => $val['price_unit'],
                            'delivery_location' => null,
                            'gl_account_id' => null,
                            'cost_center_id' => null,
                            'dlv_complete' => false,
                            'fund_center' => null,
                            'commitment_item' => null,
                            'reservation_id' => null,
                            'pr_number' => null,
                            'pr_item_number' => null,
                            'purc_doc_number' => null,
                            'purc_item_number' => null,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id,
                            'deleted' => false,
                            'group_material_id' => $val['group_material_id'],
                            'project_id' => null,
                            'delivery_date' => $val['delivery_date'],
                            'currency' => 'IDR',
                            'completed' => false,
                            'tax' => null,
                            'tax_item' => null
                        ]);
                    }
                }
            }

            // delete unsed item based unused material id
            foreach ($unused_mat as $mat_id) {
                $del_pr = PurchaseItem::where('short_text', $mat_id)
                    ->where('purchase_header_id', $id)
                    ->first();

                if ($del_pr) {
                    $del_pr->delete();
                }
            }

            DB::commit();

            $response = PurchaseHeader::with(['item', 'term_payment', 'purc_group', 'incoterm', 'vendor'])
                ->with('vendor.location')
                ->with('item.material')
                ->with('item.material.images')
                ->with('item.group_material')
                ->with('item.gl_account')
                ->with('item.plant')
                ->with('item.storage')
                ->with('item.project')
                ->with('item.order_unit')
                ->with('item.cost_center')
                ->find($id);

            $ammount = [];
            foreach ($response->item as $value) {
                $getrfq = PurchaseItem::find($value->id);
                $ammount[] = $getrfq->price_unit * $getrfq->order_qty;
            }

            $ammount = collect($ammount)->sum();

            $response->ammount = $ammount;

            return $response;
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while edit RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function send($id)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        $rfq = PurchaseHeader::find($id);

        if (!$rfq) {
            return response()->json([
                'message' => 'RFQ Not Found'
            ], 404);
        }

        if ($rfq->status != 0) {
            return response()->json([
                'message' => 'RFQ status not Open, Cant Send'
            ], 422);
        }

        $vendor = Supplier::find($rfq->vendor_id);

        if (!$vendor->email) {
            return response()->json([
                'message' => 'RFQ vendor email is empty'
            ], 422);
        }

        $data = [
            'rfq' => $rfq,
            'vendor_mail' => $vendor->email,
            'vendor_name' => $vendor->description,
        ];

        \Mail::send('email.rfq.send', $data, function ($message) use ($data, $rfq) {
            $pdf = PDF::loadview('pdf.rfq', compact('rfq'))->setPaper('a4', 'potrait');
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();
            $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 8, array(0, 0, 0));
            $message->to($data['vendor_mail'], $data['vendor_name'])->subject('RFQ '.$rfq->po_doc_no.'');
            $filename = 'RFQ_'.$rfq->po_doc_no.'_'.$rfq->vendor->code.'.pdf';
            $message->attachData($pdf->output(), $filename);
        });

        PurchaseApproval::create([
            'purchase_header_id' => $id,
            'user_id' => Auth::user()->id,
            'status_from' => $rfq->status,
            'status_to' => 1, // Sent
            'notes' => ''
        ]);

        $rfq->update([
            'status' => 1, // Sent
        ]);

        return response()->json([
            'message' => 'Success send RFQ'
        ], 200);
    }

    public function pdf($id)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        $rfq = PurchaseHeader::with('item')->find($id);

        if (!$rfq) {
            return response()->json([
                'message' => 'RFQ Not Found'
            ], 404);
        }

        // if ($rfq->status != 0) {
        //     return response()->json([
        //         'message' => 'RFQ status not Open, Cant Send'
        //     ], 422);
        // }

        $pdf = PDF::loadview('pdf.rfq', compact('rfq'))->setPaper('a4', 'potrait');
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 8, array(0, 0, 0));
        
        /** RFQ status
        * 0. Open
        * 1. Sent
        * 2. Submitted
        * 3. Awarded
        * 4. Rejected
        * 5. Closed
        * 6. Printed
        * 9. Deleted
        **/
        if (in_array($rfq->status, [2,3,4,5,9])) {

        } else {
            PurchaseApproval::create([
                'purchase_header_id' => $id,
                'user_id' => Auth::user()->id,
                'status_from' => $rfq->status,
                'status_to' => 6, // Printed
                'notes' => ''
            ]);

            $rfq->update([
                'status' => 6, // Printed
            ]);
        }

        $filename = 'RFQ_'.$rfq->po_doc_no.'_'.$rfq->vendor->code.'.pdf';
        
        return $pdf->download($filename);
    }

    public function copy($id, Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-create']);

        // validation
        $this->validate(request(), [
            'vendor_id' => 'required|exists:suppliers,id'
        ]);

        // cek rfq
        $rfq = PurchaseHeader::find($id);

        if (!$rfq) {
            return response()->json([
                'message' => 'RFQ not found'
            ], 404);
        }

        // cek bid number
        if (!$rfq->bid_number) {
            return response()->json([
                'message' => 'RFQ data not complete, Cant copy RFQ'
            ], 422);
        }

        // cek deadline date
        $rfq_deadline = Carbon::parse($rfq->deadline_date)->format('Y-m-d');
        $now = Carbon::parse(now())->format('Y-m-d');
        if ($rfq_deadline < $now) {
            return response()->json([
                'message' => 'RFQ deadline date expired'
            ], 422);
        }

        // validate vendor cant duplicate in one bid number
        $req_vendor = Supplier::find($request->vendor_id);

        $vendor_rfq = PurchaseHeader::where('vendor_id', $request->vendor_id)
            ->where('bid_number', $rfq->bid_number)
            ->where('po_doc_category', 'F')
            ->first();

        if ($vendor_rfq) {
            return response()->json([
                'message' => 'RFQ for vendor '.$req_vendor->description.' and bid number '.$rfq->bid_number.' already exist'
            ], 422);
        }

        try {
            DB::beginTransaction();

            $code = $this->rfqCode();

            // create new RFQ header
            $newrfq = PurchaseHeader::create([
                'po_doc_no' => $code,
                'po_doc_category' => $rfq->po_doc_category,
                'po_doc_type' => $rfq->po_doc_type,
                'vendor_id' => $request->vendor_id,
                'term_payment_id' => $rfq->term_payment_id,
                'purc_group_id' => $rfq->purc_group_id,
                'doc_currency' => $rfq->doc_currency,
                'exchange_rate' => $rfq->exchange_rate,
                'purc_doc_date' => $rfq->purc_doc_date,
                'valid_from' => $rfq->valid_from,
                'valid_to' => $rfq->valid_to,
                'deadline_date' => $rfq->deadline_date,
                'bid_number' => $rfq->bid_number,
                'quotation_number' => $rfq->quotation_number,
                'your_reference' => $rfq->your_reference,
                'incoterm_id' => $rfq->incoterm_id,
                'incoterm2' => $rfq->incoterm2,
                'collective_number' => $rfq->collective_number,
                'status' => 0,
                'notes' => $rfq->notes,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);

            $no = 1;
            foreach ($rfq->item as $item) {
                // insert PR item as PO item
                $rfq_item = PurchaseItem::create([
                    'purchase_header_id' => $newrfq->id,
                    'purchasing_item_no' => $no++,
                    'account_type' => $item->account_type,//0. Material; 1. Text
                    'material_id' => $item->material_id,
                    'short_text' => $item->short_text,
                    'long_text' => $item->long_text,
                    'plant_id' => $item->plant_id,
                    'storage_id' => $item->storage_id,
                    'target_qty' => $item->target_qty,
                    'order_qty' => $item->order_qty,
                    'order_unit_id' => $item->order_unit_id,
                    'price_unit' => $item->price_unit,
                    'delivery_location' => null,
                    'gl_account_id' => $item->gl_account_id,
                    'cost_center_id' => $item->cost_center_id,
                    'dlv_complete' => 0,
                    'fund_center' => $item->fund_center,
                    'commitment_item' => $item->commitment_item_id,
                    'reservation_id' => $item->reservation_id,
                    'pr_number' => null,
                    'pr_item_number' => null,
                    'purc_doc_number' => null,
                    'purc_item_number' => null,
                    'group_material_id' => $item->group_material_id,
                    'project_id' => $item->project_id,
                    'delivery_date' => $item->delivery_date,
                    'currency' => $item->currency,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);
            }

            // update po code
            TransactionCode::where('code', 'F')
                ->first()
                ->update(['lastcode' => $code]);

            DB::commit();

            return PurchaseHeader::with('item')
                ->with('item.material')
                ->find($newrfq->id);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while copy RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function submit(Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        /** RFQ status
        * 0. Open
        * 1. Sent
        * 2. Submitted
        * 3. Awarded
        * 4. Rejected
        * 5. Closed
        * 6. Printed
        * 9. Deleted
        **/

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:purchase_headers,id',
            'approval_note' => 'required',
        ]);

        foreach ($request->id as $id) {
            $rfq = PurchaseHeader::find($id);

            // validate rfq
            if (!$rfq) {
                return response()->json([
                    'message'   => 'RFQ '.$rfq->po_doc_no.' not found',
                ], 404);
            }

            // validate rfq status
            if (!in_array($rfq->status, [0,1,6])) {
                return response()->json([
                    'message'   => 'RFQ '.$rfq->po_doc_no.' status must open, sent or printed',
                ], 422);
            }

            // validate Vendor not null
            if (!$rfq->vendor_id) {
                return response()->json([
                    'message'   => 'RFQ '.$rfq->po_doc_no.' vendor cant empty',
                ], 422);
            }

            // check qty & price cant empty
            foreach ($rfq->item as $value) {
                if (!$value->order_qty | $value->order_qty == 0) {
                    return response()->json([
                        'message'   => 'RFQ '.$rfq->po_doc_no.' item Qty cant empty',
                    ], 422);
                }

                if (!$value->price_unit | $value->price_unit <= 0) {
                    return response()->json([
                        'message'   => 'RFQ '.$rfq->po_doc_no.' item Unit Price cant be zero or less',
                    ], 422);
                }
            }
        }

        try {
            DB::beginTransaction();
            
            // update po & record status po
            foreach ($request->id as $id) {
                $rfq = PurchaseHeader::find($id);

                PurchaseApproval::create([
                    'purchase_header_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $rfq->status,
                    'status_to' => 2, // submit
                    'notes' => $request->approval_note
                ]);

                $rfq->timestamps = false;
                $rfq->status = 2; // submit
                $rfq->save();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success submit RFQ'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed submit RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function evaluate($bid_number)
    {
        Auth::user()->cekRoleModules(['rfq-view']);

        $rfq = PurchaseHeader::with(['item', 'vendor', 'term_payment', 'incoterm'])
            ->with('item.material')
            ->where('bid_number', $bid_number)
            ->where('po_doc_category', 'F')
            ->orderBy('po_doc_no', 'asc')
            ->get();

        if (count($rfq) == 0) {
            return response()->json([
                'message' => 'RFQ with bid number '.$bid_number.' Not Found'
            ], 404);
        }

        $rfq = $rfq->map(function ($data){
            $submit = PurchaseApproval::where('purchase_header_id', $data->id)
                ->where('status_to', 2)
                ->first();

            $data->submited_at = $submit ? Carbon::parse($submit->created_at)->format('Y-m-d H:i:s') : null;

            // get total ammount
            $item_rfq = PurchaseItem::where('purchase_header_id', $data->id)->get();
            $total_ammount = 0;
            foreach ($item_rfq as $key => $value) {
                $total_ammount += $value->price_unit * $value->order_qty;
            }

            $data->total_ammount = $total_ammount;

            return $data;
        });

        return $rfq;
    }

    public function award($bid_number, Request $request)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        $this->validate(request(), [
            'id' => 'required|exists:purchase_headers,id',
            // 'notes' => 'nullable|string|max:100'
        ]);

        /** RFQ status
        * 0. Open
        * 1. Sent
        * 2. Submitted
        * 3. Awarded
        * 4. Rejected
        * 5. Closed
        * 6. Printed
        * 9. Deleted
        **/

        try {
            DB::beginTransaction();

            // reject rfq lose
            $rfq_reject = PurchaseHeader::where('bid_number', $bid_number)
                ->where('po_doc_category', 'F')
                ->where('id', '!=', $request->id)
                ->get();

            foreach ($rfq_reject as $reject) {
                $rfq = PurchaseHeader::find($reject->id);

                if ($rfq->status != 2) {
                    throw new \Exception('invalid RFQ '.$rfq->po_doc_no.' status, status can be proced is submit only');
                }

                PurchaseApproval::create([
                    'purchase_header_id' => $rfq->id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $rfq->status,
                    'status_to' => 4,//reject
                    // 'notes' => $request->notes
                ]);

                $rfq->timestamps = false;
                $rfq->status = 4;// reject
                $rfq->save();
            }

            // award rfq win
            $rfq_award = PurchaseHeader::find($request->id);

            if ($rfq_award->status != 2) {
                throw new \Exception('invalid RFQ '.$rfq_award->po_doc_no.' status, status can be proced is submit only');
            }

            PurchaseApproval::create([
                'purchase_header_id' => $rfq_award->id,
                'user_id' => Auth::user()->id,
                'status_from' => $rfq_award->status,
                'status_to' => 3,//awarded
                // 'notes' => $request->notes
            ]);

            $rfq_award->timestamps = false;
            $rfq_award->status = 3;// awarded
            $rfq_award->save();

            DB::commit();

            return response()->json([
                'message' => 'success awarding RFQ'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while awarding RFQ',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function convertToPo($id)
    {
        Auth::user()->cekRoleModules(['rfq-update']);

        // find RFQ
        $rfq = PurchaseHeader::find($id);

        if (!$rfq) {
            return response()->json([
                'message' => 'RFQ not found'
            ], 404);
        }

        // cek rfq status awarded
        if ($rfq->status != 3) {
            return response()->json([
                'message' => 'RFQ status can convert to Purchase order is Awarded'
            ], 422);
        }

        try {
            DB::beginTransaction();

            $code = $this->poCode();

            // create new RFQ header
            $newpo = PurchaseHeader::create([
                'po_doc_no' => $code,
                'po_doc_category' => 'B',
                'po_doc_type' => $rfq->po_doc_type,
                'vendor_id' => $rfq->vendor_id,
                'term_payment_id' => $rfq->term_payment_id,
                'purc_group_id' => $rfq->purc_group_id,
                'doc_currency' => $rfq->doc_currency,
                'exchange_rate' => $rfq->exchange_rate,
                'purc_doc_date' => $rfq->purc_doc_date,
                'valid_from' => $rfq->valid_from,
                'valid_to' => $rfq->valid_to,
                'deadline_date' => $rfq->deadline_date,
                'bid_number' => $rfq->bid_number,
                'quotation_number' => $rfq->quotation_number,
                'your_reference' => $rfq->your_reference,
                'incoterm_id' => $rfq->incoterm_id,
                'incoterm2' => $rfq->incoterm2,
                'collective_number' => $rfq->collective_number,
                'status' => 0, //draft
                'notes' => $rfq->notes,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
            ]);

            $no = 1;
            foreach ($rfq->item as $item) {
                // insert PR item as PO item
                $po_item = PurchaseItem::create([
                    'purchase_header_id' => $newpo->id,
                    'purchasing_item_no' => $no++,
                    'account_type' => $item->account_type,//0. Material; 1. Text
                    'material_id' => $item->material_id,
                    'short_text' => $item->short_text,
                    'long_text' => $item->long_text,
                    'plant_id' => $item->plant_id,
                    'storage_id' => $item->storage_id,
                    'target_qty' => $item->target_qty,
                    'order_qty' => $item->order_qty,
                    'order_unit_id' => $item->order_unit_id,
                    'price_unit' => $item->price_unit,
                    'delivery_location' => null,
                    'gl_account_id' => $item->gl_account_id,
                    'cost_center_id' => $item->cost_center_id,
                    'dlv_complete' => 0,
                    'fund_center' => $item->fund_center,
                    'commitment_item' => $item->commitment_item_id,
                    'reservation_id' => $item->reservation_id,
                    'pr_number' => null,
                    'pr_item_number' => null,
                    'purc_doc_number' => null,
                    'purc_item_number' => null,
                    'group_material_id' => $item->group_material_id,
                    'project_id' => $item->project_id,
                    'delivery_date' => $item->delivery_date,
                    'currency' => $item->currency,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);
            }

            // update po code
            TransactionCode::where('code', 'B')
                ->first()
                ->update(['lastcode' => $code]);

            PurchaseApproval::create([
                'purchase_header_id' => $rfq->id,
                'user_id' => Auth::user()->id,
                'status_from' => $rfq->status,
                'status_to' => 5,// Closed
                'notes' => $code
            ]);

            $rfq->timestamps = false;
            $rfq->status = 5;// Closed
            $rfq->save();

            DB::commit();

            return PurchaseHeader::with('item')
                ->with('item.material')
                ->find($newpo->id);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while convert RFQ to PO',
                'detail' => $e->getMessage()
            ], 422);
        }   
    }
}
