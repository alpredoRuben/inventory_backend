<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Exports\ProductionScheduleExport;
use App\Exports\ProductionScheduleFailedExport;
use Maatwebsite\Excel\Facades\Excel;

use App\Jobs\ProductScheduleJob;
use App\Jobs\NewProductionScheduleJob;

use App\Imports\NewProductionScheduleImport;

use App\Models\ProductionOrderFailed;
use App\Models\ProductionOrder;
use App\Models\ProductionOrderDetail;
use App\Models\ProductionOrderApproval;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\WorkCenter;
use App\Models\Material;
use App\Models\GroupMaterial;
use App\Models\TransactionCode;
use App\Models\Reservation;
use App\Models\MaterialPlant;
use App\Models\StockDetermination;
use App\Models\StockMain;
use App\Models\Uom;
use App\Models\Mrp;

class ProductionOrderController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $data = (new ProductionOrder)->newQuery();

        $data->with('material', 'work_center', 'plant', 'storage', 'mrp', 'gr_uom', 'prod_uom', 'reservation');

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $data->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $data->whereIn('storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material_id')) {
            $data->whereIn('material_id', request()->input('material_id'));
        }

        // filter plant
        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        // filter storage
        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        // filter work center
        if (request()->has('work_center_id')) {
            $data->whereIn('work_center_id', request()->input('work_center_id'));
        }

        // filter mrp
        if (request()->has('mrp_id')) {
            $data->whereIn('mrp_id', request()->input('mrp_id'));
        }

        // filter status
        if (request()->has('status')) {
            $data->whereIn('status', request()->input('status'));
        }

        // filter production order
        if (request()->has('order_number')) {
            $data->where('order_number', request()->input('order_number'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $data->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $data->orderBy('id', 'asc');
        }

        $data = $data->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $data;
    }

    public function showByCode($order_number)
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $po = ProductionOrder::with([
            'material', 'work_center', 'plant', 'storage', 'prod_uom'
        ])
        ->where('order_number', $order_number)
        ->first();

        if (!$po) {
            return response()->json([
                'message' => 'Production Orders not Found'
            ], 404);
        }

        if ($po->status != 2) {
            return response()->json([
                'message' => 'Production Orders must Confirmed first'
            ], 422);
        }

        return $po;
    }

    public function resourceRequirement()
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $data = (new ProductionOrder)->newQuery();

        $data->with('work_center', 'plant', 'storage', 'mrp', 'detail');

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $data->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $data->whereIn('storage_id', $storage_id);
        }

        // filter plant
        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        // filter storage
        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        // filter work center
        if (request()->has('work_center_id')) {
            $data->whereIn('work_center_id', request()->input('work_center_id'));
        }

        // filter mrp
        if (request()->has('mrp_id')) {
            $data->whereIn('mrp_id', request()->input('mrp_id'));
        }

        // filter status
        if (request()->has('status')) {
            $data->whereIn('status', request()->input('status'));
        }

        // filter production order
        if (request()->has('order_number')) {
            $data->where('order_number', request()->input('order_number'));
        }

        $po_filtered = $data->pluck('id');

        // report po detail
        $detail = (new ProductionOrderDetail)->newQuery();

        // filter material
        if (request()->has('material_id')) {
            $detail->whereIn('component_id', request()->input('material_id'));
        }

        $detail->whereIn('production_order_id', $po_filtered);

        $detail->with('component', 'base_uom');

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $detail->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $detail->orderBy('id', 'asc');
        }

        $detail = $detail->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $detail->transform(function($data){
            $stock = StockMain::where('storage_id', $data->production_order->storage_id)
                ->where('material_id', $data->production_order->material_id)
                ->first();

            $data->stock_on_hand = $stock ? $stock->qty_unrestricted : 0;
            $data->stock_on_hand_uom = $stock ? Uom::find($data->material_id) : null;
            $data->plant = Plant::find($data->production_order->plant_id);
            $data->storage = MasterStorage::find($data->production_order->storage_id);
            $data->work_center = WorkCenter::find($data->production_order->work_center_id);
            $data->mrp = Mrp::find($data->production_order->mrp_id);

            return $data;
        });

        return $detail;
    }

	public function template() 
    {
        Auth::user()->cekRoleModules(['production-order-download']);

        return Excel::download(new ProductionScheduleExport, 'production_schedule.xlsx');
    }

    public function uploadOld(Request $request)
    {
        Auth::user()->cekRoleModules(['production-order-upload']);

    	$this->validate($request, [
            'upload_file' => 'required'
        ]);

        $file = $request->file('upload_file');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $data = $file->storeAs(
            'public', $filename
        );

        $user = Auth::user();
        
        ProductScheduleJob::dispatch($filename, $user);

        return response()->json([
        	'message' => 'upload is in progess',
            'data' => $data
        ], 200);
    }

    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['production-order-upload']);

        $this->validate($request, [
            'upload_file' => 'required',
            'production_date' => 'required|date'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        $month = (int)Carbon::parse($request->production_date)->format('m');
        $year = Carbon::parse($request->production_date)->format('Y');
        
        $import = new NewProductionScheduleImport($user,$month,$year);

        Excel::import($import, $file);

        // cek if uploaded data is empty
        $empty = count($import->getRow());

        if ($empty == 0) {
            return response()->json([
                'message' => 'Data is empty',
                'valid' => false,
                'data' => $import->getRow()
            ], 422);
        }

        // store session if error is null
        $countError = collect($import->getRow())->whereIn('valid', false)->count();
        if ($countError == 0) {
            $data = collect($import->getRow())->toArray();
            $valid = true;
            $code = 200;
            foreach ($data as $inv) {
                ProductionOrderFailed::create([
                    'plant' => $inv['plant'],
                    'work_center' => $inv['work_center'],
                    'material' => $inv['material'],
                    'description' => $inv['description'],
                    'uom' => $inv['base_uom'],
                    'month' => $inv['month'],
                    'year' => $inv['year'],
                    '1' => $inv['v_1'],
                    '2' => $inv['v_2'],
                    '3' => $inv['v_3'],
                    '4' => $inv['v_4'],
                    '5' => $inv['v_5'],
                    '6' => $inv['v_6'],
                    '7' => $inv['v_7'],
                    '8' => $inv['v_8'],
                    '9' => $inv['v_9'],
                    '10' => $inv['v_10'],
                    '11' => $inv['v_11'],
                    '12' => $inv['v_12'],
                    '13' => $inv['v_13'],
                    '14' => $inv['v_14'],
                    '15' => $inv['v_15'],
                    '16' => $inv['v_16'],
                    '17' => $inv['v_17'],
                    '18' => $inv['v_18'],
                    '19' => $inv['v_19'],
                    '20' => $inv['v_20'],
                    '21' => $inv['v_21'],
                    '22' => $inv['v_22'],
                    '23' => $inv['v_23'],
                    '24' => $inv['v_24'],
                    '25' => $inv['v_25'],
                    '26' => $inv['v_26'],
                    '27' => $inv['v_27'],
                    '28' => $inv['v_28'],
                    '29' => $inv['v_29'],
                    '30' => $inv['v_30'],
                    '31' => $inv['v_31'],
                    'messages' => $inv['message'],
                    'created_by' => Auth::user()->id
                ]);
            }
        } else {
            $valid = false;
            $code = 422;
        }

        return response()->json([
            'message' => 'upload validate',
            'valid' => $valid,
            'data' => $import->getRow()
        ], $code);
    }

    public function getValidData()
    {
        Auth::user()->cekRoleModules(['production-order-upload']);

        $column = [
            'plant', 'work_center', 'material', 'description', 'uom', 'month', 'year', '1 as v_1', '2 as v_2', '3 as v_3', '4 as v_4',
            '5 as v_5', '6 as v_6', '7 as v_7', '8 as v_8', '9 as v_9', '10 as v_10', '11 as v_11', '12 as v_12', '13 as v_13',
            '14 as v_14', '15 as v_15', '16 as v_16', '17 as v_17', '18 as v_18', '19 as v_19', '20 as v_20', '21 as v_21', '22 as v_22',
            '23 as v_23', '24 as v_24', '25 as v_25', '26 as v_26', '27 as v_27', '28 as v_28', '29 as v_29', '30 as v_30', '31 as v_31',
            'messages', 'created_by'
        ];

        $data = ProductionOrderFailed::where('created_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get($column);

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $new = $data->map(function($row){
            $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row->plant))->first();
            $work_center = WorkCenter::where(DB::raw("LOWER(code)"), strtolower($row->work_center))->first();
            $material = Material::where(DB::raw("LOWER(code)"), strtolower($row->material))->first();
            $uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row->uom))->first();

            $new_data = [
                'uploaded_by' => $row->created_by,
                'month' => $row->month,
                'year' => $row->year,
                'plant' => $row->plant,
                'work_center' => $row->work_center,
                'material' => $row->material,
                'description' => $row->description,
                'base_uom' => $row->uom,
                'plant_id' => $plant ? $plant->id : null,
                'work_center_id' => $work_center ? $work_center->id : null,
                'storage_id' => $work_center ? $work_center->storage_result_id : null,
                'material_id' => $material ? $material->id : null,
                'uom_id' => $uom ? $uom->id : null,
                'v_1' => $row->v_1,
                'v_2' => $row->v_2,
                'v_3' => $row->v_3,
                'v_4' => $row->v_4,
                'v_5' => $row->v_5,
                'v_6' => $row->v_6,
                'v_7' => $row->v_7,
                'v_8' => $row->v_8,
                'v_9' => $row->v_9,
                'v_10' => $row->v_10,
                'v_11' => $row->v_11,
                'v_12' => $row->v_12,
                'v_13' => $row->v_13,
                'v_14' => $row->v_14,
                'v_15' => $row->v_15,
                'v_16' => $row->v_16,
                'v_17' => $row->v_17,
                'v_18' => $row->v_18,
                'v_19' => $row->v_19,
                'v_20' => $row->v_20,
                'v_21' => $row->v_21,
                'v_22' => $row->v_22,
                'v_23' => $row->v_23,
                'v_24' => $row->v_24,
                'v_25' => $row->v_25,
                'v_26' => $row->v_26,
                'v_27' => $row->v_27,
                'v_28' => $row->v_28,
                'v_29' => $row->v_29,
                'v_30' => $row->v_30,
                'v_31' => $row->v_31,
                'valid' => true,
                'message' => $row->messages
            ];

            return $new_data;
        });

        return response()->json([
            'message' => 'valid data',
            'valid' => true,
            'data' => $new
        ], 200);
    }

    public function uploadValidData()
    {
        Auth::user()->cekRoleModules(['production-order-upload']);

        $data = ProductionOrderFailed::where('created_by', Auth::user()->id)->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],422);
        }

        $user = Auth::user();

        NewProductionScheduleJob::dispatch($user);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }

    public function download_invalid()
    {
        Auth::user()->cekRoleModules(['production-order-download']);
        
        $cek = ProductionOrderFailed::where('created_by', Auth::user()->id)->first();
        
        if (!$cek) {
            return response()->json([
                'message' => 'Data Invalid is empty'
            ], 200);
        }

        $user = Auth::user();

        return Excel::download(new ProductionScheduleFailedExport($user), 'production_schedule_invalid.xlsx');   
    }

    public function scheduleGroupMonthYear()
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $data = (new ProductionOrder)->newQuery();

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $data->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $data->whereIn('storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material_id')) {
            $data->whereIn('material_id', request()->input('material_id'));
        }

        // filter plant
        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        // filter storage
        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        // filter work center
        if (request()->has('work_center_id')) {
            $data->whereIn('work_center_id', request()->input('work_center_id'));
        }

        // filter mrp
        if (request()->has('mrp_id')) {
            $data->whereIn('mrp_id', request()->input('mrp_id'));
        }

        // filter status
        if (request()->has('status')) {
            $data->whereIn('status', request()->input('status'));
        }

        // filter production_date
        if (request()->has('production_date')) {
            $data->whereBetween('production_date', [request()->production_date[0], request()->production_date[1]]);
        }

        $data->select(
            'material_id', 'work_center_id', 'plant_id', DB::raw('Date(production_date) as date'), 'req_source',
            DB::raw('sum(quantity) as quantity_total'), DB::raw('extract(year from production_date) as year'),
            DB::raw('extract(month from production_date) as month')
        );
        
        $data->groupBy(['plant_id', 'work_center_id', 'material_id', 'date', 'req_source', 'year', 'month']);

        $data->orderBy('date', 'asc');

        $new = $data->get()->toArray();

        $unique = $data->get()->unique(function ($item) {
            return $item['work_center_id'].$item['material_id'].$item['year'].$item['month'];
        })->values();

        $unique->transform(function ($row) use ($new){
            $production = collect($new)->where('material_id', $row->material_id)
                ->where('work_center_id', $row->work_center_id)
                ->where('year', $row->year)
                ->where('month', $row->month);

            $production->transform(function ($new) {
                if ($new['req_source'] == 1) {
                    $tipe = 'Internal from Reservation';
                } elseif  ($new['req_source'] == 2) {
                    $tipe = 'Independent from Upload';
                }

                $new = [
                    'date' => Carbon::parse($new['date'])->format('Y-m-d H:i:s'),
                    'qty' => $new['quantity_total'],
                    'type' => $new['req_source'],
                    'type_desc' => $tipe
                ];

                return $new;
            });

            $row =[
                'plant' => Plant::find($row->plant_id),
                'work_center' => WorkCenter::find($row->work_center_id),
                'material' => Material::find($row->material_id),
                'year' => Carbon::parse($row->date)->format('Y'),
                'month' => Carbon::parse($row->date)->format('m'),
                'production' => $production->values()
            ];

            return $row;
        });

        return $unique;
    }

    public function schedule()
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $data = (new ProductionOrder)->newQuery();

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $data->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $data->whereIn('storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material_id')) {
            $data->whereIn('material_id', request()->input('material_id'));
        }

        // filter plant
        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        // filter storage
        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        // filter work center
        if (request()->has('work_center_id')) {
            $data->whereIn('work_center_id', request()->input('work_center_id'));
        }

        // filter mrp
        if (request()->has('mrp_id')) {
            $data->whereIn('mrp_id', request()->input('mrp_id'));
        }

        // filter status
        if (request()->has('status')) {
            $data->whereIn('status', request()->input('status'));
        }

        // filter production_date
        if (request()->has('production_date')) {
            $data->whereBetween('production_date', [request()->production_date[0], request()->production_date[1]]);
        } else {
            $start_month = new Carbon('first day of this month');

            $start = Carbon::parse($start_month)->format('Y-m-d');

            $end_month = new Carbon('last day of this month');

            $end = Carbon::parse($end_month)->format('Y-m-d');

            $data->whereBetween('production_date', [$start, $end]);
        }

        $data->select(
            'material_id', 'work_center_id', 'plant_id', DB::raw('Date(production_date) as date'), 'req_source',
            DB::raw('sum(quantity) as quantity_total'), DB::raw('extract(year from production_date) as year'),
            DB::raw('extract(month from production_date) as month')
        );
        
        $data->groupBy(['plant_id', 'work_center_id', 'material_id', 'date', 'req_source', 'year', 'month']);

        $data->orderBy('date', 'asc');

        $new = $data->get()->toArray();

        $unique = $data->get()->unique(function ($item) {
            return $item['work_center_id'].$item['material_id'];
        })->values();

        $unique->transform(function ($row) use ($new){
            $production = collect($new)->where('material_id', $row->material_id)
                ->where('work_center_id', $row->work_center_id);

            $production->transform(function ($new) {
                if ($new['req_source'] == 1) {
                    $tipe = 'Internal from Reservation';
                } elseif  ($new['req_source'] == 2) {
                    $tipe = 'Independent from Upload';
                }

                $new = [
                    'date' => Carbon::parse($new['date'])->format('Y-m-d H:i:s'),
                    'qty' => $new['quantity_total'],
                    'type' => $new['req_source'],
                    'type_desc' => $tipe
                ];

                return $new;
            });

            $row =[
                'plant' => Plant::find($row->plant_id),
                'work_center' => WorkCenter::find($row->work_center_id),
                'material' => Material::find($row->material_id),
                'year' => Carbon::parse($row->date)->format('Y'),
                'month' => Carbon::parse($row->date)->format('m'),
                'production' => $production->values()
            ];

            return $row;
        });

        return $unique;
    }

    public function reservation()
    {
        Auth::user()->cekRoleModules(['production-order-view']);

        $data = (new Reservation)->newQuery();

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $data->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $data->whereIn('storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material_id')) {
            $data->whereIn('material_id', request()->input('material_id'));
        }

        // filter plant
        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        // filter storage
        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        // filter production_date
        if (request()->has('production_date')) {
            $data->whereBetween('requirement_date', [request()->production_date[0], request()->production_date[1]]);
        } else {
            $start_month = new Carbon('first day of this month');

            $start = Carbon::parse($start_month)->format('Y-m-d');

            $end_month = new Carbon('last day of this month');

            $end = Carbon::parse($end_month)->format('Y-m-d');

            $data->whereBetween('requirement_date', [$start, $end]);
        }

        $data->where('document_type', 'R');

        $data->select(
            'material_id', 'plant_id', 'storage_id', DB::raw('Date(requirement_date) as date'),
            DB::raw('SUM(CAST(quantity as INTEGER)) as quantity_total'), DB::raw('extract(year from requirement_date) as year'),
            DB::raw('extract(year from requirement_date) as year'), DB::raw('extract(month from requirement_date) as month')
        );
        
        $data->groupBy(['plant_id', 'storage_id', 'material_id', 'date', 'year', 'month']);

        $data->orderBy('date', 'asc');

        $new = $data->get()->toArray();

        $unique = $data->get()->unique(function ($item) {
            return $item['storage_id'].$item['material_id'];
        })->values();

        $unique->transform(function ($row) use ($new){
            $production = collect($new)->where('material_id', $row->material_id)
                ->where('storage_id', $row->storage_id);

            $production->transform(function ($new) {

                $new = [
                    'date' => Carbon::parse($new['date'])->format('Y-m-d H:i:s'),
                    'qty' => $new['quantity_total']
                ];

                return $new;
            });

            $row =[
                'plant' => Plant::find($row->plant_id),
                'storage' => MasterStorage::find($row->storage_id),
                'material' => Material::find($row->material_id),
                'year' => Carbon::parse($row->date)->format('Y'),
                'month' => Carbon::parse($row->date)->format('m'),
                'production' => $production->values()
            ];

            return $row;
        });

        return $unique;
    }

    public function confirm(Request $request)
    {
        Auth::user()->cekRoleModules(['production-order-confirm']);

        $this->validate(request(), [
            'item' => 'array',
            'item.*.work_center_id' => 'required|exists:work_centers,id',
            'item.*.material_id' => 'required|exists:materials,id',
            'item.*.year' => 'required|date_format:"Y"',
            'item.*.month' => 'required|date_format:"m"',
            'notes' => 'nullable|string|max:191',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->item as $key => $value) {
                // get Data
                $data_po = ProductionOrder::where('work_center_id', $value['work_center_id'])
                    ->where('material_id', $value['material_id'])
                    ->whereYear('production_date', $value['year'])
                    ->whereMonth('production_date', $value['month'])
                    ->where('status', 1)
                    ->pluck('id');

                foreach ($data_po as $data) {
                    $po = ProductionOrder::find($data);

                    // cek po status must 1 (open)
                    // if ($po->status != 1) {
                    //     throw new \Exception('Production Order '.$po->order_number.' status not open can\'t confirm');
                    // }

                    $plant = Plant::find($po->plant_id);
                    $storage = MasterStorage::find($po->storage_id);
                    $work_center = WorkCenter::find($po->work_center_id);

                    // get reservation code 
                    $code = TransactionCode::where('code', 'R')->first();

                    // if exist
                    if ($code->lastcode) {
                        // generate if this year is graether than year in code
                        $yearcode = substr($code->lastcode, 1, 2);
                        $increment = substr($code->lastcode, 3, 7) + 1;
                        $year = substr(date('Y'), 2);
                        if ($year > $yearcode) {
                            $lastcode = 'R';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= '0000001';
                        } else {
                            // increment if year in code is equal to this year
                            $lastcode = 'R';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= sprintf("%07d", $increment);
                        }
                    } else {//generate if not exist
                        $lastcode = 'R';
                        $lastcode .= substr(date('Y'), 2);
                        $lastcode .= '0000001';
                    }

                    $res_code = $lastcode;

                    // create header reservation
                    $reservation = Reservation::create([
                        'reservation_code' => $res_code,
                        'reservation_type' => 1, //replenish
                        'document_type' => 'R', //reservation
                        'plant_id' => $po->plant_id,
                        'storage_id' => $po->storage_id,
                        'requirement_date' => $po->requirement_date,
                        'note_header' => 'Reservation from Production Order '.$po->order_number.'',
                        'status' => 0, //draft
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);

                    // insert po detail as reservation item
                    $item = 1;
                    foreach ($po->detail as $detail) {
                        // get material plant
                        $material_plant = MaterialPlant::where('plant_id', $reservation->plant_id)
                            ->where('material_id', $detail->component_id)
                            ->first();

                        $mrp = MaterialPlant::where('plant_id', $reservation->plant_id)
                            ->where('material_id', $detail->component_id)
                            ->whereNotNull('proc_group_id')
                            ->first();

                        // get material component bom
                        $component = Material::find($detail->component_id);
                        $matgroup = GroupMaterial::find($component->group_material_id);

                        if (!$material_plant) {
                            throw new \Exception('Material '.$component->code.' doesn\'t have material plant '.$plant->code);
                        } else {
                            // cek kalau storage request (storage supply work center) & storage supply (prod storage / stock deter)
                            if ($work_center->storage_supply_id != $material_plant->production_storage_id) {
                                if (!$material_plant->production_storage_id) {
                                    // cek stock determination
                                    $cek_stock_determination = StockDetermination::where('storage_id', $storage->id)
                                        ->orderBy('sequence', 'asc')
                                        ->first();

                                    if (!$cek_stock_determination) {
                                        throw new \Exception('Storage '.$storage->code.' doesn\'t have stock determination');
                                    } else {
                                        // insert reservation item
                                        $reservation_item = new Reservation;
                                        $reservation_item->reservation_code = $reservation->reservation_code;
                                        $reservation_item->reservation_item = $item++;
                                        $reservation_item->reservation_type = $reservation->reservation_type;
                                        $reservation_item->document_type = $reservation->document_type;
                                        $reservation_item->note_header = $reservation->note_header;
                                        $reservation_item->status = $reservation->status;
                                        $reservation_item->plant_id = $reservation->plant_id;
                                        $reservation_item->storage_id = $reservation->storage_id;
                                        $reservation_item->item_type = null;
                                        $reservation_item->material_id = $component->id;
                                        $reservation_item->material_short_text = $component->description;
                                        $reservation_item->note_item = null;
                                        $reservation_item->batch = null;
                                        $reservation_item->requirement_date = $reservation->requirement_date;
                                        $reservation_item->quantity = $detail->quantity;
                                        $reservation_item->uom_id = $detail->base_uom_id;
                                        $reservation_item->quantity_base_unit = $detail->quantity;
                                        $reservation_item->uom_base_id = $detail->base_uom_id;
                                        $reservation_item->group_material_id = $matgroup ? $matgroup->id : null;
                                        $reservation_item->account_id = null;
                                        $reservation_item->unit_cost = null;
                                        $reservation_item->inout_plant_id = $cek_stock_determination->supply_plant_id;
                                        $reservation_item->inout_storage_id = $cek_stock_determination->supply_storage_id;
                                        $reservation_item->material_plant_id = $mrp ? $mrp->id : null;
                                        $reservation_item->proc_type = $mrp ? $mrp->proc_type : null;
                                        $reservation_item->proc_group_id = $mrp ? $mrp->proc_group_id : null;
                                        $reservation_item->created_by = Auth::user()->id;
                                        $reservation_item->updated_by = Auth::user()->id;
                                        $reservation_item->save();
                                    }                                    
                                } else {
                                    // get production storage
                                    $production_storage = MasterStorage::find($material_plant->production_storage_id);

                                    // insert reservation item
                                    $reservation_item = new Reservation;
                                    $reservation_item->reservation_code = $reservation->reservation_code;
                                    $reservation_item->reservation_item = $item++;
                                    $reservation_item->reservation_type = $reservation->reservation_type;
                                    $reservation_item->document_type = $reservation->document_type;
                                    $reservation_item->note_header = $reservation->note_header;
                                    $reservation_item->status = $reservation->status;
                                    $reservation_item->plant_id = $reservation->plant_id;
                                    $reservation_item->storage_id = $reservation->storage_id;
                                    $reservation_item->item_type = null;
                                    $reservation_item->material_id = $component->id;
                                    $reservation_item->material_short_text = $component->description;
                                    $reservation_item->note_item = null;
                                    $reservation_item->batch = null;
                                    $reservation_item->requirement_date = $reservation->requirement_date;
                                    $reservation_item->quantity = $detail->quantity;
                                    $reservation_item->uom_id = $detail->base_uom_id;
                                    $reservation_item->quantity_base_unit = $detail->quantity;
                                    $reservation_item->uom_base_id = $detail->base_uom_id;
                                    $reservation_item->group_material_id = $matgroup ? $matgroup->id : null;
                                    $reservation_item->account_id = null;
                                    $reservation_item->unit_cost = null;
                                    $reservation_item->inout_plant_id = $production_storage->plant_id;
                                    $reservation_item->inout_storage_id = $production_storage->id;
                                    $reservation_item->material_plant_id = $mrp ? $mrp->id : null;
                                    $reservation_item->proc_type = $mrp ? $mrp->proc_type : null;
                                    $reservation_item->proc_group_id = $mrp ? $mrp->proc_group_id : null;
                                    $reservation_item->created_by = Auth::user()->id;
                                    $reservation_item->updated_by = Auth::user()->id;
                                    $reservation_item->save();
                                }                                
                            }
                        }
                    }

                    // update lastcode reservation
                    $code->update([
                        'lastcode' => $res_code
                    ]);

                    // record to production order approval
                    ProductionOrderApproval::create([
                        'production_order_id' => $po->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => 1, //open
                        'status_to' => 2, //confirmed
                        'notes' => $request->notes
                    ]);

                    // update production order status
                    // update reservation id in po
                    $po->update([
                        'status' => 2, //Confirmed
                        'reservation_id' => $reservation->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Success confirm data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error confirm data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
