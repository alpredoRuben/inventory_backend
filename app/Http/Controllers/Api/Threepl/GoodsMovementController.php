<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;
use PDF;

use App\Helpers\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MovementHisoryExport;

use App\Models\User;
use App\Models\TransactionCode;
use App\Models\MovementHistory;
use App\Models\MovementType;
use App\Models\MovementTypeReason;
use App\Models\Material;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\Serial;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\WorkCenter;
use App\Models\CostCenter;
use App\Models\Uom;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\DeliveryHeader;
use App\Models\DeliveryItem;
use App\Models\DeliverySerial;
use App\Models\DeliveryApproval;
use App\Models\Reservation;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\ProductionOrder;
use App\Models\ProductionOrderDetail;
use App\Models\ProductionOrderApproval;
use App\Models\MovingPriceHistory;
use App\Models\MaterialAccount;
use App\Models\Asset;
use App\Models\AssetStatus;
use App\Models\Supplier;
use App\Models\GroupMaterial;
use App\Models\Account;
use App\Models\MaterialVendor;
use App\Models\MaterialVendorHistory;
use App\Models\PurchaseDocumentHistory;

class GoodsMovementController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['history-movement-view']);
  
        $history = (new MovementHistory)->newQuery();

        $history->where('item_no', '<>', null)
        ->with([
            'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
            'cost_center', 'work_center', 'createdBy', 'updatedBy'
        ]);

        // filter
        if (request()->has('plant')) {
            $history->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('storage')) {
            $history->whereIn('storage_id', request()->input('storage'));
        }

        if (request()->has('material')) {
            $history->whereIn('material_id', request()->input('material'));
        }

        if (request()->has('batch')) {
            $batch = request()->input('batch');
            $history->where(DB::raw("LOWER(batch_no)"), 'LIKE', "%".strtolower($batch)."%");
        }

        if (request()->has('reference')) {
            $reference = request()->input('reference');
            $history->where(DB::raw("LOWER(material_slip)"), 'LIKE', "%".strtolower($reference)."%");
        }

        if (request()->has('movement_type')) {
            $history->whereIn('movement_type_id', request()->input('movement_type'));
        }

        // if have organization parameter
        $mvt_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($mvt_id) > 0) {
            $history->whereIn('movement_type_id', $mvt_id);
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $history->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $history->whereIn('storage_id', $storage_id);
        }

        if (request()->has('created_by')) {
            $history->whereIn('created_by', request()->input('created_by'));
        }

        if (request()->has('supplier')) {
            $history->whereIn('supplier_id', request()->input('supplier'));
        }

        if (request()->has('customer')) {
            $history->whereIn('customer_id', request()->input('customer'));
        }

        if (request()->has('posting_date')) {
            $from = str_replace('"', '', request()->posting_date[0]);
            $to = str_replace('"', '', request()->posting_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            $history->where('posting_date', '>=', $f.' 00:00:00');
            $history->where('posting_date', '<=', $t.' 00:00:00');
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $history->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $history->orderBy('material_doc', 'desc')->orderBy('item_no');
        }

        $history = $history->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $history->transform(function($data){
            if ($data->de_ce == 'c') {
                $x = -1;
            } else {
                $x = 1;
            }

            $data->total_amount = ((int)$data->amount * (float)$data->quantity) * $x;

            return $data;
        });
        
        return $history;
    }

    public function excel()
    {
        Auth::user()->cekRoleModules(['history-movement-view']);
  
        $history = (new MovementHistory)->newQuery();

        $history->where('item_no', '<>', null)
        ->with([
            'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
            'cost_center', 'work_center', 'createdBy', 'updatedBy'
        ]);

        // filter
        if (request()->has('plant')) {
            $history->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('storage')) {
            $history->whereIn('storage_id', request()->input('storage'));
        }

        if (request()->has('material')) {
            $history->whereIn('material_id', request()->input('material'));
        }

        if (request()->has('batch')) {
            $batch = request()->input('batch');
            $history->where(DB::raw("LOWER(batch_no)"), 'LIKE', "%".strtolower($batch)."%");
        }

        if (request()->has('reference')) {
            $reference = request()->input('reference');
            $history->where(DB::raw("LOWER(material_slip)"), 'LIKE', "%".strtolower($reference)."%");
        }

        if (request()->has('movement_type')) {
            $history->whereIn('movement_type_id', request()->input('movement_type'));
        }

        // if have organization parameter
        $mvt_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($mvt_id) > 0) {
            $history->whereIn('movement_type_id', $mvt_id);
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $history->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $history->whereIn('storage_id', $storage_id);
        }

        if (request()->has('created_by')) {
            $history->whereIn('created_by', request()->input('created_by'));
        }

        if (request()->has('supplier')) {
            $history->whereIn('supplier_id', request()->input('supplier'));
        }

        if (request()->has('customer')) {
            $history->whereIn('customer_id', request()->input('customer'));
        }

        if (request()->has('posting_date')) {
            $from = str_replace('"', '', request()->posting_date[0]);
            $to = str_replace('"', '', request()->posting_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            $history->where('posting_date', '>=', $f.' 00:00:00');
            $history->where('posting_date', '<=', $t.' 00:00:00');
        }

        $history = $history
        ->get()
        ->transform(function($data){
            $data->total_amount = (int)$data->amount * (float)$data->quantity;

            // get material group if material id is null
            if (!$data->material_id) {
                $po = PurchaseHeader::where('po_doc_no', $data->po_number)->first();
                $po_item = $po->item->where('short_text', $data->material_description)->first();

                $data->mat_group = GroupMaterial::find($po_item->group_material_id);
                $data->gl_account = Account::find($po_item->gl_account_id);
            } else {
                $data->mat_group = null;
                $data->gl_account = null;
            }

            $movement_type = $data->movement_type->code;

            switch ($movement_type) {
                case 641:
                    if ($data->de_ce == 'c') {
                        $stock_type = 'On-hand';
                    } else {
                        $stock_type = 'In-transit';
                    }

                    break;
                case 605:
                    if ($data->de_ce == 'c') {
                        $stock_type = 'In-transit';
                    } else {
                        $stock_type = 'On-hand';
                    }

                    break;
                default:
                    $stock_type = 'On-hand';
            }

            $data->stock_type = $stock_type;

            return $data;
        });
        
        return Excel::download(new MovementHisoryExport($history), 'report_history_movement.xlsx');
    }

    public function movementSerial($id)
    {
        Auth::user()->cekRoleModules(['history-movement-view']);

        $serial = (new SerialHistory)->newQuery();

        $serial->where('movement_history_id', $id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $serial->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(serial)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $serial->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $serial->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $serial->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $serial->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }

        return $serial;
    }

	public function getMaterialDoc()
	{
        Auth::user()->cekRoleModules(['goods-movement-create']);

		// get reservation code 
        $code = TransactionCode::where('code', 'M')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'M';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
	}

    public function getBatchNumber()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        // get reservation code 
        $code = TransactionCode::where('code', 'BN')->first();

        // if exist
        if ($code->lastcode) {
            $increment = $code->lastcode + 1;
            $lastcode = sprintf("%010d", $increment);
        } else {//generate if not exist
            $lastcode = '0000000001';
        }

        return $lastcode;
    }

    public function incompleteList()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        // $material_doc = MovementHistory::groupBy('material_doc')
        // ->select(\DB::raw('count(*) as aggregate, material_doc'))
        // ->where('created_by', Auth::user()->id)
        // ->whereNull('material_id')
        // ->get()
        // ->map(function($data) {
        //     if ($data->aggregate == 1) {
        //         return $data->material_doc;
        //     }
        // })->reject(function ($item) {
        //     return is_null($item);
        // })->values();

        /* $list = MovementHistory::with([
            'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
            'cost_center', 'work_center'
        ])
        ->where('created_by', Auth::user()->id)
        ->whereNull('item_no')
        ->orderBy('id', 'desc')
        ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
        ->appends(Input::except('page')); */

        $list = (new MovementHistory)->newQuery();
        $list->where('movement_histories.created_by', Auth::user()->id)->with([
            'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
            'cost_center', 'work_center'
        ]);
        $list->whereNull('item_no');

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material_doc':
                    $list->orderBy($sort_field, $sort_order);
                break;

                case 'plant':
                    $list->join('plants','movement_histories.plant_id','=','plants.id');
                    $list->select('movement_histories.*');
                    $list->orderBy('plants.code',$sort_order);
                break;

                case 'storage':
                    $list->join('storages','movement_histories.storage_id','=','storages.id');
                    $list->select('movement_histories.*');
                    $list->orderBy('storages.code',$sort_order);
                break;

                case 'cost_center':
                    $list->leftJoin('cost_centers','movement_histories.cost_center_id','=','cost_centers.id');
                    $list->select('movement_histories.*');
                    $list->orderBy('cost_centers.code',$sort_order);
                break;
                
                default:
                    $list->orderBy($sort_field, $sort_order);
                break;
            }
        }else {
            $list->orderBy('id', 'desc');
        }

        $list = $list->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
        ->appends(Input::except('page'))->toArray();

        return $list;
    }

    public function incompleteGRProduction()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        $mvmnt_typ = MovementType::where('code', 131)->first();

        $list = MovementHistory::with([
            'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
            'cost_center', 'work_center'
        ])
        ->where('created_by', Auth::user()->id)
        ->where('movement_type_id', $mvmnt_typ->id)
        ->whereNull('material_id')
        ->orderBy('id', 'desc')
        ->get()
        ->unique('material_doc')
        ->values();

        if (request()->has('per_page')) {
            return (new Collection($list))->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return (new Collection($list))->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function incomplete($id)
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        $data = MovementHistory::find($id);

        if (!$data) {
            return response()->json([
                'message' => 'data not found',
            ], 404);
        }

        $mvmnt_typ = MovementType::find($data->movement_type_id);

        if (!$mvmnt_typ) {
            return response()->json([
                'message' => 'data not found',
            ], 404);
        }

        $movement_type = $mvmnt_typ->code;

        switch ($movement_type) {
            case 561:
                return MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                break;
            case 201:
                return MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                break;
            case 311:
                $from = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $data->material_doc)
                ->where('item_no', 1)
                ->first();
                
                $to = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $data->material_doc)
                ->where('item_no', 2)
                ->first();

                $from->plant_id_to = $to->plant_id;
                $from->storage_id_to = $to->storage_id;

                return $from;

                break;
            case 605:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                $do_header = DeliveryHeader::where('delivery_code', $header->delivery_no)->first();

                // do item map qty do - gr qty 605
                $mvtype = MovementType::where('code', 605)->first();
                $delivery_code = $do_header->delivery_code;
                $items = DeliveryItem::where('delivery_header_id', $do_header->id)
                    ->with('reservation')
                    ->with('material')
                    ->with('plant')
                    ->with('storage')
                    ->get()
                    ->map(function ($data) use ($delivery_code, $mvtype) {
                        $gr_do_item = MovementHistory::where('delivery_no', $delivery_code)
                            ->where('movement_type_id', $mvtype->id)
                            ->where('de_ce', 'd')
                            ->where('material_id', $data->material_id)
                            ->where('batch_no', $data->batch)
                            ->get();

                            if ($gr_do_item) {
                                $gr_item = $gr_do_item->sum('quantity_entry');
                            } else {
                                $gr_item = 0;
                            }

                            $data->pick_qty = $data->pick_qty - $gr_item;

                            return $data;
                    })->reject(function ($data) {
                        return $data->pick_qty < 1;
                    })->values();

                $header->item = $items;

                return $header;

                break;
            case 101:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                $po_code = $header->po_number;
                $po_header = PurchaseHeader::where('po_doc_no', $header->po_number)->first();

                $mvt_gr_po = MovementType::where('code', 101)->first();

                $items = PurchaseItem::where('purchase_header_id', $po_header->id)
                    ->with(['material', 'order_unit'])
                    ->get()
                    ->map(function ($data) use ($po_code, $mvt_gr_po, $po_header) {
                        $gr_po_item = MovementHistory::where('po_number', $po_code)
                            ->where('material_id', $data->material_id)
                            ->where('movement_type_id', $mvt_gr_po->id)
                            ->get();

                            if ($gr_po_item) {
                                $gr_item = $gr_po_item->sum('quantity_entry');
                            } else {
                                $gr_item = 0;
                            }

                            $data->order_qty = $data->order_qty - $gr_item;
                            $data->vendor = Supplier::find($po_header->vendor_id);
                            if ($data->material) {
                                $data->material->material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                                                            ->where('material_id', $data->material_id)
                                                            ->first();
                            }

                            // if app setting UNIT_COST_WITH_TAX true unit cost + tax item
                            if (appsetting('UNIT_COST_WITH_TAX')) {
                                $unit_cost = $data->price_unit + $data->tax_item;
                            } else {
                                $unit_cost = $data->price_unit;
                            }

                            $data->price_unit = $unit_cost;

                            return $data;
                    })->reject(function ($data) {
                        return $data->order_qty < 1;
                    });

                $header->items = $items;

                return $header;

                break;
            case 131:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                $po_code = $header->work_order;
                $po_header = ProductionOrder::with('material')
                    ->where('order_number', $header->work_order)
                    ->get();

                $header->items = $po_header;

                return $header;

                break;
            case 241:
                return MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->find($id);

                break;
            default:
                return response()->json([
                    'message' => 'movement type '.$movement_type.', will be handled soon',
                ], 422);
        }
    }

    public function showByCode($material_doc)
    {
        Auth::user()->cekRoleModules(['history-movement-view']);

        $data = MovementHistory::where('material_doc', $material_doc)->first();

        if (!$data) {
            return response()->json([
                'message'   => 'Data Not Found'
            ], 404);
        }

        $movement_type = MovementType::find($data->movement_type_id)->code;
        switch ($movement_type) {
            case 561:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 562:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 201:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 202:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 311:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $material_doc)
                ->where('item_no', 1)
                ->first();
                
                $to = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $material_doc)
                ->where('item_no', 2)
                ->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                $header->plant_id_to = $to->plant_id;
                $header->storage_id_to = $to->storage_id;

                return $header;

                break;
            case 312:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $material_doc)
                ->where('item_no', 1)
                ->first();
                
                $to = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])
                ->where('material_doc', $material_doc)
                ->where('item_no', 2)
                ->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                $header->plant_id_to = $to->plant_id;
                $header->storage_id_to = $to->storage_id;

                return $header;

                break;
            case 605:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 606:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 101:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 701:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 702:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 131:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            case 241:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial'
                ])->where('material_doc', $material_doc)->get();

                return $header;

                break;
            default:
                return response()->json([
                    'message' => 'movement type '.$movement_type.', will be handled soon',
                ], 422);
        }
    }

    public function pdf($material_doc)
    {
        Auth::user()->cekRoleModules(['history-movement-view']);

        $data = MovementHistory::where('material_doc', $material_doc)->first();

        if (!$data) {
            return response()->json([
                'message'   => 'Data Not Found'
            ], 404);
        }

        $movement_type = MovementType::find($data->movement_type_id)->code;
        switch ($movement_type) {
            case 101:
                $header = MovementHistory::with([
                    'movement_type', 'plant', 'storage', 'material', 'entry_uom', 'supplier', 'customer',
                    'cost_center', 'work_center', 'plant', 'storage', 'createdBy'
                ])->where('material_doc', $material_doc)->first();

                $header->item = MovementHistory::with([
                    'material', 'entry_uom', 'supplier', 'customer', 'serial', 'plant', 'storage'
                ])->where('material_doc', $material_doc)->get();

                // return $header;

                // generate pdf GR
                $pdf = PDF::loadview('pdf.gr', compact('header'))->setPaper('a4', 'potrait');
                $pdf->output();
                $dom_pdf = $pdf->getDomPDF();
                $canvas = $dom_pdf->get_canvas();
                $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 8, array(0, 0, 0));
                
                return $pdf->download('Goods Acceptance.pdf');

                break;
            default:
                return response()->json([
                    'message' => 'movement type '.$movement_type.', will be handled soon',
                ], 422);
        }
    }

    public function createHeader(Request $request)
    {
    	Auth::user()->cekRoleModules(['goods-movement-create']);

    	$this->validate(request(), [
    		'movement_type_id' => 'required|exists:movement_types,id',
    		'movement_type_reason_id' => 'nullable|exists:movement_type_reasons,id',
    		'plant_id' => 'required|exists:plants,id',
    		'storage_id' => 'required|exists:storages,id',
    		'document_date' => 'required|date',
    		'material_slip' => 'nullable',
    		'doc_header_text' => 'nullable',
            'work_center_id' => 'nullable|exists:work_centers,id',
    		'cost_center_id' => 'nullable|exists:cost_centers,id',
        ]);

    	// generate new material document
        $material_document = $this->getMaterialDoc();

        $dc = MovementType::find($request->movement_type_id)->dc_ind;

        $movement_type = MovementType::find($request->movement_type_id)->code;
        switch ($movement_type) {
            case 561:
                // initial Stock Balance
                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_center_id' => $request->work_center_id,
                    'cost_center_id' => $request->cost_center_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                return MovementHistory::with(['movement_type', 'plant', 'storage'])->find($movement->id);
                break;
            case 201:
                // Goods Issue to Cost center
                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_center_id' => $request->work_center_id,
                    'cost_center_id' => $request->cost_center_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                return MovementHistory::with(['movement_type', 'plant', 'storage', 'cost_center', 'work_center'])->find($movement->id);
                break;
            case 311:
                // Stock Transfer 1-step
                $this->validate(request(), [
                    'plant_id_to' => 'required|exists:plants,id',
                    'storage_id_to' => 'required|exists:storages,id',
                ]);

                // movement sender
                $movement = MovementHistory::create([
                    'item_no' => 1,
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_center_id' => $request->work_center_id,
                    'cost_center_id' => $request->cost_center_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $movement_to = MovementHistory::create([
                    'item_no' => 2,
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id_to,
                    'storage_id' => $request->storage_id_to,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_center_id' => $request->work_center_id,
                    'cost_center_id' => $request->cost_center_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                $from = MovementHistory::with(['movement_type', 'plant', 'storage'])->find($movement->id);
                $to = MovementHistory::find($movement_to->id);

                $from->plant_id_to = $to->plant_id;
                $from->storage_id_to = $to->storage_id;

                return $from;

                break;
            case 605:
                // GR from STO (intransit to Unrestricted)
                $this->validate(request(), [
                    'delivery_no' => 'required|exists:delivery_headers,delivery_code',
                ]);

                $do_header = DeliveryHeader::where('delivery_code', $request->delivery_no)->first();

                // cek total GR do
                $gr_do_mvt = MovementType::where('code', 605)->first();
                $total_gr_do = MovementHistory::where('delivery_no', $request->delivery_no)
                    ->where('movement_type_id', $gr_do_mvt->id)
                    ->where('material_id', $do_header->item[0]->material_id)
                    ->where('de_ce', 'c')
                    ->count();

                $max_gr_sto = (int)appsetting('MAX_GR_STO');

                if ($total_gr_do >= $max_gr_sto) {
                    return response()->json([
                        'message' => 'DO '.$request->delivery_no.', already exceed MAX GR STO setting',
                    ], 422);
                }

                // if ($do_header->status == 3) {
                //     return response()->json([
                //         'message' => 'DO '.$request->delivery_no.', already recieved',
                //     ], 422);
                // }

                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'delivery_no' => $request->delivery_no,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                $do_item = $do_header->item;

                $created_movement = MovementHistory::with(['movement_type', 'plant', 'storage'])->find($movement->id);
                $created_movement->item = $do_item;

                return $created_movement;
                break;
            case 101:
                // GR from PO
                $this->validate(request(), [
                    'po_number' => 'required|exists:purchase_headers,po_doc_no',
                ]);

                $po_header = PurchaseHeader::where('po_doc_no', $request->po_number)->first();

                if (!in_array($po_header->status , [2,5,6])) {
                    return response()->json([
                        'message' => 'PO '.$request->po_number.', must approved before recived',
                    ], 422);
                }

                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'po_number' => $request->po_number,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                $po_item = PurchaseItem::where('purchase_header_id', $po_header->id)->with(['material', 'order_unit'])->get();

                $created_movement = MovementHistory::with(['movement_type', 'plant', 'storage'])->find($movement->id);
                $created_movement->item = $po_item;

                return $created_movement;
                break;
            case 131:
                // GR from Production
                $this->validate(request(), [
                    'order_number' => 'required|exists:production_orders,order_number',
                ]);

                $po_headers = ProductionOrder::where('order_number', $request->order_number)->get();

                foreach ($po_headers as $po_header) {
                    if ($po_header->status != 2) {
                        return response()->json([
                            'message' => 'Production Order '.$request->order_number.', must approved before recived',
                        ], 422);
                    }
                }

                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_order' => $request->order_number,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                // $po_item = $po_header->detail;

                $created_movement = MovementHistory::with(['movement_type', 'plant', 'storage'])->find($movement->id);
                $created_movement->item = $po_headers;
                // $created_movement->item = $po_item;

                return $created_movement;
                break;
            case 241:
                // Goods Issue to Asset
                $movement = MovementHistory::create([
                    'material_doc' => $material_document,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $request->movement_type_id,
                    'de_ce' => $dc,
                    'movement_type_reason_id' => $request->movement_type_reason_id,
                    'plant_id' => $request->plant_id,
                    'storage_id' => $request->storage_id,
                    'document_date' => $request->document_date,
                    'material_slip' => $request->material_slip,
                    'doc_header_text' => $request->doc_header_text,
                    'work_center_id' => $request->work_center_id,
                    'cost_center_id' => $request->cost_center_id,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // update code document
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $material_document]);

                return MovementHistory::with(['movement_type', 'plant', 'storage', 'cost_center', 'work_center'])->find($movement->id);
                break;
            default:
                return response()->json([
                    'message' => 'movement type '.$movement_type.', will be handled soon',
                ], 422);
        }
    }

    public function createItemSerialBatch($id, Request $request)
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);
        
        $this->validate(request(), [
            'item' => 'array',
            // 'item.*.material_id' => 'required|exists:materials,id',
            'item.*.quantity_entry' => 'required|numeric|min:1',
            'item.*.uom_id' => 'required|exists:uoms,id',
        ]);

        $data = MovementHistory::find($id);

        $movement_type = MovementType::find($data->movement_type_id)->code;

        foreach ($request->item as $key => $val) {
            switch ($movement_type) {
                case 561:
                    // initial Stock Balance

                    // validate material
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                    ]);

                    // validasi serial profile
                    $material = Material::find($val['material_id']);
                    if ($material->serial_profile) {
                        if (strtolower($data->de_ce) == "c") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                            ]);
                        } elseif (strtolower($data->de_ce) == "d") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'unique:stock_serials,serial',
                            ]);
                        }

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in material plant
                    if ($material_plant->batch_ind) {
                        // cek batch indicator in movement type
                        if ($cekmvt->batch_ind) {
                            // cek global setting
                            if (appsetting('EXPIRY_BATCH')) {
                                $expiry_required = 'required';
                            } else {
                                $expiry_required = 'nullable';
                            }

                            $this->validate(request(), [
                                'item.'.$key.'.batch.manuf_date' => 'nullable|date',
                                'item.'.$key.'.batch.gr_date' => 'nullable|date|required_if:item.'.$key.'.batch.manuf_date,',
                                'item.'.$key.'.batch.sled_date' => ''.$expiry_required.'|date',
                                'item.'.$key.'.batch.vendor_id' => 'nullable|exists:suppliers,id',
                                'item.'.$key.'.batch.vendor_batch' => 'nullable',
                                'item.'.$key.'.batch.license' => 'nullable',
                                'item.'.$key.'.batch.unit_cost' => 'nullable'
                            ]);
                        } else {
                            $this->validate(request(), [
                                'item.'.$key.'.batch.stock_batch_id' => 'required|exists:stock_batches,id'
                            ]);
                        }
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    break;
                case 201:
                    // Goods Issue to Cost center
                    
                    // validate material
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                    ]);

                    // validasi serial profile
                    $material = Material::find($val['material_id']);
                    if ($material->serial_profile) {
                        if (strtolower($data->de_ce) == "c") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                            ]);
                        } elseif (strtolower($data->de_ce) == "d") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'unique:stock_serials,serial',
                            ]);
                        }

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in material plant
                    if ($material_plant->batch_ind) {
                        $this->validate(request(), [
                            'item.'.$key.'.batch.stock_batch_id' => 'required|exists:stock_batches,id'
                        ]);
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    break;
                case 311:
                    // Stock Transfer 1-step
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                        'item.*.plant_id' => 'required|exists:plants,id',
                        'item.*.storage_id' => 'required|exists:storages,id',
                    ]);

                    // validasi serial profile
                    $material = Material::find($val['material_id']);
                    if ($material->serial_profile) {
                        $this->validate(request(), [
                            'item.'.$key.'.serial' => 'required|array',
                            'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                        ]);

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in material plant
                    if ($material_plant->batch_ind) {
                        $this->validate(request(), [
                            'item.'.$key.'.batch.stock_batch_id' => 'required|exists:stock_batches,id'
                        ]);
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    // validate material storage & material plant for item 311
                    $material_plant_item = MaterialPlant::where('plant_id', $val['plant_id'])
                    ->where('material_id', $val['material_id'])->first();

                    $plant_item = Plant::find($val['plant_id']);
                    
                    if (!$material_plant_item) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant_item->code.'']
                            ]
                        ], 422);
                    }

                    $material_storage_item = MaterialStorage::where('storage_id', $val['storage_id'])
                    ->where('material_id', $val['material_id'])->first();

                    $storage_item = MasterStorage::find($val['storage_id']);

                    if (!$material_storage_item) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage_item->code.'']
                            ]
                        ], 422);
                    }

                    break;
                case 605:
                    // GR from STO(Intransit to Unres)

                    // validate material
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                    ]);

                    $material = Material::find($val['material_id']);

                    // Validasi qty cant more than delivery qty
                    $cekdo = DeliveryHeader::where('delivery_code', $data->delivery_no)->first();
                    if ($val['batch']['batch']) {
                        $cekdoitem = $cekdo->item->where('material_id', $val['material_id'])
                            ->where('batch', $val['batch']['batch'])
                            ->first();
                    } else {
                        $cekdoitem = $cekdo->item->where('material_id', $val['material_id'])->first();
                    }

                    // validate qty not bigger than do
                    // total GR item DO
                    $mvtype = MovementType::where('code', 605)->first();
                    $gr_do_item = MovementHistory::where('delivery_no', $data->delivery_no)
                        ->where('movement_type_id', $mvtype->id)
                        ->where('de_ce', 'd')
                        ->where('material_id', $val['material_id'])
                        ->where('batch_no', $cekdoitem->batch)
                        ->get();

                    if ($gr_do_item) {
                        $gr_item = $gr_do_item->sum('quantity_entry');
                    } else {
                        $gr_item = 0;
                    }

                    $sisa_do = $cekdoitem->pick_qty - $gr_item;

                    if ($val['quantity_entry'] > $sisa_do) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.quantity_entry'  => ['Qty Cant greather than '.$sisa_do.' (Delivery Qty)']
                            ]
                        ], 422);
                    }

                    // validasi serial profile
                    if ($material->serial_profile) {
                        $this->validate(request(), [
                            'item.'.$key.'.serial' => 'required|array',
                            'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                        ]);

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in material plant
                    if ($material_plant->batch_ind) {
                        $this->validate(request(), [
                            'item.'.$key.'.batch.batch' => 'required|exists:stock_batches,batch_number'
                        ]);
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    break;
                case 101:
                    // GR from PO
                    // TODO handle split item                    
                    // validate tipe po item
                    // 0. material for stock
                    // 1. free text / material for cost center, jika tipe ini wajib mengisi cost center pada
                    // 2. material for asset
                    $this->validate(request(), [
                        'item.*.account_type' => 'required|numeric|min:0|max:2',
                    ]);

                    if ($val['account_type'] == 1) {
                        // material cost center type
                        // validate material
                        $this->validate(request(), [
                            'item.*.material_id' => 'nullable|exists:materials,id',
                            'item.*.material_description' => 'required|max:191|string',
                        ]);
                    } else if ($val['account_type'] == 0) {
                        // material stock type
                        // validate material
                        $this->validate(request(), [
                            'item.*.material_id' => 'required|exists:materials,id',
                        ]);
                    } else if ($val['account_type'] == 2) {
                        // material asset type
                        // validate material
                        $this->validate(request(), [
                            'item.*.material_id' => 'required|exists:materials,id',
                        ]);
                    }

                    $mvt_gr_po = MovementType::where('code', 101)->first();

                    if ($val['material_id']) {
                        // validation
                        $material = Material::find($val['material_id']);

                        // validate material plant
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $plant = Plant::find($data->plant_id);
                        
                        if (!$material_plant) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                                ]
                            ], 422);
                        }

                        $cekmvt = MovementType::find($data->movement_type_id);

                        // cek batch indicator in material plant
                        if ($material_plant->batch_ind) {
                            // cek batch indicator in movement type
                            if ($cekmvt->batch_ind) {
                                // cek global setting
                                if (appsetting('EXPIRY_BATCH')) {
                                    $expiry_required = 'required';
                                } else {
                                    $expiry_required = 'nullable';
                                }
                                
                                $this->validate(request(), [
                                    'item.'.$key.'.batch.manuf_date' => 'nullable|date',
                                    'item.'.$key.'.batch.gr_date' => 'nullable|date|required_if:item.'.$key.'.batch.manuf_date,',
                                    'item.'.$key.'.batch.sled_date' => ''.$expiry_required.'|date',
                                    'item.'.$key.'.batch.vendor_id' => 'nullable|exists:suppliers,id',
                                    'item.'.$key.'.batch.vendor_batch' => 'nullable',
                                    'item.'.$key.'.batch.license' => 'nullable',
                                    'item.'.$key.'.batch.unit_cost' => 'nullable'
                                ]);
                            }
                        }

                        // validate material storage
                        $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $storage = MasterStorage::find($data->storage_id);

                        if (!$material_storage) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                                ]
                            ], 422);
                        }

                        // validate serial
                        if ($material->serial_profile) {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'unique:stock_serials,serial',
                            ]);

                            // validate unique serial
                            $dups = [];
                            foreach (array_count_values($val['serial']) as $valk => $c) {
                                if($c > 1) $dups[] = $valk;
                            }

                            if ($dups) {
                                return response()->json([
                                    'message'   => 'Data invalid',
                                    'errors'    => [
                                        'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                    ]
                                ], 422);
                            }

                            // validate total serial
                            $total_serial = count($val['serial']);
                            if ($val['quantity_entry'] != $total_serial) {
                                return response()->json([
                                    'message'   => 'Data invalid',
                                    'errors'    => [
                                        'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                    ]
                                ], 422);
                            }
                        }

                        // validate qty not bigger than po
                        $po = PurchaseHeader::where('po_doc_no', $data->po_number)->first();

                        // total po item qty
                        $po_item = $po->item->where('material_id', $val['material_id'])->first();

                        // total GR item PO
                        $gr_po_item = MovementHistory::where('material_doc', '!=', $data->material_doc)
                            ->where('po_number', $data->po_number)
                            ->where('material_id', $val['material_id'])
                            ->where('movement_type_id', $mvt_gr_po->id)
                            ->get();

                        if ($gr_po_item) {
                            $gr_item = $gr_po_item->sum('quantity_entry');
                        } else {
                            $gr_item = 0;
                        }

                        // sisa PO yg belum di GR
                        $sisa_po = $po_item->order_qty - $gr_item;

                        if ($val['quantity_entry'] > $sisa_po) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.quantity_entry'  => ['Qty Cant greather than '.$sisa_po.' (PO Qty)']
                                ]
                            ], 422);
                        }
                    } else {
                        // validate qty not bigger than po
                        $po = PurchaseHeader::where('po_doc_no', $data->po_number)->first();

                        // total po item qty
                        $po_item = $po->item->where('short_text', $val['material_description'])->first();

                        // total GR item PO
                        $gr_po_item = MovementHistory::where('material_doc', '!=', $data->material_doc)
                            ->where('po_number', $data->po_number)
                            ->where('material_description', $val['material_description'])
                            ->where('movement_type_id', $mvt_gr_po->id)
                            ->get();

                        if ($gr_po_item) {
                            $gr_item = $gr_po_item->sum('quantity_entry');
                        } else {
                            $gr_item = 0;
                        }

                        // sisa PO yg belum di GR
                        $sisa_po = $po_item->order_qty - $gr_item;

                        if ($val['quantity_entry'] > $sisa_po) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.quantity_entry'  => ['Qty Cant greather than '.$sisa_po.' (PO Qty)']
                                ]
                            ], 422);
                        }
                    }

                    break;
                case 131:
                    // GR from Production Order

                    // validate material
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                    ]);

                    $material = Material::find($val['material_id']);

                    // validasi serial profile
                    if ($material->serial_profile) {
                        $this->validate(request(), [
                            'item.'.$key.'.serial' => 'required|array',
                            'item.'.$key.'.serial.*' => 'unique:stock_serials,serial',
                        ]);

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in movement type
                    if ($cekmvt->batch_ind) {
                        // cek global setting
                        if (appsetting('EXPIRY_BATCH')) {
                            $expiry_required = 'required';
                        } else {
                            $expiry_required = 'nullable';
                        }
                        
                        $this->validate(request(), [
                            'item.'.$key.'.batch.manuf_date' => 'nullable|date',
                            'item.'.$key.'.batch.gr_date' => 'nullable|date|required_if:item.'.$key.'.batch.manuf_date,',
                            'item.'.$key.'.batch.sled_date' => ''.$expiry_required.'|date',
                            'item.'.$key.'.batch.vendor_id' => 'nullable|exists:suppliers,id',
                            'item.'.$key.'.batch.vendor_batch' => 'nullable',
                            'item.'.$key.'.batch.license' => 'nullable',
                            'item.'.$key.'.batch.unit_cost' => 'nullable'
                        ]);
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    break;
                case 241:
                    // Goods Issue to Asset
                    
                    // validate material
                    $this->validate(request(), [
                        'item.*.material_id' => 'required|exists:materials,id',
                    ]);

                    // validasi serial profile
                    $material = Material::find($val['material_id']);
                    if ($material->serial_profile) {
                        if (strtolower($data->de_ce) == "c") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                            ]);
                        } elseif (strtolower($data->de_ce) == "d") {
                            $this->validate(request(), [
                                'item.'.$key.'.serial' => 'required|array',
                                'item.'.$key.'.serial.*' => 'unique:stock_serials,serial',
                            ]);
                        }

                        // validate unique serial
                        $dups = [];
                        foreach (array_count_values($val['serial']) as $valk => $c) {
                            if($c > 1) $dups[] = $valk;
                        }

                        if ($dups) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                                ]
                            ], 422);
                        }

                        // validate total serial
                        $total_serial = count($val['serial']);
                        if ($val['quantity_entry'] != $total_serial) {
                            return response()->json([
                                'message'   => 'Data invalid',
                                'errors'    => [
                                    'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                                ]
                            ], 422);
                        }
                    }

                    // validate material plant batch creation indicator
                    $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $plant = Plant::find($data->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    $cekmvt = MovementType::find($data->movement_type_id);

                    // cek batch indicator in material plant
                    if ($material_plant->batch_ind) {
                        $this->validate(request(), [
                            'item.'.$key.'.batch.stock_batch_id' => 'required|exists:stock_batches,id'
                        ]);
                    }

                    // validate material storage
                    $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $storage = MasterStorage::find($data->storage_id);

                    if (!$material_storage) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material storage '.$storage->code.'']
                            ]
                        ], 422);
                    }

                    break;
                default:
                    return response()->json([
                        'message' => 'movement type '.$movement_type.', will be handled soon',
                    ], 422);
            }
        }
        
        // save
        try {
            DB::beginTransaction();
            
            $movement_type = MovementType::find($data->movement_type_id)->code;
            switch ($movement_type) {
                case 561:
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        $material = Material::find($val['material_id']);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($material->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity_entry'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                            }
                        }

                        // get material cost from material acount
                        $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $map = $material_account ? $material_account->moving_price : 0;

                        // record to movement history table
                        $movement = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => $data->de_ce,
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $item++,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $val['uom_id'],

                            // convert qty
                            'quantity' => $convert_qty,
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $map,
                            'currency' => 'IDR',

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // get material account
                        $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();
                        
                        // if material account exist
                        if ($material_account) {
                            // if moving average in $material_plant
                            if ($material_account->price_controll === 0) {
                                // calculate moving price in stock
                                // get stock material in plant
                                $plant_stock = StockMain::where('material_id', $val['material_id'])
                                    ->where('plant_id', $data->plant_id)
                                    ->get();

                                if (count($plant_stock) != 0) {
                                    // sum total stock material in plant
                                    $total_plant_stock = $plant_stock->sum('qty_unrestricted');

                                    // get moving price in material account
                                    $map_cost = $material_account->moving_price;

                                    // get total map cost
                                    $total_map_cost = $map_cost * (float)$total_plant_stock;

                                    // get gr qty
                                    $gr_qty = (float)$movement->quantity;
                                    // get unit cost from batch
                                    $gr_cost = isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0;

                                    // get total gr cost
                                    $total_gr_cost = $gr_cost * (float)$gr_qty;

                                    // total gr + map
                                    $total_gr_map_cost = $total_gr_cost + $total_map_cost;

                                    // total stock + total gr qty
                                    $total_gr_stock = $total_plant_stock + $gr_qty;

                                    // get new moving price
                                    $new_map_after_gr = $total_gr_map_cost / $total_gr_stock;

                                    // \Log::info('total_plant_stock '.$total_plant_stock.'');
                                    // \Log::info('map_cost '.$map_cost.'');
                                    // \Log::info('total_map_cost '.$total_map_cost.'');
                                    // \Log::info('gr_qty '.$gr_qty.'');
                                    // \Log::info('gr_cost '.$gr_cost.'');
                                    // \Log::info('total_gr_cost '.$total_gr_cost.'');
                                    // \Log::info('total_gr_map_cost '.$total_gr_map_cost.'');
                                    // \Log::info('total_gr_stock '.$total_gr_stock.'');
                                    // \Log::info('new_map_after_gr '.$new_map_after_gr.'');

                                    // Record Moving price History 
                                    MovingPriceHistory::create([
                                        'plant_id' => $movement->plant_id,
                                        'material_id' => $movement->material_id,
                                        'movement_history_id' => $movement->id,
                                        'gr_date' => now(),
                                        'gr_price' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                        'gr_quantity' => (float)$movement->quantity,
                                        'moving_price' => $new_map_after_gr
                                    ]);

                                    // update material account
                                    $material_account->update([
                                        'moving_price' => $new_map_after_gr,
                                        // 'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }

                        // cek stock material in storage
                        $exist_stock = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $data->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($val['material_id']);

                        // update if stock exist
                        if ($exist_stock) {
                            $qty = $exist_stock->qty_unrestricted + $convert_qty;

                            $exist_stock->update([
                                'qty_unrestricted' => $qty,
                                'updated_by' => Auth::user()->id
                            ]);

                            $stock = $exist_stock;
                        } else {
                            // create if stock not exist
                            $stock = StockMain::create([
                                'material_id' => $val['material_id'],
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'qty_unrestricted' => $convert_qty,
                                'qty_blocked' => 0,
                                'qty_transit' => 0,
                                'qty_qa' => 0,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);
                        }

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $cekmvt = MovementType::find($data->movement_type_id);

                        if ($material_plant->batch_ind) {
                            if ($cekmvt->batch_ind) {
                                // create stock batch
                                $batch = StockBatch::create([
                                    'stock_main_id' => $stock->id,
                                    'batch_number' => $batch_number,
                                    'manuf_date' => $val['batch']['manuf_date'],
                                    'gr_date' => $val['batch']['gr_date'],
                                    'sled_date' => $val['batch']['sled_date'],
                                    'vendor_id' => $val['batch']['vendor_id'],
                                    'vendor_batch' => $val['batch']['vendor_batch'],
                                    'license' => $val['batch']['license'],
                                    'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                    'qty_unrestricted' => $convert_qty,
                                    'qty_blocked' => 0,
                                    'qty_transit' => 0,
                                    'qty_qa' => 0,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                ]);

                                // update batch number with last batch
                                TransactionCode::where('code', 'BN')->update([
                                    'lastcode' => $batch_number
                                ]);

                                // update batch in movement
                                $movement->update(['batch_no' => $batch->batch_number]);
                            }
                        }

                        // validate & insert serial to stock serial, serial history
                        $material = Material::find($val['material_id']);
                        if ($material->serial_profile) {
                            foreach($val['serial'] as $serial){
                                // insert into stock serial table
                                StockSerial::create([
                                    'material_id' => $movement->material_id,
                                    'plant_id' => $movement->plant_id,
                                    'storage_id' => $movement->storage_id,
                                    'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                    'serial' => $serial,
                                    'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                    'batch_number' => $movement->batch_no, //get batch no from movement
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                    'vendor_id' => null,
                                    'customer_id' => null, // will be used in 601
                                ]);

                                // record serial history
                                SerialHistory::create([
                                    'movement_history_id' => $movement->id,
                                    'plant_id' => $movement->plant_id,
                                    'storage_id' => $movement->storage_id,
                                    'material_id' => $movement->material_id,
                                    'serial' => $serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    // delete header
                    $data->delete();

                    break;
                case 201:
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        $material = Material::find($val['material_id']);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($material->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity_entry'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                            }
                        }

                        // get material cost from material acount
                        $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $map = $material_account ? $material_account->moving_price : 0;

                        // record to movement history table
                        $movement = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => $data->de_ce,
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $item++,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $val['uom_id'],

                            // convert qty
                            'quantity' => $convert_qty,
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : $map,
                            'currency' => 'IDR',

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // cek stock material in storage
                        $exist_stock = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $data->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($val['material_id']);

                        // update if stock exist
                        if ($exist_stock) {
                            // throw new exception if stock not enought
                            if ($exist_stock->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock material '.$material->description.', is not sufficient');
                            }
                            $qty = $exist_stock->qty_unrestricted - $convert_qty;

                            $exist_stock->update([
                                'qty_unrestricted' => $qty,
                                'updated_by' => Auth::user()->id
                            ]);

                            $stock = $exist_stock;
                        } else {
                            // throw new exception if stock not enought
                            throw new \Exception('Stock material '.$material->description.', is not sufficient');
                        }

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $cekmvt = MovementType::find($data->movement_type_id);

                        $material = Material::find($val['material_id']);

                        if ($material_plant->batch_ind) {
                            $selected_batch = StockBatch::find($val['batch']['stock_batch_id']);

                            // get stock batch
                            $batch = $stock->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                            // if stock batch qty unres < qty
                            if ($batch->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock batch '.$selected_batch->batch_number.' material '.$material->description.', is not sufficient');
                            }

                            $qty_batch = $batch->qty_unrestricted - $convert_qty;

                            // update batch qty unres - 201
                            $batch->update([
                                'qty_unrestricted' => $qty_batch,
                                'updated_by' => Auth::user()->id,
                            ]);

                            // update batch in movement
                            $movement->update(['batch_no' => $batch->batch_number]);

                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement->material_id)
                                        ->where('storage_id', $movement->storage_id)
                                        ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => null,
                                        'storage_id' => null,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        } else {
                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement->material_id)
                                        ->where('storage_id', $movement->storage_id)
                                        // ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => null,
                                        'storage_id' => null,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }
                    }

                    // delete header
                    $data->delete();

                    break;
                case 311:
                    foreach ($request->item as $key => $val) {
                        $movements_document = MovementHistory::where('material_doc', $data->material_doc)
                            ->whereNotNull('material_id')
                            ->count();

                        $credit_item_number = $movements_document + 1;
                        $debit_item_number = $credit_item_number + 1;

                        $material = Material::find($val['material_id']);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($material->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity_entry'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                            }
                        }

                        // get material cost from material acount
                        $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $map = $material_account ? $material_account->moving_price : 0;

                        // record to movement history table
                        $movement_credit = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => 'c',
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $credit_item_number,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $val['uom_id'],

                            // convert qty
                            'quantity' => $convert_qty,
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : $map,
                            'currency' => 'IDR',

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        $movement_debit = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => 'd',
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $val['plant_id'],
                            'storage_id' => $val['storage_id'],
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $debit_item_number,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $val['uom_id'],

                            // convert qty
                            'quantity' => $convert_qty,
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : $map,
                            'currency' => 'IDR',

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // cek stock material in storage sender
                        $stock_pengirim = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $data->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($val['material_id']);

                        // update if stock exist
                        if ($stock_pengirim) {
                            // throw new exception if stock not enought
                            if ($stock_pengirim->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock material '.$material->description.', is not sufficient');
                            }
                            $qty = $stock_pengirim->qty_unrestricted - $convert_qty;

                            $stock_pengirim->update([
                                'qty_unrestricted' => $qty,
                                'updated_by' => Auth::user()->id
                            ]);

                            // insert to stock penerima
                            $stock_penerima = StockMain::where('material_id', $val['material_id'])
                                ->where('storage_id', $val['storage_id'])
                                ->first();

                            if ($stock_penerima) {
                                $qty_penerima = $stock_penerima->qty_unrestricted + $convert_qty;
                                $stock_penerima->update([
                                    'qty_unrestricted' => $qty_penerima,
                                    'updated_by' => Auth::user()->id
                                ]);
                            } else {
                                $stock_penerima = StockMain::create([
                                    'material_id' => $val['material_id'],
                                    'plant_id' => $val['plant_id'],
                                    'storage_id' => $val['storage_id'],
                                    'qty_unrestricted' => $convert_qty,
                                    'qty_blocked' => 0,
                                    'qty_transit' => 0,
                                    'qty_qa' => 0,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }

                            $stock_credit = $stock_pengirim;
                            $stock_debit = $stock_penerima;
                        } else {
                            // throw new exception if stock not enought
                            throw new \Exception('Stock material '.$material->description.', is not sufficient');
                        }

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $cekmvt = MovementType::find($data->movement_type_id);

                        $material = Material::find($val['material_id']);

                        if ($material_plant->batch_ind) {
                            $selected_batch = StockBatch::find($val['batch']['stock_batch_id']);
                            
                            // get stock batch pengirim
                            $batch_credit = $stock_credit->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                            // if stock batch pengirim qty unres < qty
                            if ($batch_credit->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock batch '.$selected_batch->batch_number.' material '.$material->description.', is not sufficient');
                            }

                            $qty_batch_pengirim = $batch_credit->qty_unrestricted - $convert_qty;

                            // update batch pengirim qty unres
                            $batch_credit->update([
                                'qty_unrestricted' => $qty_batch_pengirim,
                                'updated_by' => Auth::user()->id,
                            ]);

                            // get stock batch penerima
                            $batch_debit = $stock_debit->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                            if ($batch_debit) {//update batch stock penerima
                                $qty_batch_penerima = $batch_debit->qty_unrestricted + $convert_qty;

                                $batch_debit->update([
                                    'qty_unrestricted' => $qty_batch_penerima,
                                    'updated_by' => Auth::user()->id,
                                ]);
                            } else {
                                // create batch stock penerima
                                $batch_debit = StockBatch::create([
                                    'stock_main_id' => $stock_debit->id,
                                    'batch_number' => $selected_batch->batch_number,
                                    'manuf_date' => $selected_batch->manuf_date,
                                    'gr_date' => $selected_batch->gr_date,
                                    'sled_date' => $selected_batch->sled_date,
                                    'vendor_id' => $selected_batch->vendor_id,
                                    'vendor_batch' => $selected_batch->vendor_batch,
                                    'license' => $selected_batch->license,
                                    'unit_cost' => $selected_batch->unit_cost,
                                    'qty_unrestricted' => $convert_qty,
                                    'qty_blocked' => 0,
                                    'qty_transit' => 0,
                                    'qty_qa' => 0,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                ]);
                            }

                            // update batch in movement
                            $movement_credit->update(['batch_no' => $batch_credit->batch_number]);
                            $movement_debit->update(['batch_no' => $batch_debit->batch_number]);

                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement_credit->material_id)
                                        ->where('storage_id', $movement_credit->storage_id)
                                        ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history credit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_credit->id,
                                        'plant_id' => $movement_credit->plant_id,
                                        'storage_id' => $movement_credit->storage_id,
                                        'material_id' => $movement_credit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    // record serial history debit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_debit->id,
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'material_id' => $movement_debit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        } else {
                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement_credit->material_id)
                                        ->where('storage_id', $movement_credit->storage_id)
                                        // ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history credit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_credit->id,
                                        'plant_id' => $movement_credit->plant_id,
                                        'storage_id' => $movement_credit->storage_id,
                                        'material_id' => $movement_credit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    // record serial history debit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_debit->id,
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'material_id' => $movement_debit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }
                    }

                    // delete header
                    MovementHistory::where('material_doc', $data->material_doc)
                    ->whereNull('material_id')
                    ->delete();

                    break;
                case 605:
                    // get delivery header
                    $do = DeliveryHeader::where('delivery_code', $data->delivery_no)->first();

                    $material_document_gi = $this->getMaterialDoc();

                    foreach ($request->item as $key => $val) {
                        $movements_document = MovementHistory::where('material_doc', $data->material_doc)
                            ->whereNotNull('material_id')
                            ->count();

                        $credit_item_number = $movements_document + 1;
                        $debit_item_number = $credit_item_number + 1;

                        $material = Material::find($val['material_id']);

                        // get do item
                        $do_material = $do->item->where('material_id', $val['material_id'])->first();

                        // get material cost from material acount
                        $material_account = MaterialAccount::where('plant_id', $do_material->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $map = $material_account ? $material_account->moving_price : 0;

                        // record to movement history table (credit from intransit sender)
                        $movement_credit = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => 'c',
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $do_material->plant_id,
                            'storage_id' => $do_material->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $credit_item_number,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $material->uom_id,

                            'quantity' => $val['quantity_entry'],
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['batch']) ? StockBatch::where('batch_number', $val['batch']['batch'])->first()->unit_cost : $map,
                            'currency' => 'IDR',

                            'delivery_no' => $data->delivery_no,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // record to movement history table (debit to unres reciever)
                        $movement_debit = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => 'd',
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $debit_item_number,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $material->uom_id,

                            'quantity' => $val['quantity_entry'],
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['batch']) ? StockBatch::where('batch_number', $val['batch']['batch'])->first()->unit_cost : $map,
                            'currency' => 'IDR',

                            'delivery_no' => $data->delivery_no,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // cek stock material in storage sender
                        $stock_pengirim = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $do_material->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($val['material_id']);

                        // update if stock exist
                        if ($stock_pengirim) {
                            // throw new exception if stock not enought
                            if ($stock_pengirim->qty_transit < $val['quantity_entry']) {
                                throw new \Exception('Stock material '.$material->description.', is not sufficient');
                            }
                            $qty_transit = $stock_pengirim->qty_transit - $val['quantity_entry'];

                            $stock_pengirim->update([
                                'qty_transit' => $qty_transit,
                                'updated_by' => Auth::user()->id
                            ]);

                            $stock_penerima = StockMain::where('material_id', $val['material_id'])
                                ->where('storage_id', $data->storage_id)
                                ->first();

                            if ($stock_penerima) {
                                $qty_unrestricted = $stock_penerima->qty_unrestricted + $val['quantity_entry'];
                                $stock_penerima->update([
                                    'qty_unrestricted' => $qty_unrestricted,
                                    'updated_by' => Auth::user()->id
                                ]);
                            } else {
                                $stock_penerima = StockMain::create([
                                    'material_id' => $val['material_id'],
                                    'plant_id' => $data->plant_id,
                                    'storage_id' => $data->storage_id,
                                    'qty_unrestricted' => $val['quantity_entry'],
                                    'qty_blocked' => 0,
                                    'qty_transit' => 0,
                                    'qty_qa' => 0,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }

                            $stock_credit = $stock_pengirim;
                            $stock_debit = $stock_penerima;
                        } else {
                            // throw new exception if stock not enought
                            throw new \Exception('Stock material '.$material->description.', is not sufficient');
                        }

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant reciever batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        // validate material plant sender batch creation indicator
                        $material_plant_sender = MaterialPlant::where('plant_id', $do_material->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        // get plant sender
                        $plant_sender = Plant::find($do_material->plant_id);

                        // get plant reciver
                        $plant_reciver = Plant::find($data->plant_id);

                        if ($material_plant->batch_ind != $material_plant_sender->batch_ind) {
                            throw new \Exception('Batch indicator for material '.$material->description.' in plant '.$plant_sender->description.' & plant '.$plant_reciver->description.' not match');
                        }

                        $cekmvt = MovementType::find($data->movement_type_id);

                        $material = Material::find($val['material_id']);

                        if ($material_plant->batch_ind) {
                            $selected_batch = StockBatch::where('batch_number', $val['batch']['batch'])->first();

                            // get stock batch pengirim
                            $batch_credit = $stock_credit->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                            // if stock batch pengirim qty transit < qty
                            if ($batch_credit->qty_transit < $val['quantity_entry']) {
                                throw new \Exception('Stock batch '.$selected_batch->batch_number.' material '.$material->description.', is not sufficient');
                            }

                            if ($batch_credit) {//update batch stock penerima
                                $batch_qty_transit = $batch_credit->qty_transit - $val['quantity_entry'];

                                $batch_credit->update([
                                    'qty_transit' => $batch_qty_transit,
                                    'updated_by' => Auth::user()->id,
                                ]);

                                $batch_debit = $stock_debit->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                                if ($batch_debit) {
                                    $batch_qty_unrestricted = $batch_debit->qty_unrestricted + $val['quantity_entry'];

                                    $batch_debit->update([
                                        'qty_unrestricted' => $batch_qty_unrestricted,
                                        'updated_by' => Auth::user()->id,
                                    ]);
                                } else {
                                    $batch_debit = StockBatch::create([
                                        'stock_main_id' => $stock_debit->id,
                                        'batch_number' => $selected_batch->batch_number,
                                        'manuf_date' => $selected_batch->manuf_date,
                                        'gr_date' => $selected_batch->gr_date,
                                        'sled_date' => $selected_batch->sled_date,
                                        'vendor_id' => $selected_batch->vendor_id,
                                        'vendor_batch' => $selected_batch->vendor_batch,
                                        'license' => $selected_batch->license,
                                        'unit_cost' => $selected_batch->unit_cost,
                                        'qty_unrestricted' => $val['quantity_entry'],
                                        'qty_blocked' => 0,
                                        'qty_transit' => 0,
                                        'qty_qa' => 0,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                    ]);
                                }
                            }

                            // update batch in movement
                            $movement_credit->update(['batch_no' => $batch_credit->batch_number]);
                            $movement_debit->update(['batch_no' => $batch_credit->batch_number]);

                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement_credit->material_id)
                                        ->where('storage_id', $movement_credit->storage_id)
                                        ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 2)//stock type must transit
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history credit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_credit->id,
                                        'plant_id' => $movement_credit->plant_id,
                                        'storage_id' => $movement_credit->storage_id,
                                        'material_id' => $movement_credit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    // record serial history debit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_debit->id,
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'material_id' => $movement_debit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        } else {
                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement_credit->material_id)
                                        ->where('storage_id', $movement_credit->storage_id)
                                        // ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 2)//stock type must transit
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history credit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_credit->id,
                                        'plant_id' => $movement_credit->plant_id,
                                        'storage_id' => $movement_credit->storage_id,
                                        'material_id' => $movement_credit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    // record serial history debit
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_debit->id,
                                        'plant_id' => $movement_debit->plant_id,
                                        'storage_id' => $movement_debit->storage_id,
                                        'material_id' => $movement_debit->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }                        

                        // validate material storage bin indicator
                        $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        // jika bin false dapatkan cost center dari work center, jika supply & result storage match
                        if ($material_storage->bin_indicator == false) {
                            $work_ctr = WorkCenter::where('storage_supply_id', $data->storage_id)
                                ->where('storage_result_id', $data->storage_id)
                                ->first();

                            if (!$work_ctr) {
                                throw new \Exception('Storage '.MasterStorage::find($data->storage_id)->description.', not have cost center');
                            }

                            $cost_ctr = CostCenter::find($work_ctr->cost_center_id);

                            // get 201 mvt
                            $gi_mvt = MovementType::where('code', 201)->first();

                            // record 201 movement
                            $gi_movement = MovementHistory::create([
                                'material_doc' => $material_document_gi,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $gi_mvt->id,
                                'de_ce' => $gi_mvt->dc_ind,
                                'movement_type_reason_id' => null,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $work_ctr->id,
                                'cost_center_id' => $cost_ctr->id,

                                'item_no' => $movement_credit->item_no,
                                'material_id' => $movement_credit->material_id,
                                'material_description' => $movement_credit->material_description,
                                'quantity_entry' => $movement_credit->quantity_entry,
                                'entry_uom_id' => $movement_credit->entry_uom_id,

                                // convert qty
                                'quantity' => $movement_credit->quantity,
                                'base_uom_id' => $movement_credit->base_uom_id,
                                'amount' => isset($val['batch']['batch']) ? StockBatch::where('batch_number', $val['batch']['batch'])->first()->unit_cost : $map,
                                'currency' => 'IDR',

                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // cek stock material in storage
                            $exist_stock_gi = StockMain::where('material_id', $val['material_id'])
                                ->where('storage_id', $data->storage_id)
                                ->first();
                            
                            $qty_gi = $exist_stock_gi->qty_unrestricted - $gi_movement->quantity;
                            
                            $exist_stock_gi->update([
                                'qty_unrestricted' => $qty_gi,
                                'updated_by' => Auth::user()->id
                            ]);

                            $material = Material::find($val['material_id']);

                            // update batch stock
                            if ($material_plant->batch_ind) {
                                $selected_batch = StockBatch::where('batch_number', $val['batch']['batch'])->first();
    
                                // get stock batch
                                $batch_gi = $exist_stock_gi->batch_stock->where('batch_number', $selected_batch->batch_number)->first();
    
                                $qty_batch_gi = $batch_gi->qty_unrestricted - $gi_movement->quantity;
    
                                // update batch qty unres - 201
                                $batch_gi->update([
                                    'qty_unrestricted' => $qty_batch_gi,
                                    'updated_by' => Auth::user()->id,
                                ]);
    
                                // update batch in movement gi
                                $gi_movement->update(['batch_no' => $batch_gi->batch_number]);

                                // validate & insert serial to stock serial, serial history
                                if ($material->serial_profile) {
                                    foreach ($val['serial'] as $serial) {
                                        // update stock serial table
                                        $serials = StockSerial::where('serial', $serial)
                                            ->where('material_id', $gi_movement->material_id)
                                            ->where('storage_id', $gi_movement->storage_id)
                                            ->where('batch_number', $selected_batch->batch_number)
                                            ->where('stock_type', 1)//stock type must unrest
                                            ->first();
        
                                        if (!$serials) {
                                            throw new \Exception('Serial '.$serial.', not found');
                                        }
                                        
                                        $serials->update([
                                            'plant_id' => null,
                                            'storage_id' => null,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            // 'serial' => $serial,
                                            'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                            // 'batch_number' => $movement->batch_no, //get batch no from movement
                                            // 'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            // 'vendor_id' => null,
                                            // 'customer_id' => null, // will be used in 601
                                        ]);
        
                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $gi_movement->id,
                                            'plant_id' => $gi_movement->plant_id,
                                            'storage_id' => $gi_movement->storage_id,
                                            'material_id' => $gi_movement->material_id,
                                            'serial' => $serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }
                            } else {
                                // validate & insert serial to stock serial, serial history
                                if ($material->serial_profile) {
                                    foreach ($val['serial'] as $serial) {
                                        // update stock serial table
                                        $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $gi_movement->material_id)
                                        ->where('storage_id', $gi_movement->storage_id)
                                        // ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();
        
                                        if (!$serials) {
                                            throw new \Exception('Serial '.$serial.', not found');
                                        }
                                        
                                        $serials->update([
                                            'plant_id' => null,
                                            'storage_id' => null,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            // 'serial' => $serial,
                                            'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                            // 'batch_number' => $movement->batch_no, //get batch no from movement
                                            // 'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            // 'vendor_id' => null,
                                            // 'customer_id' => null, // will be used in 601
                                        ]);
        
                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $gi_movement->id,
                                            'plant_id' => $gi_movement->plant_id,
                                            'storage_id' => $gi_movement->storage_id,
                                            'material_id' => $gi_movement->material_id,
                                            'serial' => $serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }
                            }
                        }

                        // update gi code document
                        if (isset($gi_movement)) {
                            $code_gi = TransactionCode::where('code', 'M')->first();
                            $code_gi->update(['lastcode' => $material_document_gi]);
                        }
                    }

                    // delete header
                    MovementHistory::where('material_doc', $data->material_doc)
                        ->whereNull('material_id')
                        ->delete();

                    // record do approval
                    DeliveryApproval::create([
                        'delivery_header_id' => $do->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $do->status,
                        'status_to' => 3,
                        'notes' => $movement_debit->doc_header_text,
                    ]);

                    // update do header
                    $do->update([
                        'status' => 3, // 0. Open, 1. Picking/Packing, 2. Sent, 3. Received
                        'updated_by' => Auth::user()->id,
                        'sent_by' => Auth::user()->id,
                        'sent_date' => now()
                    ]);

                    break;
                case 101:
                    // TODO handle split item
                    $po = PurchaseHeader::where('po_doc_no', $data->po_number)->first();

                    $material_document_gi = $this->getMaterialDoc();

                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        // handle per item
                        if ($val['account_type'] == 1) {
                            // material cost center type
                            $material = Material::find($val['material_id']);

                            // get gi movemt type
                            $mvt_gi = MovementType::where('code', 201)->first();
                            
                            // get po item
                            if ($val['material_id']) {
                                // if have master material
                                $po_item = $po->item->where('material_id', $val['material_id'])->first();
                            } else {
                                // if free text
                                $po_item = $po->item->where('short_text', $val['material_description'])->first();
                            }

                            // if app setting UNIT_COST_WITH_TAX true unit cost + tax item
                            if (appsetting('UNIT_COST_WITH_TAX')) {
                                $unit_cost = $po_item->price_unit + $po_item->tax_item;
                            } else {
                                $unit_cost = $po_item->price_unit;
                            }

                            // 101 for GR PO
                            $movement = MovementHistory::create([
                                'material_doc' => $data->material_doc,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $data->movement_type_id,
                                'de_ce' => $data->de_ce,
                                'movement_type_reason_id' => $data->movement_type_reason_id,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $data->cost_center_id,

                                'item_no' => $item++,
                                'material_id' => isset($val['material_id']) ? $val['material_id'] : null,
                                'material_description' => isset($val['material_description']) ? $val['material_description'] : $material->description,
                                'quantity_entry' => $val['quantity_entry'],
                                'entry_uom_id' => $val['uom_id'],

                                // convert qty
                                'quantity' => $val['quantity_entry'],
                                'base_uom_id' => $val['uom_id'],
                                'amount' => $unit_cost,
                                'currency' => 'IDR',

                                'po_number' => $data->po_number,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // then 201 GI to cost center
                            $movement_gi = MovementHistory::create([
                                'material_doc' => $material_document_gi,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $mvt_gi->id,//get 201
                                'de_ce' => $mvt_gi->dc_ind,
                                // 'movement_type_reason_id' => $data->movement_type_reason_id,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $po_item->cost_center_id,

                                'item_no' => $movement->item_no,
                                'material_id' => isset($val['material_id']) ? $val['material_id'] : null,
                                'material_description' => isset($val['material_description']) ? $val['material_description'] : $material->description,
                                'quantity_entry' => $val['quantity_entry'],
                                'entry_uom_id' => $val['uom_id'],

                                // convert qty
                                'quantity' => $val['quantity_entry'],
                                'base_uom_id' => $val['uom_id'],
                                'amount' => $unit_cost,
                                'currency' => 'IDR',

                                'po_number' => $data->po_number,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // get po item
                            if ($val['material_id']) {
                                $po_item_qty = (float)$po_item->order_qty;

                                // sum total gr po item
                                $gr = MovementHistory::where('movement_type_id', $data->movement_type_id)
                                    ->where('po_number', $data->po_number)
                                    ->where('material_id', $val['material_id'])
                                    ->where('movement_type_id', $data->movement_type_id)
                                    ->get();

                                $total_qty_gr = (float)$gr->sum('quantity');

                                // update po item complete if qty is full
                                if ($total_qty_gr >= $po_item_qty) {
                                    $po_item->update([
                                        'completed' => 1,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $total_po_item = $po->item->count();

                                    $total_po_item_completed = $po->item->where('completed', 1)
                                        ->count();

                                    // if po already gr all item update status po to closed
                                    if ($total_po_item == $total_po_item_completed) {
                                        $po->update([
                                            'status' => 4
                                        ]);
                                    } else {
                                        // else update to gr partial
                                        $po->update([
                                            'status' => 6
                                        ]);
                                    }
                                } else {
                                    // else update to gr partial
                                    $po->update([
                                        'status' => 6
                                    ]);
                                }
                            } else {
                                $po_item_qty = (float)$po_item->order_qty;

                                // sum total gr po item
                                $gr = MovementHistory::where('movement_type_id', $data->movement_type_id)
                                    ->where('po_number', $data->po_number)
                                    ->where('material_description', $val['material_description'])
                                    ->where('movement_type_id', $data->movement_type_id)
                                    ->get();

                                $total_qty_gr = (float)$gr->sum('quantity');

                                // update po item complete if qty is full
                                if ($total_qty_gr >= $po_item_qty) {
                                    $po_item->update([
                                        'completed' => 1,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $total_po_item = $po->item->count();

                                    $total_po_item_completed = $po->item->where('completed', 1)
                                        ->count();

                                    // if po already gr all item update status po to closed
                                    if ($total_po_item == $total_po_item_completed) {
                                        $po->update([
                                            'status' => 4
                                        ]);
                                    } else {
                                        // else update to gr partial
                                        $po->update([
                                            'status' => 6
                                        ]);
                                    }
                                } else {
                                    // else update to gr partial
                                    $po->update([
                                        'status' => 6
                                    ]);
                                }
                            }

                        } else if ($val['account_type'] == 0) {
                            // material stock type

                            $material = Material::find($val['material_id']);
                        
                            // uom entry
                            $unit_entry = Uom::find($val['uom_id']);

                            // convert uom
                            if ($material->uom_id == $val['uom_id']) {
                                $convert_qty = $val['quantity_entry'];
                            } else {
                                $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                                if (!$convert_qty) {
                                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                                }
                            }

                            // get po item
                            $po_item = $po->item->where('material_id', $val['material_id'])->first();

                            // if app setting UNIT_COST_WITH_TAX true unit cost + tax item
                            if (appsetting('UNIT_COST_WITH_TAX')) {
                                $unit_cost = $po_item->price_unit + $po_item->tax_item;
                            } else {
                                $unit_cost = $po_item->price_unit;
                            }

                            // record to movement history table
                            $movement = MovementHistory::create([
                                'material_doc' => $data->material_doc,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $data->movement_type_id,
                                'de_ce' => $data->de_ce,
                                'movement_type_reason_id' => $data->movement_type_reason_id,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $data->cost_center_id,

                                'item_no' => $item++,
                                'material_id' => $val['material_id'],
                                'material_description' => $material->description,
                                'quantity_entry' => $val['quantity_entry'],
                                'entry_uom_id' => $val['uom_id'],

                                // convert qty
                                'quantity' => $convert_qty,
                                'base_uom_id' => $material->uom_id,
                                'amount' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $unit_cost,
                                'currency' => 'IDR',

                                'po_number' => $data->po_number,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // get material account
                            $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                                ->where('material_id', $val['material_id'])
                                ->first();
                            
                            // if material account exist
                            if ($material_account) {
                                // if moving average in $material_plant
                                if ($material_account->price_controll === 0) {
                                    // calculate moving price in stock
                                    // get stock material in plant
                                    $plant_stock = StockMain::where('material_id', $val['material_id'])
                                        ->where('plant_id', $data->plant_id)
                                        ->get();

                                    if (count($plant_stock) != 0) {
                                        // sum total stock material in plant
                                        $total_plant_stock = $plant_stock->sum('qty_unrestricted');

                                        // get moving price in material account
                                        $map_cost = $material_account->moving_price;

                                        // get total map cost
                                        $total_map_cost = $map_cost * (float)$total_plant_stock;

                                        // get gr qty
                                        $gr_qty = (float)$movement->quantity;
                                        // get unit cost from batch
                                        $gr_cost = isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0;

                                        // get total gr cost
                                        $total_gr_cost = $gr_cost * (float)$gr_qty;

                                        // total gr + map
                                        $total_gr_map_cost = $total_gr_cost + $total_map_cost;

                                        // total stock + total gr qty
                                        $total_gr_stock = $total_plant_stock + $gr_qty;

                                        // get new moving price
                                        $new_map_after_gr = $total_gr_map_cost / $total_gr_stock;

                                        // \Log::info('total_plant_stock '.$total_plant_stock.'');
                                        // \Log::info('map_cost '.$map_cost.'');
                                        // \Log::info('total_map_cost '.$total_map_cost.'');
                                        // \Log::info('gr_qty '.$gr_qty.'');
                                        // \Log::info('gr_cost '.$gr_cost.'');
                                        // \Log::info('total_gr_cost '.$total_gr_cost.'');
                                        // \Log::info('total_gr_map_cost '.$total_gr_map_cost.'');
                                        // \Log::info('total_gr_stock '.$total_gr_stock.'');
                                        // \Log::info('new_map_after_gr '.$new_map_after_gr.'');

                                        // Record Moving price History 
                                        MovingPriceHistory::create([
                                            'plant_id' => $movement->plant_id,
                                            'material_id' => $movement->material_id,
                                            'movement_history_id' => $movement->id,
                                            'gr_date' => now(),
                                            'gr_price' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                            'gr_quantity' => (float)$movement->quantity,
                                            'moving_price' => $new_map_after_gr
                                        ]);

                                        // update material account
                                        $material_account->update([
                                            'moving_price' => $new_map_after_gr,
                                            // 'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }
                            }

                            // cek stock material in storage
                            $exist_stock = StockMain::where('material_id', $val['material_id'])
                                ->where('storage_id', $data->storage_id)
                                ->first();

                            // get material
                            $material = Material::find($val['material_id']);

                            // cek stock type
                            if ($val['stock_type'] == 1) {//insert as qty qa
                                // update if stock exist
                                if ($exist_stock) {
                                    $qty = $exist_stock->qty_qa + $convert_qty;

                                    $exist_stock->update([
                                        'qty_qa' => $qty,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $stock = $exist_stock;
                                } else {
                                    // create if stock not exist
                                    $stock = StockMain::create([
                                        'material_id' => $val['material_id'],
                                        'plant_id' => $data->plant_id,
                                        'storage_id' => $data->storage_id,
                                        'qty_unrestricted' => 0,
                                        'qty_blocked' => 0,
                                        'qty_transit' => 0,
                                        'qty_qa' => $convert_qty,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            } else {//insert as qty unrest
                                // update if stock exist
                                if ($exist_stock) {
                                    $qty = $exist_stock->qty_unrestricted + $convert_qty;

                                    $exist_stock->update([
                                        'qty_unrestricted' => $qty,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $stock = $exist_stock;
                                } else {
                                    // create if stock not exist
                                    $stock = StockMain::create([
                                        'material_id' => $val['material_id'],
                                        'plant_id' => $data->plant_id,
                                        'storage_id' => $data->storage_id,
                                        'qty_unrestricted' => $convert_qty,
                                        'qty_blocked' => 0,
                                        'qty_transit' => 0,
                                        'qty_qa' => 0,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }

                            // generate new batch number
                            $batch_number = $this->getBatchNumber();

                            // validate material plant batch creation indicator
                            $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                                ->where('material_id', $val['material_id'])
                                ->first();

                            $cekmvt = MovementType::find($data->movement_type_id);

                            if ($material_plant->batch_ind) {
                                if ($cekmvt->batch_ind) {
                                    if ($val['stock_type'] == 1) {//insert as qty qa
                                        // create stock batch
                                        $batch = StockBatch::create([
                                            'stock_main_id' => $stock->id,
                                            'batch_number' => $batch_number,
                                            'manuf_date' => $val['batch']['manuf_date'],
                                            'gr_date' => $val['batch']['gr_date'],
                                            'sled_date' => $val['batch']['sled_date'],
                                            'vendor_id' => $val['batch']['vendor_id'],
                                            'vendor_batch' => $val['batch']['vendor_batch'],
                                            'license' => $val['batch']['license'],
                                            'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                            'qty_unrestricted' => 0,
                                            'qty_blocked' => 0,
                                            'qty_transit' => 0,
                                            'qty_qa' => $convert_qty,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                        ]);
                                    } else {//insert as qty unrest
                                        // create stock batch
                                        $batch = StockBatch::create([
                                            'stock_main_id' => $stock->id,
                                            'batch_number' => $batch_number,
                                            'manuf_date' => $val['batch']['manuf_date'],
                                            'gr_date' => $val['batch']['gr_date'],
                                            'sled_date' => $val['batch']['sled_date'],
                                            'vendor_id' => $val['batch']['vendor_id'],
                                            'vendor_batch' => $val['batch']['vendor_batch'],
                                            'license' => $val['batch']['license'],
                                            'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                            'qty_unrestricted' => $convert_qty,
                                            'qty_blocked' => 0,
                                            'qty_transit' => 0,
                                            'qty_qa' => 0,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                        ]);
                                    }

                                    // update batch number with last batch
                                    TransactionCode::where('code', 'BN')->update([
                                        'lastcode' => $batch_number
                                    ]);

                                    // update batch in movement
                                    $movement->update(['batch_no' => $batch->batch_number]);
                                }
                            }
                            
                            // validate & insert serial to stock serial, serial history
                            $material = Material::find($val['material_id']);
                            if ($material->serial_profile) {
                                foreach($val['serial'] as $serial){
                                    // insert into stock serial table
                                    StockSerial::create([
                                        'material_id' => $movement->material_id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        'batch_number' => $movement->batch_no, //get batch no from movement
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        'vendor_id' => null,
                                        'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }

                            // get po item
                            $po_item = $po->item->where('material_id', $val['material_id'])->first();
                            $po_item_qty = (float)$po_item->order_qty;

                            // sum total gr po item
                            $gr = MovementHistory::where('movement_type_id', $data->movement_type_id)
                                ->where('po_number', $data->po_number)
                                ->where('material_id', $val['material_id'])
                                ->get();

                            $total_qty_gr = (float)$gr->sum('quantity');

                            // update po item complete if qty is full
                            if ($total_qty_gr >= $po_item_qty) {
                                $po_item->update([
                                    'completed' => 1,
                                    'updated_by' => Auth::user()->id
                                ]);

                                $total_po_item = $po->item->count();

                                $total_po_item_completed = $po->item->where('completed', 1)
                                    ->count();

                                // if po already gr all item update status po to closed
                                if ($total_po_item == $total_po_item_completed) {
                                    $po->update([
                                        'status' => 4
                                    ]);
                                } else {
                                    // else update to gr partial
                                    $po->update([
                                        'status' => 6
                                    ]);
                                }
                            } else {
                                // else update to gr partial
                                $po->update([
                                    'status' => 6
                                ]);
                            }

                            // validate material storage bin indicator
                            $material_storage = MaterialStorage::where('storage_id', $data->storage_id)
                                ->where('material_id', $val['material_id'])
                                ->first();

                            // jika bin false dapatkan cost center dari work center, jika supply & result storage match
                            if ($material_storage->bin_indicator == false) {
                                $work_ctr = WorkCenter::where('storage_supply_id', $data->storage_id)
                                    ->where('storage_result_id', $data->storage_id)
                                    ->first();

                                if (!$work_ctr) {
                                    throw new \Exception('Storage '.MasterStorage::find($data->storage_id)->description.', not have cost center');
                                }

                                $cost_ctr = CostCenter::find($work_ctr->cost_center_id);

                                // get 201 mvt
                                $gi_mvt = MovementType::where('code', 201)->first();

                                // record 201 movement
                                $movement_gi = MovementHistory::create([
                                    'material_doc' => $material_document_gi,
                                    'doc_year' => $data->doc_year,
                                    'movement_type_id' => $gi_mvt->id,
                                    'de_ce' => $gi_mvt->dc_ind,
                                    'movement_type_reason_id' => null,
                                    'plant_id' => $data->plant_id,
                                    'storage_id' => $data->storage_id,
                                    'posting_date' => now(),
                                    'document_date' => $data->document_date,
                                    'material_slip' => $data->material_slip,
                                    'doc_header_text' => $data->doc_header_text,
                                    'work_center_id' => $work_ctr->id,
                                    'cost_center_id' => $cost_ctr->id,

                                    'item_no' => $movement->item_no,
                                    'material_id' => $movement->material_id,
                                    'material_description' => $movement->material_description,
                                    'quantity_entry' => $movement->quantity_entry,
                                    'entry_uom_id' => $movement->entry_uom_id,

                                    // convert qty
                                    'quantity' => $movement->quantity,
                                    'base_uom_id' => $movement->base_uom_id,
                                    'amount' => $movement->amount,
                                    'currency' => $movement->currency,

                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // cek stock material in storage
                                $exist_stock_gi = StockMain::where('material_id', $val['material_id'])
                                    ->where('storage_id', $data->storage_id)
                                    ->first();
                                
                                $qty_gi = $exist_stock_gi->qty_unrestricted - $movement_gi->quantity;
                                
                                $exist_stock_gi->update([
                                    'qty_unrestricted' => $qty_gi,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // update batch stock
                                if ($material_plant->batch_ind) {
                                    $selected_batch = StockBatch::where('batch_number', $movement->batch_no)->first();

                                    // get stock batch
                                    $batch_gi = $exist_stock_gi->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                                    $qty_batch_gi = $batch_gi->qty_unrestricted - $movement_gi->quantity;

                                    // update batch qty unres - 201
                                    $batch_gi->update([
                                        'qty_unrestricted' => $qty_batch_gi,
                                        'updated_by' => Auth::user()->id,
                                    ]);

                                    // update batch in movement gi
                                    $movement_gi->update(['batch_no' => $batch_gi->batch_number]);

                                    // validate & insert serial to stock serial, serial history
                                    $material = Material::find($val['material_id']);
                                    if ($material->serial_profile) {
                                        foreach ($val['serial'] as $serial) {
                                            // update stock serial table
                                            $serials = StockSerial::where('serial', $serial)
                                                ->where('material_id', $movement_gi->material_id)
                                                ->where('storage_id', $movement_gi->storage_id)
                                                ->where('batch_number', $selected_batch->batch_number)
                                                ->where('stock_type', 1)//stock type must unrest
                                                ->first();

                                            if (!$serials) {
                                                throw new \Exception('Serial '.$serial.', not found');
                                            }
                                            
                                            $serials->update([
                                                'plant_id' => null,
                                                'storage_id' => null,
                                                'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                                // 'serial' => $serial,
                                                'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                                // 'batch_number' => $movement->batch_no, //get batch no from movement
                                                // 'created_by' => Auth::user()->id,
                                                'updated_by' => Auth::user()->id,
                                                // 'vendor_id' => null,
                                                // 'customer_id' => null, // will be used in 601
                                            ]);

                                            // record serial history
                                            SerialHistory::create([
                                                'movement_history_id' => $movement_gi->id,
                                                'plant_id' => $movement_gi->plant_id,
                                                'storage_id' => $movement_gi->storage_id,
                                                'material_id' => $movement_gi->material_id,
                                                'serial' => $serial,
                                                'created_by' => Auth::user()->id,
                                                'updated_by' => Auth::user()->id
                                            ]);
                                        }
                                    }
                                } else {
                                    // validate & insert serial to stock serial, serial history
                                    $material = Material::find($val['material_id']);
                                    if ($material->serial_profile) {
                                        foreach ($val['serial'] as $serial) {
                                            // update stock serial table
                                            $serials = StockSerial::where('serial', $serial)
                                                ->where('material_id', $movement_gi->material_id)
                                                ->where('storage_id', $movement_gi->storage_id)
                                                // ->where('batch_number', $selected_batch->batch_number)
                                                ->where('stock_type', 1)//stock type must unrest
                                                ->first();

                                            if (!$serials) {
                                                throw new \Exception('Serial '.$serial.', not found');
                                            }
                                            
                                            $serials->update([
                                                'plant_id' => null,
                                                'storage_id' => null,
                                                'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                                // 'serial' => $serial,
                                                'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                                                // 'batch_number' => $movement->batch_no, //get batch no from movement
                                                // 'created_by' => Auth::user()->id,
                                                'updated_by' => Auth::user()->id,
                                                // 'vendor_id' => null,
                                                // 'customer_id' => null, // will be used in 601
                                            ]);

                                            // record serial history
                                            SerialHistory::create([
                                                'movement_history_id' => $movement_gi->id,
                                                'plant_id' => $movement_gi->plant_id,
                                                'storage_id' => $movement_gi->storage_id,
                                                'material_id' => $movement_gi->material_id,
                                                'serial' => $serial,
                                                'created_by' => Auth::user()->id,
                                                'updated_by' => Auth::user()->id
                                            ]);
                                        }
                                    }
                                }
                            }

                        } else if ($val['account_type'] == 2) {
                            // material asset type
                            // store in movement history gr po
                            $material = Material::find($val['material_id']);
                        
                            // uom entry
                            $unit_entry = Uom::find($val['uom_id']);

                            // convert uom
                            if ($material->uom_id == $val['uom_id']) {
                                $convert_qty = $val['quantity_entry'];
                            } else {
                                $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                                if (!$convert_qty) {
                                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                                }
                            }

                            // get po item
                            $po_item = $po->item->where('material_id', $val['material_id'])->first();

                            // if app setting UNIT_COST_WITH_TAX true unit cost + tax item
                            if (appsetting('UNIT_COST_WITH_TAX')) {
                                $unit_cost = $po_item->price_unit + $po_item->tax_item;
                            } else {
                                $unit_cost = $po_item->price_unit;
                            }

                            // record to movement history table
                            $movement = MovementHistory::create([
                                'material_doc' => $data->material_doc,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $data->movement_type_id,
                                'de_ce' => $data->de_ce,
                                'movement_type_reason_id' => $data->movement_type_reason_id,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $data->cost_center_id,

                                'item_no' => $item++,
                                'material_id' => $val['material_id'],
                                'material_description' => $material->description,
                                'quantity_entry' => $val['quantity_entry'],
                                'entry_uom_id' => $val['uom_id'],

                                // convert qty
                                'quantity' => $convert_qty,
                                'base_uom_id' => $material->uom_id,
                                'amount' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $unit_cost,
                                'currency' => 'IDR',

                                'po_number' => $data->po_number,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // update stock
                            // cek stock material in storage
                            $exist_stock = StockMain::where('material_id', $val['material_id'])
                                ->where('storage_id', $data->storage_id)
                                ->first();

                            // cek stock type
                            if ($val['stock_type'] == 1) {//insert as qty qa
                                // update if stock exist
                                if ($exist_stock) {
                                    $qty = $exist_stock->qty_qa + $convert_qty;

                                    $exist_stock->update([
                                        'qty_qa' => $qty,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $stock = $exist_stock;
                                } else {
                                    // create if stock not exist
                                    $stock = StockMain::create([
                                        'material_id' => $val['material_id'],
                                        'plant_id' => $data->plant_id,
                                        'storage_id' => $data->storage_id,
                                        'qty_unrestricted' => 0,
                                        'qty_blocked' => 0,
                                        'qty_transit' => 0,
                                        'qty_qa' => $convert_qty,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            } else {//insert as qty unrest
                                // update if stock exist
                                if ($exist_stock) {
                                    $qty = $exist_stock->qty_unrestricted + $convert_qty;

                                    $exist_stock->update([
                                        'qty_unrestricted' => $qty,
                                        'updated_by' => Auth::user()->id
                                    ]);

                                    $stock = $exist_stock;
                                } else {
                                    // create if stock not exist
                                    $stock = StockMain::create([
                                        'material_id' => $val['material_id'],
                                        'plant_id' => $data->plant_id,
                                        'storage_id' => $data->storage_id,
                                        'qty_unrestricted' => $convert_qty,
                                        'qty_blocked' => 0,
                                        'qty_transit' => 0,
                                        'qty_qa' => 0,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }

                            // insert batch
                            // generate new batch number
                            $batch_number = $this->getBatchNumber();

                            // validate material plant batch creation indicator
                            $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                                ->where('material_id', $val['material_id'])
                                ->first();

                            $cekmvt = MovementType::find($data->movement_type_id);

                            if ($material_plant->batch_ind) {
                                if ($cekmvt->batch_ind) {
                                    if ($val['stock_type'] == 1) {//insert as qty qa
                                        // create stock batch
                                        $batch = StockBatch::create([
                                            'stock_main_id' => $stock->id,
                                            'batch_number' => $batch_number,
                                            'manuf_date' => $val['batch']['manuf_date'],
                                            'gr_date' => $val['batch']['gr_date'],
                                            'sled_date' => $val['batch']['sled_date'],
                                            'vendor_id' => $val['batch']['vendor_id'],
                                            'vendor_batch' => $val['batch']['vendor_batch'],
                                            'license' => $val['batch']['license'],
                                            'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                            'qty_unrestricted' => 0,
                                            'qty_blocked' => 0,
                                            'qty_transit' => 0,
                                            'qty_qa' => $convert_qty,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                        ]);
                                    } else {//insert as qty unrest
                                        // create stock batch
                                        $batch = StockBatch::create([
                                            'stock_main_id' => $stock->id,
                                            'batch_number' => $batch_number,
                                            'manuf_date' => $val['batch']['manuf_date'],
                                            'gr_date' => $val['batch']['gr_date'],
                                            'sled_date' => $val['batch']['sled_date'],
                                            'vendor_id' => $val['batch']['vendor_id'],
                                            'vendor_batch' => $val['batch']['vendor_batch'],
                                            'license' => $val['batch']['license'],
                                            'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                            'qty_unrestricted' => $convert_qty,
                                            'qty_blocked' => 0,
                                            'qty_transit' => 0,
                                            'qty_qa' => 0,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                        ]);
                                    }

                                    // update batch number with last batch
                                    TransactionCode::where('code', 'BN')->update([
                                        'lastcode' => $batch_number
                                    ]);

                                    // update batch in movement
                                    $movement->update(['batch_no' => $batch->batch_number]);
                                }
                            }

                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach($val['serial'] as $serial){
                                    // insert into stock serial table
                                    StockSerial::create([
                                        'material_id' => $movement->material_id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        'serial' => $serial,
                                        'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                        'batch_number' => $movement->batch_no, //get batch no from movement
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        'vendor_id' => null,
                                        'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }

                            // get po item
                            $po_item = $po->item->where('material_id', $val['material_id'])->first();
                            $po_item_qty = (float)$po_item->order_qty;

                            // sum total gr po item
                            $gr = MovementHistory::where('movement_type_id', $data->movement_type_id)
                                ->where('po_number', $data->po_number)
                                ->where('material_id', $val['material_id'])
                                ->get();

                            $total_qty_gr = (float)$gr->sum('quantity');

                            // update po item complete if qty is full
                            if ($total_qty_gr >= $po_item_qty) {
                                $po_item->update([
                                    'completed' => 1,
                                    'updated_by' => Auth::user()->id
                                ]);

                                $total_po_item = $po->item->count();

                                $total_po_item_completed = $po->item->where('completed', 1)
                                    ->count();

                                // if po already gr all item update status po to closed
                                if ($total_po_item == $total_po_item_completed) {
                                    $po->update([
                                        'status' => 4
                                    ]);
                                } else {
                                    // else update to gr partial
                                    $po->update([
                                        'status' => 6
                                    ]);
                                }
                            } else {
                                // else update to gr partial
                                $po->update([
                                    'status' => 6
                                ]);
                            }

                            // store in movement history gi asset
                            $mvt_gi = MovementType::where('code', 241)->first();

                            $movement_gi = MovementHistory::create([
                                'material_doc' => $material_document_gi,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $mvt_gi->id,//get 201
                                'de_ce' => $mvt_gi->dc_ind,
                                // 'movement_type_reason_id' => $data->movement_type_reason_id,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->material_slip,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $po_item->cost_center_id,

                                'item_no' => $movement->item_no,
                                'material_id' => isset($val['material_id']) ? $val['material_id'] : null,
                                'material_description' => isset($val['material_description']) ? $val['material_description'] : $material->description,
                                'quantity_entry' => $val['quantity_entry'],
                                'entry_uom_id' => $val['uom_id'],

                                // convert qty
                                'quantity' => $val['quantity_entry'],
                                'base_uom_id' => $val['uom_id'],
                                'amount' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $unit_cost,
                                'currency' => 'IDR',

                                'po_number' => $data->po_number,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id,
                                'batch_no' => $movement->batch_no
                            ]);

                            // update stock
                            if ($val['stock_type'] == 1) { 
                                $qty_after_gi = $stock->qty_qa - $convert_qty;

                                $stock->update([
                                    'qty_qa' => $qty_after_gi,
                                    'updated_by' => Auth::user()->id
                                ]);
                            } else {
                                $qty_after_gi = $stock->qty_unrestricted - $convert_qty;

                                $stock->update([
                                    'qty_unrestricted' => $qty_after_gi,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }

                            // update batch
                            if ($material_plant->batch_ind) {
                                if ($cekmvt->batch_ind) {
                                    if ($val['stock_type'] == 1) {//insert as qty qa
                                        // update stock batch
                                        $batch_after_gi = $batch->qty_qa - $convert_qty;

                                        $batch->update([
                                            'qty_qa' => $batch_after_gi,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    } else {//insert as qty unrest
                                        // update stock batch
                                        $batch_after_gi = $batch->qty_unrestricted - $convert_qty;

                                        $batch->update([
                                            'qty_unrestricted' => $batch_after_gi,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }

                                    // update batch number with last batch
                                    TransactionCode::where('code', 'BN')->update([
                                        'lastcode' => $batch_number
                                    ]);

                                    // update batch in movement
                                    $movement->update(['batch_no' => $batch->batch_number]);
                                }
                            }

                            // update serial if serialize
                            if ($material->serial_profile) {
                                foreach($val['serial'] as $serial){
                                    // update serial
                                    StockSerial::where('serial', $serial)
                                    ->update([
                                        'serial' => $serial,
                                        'status' => 3,//1=AVLB,2=ESTO,3=EQUI
                                        'plant_id' => null,
                                        'storage_id' => null
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement_gi->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }

                            // create asset
                            $plant_code = Plant::find($data->plant_id);
                            $code_asset = $this->getAssetCode($plant_code->code);

                            // get draft status asset
                            $asset_status = AssetStatus::where('code', '001')->first();

                            if($asset_status) {
                                $asset_status_draft = AssetStatus::where('code', '001')->first();
                            } else {
                                $asset_status_draft = AssetStatus::create([
                                    'code' => '001',
                                    'name' => 'Draft',
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                    'deleted' => 0
                                ]);
                            }

                            if ($material->serial_profile) {
                                // if serialize 1 qty 1 asset
                                foreach($val['serial'] as $serial){
                                    Asset::create([
                                        'code' => $this->getAssetCode($plant_code->code),
                                        'description' => $po_item->short_text,
                                        'material_id' => $material->id,
                                        'serial_number' => $serial,
                                        'group_material_id' => $material->group_material_id,
                                        'qty_asset' => 1,
                                        'status_id' => $asset_status_draft->id,//draft
                                        'plant_id' => $data->plant_id,
                                        'location_id' => $plant_code->location_id,
                                        'supplier_id' => isset($val['batch']['vendor_id']) ? $val['batch']['vendor_id'] : null,
                                        'purchase_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $unit_cost,
                                        'purchase_date' => now(),
                                        'level_asset' => 1,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            } else {
                                // if not serialize all qty 1 asset
                                Asset::create([
                                    'code' => $code_asset,
                                    'description' => $po_item->short_text,
                                    'material_id' => $material->id,
                                    'serial_number' => null,
                                    'group_material_id' => $material->group_material_id,
                                    'qty_asset' => $convert_qty,
                                    'status_id' => $asset_status_draft->id,//draft
                                    'plant_id' => $data->plant_id,
                                    'location_id' => $plant_code->location_id,
                                    'supplier_id' => isset($val['batch']['vendor_id']) ? $val['batch']['vendor_id'] : null,
                                    'purchase_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : $unit_cost,
                                    'purchase_date' => now(),
                                    'level_asset' => 1,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                        
                        // update gi code document
                        if (isset($movement_gi)) {
                            $code_gi = TransactionCode::where('code', 'M')->first();
                            $code_gi->update(['lastcode' => $material_document_gi]);
                        }
                        
                        if (isset($val['batch']['vendor_id'])) {
                            // get info record
                            $info_record = MaterialVendor::where('material_id', $val['material_id'])
                                ->where('vendor_id', $val['batch']['vendor_id'])
                                // ->where('storage_id', $data->storage_id)
                                ->first();
                            
                            if ($info_record) {
                                if ($info_record->unit_price != $val['batch']['unit_cost']) {
                                    // create history info record
                                    MaterialVendorHistory::create([
                                        'material_vendor_id' => $info_record->id,
                                        'price' => $info_record->unit_price,
                                        'currency' => $info_record->currency,
                                        'purchase_header_id' => $info_record->purchase_header_id,
                                        'purchase_item_id' => $info_record->purchase_item_id,
                                        'created_by' => $info_record->updated_by
                                    ]);
                                }
                                // update info record if price different
                                $info_record->update([
                                    'unit_price' => $val['batch']['unit_cost'],
                                    'purchase_header_id' => $po->id,
                                    'purchase_item_id' => $po_item->id,
                                    'updated_by' => Auth::user()->id,
                                ]);
                            } else {
                                // create info record
                                if (isset($val['material_id'])) {
                                    MaterialVendor::create([
                                        'material_id' => $val['material_id'],
                                        'vendor_id' => $val['batch']['vendor_id'],
                                        // 'plant_id' => $data->plant_id,
                                        // 'storage_id' => $data->storage_id,
                                        'unit_price' => $val['batch']['unit_cost'],
                                        'purchase_header_id' => $po->id,
                                        'purchase_item_id' => $po_item->id,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }

                        // record to PurchaseDocumentHistory
                        PurchaseDocumentHistory::create([
                            'purchase_header_id' => $po->id,
                            'purchase_item_id' => $po_item->id,
                            'transaction' => 1, //GR
                            'material_year' => $movement->doc_year,
                            'material_doc' => $movement->material_doc,
                            'material_doc_item' => $movement->item_no,
                            'de_ce' => $movement->de_ce,
                            'quantity' => $movement->quantity_entry,
                            'amount_lc' => $movement->amount,
                            'quantity_order' => $po_item->order_qty,
                            'amount' => $movement->amount,
                            'reference_doc' => null,
                        ]);
                    }

                    // delete header
                    $data->delete();

                    break;
                case 131:
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        $material = Material::find($val['material_id']);

                        // get production order
                        $po = ProductionOrder::where('order_number', $data->work_order)->first();

                        $work_center = WorkCenter::find($po->work_center_id);

                        $cost_center = CostCenter::find($work_center->cost_center_id);

                        $po_detail = $po->detail;

                        // get new mat code
                        $gi_material_doc = $this->getMaterialDoc();

                        // do 201 (Good issue to wrok center) for component Bill of material
                        $gi_no = 1;
                        foreach ($po_detail as $detail) {
                            $gi_material = Material::find($detail->component_id);
                            $gi_plant = Plant::find($data->plant_id);

                            // get stock
                            $gi_stock = StockMain::where('material_id', $detail->component_id)
                                ->where('storage_id', $data->storage_id)
                                ->first();

                            // check stock not found
                            if (!$gi_stock) {
                                throw new \Exception('Stock material '.$gi_material->description.', is not sufficient');
                            }

                            // check stock not sufficient
                            if ($gi_stock->qty_unrestricted < $detail->quantity) {
                                throw new \Exception('Stock material '.$gi_material->description.', is not sufficient');
                            }

                            // find movmnt type 201
                            $gi_mvt = MovementType::where('code', '201')->first();

                            // get material plant
                            $material_plant_gi = MaterialPlant::where('plant_id', $data->plant_id)
                                ->where('material_id', $detail->component_id)
                                ->first();

                            if (!$material_plant_gi) {
                                throw new \Exception('Component '.$gi_material->description.' not have material plant '.$gi_plant->description.'');
                            }

                            if ($material_plant_gi->batch_ind) {
                                // get greates batch qty
                                $batch_gi = StockBatch::where('stock_main_id', $gi_stock->id)
                                    ->where('deleted', false)
                                    ->orderBy('qty_unrestricted', 'desc')
                                    ->first();

                                // check stock not found
                                if (!$batch_gi) {
                                    throw new \Exception('Stock batch '.$batch_gi->batch_number.' material '.$material->description.', is not sufficient');
                                }

                                // check stock not sufficient
                                if ($batch_gi->qty_unrestricted < $detail->quantity) {
                                    throw new \Exception('Stock batch '.$batch_gi->batch_number.' material '.$material->description.', is not sufficient');
                                }
                            }

                            // record to movement history
                            $gi_movement = MovementHistory::create([
                                'material_doc' => $gi_material_doc,
                                'doc_year' => $data->doc_year,
                                'movement_type_id' => $gi_mvt->id,
                                'de_ce' => $gi_mvt->dc_ind,
                                'movement_type_reason_id' => null,
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'posting_date' => now(),
                                'document_date' => $data->document_date,
                                'material_slip' => $data->work_order,
                                'doc_header_text' => $data->doc_header_text,
                                'work_center_id' => $data->work_center_id,
                                'cost_center_id' => $data->cost_center_id,

                                'item_no' => $gi_no++,
                                'material_id' => $detail->component_id,
                                'material_description' => $gi_material->description,
                                'quantity_entry' => $detail->quantity,
                                'entry_uom_id' => $detail->base_uom_id,

                                // convert qty
                                'quantity' => $detail->quantity,
                                'base_uom_id' => $detail->base_uom_id,

                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);

                            // update stock
                            $gi_stock_unres = $gi_stock->qty_unrestricted - $detail->quantity;
                            $gi_stock->update([
                                'qty_unrestricted' => $gi_stock_unres,
                                'updated_by' => Auth::user()->id
                            ]);

                            // update stock batch
                            if ($material_plant_gi->batch_ind) {
                                // get greates batch qty
                                $batch_gi = StockBatch::where('stock_main_id', $gi_stock->id)
                                    ->where('deleted', false)
                                    ->orderBy('qty_unrestricted', 'desc')
                                    ->first();

                                $gi_batch_stock_unres = $batch_gi->qty_unrestricted - $detail->quantity;
                                $batch_gi->update([
                                    'qty_unrestricted' => $gi_batch_stock_unres,
                                    'updated_by' => Auth::user()->id
                                ]);

                                $gi_movement->update([
                                    'batch_no' => $batch_gi->batch_number
                                ]);
                            }
                        }

                        // update last transaction code
                        $code_gi = TransactionCode::where('code', 'M')->first();
                        $code_gi->update(['lastcode' => $gi_material_doc]);

                        // do 131 GR production
                        $gr_movement = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => $data->de_ce,
                            'movement_type_reason_id' => null,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->work_order,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $po->work_center_id,
                            'cost_center_id' => $cost_center->id,

                            'item_no' => $item++,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $po->quantity,
                            'entry_uom_id' => $po->prod_uom_id,

                            // convert qty
                            'quantity' => $po->quantity,
                            'base_uom_id' => $po->prod_uom_id,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // update stock after GR
                        $gr_stock = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $data->storage_id)
                            ->first();

                        // create if stock not exist
                        if (!$gr_stock) {
                            $gr_stock = StockMain::create([
                                'material_id' => $val['material_id'],
                                'plant_id' => $data->plant_id,
                                'storage_id' => $data->storage_id,
                                'qty_unrestricted' => $po->quantity,
                                'qty_blocked' => 0,
                                'qty_transit' => 0,
                                'qty_qa' => 0,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);
                        } else {
                            // update if stock exist
                            $gr_stock_unres = $gr_stock->qty_unrestricted + $po->quantity;
                            $gr_stock->update([
                                'qty_unrestricted' => $gr_stock_unres,
                                'updated_by' => Auth::user()->id
                            ]);
                        }

                        // record to production order approval
                        ProductionOrderApproval::create([
                            'production_order_id' => $po->id,
                            'user_id' => Auth::user()->id,
                            'status_from' => 2, //confirmed
                            'status_to' => 3, //completed
                            'notes' => 'Goods recived'
                        ]);

                        // update Production order
                        $po->update([
                            'status' => 3,
                            'gr_quantity' => $gr_movement->quantity,
                            'gr_uom_id' => $gr_movement->base_uom_id,
                            'gr_date' => $gr_movement->posting_date,
                            'material_doc' => $gr_movement->material_doc,
                            'material_doc_year' => $gr_movement->doc_year,
                            'material_doc_item' => $gr_movement->item_no,
                            'updated_by' => Auth::user()->id
                        ]);

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $cekmvt = MovementType::find($data->movement_type_id);
                        
                        if ($material_plant->batch_ind) {
                            if ($cekmvt->batch_ind) {
                                // create stock batch
                                $batch = StockBatch::create([
                                    'stock_main_id' => $gr_stock->id,
                                    'batch_number' => $batch_number,
                                    'manuf_date' => $val['batch']['manuf_date'],
                                    'gr_date' => $val['batch']['gr_date'],
                                    'sled_date' => $val['batch']['sled_date'],
                                    'vendor_id' => $val['batch']['vendor_id'],
                                    'vendor_batch' => $val['batch']['vendor_batch'],
                                    'license' => $val['batch']['license'],
                                    'unit_cost' => isset($val['batch']['unit_cost']) ? $val['batch']['unit_cost'] : 0,
                                    'qty_unrestricted' => $po->quantity,
                                    'qty_blocked' => 0,
                                    'qty_transit' => 0,
                                    'qty_qa' => 0,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                ]);

                                // update batch number with last batch
                                TransactionCode::where('code', 'BN')->update([
                                    'lastcode' => $batch_number
                                ]);

                                // update batch in movement
                                $gr_movement->update(['batch_no' => $batch->batch_number]);
                            }
                        }

                        // validate & insert serial to stock serial, serial history
                        $material = Material::find($val['material_id']);
                        if ($material->serial_profile) {
                            foreach($val['serial'] as $serial){
                                // insert into stock serial table
                                StockSerial::create([
                                    'material_id' => $gr_movement->material_id,
                                    'plant_id' => $gr_movement->plant_id,
                                    'storage_id' => $gr_movement->storage_id,
                                    'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                    'serial' => $serial,
                                    'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                    'batch_number' => $gr_movement->batch_no, //get batch no from movement
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                    'vendor_id' => null,
                                    'customer_id' => null, // will be used in 601
                                ]);

                                // record serial history
                                SerialHistory::create([
                                    'movement_history_id' => $gr_movement->id,
                                    'plant_id' => $gr_movement->plant_id,
                                    'storage_id' => $gr_movement->storage_id,
                                    'material_id' => $gr_movement->material_id,
                                    'serial' => $serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    // delete header
                    $data->delete();

                    break;
                case 241:
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        $material = Material::find($val['material_id']);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($material->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity_entry'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity_entry']);

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$material->code.' unit conversion');
                            }
                        }

                        // get material cost from material acount
                        $material_account = MaterialAccount::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $map = $material_account ? $material_account->moving_price : 0;

                        // record to movement history table
                        $movement = MovementHistory::create([
                            'material_doc' => $data->material_doc,
                            'doc_year' => $data->doc_year,
                            'movement_type_id' => $data->movement_type_id,
                            'de_ce' => $data->de_ce,
                            'movement_type_reason_id' => $data->movement_type_reason_id,
                            'plant_id' => $data->plant_id,
                            'storage_id' => $data->storage_id,
                            'posting_date' => now(),
                            'document_date' => $data->document_date,
                            'material_slip' => $data->material_slip,
                            'doc_header_text' => $data->doc_header_text,
                            'work_center_id' => $data->work_center_id,
                            'cost_center_id' => $data->cost_center_id,

                            'item_no' => $item++,
                            'material_id' => $val['material_id'],
                            'material_description' => $material->description,
                            'quantity_entry' => $val['quantity_entry'],
                            'entry_uom_id' => $val['uom_id'],

                            // convert qty
                            'quantity' => $convert_qty,
                            'base_uom_id' => $material->uom_id,
                            'amount' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : $map,
                            'currency' => 'IDR',

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // cek stock material in storage
                        $exist_stock = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $data->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($val['material_id']);

                        // update if stock exist
                        if ($exist_stock) {
                            // throw new exception if stock not enought
                            if ($exist_stock->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock material '.$material->description.', is not sufficient');
                            }
                            $qty = $exist_stock->qty_unrestricted - $convert_qty;

                            $exist_stock->update([
                                'qty_unrestricted' => $qty,
                                'updated_by' => Auth::user()->id
                            ]);

                            $stock = $exist_stock;
                        } else {
                            // throw new exception if stock not enought
                            throw new \Exception('Stock material '.$material->description.', is not sufficient');
                        }

                        // generate new batch number
                        $batch_number = $this->getBatchNumber();

                        // validate material plant batch creation indicator
                        $material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                            ->where('material_id', $val['material_id'])
                            ->first();

                        $cekmvt = MovementType::find($data->movement_type_id);

                        $material = Material::find($val['material_id']);

                        if ($material_plant->batch_ind) {
                            $selected_batch = StockBatch::find($val['batch']['stock_batch_id']);

                            // get stock batch
                            $batch = $stock->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                            // if stock batch qty unres < qty
                            if ($batch->qty_unrestricted < $convert_qty) {
                                throw new \Exception('Stock batch '.$selected_batch->batch_number.' material '.$material->description.', is not sufficient');
                            }

                            $qty_batch = $batch->qty_unrestricted - $convert_qty;

                            // update batch qty unres - 201
                            $batch->update([
                                'qty_unrestricted' => $qty_batch,
                                'updated_by' => Auth::user()->id,
                            ]);

                            // update batch in movement
                            $movement->update(['batch_no' => $batch->batch_number]);

                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement->material_id)
                                        ->where('storage_id', $movement->storage_id)
                                        ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => null,
                                        'storage_id' => null,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 3,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        } else {
                            // validate & insert serial to stock serial, serial history
                            if ($material->serial_profile) {
                                foreach ($val['serial'] as $serial) {
                                    // update stock serial table
                                    $serials = StockSerial::where('serial', $serial)
                                        ->where('material_id', $movement->material_id)
                                        ->where('storage_id', $movement->storage_id)
                                        // ->where('batch_number', $selected_batch->batch_number)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial.', not found');
                                    }
                                    
                                    $serials->update([
                                        'plant_id' => null,
                                        'storage_id' => null,
                                        'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                        // 'serial' => $serial,
                                        'status' => 3,//1=AVLB,2=ESTO,3=EQUI
                                        // 'batch_number' => $movement->batch_no, //get batch no from movement
                                        // 'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id,
                                        // 'vendor_id' => null,
                                        // 'customer_id' => null, // will be used in 601
                                    ]);

                                    // record serial history
                                    SerialHistory::create([
                                        'movement_history_id' => $movement->id,
                                        'plant_id' => $movement->plant_id,
                                        'storage_id' => $movement->storage_id,
                                        'material_id' => $movement->material_id,
                                        'serial' => $serial,
                                        'created_by' => Auth::user()->id,
                                        'updated_by' => Auth::user()->id
                                    ]);
                                }
                            }
                        }

                        // create asset
                        $plant_code = Plant::find($data->plant_id);
                        $code_asset = $this->getAssetCode($plant_code->code);

                        // get draft status asset
                        $asset_status = AssetStatus::where('code', '001')->first();

                        if($asset_status) {
                            $asset_status_draft = AssetStatus::where('code', '001')->first();
                        } else {
                            $asset_status_draft = AssetStatus::create([
                                'code' => '001',
                                'name' => 'Draft',
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id,
                                'deleted' => 0
                            ]);
                        }

                        if ($material->serial_profile) {
                            // if serialize 1 qty 1 asset
                            foreach($val['serial'] as $serial){
                                Asset::create([
                                    'code' => $this->getAssetCode($plant_code->code),
                                    'description' => $material->short_text,
                                    'material_id' => $material->id,
                                    'serial_number' => $serial,
                                    'group_material_id' => $material->group_material_id,
                                    'qty_asset' => 1,
                                    'status_id' => $asset_status_draft->id,//draft
                                    'plant_id' => $data->plant_id,
                                    'location_id' => $plant_code->location_id,
                                    'supplier_id' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->vendor_id : null,
                                    'purchase_cost' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : 0,
                                    'purchase_date' => now(),
                                    'level_asset' => 1,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        } else {
                            // if not serialize all qty 1 asset
                            Asset::create([
                                'code' => $code_asset,
                                'description' => $material->short_text,
                                'material_id' => $material->id,
                                'serial_number' => null,
                                'group_material_id' => $material->group_material_id,
                                'qty_asset' => $convert_qty,
                                'status_id' => $asset_status_draft->id,//draft
                                'plant_id' => $data->plant_id,
                                'location_id' => $plant_code->location_id,
                                'supplier_id' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->vendor_id : null,
                                'purchase_cost' => isset($val['batch']['stock_batch_id']) ? StockBatch::find($val['batch']['stock_batch_id'])->unit_cost : 0,
                                'purchase_date' => now(),
                                'level_asset' => 1,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id
                            ]);
                        }
                    }

                    // delete header
                    $data->delete();

                    break;
                default:
                    throw new \Exception('movement type '.$movement_type.', will be handled soon');
            }

            DB::commit();

            return MovementHistory::where('material_doc', $data->material_doc)->whereNotNull('material_description')->get();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while processing Goods Movement',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function batchList()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        if (request()->has('storage_id') && request()->has('material_id')) {
            $main = StockMain::where('storage_id', request()->input('storage_id'))
                ->where('material_id', request()->input('material_id'))
                ->first();
        } else {
            $main = null;
        }

        $batch = (new StockBatch)->newQuery();

        if ($main) {
            $batch->where('stock_main_id', $main->id);
        } else {
            $batch->whereNull('stock_main_id');
        }

        $batch->where('deleted', false)->with(['vendor']);

        $batch->with(['stock_main.material', 'stock_main.plant', 'stock_main.storage']);

        // cek global setting
        if (appsetting('SORT_BATCH')) {
            $batch->orderBy('gr_date', 'asc');//FIFO (by GR)
        } else {
            $batch->orderBy('sled_date', 'asc');//FEFO (by expiry)
        }

        if (request()->has('q')) {
            $batch->where(DB::raw("LOWER(batch_number)"), 'LIKE', "%".strtolower(request()->input('q'))."%");
        }

        $batch = $batch->get()->unique('batch_number')->values();

        if (request()->has('per_page')) {
            return (new Collection($batch))->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return (new Collection($batch))->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function serialList()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        $serial = (new StockSerial)->newQuery();

        if (request()->has('material_id') && request()->input('material_id') != '') {
            $serial->where('material_id', request()->input('material_id'));
        }

        if (request()->has('storage_id') && request()->input('storage_id') != '') {
            $serial->where('storage_id', request()->input('storage_id'));
        }

        if (request()->has('batch') && request()->input('batch') != '') {
            $serial->where('batch_number', request()->input('batch'));
        }

        $serial->where('deleted', false);

        if (request()->has('q')) {
            $serial->where(DB::raw("LOWER(serial)"), 'LIKE', "%".strtolower(request()->input('q'))."%");
        }

        if (request()->has('per_page')) {
            return $serial->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $serial->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function getAssetCode($plant)
	{
        $q = date('Y').''.strtolower($plant);

        $code = Asset::where(DB::raw("LOWER(code)"), 'LIKE', $q."%")
            ->orderBy('id', 'desc')
            ->first();

        // if exist
        if ($code) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->code, 0, 4);
            $increment = substr($code->code, -10) + 1;
            $year = date('Y');
            if ($year > $yearcode) {
                $lastcode = date('Y');
                $lastcode .= $plant;
                $lastcode .= '0000000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = $yearcode;
                $lastcode .= $plant;
                $lastcode .= sprintf("%010d", $increment);
            }
        } else {//generate if not exist
            $lastcode = date('Y');
            $lastcode .= $plant;
            $lastcode .= '0000000001';
        }

        return $lastcode;
	}
}
