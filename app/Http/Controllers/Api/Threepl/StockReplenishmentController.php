<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Models\User;
use App\Models\Reservation;
use App\Models\TransactionCode;
use App\Models\Plant;
use App\Models\CostCenter;
use App\Models\Storage as MasterStorage;
use App\Models\StockDetermination;
use App\Models\GroupMaterial;
use App\Models\Material;
use App\Models\Uom;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\MaterialStorage;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\DeliveryHeader;

class StockReplenishmentController extends Controller
{
    public function PRCode()
    {
        // Auth::user()->cekRoleModules(['reservation-create']);
        
        // get reservation code 
        $code = TransactionCode::where('code', 'A')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is greater than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'A';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function replenishment(Request $request)
    {
        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:stock_mains,id'
        ]);

        // validate from 1 storage
        $stock = StockMain::whereIn('id', $request->id)->get();

        $storage_id1 = $stock[0]['storage_id'];
        $storage_id = true;

        for ($i = 0; $i < count($stock); $i++) {
            $storage = $stock[$i]['storage_id'];
            if ($storage !== $storage_id1) {
                $storage_id = false;
            }
        }

        if (!$storage_id) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'id'  => ['the selected data not in one storage']
                ]
            ], 422);
        }

        try {
            DB::beginTransaction();

            $mst_storage = MasterStorage::find($stock[0]['storage_id']);
            $mst_plant = Plant::find($mst_storage->plant_id);
            $day = appsetting('PROPOSE_DELIV_DATE') ? (int)appsetting('PROPOSE_DELIV_DATE') : 2;
            
            // create pr header
            $header = new Reservation;
            $header->reservation_code = $this->PRCode();
            $header->reservation_type = 3;
            $header->document_type = 'A';
            $header->plant_id = $mst_plant->id;
            $header->storage_id = $mst_storage->id;
            $header->requirement_date = Carbon::parse(now())->addDays($day)->format('Y-m-d H:i:s');;
            $header->note_header = '';
            $header->status = 0;
            $header->created_by = Auth::user()->id;
            $header->updated_by = Auth::user()->id;
            $header->save();

            // create pr item
            $item_no = 1;
            foreach ($stock as $item) {
                $material = Material::find($item->material_id);
                $matgroup = GroupMaterial::find($material->group_material_id);

                $mrp = MaterialPlant::where('material_id', $item->material_id)
                        ->where('plant_id', $mst_plant->id)
                        ->whereNotNull('proc_group_id')
                        ->first();

                // get material storage for max & min stock
                $material_storage = MaterialStorage::where('material_id', $item->material_id)
                        ->where('storage_id', $item->storage_id)
                        ->first();
                
                // get reservation in processed
                $on_order = Reservation::whereIn('status', [2,5])
                    ->where('document_type', 'R')
                    ->whereNull('fulfillment_status')
                    ->where('material_id', $item->material_id)
                    ->where('inout_storage_id', $item->storage_id)
                    ->get();

                if (count($on_order) > 0) {
                    $do_reservation = 0;
                    foreach ($on_order as $key => $value) {
                        $res = Reservation::find($value->id);
                        $sent_do_id = DeliveryHeader::whereIn('status', [2,3])->pluck('id');

                        $do_reservation += $res->delivery_orders
                            ->whereIn('delivery_header_id', $sent_do_id)
                            ->sum('delivery_qty');
                    }
                    $qty_on_order = (($on_order->sum('quantity_base_unit')) - $do_reservation);
                } else {
                    $qty_on_order = 0;
                }

                // hitung qty
                $material_min_stock = $material_storage ? $material_storage->min_stock : 0;

                $hitung_qty = $item->qty_unrestricted - (int)$material_min_stock - $qty_on_order;

                if ($hitung_qty < 0) {
                    $positive_qty = $hitung_qty * -1;

                    $uop = $mrp ? Uom::find($mrp->unit_of_purchase_id) : null;

                    // convert uom
                    if ($uop) {
                        if ($material->uom_id == $uop->id) {
                            $convert_qty = $positive_qty;
                        } else {
                            $convert_qty = uomconversion($material->id, $uop->id, $positive_qty);                
    
                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$uop->code.' exists on material '.$material->code.' unit conversion');
                            }
                        }
                    } else {
                        $convert_qty = $positive_qty;
                    }

                    Reservation::create([
                        'material_short_text' => $material->description,
                        'reservation_code' => $header->reservation_code,
                        'reservation_item' => $item_no++,
                        'reservation_type' => $header->reservation_type,
                        'document_type' => $header->document_type,
                        'note_header' => $header->note_header,
                        'status' => $header->status,
                        'plant_id' => $header->plant_id,
                        'storage_id' => $header->storage_id,
                        'item_type' => 0,
                        'material_id' => $material->id,
                        'note_item' => null,
                        'batch' => null,
                        'requirement_date' => $header->requirement_date,
                        'quantity' => $convert_qty,
                        'uom_id' => $uop ? $uop->id : $item->uom_id,
                        'quantity_base_unit' => $positive_qty,
                        'uom_base_id' => $material->uom_id,
                        'group_material_id' => $matgroup ? $matgroup->id : null,
                        'account_id' => $matgroup ? $matgroup->account_id : null,
                        'unit_cost' => null,
                        'inout_plant_id' => null,
                        'inout_storage_id' => null,
                        'material_plant_id' => $mrp ? $mrp->id : null,
                        'proc_type' => 1,
                        'proc_group_id' => $mrp ? $mrp->proc_group_id : null,
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id,
                    ]);
                }
            }

            // update last pr code
            $code = TransactionCode::where('code', 'A')->first();

            $code->update(['lastcode' => $header->reservation_code]);

            DB::commit();

            return response()->json([
                'message' => 'success stock replenishment',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while stock replenishment',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
