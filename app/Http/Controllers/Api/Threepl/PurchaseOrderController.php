<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\Storage as MasterStorage;
use App\Models\Reservation;
use App\Models\PurchaseApproval;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\MovementHistory;
use App\Models\MaterialStorage;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\Supplier;
use App\Models\PurchaseAttachment;
use App\Models\PurchaseCondition;

use App\Models\ReleaseObject;
use App\Models\ReleaseGroup;
use App\Models\ReleaseStrategy;
use App\Models\ReleaseCode;
use App\Models\ReleaseCodeUser;
use App\Models\CodeStrategy;
use App\Models\ReleaseStrategyParameter;
use App\Models\ReleaseStatus;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationParameter;

class PurchaseOrderController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['purchase-orders-view']);

        $po = (new PurchaseHeader)->newQuery();

        $po->with([
            'term_payment', 'purc_group', 'incoterm', 'vendor', 'createdBy', 'updatedBy'
        ]);

        // for PO
        $po->where('po_doc_category', 'B');

        // filter
        if (request()->has('po_doc_no')) {
            $po->where(DB::raw("LOWER(po_doc_no)"), 'LIKE', "%".strtolower(request()->input('po_doc_no'))."%");
        }

        if (request()->has('vendor')) {
            $po->whereIn('vendor_id', request()->input('vendor'));
        }

        if (request()->has('status')) {
            $po->whereIn('status', request()->input('status'));
        } else {
            $po->where('status', '!=', 9);
        }

        if (request()->has('purc_group')) {
            $po->whereIn('purc_group_id', request()->input('purc_group'));
        }

        if (request()->has('created_by')) {
            $po->whereIn('created_by', request()->input('created_by'));
        }

        if (request()->has('created_at')) {
            $po->whereBetween('created_at', 
                [request()->created_at[0], request()->created_at[1]]
            );
        }

        if (request()->has('plant')) {
            $plant = request()->input('plant');
            $po->whereHas('item', function ($data) use ($plant){
                $data->whereIn('plant_id', $plant);
                return $data;
            });
        }

        if (request()->has('storage')) {
            $storage = request()->input('storage');
            $po->whereHas('item', function ($data) use ($storage){
                $data->whereIn('storage_id', $storage);
                return $data;
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'plant':
                    $po->join('purchase_items','purchase_items.purchase_header_id','=','purchase_headers.id');
                    $po->join('plants','purchase_items.plant_id','=','plants.id');
                    $po->select('purchase_headers.*');
                    $po->orderBy('plants.code',$sort_order);
                break;

                case 'storage':
                    $po->join('purchase_items','purchase_items.purchase_header_id','=','purchase_headers.id');
                    $po->join('storages','purchase_items.storage_id','=','storages.id');
                    $po->select('purchase_headers.*');
                    $po->orderBy('storages.code',$sort_order);
                break;

                case 'delivery_date':
                    $po->join('purchase_items','purchase_items.purchase_header_id','=','purchase_headers.id');
                    $po->select('purchase_headers.*');
                    $po->orderBy('purchase_items.delivery_date',$sort_order);
                break;

                case 'vendor':
                    $po->leftJoin('suppliers','purchase_headers.vendor_id','=','suppliers.id');
                    $po->select('purchase_headers.*');
                    $po->orderBy('suppliers.code',$sort_order);
                break;

                case 'term_payment':
                    $po->leftJoin('term_payments','term_payments.id','=','purchase_headers.term_payment_id');
                    $po->select('purchase_headers.*');
                    $po->orderBy('term_payments.code',$sort_order);
                break;

                case 'ammount':
                    $po->leftJoin('purchase_items','purchase_items.purchase_header_id','=','purchase_headers.id');
                    $po->select('purchase_headers.*',
                        DB::raw('sum(CAST(purchase_items.price_unit AS double precision) * purchase_items.order_qty) as tam')
                    );
                    $po->groupBy('purchase_headers.id');
                    $po->orderBy('tam',$sort_order);
                break;
                
                default:
                    $po->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $po->orderBy('id', 'desc');
        }

        $po = $po->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $po->transform(function($data){
            // get delivery date, plant, storage from 1st item
            $data->delivery_date = $data->item->first()->delivery_date;
            $data->plant = $data->item->first()->plant;
            $data->storage = $data->item->first()->storage;

            // get last po approval
            $last_approval = $data->approval->last();
            $data->approved_by = $last_approval ? $last_approval->user : null;
            $data->approved_time = $last_approval ? Carbon::parse($last_approval->created_at)->format('Y-m-d H:i:s') : null;

            // get po ammount from po item
            $subtotal = 0;
            $tax = 0;
            foreach ($data->item as $value) {
                $getpo = PurchaseItem::find($value->id);
                $subtotal += $getpo->price_unit * $getpo->order_qty;
                $tax += $getpo->tax_item * $getpo->order_qty;
            }

            $ongkir = 0;
            $diskon = 0;
            foreach ($data->condition as $c) {
                if ($c->type == 0) {
                    // 0 = discount, credit
                    $diskon += $c->amount;
                } elseif  ($c->type == 1) {
                    // 1 = ongkir, debit
                    $ongkir += $c->amount;
                }
            }

            $data->ammount = ($subtotal + $tax + $ongkir) - $diskon;
            $data->subtotal = $subtotal;
            $data->tax = $tax;
            $data->diskon = $diskon;
            $data->ongkir = $ongkir;

            return $data;
        });

        return $po;
    }

    public function updateHeader($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

     	$this->validate(request(), [
     		// validate header
            'doc_currency' => 'required',
            // 'ammount' => 'required',
            // 'status' => 'required',
            'vendor_id' => 'required|exists:suppliers,id',
            'term_payment_id' => 'required|exists:term_payments,id',
            'purc_group_id' => 'nullable|exists:procurement_groups,id',
            'incoterm_id' => 'required|exists:incoterms,id',
            'incoterm2' => 'required',
            'notes' => 'nullable',
            'your_reference' => 'nullable',
        ]);

        $po = PurchaseHeader::find($id);

        if (!$po) {
            return response()->json([
                'message' => 'Purchase Orders Not Found'
            ], 404);
        }

        if ($po->status != 0) {
            return response()->json([
                'message' => 'Purchase Orders status not Open, Cant Edit again'
            ], 422);
        }

        $vendor = Supplier::find($request->vendor_id);

        if ($vendor->type === 4) {
            $this->validate(request(), [
               'vendor_description' => 'required|string|max:191',
               'vendor_address' => 'required|string|max:191'
           ]);
        }

        try {
            DB::beginTransaction();

            // update po header
            $po->update([
            	'purc_group_id' => $request->purc_group_id,
                'vendor_id' => $request->vendor_id,
            	'term_payment_id' => $request->term_payment_id,
			    'doc_currency' => $request->doc_currency,
			    'incoterm_id' => $request->incoterm_id,
			    'incoterm2'  => $request->incoterm2,
                'notes' => $request->notes,
                'your_reference' => $request->your_reference,
                'po_doc_category' => 'B',
                'updated_by' => Auth::user()->id,
                'vendor_description' => $request->vendor_description ? $request->vendor_description : $vendor->description,
                'vendor_address' => $request->vendor_address ? $request->vendor_address : $vendor->location->address
            ]);

            $tax = $vendor->tax_indicator;

            foreach ($po->item as $item) {
                $po_item = PurchaseItem::find($item->id);

                $tax_item = ($tax * $po_item->price_unit) / 100;

                $po_item->update([
                    'tax' => $tax,
                    'tax_item' => $tax_item,
                    'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            $response = PurchaseHeader::with('item')->find($id);

            // get po ammount from po item
            $subtotal = 0;
            $tax = 0;
            foreach ($response->item as $value) {
                $getpo = PurchaseItem::find($value->id);
                $subtotal += $getpo->price_unit * $getpo->order_qty;
                $tax += $getpo->tax_item * $getpo->order_qty;
            }

            $ongkir = 0;
            $diskon = 0;
            foreach ($response->condition as $c) {
                if ($c->type == 0) {
                    // 0 = discount, credit
                    $diskon += $c->amount;
                } elseif  ($c->type == 1) {
                    // 1 = ongkir, debit
                    $ongkir += $c->amount;
                }
            }

            $response->ammount = ($subtotal + $tax + $ongkir) - $diskon;
            $response->subtotal = $subtotal;
            $response->tax = $tax;
            $response->diskon = $diskon;
            $response->ongkir = $ongkir;

            return $response;
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while converting PR as PO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-view']);

        $po = PurchaseHeader::with([
                'item', 'term_payment', 'purc_group', 'incoterm', 'vendor', 'attachment', 'condition', 'release_strategy'
            ])
            ->with('vendor.location')
            ->with('item.material')
            ->with('item.material.images')
            ->with('item.group_material')
            ->with('item.gl_account')
            ->with('item.plant')
            ->with('item.storage')
            ->with('item.project')
            ->with('item.order_unit')
            ->with('item.cost_center')
            ->find($id);
            
        $subtotal = 0;
        $tax = 0;
        foreach ($po->item as $value) {
            $getpo = PurchaseItem::find($value->id);
            $subtotal += $getpo->price_unit * $getpo->order_qty;
            $tax += $getpo->tax_item * $getpo->order_qty;
        }

        $ongkir = 0;
        $diskon = 0;
        foreach ($po->condition as $c) {
            if ($c->type == 0) {
                // 0 = discount, credit
                $diskon += $c->amount;
            } elseif  ($c->type == 1) {
                // 1 = ongkir, debit
                $ongkir += $c->amount;
            }
        }

        $po->ammount = ($subtotal + $tax + $ongkir) - $diskon;
        $po->subtotal = $subtotal;
        $po->tax = $tax;
        $po->diskon = $diskon;
        $po->ongkir = $ongkir;

        if (!$po->vendor_id) {
            $first_reservation = Reservation::find($po->item[0]->reservation_id);
            $po->vendor_reservation = Supplier::find($first_reservation->prefered_vendor_id);
        }

        // get po release strategy
        if ($po->release_strategy) {
            $strategy = ReleaseStrategy::find($po->release_strategy_id);
            // get release code & user approver from release strategy po
            $workflow = CodeStrategy::where('release_strategy_id', $strategy->id)
                ->orderBy('created_at', 'asc')
                ->get()
                ->transform( function ($data) use ($strategy, $id) {
                    // get release code approver
                    $release_code_user = ReleaseCodeUser::where('release_code_id', $data->release_code_id)
                        ->where('release_group_id', $strategy->release_group_id)
                        ->where('deleted', false)
                        ->first();
                    
                    $user = $release_code_user ? User::find($release_code_user->user_id) : null;
                    // get release code
                    $release_code = ReleaseCode::find($data->release_code_id);

                    // if login user = approver
                    if (Auth::user()->id == $release_code_user->user_id) {
                        $can_approve = true;
                    } else {
                        $can_approve = false;
                    }

                    // check alredy approved or not
                    $approval = PurchaseApproval::where('purchase_header_id', $id)
                        ->where('user_id',  $release_code_user->user_id)
                        ->where('status_from', 1)
                        ->first();
                    
                    if ($approval) {
                        $approved = true;
                    } else {
                        $approved = false;
                    }                    

                    $new = [
                        'user' => $user,
                        'release_code' => $release_code,
                        'can_approve' => $can_approve,
                        'approved' => $approved,
                        'approval' => $approval
                    ];

                    return $new;
                });
            
            /* btn approve show if
            * prev_approved = true
            * approved = false
            * can_approve = true 
            */
            $new_workflow = [];
            foreach ($workflow as $key => $ar) {
                // get prev approved or not
                if ($key != 0) {
                    $prev_approved = $workflow[$key - 1]['approved'];
                } else {
                    $prev_approved = true;
                }

                if ($prev_approved == true && $ar['approved'] == false && $ar['can_approve'] == true ) {
                    $btn_approve = true;
                } else {
                    $btn_approve = false;
                }

                $new_workflow[] = [
                    'btn_approve' => $btn_approve,
                    'prev_approved' => $prev_approved,
                    'approved' => $ar['approved'],
                    'approval' => $ar['approval'],
                    'can_approve' => $ar['can_approve'],
                    'release_code' => $ar['release_code'],
                    'user' => $ar['user']
                ];
            }
            $po->workflow = $new_workflow;
        }

        return $po;
    }

    public function showItem($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-view']);

        $item = PurchaseItem::with([
            'material', 'group_material', 'gl_account', 'plant', 'storage', 'project', 'order_unit', 'cost_center', 'reservation'
        ])
        ->with('material.images')
        ->find($id);

        return $item;
    }

    public function updateItem($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

    	$this->validate(request(), [
            // validate item
            'item' => 'array',
            'item.*.id' => 'required|exists:purchase_items,id',
            'item.*.long_text' => 'nullable',
            'item.*.order_qty' => 'required|numeric|min:1',
            'item.*.price_unit' => 'required|numeric|min:1',
        ]);

        $po = PurchaseHeader::find($id);

        // cek po status
        if ($po->status != 0) {
            return response()->json([
                'message' => 'Purchase Orders status not Open, Cant Edit again'
            ], 422);
        }

        // cek qty cant greater than pr qty
        foreach ($request->item as $key => $val) {
            $po_item = PurchaseItem::find($val['id']);
            $res_item = Reservation::find($po_item->reservation_id);

            if ($val['order_qty'] > $res_item->quantity_base_unit) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'item.'.$key.'.order_qty'  => ['Qty Cant greather than '.$res_item->quantity_base_unit.'']
                    ]
                ], 422);
            }
        }

        try {
            DB::beginTransaction();
            // vendor 
            $vendor = Supplier::find($po->vendor_id);

            $tax = $vendor ? $vendor->tax_indicator : 0;

            // update po item
            $item = 1;
            foreach ($request->item as $key => $val) {
                $po_item = PurchaseItem::find($val['id']);
                
                $tax_item = ($tax * $val['price_unit']) / 100;

                $po_item->update([
                    'purchasing_item_no' => $item++,
                    'long_text' => $val['long_text'],
                    'order_qty' => $val['order_qty'],
                    'price_unit' => $val['price_unit'],
                    'tax' => $tax,
                    'tax_item' => $tax_item,
                    'updated_by' => Auth::user()->id,
                ]);
            }

            DB::commit();

            $response = PurchaseHeader::with(['item', 'term_payment', 'purc_group', 'incoterm', 'vendor'])
                ->with('vendor.location')
                ->with('item.material')
                ->with('item.material.images')
                ->with('item.group_material')
                ->with('item.gl_account')
                ->with('item.plant')
                ->with('item.storage')
                ->with('item.project')
                ->with('item.order_unit')
                ->with('item.cost_center')
                ->find($id);

            // get po ammount from po item
            $subtotal = 0;
            $tax = 0;
            foreach ($response->item as $value) {
                $getpo = PurchaseItem::find($value->id);
                $subtotal += $getpo->price_unit * $getpo->order_qty;
                $tax += $getpo->tax_item * $getpo->order_qty;
            }

            $ongkir = 0;
            $diskon = 0;
            foreach ($response->condition as $c) {
                if ($c->type == 0) {
                    // 0 = discount, credit
                    $diskon += $c->amount;
                } elseif  ($c->type == 1) {
                    // 1 = ongkir, debit
                    $ongkir += $c->amount;
                }
            }

            $response->ammount = ($subtotal + $tax + $ongkir) - $diskon;
            $response->subtotal = $subtotal;
            $response->tax = $tax;
            $response->diskon = $diskon;
            $response->ongkir = $ongkir;

            return $response;
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while edit PO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function updateAccountAssignment($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        // validation
        $this->validate(request(), [
            'cost_center_id' => 'nullable|exists:cost_centers,id',
            'project_id' => 'nullable|exists:projects,id',
            'commitment_item' => 'nullable',
            'gl_account_id' => 'nullable|exists:accounts,id',
            'fund_center' => 'nullable',
        ]);

        $po_item = PurchaseItem::find($id);
        $po_item->cost_center_id = $request->cost_center_id;
        $po_item->project_id = $request->project_id;
        $po_item->commitment_item = $request->commitment_item;
        $po_item->gl_account_id = $request->gl_account_id;
        $po_item->fund_center = $request->fund_center;
        $po_item->updated_by = Auth::user()->id;
        $save = $po_item->save();

        if ($save) {
            return $po_item;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteHeader($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        $po = PurchaseHeader::find($id);

        if (!$po) {
            return response()->json([
                'message' => 'Purchase Orders Not Found'
            ], 404);
        }

        if ($po->status != 0) {
            return response()->json([
                'message' => 'Purchase Orders status not Open, Cant delete PO'
            ], 422);
        }

        PurchaseApproval::create([
            'purchase_header_id' => $id,
            'user_id' => Auth::user()->id,
            'status_from' => $po->status, // submit
            'status_to' => 9,
            'notes' => ''
        ]);

        $po->timestamps = false;
        $po->status = 9;
        $po->save();

        return response()->json([
            'message' => 'Purchase Orders deleted'
        ], 200);
    }

    /**
     * Get New Code For Purchase Order.
     *
     * @return new PO Code
     */
    public function getPoCode()
    {
        $code = TransactionCode::where('code', 'B')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'B';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    /**
     * Show the Purchase Header & Detail
     *
     * @param  $po_code
     * @return \App\Models\PurchaseHeader
     */
    public function showByCode($po_code)
    {
        Auth::user()->cekRoleModules(['purchase-orders-view']);

        $po = PurchaseHeader::with([
            'term_payment', 'purc_group', 'incoterm'
        ])
        ->where('po_doc_no', $po_code)
        ->first();

        if (!$po) {
            return response()->json([
                'message' => 'Purchase Orders not Found'
            ], 404);
        }

        if (!in_array($po->status , [2,5,6])) {
            return response()->json([
                'message' => 'Purchase Orders must Approved first'
            ], 422);
        }

        $mvt_gr_po = MovementType::where('code', 101)->first();

        // po item map qty po - gr qty 101
        $items = PurchaseItem::where('purchase_header_id', $po->id)
            ->where('completed', false)
            ->with('material')
            ->get()
            ->map(function ($data) use ($po_code, $mvt_gr_po, $po) {
                $gr_po_item = MovementHistory::where('po_number', $po_code)
                    ->where('material_id', $data->material_id)
                    ->where('movement_type_id', $mvt_gr_po->id)
                    ->get();

                    if ($gr_po_item) {
                        $gr_item = $gr_po_item->sum('quantity_entry');
                    } else {
                        $gr_item = 0;
                    }

                    $data->order_qty = $data->order_qty - $gr_item;
                    $data->vendor = Supplier::find($po->vendor_id);
                    if ($data->material) {
                        $data->material->material_plant = MaterialPlant::where('plant_id', $data->plant_id)
                                                ->where('material_id', $data->material_id)
                                                ->first();
                    }                    

                    // if app setting UNIT_COST_WITH_TAX true unit cost + tax item
                    if (appsetting('UNIT_COST_WITH_TAX')) {
                        $unit_cost = $data->price_unit + $data->tax_item;
                    } else {
                        $unit_cost = $data->price_unit;
                    }

                    $data->price_unit = $unit_cost;

                    return $data;
            })->reject(function ($data) {
                return $data->order_qty < 1;
            });

        $po->items = $items;

        if (count($po->items) == 0) {
            return response()->json([
                'message' => 'Purchase Orders '.$po_code.' already goods recived'
            ], 422);
        }

        $po->plant = $items->first()->plant;
        $po->storage = $items->first()->storage;

        return $po;
    }

    public function submit(Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        /** PO status
        * 0. Open
        * 1. Submitted
        * 2. Approved
        * 9. Deleted
        */

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:purchase_headers,id',
            'approval_note' => 'required',
        ]);

        foreach ($request->id as $id) {
            $po = PurchaseHeader::find($id);

            // validate po
            if (!$po) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' not found',
                ], 404);
            }

            // validate po status
            if ($po->status == 2) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' already approved',
                ], 422);
            }

            if ($po->status == 1) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' already submitted',
                ], 422);
            }

            // validate Vendor not null
            if (!$po->vendor_id) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' vendor cant empty',
                ], 422);
            }

            $subtotal = 0;
            $tax = 0;
            foreach ($po->item as $value) {
                $getpo = PurchaseItem::find($value->id);
                $subtotal += $getpo->price_unit * $getpo->order_qty;
                $tax += $getpo->tax_item * $getpo->order_qty;
            }

            $ongkir = 0;
            $diskon = 0;
            foreach ($po->condition as $c) {
                if ($c->type == 0) {
                    // 0 = discount, credit
                    $diskon += $c->amount;
                } elseif  ($c->type == 1) {
                    // 1 = ongkir, debit
                    $ongkir += $c->amount;
                }
            }

            $po->ammount = ($subtotal + $tax + $ongkir) - $diskon;
            $po->subtotal = $subtotal;
            $po->tax = $tax;
            $po->diskon = $diskon;
            $po->ongkir = $ongkir;

            if (is_null($po->item[0]['plant']['code'])) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' plant is empty'
                ], 422);
            }

            if (is_null($po->item[0]['group_material']['material_group'])) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' material group is empty'
                ], 422);
            }

            if (is_null($po->item[0]['cost_center']['code'])) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' cost center is empty'
                ], 422);
            }

            $strategy = $this->getReleaseStrategy($po->ammount, $po->item[0]['plant']['code'], $po->item[0]['group_material']['material_group'], $po->item[0]['cost_center']['code']);

            $match_strategy = collect($strategy)->whereIn('match', true)->values();

            if (count($match_strategy) == 0) {
                $not_match_strategy_message = collect($strategy)->whereIn('match', false)->pluck('message');
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' '.$not_match_strategy_message,
                    'detail' => [
                        'ammount' => $po->ammount,
                        'plant' => $po->item[0]['plant']['code'],
                        'material_group' => $po->item[0]['group_material']['material_group'],
                        'cost_center' => $po->item[0]['cost_center']['code']
                    ]
                ], 422);
            }

            // check qty & price cant empty
            foreach ($po->item as $value) {
                if (!$value->order_qty | $value->order_qty == 0) {
                    return response()->json([
                        'message'   => 'PO '.$po->po_doc_no.' item Qty cant empty',
                    ], 422);
                }

                if (!$value->price_unit | $value->price_unit <= 0) {
                    return response()->json([
                        'message'   => 'PO '.$po->po_doc_no.' item Unit Price cant be zero or less',
                    ], 422);
                }
            }
        }

        try {
            DB::beginTransaction();
            
            // update po & record status po
            foreach ($request->id as $id) {
                $po = PurchaseHeader::find($id);

                PurchaseApproval::create([
                    'purchase_header_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $po->status, // submit
                    'status_to' => 1,
                    'notes' => $request->approval_note
                ]);

                $po->timestamps = false;
                $po->status = 1;
                $po->release_strategy_id = $match_strategy[0]['id'];
                $po->save();

                if ($po->release_strategy) {
                    $strategy = ReleaseStrategy::find($po->release_strategy_id);
                    // get release code & user approver from release strategy po
                    $workflow = CodeStrategy::where('release_strategy_id', $strategy->id)
                        ->orderBy('created_at', 'asc')
                        ->get()
                        ->transform( function ($data) use ($strategy, $id) {
                            // get release code approver
                            $release_code_user = ReleaseCodeUser::where('release_code_id', $data->release_code_id)
                                ->where('release_group_id', $strategy->release_group_id)
                                ->where('deleted', false)
                                ->first();
                            
                            $user = $release_code_user ? User::find($release_code_user->user_id) : null;
                            // get release code
                            $release_code = ReleaseCode::find($data->release_code_id);

                            // get release group
                            $release_group = ReleaseGroup::find($strategy->release_group_id);

                            if (!$release_code_user) {
                                throw new \Exception('please input release code user, with release code '.$release_code->code.' & release group '.$release_group->code.'');
                            }

                            $release_code_user_user_id = $release_code_user ? $release_code_user->id : null;

                            // if login user = approver
                            if (Auth::user()->id == $release_code_user_user_id) {
                                $can_approve = true;
                            } else {
                                $can_approve = false;
                            }

                            // check alredy approved or not
                            $approval = PurchaseApproval::where('purchase_header_id', $id)
                                ->where('user_id',  $release_code_user_user_id)
                                ->where('status_from', 1)
                                ->first();
                            
                            if ($approval) {
                                $approved = true;
                            } else {
                                $approved = false;
                            }                    

                            $new = [
                                'user' => $user,
                                'release_code' => $release_code,
                                'can_approve' => $can_approve,
                                'approved' => $approved
                            ];

                            return $new;
                        });

                    if (count($workflow) == 0) {
                        throw new \Exception('Release strategy '.$strategy->code.' missing release code');
                    } else {
                        // send mail to approver pertama
                        if ($workflow[0]['user']) {
                            \Mail::send(new \App\Mail\ApprovalPurchaseOrder($workflow[0]['user'], $po));
                        }
                    }
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Success submit PO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed submit PO, ' .$e->getMessage(),
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function approve(Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-approval']);

        /** PO status
        * 0. Open
        * 1. Submitted
        * 2. Approved
        * 9. Deleted
        */

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:purchase_headers,id',
            'approval_note' => 'required',
        ]);

        foreach ($request->id as $id) {
            $po = PurchaseHeader::find($id);

            // validate po
            if (!$po) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' not found',
                ], 404);
            }

            // validate po status
            if ($po->status == 2) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' already approved',
                ], 422);
            }

            if ($po->status != 1) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' must Submitted first, before approval',
                ], 422);
            }

            // check qty & price cant empty
            foreach ($po->item as $value) {
                if (!$value->order_qty | $value->order_qty == 0) {
                    return response()->json([
                        'message'   => 'PO '.$po->po_doc_no.' item Qty cant empty',
                    ], 422);
                }

                if (!$value->price_unit | $value->price_unit <= 0) {
                    return response()->json([
                        'message'   => 'PO '.$po->po_doc_no.' item Unit Price cant be zero or less',
                    ], 422);
                }
            }
        }

        try {
            DB::beginTransaction();
            
            // update po & record status po
            foreach ($request->id as $id) {
                $po = PurchaseHeader::find($id);

                // get po release strategy
                if ($po->release_strategy) {
                    $strategy = ReleaseStrategy::find($po->release_strategy_id);
                    // get release code & user approver from release strategy po
                    $workflow = CodeStrategy::where('release_strategy_id', $strategy->id)
                        ->orderBy('created_at', 'asc')
                        ->get()
                        ->transform( function ($data) use ($strategy, $id) {
                            // get release code approver
                            $release_code_user = ReleaseCodeUser::where('release_code_id', $data->release_code_id)
                                ->where('release_group_id', $strategy->release_group_id)
                                ->first();
                            
                            $user = $release_code_user ? User::find($release_code_user->user_id) : null;
                            // get release code
                            $release_code = ReleaseCode::find($data->release_code_id);

                            // if login user = approver
                            if (Auth::user()->id == $release_code_user->user_id) {
                                $can_approve = true;
                            } else {
                                $can_approve = false;
                            }

                            // check alredy approved or not
                            $approval = PurchaseApproval::where('purchase_header_id', $id)
                                ->where('user_id',  $release_code_user->user_id)
                                ->where('status_from', 1)
                                ->first();
                            
                            if ($approval) {
                                $approved = true;
                            } else {
                                $approved = false;
                            }                    

                            $new = [
                                'user' => $user,
                                'release_code' => $release_code,
                                'can_approve' => $can_approve,
                                'approved' => $approved
                            ];

                            return $new;
                        });
                    
                    // get total po - 1
                    $total_release_code = count($workflow) - 1;
                    
                    // check if this is last release code
                    if ($workflow[$total_release_code]['can_approve'] == true) {
                        $status = 2;
                    }

                    foreach ($workflow as $k => $w) {
                        // get match approver with loggedin user
                        if ($workflow[$k]['user']['id'] == Auth::user()->id) {
                            // get next array
                            $next = $k + 1;
                            // if next approver exist
                            if ($next <= $total_release_code) {
                                // send mail to approver selanjutnya
                                if ($workflow[$next]['user']) {
                                    \Mail::send(new \App\Mail\ApprovalPurchaseOrder($workflow[$next]['user'], $po));
                                }
                            }                            
                        }
                    }

                    PurchaseApproval::create([
                        'purchase_header_id' => $id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $po->status, // submit
                        'status_to' => isset($status) ? $status : 1,
                        'notes' => $request->approval_note
                    ]);
    
                    $po->timestamps = false;
                    $po->status = isset($status) ? $status : 1;
                    $po->save();

                    if ($po->status == 2) {
                        $vendor = Supplier::find($po->vendor_id);

                        if ($vendor->email) {
                            // send mail to vendor
                            \Mail::send(new \App\Mail\ApprovalPONotification($vendor, $po));
                        }
                    }
                } else {
                    PurchaseApproval::create([
                        'purchase_header_id' => $id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $po->status, // submit
                        'status_to' => 2,
                        'notes' => $request->approval_note
                    ]);
    
                    $po->timestamps = false;
                    $po->status = 2;
                    $po->save();
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Success submit approval PO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed submit approval PO',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function reject(Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-approval']);

        /** PO status
        * 0. Open
        * 1. Submitted
        * 2. Approved
        * 9. Deleted
        */

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:purchase_headers,id',
            'approval_note' => 'required',
        ]);

        foreach ($request->id as $id) {
            $po = PurchaseHeader::find($id);

            // validate po
            if (!$po) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' not found',
                ], 404);
            }

            // validate po status
            if ($po->status == 3) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' already reject',
                ], 422);
            }

            if ($po->status != 1) {
                return response()->json([
                    'message'   => 'PO '.$po->po_doc_no.' must Submitted first, before approval',
                ], 422);
            }
        }

        try {
            DB::beginTransaction();
            
            // update po & record status po
            foreach ($request->id as $id) {
                $po = PurchaseHeader::find($id);

                PurchaseApproval::create([
                    'purchase_header_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $po->status, // submit
                    'status_to' => 3,
                    'notes' => $request->approval_note
                ]);

                $po->timestamps = false;
                $po->status = 3;
                $po->save();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success submit approval PO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed submit approval PO',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    /**
     * PO Pdf
     *
     * @param  int  $id
     * @return \PDF
     */
    public function pdf($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-pdf']);

        $po = PurchaseHeader::with([
            'item',
            'item.material'
        ])->find($id);

        // validate po
        if (!$po) {
            return response()->json([
                'message' => 'Purchase Orders not Found'
            ], 404);
        }

        // validate po status must approved or printed
        if (!in_array($po->status , [2,5,6])) {
            return response()->json([
                'message' => 'Purchase Orders must Approved'
            ], 422);
        }

        $mvt_gr_po = MovementType::where('code', 101)->first();

        // get po code
        $po_code = $po->po_doc_no;

        // validate po item qty already fully gr / no
        $items = PurchaseItem::where('purchase_header_id', $po->id)
            ->where('completed', false)
            ->with('material')
            ->get()
            ->map(function ($data) use ($po_code, $mvt_gr_po) {
                $gr_po_item = MovementHistory::where('po_number', $po_code)
                    ->where('movement_type_id', $mvt_gr_po->id)
                    ->where('material_id', $data->material_id)
                    ->get();

                if ($gr_po_item) {
                    $gr_item = $gr_po_item->sum('quantity_entry');
                } else {
                    $gr_item = 0;
                }

                $data->order_qty = $data->order_qty - $gr_item;

                return $data;
            })->reject(function ($data) {
                return $data->order_qty < 1;
            });

        if (count($items) == 0) {
            return response()->json([
                'message' => 'Purchase Orders already goods received'
            ], 422);
        }

        $po->update([
            'status' => 5 //5. Printed GR
        ]);

        $subtotal = 0;
        $tax = 0;
        foreach ($po->item as $value) {
            $getpo = PurchaseItem::find($value->id);
            $subtotal += $getpo->price_unit * $getpo->order_qty;
            $tax += $getpo->tax_item * $getpo->order_qty;
        }

        $ongkir = 0;
        $diskon = 0;
        foreach ($po->condition as $c) {
            if ($c->type == 0) {
                // 0 = discount, credit
                $diskon += $c->amount;
            } elseif  ($c->type == 1) {
                // 1 = ongkir, debit
                $ongkir += $c->amount;
            }
        }

        $po->ammount = ($subtotal + $tax + $ongkir) - $diskon;
        $po->subtotal = $subtotal;
        $po->tax = $tax;
        $po->diskon = $diskon;
        $po->ongkir = $ongkir;

        // generate pdf po
        $pdf = PDF::loadview('pdf.po', compact('po'))->setPaper('a4', 'potrait');
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 8, array(0, 0, 0));
        
        return $pdf->download('Purchase_Orders.pdf');
    }

    public function completeItem($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:purchase_items,id',
        ]);

        $po = PurchaseHeader::find($id);

        // validate po
        if (!$po) {
            return response()->json([
                'message'   => 'PO '.$po->po_doc_no.' not found',
            ], 404);
        }

        try {
            DB::beginTransaction();
            
            // update po & record status po
            foreach ($request->id as $id_item) {
                $po_item = PurchaseItem::find($id_item);

                // validate completed
                if ($po_item->completed) {
                    throw new \Exception('PO item id '.$id_item.' already completed');
                }

                $po_item->update([
                    'completed' => 1,
                    'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success completed PO item'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed completed PO item',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function addAttachment($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        // validation
        $this->validate(request(), [
            'file' => 'required|file|max:5192',
        ]);

        // get file
        $file = $request->file('file');

        // get file extension for validation
        $ext_file = $file->getClientOriginalExtension();

        $ext = ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'zip', 'rar'];

        if (!in_array($ext_file, $ext)) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type support to upload is png, jpg, jpeg, pdf, doc, docx, ppt, pptx, xls, xlsx, zip, rar']
                ]
            ],422);
        }

        try {
            DB::beginTransaction();

            $po = PurchaseHeader::find($id);

            if (!$po) {
                throw new \Exception('PO not found');
            }

            // upload file
            $file_data = request()->file('file');
            $file_ext  = request()->file('file')->getClientOriginalExtension();
            $file_old  = request()->file('file')->getClientOriginalName();
            $file_name = $po->po_doc_no . md5(time().$file_old). "." .$file_ext;
            $file_path = 'file/po/'.$po->po_doc_no;

            $uploaded = Storage::disk('public')->putFileAs($file_path, $file_data, $file_name);

            // insert data attachment
            PurchaseAttachment::create([
                'purchase_header_id' => $id,
                'name' => $uploaded,
                'created_by' => Auth::user()->id,
                'original_name' => $file_old
            ]);

            DB::commit();

            return PurchaseAttachment::where('purchase_header_id', $id)->get();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error save attachment',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function deleteAttachment($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:purchase_attachments,id',
        ]);

        try {
            DB::beginTransaction();

            $po = PurchaseHeader::find($id);

            if (!$po) {
                throw new \Exception('PO not found');
            }

            foreach (request()->id as $ids) {
                $file = PurchaseAttachment::find($ids);

                // delete file
                if (Storage::disk('public')->exists($file->name)) {
                    Storage::delete($file->name);
                }

                // delete data
                $delete = $file->delete();
            }

            DB::commit();

            return PurchaseAttachment::where('purchase_header_id', $id)->get();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete attachment',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function addCondition($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        // validation
        $this->validate(request(), [
            'type' => 'required|numeric|min:0|max:1',
            'currency' => 'nullable',
            'amount' => 'required'
        ]);

        $po = PurchaseHeader::find($id);

        if (!$po) {
            return response()->json([
                'message' => 'PO not found'
            ], 404);
        }

        $total_condition = PurchaseCondition::where('purchase_header_id', $id)->count();

        if ($total_condition == 2 ) {
            return response()->json([
                'message' => 'PO condition exceed max condition'
            ], 422);
        }

        $cek_condition_type = PurchaseCondition::where('purchase_header_id', $id)
            ->where('type', $request->type)
            ->first();

        if ($cek_condition_type) {
            return response()->json([
                'message' => 'Can\'t Input condition type more than once'
            ], 422);
        }

        /* type
        * 0 = discount, credit
        * 1 = ongkir, debit
        */

        if ($request->type == 1) {
            $dc = 'd';
        } elseif ($request->type == 0) {
            $dc = 'c';
        }

        $condition = PurchaseCondition::create([
            'purchase_header_id' => $id,
            'type' => $request->type,
            'dece' => $dc,
            'currency' => $request->currency,
            'amount' => $request->amount,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        return $condition;
    }

    public function editCondition($id, Request $request)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        // validation
        $this->validate(request(), [
            'type' => 'required|numeric|min:0|max:1',
            'currency' => 'nullable',
            'amount' => 'required'
        ]);

        $condition = PurchaseCondition::find($id);

        if (!$condition) {
            return response()->json([
                'message' => 'Condition not found'
            ], 404);
        }

        $cek_condition_type = PurchaseCondition::where('type', $request->type)
            ->where('purchase_header_id', $condition->purchase_header_id)
            ->where('id', '<>', $id)
            ->first();

        if ($cek_condition_type) {
            return response()->json([
                'message' => 'Can\'t Input condition type more than once'
            ], 422);
        }

        /* type
        * 0 = discount, credit
        * 1 = ongkir, debit
        */

        if ($request->type == 1) {
            $dc = 'd';
        } elseif ($request->type == 0) {
            $dc = 'c';
        }

        $condition->update([
            'type' => $request->type,
            'dece' => $dc,
            'currency' => $request->currency,
            'amount' => $request->amount,
            'updated_by' => Auth::user()->id
        ]);

        return $condition;
    }

    public function deleteCondition($id)
    {
        Auth::user()->cekRoleModules(['purchase-orders-update']);

        $condition = PurchaseCondition::find($id);

        if (!$condition) {
            return response()->json([
                'message' => 'Condition not found'
            ], 404);
        }

        $condition->delete();

        return response()->json([
            'message' => 'success delete condition'
        ], 200);
    }

    public function getReleaseStrategy($amount, $plant, $mat_group, $cost_center)
    {
        // get po release object
        $release_object = ReleaseObject::where(DB::raw("LOWER(code)"), 'LIKE', "%".'po'."%")
            ->where('deleted', false)
            ->first();

        if (!$release_object) {
            return [
                [
                    'id' => null,
                    'match' => false,
                    'message' => 'release strategy not match, release object PO not available'
                ]
            ];
        }

        // get po release group
        $release_group_id = ReleaseGroup::where('release_object_id', $release_object->id)
            ->where('active', true)
            ->where('deleted', false)
            ->pluck('id');
        
        if (count($release_group_id) == 0) {
            return [
                [
                    'id' => null,
                    'match' => false,
                    'message' => 'release strategy not match, release group not available or inactive'
                ]
            ];
        }

        // get po release strategy
        $release_startegy = ReleaseStrategy::whereIn('release_group_id', $release_group_id)
            ->where('deleted', false)
            ->where('active', true)
            ->get();

        if (count($release_startegy) == 0) {
            return [
                [
                    'id' => null,
                    'match' => false,
                    'message' => 'release strategy not match, release strategy not available'
                ]
            ];
        }

        $match = [];
        foreach ($release_startegy as $s) {
            $valid = [];
            $messages = [];
            $startegy = ReleaseStrategy::find($s->id);

            $group = ReleaseGroup::find($s->release_group_id);

            // get classification parameter
            // $calssification_params = $startegy->release_group->classification->parameters;
            $calssification_params = ClassificationParameter::where('classification_id', $group->classification_id)->get();

            // get plant parameter id
            $plant_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'plant';
            })->first();

            $plant_param_id = $plant_params ? $plant_params->id : 0;

            // get mat group parameter id
            $mat_group_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'material_group';
            })->first();

            $mat_group_param_id = $mat_group_params ? $mat_group_params->id : 0;

            // get cost center parameter id
            $cost_center_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'cost_center';
            })->first();

            $cost_center_param_id = $cost_center_params ? $cost_center_params->id : 0;

            // get amount parameter id
            $amount_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'amount';
            })->first();

            $amount_param_id = $amount_params ? $amount_params->id : 0;

            // get strategy parameter value
            $strategy_parameter_value = ReleaseStrategyParameter::where('release_strategy_id', $s->id)->get();

            // check plant match with classification
            $plant_values = $strategy_parameter_value->where('classification_parameter_id', $plant_param_id)->first();

            $plant_value = $plant_values ? $plant_values->values : [];

            if (in_array($plant, $plant_value)) {
                array_push($valid, '1');
                $plant_match = $plant;
            } else {
                array_push($valid, '0');
                array_push($messages, 'plant');
            }

            // check matgroup match with classification
            $matgroup_values = $strategy_parameter_value->where('classification_parameter_id', $mat_group_param_id)->first();

            $matgroup_value = $matgroup_values ? $matgroup_values->values : [];

            if (in_array($mat_group, $matgroup_value)) {
                array_push($valid, '1');
                $mat_group_match = $mat_group;
            } else {
                array_push($valid, '0');
                array_push($messages, 'material group');
            }

            // check costcenter match with classification
            $costcenter_values = $strategy_parameter_value->where('classification_parameter_id', $cost_center_param_id)->first();

            $costcenter_value = $costcenter_values ? $costcenter_values->values : [];

            if (in_array($cost_center, $costcenter_value)) {
                array_push($valid, '1');
                $cost_center_match = $cost_center;
            } else {
                array_push($valid, '0');
                array_push($messages, 'cost center');
            }

            // check amount match with classification
            $amount_values = $strategy_parameter_value->where('classification_parameter_id', $amount_param_id)->first();

            $amount_value = $amount_values ? explode("-",$amount_values->values) : explode("-","0-0");

            $amount_int = (int) $amount;
            $batas_bawah = (int) $amount_value[0];
            $batas_atas = (int) $amount_value[1];
            
            if ($amount_int >= $batas_bawah && $amount_int <= $batas_atas) {
                array_push($valid, '1');
                $amount_match = $amount;
            } else {
                array_push($valid, '0');
                array_push($messages, 'amount');
            }

            if (in_array('0', $valid)) {
                array_push($messages, 'not match with release strategy '.$startegy->code.'');

                $match[] = [
                    'id' => $s->id,
                    'match' => false,
                    'message' => implode(', ',$messages)
                ];
            } else {
                $match[] = [
                    'id' => $s->id,
                    'match' => true,
                    'message' => implode(', ',$messages)
                ];
            }
        }

        return $match;
    }

    public function cekReleaseStrategy($amount, $plant, $mat_group, $cost_center)
    {
        // get po release object
        $release_object = ReleaseObject::where(DB::raw("LOWER(code)"), 'LIKE', "%".'po'."%")
            ->where('deleted', false)
            ->first();

        if (!$release_object) {
            return [];
        }

        // get po release group
        $release_group_id = ReleaseGroup::where('release_object_id', $release_object->id)
            ->where('active', true)
            ->where('deleted', false)
            ->pluck('id');
        
        if (count($release_group_id) == 0) {
            return [];
        }

        // get po release strategy
        $release_startegy = ReleaseStrategy::whereIn('release_group_id', $release_group_id)
            ->where('deleted', false)
            ->where('active', true)
            ->get();

        if (count($release_startegy) == 0) {
            return [];
        }

        $match = [];
        foreach ($release_startegy as $s) {
            $valid = [];
            $messages = [];
            $startegy = ReleaseStrategy::find($s->id);

            $group = ReleaseGroup::find($s->release_group_id);

            // get classification parameter
            // $calssification_params = $startegy->release_group->classification->parameters;
            $calssification_params = ClassificationParameter::where('classification_id', $group->classification_id)->get();

            // get plant parameter id
            $plant_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'plant';
            })->first();

            $plant_param_id = $plant_params ? $plant_params->id : 0;

            // get mat group parameter id
            $mat_group_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'material_group';
            })->first();

            $mat_group_param_id = $mat_group_params ? $mat_group_params->id : 0;

            // get cost center parameter id
            $cost_center_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'cost_center';
            })->first();

            $cost_center_param_id = $cost_center_params ? $cost_center_params->id : 0;

            // get amount parameter id
            $amount_params = $calssification_params->filter(function ($item){
                return strtolower($item->name) == 'amount';
            })->first();

            $amount_param_id = $amount_params ? $amount_params->id : 0;

            // get strategy parameter value
            $strategy_parameter_value = ReleaseStrategyParameter::where('release_strategy_id', $s->id)->get();

            // check plant match with classification
            $plant_values = $strategy_parameter_value->where('classification_parameter_id', $plant_param_id)->first();

            $plant_value = $plant_values ? $plant_values->values : [];

            if (in_array($plant, $plant_value)) {
                array_push($valid, '1');
                $plant_match = $plant;
            } else {
                array_push($valid, '0');
                array_push($messages, 'plant');
            }

            // check matgroup match with classification
            $matgroup_values = $strategy_parameter_value->where('classification_parameter_id', $mat_group_param_id)->first();

            $matgroup_value = $matgroup_values ? $matgroup_values->values : [];

            if (in_array($mat_group, $matgroup_value)) {
                array_push($valid, '1');
                $mat_group_match = $mat_group;
            } else {
                array_push($valid, '0');
                array_push($messages, 'material group');
            }

            // check costcenter match with classification
            $costcenter_values = $strategy_parameter_value->where('classification_parameter_id', $cost_center_param_id)->first();

            $costcenter_value = $costcenter_values ? $costcenter_values->values : [];

            if (in_array($cost_center, $costcenter_value)) {
                array_push($valid, '1');
                $cost_center_match = $cost_center;
            } else {
                array_push($valid, '0');
                array_push($messages, 'cost center');
            }

            // check amount match with classification
            $amount_values = $strategy_parameter_value->where('classification_parameter_id', $amount_param_id)->first();

            $amount_value = $amount_values ? explode("-",$amount_values->values) : explode("-","0-0");

            $amount_int = (int) $amount;
            $batas_bawah = (int) $amount_value[0];
            $batas_atas = (int) $amount_value[1];
            
            if ($amount_int >= $batas_bawah && $amount_int <= $batas_atas) {
                array_push($valid, '1');
                $amount_match = $amount;
            } else {
                array_push($valid, '0');
                array_push($messages, 'amount');
            }

            if (in_array('0', $valid)) {
                array_push($messages, 'not match with release strategy '.$startegy->code.'');

                $match[] = [
                    'id' => $s->id,
                    'match' => false,
                    'message' => implode(', ',$messages)
                ];
            } else {
                $match[] = [
                    'id' => $s->id,
                    'match' => true,
                    'message' => implode(', ',$messages)
                ];
            }
        }

        return $match;
    }
}
