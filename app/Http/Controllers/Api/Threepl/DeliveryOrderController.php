<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\MaterialAccount;
use App\Models\Storage as MasterStorage;
use App\Models\Reservation;
use App\Models\DeliveryApproval;
use App\Models\DeliveryHeader;
use App\Models\DeliveryItem;
use App\Models\DeliverySerial;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\MovementHistory;
use App\Models\MaterialStorage;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;

class DeliveryOrderController extends Controller
{
    /**
     * Get New Code For Movement Document.
     *
     * @return new Movement Document
     */
    public function getMovementDoc()
    {
        // get reservation code 
        $code = TransactionCode::where('code', 'M')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'M';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    /**
     * Show the Delivery Header
     *
     * @return \App\Models\DeliveryHeader
     */
    public function index()
    {
        Auth::user()->cekRoleModules(['delivery-orders-view']);

        $do = (new DeliveryHeader)->newQuery();

        $do->where('deleted', false)->with([
            'dlv_to_storage', 'transport_type', 'createdBy'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $do->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(delivery_code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(delivery_note)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(courier_name)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sales_org)"), 'LIKE', "%".$q."%");
            });
        }

        // filter type
        if (request()->has('delivery_types')) {
            $do->whereIn('delivery_types', request()->input('delivery_types'));
        }

        // filter plant sender
        if (request()->has('plant')) {
            $plant = request()->input('plant');
            $do->whereHas('item', function ($q) use ($plant){
                $q->whereIn('plant_id', $plant);
            });
        }

        // filter storage
        if (request()->has('storage')) {
            $do->whereIn('dlv_to_storage_id', request()->input('storage'));
        }

        // filter status
        if (request()->has('status')) {
            $do->whereIn('status', request()->input('status'));
        }

        // filter delivery_code
        if (request()->has('delivery_code')) {
            $delivery_code = request()->input('delivery_code');
            $do->where(DB::raw("LOWER(delivery_code)"), 'LIKE', "%".strtolower($delivery_code)."%");
        }

        // filter destination
        if (request()->has('destination')) {
            $do->whereIn('dlv_to_storage_id', request()->input('destination'));
        }

        // filter delivery_date
        if (request()->has('delivery_date')) {
            $do->whereBetween('delivery_date', [request()->delivery_date[0], request()->delivery_date[1]]);
        }

        // filter reference
        if (request()->has('reference')) {
            $do->where(DB::raw("LOWER(sales_org)"), 'LIKE', "%".strtolower(request()->input('reference'))."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $do->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $do->orderBy('id', 'desc');
        }

        $do = $do->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $do->transform(function($data) {
            // find sender from first do item
            $item = $data->item->first();
            $reservation = Reservation::find($item->reservation_id);

            $data->reservation_code = $reservation->reservation_code;
            $data->plant_sender_id = $item->plant_id;
            $data->storage_sender_id = $item->storage_id;
            $data->plant_sender = $item->plant;
            $data->storage_sender = $item->storage;

            return $data;
        });

        return $do;
    }

    /**
     * Show the Delivery Header & Detail
     *
     * @param  $id
     * @return \App\Models\DeliveryHeader
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-view']);

        $do = DeliveryHeader::with([
            'dlv_to_plant', 'dlv_to_storage', 'customer', 'item'
        ])
        ->with('dlv_to_storage.location')
        ->with('item.reservation')
        ->with('item.material')
        ->with('item.material.images')
        ->with('item.plant')
        ->with('item.storage')
        ->with('item.uom')
        ->with('item.stock_batch')
        ->find($id);

        if (!$do) {
            return response()->json([
                'message' => 'Delivery Orders not Found'
            ], 404);
        }

        // if do status send/goods issued add object material doc for reversal do
        $movement_type = MovementType::where('code', 641)->first();

        $movement = MovementHistory::where('movement_type_id', $movement_type->id)
        ->where('material_slip', $do->delivery_code)
        ->orWhere('delivery_no', $do->delivery_code)
        ->orderBy('id', 'desc')
        ->first();

        if ($do->status == 2) {
            $do->material_doc = $movement->material_doc;
        }

        return $do;
    }

    /**
     * Show the Delivery Header & Detail
     *
     * @param  $delivery_code
     * @return \App\Models\DeliveryHeader
     */
    public function showByCode($delivery_code)
    {
        Auth::user()->cekRoleModules(['delivery-orders-view']);

        $do = DeliveryHeader::with([
            'dlv_to_plant', 'dlv_to_storage', 'customer'
        ])
        ->with('dlv_to_storage.location')
        ->where('delivery_code', $delivery_code)
        ->first();

        if (!$do) {
            return response()->json([
                'message' => 'Delivery Orders not Found'
            ], 404);
        }

        if ($do->status == 0 | $do->status == 1) {
            return response()->json([
                'message' => 'Delivery Orders must Sent first'
            ], 422);
        }

        // if have storage organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            if (!in_array($do->dlv_to_storage_id, $storage_id)) {
                $storage = MasterStorage::find($do->dlv_to_storage_id);
                return response()->json([
                    'message' => 'You not have organization parameter for storage '.$storage->description.''
                ], 422);
            }
        }

        // do item map qty do - gr qty 605
        $mvtype = MovementType::where('code', 605)->first();
        $items = DeliveryItem::where('delivery_header_id', $do->id)
        ->with('reservation')
        ->with('material')
        ->with('plant')
        ->with('storage')
        ->with('stock_batch')
        ->get()
        ->map(function ($data) use ($delivery_code, $mvtype) {
            $gr_do_item = MovementHistory::where('delivery_no', $delivery_code)
            ->where('movement_type_id', $mvtype->id)
            ->where('de_ce', 'd')
            ->where('material_id', $data->material_id)
            ->where('batch_no', $data->batch)
            ->get();

            if ($gr_do_item) {
                $gr_item = $gr_do_item->sum('quantity_entry');
            } else {
                $gr_item = 0;
            }

            $data->pick_qty = $data->pick_qty - $gr_item;

            return $data;
        })->reject(function ($data) {
            return $data->pick_qty < 1;
        })->values();

        $do->item = $items;

        if (count($do->item) == 0) {
            return response()->json([
                'message' => 'Delivery Orders '.$delivery_code.' already goods recived'
            ], 422);
        }

        return $do;
    }

    /**
     * Convert Approved Reservation as Delivery Order For Internal Purchase Type
     *
     * @param  string  $do_code
     * @return \PDF
     */
    public function pdf($id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-pdf']);

        $do = DeliveryHeader::find($id);

        if (!$do) {
            return response()->json([
                'message' => 'Delivery Orders not Found'
            ], 404);
        }

        if ($do->status == 0 | $do->status == 1) {
            return response()->json([
                'message' => 'Delivery Orders must Sent first'
            ], 422);
        }

        $do->item->map(function ($item){
            // if item have batch
            if ($item->batch) {  
                // find first stock main by material & storage
                $stock_main = StockMain::where('storage_id', $item->storage_id)
                    ->where('material_id', $item->material_id)
                    ->first();

                // find first stock batch by stock main & batch number
                $stock_batch = StockBatch::where('batch_number', $item->batch)
                    ->where('stock_main_id', $stock_main->id)
                    ->first();

                $item->stock_batch = $stock_batch;
            }

            return $item;
        });

        $total = collect($do->item)->groupBy('material.uom_id')->values()->map(function ($row) {
            $uom = Uom::find($row[0]['material']->uom_id);
            return number_format($row->sum('pick_qty'),  $uom->decimal, ',', '.') .' '.$uom->code;
        });

        $do->total = $total;

        $pdf = PDF::loadview('pdf.do', compact('do'))->setPaper('a4', 'potrait');
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();

        // text at bottom
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 10, array(0, 0, 0));

        // text at top
        // $canvas->page_text(498, 10, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 10, array(0, 0, 0));
        
        return $pdf->download('Delivery_Orders.pdf');
    }

    public function editDO(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-update']);

        $this->validate(request(), [
            'delivery_note' => 'nullable|max:191',
            'item' => 'array',
            'item.*.id' => 'nullable|exists:delivery_items,id',
            'item.*.material_id' => 'required|exists:materials,id',
            'item.*.ref_item' => 'required|numeric',
            'item.*.qty' => 'required|numeric|min:1',
        ]);

        $header = DeliveryHeader::find($id);

        if (!$header) {
            return response()->json([
                'message'   => 'Data Not Found',
            ], 404);
        }

        if ($header->status == 2) {
            return response()->json([
                'message' => 'Delivery Orders already Sent, Cant Edit anymore'
            ], 422);
        }

        if ($header->status == 3) {
            return response()->json([
                'message' => 'Delivery Orders already Received, Cant Edit anymore'
            ], 422);
        }

        // validasi ref item = from delivery item
        foreach ($request->item as $key => $val) {
            $validate_item = DeliveryItem::where('delivery_item', $val['ref_item'])
                ->where('delivery_header_id', $id)
                ->first();

            if (!$validate_item) {
                $line = $key + 1;
                return response()->json([
                    'message' => 'Ref Item line '.$line.' is invalid, ref item must exist in delivery item'
                ], 422);
            }
        }

        // cek one delivery item in multiple batch
        $aSortedArray = array();
        foreach ($request->item as $aArray) {
            $bSet = false;
            foreach ($aSortedArray as $iPos => $aTempSortedArray) {
                if ($aTempSortedArray['ref_item'] == $aArray['ref_item']) {

                    $aSortedArray[$iPos]['qty'] += $aArray['qty'];

                    $bSet = true;
                }
            }

            if(!$bSet) {
                $aSortedArray[] = [
                    'id' => $aArray['id'], 
                    'material_id' => $aArray['material_id'], 
                    'ref_item' => $aArray['ref_item'],
                    'qty' => $aArray['qty'],
                    'batch' => isset($aArray['batch']) ? $aArray['batch'] : null
                ];
            }
        }

        // validate pick qty < delivery qty
        foreach ($aSortedArray as $key => $val) {
            if (isset($val['id'])) {
                $item = DeliveryItem::find($val['id']);
            } else {
                $item = DeliveryItem::where('delivery_header_id', $id)
                ->where('delivery_item', $val['ref_item'])
                ->orderBy('id', 'desc')
                ->first();
            }

            if ($val['qty'] > $item->delivery_qty) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'item.'.$key.'.qty'  => ['quantity reservation cant greather than '.$item->delivery_qty.'']
                    ]
                ], 422);
            }
        }

        // validasi serial profile
        foreach ($request->item as $key => $val) {
            if (isset($val['id'])) {
                $item = DeliveryItem::find($val['id']);
            } else {
                $item = DeliveryItem::where('delivery_header_id', $id)
                ->where('delivery_item', $val['ref_item'])
                ->orderBy('id', 'desc')
                ->first();
            }

            $material = Material::find($item->material_id);
            if ($material->serial_profile) {
                $this->validate(request(), [
                    'item.'.$key.'.serial' => 'required|array',
                    'item.'.$key.'.serial.*' => 'exists:stock_serials,serial',
                ]);

                // validate unique serial
                $dups = [];
                foreach (array_count_values($val['serial']) as $valk => $c) {
                    if($c > 1) $dups[] = $valk;
                }

                if ($dups) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'item.'.$key.'.serial'  => ['a Serial cant entry more than once']
                        ]
                    ], 422);
                }

                // validate total serial
                $total_serial = count($val['serial']);
                if ($val['qty'] != $total_serial) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'item.'.$key.'.serial'  => ['Total Serial not match with total quantity']
                        ]
                    ], 422);
                }
            }

            // validate batch
            $first_item = DeliveryItem::find($request->item[0]['id']);

            $stock_main_exist = StockMain::where('storage_id', $first_item->storage_id)
                ->where('material_id', $item->material_id)
                ->first();

            $stock_batch_exist = $stock_main_exist->batch_stock->where('deleted', false)->first();

            if ($stock_batch_exist) {
                $this->validate(request(), [
                    'item.'.$key.'.batch' => 'required|exists:stock_batches,id'
                ]);
            } else {
                $this->validate(request(), [
                    'item.'.$key.'.batch' => 'nullable|exists:stock_batches,id'
                ]);
            }
        }

        // validate duplicate batch & ref item
        foreach ($request->item as $current_key => $current_array) {
            foreach ($request->item as $search_key => $search_array) {
                if ($search_array['batch'] == $current_array['batch'] && $search_array['ref_item'] == $current_array['ref_item']) {
                    if ($search_key != $current_key) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$search_key.'.batch'  => ['Can not enter the same batch more than once']
                            ]
                        ], 422);
                    }
                }
            }
        }

        try {
            DB::beginTransaction();

            foreach ($request->item as $key => $val) {
                $batch_id = isset($val['batch']) ? $val['batch'] : 0;
                $batch = StockBatch::find($batch_id);

                // update delivery item if id exist
                if (isset($val['id'])) {
                    // get delivery item
                    $di = DeliveryItem::find($val['id']);

                    // cek batch qty
                    if ($batch) {
                        // find material
                        $material = Material::find($val['material_id']);

                        // find stock material storage pengirim
                        $stock_pengirim = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $di->storage_id)
                            ->first();

                        // find batch stock pengirim
                        $batch_credit = $stock_pengirim->batch_stock->where('batch_number', $batch->batch_number)->first();

                        // if stock batch pengirim qty unres < qty
                        if ($batch_credit->qty_unrestricted < $val['qty']) {
                            throw new \Exception('Stock batch '.$batch->batch_number.' material '.$material->description.', is not sufficient');
                        }
                    }

                    // update delivery item
                    $di->update([
                        'batch' => $batch ? $batch->batch_number : null,
                        'stock_batch_id' => $val['batch'],
                        'pick_qty' => $val['qty'],
                        'updated_by' => Auth::user()->id
                    ]);
                    $created_item = DeliveryItem::find($val['id']);
                } else { //create new item if id not exist
                    $item_ref = DeliveryItem::where('delivery_header_id', $id)
                        ->where('delivery_item', $val['ref_item'])
                        ->orderBy('id', 'desc')
                        ->first();

                    // cek batch qty
                    if ($batch) {
                        // find material
                        $material = Material::find($val['material_id']);

                        // find stock material storage pengirim
                        $stock_pengirim = StockMain::where('material_id', $val['material_id'])
                            ->where('storage_id', $item_ref->storage_id)
                            ->first();

                        // find batch stock pengirim
                        $batch_credit = $stock_pengirim->batch_stock->where('batch_number', $batch->batch_number)->first();

                        // if stock batch pengirim qty unres < qty
                        if ($batch_credit->qty_unrestricted < $val['qty']) {
                            throw new \Exception('Stock batch '.$batch->batch_number.' material '.$material->description.', is not sufficient');
                        }
                    }

                    $created_item = DeliveryItem::create([
                        'delivery_header_id' => $id,
                        'delivery_item' => $item->delivery_item,
                        'sequence' => $item_ref->sequence + 1,//lalu sequence ini untuk pemisah batch number. misal dari reservation minta barang 50qty. lalu pas mau dikirim DO qty 50 ini perlu di supply dari lebih dari 1 batch. misal Batch 100 qty 30 dan Batch 101 qty 20
                        'plant_id' => $item_ref->plant_id,
                        'storage_id' => $item_ref->storage_id,
                        'batch' => $batch ? $batch->batch_number : null,
                        'stock_batch_id' => $val['batch'],
                        'reservation_id' => $item_ref->reservation_id,
                        'material_id' => $item_ref->material_id,
                        'uom_id' => $item_ref->uom_id,
                        'delivery_qty' => 0, // null deliv qty for new item
                        'pick_qty' => $val['qty'], //Pick qty nanti diinput pas edit DO sesuai dgn batch number
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id
                    ]);
                }

                // validate & insert serial to delivery serial
                $item2 = DeliveryItem::find($created_item->id);
                $material = Material::find($item2->material_id);
                if ($material->serial_profile) {
                    $dlv_serials = DeliverySerial::where('delivery_header_id', $id)
                        ->where('sequence', $created_item->sequence)
                        ->where('material_id', $item2->material_id)
                        ->pluck('serial')
                        ->toArray();

                    // get serial from request
                    $req_serials = collect($val['serial'])->toArray();

                    // compare serial with serial in dlv serial
                    $unused_serials = array_diff($dlv_serials, $req_serials);
                    $new_serials = array_diff($req_serials, $dlv_serials);

                    foreach ($val['serial'] as $serial) {
                        $serials = StockSerial::where('serial', $serial)
                            ->where('material_id', $item2->material_id)
                            ->where('storage_id', $item2->storage_id)
                            ->where('stock_type', 1)//stock type must unrest
                            ->first();

                        if (!$serials) {
                            throw new \Exception('Serial '.$serial.', not found');
                        }

                        // insert into delivery serial
                        DeliverySerial::updateOrCreate(
                            [
                                'delivery_header_id' => $id,
                                // 'delivery_item' => null,
                                'sequence' => $created_item->sequence,
                                'material_id' => $item2->material_id,
                                'serial' => $serial,
                            ],
                            [
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id,
                            ]
                        );
                    }

                    // delete unsed delivery serial
                    foreach ($unused_serials as $serial) {
                        $userial = DeliverySerial::where('delivery_header_id', $id)
                            ->where('sequence', $created_item->sequence)
                            ->where('material_id', $item2->material_id)
                            ->first();

                        if ($userial) {
                            $userial->delete();
                        }
                    }
                }
            }

            // record do approval
            DeliveryApproval::create([
                'delivery_header_id' => $header->id,
                'user_id' => Auth::user()->id,
                'status_from' => $header->status,
                'status_to' => 1,
                'notes' => null,
            ]);

            // update status header
            $header->update([
                'delivery_note' => $request->delivery_note,
                'status' => 1, //1. Picking/Packing
                'updated_by' => Auth::user()->id
            ]);

            DB::commit();

            return response()->json([
                'message' => 'success edit DO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while edit DO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function sendDO(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['delivery-orders-send']);

        $header = DeliveryHeader::find($id);

        $this->validate(request(), [
            'transport_type_id' => 'required|exists:transport_types,id',
            'transport_id' => 'nullable',
            'courier_name' =>'nullable',
        ]);

        if (!$header) {
            return response()->json([
                'message' => 'Delivery Orders Not Found'
            ], 404);
        }

        if ($header->status == 2) {
            return response()->json([
                'message' => 'Delivery Orders already Sent, Cant Sent again'
            ], 422);
        }

        if ($header->status == 3) {
            return response()->json([
                'message' => 'Delivery Orders already Received, Cant Sent again'
            ], 422);
        }

        $j = collect($header->item)->values();

        $aSortedArray = [];
        foreach ($j as $aArray) {
            $bSet = false;
            foreach ($aSortedArray as $iPos => $aTempSortedArray) {
                if ($aTempSortedArray['material_id'] == $aArray['material_id']) {

                    $aSortedArray[$iPos]['pick_qty'] += $aArray['pick_qty'];

                    $bSet = true;
                }
            }

            if(!$bSet) {
                $aSortedArray[] = [
                    'id' => $aArray['id'], 
                    'delivery_header_id' => $aArray['delivery_header_id'],
                    'delivery_item' => $aArray['delivery_item'],
                    // 'sequence' => $aArray['sequence'],
                    'plant_id' => $aArray['plant_id'],
                    'storage_id' => $aArray['storage_id'],
                    'batch' => $aArray['batch'],
                    'reservation_id' => $aArray['reservation_id'],
                    'material_id' => $aArray['material_id'],
                    'uom_id' => $aArray['uom_id'],
                    'delivery_qty' => $aArray['delivery_qty'],
                    'pick_qty' => $aArray['pick_qty'],
                ];
            }
        }

        $movement_doc = $this->getMovementDoc();

        try {
            DB::beginTransaction();
            
            // record to movement 641
            $mvt = MovementType::where('code', 641)->first();
            
            $item = 1;

            foreach ($header->item as $val) {
                $material = Material::find($val['material_id']);
                
                if (!$val['pick_qty'] | $val['pick_qty'] == 0) {
                    throw new \Exception('Item Pick Qty for Material '.$material->description.' cant null');
                }

                // record to movement history table
                $movements_document = MovementHistory::where('material_doc', $movement_doc)
                    ->whereNotNull('material_id')
                    ->count();

                $credit_item_number = $movements_document + 1;
                $debit_item_number = $credit_item_number + 1;

                $material = Material::find($val['material_id']);
                // record to movement history table
                $movement_credit = MovementHistory::create([
                    'material_doc' => $movement_doc,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $mvt->id,
                    'de_ce' => 'c',
                    'movement_type_reason_id' => null,
                    'plant_id' => $val['plant_id'],
                    'storage_id' => $val['storage_id'],
                    'posting_date' => now(),
                    'document_date' => now(),
                    'material_slip' => $header->delivery_code,
                    'doc_header_text' => $header->delivery_note,
                    'work_center_id' => null,
                    'cost_center_id' => null,

                    'item_no' => $credit_item_number,
                    'material_id' => $val['material_id'],
                    'material_description' => $material->description,
                    'quantity_entry' => $val['pick_qty'],
                    'entry_uom_id' => $val['uom_id'],

                    'quantity' => $val['pick_qty'],
                    'base_uom_id' => $val['uom_id'],

                    'delivery_no' => $header->delivery_code,
                    'delivery_item' => $val['delivery_item'],

                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                $movement_debit = MovementHistory::create([
                    'material_doc' => $movement_doc,
                    'doc_year' => date('Y'),
                    'movement_type_id' => $mvt->id,
                    'de_ce' => 'd',
                    'movement_type_reason_id' => null,
                    'plant_id' => $val['plant_id'],
                    'storage_id' => $val['storage_id'],
                    'posting_date' => now(),
                    'document_date' => now(),
                    'material_slip' => $header->delivery_code,
                    'doc_header_text' => $header->delivery_note,
                    'work_center_id' => null,
                    'cost_center_id' => null,

                    'item_no' => $debit_item_number,
                    'material_id' => $val['material_id'],
                    'material_description' => $material->description,
                    'quantity_entry' => $val['pick_qty'],
                    'entry_uom_id' => $val['uom_id'],

                    'quantity' => $val['pick_qty'],
                    'base_uom_id' => $val['uom_id'],

                    'delivery_no' => $header->delivery_code,
                    'delivery_item' => $val['delivery_item'],

                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id
                ]);

                // cek stock material in storage sender
                $stock_pengirim = StockMain::where('material_id', $val['material_id'])
                    ->where('storage_id', $val['storage_id'])
                    ->first();

                // get material
                $material = Material::find($val['material_id']);

                // update if stock exist
                if ($stock_pengirim) {
                    // throw new exception if stock not enought
                    if ($stock_pengirim->qty_unrestricted < $val['pick_qty']) {
                        throw new \Exception('Stock material '.$material->description.', is not sufficient');
                    }
                    $qty_credit_unrest = $stock_pengirim->qty_unrestricted - $val['pick_qty'];

                    $stock_pengirim->update([
                        'qty_unrestricted' => $qty_credit_unrest,
                        'updated_by' => Auth::user()->id
                    ]);

                    // insert to stock penerima
                    $stock_penerima = $stock_pengirim;

                    if ($stock_penerima) {
                        $qty_debit_transit = $stock_penerima->qty_transit + $val['pick_qty'];
                        $stock_penerima->update([
                            'qty_transit' => $qty_debit_transit,
                            'updated_by' => Auth::user()->id
                        ]);
                    }

                    $stock_credit = $stock_pengirim;
                    $stock_debit = $stock_penerima;
                } else {
                    // throw new exception if stock not enought
                    throw new \Exception('Stock material '.$material->description.', is not sufficient');
                }

                // insert batch
                $selected_batch = StockBatch::where('batch_number', $val['batch'])->first();

                if ($selected_batch) {
                    // get stock batch pengirim
                    $batch_credit = $stock_credit->batch_stock->where('batch_number', $selected_batch->batch_number)->first();

                    // if stock batch pengirim qty unres < qty
                    if ($batch_credit->qty_unrestricted < $val['pick_qty']) {
                        throw new \Exception('Stock batch '.$selected_batch->batch_number.' material '.$material->description.', is not sufficient');
                    }

                    $qty_batch_pengirim = $batch_credit->qty_unrestricted - $val['pick_qty'];

                    // update batch pengirim qty unres
                    $batch_credit->update([
                        'qty_unrestricted' => $qty_batch_pengirim
                    ]);

                    // get stock batch penerima
                    $batch_debit = $batch_credit;

                    if ($batch_debit) {//update batch stock penerima
                        $qty_batch_pengirim = $batch_debit->qty_transit + $val['pick_qty'];

                        $batch_debit->update([
                            'qty_transit' => $qty_batch_pengirim,
                        ]);
                    }

                    // update batch in movement
                    $movement_credit->update([
                        'batch_no' => $batch_credit->batch_number,
                        'amount' => isset($batch_credit->batch_number) ? StockBatch::where('batch_number', $batch_credit->batch_number)->first()->unit_cost : 0,
                        'currency' => 'IDR'
                    ]);
                    $movement_debit->update([
                        'batch_no' => $batch_debit->batch_number,
                        'amount' => isset($batch_credit->batch_number) ? StockBatch::where('batch_number', $batch_credit->batch_number)->first()->unit_cost : 0,
                        'currency' => 'IDR'
                    ]);
                } else {
                    // get material cost from material acount
                    $material_account = MaterialAccount::where('plant_id', $val['plant_id'])
                        ->where('material_id', $val['material_id'])
                        ->first();

                    $map = $material_account ? $material_account->moving_price : 0;

                    // update cost in movement
                    $movement_credit->update([
                        'amount' => $map,
                        'currency' => 'IDR'
                    ]);
                    $movement_debit->update([
                        'amount' => $map,
                        'currency' => 'IDR'
                    ]);
                }

                // validate & insert serial to stock serial, serial history
                $material = Material::find($val['material_id']);
                if ($material->serial_profile) {
                    $serials = $val->serials;
                    foreach ($serials as $serial) {
                        // update stock serial table
                        $serials = StockSerial::where('serial', $serial->serial)
                            ->where('material_id', $movement_credit->material_id)
                            ->where('storage_id', $movement_credit->storage_id)
                            // ->where('batch_number', $selected_batch->batch_number)
                            ->where('stock_type', 1)//stock type must unrest
                            ->first();

                        if (!$serials) {
                            throw new \Exception('Serial '.$serial->serial.', not found');
                        }
                        
                        $serials->update([
                            'plant_id' => $movement_debit->plant_id,
                            'storage_id' => $movement_debit->storage_id,
                            'stock_type' => 2,//1=unrest,2=in-transit,3=quality,4=blocked
                            // 'serial' => $serial,
                            'status' => 1,//1=AVLB,2=ESTO,3=EQUI
                            // 'batch_number' => $movement->batch_no, //get batch no from movement
                            // 'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id,
                            // 'vendor_id' => null,
                            // 'customer_id' => null, // will be used in 601
                        ]);

                        // record serial history credit
                        SerialHistory::create([
                            'movement_history_id' => $movement_credit->id,
                            'plant_id' => $movement_credit->plant_id,
                            'storage_id' => $movement_credit->storage_id,
                            'material_id' => $movement_credit->material_id,
                            'serial' => $serial->serial,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // record serial history debit
                        SerialHistory::create([
                            'movement_history_id' => $movement_debit->id,
                            'plant_id' => $movement_debit->plant_id,
                            'storage_id' => $movement_debit->storage_id,
                            'material_id' => $movement_debit->material_id,
                            'serial' => $serial->serial,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);
                    }
                }

                // update reservation
                $reservation = Reservation::find($val['reservation_id']);

                // get do header status sent
                $sent_do_header = DeliveryHeader::whereIn('status', [2,3])->pluck('id');

                // sum qty sent in do
                $do_reservation = $reservation->delivery_orders->whereIn('delivery_header_id', $sent_do_header)->sum('pick_qty');
                
                // update fullfillment status if qty reservation match with all qty send do for selected reservation
                if ($reservation->quantity_base_unit == $do_reservation) {
                    $reservation->update([
                        'fulfillment_status' => 1,//0.Open, 1.Completed
                        'updated_by' => Auth::user()->id
                    ]);
                }

                // throw exception if qty do greather than reservation qty
                $qty_all = $do_reservation + $val['pick_qty'];
                if ($qty_all > $reservation->quantity_base_unit) {
                    throw new \Exception('Qty for material '.$material->code.', exceed Reservation '.$reservation->reservation_code.' qty');
                }

                if ($val['pick_qty'] == $val['delivery_qty']) {
                    $reservation->update([
                        'fulfillment_status' => 1,//0.Open, 1.Completed
                        'updated_by' => Auth::user()->id
                    ]);

                    // get all reservation
                    $cek = Reservation::whereNotNull('material_id')
                        ->where('reservation_code', $reservation->reservation_code)
                        ->where('status', 5)
                        ->count();

                    // get all reservation fulfillment status
                    $cekfull = Reservation::whereNotNull('material_id')
                        ->where('reservation_code', $reservation->reservation_code)
                        ->where('status', 5)
                        ->where('fulfillment_status', 1)
                        ->count();

                    if ($cek == $cekfull) {
                        // update reservation header fullfilment status
                        Reservation::whereNull('material_id')
                            ->where('reservation_code', $reservation->reservation_code)
                            ->first()
                            ->update([
                                'fulfillment_status' => 1,//0.Open, 1.Completed
                                'updated_by' => Auth::user()->id
                            ]);
                    }
                }
            }

            // record do approval
            DeliveryApproval::create([
                'delivery_header_id' => $header->id,
                'user_id' => Auth::user()->id,
                'status_from' => $header->status,
                'status_to' => 2,
                'notes' => $header->delivery_note,
            ]);

            // update delivery header
            $header->update([
                'transport_type_id' => $request->transport_type_id,
                'transport_id' => $request->transport_id,
                'courier_name' => $request->courier_name,
                'status' => 2, // 0. Open, 1. Picking/Packing, 2. Sent, 3. Received
                'updated_by' => Auth::user()->id,
                'sent_by' => Auth::user()->id,
                'sent_date' => now()
            ]);

            $code = TransactionCode::where('code', 'M')->first();
            $code->update(['lastcode' => $movement_credit->material_doc]);

            DB::commit();

            return response()->json([
                'message' => 'success send DO'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while send DO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }
}
