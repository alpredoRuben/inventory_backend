<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Threepl\StockDeterminationTemplateExport;
use App\Exports\Threepl\StockDeterminationInvalidExport;
use App\Imports\Threepl\StockDeterminationImport;

use App\Models\User;
use App\Models\StockDetermination;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\MaterialPlant;
use App\Models\StockDeterminationFailedUpload;
use App\Helpers\HashId;
use App\Helpers\Collection;

class StockDeterminationController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['stock-determination-view']);

        $stockDetermination = (new StockDetermination)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));

            // search plant
            $stockDetermination->whereHas('plant', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['plant' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search plant supply
            $stockDetermination->orWhereHas('supply_plant', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['supply_plant' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search storage
            $stockDetermination->orWhereHas('storage', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['storage' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search storage supply
            $stockDetermination->orWhereHas('supply_storage', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['supply_storage' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);
        }

        $stockDetermination->with([
            'plant', 'storage', 'supply_plant', 'supply_storage', 'createdBy', 'updatedBy'
        ]);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $stockDetermination->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $stockDetermination->orderBy('id', 'desc');
        }

        $stockDetermination = $stockDetermination->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $stockDetermination;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['stock-determination-view']);

        $stockDetermination = (new StockDetermination)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));

            // search plant
            $stockDetermination->whereHas('plant', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['plant' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search plant supply
            $stockDetermination->orWhereHas('supply_plant', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['supply_plant' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search storage
            $stockDetermination->orWhereHas('storage', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['storage' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search storage supply
            $stockDetermination->orWhereHas('supply_storage', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['supply_storage' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);
        }

        $stockDetermination->with([
            'plant', 'storage', 'supply_plant', 'supply_storage', 'createdBy', 'updatedBy'
        ]);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'plant':
                    $stockDetermination->join('plants','plants.id','=','stock_determinations.plant_id');
                    $stockDetermination->select('stock_determinations.*');
                    $stockDetermination->orderBy('plants.code',$sort_order);
                break;

                case 'storage':
                    $stockDetermination->join('storages','storages.id','=','stock_determinations.storage_id');
                    $stockDetermination->select('stock_determinations.*');
                    $stockDetermination->orderBy('storages.code',$sort_order);
                break;

                case 'supply_plant':
                    $stockDetermination->leftJoin('plants','plants.id','=','stock_determinations.supply_plant_id');
                    $stockDetermination->select('stock_determinations.*');
                    $stockDetermination->orderBy('plants.code', $sort_order);
                break;

                case 'supply_storage':
                    $stockDetermination->leftJoin('storages','storages.id','=','stock_determinations.supply_storage_id');
                    $stockDetermination->select('stock_determinations.*');
                    $stockDetermination->orderBy('storages.code',$sort_order);
                break;
                
                default:
                    $stockDetermination->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $stockDetermination->orderBy('id', 'desc');
        }

        $stockDetermination = $stockDetermination->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($stockDetermination['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $stockDetermination['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $stockDetermination;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        $this->validate(request(), [
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            'sequence' => 'nullable|numeric|min:0|digits_between:1,4',
            // 'supply_plant_id' => 'nullable',
            // 'supply_storage_id' => 'nullable',
			'ext_purchase' => 'nullable|boolean',
        ]);

        if ($request->ext_purchase == "0" || $request->ext_purchase == null) {
        	$this->validate(request(), [
        		'supply_plant_id' => 'required|exists:plants,id',
            	'supply_storage_id' => 'required|exists:storages,id',
            ]);
        }

        if ($request->storage_id == $request->supply_storage_id) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['Storage & Storage Supply Cant Same']
                ]
            ], 422);
        }

        $check = StockDetermination::where('ext_purchase', $request->ext_purchase)
    		->where('storage_id', $request->storage_id)
    		->where('supply_storage_id', $request->supply_storage_id)
    		->first();

        if ($check) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['Data Already Exists'],
                ]
            ], 422);
        }

        $check_sequence = StockDetermination::where('ext_purchase', $request->ext_purchase)
            ->where('storage_id', $request->storage_id)
            ->where('sequence', $request->sequence)
            ->first();

        if ($check_sequence) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'sequence'  => ['sequence already exists'],
                ]
            ], 422);
        }

        $save = StockDetermination::create([
            'plant_id' => $request->plant_id,
            'storage_id' => $request->storage_id,
            'sequence' => $request->sequence ? $request->sequence : 0,
            'supply_plant_id' => $request->supply_plant_id,
            'supply_storage_id' => $request->supply_storage_id,
			'ext_purchase' => $request->ext_purchase ? $request->ext_purchase : 0,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 401);
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['stock-determination-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return StockDetermination::with([
            'plant', 'storage', 'supply_plant', 'supply_storage', 'createdBy', 'updatedBy'
        ])->findOrFail($id);
    }

    public function update($id, Request $request)
    {
        Auth::user()->cekRoleModules(['stock-determination-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $stockDetermination = StockDetermination::findOrFail($id);

        $this->validate(request(), [
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            'sequence' => 'nullable|numeric|min:0|digits_between:1,4',
            // 'supply_plant_id' => 'nullable',
            // 'supply_storage_id' => 'nullable',
			'ext_purchase' => 'nullable|boolean',
        ]);

        if ($request->ext_purchase == "0" || $request->ext_purchase == null) {
        	$this->validate(request(), [
        		'supply_plant_id' => 'required|exists:plants,id',
            	'supply_storage_id' => 'required|exists:storages,id',
            ]);
        }

        if ($request->storage_id == $request->supply_storage_id) {
        	return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['Storage & Storage Supply Cant Same']
                ]
            ], 422);
        }

        $check = StockDetermination::where('ext_purchase', $request->ext_purchase)
    		->where('storage_id', $request->storage_id)
    		->where('supply_storage_id', $request->supply_storage_id)
    		->where('id', '<>', $id)
    		->first();

        if ($check) {
        	return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['Data Already Exists'],
                ]
            ], 422);
        }

        $check_sequence = StockDetermination::where('ext_purchase', $request->ext_purchase)
            ->where('storage_id', $request->storage_id)
            ->where('sequence', $request->sequence)
            ->where('id', '<>', $id)
            ->first();

        if ($check_sequence) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'sequence'  => ['sequence already exists'],
                ]
            ], 422);
        }

        $save = $stockDetermination->update([
            'plant_id' => $request->plant_id,
            'storage_id' => $request->storage_id,
            'sequence' => $request->sequence ? $request->sequence : 0,
            'supply_plant_id' => $request->supply_plant_id,
            'supply_storage_id' => $request->supply_storage_id,
			'ext_purchase' => $request->ext_purchase ? $request->ext_purchase : 0,
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $stockDetermination;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function delete($id)
    {
        Auth::user()->cekRoleModules(['stock-determination-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = StockDetermination::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['stock-determination-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:stock_determinations,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = StockDetermination::findOrFail($ids)->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function getStockDeterminationOld(Request $request)
    {
        $stock = StockDetermination::with(['supply_plant', 'supply_storage'])
            ->where('plant_id', $request->plant_id)
            ->where('storage_id', $request->storage_id)
            ->orderBy('sequence', 'asc')
            ->paginate(20);

        return $stock;
    }

    /*
    |--------------------------------------------------------------------------
    | List Pilihan Supply Plant & Storage untuk reservasi & PR
    |--------------------------------------------------------------------------
    |
    | 1. dari Supplying Storage MRP material plant requester. jika tdk ada next
    | 2. ambil plant supply dari stock determination, lalu ambil Supplying Storage MRP material plant tersebut. jika tdk ada next
    | 3. dari stock determination sequence terendah
    */

    public function getStockDetermination(Request $request)
    {
        $material_storage = MaterialPlant::where('plant_id', $request->plant_id)
            ->where('material_id', $request->material_id)
            ->first();

        // ambil dari supplying storage mrp
        if ($material_storage) {
            $storage = MasterStorage::find($material_storage->production_storage_id);

            $plant_id = $storage ? $storage->plant_id : 0;

            $plant = Plant::find($plant_id);

            if ($storage && $plant) {
                $new = [
                    'supply_plant_id' => $plant->id,
                    'supply_plant' => $plant,
                    'supply_storage_id' => $storage->id,
                    'supply_storage' => $storage
                ];
            }
        }

        $stock = StockDetermination::with(['supply_plant', 'supply_storage'])
            ->where('plant_id', $request->plant_id)
            ->where('storage_id', $request->storage_id)
            ->orderBy('sequence', 'asc')
            ->get();

        if (isset($new)) {
            $storage_list = collect($stock)->pluck('supply_storage_id')->toArray();
            if (in_array($new['supply_storage_id'], $storage_list)) {
                $data = collect($stock);
                return (new Collection($data))->paginate(20);
            } else {
                $data = collect($stock)->prepend($new);
                return (new Collection($data))->paginate(20);
            }
        } else {
            // jika tidak ada supplying storage mrp
            // ambil plant dari stock determination
            $supply_plant_stock_deter = $stock->pluck('supply_plant_id')->unique();
            
            // cek mrp plant2 tsb yg memiliki supply storage
            $material_storage_supply_stock_deter = MaterialPlant::whereIn('plant_id', $supply_plant_stock_deter)
                ->where('material_id', $request->material_id)
                ->whereNotNull('production_storage_id')
                ->first();

            // ambil dari supplying storage mrp
            if ($material_storage_supply_stock_deter) {
                $storage_stck = MasterStorage::find($material_storage_supply_stock_deter->production_storage_id);

                $plant_id_stck = $storage_stck ? $storage_stck->plant_id : 0;

                $plant_stck = Plant::find($plant_id_stck);

                if ($storage_stck && $plant_stck) {
                    $new2 = [
                        'supply_plant_id' => $plant_stck->id,
                        'supply_plant' => $plant_stck,
                        'supply_storage_id' => $storage_stck->id,
                        'supply_storage' => $storage_stck
                    ];
                }
            }

            if (isset($new2)) {
                $storage_list2 = collect($stock)->pluck('supply_storage_id')->toArray();
                if (in_array($new2['supply_storage_id'], $storage_list2)) {
                    $data = collect($stock);
                    return (new Collection($data))->paginate(20);
                } else {
                    $data = collect($stock)->prepend($new2);
                    return (new Collection($data))->paginate(20);
                }
            }

            return (new Collection($stock))->paginate(20);
        }
    }

    public function getSenderPlantStockDetermination(Request $request)
    {
        $material_storage = MaterialPlant::where('plant_id', $request->plant_id)
            ->where('material_id', $request->material_id)
            ->first();

        // ambil dari supplying storage mrp
        if ($material_storage) {
            $storage = MasterStorage::find($material_storage->production_storage_id);

            $plant_id = $storage ? $storage->plant_id : 0;

            $plant = Plant::find($plant_id);

            if ($storage && $plant) {
                $new = [
                    'supply_plant_id' => $plant->id
                ];
            }
        }

        $stock = StockDetermination::with(['supply_plant', 'supply_storage'])
            ->where('plant_id', $request->plant_id)
            ->where('storage_id', $request->storage_id)
            ->get();

        if (!$stock) {
            return Plant::whereNull('id')->paginate(20)->appends(Input::except('page'));
        }

        $plant = Plant::whereIn('id', $stock->pluck('supply_plant_id'))->get();

        if (isset($new)) {
            $plant_list = collect($plant)->pluck('id')->toArray();
            if (in_array($new['supply_plant_id'], $plant_list)) {
                $data = collect($plant);
                return (new Collection($data))->paginate(20);
            } else {
                $def_plant = Plant::find($new['supply_plant_id']);
                $data = collect($plant)->prepend($def_plant);
                return (new Collection($data))->paginate(20);
            }
        } else {
            // jika tidak ada supplying storage mrp
            // ambil plant dari stock determination
            $supply_plant_stock_deter = $stock->pluck('supply_plant_id')->unique();
            
            // cek mrp plant2 tsb yg memiliki supply storage
            $material_storage_supply_stock_deter = MaterialPlant::whereIn('plant_id', $supply_plant_stock_deter)
                ->where('material_id', $request->material_id)
                ->whereNotNull('production_storage_id')
                ->first();

            // ambil dari supplying storage mrp
            if ($material_storage_supply_stock_deter) {
                $storage_stck = MasterStorage::find($material_storage_supply_stock_deter->production_storage_id);

                $plant_id_stck = $storage_stck ? $storage_stck->plant_id : 0;

                $plant_stck = Plant::find($plant_id_stck);

                if ($storage_stck && $plant_stck) {
                    $new2 = [
                        'supply_plant_id' => $plant_stck->id
                    ];
                }
            }

            if (isset($new2)) {
                $plant_list = collect($plant)->pluck('id')->toArray();
                if (in_array($new2['supply_plant_id'], $plant_list)) {
                    $data = collect($plant);
                    return (new Collection($data))->paginate(20);
                } else {
                    $def_plant = Plant::find($new2['supply_plant_id']);
                    $data = collect($plant)->prepend($def_plant);
                    return (new Collection($data))->paginate(20);
                }
            }

            return (new Collection($plant))->paginate(20);
        }
    }

    public function getSenderStorageStockDetermination(Request $request)
    {
        $material_storage = MaterialPlant::where('plant_id', $request->plant_id)
            ->where('material_id', $request->material_id)
            ->first();

        // ambil dari supplying storage mrp
        if ($material_storage) {
            $storage = MasterStorage::find($material_storage->production_storage_id);

            $plant_id = $storage ? $storage->plant_id : 0;

            $plant = Plant::find($plant_id);

            if ($storage && $plant) {
                $new = [
                    'supply_storage_id' => $storage->id
                ];
            }
        }

        $stock = StockDetermination::with(['supply_plant', 'supply_storage'])
            ->where('plant_id', $request->plant_id)
            ->where('storage_id', $request->storage_id)
            ->where('supply_plant_id', $request->supply_plant_id)
            ->get();

        if (!$stock) {
            return MasterStorage::whereNull('id')->paginate(20)->appends(Input::except('page'));
        }

        $storage = MasterStorage::with('location')
            ->whereIn('id', $stock->pluck('supply_storage_id'))
            ->get();

        if (isset($new)) {
            $storage_list = collect($storage)->pluck('id')->toArray();
            if (in_array($new['supply_storage_id'], $storage_list)) {
                $data = collect($storage);
                return (new Collection($data))->paginate(20);
            } else {
                $def_storage = MasterStorage::find($new['supply_storage_id']);
                $data = collect($storage)->prepend($def_storage);
                return (new Collection($data))->paginate(20);
            }
        } else {
            // jika tidak ada supplying storage mrp
            // ambil plant dari stock determination
            $supply_plant_stock_deter = $stock->pluck('supply_plant_id')->unique();
            
            // cek mrp plant2 tsb yg memiliki supply storage
            $material_storage_supply_stock_deter = MaterialPlant::whereIn('plant_id', $supply_plant_stock_deter)
                ->where('material_id', $request->material_id)
                ->whereNotNull('production_storage_id')
                ->first();

            // ambil dari supplying storage mrp
            if ($material_storage_supply_stock_deter) {
                $storage_stck = MasterStorage::find($material_storage_supply_stock_deter->production_storage_id);

                $plant_id_stck = $storage_stck ? $storage_stck->plant_id : 0;

                $plant_stck = Plant::find($plant_id_stck);

                if ($storage_stck && $plant_stck) {
                    $new2 = [
                        'supply_storage_id' => $storage_stck->id
                    ];
                }
            }

            if (isset($new2)) {
                $storage_list = collect($storage)->pluck('id')->toArray();
                if (in_array($new2['supply_storage_id'], $storage_list)) {
                    $data = collect($storage);
                    return (new Collection($data))->paginate(20);
                } else {
                    $def_storage = MasterStorage::find($new2['supply_storage_id']);
                    $data = collect($storage)->prepend($def_storage);
                    return (new Collection($data))->paginate(20);
                }
            }
        
            return (new Collection($storage))->paginate(20);
        }
    }

    public function getReceiverPlantStockDetermination(Request $request)
    {
        $stock = StockDetermination::where('supply_plant_id', $request->plant_id)
            ->where('supply_storage_id', $request->storage_id)
            ->pluck('plant_id');

        $plant = Plant::whereIn('id', $stock)->paginate(20)->appends(Input::except('page'));
        
        return $plant;
    }

    public function getReceiverStorageStockDetermination(Request $request)
    {
        $stock = StockDetermination::where('supply_plant_id', $request->plant_id)
            ->where('supply_storage_id', $request->storage_id)
            ->where('plant_id', $request->plant_id_to)
            ->pluck('storage_id');

        $storage = MasterStorage::whereIn('id', $stock)->paginate(20)->appends(Input::except('page'));
        
        return $storage;
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        return Excel::download(new StockDeterminationTemplateExport, 'stock_determination.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new StockDeterminationImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        $data = StockDeterminationFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        $data = StockDeterminationFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new StockDeterminationInvalidExport($data), 'invalid_stock_determination_upload.xlsx');

        // delete failed upload data
        StockDeterminationFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['stock-determination-create']);

        $data = StockDeterminationFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        StockDeterminationFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
