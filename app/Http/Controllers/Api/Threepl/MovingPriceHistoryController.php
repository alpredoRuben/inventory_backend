<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Models\MovingPriceHistory;

class MovingPriceHistoryController extends Controller
{
    public function index()
    {
        $moving = (new MovingPriceHistory)->newQuery();

        $moving->with(['plant', 'material', 'movement_history']);

        $moving->with('movement_history.createdBy');

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $moving->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $moving->orderBy('id', 'desc');
        }

        $moving = $moving->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $moving;
    }
}
