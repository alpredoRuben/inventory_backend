<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\MaterialStorage;
use App\Models\Reservation;
use App\Models\DeliveryHeader;
use App\Models\Material;

class StockController extends Controller
{
    public function index(Request $request)
    {
        Auth::user()->cekRoleModules(['stock-overview']);

        $stock = (new StockMain)->newQuery();

        $stock->with([
            'material', 'plant', 'storage', 'batch_stock', 'createdBy', 'updatedBy'
        ]);

        $stock->with('material.base_uom');

        $stock->with('material.images');

        $stock->with('batch_stock.vendor');

        // only not deleted storage
        $stock->whereHas('storage', function ($q) {
            $q->where('deleted', false);
        });

        // only not deleted material
        $stock->whereHas('material', function ($q) {
            $q->where('deleted', false);
        });

        // filter
        if (request()->has('plant')) {
            $stock->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('storage')) {
            $stock->whereIn('storage_id', request()->input('storage'));
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $stock->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $stock->whereIn('storage_id', $storage_id);
        }

        if (request()->has('material')) {
            $stock->whereIn('material_id', request()->input('material'));
        }

        if (request()->has('batch')) {
            $batch = strtolower(request()->input('batch'));
            $stock->whereHas('batch_stock', function ($q) use ($batch){
                $q->where(DB::raw("LOWER(stock_batches.batch_number)"), 'LIKE', "%".$batch."%");
            });
        }

        if (request()->has('on_hand_only')) {
            if (request()->input('on_hand_only')) {
                $stock->where('qty_unrestricted', '>', 0);                
            }
        }

        if (request()->has('sku_code')) {
            $sku = strtolower(request()->input('sku_code'));
            $stock->whereHas('material', function($q) use ($sku){
                $q->where('sku_code', $sku);
            });
        }

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $stock->whereHas('material', function ($data) use ($q){
                $data->where(DB::raw("LOWER(materials.code)"), 'LIKE', "%".$q."%");
                $data->orWhere(DB::raw("LOWER(materials.description)"), 'LIKE', "%".$q."%");
                $data->orWhere(DB::raw("LOWER(materials.sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
                    $stock->with(['material' => function ($data) use ($sort_order){
                        $data->orderBy('materials.code', $sort_order);
                    }]);
                break;

                default:
                    $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
                    $stock->orderBy(request()->input('sort_field'), $sort_order);
            }
        } else {
            $stock->orderBy('id', 'desc');
        }

        $stock = $stock->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($stock['data'] as $k => $v) {
            $material_storage = MaterialStorage::where('material_id', $v['material_id'])
                ->where('storage_id', $v['storage_id'])
                ->first();

            // filter requirement date
            if (request()->has('requirement_date')) {
                // get reservation in processed
                $on_order = Reservation::whereIn('status', [2,5])
                    ->where('document_type', 'R')
                    ->whereNull('fulfillment_status')
                    ->where('material_id', $v['material_id'])
                    ->where('inout_storage_id', $v['storage_id'])
                    ->whereBetween('requirement_date', [request()->requirement_date[0], request()->requirement_date[1]])
                    ->get();
            } else {
                // get reservation in processed
                $on_order = Reservation::whereIn('status', [2,5])
                    ->where('document_type', 'R')
                    ->whereNull('fulfillment_status')
                    ->where('material_id', $v['material_id'])
                    ->where('inout_storage_id', $v['storage_id'])
                    ->get();
            }

            if (count($on_order) > 0) {
                $do_reservation = 0;
                foreach ($on_order as $key => $value) {
                    $res = Reservation::find($value->id);
                    $sent_do_id = DeliveryHeader::whereIn('status', [2,3])->pluck('id');

                    $do_reservation += $res->delivery_orders
                        ->whereIn('delivery_header_id', $sent_do_id)
                        ->sum('delivery_qty');
                }
                $qty_on_order = (($on_order->sum('quantity_base_unit')) - $do_reservation);
            } else {
                $qty_on_order = 0;
            }

            // get total amount from batch unit cost * qty onhand
            if (count($v['batch_stock']) > 0) {
                $total_amount = 0;
                foreach ($v['batch_stock'] as $batch_stock) {
                    $total_amount += $batch_stock['qty_unrestricted'] * $batch_stock['unit_cost'];
                }
            } else {
                $total_amount = 0;
            }

            // sortage
            $material_min_stock = $material_storage ? $material_storage->min_stock : 0;

            $ceksortage = $v['qty_unrestricted'] - (int)$material_min_stock - $qty_on_order;
            if ($ceksortage < 0){
                $sortage = true;
            } else {
                $sortage = false;
            }

            $stock['data'][$k] = $v;
            $stock['data'][$k]['material_storage'] = $material_storage;
            $stock['data'][$k]['qty_on_order'] = $qty_on_order;
            $stock['data'][$k]['total_amount'] = $total_amount;
            $stock['data'][$k]['ceksortage'] = $ceksortage;
            $stock['data'][$k]['sortage'] = $sortage;
        }

        if (request()->has('sortage')) {
            if (request()->sortage == 1) {
                foreach($stock['data'] as $k => $v) {                
                    if (!$stock['data'][$k]['sortage']){
                        unset($stock['data'][$k]);
                    }
                }
            }
        }

        $stock['data'] = array_values($stock['data']);

        return $stock;
    }

    public function serialStockMain($id)
    {
        Auth::user()->cekRoleModules(['stock-overview']);

        $main = StockMain::find($id);

        $serial = (new StockSerial)->newQuery();

        $serial->where('material_id', $main->material_id);

        $serial->where('storage_id', $main->storage_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $serial->where(DB::raw("LOWER(serial)"), 'LIKE', "%".$q."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $serial->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $serial->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $serial->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $serial->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }

        return $serial;
    }

    public function serialStockBatch($id)
    {
        Auth::user()->cekRoleModules(['stock-overview']);

        $batch = StockBatch::find($id);

        $main = $batch->stock_main;

        $serial = StockSerial::with([
            'plant', 'storage', 'material', 'vendor', 'customer', 'createdBy', 'updatedBy'
        ])
        ->where('material_id', $main->material_id)
        ->where('storage_id', $main->storage_id)
        ->where('batch_number', $batch->batch_number)
        ->get();

        return $serial;
    }

    public function materialStock(Request $request)
    {
        Auth::user()->cekRoleModules(['material-view']);

        if (!$request->sku_code) {
            return response()->json([
                'message' => 'Material Not found'
            ], 404);
        }

        $material = Material::with(['images'])
            ->where('sku_code', $request->sku_code)
            ->orWhere('code', $request->sku_code)
            ->where('deleted', false)
            ->first();

        if (!$material) {
            return response()->json([
                'message' => 'Material Not found'
            ], 404);
        }

        $stock = StockMain::where('storage_id', $request->storage_id)
            ->where('material_id', $material->id)
            ->where('deleted', false)
            ->first();

        if (!$stock) {
            return response()->json([
                'message' => 'Material Not found'
            ], 404);
        }

        return $material;
    }

    public function serialStockByStorageMaterial(Request $request)
    {
        $serial = (new StockSerial)->newQuery();

        $serial->where('material_id', (int)$request->material_id);

        $serial->where('storage_id', (int)$request->storage_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $serial->where(DB::raw("LOWER(serial)"), 'LIKE', "%".$q."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $serial->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $serial->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $serial->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $serial->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }

        return $serial;
    }
}
