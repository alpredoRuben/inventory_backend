<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\MovementType;
use App\Models\MovementTypeReason;
use App\Models\Uom;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\WorkCenter;
use App\Models\CostCenter;
use App\Models\StockDetermination;
use App\Models\Material;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\MaterialAccount;
use App\Models\Supplier;
use App\Models\TransactionCode;

class ReferenceListController extends Controller
{
    public function documentCode()
    {
        return TransactionCode::orderBy('id', 'desc')->paginate(20);
    }

    public function materialGm()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $mat_id = MaterialPlant::where('plant_id', request()->input('plant_id'))->pluck('material_id');

        $material = (new Material)->newQuery();

        $material->has('plants');
        
        $material->has('base_uom');

        $material->with(['base_uom', 'images', 'group_material', 'material_parameters']);
        $material->with('material_parameters.classification_parameter');

        $material->where('deleted', false);

        $material->whereIn('id', $mat_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('group_material_id')) {
            $material->whereIn('group_material_id', request()->input('group_material_id'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $material->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $material->orderBy('id', 'desc');
        }

        $material = $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $plant = (int)(request()->input('plant_id'));
        $storage = (int)(request()->input('storage_id'));
        $material->transform(function($data) use ($plant, $storage){
            $mrp = MaterialPlant::where('material_id', $data->id)
                ->where('plant_id', $plant)
                ->first();

            $data->material_plant = $mrp;

            // get material price from stock
            $stock = StockMain::where('material_id', $data->id)
                ->where('storage_id', $storage)
                ->first();

            // get price from info record
            $info_record = MaterialVendor::where('material_id', $data->id)
                    ->where('storage_id', $storage)
                    ->first();

            if ($stock) {//get price from stock
                $batch = StockBatch::where('stock_main_id', $stock->id)
                    ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                    ->orderBy('id', 'asc')
                    ->first();

                if ($batch) {
                    $unit_cost = $batch->unit_cost;
                } else {
                    $unit_cost = 0;
                }
            } else if ($info_record) {//get price from info record
                if ($info_record) {
                    $unit_cost = $info_record->unit_price;
                } else {
                    $unit_cost = 0;
                }
            } else {//get price from material account
                $mat_account = MaterialAccount::where('material_id', $data->id)
                    ->where('plant_id', $plant)
                    ->first();

                if ($mat_account) {
                    // cek material account type
                    if ($mat_account->price_controll) {
                        $unit_cost = $mat_account->standart_price;
                    } else {
                        $unit_cost = $mat_account->moving_price;
                    }
                } else {
                    $unit_cost = 0;
                }
            }

            $data->price = $unit_cost ? $unit_cost : 0;

            return $data;
        });

        return $material;
    }

    public function materialReservation()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $plant = (int)(request()->input('plant_id'));
        $storage = (int)(request()->input('storage_id'));

        $supply_plant = stockDetermination::where('plant_id', $plant)
            ->where('storage_id', $storage)
            ->pluck('supply_plant_id');

        $mat_id = MaterialPlant::whereIn('plant_id', $supply_plant)->pluck('material_id');

        $material = (new Material)->newQuery();

        $material->with(['base_uom', 'images', 'group_material', 'material_parameters']);
        $material->with('material_parameters.classification_parameter');

        $material->where('deleted', false);

        $material->whereIn('id', $mat_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('group_material_id')) {
            $material->whereIn('group_material_id', request()->input('group_material_id'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $material->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $material->orderBy('id', 'desc');
        }

        $material = $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $material->transform(function($data) use ($plant, $storage){
            $mrp = MaterialPlant::where('material_id', $data->id)
                ->where('plant_id', $plant)
                ->first();

            $data->material_plant = $mrp;

            // get material price from stock
            $stock = StockMain::where('material_id', $data->id)
                ->where('storage_id', $storage)
                ->first();

            // get price from info record
            $info_record = MaterialVendor::where('material_id', $data->id)
                    ->where('storage_id', $storage)
                    ->first();

            if ($stock) {//get price from stock
                $batch = StockBatch::where('stock_main_id', $stock->id)
                    ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                    ->orderBy('id', 'asc')
                    ->first();

                if ($batch) {
                    $unit_cost = $batch->unit_cost;
                } else {
                    $unit_cost = 0;
                }
            } else if ($info_record) {//get price from info record
                if ($info_record) {
                    $unit_cost = $info_record->unit_price;
                } else {
                    $unit_cost = 0;
                }
            } else {//get price from material account
                $mat_account = MaterialAccount::where('material_id', $data->id)
                    ->where('plant_id', $plant)
                    ->first();

                if ($mat_account) {
                    // cek material account type
                    if ($mat_account->price_controll) {
                        $unit_cost = $mat_account->standart_price;
                    } else {
                        $unit_cost = $mat_account->moving_price;
                    }
                } else {
                    $unit_cost = 0;
                }
            }

            $data->price = $unit_cost ? $unit_cost : 0;

            return $data;
        });

        return $material;
    }

    public function materialCatalog()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $plant = (int)(request()->input('plant_id'));
        $storage = (int)(request()->input('storage_id'));

        $supply_plant = stockDetermination::where('plant_id', $plant)
            ->where('storage_id', $storage)
            ->pluck('supply_plant_id');

        $mat_id = MaterialPlant::whereIn('plant_id', $supply_plant)
            ->whereNotNull('production_storage_id')
            ->get();

        $material = (new MaterialPlant)->newQuery();

        $material->whereIn('plant_id', $supply_plant);
        $material->whereNotNull('production_storage_id');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->whereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('group_material_id')) {
            $material->whereHas('material', function($data){
                $data->whereIn('group_material_id', request()->input('group_material_id'));
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $material->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $material->orderBy('material_id', 'asc');
        }

        $material = $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $material->transform(function ($new) {
            $mrp = MaterialPlant::where('material_id', $new->material_id)
                ->where('plant_id', $new->plant_id)
                ->whereNotNull('production_storage_id')
                ->first();

            // get material price from stock
            $stock = StockMain::where('material_id', $new->material_id)
                ->where('storage_id', $mrp->production_storage_id)
                ->first();

            // get price from info record
            $info_record = MaterialVendor::where('material_id', $new->material_id)
                    ->where('storage_id', $mrp->production_storage_id)
                    ->first();

            if ($stock) {//get price from stock
                $batch = StockBatch::where('stock_main_id', $stock->id)
                    ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                    ->orderBy('id', 'asc')
                    ->first();

                if ($batch) {
                    $unit_cost = $batch->unit_cost;
                } else {
                    $unit_cost = 0;
                }
            } else if ($info_record) {//get price from info record
                if ($info_record) {
                    $unit_cost = $info_record->unit_price;
                } else {
                    $unit_cost = 0;
                }
            } else {//get price from material account
                $mat_account = MaterialAccount::where('material_id', $new->material_id)
                    ->where('plant_id', $new->plant_id)
                    ->first();

                if ($mat_account) {
                    // cek material account type
                    if ($mat_account->price_controll) {
                        $unit_cost = $mat_account->standart_price;
                    } else {
                        $unit_cost = $mat_account->moving_price;
                    }
                } else {
                    $unit_cost = 0;
                }
            }

            $price = $unit_cost ? $unit_cost : 0;

            $data = Material::with(['base_uom', 'images', 'group_material', 'material_parameters'])
                ->with('material_parameters.classification_parameter')
                ->find($new->material_id);

            $data->price = $price;
            $data->price_batch = isset($batch) ? $batch : null;

            $data->material_plant = $mrp;

            $storage_supply = MasterStorage::find($mrp->production_storage_id);
            $data->storage_supply = $storage_supply;
            $data->plant_supply = Plant::find($storage_supply->plant_id);

            return $data;
        });

        return $material;
    }

    public function materialSku(Request $request)
    {
        Auth::user()->cekRoleModules(['material-view']);

        if (!$request->sku_code) {
            return response()->json([
                'message' => 'Material Not found'
            ], 404);
        }

        $material = Material::with(['images'])
            ->where('sku_code', $request->sku_code)
            ->orWhere('code', $request->sku_code)
            ->where('deleted', false)
            ->first();

        if (!$material) {
            return response()->json([
                'message' => 'Material Not found'
            ], 404);
        }

        $material->material_plant = MaterialPlant::where('material_id', $material->id)
            ->where('plant_id', $request->plant_id)
            ->first();

        return $material;
    }

    public function materialbyCode($code)
    {
        $material = Material::with(['base_uom'])->where('code', $code)->first();
        if (!$material) {
            return response()->json([
                'message' => 'Data not found',
            ], 404);
        }

        return $material;
    }

    public function costCenter()
    {
        Auth::user()->cekRoleModules(['cost-center-view']);

        $costCenter = (new CostCenter)->newQuery();

        $costCenter->where('deleted', false)->with(['company', 'createdBy', 'updatedBy']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $costCenter->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $costCenter->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $costCenter->orderBy('code', 'asc');
        }

        // from user detail
        $user_detail = Auth::user()->detail;

        if ($user_detail) {
            $cost_user_detail = $user_detail->cost_center_id;

            $costCenter->where('id', $cost_user_detail);
        } else {
            // if have organization parameter
            $cost_id = Auth::user()->roleOrgParam(['cost_center']);

            if (count($cost_id) > 0) {
                $costCenter->whereIn('id', $cost_id);
            }
        }

        if (request()->has('per_page')) {
            return $costCenter->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $costCenter->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function vendorList($material_id)
    {
        Auth::user()->cekRoleModules(['supplier-view']);

        // vendor for free material
        if ($material_id == 0) {
            $supplier = (new Supplier)->newQuery();
            $supplier->where('type', '<>', 3)->where('deleted', false);

            if (request()->has('q')) {
                $q = strtolower(request()->input('q'));
                $supplier->where(function($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                });
            }

            $supplier->orderBy('id', 'asc');

            $supplier = $supplier->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
                ->appends(Input::except('page'));

            $supplier->transform(function ($data) {
                $new = [
                    'id' => $data->id,
                    'code' => $data->code,
                    'type' => $data->type,
                    'description' => $data->description,
                    'address' => $data->location->address,
                    'currency' => null,
                    'unit_price' => 0
                ];

                return $new;
            });

            return $supplier;
        } else {

            $info_record = (new MaterialVendor)->newQuery();

            $info_record->where('material_id', $material_id);

            $info_record->where('deleted', false);

            $info_record->with('vendor');

            if (request()->has('q')) {
                $q = strtolower(request()->input('q'));

                // search vendor
                $info_record->whereHas('vendor', function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                })
                ->with(['vendor' => function ($query) use ($q) {
                    $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                    $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                }]);
            };

            $info_record = $info_record->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
                ->appends(Input::except('page'));

            $info_record->transform(function ($data) {
                $new = [
                    'id' => $data->vendor_id,
                    'code' => $data->vendor->code,
                    'type' => $data->vendor->type,
                    'description' => $data->vendor->description,
                    'address' => $data->vendor->location->address,
                    'currency' => $data->currency,
                    'unit_price' => (float)$data->unit_price
                ];

                return $new;
            });

            return $info_record;
        }
    }

    public function getSpecificMovementType()
    {
        Auth::user()->cekRoleModules(['movement-type-view']);
  
        $movementType = (new MovementType)->newQuery();

        $movementType->where('deleted', false)->with(['rev_code', 'createdBy', 'updatedBy']);

        $movementType->whereIn('code', ['561', '201', '311', '605', '101', '131', '241']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $movementType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $movementType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $movementType->orderBy('code', 'asc');
        }

        // if have organization parameter
        $movement_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($movement_id) > 0) {
            $movementType->whereIn('id', $movement_id);
        }

        if (request()->has('per_page')) {
            return $movementType->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $movementType->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function getMovementTypeReason($id)
    {
        Auth::user()->cekRoleModules(['movement-type-view']);
  
        $reason = (new MovementTypeReason)->newQuery();

        $reason->where('deleted', false);

        $reason->where('movement_type_id', (int)$id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $reason->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(reason)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('per_page')) {
            return $reason->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $reason->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function workCenterByStorage($id)
    {
        Auth::user()->cekRoleModules(['work-center-view']);

        $work = (new WorkCenter)->newQuery();

        $work->where('storage_supply_id', $id);

        $work->where('deleted', false);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $work->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $work->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $work->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $work->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $work->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function costCenterByWorkCenter($id)
    {
        Auth::user()->cekRoleModules(['cost-center-view']);

        $workCenter = WorkCenter::findOrFail((int)$id);

        $costCenter = (new CostCenter)->newQuery();

        $costCenter->where('deleted', false);

        $costCenter->where('id', $workCenter->cost_center_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $costCenter->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $costCenter->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $costCenter->orderBy('id', 'desc');
        }

        // if have organization parameter
        $cost_id = Auth::user()->roleOrgParam(['cost_center']);

        if (count($cost_id) > 0) {
            $costCenter->whereIn('id', $cost_id);
        }

        if (request()->has('per_page')) {
            return $costCenter->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $costCenter->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function uomList($id)
    {
        Auth::user()->cekRoleModules(['uom-view']);

        // get material
        $material = Material::findOrFail((int)$id);

        // get base uom in material
        $base = $material->uom_id;

        // get uom conversion in material
        $conversion = $material->uom_conversion->pluck('id');

        // merge base & conversion id
        $uom_id = $conversion->push($base);
 
        $uom = (new Uom)->newQuery();

        $uom->whereIn('id', $uom_id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $uom->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $uom->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $uom->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $uom->where('deleted', false)->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $uom->where('deleted', false)->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }

    public function uop($material_id, $plant_id)
    {
        Auth::user()->cekRoleModules(['uom-view']);

        $id_mat = (int)$material_id;
        $id_plant = (int)$plant_id;

        // get material
        $material = Material::findOrFail($id_mat);

        $material_plant = MaterialPlant::where('material_id', $id_mat)
            ->where('plant_id', $id_plant)
            ->first();

        return $uop_id = $material_plant ? Uom::find($material_plant->unit_of_purchase_id) : null;

        // // get base uom in material
        // $base = Uom::find($material->uom_id);

        // // get uom conversion in material
        // $conversion = $material->uom_conversion->pluck('id');

        // $uom_conv = Uom::whereIn('id', $conversion)->get();
        
        // if ($uop_id) {
        //     if ($base == $uop_id) {
        //         // merge base & conversion id
        //         $uom = $uom_conv->prepend($base)->unique();
        //     } else {
        //         // merge base & conversion id
        //         $uom = $uom_conv->prepend($base)->prepend($uop_id)->unique();
        //     }
        // } else {
        //     $uom = $uom_conv->prepend($base)->unique();
        // }

        // return $uom;
    }
}
