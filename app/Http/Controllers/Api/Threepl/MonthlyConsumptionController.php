<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MonthlyConsumptionExport;

use App\Helpers\Collection;
use App\Models\MovementHistory;
use App\Models\MovementType;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;

class MonthlyConsumptionController extends Controller
{
    public function index()
    {
        Auth::user()->cekRoleModules(['monthly-consumption-view']);
  
        $history = (new MovementHistory)->newQuery();

        $history->where('item_no', '<>', null)
        ->with([
            'movement_type', 'plant', 'storage',
        ]);

        // filter
        if (request()->has('plant')) {
            $history->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('storage')) {
            $history->whereIn('storage_id', request()->input('storage'));
        }

        if (request()->has('movement_type')) {
            $history->whereIn('movement_type_id', request()->input('movement_type'));
        }

        // if have organization parameter
        $mvt_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($mvt_id) > 0) {
            $history->whereIn('movement_type_id', $mvt_id);
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $history->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $history->whereIn('storage_id', $storage_id);
        }

        if (request()->has('period') && request()->input('period') != '') {
            $period = str_replace('"', '', request()->input('period'));
            $month = Carbon::parse($period)->format('m');
            $year = Carbon::parse($period)->format('Y');

            $history->whereMonth('created_at', $month);
            $history->whereYear('created_at', $year);

            $filter_period = Carbon::parse($period)->format('F-Y');
        // } else {
        //     $period = explode("-", date('Y-m'));
        //     $month = $period[1];
        //     $year = $period[0];
        //     $history->whereMonth('created_at', $month);
        //     $history->whereYear('created_at', $year);

        //     $filter_period = \DateTime::createFromFormat('Y-n', date('Y-m'))->format('F-Y');
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $history->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $history->orderBy('material_doc', 'desc')->orderBy('item_no');
        }

        $data = $history->get();

        $period = isset($filter_period) ? $filter_period : null;

        $map_data = $data->map(function ($item) {
            if ($item->de_ce == 'c') {
                $x = -1;
            } else {
                $x = 1;
            }

            $item->total_amount = ((int)$item->amount * (float)$item->quantity) * $x;

            return $item;
        })
        ->groupBy('storage_id')
        ->values()
        ->transform(function ($data) use ($period) {
            $new = [
                'plant' => $data[0]['plant'],
                'storage' => $data[0]['storage'],
                'period' => $period,
                'currency' => $data[0]['currency'],
                'amount' => collect($data)->sum('total_amount')
            ];

            return $new;
        });

        return (new Collection($map_data))->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));;
    }

    public function excel()
    {
        Auth::user()->cekRoleModules(['monthly-consumption-download']);
  
        $history = (new MovementHistory)->newQuery();

        $history->where('item_no', '<>', null)
        ->with([
            'movement_type', 'plant', 'storage',
        ]);

        // filter
        if (request()->has('plant')) {
            $history->whereIn('plant_id', request()->input('plant'));
            $filter_plant = Plant::whereIn('id', request()->input('plant'))->pluck('code');
        }

        if (request()->has('storage')) {
            $history->whereIn('storage_id', request()->input('storage'));
            $filer_storage = MasterStorage::whereIn('id', request()->input('storage'))->pluck('code');
        }

        if (request()->has('movement_type')) {
            $history->whereIn('movement_type_id', request()->input('movement_type'));
            $filter_movement_type = MovementType::whereIn('id', request()->input('movement_type'))->pluck('code');
        }

        // if have organization parameter
        $mvt_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($mvt_id) > 0) {
            $history->whereIn('movement_type_id', $mvt_id);
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $history->whereIn('plant_id', $plant_id);
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $history->whereIn('storage_id', $storage_id);
        }

        if (request()->has('period') && request()->input('period') != '') {
            $period = str_replace('"', '', request()->input('period'));
            $month = Carbon::parse($period)->format('m');
            $year = Carbon::parse($period)->format('Y');

            $history->whereMonth('created_at', $month);
            $history->whereYear('created_at', $year);

            $filter_period = Carbon::parse($period)->format('F-Y');
        // } else {
        //     $period = explode("-", date('Y-m'));
        //     $month = $period[1];
        //     $year = $period[0];
        //     $history->whereMonth('created_at', $month);
        //     $history->whereYear('created_at', $year);

        //     $filter_period = \DateTime::createFromFormat('Y-n', date('Y-m'))->format('F-Y');
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $history->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $history->orderBy('material_doc', 'desc')->orderBy('item_no');
        }

        $data = $history->get();

        $period = isset($filter_period) ? $filter_period : null;

        $map_data = $data->map(function ($item) {
            if ($item->de_ce == 'c') {
                $x = -1;
            } else {
                $x = 1;
            }

            $item->total_amount = ((int)$item->amount * (float)$item->quantity) * $x;

            return $item;
        })
        ->groupBy('storage_id')
        ->values()
        ->transform(function ($data) use ($period) {
            $new = [
                'plant' => $data[0]['plant'],
                'storage' => $data[0]['storage'],
                'period' => $period,
                'currency' => $data[0]['currency'],
                'amount' => collect($data)->sum('total_amount')
            ];

            return $new;
        });

        $filter = [
            'plant' => isset($filter_plant) ? $filter_plant : [],
            'storage' => isset($filer_storage) ? $filer_storage : [],
            'movement_type' => isset($filter_movement_type) ? $filter_movement_type : [],
            'period' => isset($filter_period) ? $filter_period : null
        ];

        return Excel::download(new MonthlyConsumptionExport($map_data, $filter), 'report_monthly_consumption.xlsx');
    }

    public function getSpecificMovementType()
    {
        Auth::user()->cekRoleModules(['movement-type-view']);
  
        $movementType = (new MovementType)->newQuery();

        $movementType->where('deleted', false)->with(['rev_code', 'createdBy', 'updatedBy']);

        $movementType->whereIn('code', ['202', '201']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $movementType->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $movementType->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $movementType->orderBy('code', 'asc');
        }

        // if have organization parameter
        $movement_id = Auth::user()->roleOrgParam(['movement_type']);

        if (count($movement_id) > 0) {
            $movementType->whereIn('id', $movement_id);
        }

        if (request()->has('per_page')) {
            return $movementType->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $movementType->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
    }
}
