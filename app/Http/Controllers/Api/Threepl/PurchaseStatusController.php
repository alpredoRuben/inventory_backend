<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Material;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\PurchaseApproval;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\PurchaseAttachment;
use App\Models\PurchaseCondition;
use App\Models\PurchaseDocumentHistory;

class PurchaseStatusController extends Controller
{
    public function index()
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-view']);

        $data = (new PurchaseItem)->newQuery();

        $data->with([
            'gl_account', 'material', 'group_material', 'plant', 'storage', 'cost_center'
        ]);

        // only get PO
        $data->whereHas('purchase_header', function ($d){
            $d->where('po_doc_category', 'B');
        });

        // filter
        // if (request()->has('status')) {
        //     $data->whereIn('status', request()->input('status'));
        // }

        if (request()->has('plant_id')) {
            $data->whereIn('plant_id', request()->input('plant_id'));
        }

        if (request()->has('storage_id')) {
            $data->whereIn('storage_id', request()->input('storage_id'));
        }

        if (request()->has('material_id')) {
            $data->whereIn('material_id', request()->input('material_id'));
        }

        if (request()->has('group_material_id')) {
            $data->whereIn('group_material_id', request()->input('group_material_id'));
        }

        if (request()->has('gl_account_id')) {
            $data->whereIn('gl_account_id', request()->input('gl_account_id'));
        }

        if (request()->has('cost_center_id')) {
            $data->whereIn('cost_center_id', request()->input('cost_center_id'));
        }

        if (request()->has('po_date')) {
            $from = str_replace('"', '', request()->po_date[0]);
            $to = str_replace('"', '', request()->po_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            // relasi purchase_header
            $data->whereHas('purchase_header', function($v) use ($t, $f) {
                $v->where('purc_doc_date', '>=', $f.' 00:00:00');
                $v->where('purc_doc_date', '<=', $t.' 00:00:00');
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $data->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $data->orderBy('id', 'desc');
        }

        $data = $data->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $data->transform(function($data){
            // get header po
            $header = $data->purchase_header;

            $data->po_date = $header->purc_doc_date;
            $data->po_number = $header->po_doc_no;
            $data->order_amount = ((int)$data->price_unit + (int)$data->tax_item) * (float)$data->order_qty;

            // get data gr po item
            $gr = $data->purchase_document_history->where('transaction', 1)->values();
            // get data invoice po item
            $invoice = $data->purchase_document_history->where('transaction', 2)->values();

            // get amount gr po item
            $gr_amount = 0;
            foreach ($gr as $g){
                $gr_amount += $g->quantity * $g->amount;
            }

            // get amount invoice po item
            $invoice_amount = 0;
            foreach ($invoice as $iv){
                $invoice_amount += $iv->quantity * $iv->amount;
            }

            // get gr qty
            $gr_qty = $gr->sum('quantity');

            $data->gr = $gr;
            $data->gr_qty = $gr_qty;
            $data->gr_amount = $gr_amount;
            $data->invoice = $invoice;
            $data->invoice_amount = $invoice_amount;

            // get status po
            if (count($gr) == 0) {
                $status = 'Open';
            } else if ($data->order_qty > $gr_qty) {
                $status = 'Partial GR';
            } else if ($gr_amount == $invoice_amount) {
                $status = 'Invoiced';
            } else if ($data->order_qty == $gr_qty && $data->completed) {
                $status = 'GR Completed';
            } else if (count($gr) == 0 && $data->completed) {
                $status = 'Canceled';
            }

            $data->status = $status;

            return $data;
        });
        
        return $data;
    }
}
