<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\StockOpname;
use App\Models\StockOpnameSerial;
use App\Models\StockOpnameApproval;
use App\Models\MovementHistory;
use App\Models\SerialHistory;

class StockOpnameController extends Controller
{
    public function getMaterialDoc()
	{
        Auth::user()->cekRoleModules(['stock-opname-create']);

		// get inspection code 
        $code = TransactionCode::where('code', 'M')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'M';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
	}

    public function index()
    {
        Auth::user()->cekRoleModules(['stock-opname-view']);

        $so = (new StockOpname)->newQuery();

        $so->where('deleted', false)->with([
            'plant', 'storage', 'updatedBy', 'approval'
        ]);

        $so->with('approval.user');

        // filter plant
        if (request()->has('plant')) {
            $so->whereIn('plant_id', request()->input('plant'));
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $so->whereIn('plant_id', $plant_id);
        }

        // filter storage
        if (request()->has('storage')) {
            $so->whereIn('storage_id', request()->input('storage'));
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $so->whereIn('storage_id', $storage_id);
        }

        // filter document date
        if (request()->has('document_date')) {
            $so->whereBetween('document_date', [request()->document_date[0], request()->document_date[1]]);
        }

        // filter status
        if (request()->has('status')) {
            $so->whereIn('status', request()->input('status'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $so->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $so->orderBy('id', 'desc');
        }

        $so = $so->get()->unique('stock_opname_doc')->values();

        $data = (new Collection($so))
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $data;
    }

    public function detail($stock_opname_doc)
    {
        Auth::user()->cekRoleModules(['stock-opname-view']);

        $header = StockOpname::with(['Storage', 'plant'])
            ->where('stock_opname_doc', $stock_opname_doc)
            ->first();

        if (!$header) {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }

        $item = StockOpname::with(['serial', 'material', 'stock_batch', 'entry_uom'])
            ->where('stock_opname_doc', $stock_opname_doc)
            ->get();

        $item->map(function($data){
            if ($data->status == 1) {
                $stock = StockMain::where('storage_id', $data->storage_id)
                    ->where('material_id', $data->material_id)
                    ->first();

                $data->stock_on_hand = $stock->qty_unrestricted;

                $material = Material::find($data->material_id);
                $data->uom_stock = Uom::find($material->uom_id);

                return $data;
            }
        });

        $header->item = $item;

        return $header;
    }

	public function list(Request $request)
	{
		Auth::user()->cekRoleModules(['stock-opname-view']);

        $draft = (new StockOpname)->newQuery();

        $draft->with(['serial', 'material', 'plant', 'storage', 'stock_batch', 'entry_uom']);
        $draft->with('material.images');

        $draft->where('deleted', false);

        $draft->where('storage_id', $request->storage_id);
        $draft->where('status', 0);//open status
        $draft->where('created_by', Auth::user()->id);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $draft->whereHas('material', function ($data) use ($q){
                $data->where(DB::raw("LOWER(materials.code)"), 'LIKE', "%".$q."%");
                $data->orWhere(DB::raw("LOWER(materials.description)"), 'LIKE', "%".$q."%");
                $data->orWhere(DB::raw("LOWER(materials.sku_code)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('per_page')) {
            return $draft->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $draft->paginate(appsetting('PAGINATION_DEFAULT'))->appends(Input::except('page'));
        }
	}

    public function draft(Request $request)
    {
        Auth::user()->cekRoleModules(['stock-opname-view']);

        $draft = StockOpname::with(['serial', 'material', 'plant', 'storage', 'stock_batch', 'entry_uom'])
            ->with('material.images')
            ->where('storage_id', $request->storage_id)
            ->where('status', 0)//open status
            ->where('created_by', Auth::user()->id)
            ->first();

        if ($draft) {
            return response()->json([
                'stock_opname_doc' => $draft->stock_opname_doc
            ], 200);
        } else {
            return response()->json([
                'stock_opname_doc' => NULL
            ], 200);
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['stock-opname-view']);

        $stock_opname = StockOpname::with(['serial', 'material', 'plant', 'storage', 'stock_batch', 'entry_uom'])
            ->with('material.images')
            ->find($id);

        if (!$stock_opname) {
            return response()->json([
                'message' => 'Stock Opname not found'
            ], 404);
        }

        return $stock_opname;
    }

	public function create(Request $request)
	{
		Auth::user()->cekRoleModules(['stock-opname-create']);

		$this->validate(request(), [
            'stock_opname_doc' => 'nullable',
            'material_id' => 'required|exists:materials,id',
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            // 'stock_batch_id' => 'nullable|exists:stock_batches,id',
            // 'quantity' => 'required|numeric',
            // 'base_uom_id' => 'required|exists:uoms,id',
            'zero_count' => 'nullable|boolean',
            'quantity_entry' => 'required|numeric',
            'entry_uom_id' => 'required|exists:uoms,id',
        ]);

        // get material
        $mat = Material::find($request->material_id);

        // validate serial
        if ($mat->serial_profile) {
            $this->validate(request(), [
                'serial' => 'required|array',
                'serial.*' => 'exists:stock_serials,serial',
            ]);

            // validate unique serial
            $dups = [];
            foreach (array_count_values($request->serial) as $valk => $c) {
                if($c > 1) $dups[] = $valk;
            }

            if ($dups) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'serial'  => ['a Serial cant entry more than once']
                    ]
                ], 422);
            }

            // validate total serial
            $total_serial = count($request->serial);
            if ($request->quantity_entry != $total_serial) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'serial'  => ['Total Serial not match with total quantity']
                    ]
                ], 422);
            }
        }

        // validate material plant batch creation indicator
        $material_plant = MaterialPlant::where('plant_id', $request->plant_id)
            ->where('material_id', $request->material_id)
            ->first();

        if (!$request->stock_opname_doc) {
            $stock_opname_code = $this->getMaterialDoc();
            $item_number = 1;
        } else {
            $stock_opname_code = $request->stock_opname_doc;
        }

        // validate duplicate material
        $StockOpname = StockOpname::where('storage_id', $request->storage_id)
            ->where('material_id', $request->material_id)
            ->whereIn('status', [0,1])
            ->first();

        $item_number = StockOpname::where('stock_opname_doc', $stock_opname_code)->count() + 1;

        if ($StockOpname) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'material_id'  => ['Material '.$mat->code.' already exists in doc stock opname '.$StockOpname->stock_opname_doc.'']
                ]
            ], 422);
        }

        // cek batch indicator in material plant
        if ($material_plant->batch_ind) {
            $this->validate(request(), [
                'stock_batch_id' => 'required|exists:stock_batches,id'
            ]);

            // validate duplicate batch
            $StockOpnameBatch = StockOpname::where('storage_id', $request->storage_id)
                ->where('material_id', $request->material_id)
                ->where('stock_batch_id', $request->stock_batch_id)
                ->whereIn('status', [0,1])
                ->first();

            $sb = StockBatch::find($request->stock_batch_id);

            if ($StockOpnameBatch) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'stock_batch_id'  => ['Batch '.$sb->batch_number.' already exists in doc stock opname '.$StockOpnameBatch->stock_opname_doc.'']
                    ]
                ], 422);
            }
        }

        // validate duplicate serial
        if ($mat->serial_profile) {
            $so_not_approved = StockOpname::whereIn('status', [0,1])->pluck('id');
            $StockOpnameSerial = StockOpnameSerial::whereIn('serial', $request->serial)
                ->whereIn('stock_opname_id', $so_not_approved)
                ->first();

            if ($StockOpnameSerial) {
                $sh = $StockOpnameSerial->stock_opname;
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'material_id'  => ['Serial '.$StockOpnameSerial->serial.' already exists in doc stock opname '.$sh->stock_opname_doc.'']
                    ]
                ], 422);
            }
        }

        // insert
        try {
            DB::beginTransaction();

            // get uom entry
            $unit_entry = Uom::find($request->entry_uom_id);

            // convert uom
            if ($mat->uom_id == $request->entry_uom_id) {
                $quantity = $request->quantity_entry;
            } else {
                $quantity = uomconversion($request->material_id, $request->entry_uom_id, $request->quantity_entry);

                if (!$quantity) {
                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                }
            }

            // create stock opname
            $stock_opname = StockOpname::create([
                'stock_opname_doc' => $stock_opname_code,
                'item_number' => $item_number,
                'type' => 1,
                'document_date' => now(),
                'material_id' => $request->material_id,
                'plant_id' => $request->plant_id,
                'storage_id' => $request->storage_id,
                'stock_batch_id' => isset($request->stock_batch_id) ? $request->stock_batch_id : null,
                'zero_count' => isset($request->zero_count) ? $request->zero_count : 0,
                'quantity' => $quantity,
                'base_uom_id' => $mat->uom_id,
                'quantity_entry' => $request->quantity_entry,
                'entry_uom_id' => $request->entry_uom_id,
                'status' => 0,//open
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id,
                'deleted' => false
            ]);

            // insert stock opname serial if material serialize
            if ($mat->serial_profile) {
                $sequence_serial = StockOpnameSerial::where('stock_opname_id', $stock_opname->id)->count() + 1;
                foreach ($request->serial as $serial) {
                    $stock_opname_serial = StockOpnameSerial::create([
                        'stock_opname_id' => $stock_opname->id,
                        'sequence' => $sequence_serial++,
                        'serial' => $serial
                    ]);
                }
            }

            // update transaction code if create new doc
            if (!$request->stock_opname_doc) {
                $code = TransactionCode::where('code', 'M')->first();
                $code->update(['lastcode' => $stock_opname_code]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success save Stock Opname'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while processing Stock Opname',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
	}

    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['stock-opname-update']);

        $stock_opname = StockOpname::find($id);

        if (!$stock_opname) {
            return response()->json([
                'message' => 'Stock Opname not found'
            ], 404);
        }

        if ($stock_opname->status != 0) {
            return response()->json([
                'message' => 'Stock Opname status not Open'
            ], 422);
        }

        $item_number = StockOpname::where('stock_opname_doc', $stock_opname->stock_opname_doc)
            ->where('id', '<>', $id)
            ->count() + 1;

        $this->validate(request(), [
            'material_id' => 'required|exists:materials,id',
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            // 'stock_batch_id' => 'nullable|exists:stock_batches,id',
            // 'quantity' => 'required|numeric',
            // 'base_uom_id' => 'required|exists:uoms,id',
            'zero_count' => 'nullable|boolean',
            'quantity_entry' => 'required|numeric',
            'entry_uom_id' => 'required|exists:uoms,id',
        ]);

        // get material
        $mat = Material::find($request->material_id);

        // validate serial
        if ($mat->serial_profile) {
            $this->validate(request(), [
                'serial' => 'required|array',
                'serial.*' => 'exists:stock_serials,serial',
            ]);

            // validate unique serial
            $dups = [];
            foreach (array_count_values($request->serial) as $valk => $c) {
                if($c > 1) $dups[] = $valk;
            }

            if ($dups) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'serial'  => ['a Serial cant entry more than once']
                    ]
                ], 422);
            }

            // validate total serial
            $total_serial = count($request->serial);
            if ($request->quantity_entry != $total_serial) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'serial'  => ['Total Serial not match with total quantity']
                    ]
                ], 422);
            }
        }

        // validate material plant batch creation indicator
        $material_plant = MaterialPlant::where('plant_id', $request->plant_id)
            ->where('material_id', $request->material_id)
            ->first();

        // cek batch indicator in material plant
        if ($material_plant->batch_ind) {
            $this->validate(request(), [
                'stock_batch_id' => 'required|exists:stock_batches,id'
            ]);
        }

        // insert
        try {
            DB::beginTransaction();

            // get uom entry
            $unit_entry = Uom::find($request->entry_uom_id);

            // convert uom
            if ($mat->uom_id == $request->entry_uom_id) {
                $quantity = $request->quantity_entry;
            } else {
                $quantity = uomconversion($request->material_id, $request->entry_uom_id, $request->quantity_entry);

                if (!$quantity) {
                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                }
            }

            // create stock opname
            $stock_opname->update([
                'item_number' => $item_number++,
                'type' => 1,
                'document_date' => now(),
                'material_id' => $request->material_id,
                'plant_id' => $request->plant_id,
                'storage_id' => $request->storage_id,
                'stock_batch_id' => isset($request->stock_batch_id) ? $request->stock_batch_id : null,
                'zero_count' => isset($request->zero_count) ? $request->zero_count : 0,
                'quantity' => $quantity,
                'base_uom_id' => $mat->uom_id,
                'quantity_entry' => $request->quantity_entry,
                'entry_uom_id' => $request->entry_uom_id,
                'status' => 0,//open
                'updated_by' => Auth::user()->id,
            ]);

            // insert stock opname serial if material serialize
            if ($mat->serial_profile) {
                StockOpnameSerial::where('stock_opname_id', $id)->delete();

                $sequence_serial = StockOpnameSerial::where('stock_opname_id', $id)->count() + 1;

                foreach ($request->serial as $serial) {
                    $stock_opname_serial = StockOpnameSerial::create([
                        'stock_opname_id' => $id,
                        'sequence' => $sequence_serial++,
                        'serial' => $serial
                    ]);
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'Success update Stock Opname'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while update Stock Opname',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function delete()
    {
        Auth::user()->cekRoleModules(['stock-opname-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:stock_opnames,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $id) {
                $stock_opname = StockOpname::find($id);

                $stock_opname_serial = StockOpnameSerial::where('stock_opname_id', $id)->delete();

                $stock_opname->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete Stock Opname'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while delete Stock Opname',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function submit($stock_opname_doc, Request $request)
    {
        Auth::user()->cekRoleModules(['stock-opname-submit']);

        $this->validate(request(), [
            'notes'          => 'nullable|string|max:191'
        ]);

        $cek_data = StockOpname::where('stock_opname_doc', $stock_opname_doc)->first();

        if (!$cek_data) {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }

        $data = StockOpname::where('stock_opname_doc', $stock_opname_doc)->get();

        try {
            DB::beginTransaction();

            /** Status
            * 0 = open
            * 1 = submit
            * 2 = approved
            * 3 = rejected
            **/

            foreach ($data as $key => $value) {
                $so = StockOpname::find($value->id);

                if ($so->status != 0) {
                    throw new \Exception('Stock opname status not open');
                }

                // record approval
                StockOpnameApproval::create([
                    'stock_opname_id' => $so->id,
                    'user_id' => Auth::user()->id,
                    'status_from' => 0, // open
                    'status_to' => 1,// submit
                    'notes' => $request->notes
                ]);

                $so->update([
                        'status' => 1,//submit
                        'updated_by' => Auth::user()->id
                    ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success submit Stock Opname'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while submit Stock Opname',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function approve($stock_opname_doc, Request $request)
    {
        Auth::user()->cekRoleModules(['stock-opname-approve']);

        $this->validate(request(), [
            'notes'         => 'nullable|string|max:191',
            'status'        => 'required|numeric|min:2|max:3'
        ]);

        $cek_data = StockOpname::where('stock_opname_doc', $stock_opname_doc)->first();

        if (!$cek_data) {
            return response()->json([
                'message' => 'data not found'
            ], 404);
        }

        $data = StockOpname::where('stock_opname_doc', $stock_opname_doc)->get();

        try {
            DB::beginTransaction();

            /** Status
            * 0 = open
            * 1 = submit
            * 2 = approved
            * 3 = rejected
            **/

            $i = 1;
            foreach ($data as $key => $value) {
                $so = StockOpname::find($value->id);

                // cek so status
                if ($so->status == 2) {
                    throw new \Exception('Stock opname already approved');
                }

                if ($so->status == 3) {
                    throw new \Exception('Stock opname already rejected');
                }

                if ($so->status == 0) {
                    throw new \Exception('Stock opname must submited first');
                }

                // update stock only if request approved
                if ($request->status == 2) {
                    // get stock
                    $stock = StockMain::where('material_id', $so->material_id)
                        ->where('storage_id', $so->storage_id)
                        ->first();

                    // handle zero count
                    if ($so->zero_count) {
                        // record to movement 701
                        $mvt = MovementType::where('code', 701)->first();

                        // if stock have batch
                        if (count($stock->batch_stock) > 0) {
                            // get total batch stock as stock unres
                            $batch_unres = $stock->batch_stock->sum('qty_unrestricted');
                            
                            // get batch stock used by stock opname
                            $stock_batch_opname = StockBatch::find($so->stock_batch_id);

                            // get total stock (total batch - batch used by opname)
                            $total_batch_unres = $batch_unres - $stock_batch_opname->qty_unrestricted;

                            // posting selisih ke movement history
                            $selisih = $stock->qty_unrestricted - $total_batch_unres;
                        } else {
                            // posting selisih ke movement history
                            $selisih = $stock->qty_unrestricted - 0;

                            $total_batch_unres = 0;
                        }

                        if ($so->stock_batch_id) {
                            $stock_batch = StockBatch::find($so->stock_batch_id);
                        }

                        $movement = MovementHistory::create([
                            'material_doc' => $stock_opname_doc,
                            'doc_year' => date('Y'),
                            'movement_type_id' => $mvt->id,
                            'de_ce' => $mvt->dc_ind,
                            'movement_type_reason_id' => null,
                            'plant_id' => $so->plant_id,
                            'storage_id' => $so->storage_id,
                            'posting_date' => now(),
                            'document_date' => now(),
                            'material_slip' => null,
                            'doc_header_text' => null,
                            'work_center_id' => null,
                            'cost_center_id' => null,

                            'item_no' => $i++,
                            'material_id' => $so->material_id,
                            'quantity_entry' => $selisih,
                            'entry_uom_id' => $so->base_uom_id,

                            // convert qty
                            'quantity' => $selisih,
                            'base_uom_id' => $so->base_uom_id,

                            'batch_no' => isset($stock_batch) ?  $stock_batch->batch_number : null,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // record if serialize
                        $material = Material::find($so->material_id);
                        if ($material->serial_profile) {
                            $batch_opname = StockBatch::find($so->stock_batch_id);
                            if ($batch_opname) {
                                $serial_lama = StockSerial::where('material_id', $so->material_id)
                                    ->where('storage_id', $so->storage_id)
                                    ->where('batch_number', $batch_opname->batch_number)
                                    ->delete();
                            } else {
                                $serial_lama = StockSerial::where('material_id', $so->material_id)
                                    ->where('storage_id', $so->storage_id)
                                    ->delete();
                            }

                            foreach($so->serial as $serial){
                                // record serial history
                                SerialHistory::create([
                                    'movement_history_id' => $movement->id,
                                    'plant_id' => $movement->plant_id,
                                    'storage_id' => $movement->storage_id,
                                    'material_id' => $movement->material_id,
                                    'serial' => $serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }

                        // update stock
                        $stock->update([
                            'qty_unrestricted' => $total_batch_unres,
                            'updated_by' => Auth::user()->id
                        ]);

                        //update stock batch if batch
                        if ($so->stock_batch_id) {
                            $stock_batch = StockBatch::find($so->stock_batch_id)
                                ->update([
                                    'qty_unrestricted' => 0,
                                    'updated_by' => Auth::user()->id
                                ]);
                        }
                    } else {
                        // if batch stock
                        if (count($stock->batch_stock) > 0) {
                            // get batch stock used by stock opname
                            $stock_batch_opname = StockBatch::find($so->stock_batch_id);

                            if ($so->quantity < $stock_batch_opname->qty_unrestricted) {
                                // record to movement 701
                                $mvt = MovementType::where('code', 701)->first();

                                // get total batch stock
                                $batch_unres = $stock->batch_stock->sum('qty_unrestricted');//10

                                $batch_unres_min_opname = $batch_unres - $stock_batch_opname->qty_unrestricted;//5

                                // posting selisih ke movement history
                                $selisih = $stock_batch_opname->qty_unrestricted - $so->quantity;//1

                                // get total stock (total batch - batch used by opname)
                                $total_stock_unres = $batch_unres - $selisih;//9

                                $total_batch_unres = $stock_batch_opname->qty_unrestricted - $selisih;//4

                                $movement = MovementHistory::create([
                                    'material_doc' => $stock_opname_doc,
                                    'doc_year' => date('Y'),
                                    'movement_type_id' => $mvt->id,
                                    'de_ce' => $mvt->dc_ind,
                                    'movement_type_reason_id' => null,
                                    'plant_id' => $so->plant_id,
                                    'storage_id' => $so->storage_id,
                                    'posting_date' => now(),
                                    'document_date' => now(),
                                    'material_slip' => null,
                                    'doc_header_text' => null,
                                    'work_center_id' => null,
                                    'cost_center_id' => null,

                                    'item_no' => $i++,
                                    'material_id' => $so->material_id,
                                    'quantity_entry' => $selisih,
                                    'entry_uom_id' => $so->base_uom_id,

                                    // convert qty
                                    'quantity' => $selisih,
                                    'base_uom_id' => $so->base_uom_id,

                                    'batch_no' => isset($stock_batch_opname) ?  $stock_batch_opname->batch_number : null,

                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // record if serialize
                                $material = Material::find($so->material_id);
                                if ($material->serial_profile) {
                                    // get all serial by material by storage
                                    $serial_lama = StockSerial::where('material_id', $so->material_id)
                                        ->where('storage_id', $so->storage_id)
                                        ->where('batch_number', $stock_batch_opname->batch_number)
                                        ->delete();

                                    foreach($so->serial as $serial){
                                        // insert into stock serial table
                                        StockSerial::create([
                                            'material_id' => $so->material_id,
                                            'plant_id' => $so->plant_id,
                                            'storage_id' => $so->storage_id,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            'serial' => $serial->serial,
                                            'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                            'batch_number' => $stock_batch_opname->batch_number, //get batch no from so
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            'vendor_id' => null,
                                            'customer_id' => null, // will be used in 601
                                        ]);

                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $movement->id,
                                            'plant_id' => $movement->plant_id,
                                            'storage_id' => $movement->storage_id,
                                            'material_id' => $movement->material_id,
                                            'serial' => $serial->serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }

                                // update stock
                                $stock->update([
                                    'qty_unrestricted' => $total_stock_unres,
                                    'updated_by' => Auth::user()->id
                                ]);

                                //update stock batch if batch
                                $stock_batch_opname->update([
                                    'qty_unrestricted' => $total_batch_unres,
                                    'updated_by' => Auth::user()->id
                                ]);

                            } elseif ($so->quantity > $stock_batch_opname->qty_unrestricted) {
                                // record to movement 702
                                $mvt = MovementType::where('code', 702)->first();

                                // get total batch stock
                                $batch_unres = $stock->batch_stock->sum('qty_unrestricted');//10

                                $batch_unres_min_opname = $batch_unres - $stock_batch_opname->qty_unrestricted;//5

                                // posting selisih ke movement history
                                $selisih = $so->quantity - $stock_batch_opname->qty_unrestricted;//1

                                // get total stock (total batch - batch used by opname)
                                $total_stock_unres = $batch_unres + $selisih;//11

                                $total_batch_unres = $stock_batch_opname->qty_unrestricted + $selisih;//6

                                $movement = MovementHistory::create([
                                    'material_doc' => $stock_opname_doc,
                                    'doc_year' => date('Y'),
                                    'movement_type_id' => $mvt->id,
                                    'de_ce' => $mvt->dc_ind,
                                    'movement_type_reason_id' => null,
                                    'plant_id' => $so->plant_id,
                                    'storage_id' => $so->storage_id,
                                    'posting_date' => now(),
                                    'document_date' => now(),
                                    'material_slip' => null,
                                    'doc_header_text' => null,
                                    'work_center_id' => null,
                                    'cost_center_id' => null,

                                    'item_no' => $i++,
                                    'material_id' => $so->material_id,
                                    'quantity_entry' => $selisih,
                                    'entry_uom_id' => $so->base_uom_id,

                                    // convert qty
                                    'quantity' => $selisih,
                                    'base_uom_id' => $so->base_uom_id,

                                    'batch_no' => isset($stock_batch_opname) ?  $stock_batch_opname->batch_number : null,

                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // record if serialize
                                $material = Material::find($so->material_id);
                                if ($material->serial_profile) {
                                    // get all serial by material by storage
                                    $serial_lama = StockSerial::where('material_id', $so->material_id)
                                        ->where('storage_id', $so->storage_id)
                                        ->where('batch_number', $stock_batch_opname->batch_number)
                                        ->delete();

                                    foreach($so->serial as $serial){
                                        // insert into stock serial table
                                        StockSerial::create([
                                            'material_id' => $so->material_id,
                                            'plant_id' => $so->plant_id,
                                            'storage_id' => $so->storage_id,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            'serial' => $serial->serial,
                                            'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                            'batch_number' => $stock_batch_opname->batch_number, //get batch no from so
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            'vendor_id' => null,
                                            'customer_id' => null, // will be used in 601
                                        ]);

                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $movement->id,
                                            'plant_id' => $movement->plant_id,
                                            'storage_id' => $movement->storage_id,
                                            'material_id' => $movement->material_id,
                                            'serial' => $serial->serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }

                                // update stock
                                $stock->update([
                                    'qty_unrestricted' => $total_stock_unres,
                                    'updated_by' => Auth::user()->id
                                ]);

                                //update stock batch if batch
                                $stock_batch_opname->update([
                                    'qty_unrestricted' => $total_batch_unres,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        } else {
                            if ($so->quantity < $stock->qty_unrestricted) {
                                // record to movement 701
                                $mvt = MovementType::where('code', 701)->first();
                                
                                // posting selisih ke movement history
                                $selisih = $stock->qty_unrestricted - $so->quantity;

                                $movement = MovementHistory::create([
                                    'material_doc' => $stock_opname_doc,
                                    'doc_year' => date('Y'),
                                    'movement_type_id' => $mvt->id,
                                    'de_ce' => $mvt->dc_ind,
                                    'movement_type_reason_id' => null,
                                    'plant_id' => $so->plant_id,
                                    'storage_id' => $so->storage_id,
                                    'posting_date' => now(),
                                    'document_date' => now(),
                                    'material_slip' => null,
                                    'doc_header_text' => null,
                                    'work_center_id' => null,
                                    'cost_center_id' => null,

                                    'item_no' => $i++,
                                    'material_id' => $so->material_id,
                                    'quantity_entry' => $selisih,
                                    'entry_uom_id' => $so->base_uom_id,

                                    // convert qty
                                    'quantity' => $selisih,
                                    'base_uom_id' => $so->base_uom_id,

                                    'batch_no' => null,

                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // record if serialize
                                $material = Material::find($so->material_id);
                                if ($material->serial_profile) {
                                    // get all serial by material by storage
                                    $serial_lama = StockSerial::where('material_id', $so->material_id)
                                        ->where('storage_id', $so->storage_id)
                                        ->delete();

                                    foreach($so->serial as $serial){
                                        // insert into stock serial table
                                        StockSerial::create([
                                            'material_id' => $so->material_id,
                                            'plant_id' => $so->plant_id,
                                            'storage_id' => $so->storage_id,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            'serial' => $serial->serial,
                                            'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                            'batch_number' => null, //get batch no from so
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            'vendor_id' => null,
                                            'customer_id' => null, // will be used in 601
                                        ]);

                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $movement->id,
                                            'plant_id' => $movement->plant_id,
                                            'storage_id' => $movement->storage_id,
                                            'material_id' => $movement->material_id,
                                            'serial' => $serial->serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }

                                // update stock
                                $stock->update([
                                    'qty_unrestricted' => $so->quantity,
                                    'updated_by' => Auth::user()->id
                                ]);
                            } elseif ($so->quantity > $stock->qty_unrestricted) {
                                // record to movement 702
                                $mvt = MovementType::where('code', 702)->first();

                                // posting selisih ke movement history
                                $selisih = $so->quantity - $stock->qty_unrestricted;

                                $movement = MovementHistory::create([
                                    'material_doc' => $stock_opname_doc,
                                    'doc_year' => date('Y'),
                                    'movement_type_id' => $mvt->id,
                                    'de_ce' => $mvt->dc_ind,
                                    'movement_type_reason_id' => null,
                                    'plant_id' => $so->plant_id,
                                    'storage_id' => $so->storage_id,
                                    'posting_date' => now(),
                                    'document_date' => now(),
                                    'material_slip' => null,
                                    'doc_header_text' => null,
                                    'work_center_id' => null,
                                    'cost_center_id' => null,

                                    'item_no' => $i++,
                                    'material_id' => $so->material_id,
                                    'quantity_entry' => $selisih,
                                    'entry_uom_id' => $so->base_uom_id,

                                    // convert qty
                                    'quantity' => $selisih,
                                    'base_uom_id' => $so->base_uom_id,

                                    'batch_no' => null,

                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // record if serialize
                                $material = Material::find($so->material_id);
                                if ($material->serial_profile) {
                                    // get all serial by material by storage
                                    $serial_lama = StockSerial::where('material_id', $so->material_id)
                                        ->where('storage_id', $so->storage_id)
                                        ->delete();

                                    foreach($so->serial as $serial){
                                        // insert into stock serial table
                                        StockSerial::create([
                                            'material_id' => $so->material_id,
                                            'plant_id' => $so->plant_id,
                                            'storage_id' => $so->storage_id,
                                            'stock_type' => 1,//1=unrest,2=in-transit,3=quality,4=blocked
                                            'serial' => $serial->serial,
                                            'status' => 2,//1=AVLB,2=ESTO,3=EQUI
                                            'batch_number' => null, //get batch no from so
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id,
                                            'vendor_id' => null,
                                            'customer_id' => null, // will be used in 601
                                        ]);

                                        // record serial history
                                        SerialHistory::create([
                                            'movement_history_id' => $movement->id,
                                            'plant_id' => $movement->plant_id,
                                            'storage_id' => $movement->storage_id,
                                            'material_id' => $movement->material_id,
                                            'serial' => $serial->serial,
                                            'created_by' => Auth::user()->id,
                                            'updated_by' => Auth::user()->id
                                        ]);
                                    }
                                }

                                // update stock
                                $stock->update([
                                    'qty_unrestricted' => $so->quantity,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }
                }

                // record approval
                StockOpnameApproval::create([
                    'stock_opname_id' => $so->id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $so->status,
                    'status_to' => $request->status,
                    'notes' => $request->notes
                ]);

                // update so status
                $so->update([
                    'status' => $request->status, //approves
                    'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success approve Stock Opname'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while approve Stock Opname',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
