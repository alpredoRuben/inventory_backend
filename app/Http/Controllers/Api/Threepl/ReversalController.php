<?php

namespace App\Http\Controllers\Api\Threepl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\TransactionCode;
use App\Models\MovementHistory;
use App\Models\MovementType;
use App\Models\MovementTypeReason;
use App\Models\Material;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\Serial;
use App\Models\SerialHistory;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\StockSerial;
use App\Models\WorkCenter;
use App\Models\CostCenter;
use App\Models\Uom;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\DeliveryHeader;
use App\Models\DeliveryItem;
use App\Models\DeliverySerial;
use App\Models\DeliveryApproval;
use App\Models\Reservation;

class ReversalController extends Controller
{
    public function getMaterialDoc()
	{
        Auth::user()->cekRoleModules(['goods-movement-create']);

		// get reservation code 
        $code = TransactionCode::where('code', 'M')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'M';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'M';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
	}

    public function getBatchNumber()
    {
        Auth::user()->cekRoleModules(['goods-movement-create']);

        // get reservation code 
        $code = TransactionCode::where('code', 'BN')->first();

        // if exist
        if ($code->lastcode) {
            $increment = $code->lastcode + 1;
            $lastcode = sprintf("%010d", $increment);
        } else {//generate if not exist
            $lastcode = '0000000001';
        }

        return $lastcode;
    }

    public function reverse(Request $request, $material_doc)
    {
        // get all movement history
    	$main_movement = MovementHistory::where('material_doc', $material_doc)->get();

        // get first movement history
        $first_movement = MovementHistory::where('material_doc', $material_doc)->first();

        // cek history movement
        if (!$first_movement) {
            return response()->json([
                'message' => 'history movement not found',
            ], 404);
        }

        // find movement type used in movement history
        $movement_type = MovementType::find($first_movement->movement_type_id);

        // cek movement type
        if (!$movement_type) {
            return response()->json([
                'message' => 'movement type not found',
            ], 404);
        }

        // cek reversal movement
        if (!$movement_type->rev_code_id) {
            return response()->json([
                'message' => 'movement type not have reversal',
            ], 404);
        }

        // if have organization parameter
        $org_movement_type = Auth::user()->roleOrgParam(['movement_type']);

        if (!in_array(0, $org_movement_type) | !in_array(null, $org_movement_type)) {
            if (count($org_movement_type) > 0) {
                if (!in_array($movement_type->rev_code_id, $org_movement_type)) {
                    abort(403, 'Movement Type '. MovementType::find($movement_type->rev_code_id)->code.' In Organization Parameter');
                }
            }
        }

        $cekreversal = MovementHistory::where('material_slip', $material_doc)->first();

        if ($cekreversal) {
            return response()->json([
                'message' => 'document '.$material_doc.' already reverse. cant reverse again'
            ], 422);
        }

        // get new material document
        $new_doc = $this->getMaterialDoc();

        /*serial status
        *1 AVLB = Setelah GI, Atau nanti bs aja masukin data serial sebelum jd stock
        *2 ESTO = kalau ada isinya plant/storage
        *3 EQUI = kalau jadi asset status
        */

        /* serial stock type
        *1=unrest
        *2=in-transit
        *3=quality
        *4=blocked
        */

        try {
            DB::beginTransaction();
            
            $movement_type_code = $movement_type->code;
            switch ($movement_type_code) {
                case 561:
                    $reversal_code = $movement_type->rev_code_id;

                    $reversal_mvt = MovementType::find($movement_type->rev_code_id);

                    $item = 1;

                    foreach ($main_movement as $mvmnt) {
                        // get stock
                        $cekstock = StockMain::where('material_id', $mvmnt->material_id)
                            ->where('storage_id', $mvmnt->storage_id)
                            ->first();

                        // cek stock
                        if (!$cekstock) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Devisit stock');
                        }

                        // cek stock qty
                        if ($cekstock->qty_unrestricted < $mvmnt->quantity) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                        }

                        //cek stock batch qty
                        $stock_batch = StockBatch::where('batch_number', $mvmnt->batch_no)->first();

                        if (!$stock_batch) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Devisit stock');
                        }

                        if ($stock_batch->qty_unrestricted < $mvmnt->quantity) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                        }

                        // get material
                        $material = Material::find($mvmnt->material_id);

                        // cek material
                        if (!$material) {
                            throw new \Exception('material not found');
                        }

                        // cek serial
                        if ($material->serial_profile) {
                            foreach ($mvmnt->serial as $serial) {
                                $serials = StockSerial::where('serial', $serial->serial)
                                    ->where('material_id', $mvmnt->material_id)
                                    ->where('storage_id', $mvmnt->storage_id)
                                    ->where('batch_number', $mvmnt->batch_no)
                                    ->where('stock_type', 1)//stock type must unrest
                                    ->first();

                                if (!$serials) {
                                    throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                                }
                            }
                        }

                        // Record Reversal to Movement History
                        $reversal = MovementHistory::create([
                            'material_doc' => $new_doc,
                            'doc_year' => date('Y'),
                            'movement_type_id' => $reversal_mvt->id,
                            'de_ce' => $reversal_mvt->dc_ind,
                            'movement_type_reason_id' => null,
                            'plant_id' => $mvmnt->plant_id,
                            'storage_id' => $mvmnt->storage_id,
                            'posting_date' => now(),
                            'document_date' => now(),
                            'material_slip' => $material_doc,
                            'doc_header_text' => 'Reversal '.$material_doc,
                            'work_center_id' => $mvmnt->work_center_id,
                            'cost_center_id' => $mvmnt->cost_center_id,

                            'item_no' => $item++,
                            'material_id' => $mvmnt->material_id,
                            'quantity_entry' => $mvmnt->quantity_entry,
                            'entry_uom_id' => $mvmnt->entry_uom_id,
                            'batch_no' => $mvmnt->batch_no,

                            'quantity' => $mvmnt->quantity,
                            'base_uom_id' => $mvmnt->base_uom_id,
                            'amount' => $mvmnt->amount,
                            'currency' => $mvmnt->currency,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // delete batch
                        $stock_batch->update([
                            'deleted' => 1,
                            'updated_by' => Auth::user()->id
                        ]);

                        // Update Stock
                        $qty = $cekstock->qty_unrestricted - $mvmnt->quantity;

                        $cekstock->update([
                            'qty_unrestricted' => $qty,
                            'updated_by' => Auth::user()->id
                        ]);

                        if ($material->serial_profile) {
                            foreach ($mvmnt->serial as $serial) {
                                // delete Serial if material serialize
                                $serials = StockSerial::where('serial', $serial->serial)
                                    ->where('material_id', $mvmnt->material_id)
                                    ->where('storage_id', $mvmnt->storage_id)
                                    ->where('batch_number', $mvmnt->batch_no)
                                    ->where('stock_type', 1)//stock type must unrest
                                    ->first();

                                $serials->update([
                                    'plant_id' => null,
                                    'storage_id' => null,
                                    'deleted' => 1,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // Record Serial Hitory if material serialize
                                SerialHistory::create([
                                    'movement_history_id' => $reversal->id,
                                    'plant_id' => $reversal->plant_id,
                                    'storage_id' => $reversal->storage_id,
                                    'material_id' => $reversal->material_id,
                                    'serial' => $serial->serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    // update code document
                    $code = TransactionCode::where('code', 'M')->first();
                    $code->update(['lastcode' => $new_doc]);

                    break;
                case 201:
                    $reversal_code = $movement_type->rev_code_id;

                    $reversal_mvt = MovementType::find($movement_type->rev_code_id);

                    $item = 1;

                    foreach ($main_movement as $mvmnt) {
                        // get stock
                        $cekstock = StockMain::where('material_id', $mvmnt->material_id)
                            ->where('storage_id', $mvmnt->storage_id)
                            ->first();

                        // get material
                        $material = Material::find($mvmnt->material_id);

                        // cek material
                        if (!$material) {
                            throw new \Exception('material not found');
                        }

                        // cek serial
                        if ($material->serial_profile) {
                            // if movement using batch
                            if ($mvmnt->batch_no) {
                                foreach ($mvmnt->serial as $serial) {
                                    $serials = StockSerial::where('serial', $serial->serial)
                                        ->where('material_id', $mvmnt->material_id)
                                        ->where('storage_id', null)
                                        ->where('batch_number', $mvmnt->batch_no)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->where('status', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                                    }
                                }
                            } else {
                                foreach ($mvmnt->serial as $serial) {
                                    $serials = StockSerial::where('serial', $serial->serial)
                                        ->where('material_id', $mvmnt->material_id)
                                        ->where('storage_id', null)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->where('status', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                                    }
                                }
                            }
                        }

                        // Record Reversal to Movement History
                        $reversal = MovementHistory::create([
                            'material_doc' => $new_doc,
                            'doc_year' => date('Y'),
                            'movement_type_id' => $reversal_mvt->id,
                            'de_ce' => $reversal_mvt->dc_ind,
                            'movement_type_reason_id' => null,
                            'plant_id' => $mvmnt->plant_id,
                            'storage_id' => $mvmnt->storage_id,
                            'posting_date' => now(),
                            'document_date' => now(),
                            'material_slip' => $material_doc,
                            'doc_header_text' => 'Reversal '.$material_doc,
                            'work_center_id' => $mvmnt->work_center_id,
                            'cost_center_id' => $mvmnt->cost_center_id,

                            'item_no' => $item++,
                            'material_id' => $mvmnt->material_id,
                            'quantity_entry' => $mvmnt->quantity_entry,
                            'entry_uom_id' => $mvmnt->entry_uom_id,
                            'batch_no' => $mvmnt->batch_no,

                            'quantity' => $mvmnt->quantity,
                            'base_uom_id' => $mvmnt->base_uom_id,
                            'amount' => $mvmnt->amount,
                            'currency' => $mvmnt->currency,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // Update Stock
                        $qty = $cekstock->qty_unrestricted + $mvmnt->quantity;

                        $cekstock->update([
                            'qty_unrestricted' => $qty,
                            'updated_by' => Auth::user()->id
                        ]);

                        // update stock batch if batch
                        if ($mvmnt->batch_no) {
                            $stock_batch = StockBatch::where('batch_number', $mvmnt->batch_no)->first();
                            $stock_batch_qty = $stock_batch->qty_unrestricted + $mvmnt->quantity;
                            $stock_batch->update([
                                'qty_unrestricted' => $stock_batch_qty,
                                'updated_by' => Auth::user()->id
                            ]);
                        }

                        if ($material->serial_profile) {
                            foreach ($mvmnt->serial as $serial) {
                                // delete Serial if material serialize
                                $serials = StockSerial::where('serial', $serial->serial)
                                    ->where('material_id', $mvmnt->material_id)
                                    ->where('storage_id', null)
                                    ->where('stock_type', 1)//stock type must unrest
                                    ->where('status', 1)//stock type must unrest
                                    ->first();

                                $serials->update([
                                    'plant_id' => $mvmnt->plant_id,
                                    'storage_id' => $mvmnt->storage_id,
                                    'stock_type' => 2,
                                    'deleted' => 0,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // Record Serial Hitory if material serialize
                                SerialHistory::create([
                                    'movement_history_id' => $reversal->id,
                                    'plant_id' => $reversal->plant_id,
                                    'storage_id' => $reversal->storage_id,
                                    'material_id' => $reversal->material_id,
                                    'serial' => $serial->serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    // update code document
                    $code = TransactionCode::where('code', 'M')->first();
                    $code->update(['lastcode' => $new_doc]);

                    break;
                case 311:
                    $reversal_code = $movement_type->rev_code_id;

                    $reversal_mvt = MovementType::find($movement_type->rev_code_id);

                    // get all movement history
                    $recive_movement = MovementHistory::where('material_doc', $material_doc)
                        ->where('de_ce', 'd')
                        ->get();

                    // get first movement history
                    $sender_movement = MovementHistory::where('material_doc', $material_doc)
                        ->where('de_ce', 'c')
                        ->where('item_no', 1)
                        ->first();

                    $item = 1;

                    foreach ($recive_movement as $mvmnt) {
                        // get stock
                        $cekstock = StockMain::where('material_id', $mvmnt->material_id)
                            ->where('storage_id', $mvmnt->storage_id)
                            ->first();

                        // cek stock
                        if (!$cekstock) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Devisit stock');
                        }

                        // cek stock qty
                        if ($cekstock->qty_unrestricted < $mvmnt->quantity) {
                            throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                        }

                        // get material
                        $material = Material::find($mvmnt->material_id);

                        // cek material
                        if (!$material) {
                            throw new \Exception('material not found');
                        }

                        // cek batch
                        if ($mvmnt->batch_no) {
                            $stock_batch_recive = StockBatch::where('stock_main_id', $cekstock->id)
                                ->where('batch_number', $mvmnt->batch_no)
                                ->first();

                            if (!$stock_batch_recive) {
                                throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                            }

                            if ($stock_batch_recive->qty_unrestricted < $mvmnt->quantity) {
                                throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                            }                            
                        }

                        // cek serial
                        if ($material->serial_profile) {
                            // if movement using batch
                            if ($mvmnt->batch_no) {
                                foreach ($mvmnt->serial as $serial) {
                                    $serials = StockSerial::where('serial', $serial->serial)
                                        ->where('material_id', $mvmnt->material_id)
                                        ->where('storage_id', $mvmnt->storage_id)
                                        ->where('batch_number', $mvmnt->batch_no)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                                    }
                                }
                            } else {
                                foreach ($mvmnt->serial as $serial) {
                                    $serials = StockSerial::where('serial', $serial->serial)
                                        ->where('material_id', $mvmnt->material_id)
                                        ->where('storage_id', $mvmnt->storage_id)
                                        ->where('stock_type', 1)//stock type must unrest
                                        ->first();

                                    if (!$serials) {
                                        throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                                    }
                                }
                            }
                        }

                        $movements_document = MovementHistory::where('material_doc', $new_doc)->count();

                        $credit_item_number = $movements_document + 1;
                        $debit_item_number = $credit_item_number + 1;

                        // Record Reversal to Movement History
                        $reversal_credit = MovementHistory::create([
                            'material_doc' => $new_doc,
                            'doc_year' => date('Y'),
                            'movement_type_id' => $reversal_mvt->id,
                            'de_ce' => 'c',
                            'movement_type_reason_id' => null,
                            'plant_id' => $mvmnt->plant_id,
                            'storage_id' => $mvmnt->storage_id,
                            'posting_date' => now(),
                            'document_date' => now(),
                            'material_slip' => $material_doc,
                            'doc_header_text' => 'Reversal '.$material_doc,
                            'work_center_id' => $mvmnt->work_center_id,
                            'cost_center_id' => $mvmnt->cost_center_id,

                            'item_no' => $credit_item_number,
                            'material_id' => $mvmnt->material_id,
                            'quantity_entry' => $mvmnt->quantity_entry,
                            'entry_uom_id' => $mvmnt->entry_uom_id,
                            'batch_no' => $mvmnt->batch_no,

                            'quantity' => $mvmnt->quantity,
                            'base_uom_id' => $mvmnt->base_uom_id,
                            'amount' => $mvmnt->amount,
                            'currency' => $mvmnt->currency,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        $reversal_debit = MovementHistory::create([
                            'material_doc' => $new_doc,
                            'doc_year' => date('Y'),
                            'movement_type_id' => $reversal_mvt->id,
                            'de_ce' => 'd',
                            'movement_type_reason_id' => null,
                            'plant_id' => $sender_movement->plant_id,
                            'storage_id' => $sender_movement->storage_id,
                            'posting_date' => now(),
                            'document_date' => now(),
                            'material_slip' => $material_doc,
                            'doc_header_text' => 'Reversal '.$material_doc,
                            'work_center_id' => $sender_movement->work_center_id,
                            'cost_center_id' => $sender_movement->cost_center_id,

                            'item_no' => $debit_item_number,
                            'material_id' => $mvmnt->material_id,
                            'quantity_entry' => $mvmnt->quantity_entry,
                            'entry_uom_id' => $mvmnt->entry_uom_id,
                            'batch_no' => $mvmnt->batch_no,

                            'quantity' => $mvmnt->quantity,
                            'base_uom_id' => $mvmnt->base_uom_id,
                            'amount' => $mvmnt->amount,
                            'currency' => $mvmnt->currency,

                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id
                        ]);

                        // Update Stock Recive
                        $qty = $cekstock->qty_unrestricted - $mvmnt->quantity;

                        $cekstock->update([
                            'qty_unrestricted' => $qty,
                            'updated_by' => Auth::user()->id
                        ]);

                        // Update Stock Sender
                        $cekstock2 = StockMain::where('material_id', $mvmnt->material_id)
                            ->where('storage_id', $sender_movement->storage_id)
                            ->first();

                        $qty2 = $cekstock2->qty_unrestricted + $mvmnt->quantity;

                        $cekstock2->update([
                            'qty_unrestricted' => $qty2,
                            'updated_by' => Auth::user()->id
                        ]);

                        // update stock batch if batch
                        if ($mvmnt->batch_no) {
                            // update batch stock sender
                            $stock_batch_sender = StockBatch::where('stock_main_id', $cekstock2->id)
                                ->where('batch_number', $mvmnt->batch_no)
                                ->first();

                            $stock_batch_sender_qty = $stock_batch_sender->qty_unrestricted + $mvmnt->quantity;

                            $stock_batch_sender->update([
                                'qty_unrestricted' => $stock_batch_sender_qty,
                                'updated_by' => Auth::user()->id
                            ]);

                            // update batch stock recive
                            $stock_batch_recive_qty = $stock_batch_recive->qty_unrestricted - $mvmnt->quantity;
                            
                            $stock_batch_recive->update([
                                'qty_unrestricted' => $stock_batch_recive_qty,
                                'updated_by' => Auth::user()->id
                            ]);
                        }

                        if ($material->serial_profile) {
                            foreach ($mvmnt->serial as $serial) {
                                // update Serial if material serialize
                                $serials = StockSerial::where('serial', $serial->serial)
                                    ->where('material_id', $mvmnt->material_id)
                                    ->where('storage_id', $mvmnt->storage_id)
                                    ->where('stock_type', 1)//stock type must unrest
                                    ->first();

                                $serials->update([
                                    'plant_id' => $sender_movement->plant_id,
                                    'storage_id' => $sender_movement->storage_id,
                                    'deleted' => 0,
                                    'updated_by' => Auth::user()->id
                                ]);

                                // Record Serial Hitory if material serialize
                                $serial_credit = SerialHistory::create([
                                    'movement_history_id' => $reversal_credit->id,
                                    'plant_id' => $reversal_credit->plant_id,
                                    'storage_id' => $reversal_credit->storage_id,
                                    'material_id' => $reversal_credit->material_id,
                                    'serial' => $serial->serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);

                                $serial_debit = SerialHistory::create([
                                    'movement_history_id' => $reversal_debit->id,
                                    'plant_id' => $reversal_debit->plant_id,
                                    'storage_id' => $reversal_debit->storage_id,
                                    'material_id' => $reversal_debit->material_id,
                                    'serial' => $serial->serial,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    // update code document
                    $code = TransactionCode::where('code', 'M')->first();
                    $code->update(['lastcode' => $new_doc]);

                    break;
                // case 605:
                //     $reversal_code = $movement_type->rev_code_id;

                //     $reversal_mvt = MovementType::find($movement_type->rev_code_id);

                //     // get all movement history debit
                //     $debit_movement = MovementHistory::where('material_doc', $material_doc)
                //         ->where('de_ce', 'd')
                //         ->get();

                //     // get first movement history
                //     $sender_movement = MovementHistory::where('material_doc', $material_doc)
                //         ->where('de_ce', 'c')
                //         ->where('item_no', 1)
                //         ->first();

                //     $item = 1;

                //     foreach ($debit_movement as $mvmnt) {
                //         // get stock reciver
                //         $cekstock = StockMain::where('material_id', $mvmnt->material_id)
                //             ->where('storage_id', $mvmnt->storage_id)
                //             ->first();

                //         // cek stock
                //         if (!$cekstock) {
                //             throw new \Exception('reversal movement for document '.$material_doc.' failed, Devisit stock');
                //         }

                //         // cek stock qty
                //         if ($cekstock->qty_unrestricted < $mvmnt->quantity) {
                //             throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //         }

                //         // get material
                //         $material = Material::find($mvmnt->material_id);

                //         // cek material
                //         if (!$material) {
                //             throw new \Exception('material not found');
                //         }

                //         // cek batch
                //         if ($mvmnt->batch_no) {
                //             $stock_batch_recive = StockBatch::where('stock_main_id', $cekstock->id)
                //                 ->where('batch_number', $mvmnt->batch_no)
                //                 ->first();

                //             if (!$stock_batch_recive) {
                //                 throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //             }

                //             if ($stock_batch_recive->qty_unrestricted < $mvmnt->quantity) {
                //                 throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //             }
                //         }

                //         // cek serial
                //         if ($material->serial_profile) {
                //             // if movement using batch
                //             if ($mvmnt->batch_no) {
                //                 foreach ($mvmnt->serial as $serial) {
                //                     $serials = StockSerial::where('serial', $serial->serial)
                //                         ->where('material_id', $mvmnt->material_id)
                //                         ->where('storage_id', $mvmnt->storage_id)
                //                         ->where('batch_number', $mvmnt->batch_no)
                //                         ->where('stock_type', 1)//stock type must unrest
                //                         ->first();

                //                     if (!$serials) {
                //                         throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                //                     }
                //                 }
                //             } else {
                //                 foreach ($mvmnt->serial as $serial) {
                //                     $serials = StockSerial::where('serial', $serial->serial)
                //                         ->where('material_id', $mvmnt->material_id)
                //                         ->where('storage_id', $mvmnt->storage_id)
                //                         ->where('stock_type', 1)//stock type must unrest
                //                         ->first();

                //                     if (!$serials) {
                //                         throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                //                     }
                //                 }
                //             }
                //         }

                //         $movements_document = MovementHistory::where('material_doc', $new_doc)->count();

                //         $credit_item_number = $movements_document + 1;
                //         $debit_item_number = $credit_item_number + 1;

                //         // Record Reversal to Movement History
                //         $reversal_credit = MovementHistory::create([
                //             'material_doc' => $new_doc,
                //             'doc_year' => date('Y'),
                //             'movement_type_id' => $reversal_mvt->id,
                //             'de_ce' => 'c',
                //             'movement_type_reason_id' => null,
                //             'plant_id' => $sender_movement->plant_id,
                //             'storage_id' => $sender_movement->storage_id,
                //             'posting_date' => now(),
                //             'document_date' => now(),
                //             'material_slip' => $material_doc,
                //             'doc_header_text' => 'Reversal '.$material_doc,
                //             'work_center_id' => $mvmnt->work_center_id,
                //             'cost_center_id' => $mvmnt->cost_center_id,

                //             'item_no' => $credit_item_number,
                //             'material_id' => $mvmnt->material_id,
                //             'quantity_entry' => $mvmnt->quantity_entry,
                //             'entry_uom_id' => $mvmnt->entry_uom_id,
                //             'batch_no' => $mvmnt->batch_no,

                //             'quantity' => $mvmnt->quantity,
                //             'base_uom_id' => $mvmnt->base_uom_id,

                //             'created_by' => Auth::user()->id,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         $reversal_debit = MovementHistory::create([
                //             'material_doc' => $new_doc,
                //             'doc_year' => date('Y'),
                //             'movement_type_id' => $reversal_mvt->id,
                //             'de_ce' => 'd',
                //             'movement_type_reason_id' => null,
                //             'plant_id' => $mvmnt->plant_id,
                //             'storage_id' => $mvmnt->storage_id,
                //             'posting_date' => now(),
                //             'document_date' => now(),
                //             'material_slip' => $material_doc,
                //             'doc_header_text' => 'Reversal '.$material_doc,
                //             'work_center_id' => $mvmnt->work_center_id,
                //             'cost_center_id' => $mvmnt->cost_center_id,

                //             'item_no' => $debit_item_number,
                //             'material_id' => $mvmnt->material_id,
                //             'quantity_entry' => $mvmnt->quantity_entry,
                //             'entry_uom_id' => $mvmnt->entry_uom_id,
                //             'batch_no' => $mvmnt->batch_no,

                //             'quantity' => $mvmnt->quantity,
                //             'base_uom_id' => $mvmnt->base_uom_id,

                //             'created_by' => Auth::user()->id,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // Update Stock 
                //         $qty = $cekstock->qty_unrestricted - $mvmnt->quantity;

                //         $cekstock->update([
                //             'qty_unrestricted' => $qty,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // get stock sender
                //         $cekstock2 = StockMain::where('material_id', $mvmnt->material_id)
                //             ->where('storage_id', $sender_movement->storage_id)
                //             ->first();

                //         $qty_transit = $cekstock2->qty_transit + $mvmnt->quantity;

                //         $cekstock2->update([
                //             'qty_transit' => $qty_transit,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // update stock batch if batch
                //         if ($mvmnt->batch_no) {
                //             // update batch stock recive
                //             $stock_batch_recive_qty = $stock_batch_recive->qty_unrestricted - $mvmnt->quantity;
                            
                //             $stock_batch_recive->update([
                //                 'qty_unrestricted' => $stock_batch_recive_qty,
                //                 'updated_by' => Auth::user()->id
                //             ]);

                //             $stock_batch_sender = StockBatch::where('stock_main_id', $cekstock2->id)
                //                 ->where('batch_number', $mvmnt->batch_no)
                //                 ->first();

                //             $stock_batch_sender_qty_transit = $stock_batch_sender->qty_transit + $mvmnt->quantity;

                //             $stock_batch_sender->update([
                //                 'qty_transit' => $stock_batch_sender_qty_transit,
                //                 'updated_by' => Auth::user()->id
                //             ]);
                //         }

                //         if ($material->serial_profile) {
                //             foreach ($mvmnt->serial as $serial) {
                //                 // delete Serial if material serialize
                //                 $serials = StockSerial::where('serial', $serial->serial)
                //                     ->where('material_id', $mvmnt->material_id)
                //                     ->where('storage_id', $mvmnt->storage_id)
                //                     ->where('stock_type', 1)//stock type must unrest
                //                     ->first();

                //                 $serials->update([
                //                     'plant_id' => $sender_movement->plant_id,
                //                     'storage_id' => $sender_movement->storage_id,
                //                     'stock_type' => 2,
                //                     'status' => 2,
                //                     'updated_by' => Auth::user()->id
                //                 ]);

                //                 // Record Serial Hitory if material serialize
                //                 $serial_credit = SerialHistory::create([
                //                     'movement_history_id' => $reversal_credit->id,
                //                     'plant_id' => $reversal_credit->plant_id,
                //                     'storage_id' => $reversal_credit->storage_id,
                //                     'material_id' => $reversal_credit->material_id,
                //                     'serial' => $serial->serial,
                //                     'created_by' => Auth::user()->id,
                //                     'updated_by' => Auth::user()->id
                //                 ]);

                //                 $serial_debit = SerialHistory::create([
                //                     'movement_history_id' => $reversal_debit->id,
                //                     'plant_id' => $reversal_debit->plant_id,
                //                     'storage_id' => $reversal_debit->storage_id,
                //                     'material_id' => $reversal_debit->material_id,
                //                     'serial' => $serial->serial,
                //                     'created_by' => Auth::user()->id,
                //                     'updated_by' => Auth::user()->id
                //                 ]);
                //             }
                //         }
                //     }

                //     // update DO
                //     $do = DeliveryHeader::where('delivery_code', $first_movement->delivery_no)->first();

                //     if (!$do) {
                //         throw new \Exception('DO not found');
                //     }

                //     // record do approval
                //     DeliveryApproval::create([
                //         'delivery_header_id' => $do->id,
                //         'user_id' => Auth::user()->id,
                //         'status_from' => $do->status,
                //         'status_to' => 2,
                //         'notes' => 'reversal',
                //     ]);

                //     $do->update([
                //         'status' => 2, // 0. Open, 1. Picking/Packing, 2. Sent, 3. Received
                //         'updated_by' => Auth::user()->id
                //     ]);

                //     // update code document
                //     $code = TransactionCode::where('code', 'M')->first();
                //     $code->update(['lastcode' => $new_doc]);

                //     break;
                // case 641:
                //     $reversal_code = $movement_type->rev_code_id;

                //     $reversal_mvt = MovementType::find($movement_type->rev_code_id);

                //     // get all movement history
                //     $recive_movement = MovementHistory::where('material_doc', $material_doc)
                //         ->where('de_ce', 'd')
                //         ->get();

                //     // get first movement history
                //     $sender_movement = MovementHistory::where('material_doc', $material_doc)
                //         ->where('de_ce', 'c')
                //         ->where('item_no', 1)
                //         ->first();

                //     // cek status DO
                //     $do = DeliveryHeader::where('delivery_code', $first_movement->delivery_no)->first();

                //     if (!$do) {
                //         throw new \Exception('DO not found');
                //     }

                //     if ($do->status == 3) {
                //         throw new \Exception('DO already recived, cant reversal');
                //     }

                //     $item = 1;

                //     foreach ($recive_movement as $mvmnt) {
                //         // get stock
                //         $cekstock = StockMain::where('material_id', $mvmnt->material_id)
                //             ->where('storage_id', $mvmnt->storage_id)
                //             ->first();

                //         // cek stock
                //         if (!$cekstock) {
                //             throw new \Exception('reversal movement for document '.$material_doc.' failed, Devisit stock');
                //         }

                //         // cek stock qty
                //         if ($cekstock->qty_transit < $mvmnt->quantity) {
                //             throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //         }

                //         // get material
                //         $material = Material::find($mvmnt->material_id);

                //         // cek material
                //         if (!$material) {
                //             throw new \Exception('material not found');
                //         }

                //         // cek batch
                //         if ($mvmnt->batch_no) {
                //             $stock_batch_recive = StockBatch::where('stock_main_id', $cekstock->id)
                //                 ->where('batch_number', $mvmnt->batch_no)
                //                 ->first();

                //             if (!$stock_batch_recive) {
                //                 throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //             }

                //             if ($stock_batch_recive->qty_transit < $mvmnt->quantity) {
                //                 throw new \Exception('reversal movement for document '.$material_doc.' failed, Defisit stock');
                //             }
                //         }

                //         // cek serial
                //         if ($material->serial_profile) {
                //             // if movement using batch
                //             if ($mvmnt->batch_no) {
                //                 foreach ($mvmnt->serial as $serial) {
                //                     $serials = StockSerial::where('serial', $serial->serial)
                //                         ->where('material_id', $mvmnt->material_id)
                //                         ->where('storage_id', $mvmnt->storage_id)
                //                         ->where('batch_number', $mvmnt->batch_no)
                //                         ->where('stock_type', 2)//stock type must unrest
                //                         ->first();

                //                     if (!$serials) {
                //                         throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                //                     }
                //                 }
                //             } else {
                //                 foreach ($mvmnt->serial as $serial) {
                //                     $serials = StockSerial::where('serial', $serial->serial)
                //                         ->where('material_id', $mvmnt->material_id)
                //                         ->where('storage_id', $mvmnt->storage_id)
                //                         ->where('stock_type', 2)//stock type must unrest
                //                         ->first();

                //                     if (!$serials) {
                //                         throw new \Exception('Serial '.$serial->serial.' not found, Defisit stock');
                //                     }
                //                 }
                //             }
                //         }

                //         $movements_document = MovementHistory::where('material_doc', $new_doc)->count();

                //         $credit_item_number = $movements_document + 1;
                //         $debit_item_number = $credit_item_number + 1;

                //         // Record Reversal to Movement History
                //         $reversal_credit = MovementHistory::create([
                //             'material_doc' => $new_doc,
                //             'doc_year' => date('Y'),
                //             'movement_type_id' => $reversal_mvt->id,
                //             'de_ce' => 'c',
                //             'movement_type_reason_id' => null,
                //             'plant_id' => $mvmnt->plant_id,
                //             'storage_id' => $mvmnt->storage_id,
                //             'posting_date' => now(),
                //             'document_date' => now(),
                //             'material_slip' => $material_doc,
                //             'doc_header_text' => 'Reversal '.$material_doc,
                //             'work_center_id' => $mvmnt->work_center_id,
                //             'cost_center_id' => $mvmnt->cost_center_id,

                //             'item_no' => $credit_item_number,
                //             'material_id' => $mvmnt->material_id,
                //             'quantity_entry' => $mvmnt->quantity_entry,
                //             'entry_uom_id' => $mvmnt->entry_uom_id,
                //             'batch_no' => $mvmnt->batch_no,

                //             'quantity' => $mvmnt->quantity,
                //             'base_uom_id' => $mvmnt->base_uom_id,

                //             'created_by' => Auth::user()->id,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         $reversal_debit = MovementHistory::create([
                //             'material_doc' => $new_doc,
                //             'doc_year' => date('Y'),
                //             'movement_type_id' => $reversal_mvt->id,
                //             'de_ce' => 'd',
                //             'movement_type_reason_id' => null,
                //             'plant_id' => $sender_movement->plant_id,
                //             'storage_id' => $sender_movement->storage_id,
                //             'posting_date' => now(),
                //             'document_date' => now(),
                //             'material_slip' => $material_doc,
                //             'doc_header_text' => 'Reversal '.$material_doc,
                //             'work_center_id' => $sender_movement->work_center_id,
                //             'cost_center_id' => $sender_movement->cost_center_id,

                //             'item_no' => $debit_item_number,
                //             'material_id' => $mvmnt->material_id,
                //             'quantity_entry' => $mvmnt->quantity_entry,
                //             'entry_uom_id' => $mvmnt->entry_uom_id,
                //             'batch_no' => $mvmnt->batch_no,

                //             'quantity' => $mvmnt->quantity,
                //             'base_uom_id' => $mvmnt->base_uom_id,

                //             'created_by' => Auth::user()->id,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // Update Stock 
                //         $qty_transit = $cekstock->qty_transit - $mvmnt->quantity;

                //         $cekstock->update([
                //             'qty_transit' => $qty_transit,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // sender stock
                //         $cekstock2 = StockMain::where('material_id', $sender_movement->material_id)
                //             ->where('storage_id', $sender_movement->storage_id)
                //             ->first();

                //         $qty_unrestricted = $cekstock2->qty_unrestricted + $mvmnt->quantity;

                //         // update sender stock
                //         $cekstock2->update([
                //             'qty_unrestricted' => $qty_unrestricted,
                //             'updated_by' => Auth::user()->id
                //         ]);

                //         // update stock batch if batch
                //         if ($mvmnt->batch_no) {
                //             // update batch stock sender
                //             $stock_batch_sender = StockBatch::where('stock_main_id', $cekstock2->id)
                //                 ->where('batch_number', $mvmnt->batch_no)
                //                 ->first();

                //             $stock_batch_sender_qty = $stock_batch_sender->qty_unrestricted + $mvmnt->quantity;

                //             $stock_batch_sender->update([
                //                 'qty_unrestricted' => $stock_batch_sender_qty,
                //                 'updated_by' => Auth::user()->id
                //             ]);

                //             // update batch stock recive
                //             $stock_batch_recive_qty = $stock_batch_recive->qty_transit - $mvmnt->quantity;
                            
                //             $stock_batch_recive->update([
                //                 'qty_transit' => $stock_batch_recive_qty,
                //                 'updated_by' => Auth::user()->id
                //             ]);
                //         }

                //         if ($material->serial_profile) {
                //             foreach ($mvmnt->serial as $serial) {
                //                 // delete Serial if material serialize
                //                 $serials = StockSerial::where('serial', $serial->serial)
                //                     ->where('material_id', $mvmnt->material_id)
                //                     ->where('storage_id', $mvmnt->storage_id)
                //                     ->where('stock_type', 2)//stock type must transit
                //                     ->first();

                //                 $serials->update([
                //                     'plant_id' => $sender_movement->plant_id,
                //                     'storage_id' => $sender_movement->storage_id,
                //                     'stock_type' => 1,
                //                     'status' => 2,
                //                     'updated_by' => Auth::user()->id
                //                 ]);

                //                 // Record Serial Hitory if material serialize
                //                 $serial_credit = SerialHistory::create([
                //                     'movement_history_id' => $reversal_credit->id,
                //                     'plant_id' => $reversal_credit->plant_id,
                //                     'storage_id' => $reversal_credit->storage_id,
                //                     'material_id' => $reversal_credit->material_id,
                //                     'serial' => $serial->serial,
                //                     'created_by' => Auth::user()->id,
                //                     'updated_by' => Auth::user()->id
                //                 ]);

                //                 $serial_debit = SerialHistory::create([
                //                     'movement_history_id' => $reversal_debit->id,
                //                     'plant_id' => $reversal_debit->plant_id,
                //                     'storage_id' => $reversal_debit->storage_id,
                //                     'material_id' => $reversal_debit->material_id,
                //                     'serial' => $serial->serial,
                //                     'created_by' => Auth::user()->id,
                //                     'updated_by' => Auth::user()->id
                //                 ]);
                //             }
                //         }
                //     }

                //     // record do approval
                //     DeliveryApproval::create([
                //         'delivery_header_id' => $do->id,
                //         'user_id' => Auth::user()->id,
                //         'status_from' => $do->status,
                //         'status_to' => 1,
                //         'notes' => 'reversal',
                //     ]);

                //     $do->update([
                //         'status' => 1, // 0. Open, 1. Picking/Packing, 2. Sent, 3. Received
                //         'updated_by' => Auth::user()->id
                //     ]);

                //     // update reservation fullfilment status
                //     foreach ($do->item as $val) {
                //         $reservation = Reservation::find($val['reservation_id']);
                //         $reservation->update([
                //             'fulfillment_status' => 0,//0.Open, 1.Completed
                //             'updated_by' => Auth::user()->id
                //         ]);
                //     }

                //     // update code document
                //     $code = TransactionCode::where('code', 'M')->first();
                //     $code->update(['lastcode' => $new_doc]);

                //     break;
                default:
                    return response()->json([
                        'message' => 'reversal movement type '.$movement_type_code.', will be handled soon',
                    ], 422);
            }

            DB::commit();

            return response()->json([
                'message' => 'success reverse document '.$material_doc.''
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while reverse document '.$material_doc.'',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
