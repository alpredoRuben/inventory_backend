<?php

namespace App\Http\Controllers\Api\Accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use PDF;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\DocumentType;
use App\Models\AccountingHeader;
use App\Models\AccountingDetail;
use App\Models\AccountingAttachment;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\MovementHistory;
use App\Models\MovementType;
use App\Models\PurchaseDocumentHistory;
use App\Models\Supplier;
use App\Models\TransactionCode;

class IncomingInvoiceController extends Controller
{
    public function documentType()
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $doc = (new DocumentType)->newQuery();

        $doc->where('active', true);

        if (request()->has('account_type') && request()->input('account_type') != '') {
            $doc->where('account_type', request()->input('account_type'));
        }

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $doc->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $doc->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $doc->orderBy('code', 'asc');
        }

        $doc = $doc->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $doc;
    }

    public function getPoNumberGrByVendor($vendor_id)
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $po_no = PurchaseHeader::where('vendor_id', (int)$vendor_id)->pluck('po_doc_no');

        $po_mvt = MovementType::where('code', 101)->first();

        $po_gr = MovementHistory::whereIn('po_number', $po_no)
            ->where('movement_type_id', $po_mvt->id)
            ->orderBy('po_number', 'asc')
            ->pluck('po_number');

        $data = $po_gr->unique()->values();

        return (new Collection($data))->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }

    public function poDetail($po_number)
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $po = PurchaseHeader::where('po_doc_no', $po_number)->first();

        $po->gr = PurchaseDocumentHistory::where('purchase_header_id', $po->id)
            ->where('transaction', 1)
            ->with(['purchase_item'])
            ->get();

        return $po;
    }

    public function getPoGrByVendor($vendor_id)
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $po_id = PurchaseHeader::where('vendor_id', (int)$vendor_id)->pluck('id');

        // $po = PurchaseDocumentHistory::whereIn('purchase_header_id', $po_id)
        //     ->where('transaction', 1)
        //     ->with(['purchase_header', 'purchase_item'])
        //     ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
        //     ->appends(Input::except('page'));

        // get po have gr
        $po_history_gr = PurchaseDocumentHistory::whereIn('purchase_header_id', $po_id)
            ->where('transaction', 1)
            ->get();

        // get only gr blm invoice
        $gr_blm_invoice = [];
        foreach ($po_history_gr as $gr) {
            $ref_gr_blm_invoice = $gr->material_doc.'-'.$gr->material_doc_item;
            $gr_sdh_invoice = PurchaseDocumentHistory::whereIn('purchase_header_id', $po_id)
                ->where('transaction', 2)
                ->where('reference_doc', $ref_gr_blm_invoice)
                ->first();

            if (!$gr_sdh_invoice) {
                $gr_blm_invoice[] = $gr->id; 
            }
        }

        $data = PurchaseDocumentHistory::whereIn('id', $gr_blm_invoice)
            ->with(['purchase_header', 'purchase_item'])
            ->with('purchase_header.term_payment')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $data;
    }

    public function getMaterialDoc()
	{
        // Auth::user()->cekRoleModules(['goods-movement-create']);

		// get code 
        $code = TransactionCode::where('code', 'VI')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is graether than year in code
            $yearcode = substr($code->lastcode, 2, 2);
            $increment = substr($code->lastcode, 4, 6) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'VI';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'VI';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%06d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'VI';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '000001';
        }

        return $lastcode;
    }
    
    public function index()
    {
        Auth::user()->cekRoleModules(['incoming-invoice-view']);

        $data = (new AccountingHeader)->newQuery();

        $data->with([
            'detail', 'document_type', 'company', 'createdBy'
        ]);

        // filter
        if (request()->has('status')) {
            $data->whereIn('status', request()->input('status'));
        }

        if (request()->has('document_type_id')) {
            $data->whereIn('document_type_id', request()->input('document_type_id'));
        }

        if (request()->has('vendor_id')) {
            $vendor = request()->input('vendor_id');
            // relasi ke detail
            $data->whereHas('detail', function($v) use ($vendor) {
                $v->whereIn('vendor_id', $vendor);
            });
        }

        if (request()->has('reference')) {
            $reference = request()->input('reference');
            $data->where(DB::raw("LOWER(reference)"), 'LIKE', "%".strtolower($reference)."%");
        }

        if (request()->has('created_by')) {
            $data->whereIn('created_by', request()->input('created_by'));
        }

        if (request()->has('invoice_date')) {
            $from = str_replace('"', '', request()->invoice_date[0]);
            $to = str_replace('"', '', request()->invoice_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            $data->where('doc_date', '>=', $f.' 00:00:00');
            $data->where('doc_date', '<=', $t.' 00:00:00');
        }

        if (request()->has('due_date')) {
            $from = str_replace('"', '', request()->due_date[0]);
            $to = str_replace('"', '', request()->due_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            // relasi detail
            $data->whereHas('detail', function($v) use ($t, $f) {
                $v->where('due_date', '>=', $f.' 00:00:00');
                $v->where('due_date', '<=', $t.' 00:00:00');
            });
        }

        if (request()->has('created_date')) {
            $from = str_replace('"', '', request()->created_date[0]);
            $to = str_replace('"', '', request()->created_date[1]);
            $f = Carbon::parse($from)->format('Y-m-d');
            $t = Carbon::parse($to)->addDays(1)->format('Y-m-d');

            $data->where('created_at', '>=', $f.' 00:00:00');
            $data->where('created_at', '<=', $t.' 00:00:00');
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $data->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $data->orderBy('document_no', 'desc');
        }

        $data = $data->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $data->transform(function($data){
            $detail_fisrt = $data->detail->first();

            $data->vendor = $detail_fisrt->vendor;
            $data->due_date = $detail_fisrt->due_date;
            $data->po_number = $detail_fisrt->purchasing_doc_no;
            $data->term_payment = $detail_fisrt->term_payment;
            $data->amount = $data->detail->sum('amount');

            return $data;
        });
        
        return $data;
    }

    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $this->validate(request(), [
            'document_type_id' => 'required|exists:document_types,id',
            'company_id' => 'required|exists:companies,id',
            'invoice_date' => 'required|date',
            'posting_date' => 'required|date',
            'vendor_id' => 'required|exists:document_types,id',
            'reference' => 'required|max:191',
            'header_text' => 'nullable',
            'due_date' => 'required|date',
            'term_payment_id' => 'required|exists:term_payments,id',
            'amount' => 'required|numeric',
            'total_amount_gr' => 'required|numeric',
            'tax_amount' => 'nullable|numeric',
            'currency' => 'nullable',
            'item' => 'array',
            'item.*.gr_id' => 'required|exists:purchase_document_histories,id',
            'attachments' => 'array',
            'attachments.*' => 'nullable|file|max:5192'
        ]);

        // validate doc type vendor invoice KR
        $vi_dt = DocumentType::where('code', 'KR')->first();

        if ($vi_dt->id != $request->document_type_id) {
            return response()->json([
                'message'   => 'Document Type '.$vi_dt->code.' will be handled soon'
            ], 422);
        }
        
        $gr_id = [];
        foreach ($request->item as $val) {
            $gr_id[] = $val['gr_id'];
        }

        // validasi gr in 1 po number
        $gr_item = PurchaseDocumentHistory::whereIn('id', $gr_id)->pluck('purchase_header_id');

        $no_po = $gr_item->unique()->count();

        if ($no_po > 1) {
            return response()->json([
                'message'   => 'Data invalid, GR can be proceed must in 1 Purchase Order'
            ], 422);
        }

        // Reference, ini unik meskiput freetext. Untuk vendor yg sama tidak boleh duplicate. Field ini untuk mencegah Invoice yg sama di proses lebih dari sekali.
        $vendor = Supplier::find($request->vendor_id);
        $used_ref = AccountingHeader::where('reference', $request->reference)->first();

        if ($used_ref) {
            $used_ref_vendor = $used_ref->detail->where('vendor_id', $request->vendor_id)->first();
            if ($used_ref_vendor) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'reference'  => ['Reference has been used for invoice vendor '.$vendor->code.'']
                    ]
                ], 422);
            }
        }

        // cek Total GR PO different from Amount
        $global_setting = (int)appsetting('VENDOR_INVOICE_DIFF') ? (int)appsetting('VENDOR_INVOICE_DIFF') : 0;
        $selisih = ($request->total_amount_gr - $request->amount) * -1;
        if ($selisih >= $global_setting) {
            return response()->json([
                'message'   => 'Data invalid, Total GR PO different from Amount'
            ], 422);
        }

        // validasi attchment
        if ($request->has('attachments')) {
            foreach ($request->attachments as $key => $attachment) {
                // get file
                $file = $attachment;

                // get file extension for validation
                $ext_file = $file->getClientOriginalExtension();

                $ext = ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'zip', 'rar'];

                if (!in_array($ext_file, $ext)) {
                    return response()->json([
                        'message' => 'Data invalid',
                        'errors' => [
                            'attachments.'.$key => ['File Type support to upload is png, jpg, jpeg, pdf, doc, docx, ppt, pptx, xls, xlsx, zip, rar']
                        ]
                    ],422);
                }
            }
        }

        $new_doc = $this->getMaterialDoc();

        try {
            DB::beginTransaction();

            // save header
            $header = AccountingHeader::create([
                'company_id' => $request->company_id,
                'document_no' => $new_doc,
                'year' => Carbon::parse($request->invoice_date)->format('Y'),
                'document_type_id' => $request->document_type_id,
                'doc_date' => $request->invoice_date,
                'posting_date' => $request->posting_date,
                'fiscal_period' => Carbon::parse($request->invoice_date)->format('m'),
                'currency' => $request->currency,
                'reference' => $request->reference,
                'header_text' => $request->header_text,
                'status' => 0,
                'created_by' => Auth::user()->id
            ]);

            // save detail
            $seq = 1;
            foreach ($request->item as $val) {
                $gr = PurchaseDocumentHistory::find($val['gr_id']);

                $detail = AccountingDetail::create([
                    'accounting_header_id' => $header->id,
                    'seq_number' => $seq++,
                    'account_type' => null,
                    'posting_key_id' => null,
                    'd_c' => 'c',
                    'clearing_date' => null,
                    'clearing_created_time' => null,
                    'clearning_doc' => null,
                    'tax_code' => $gr->purchase_item->tax,
                    'tax_type' => 1,
                    'amount' => $gr->amount,
                    'currency' => $request->currency,
                    'amount_lc' => $gr->amount_lc,
                    'assignment' => null,
                    'co_area' => null,
                    'cost_center_id' => $gr->purchase_item->cost_center_id,
                    'purchasing_doc_no' => $gr->purchase_header->po_doc_no,
                    'purchasing_doc_item' => $gr->purchase_item->purchasing_item_no,
                    'billing_doc' => null,
                    'sales_order' => null,
                    'item_no' => null,
                    'asset_id' => null,
                    'account_id' => $gr->purchase_item->gl_account_id,
                    'vendor_id' => $request->vendor_id,
                    'customer_id' => null,
                    'bl_pl' => null,
                    'baseline_date' => null,
                    'term_payment_id' => $gr->purchase_header->term_payment_id,
                    'due_date' => $request->due_date
                ]);

                // reocord to purchase document history
                PurchaseDocumentHistory::create([
                    'purchase_header_id' => $gr->purchase_header_id,
                    'purchase_item_id' => $gr->purchase_item_id,
                    'transaction' => 2, //Invoice
                    'material_year' => $header->year,
                    'material_doc' => $header->document_no,
                    'material_doc_item' => $detail->seq_number,
                    'de_ce' => $detail->d_c,
                    'quantity' => $gr->quantity,
                    'amount_lc' => $gr->amount_lc,
                    'quantity_order' => $gr->quantity_order,
                    'amount' => $gr->amount,
                    'reference_doc' => $gr->material_doc.'-'.$gr->material_doc_item,
                ]);
            }

            // attachment
            if ($request->has('attachments')) {
                foreach ($request->attachments as $key => $attachment) {
                    // upload file
                    $file_data = $attachment;
                    $file_ext  = $attachment->getClientOriginalExtension();
                    $file_old  = $attachment->getClientOriginalName();
                    $file_name = $header->doc_no . md5(time().$file_old). "." .$file_ext;
                    $file_path = 'file/accounting/'.$header->document_no;

                    $uploaded = Storage::disk('public')->putFileAs($file_path, $file_data, $file_name);

                    // insert data attachment
                    AccountingAttachment::create([
                        'accounting_header_id' => $header->id,
                        'name' => $uploaded,
                        'created_by' => Auth::user()->id,
                        'original_name' => $file_old
                    ]);
                }
            }

            // update code document
            $code = TransactionCode::where('code', 'VI')->first();
            $code->update(['lastcode' => $new_doc]);

            DB::commit();
            
            return response()->json([
                'message' => 'success proceed invoice'
            ], 200);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while proceed invoice',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function show($id)
    {
        Auth::user()->cekRoleModules(['incoming-invoice-view']);

        $data = AccountingHeader::with(['detail', 'document_type', 'company', 'attachment', 'createdBy'])
            ->find($id);
            
        $detail_fisrt = $data->detail->first();

        $data->vendor = $detail_fisrt->vendor;
        $data->due_date = $detail_fisrt->due_date;
        $data->po_number = $detail_fisrt->purchasing_doc_no;
        $data->term_payment = $detail_fisrt->term_payment;
        $data->amount = $data->detail->sum('amount');
        
        return $data;
    }
}
