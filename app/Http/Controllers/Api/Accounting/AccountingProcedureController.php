<?php

namespace App\Http\Controllers\Api\Accounting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Support\Facades\Input;

use App\Models\AccountingProcedure;
use App\Models\TransactionType;
use App\Models\SubType;
use App\Helpers\HashId;

class AccountingProcedureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource with hash id.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        Auth::user()->cekRoleModules(['accounting-procedure-view']);

        $accountingProcedure = (new AccountingProcedure)->newQuery();

        if (request()->has('q') && request()->input('q') != '') {
            $q = strtolower(request()->input('q'));

            // search transaction_type
            $accountingProcedure->whereHas('transaction_type', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['transaction_type' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search sub_type
            $accountingProcedure->orWhereHas('sub_type', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['sub_type' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search valuation_class
            $accountingProcedure->orWhereHas('valuation_class', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['valuation_class' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);

            // search gl_account
            $accountingProcedure->orWhereHas('gl_account', function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            })
            ->with(['gl_account' => function ($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            }]);
        }

        $accountingProcedure->with(['transaction_type', 'sub_type', 'valuation_class', 'gl_account']);
        $accountingProcedure->with('gl_account.account_group');


        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'transaction_type':
                    $accountingProcedure->join('transaction_types', 'transaction_types.id', '=', 'accounting_procedures.transaction_type_id');
                    $accountingProcedure->select('accounting_procedures.*');
                    $accountingProcedure->orderBy('transaction_types.code', $sort_order);
                break;

                case 'sub_type':
                    $accountingProcedure->leftJoin('sub_types', 'sub_types.id', '=', 'accounting_procedures.sub_type_id');
                    $accountingProcedure->select('accounting_procedures.*');
                    $accountingProcedure->orderBy('sub_types.code', $sort_order);
                break;

                case 'valuation_class':
                    $accountingProcedure->join('valuation_classes', 'valuation_classes.id', '=', 'accounting_procedures.valuation_class_id');
                    $accountingProcedure->select('accounting_procedures.*');
                    $accountingProcedure->orderBy('valuation_classes.code', $sort_order);
                break;

                case 'gl_account':
                    $accountingProcedure->join('accounts', 'accounts.id', '=', 'accounting_procedures.gl_account_id');
                    $accountingProcedure->select('accounting_procedures.*');
                    $accountingProcedure->orderBy('accounts.code', $sort_order);
                break;

                default:
                $accountingProcedure->orderBy($sort_field, $sort_order);
            }
        } else {
            $accountingProcedure->orderBy('id', 'desc');
        }

        $accountingProcedure = $accountingProcedure->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($accountingProcedure['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $accountingProcedure['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $accountingProcedure;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Auth::user()->cekRoleModules(['accounting-procedure-create']);

        $this->validate(request(), [
            'transaction_type_id' => 'required|exists:transaction_types,id',
            'sub_type_id' => 'nullable|exists:sub_types,id',
            'valuation_class_id' => 'required|exists:valuation_classes,id',
            'gl_account_id' => 'required|exists:accounts,id'
        ]);

        $accountingProcedure = AccountingProcedure::where('transaction_type_id', $request->transaction_type_id)
            ->where('sub_type_id', $request->sub_type_id)
            ->where('valuation_class_id', $request->valuation_class_id)
            ->where('gl_account_id', $request->gl_account_id)
            ->first();

        if ($accountingProcedure) {

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'transaction_type_id' => ['Data Already exist']
                ]
            ],422);
        } else {
            $save = AccountingProcedure::create([
                'transaction_type_id' => $request->transaction_type_id,
                'sub_type_id' => $request->sub_type_id,
                'valuation_class_id' => $request->valuation_class_id,
                'gl_account_id' => $request->gl_account_id
            ]);

            return $save;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->cekRoleModules(['accounting-procedure-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        return AccountingProcedure::with(['transaction_type', 'sub_type', 'valuation_class', 'gl_account'])
        	->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['accounting-procedure-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $accountingProcedure = AccountingProcedure::findOrFail($id);

        $this->validate(request(), [
            'transaction_type_id' => 'required|exists:transaction_types,id',
            'sub_type_id' => 'nullable|exists:sub_types,id',
            'valuation_class_id' => 'required|exists:valuation_classes,id',
            'gl_account_id' => 'required|exists:accounts,id'
        ]);

        $save = $accountingProcedure->update([
            'transaction_type_id' => $request->transaction_type_id,
            'sub_type_id' => $request->sub_type_id,
            'valuation_class_id' => $request->valuation_class_id,
            'gl_account_id' => $request->gl_account_id
        ]);

        if ($save) {
            return $accountingProcedure;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->cekRoleModules(['accounting-procedure-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 400);
        }

        $delete = AccountingProcedure::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDelete(Request $request)
    {
        Auth::user()->cekRoleModules(['accounting-procedure-update']);

        try {
            $array = array();
            foreach (request()->id as $id) {
                $decode = HashId::decode($id);
                array_push($array, $decode);
            }
            request()->merge(["id"=>$array]);    
        }
        catch(\Exception $ex) {
            return response()->json([
                'message' => 'Unable to delete. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate($request, [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:accounting_procedures,id',
        ]);

        try {
            DB::beginTransaction();

            foreach ($request->id as $id) {
                $delete = AccountingProcedure::findOrFail($id);
                $delete->delete();
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function transactionType()
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $doc = (new TransactionType)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $doc->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $doc->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $doc->orderBy('code', 'asc');
        }

        $doc = $doc->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $doc;
    }

    public function subType()
    {
        // Auth::user()->cekRoleModules(['incoming-invoice-create']);

        $doc = (new SubType)->newQuery();

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $doc->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $doc->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $doc->orderBy('code', 'asc');
        }

        $doc = $doc->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $doc;
    }
}
