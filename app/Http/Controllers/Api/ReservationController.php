<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Helpers\Collection;

use App\Models\User;
use App\Models\Reservation;
use App\Models\ReservationApproval;
use App\Models\TransactionCode;
use App\Models\MovementType;
use App\Models\Storage as MasterStorage;
use App\Models\StockDetermination;
use App\Models\GroupMaterial;
use App\Models\Material;
use App\Models\MaterialPlant;
use App\Models\MaterialVendor;
use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\Plant;
use App\Models\CostCenter;
use App\Models\StockMain;
use App\Models\StockBatch;
use App\Models\Uom;
use App\Models\MaterialAccount;
use App\Models\ReservationAttachment;
use App\Models\Supplier;

class ReservationController extends Controller
{
    public function reservationCode()
    {
        Auth::user()->cekRoleModules(['reservation-create']);
        
        // get reservation code 
        $code = TransactionCode::where('code', 'R')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is greater than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'R';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'R';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'R';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function PRCode()
    {
        Auth::user()->cekRoleModules(['reservation-create']);
        
        // get reservation code 
        $code = TransactionCode::where('code', 'A')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is greater than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'A';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'A';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function POCode()
    {
        // get PO code 
        $code = TransactionCode::where('code', 'B')->first();

        // if exist
        if ($code->lastcode) {
            // generate if this year is greater than year in code
            $yearcode = substr($code->lastcode, 1, 2);
            $increment = substr($code->lastcode, 3, 7) + 1;
            $year = substr(date('Y'), 2);
            if ($year > $yearcode) {
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= '0000001';
            } else {
                // increment if year in code is equal to this year
                $lastcode = 'B';
                $lastcode .= substr(date('Y'), 2);
                $lastcode .= sprintf("%07d", $increment);
            }
        } else {//generate if not exist
            $lastcode = 'B';
            $lastcode .= substr(date('Y'), 2);
            $lastcode .= '0000001';
        }

        return $lastcode;
    }

    public function index()
    {
        Auth::user()->cekRoleModules(['reservation-view']);

        $reservation = (new Reservation)->newQuery();

        $reservation->with([
        	'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
        	'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
        ]);

        // get reservation header only (unique reservation code)
        $reservation->whereNull('material_id');

        // get reservation type only
        $reservation->where('document_type', 'R');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $reservation->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_header)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_item)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(unit_cost)"), 'LIKE', "%".$q."%");
            });
        }        

        // filter type
        if (request()->has('reservation_type')) {
            $reservation->whereIn('reservation_type', request()->input('reservation_type'));
        }

        // filter plant
        if (request()->has('plant')) {
            $reservation->whereIn('plant_id', request()->input('plant'));
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $reservation->whereIn('plant_id', $plant_id);
        }

        // filter storage
        if (request()->has('storage')) {
            $reservation->whereIn('storage_id', request()->input('storage'));
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $reservation->whereIn('storage_id', $storage_id);
        }

        // filter material
        if (request()->has('material')) {
            $reservation->whereIn('material_id', request()->input('material'));
        }

        // filter creator
        if (request()->has('created_by')) {
            $reservation->whereIn('created_by', request()->input('created_by'));
        }

        // filter reservation code
        if (request()->has('reservation_code')) {
            $reservation->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".strtolower(request()->input('reservation_code'))."%");
        }

        // filter requirement date
        if (request()->has('requirement_date')) {
            $reservation->whereBetween('requirement_date', [request()->requirement_date[0], request()->requirement_date[1]]);
        }

        // filter status
        if (request()->has('status')) {
            $reservation->whereIn('status', request()->input('status'));
        } else {
            $reservation->where('status', '!=', 10);
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $reservation->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $reservation->orderBy('id', 'desc');
        }

        $reservation = $reservation->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $reservation->transform(function($data){
            $reservation_item = Reservation::with([
                'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy',
                'group_material', 'procurement_group', 'commitment_item', 'account', 'project',
                'prefered_vendor'
            ])
            ->with('material.images')
            ->with('material.base_uom')
            ->whereNotNull('material_id')
            ->where('reservation_code', $data->reservation_code)
            ->orderBy('id', 'desc')
            ->get();

            // get on hand stock material
            $reservation_item->map(function($dataq){
                // get material price from stock
                $stock = StockMain::where('material_id', $dataq->material_id)
                    ->where('storage_id', $dataq->inout_storage_id)
                    ->first();

                // get price from info record
                $info_record = MaterialVendor::where('material_id', $dataq->material_id)
                        ->where('storage_id', $dataq->inout_storage_id)
                        ->first();

                if ($stock) {//get price from stock
                    $batch = StockBatch::where('stock_main_id', $stock->id)
                        ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                        ->orderBy('id', 'asc')
                        ->first();
                    
                    if ($batch) {
                        $unit_cost = $batch->unit_cost;
                    } else {
                        $unit_cost = 0;
                    }
                } else if ($info_record) {//get price from info record
                    if ($info_record) {
                        $unit_cost = $info_record->unit_price;
                    } else {
                        $unit_cost = 0;
                    }
                } else {//get price from material account
                    $mat_account = MaterialAccount::where('material_id', $dataq->material_id)
                        ->where('plant_id', $dataq->inout_plant_id)
                        ->first();

                    if ($mat_account) {
                        // cek material account type
                        if ($mat_account->price_controll) {
                            $unit_cost = $mat_account->standart_price;
                        } else {
                            $unit_cost = $mat_account->moving_price;
                        }
                    } else {
                        $unit_cost = 0;
                    }
                }

                $price = $unit_cost ? $unit_cost : 0;
                $dataq->price = $price * $dataq->quantity_base_unit;

                return $dataq;
            });

            $data->total_amount = collect($reservation_item)->sum('price');

            return $data;
        });

        return $reservation;
    }

    public function prList()
    {
        Auth::user()->cekRoleModules(['reservation-view']);

        $pr = (new Reservation)->newQuery();

        $pr->with([
            'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
            'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
        ]);

        // get reservation header only (unique reservation code)
        $pr->whereNull('material_short_text');

        // get reservation type only
        $pr->where('document_type', 'A');

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $pr->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_header)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(note_item)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(unit_cost)"), 'LIKE', "%".$q."%");
            });
        }        

        // filter proc_group_id
        if (request()->has('proc_group')) {
            $pr->whereIn('proc_group_id', request()->input('proc_group'));
        }

        // filter plant
        if (request()->has('plant')) {
            $pr->whereIn('plant_id', request()->input('plant'));
        }

        // if have organization parameter
        $plant_id = Auth::user()->roleOrgParam(['plant']);

        if (count($plant_id) > 0) {
            $pr->whereIn('plant_id', $plant_id);
        }

        // filter storage
        if (request()->has('storage')) {
            $pr->whereIn('storage_id', request()->input('storage'));
        }

        // if have organization parameter
        $storage_id = Auth::user()->roleOrgParam(['storage']);

        if (count($storage_id) > 0) {
            $pr->whereIn('storage_id', $storage_id);
        }

        // filter creator
        if (request()->has('created_by')) {
            $pr->whereIn('created_by', request()->input('created_by'));
        }

        // filter reservation code
        if (request()->has('reservation_code')) {
            $pr->where(DB::raw("LOWER(reservation_code)"), 'LIKE', "%".strtolower(request()->input('reservation_code'))."%");
        }

        // filter requirement date
        if (request()->has('requirement_date')) {
            $pr->whereBetween('requirement_date', [request()->requirement_date[0], request()->requirement_date[1]]);
        }

        // filter status
        if (request()->has('status')) {
            $pr->whereIn('status', request()->input('status'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $pr->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $pr->orderBy('id', 'desc');
        }

        $pr = $pr->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        $pr->transform(function($data){
            $r = Reservation::where('reservation_code', $data->reservation_code)->pluck('id');

            $ammount = [];
            foreach ($r as $value) {
                $getpr = Reservation::find($value);
                $ammount[] = $getpr->unit_cost * $getpr->quantity;
            }

            $data->ammount = collect($ammount)->sum();
            return $data;
        });

        return $pr;
    }

    public function show($reservation_code)
    {
        Auth::user()->cekRoleModules(['reservation-view']);

        $first = Reservation::where('reservation_code', $reservation_code)->first();

        if (!$first) {
            return response()->json([
                'message' => 'Reservation code '.$reservation_code.', not found',
            ], 404);
        }

        $doc_type = $first->document_type;
        
        if ($doc_type == 'R') {
            $res_type = 'Reservation';
        } elseif ($doc_type == 'A') {
            $res_type = 'Purchase Requisition';
        }

        switch ($doc_type) {
            case 'R':
                $reservation = Reservation::with([
                    'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage', 'attachment',
                    'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                ])
                ->where('reservation_code', $reservation_code)
                ->whereNull('material_id')
                ->first();

                $reservation_item = Reservation::with([
                    'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                    'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy',
                    'group_material', 'procurement_group', 'commitment_item', 'account', 'project',
                    'prefered_vendor'
                ])
                ->with('material.images')
                ->with('material.base_uom')
                ->whereNotNull('material_id')
                ->where('reservation_code', $reservation_code)
                ->orderBy('reservation_item', 'asc')
                ->get();

                // get on hand stock material
                $reservation_item->map(function($data){
                    $stock = StockMain::where('material_id', $data->material_id)
                        ->where('storage_id', $data->storage_id)
                        ->first();

                    $data->on_hand = $stock ? $stock->qty_unrestricted : 0;

                    // get material price from stock
                    $stock_inout = StockMain::where('material_id', $data->material_id)
                        ->where('storage_id', $data->inout_storage_id)
                        ->first();

                    // get price from info record
                    $info_record = MaterialVendor::where('material_id', $data->material_id)
                            ->where('storage_id', $data->inout_storage_id)
                            ->first();

                    if ($stock_inout) {//get price from stock
                        // $batch = $stock_inout->batch_stock->first();
                        $batch = StockBatch::where('stock_main_id', $stock_inout->id)
                            ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                            ->orderBy('id', 'asc')
                            ->first();

                        if ($batch) {
                            $unit_cost = $batch->unit_cost;
                        } else{
                            $unit_cost = 0;
                        }
                    } else if ($info_record) {//get price from info record
                        if ($info_record) {
                            $unit_cost = $info_record->unit_price;
                        } else {
                            $unit_cost = 0;
                        }
                    } else {//get price from material account
                        $mat_account = MaterialAccount::where('material_id', $data->material_id)
                            ->where('plant_id', $data->inout_plant_id)
                            ->first();

                        if ($mat_account) {
                            // cek material account type
                            if ($mat_account->price_controll) {
                                $unit_cost = $mat_account->standart_price;
                            } else {
                                $unit_cost = $mat_account->moving_price;
                            }
                        } else {
                            $unit_cost = 0;
                        }
                    }

                    $price = $unit_cost ? $unit_cost : 0;
                    $data->price = $price * $data->quantity_base_unit;
                });

                $reservation->total_amount = collect($reservation_item)->sum('price');

                return [
                    'reservation' => $reservation,
                    'reservation_item' => $reservation_item
                ];
                break;
            case 'A':
                $reservation = Reservation::with([
                    'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage', 'attachment',
                    'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                ])
                ->where('reservation_code', $reservation_code)
                ->whereNull('material_short_text')
                ->first();

                $reservation_item = Reservation::with([
                    'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                    'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy',
                    'group_material', 'procurement_group', 'commitment_item', 'account', 'project',
                    'prefered_vendor'
                ])
                ->with('material.images')
                ->with('material.base_uom')
                ->whereNotNull('material_short_text')
                ->where('reservation_code', $reservation_code)
                ->orderBy('reservation_item', 'asc')
                ->get();

                // get on hand stock material
                $reservation_item->map(function($data){
                    $stock = StockMain::where('material_id', $data->material_id)
                        ->where('storage_id', $data->storage_id)
                        ->first();

                    $data->on_hand = $stock ? $stock->qty_unrestricted : 0;

                    // get material price from stock
                    $stock_inout = StockMain::where('material_id', $data->material_id)
                        ->where('storage_id', $data->inout_storage_id)
                        ->first();

                    // get price from info record
                    $info_record = MaterialVendor::where('material_id', $data->material_id)
                            ->where('storage_id', $data->inout_storage_id)
                            ->first();

                    if ($stock_inout) {//get price from stock
                        // $batch = $stock_inout->batch_stock->first();
                        $batch = StockBatch::where('stock_main_id', $stock_inout->id)
                            ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                            ->orderBy('id', 'asc')
                            ->first();
                        if ($batch) {
                            $unit_cost = $batch->unit_cost;
                        } else{
                            $unit_cost = 0;
                        }
                    } else if ($info_record) {//get price from info record
                        if ($info_record) {
                            $unit_cost = $info_record->unit_price;
                        } else {
                            $unit_cost = 0;
                        }
                    } else {//get price from material account
                        $mat_account = MaterialAccount::where('material_id', $data->material_id)
                            ->where('plant_id', $data->inout_plant_id)
                            ->first();

                        if ($mat_account) {
                            // cek material account type
                            if ($mat_account->price_controll) {
                                $unit_cost = $mat_account->standart_price;
                            } else {
                                $unit_cost = $mat_account->moving_price;
                            }
                        } else {
                            $unit_cost = 0;
                        }
                    }

                    $price = $unit_cost ? $unit_cost : 0;
                    $data->price = $price * $data->quantity_base_unit;
                });

                $reservation->total_amount = collect($reservation_item)->sum('price');

                return [
                    'reservation' => $reservation,
                    'reservation_item' => $reservation_item
                ];
                break;
            default:
                return response()->json([
                    'message' => 'Reservation type '.$res_type.', will be handled soon',
                ], 422);
        }
    }

    public function getDraftReservation()
    {
        Auth::user()->cekRoleModules(['reservation-create']);
        
        // get draft reservation header
        $reservation = Reservation::with(['plant', 'storage'])
            ->with('storage.user')
            ->with('storage.location')
            ->where('status', 0)
            ->where('created_by', Auth::user()->id)
            ->where('document_type', 'R')
            ->whereNull('material_id')
            ->orderBy('id', 'asc')
            ->first();

        if (!$reservation) {
            return response()->json([
                'new_code' => $this->reservationCode(),
                'header_draft' => NULL
            ], 200);
        } else {
            $reservation->items = Reservation::with(['material', 'uom'])
                ->with('material.images')
                ->with('material.base_uom')
                ->whereNotNull('material_id')
                ->where('reservation_code', $reservation->reservation_code)
                ->orderBy('reservation_item', 'asc')
                ->get();

            $price = $reservation->items->map(function($data){
                // get material price from stock
                $stock_inout = StockMain::where('material_id', $data->material_id)
                    ->where('storage_id', $data->inout_storage_id)
                    ->first();

                // get price from info record
                $info_record = MaterialVendor::where('material_id', $data->material_id)
                        ->where('storage_id', $data->inout_storage_id)
                        ->first();

                if ($stock_inout) {//get price from stock
                    // $batch = $stock_inout->batch_stock->first();
                    $batch = StockBatch::where('stock_main_id', $stock_inout->id)
                        ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                        ->orderBy('id', 'asc')
                        ->first();
                    if ($batch) {
                        $unit_cost = $batch->unit_cost;
                    } else{
                        $unit_cost = 0;
                    }
                } else if ($info_record) {//get price from info record
                    if ($info_record) {
                        $unit_cost = $info_record->unit_price;
                    } else {
                        $unit_cost = 0;
                    }
                } else {//get price from material account
                    $mat_account = MaterialAccount::where('material_id', $data->material_id)
                        ->where('plant_id', $data->inout_plant_id)
                        ->first();

                    if ($mat_account) {
                        // cek material account type
                        if ($mat_account->price_controll) {
                            $unit_cost = $mat_account->standart_price;
                        } else {
                            $unit_cost = $mat_account->moving_price;
                        }
                    } else {
                        $unit_cost = 0;
                    }
                }

                $price = $unit_cost ? $unit_cost : 0;
                $data->price = $price * $data->quantity_base_unit;

                return $data;
            });

            $reservation->total_amount = collect($price)->sum('price');

            return response()->json([
                'new_code' => NULL,
                'header_draft' => $reservation
            ], 200);
        }
    }

    public function create(Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-create']);

        // validation
        $this->validate(request(), [
            'reservation_code' => 'required',
            'reservation_type' => 'required',//1. Replenishment, 2. Consumption, 3. Purchase
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            'requirement_date' => 'required|date',
            'note_header' => 'nullable',
        ]);

        // validate requirement date > global config PROPOSE_DELIV_DATE
        $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
        $date_req = Carbon::parse($request->requirement_date)->format('Y-m-d');

        if ($date_req < $date_config) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                ]
            ], 422);
        }

        // cek reservation type
        if ($request->reservation_type == 3) {
            $doc_type = 'A';
            $tipe_res = 'PR';
        } else {
            $doc_type = 'R';
            $tipe_res = 'Reservation';
        }

        // validate code
        $code = Reservation::where('reservation_code', $request->reservation_code)->first();
        if ($code) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'reservation_code'  => ['This '.$tipe_res.' code Already taken']
                ]
            ], 422);
        }

        // cek responsible person selected storage exist or not
        $storage = MasterStorage::find($request->storage_id);
        if (!$storage->user_id) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['This Storage '.$storage->description.' doesn\'t have supervisor, 
                    Please contact your system administrator']
                ]
            ], 422);
        }

        // cek stock determination for selected plant & storage
        $stockDetermination = StockDetermination::where('plant_id', $request->plant_id)
        ->where('storage_id', $request->storage_id)
        ->first();
        if (!$stockDetermination) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['This Storage '.$storage->description.' doesn\'t have Stock Determination']
                ]
            ], 422);
        }

        $reservation = new Reservation;
        $reservation->reservation_code = $request->reservation_code;
        $reservation->reservation_type = $request->reservation_type;
        $reservation->document_type = $doc_type;
        $reservation->plant_id = $request->plant_id;
        $reservation->storage_id = $request->storage_id;
        $reservation->requirement_date = $request->requirement_date;
        $reservation->note_header = $request->note_header;
        $reservation->status = 0;
        $reservation->created_by = Auth::user()->id;
        $reservation->updated_by = Auth::user()->id;
        $save = $reservation->save();

        if ($save) {
            if ($request->reservation_type == 3) {
                // update last pr code
                $code = TransactionCode::where('code', 'A')->first();

                $code->update(['lastcode' => $request->reservation_code]);
            } else {
                // update last reservation code
                $code = TransactionCode::where('code', 'R')->first();

                $code->update(['lastcode' => $request->reservation_code]);
            }
            return Reservation::with('plant')->find($reservation->id);
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function addItemLine($reservation_code, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-create']);

        $header = Reservation::where('reservation_code', $reservation_code)
            ->whereNull('material_id')
            ->first();

        if (!$header) {
            return response()->json([
                'message' => 'Reservation '.$reservation_code.', Not Found',
            ], 404);
        }

        $doc_type = $header->document_type;
        
        if ($doc_type == 'R') {
            $res_type = 'Reservation';
        } elseif ($doc_type == 'A') {
            $res_type = 'Purchase Requisition';
        }

        // cek status must draft
        if ($header->status != 0) {
            return response()->json([
                'message' => 'Can\'t edit '.$res_type.', status not draft',
            ], 422);
        }

        switch ($doc_type) {
            case 'R':
                // validation
                $this->validate(request(), [
                    'item' => 'array',
                    'item.*.requirement_date' => 'nullable|date',
                    'item.*.item_type' => 'nullable|boolean',
                    'item.*.material_id' => 'required|exists:materials,id',
                    'item.*.material_short_text' => 'nullable',
                    'item.*.note_item' => 'nullable',
                    'item.*.batch' => 'nullable|exists:stock_batches,batch_number',
                    'item.*.quantity' => 'required|numeric|min:1',
                    'item.*.uom_id' => 'required|exists:uoms,id',
                    'item.*.unit_cost' => 'nullable',
                    'item.*.inout_plant_id' => 'required|exists:plants,id',
                    'item.*.inout_storage_id' => 'required|exists:storages,id',
                ]);

                // validate duplicate material
                foreach ($request->item as $current_key => $current_array) {
                    foreach ($request->item as $search_key => $search_array) {
                        if ($search_array['material_id'] == $current_array['material_id']) {
                            if ($search_key != $current_key) {
                                return response()->json([
                                    'message'   => 'Data invalid',
                                    'errors'    => [
                                        'item.'.$search_key.'.material_id'  => ['Can not enter the same material code more than once']
                                    ]
                                ], 422);
                            }
                        }
                    }
                }

                foreach ($request->item as $key => $val) {
                    $material = Material::find($val['material_id']);

                    // validate duplicate material
                    $dups_mat = Reservation::where('reservation_code', $reservation_code)
                        ->where('material_id', $val['material_id'])
                        ->first();

                    if ($dups_mat) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' Already exists in reservation']
                            ]
                        ], 422);
                    }

                    // validate material plant
                    $material_plant = MaterialPlant::where('plant_id', $header->plant_id)
                        ->where('material_id', $val['material_id'])
                        ->whereNotNull('proc_group_id')
                        ->first();

                    $plant = Plant::find($header->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    // validate requirement date > global config PROPOSE_DELIV_DATE
                    $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
                    if (isset($val['requirement_date'])) {
                        $date_req = Carbon::parse($val['requirement_date'])->format('Y-m-d');
                    } else {
                        $date_req = Carbon::parse($header->requirement_date)->format('Y-m-d');
                    }

                    if ($date_req < $date_config) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                            ]
                        ], 422);
                    }
                }

                break;
            case 'A':
                // validation
                // item type
                // 0. material for stock
                // 1. free text / material for cost center, jika tipe ini wajib mengisi cost center pada
                // 2. material for asset
                $this->validate(request(), [
                    'item' => 'array',
                    'item.*.requirement_date' => 'nullable|date',
                    'item.*.item_type' => 'nullable|numeric|min:0|max:2',
                    'item.*.note_item' => 'nullable',
                    'item.*.batch' => 'nullable|exists:stock_batches,batch_number',
                    'item.*.quantity' => 'required|numeric|min:1',
                    'item.*.unit_cost' => 'nullable',
                    'item.*.inout_plant_id' => 'nullable|exists:plants,id',
                    'item.*.inout_storage_id' => 'nullable|exists:storages,id',
                ]);

                foreach ($request->item as $key => $val) {
                    // validate free material
                    if ($val['item_type'] == 1) {
                        if (!isset($val['material_id'])) {
                            $this->validate(request(), [
                                'item.'.$key.'.material_short_text' => 'required',
                                'item.'.$key.'.uom_id' => 'required|exists:uoms,id',
                            ]);
                        }
                    }

                    if ($val['item_type'] == 0 | $val['item_type'] == '' | $val['item_type'] == 2) {
                        $this->validate(request(), [
                            'item.'.$key.'.material_id' => 'required|exists:materials,id',
                            'item.'.$key.'.uom_id' => 'required|exists:uoms,id',
                        ]);
                    }

                    // validate requirement date > global config PROPOSE_DELIV_DATE
                    $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
                    if (isset($val['requirement_date'])) {
                        $date_req = Carbon::parse($val['requirement_date'])->format('Y-m-d');
                    } else {
                        $date_req = Carbon::parse($header->requirement_date)->format('Y-m-d');
                    }

                    if ($date_req < $date_config) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                            ]
                        ], 422);
                    }
                }
                break;
            default:
                return response()->json([
                    'message' => 'Reservation type '.$res_type.', will be handled soon',
                ], 422);
        }

        // save
        try {
            DB::beginTransaction();

            switch ($doc_type) {
                case 'R':
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        // find material plant
                        $mrp = MaterialPlant::where('material_id', $val['material_id'])
                            ->where('plant_id', $header->plant_id)
                            ->whereNotNull('proc_group_id')
                            ->first();

                        // find material
                        $mat = Material::find($val['material_id']);
                        $matgroup = GroupMaterial::find($mat->group_material_id);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($mat->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity']);                

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                            }
                        }

                        // insert new reservation
                        $reservation = new Reservation;
                        $reservation->reservation_code = $header->reservation_code;
                        $reservation->reservation_item = $item++;
                        $reservation->reservation_type = $header->reservation_type;
                        $reservation->document_type = $header->document_type;
                        $reservation->note_header = $header->note_header;
                        $reservation->status = $header->status;
                        $reservation->plant_id = $header->plant_id;
                        $reservation->storage_id = $header->storage_id;
                        $reservation->item_type = isset($val['item_type']) ? $val['item_type'] : null;
                        $reservation->material_id = $val['material_id'];
                        $reservation->material_short_text = isset($val['material_short_text']) ? $val['material_short_text'] : $mat->description;
                        $reservation->note_item = isset($val['note_item']) ? $val['note_item'] : null;
                        $reservation->batch = isset($val['batch']) ? $val['batch'] : null;
                        $reservation->requirement_date = isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date;
                        $reservation->quantity = $val['quantity'];
                        $reservation->uom_id = $val['uom_id'];
                        $reservation->quantity_base_unit = $convert_qty;
                        $reservation->uom_base_id = $mat->uom_id;
                        $reservation->group_material_id = $matgroup ? $matgroup->id : null;
                        $reservation->account_id = $matgroup ? $matgroup->account_id : null;
                        $reservation->unit_cost = isset($val['unit_cost']) ? $val['unit_cost'] : null;
                        $reservation->inout_plant_id = $val['inout_plant_id'];
                        $reservation->inout_storage_id = $val['inout_storage_id'];
                        $reservation->material_plant_id = $mrp ? $mrp->id : null;
                        $reservation->proc_type = $mrp ? $mrp->proc_type : null;
                        $reservation->proc_group_id = $mrp ? $mrp->proc_group_id : null;
                        $reservation->created_by = Auth::user()->id;
                        $reservation->updated_by = Auth::user()->id;
                        $reservation->save();
                    }
                    break;
                case 'A':
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        // for pr with material
                        if (isset($val['material_id'])) {
                            // find material plant
                            $mrp = MaterialPlant::where('material_id', $val['material_id'])
                                ->where('plant_id', $header->plant_id)
                                ->whereNotNull('proc_group_id')
                                ->first();

                            // find material
                            $mat = Material::find($val['material_id']);
                            $matgroup = GroupMaterial::find($mat->group_material_id);

                            // uom entry
                            $unit_entry = Uom::find($val['uom_id']);

                            // convert uom
                            if ($mat->uom_id == $val['uom_id']) {
                                $convert_qty = $val['quantity'];
                            } else {
                                $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity']);                

                                if (!$convert_qty) {
                                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                                }
                            }

                            // insert new reservation
                            $reservation = new Reservation;
                            $reservation->reservation_code = $header->reservation_code;
                            $reservation->reservation_item = $item++;
                            $reservation->reservation_type = $header->reservation_type;
                            $reservation->document_type = $header->document_type;
                            $reservation->note_header = $header->note_header;
                            $reservation->status = $header->status;
                            $reservation->plant_id = $header->plant_id;
                            $reservation->storage_id = $header->storage_id;
                            $reservation->item_type = isset($val['item_type']) ? $val['item_type'] : null;
                            $reservation->material_id = $val['material_id'];
                            $reservation->material_short_text = isset($val['material_short_text']) ? $val['material_short_text'] : $mat->description;
                            $reservation->note_item = isset($val['note_item']) ? $val['note_item'] : null;
                            $reservation->batch = isset($val['batch']) ? $val['batch'] : null;
                            $reservation->requirement_date = isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date;
                            $reservation->quantity = $val['quantity'];
                            $reservation->uom_id = $val['uom_id'];
                            $reservation->quantity_base_unit = $convert_qty;
                            $reservation->uom_base_id = $mat->uom_id;
                            $reservation->group_material_id = $matgroup ? $matgroup->id : null;
                            $reservation->account_id = $matgroup ? $matgroup->account_id : null;
                            $reservation->unit_cost = isset($val['unit_cost']) ? $val['unit_cost'] : null;
                            $reservation->inout_plant_id = $header->plant_id;
                            $reservation->inout_storage_id = $header->storage_id;
                            $reservation->material_plant_id = $mrp ? $mrp->id : null;
                            $reservation->proc_type = 0;
                            $reservation->proc_group_id = $mrp ? $mrp->proc_group_id : null;
                            $reservation->created_by = Auth::user()->id;
                            $reservation->updated_by = Auth::user()->id;
                            $reservation->save();
                        } else {// for pr with free text
                            // insert new reservation
                            $reservation = new Reservation;
                            $reservation->reservation_code = $header->reservation_code;
                            $reservation->reservation_item = $item++;
                            $reservation->reservation_type = $header->reservation_type;
                            $reservation->document_type = $header->document_type;
                            $reservation->note_header = $header->note_header;
                            $reservation->status = $header->status;
                            $reservation->plant_id = $header->plant_id;
                            $reservation->storage_id = $header->storage_id;
                            $reservation->item_type = isset($val['item_type']) ? $val['item_type'] : null;
                            $reservation->material_id = null;
                            $reservation->material_short_text = isset($val['material_short_text']) ? $val['material_short_text'] : $mat->description;
                            $reservation->note_item = isset($val['note_item']) ? $val['note_item'] : null;
                            $reservation->batch = isset($val['batch']) ? $val['batch'] : null;
                            $reservation->requirement_date = isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date;
                            $reservation->quantity = $val['quantity'];
                            $reservation->uom_id = $val['uom_id'];
                            $reservation->quantity_base_unit = $val['quantity'];
                            $reservation->uom_base_id = $val['uom_id'];
                            $reservation->group_material_id = null;
                            $reservation->account_id = null;
                            $reservation->unit_cost = isset($val['unit_cost']) ? $val['unit_cost'] : null;
                            $reservation->inout_plant_id = $header->plant_id;
                            $reservation->inout_storage_id = $header->storage_id;
                            $reservation->material_plant_id = null;
                            $reservation->proc_type = 0;
                            $reservation->proc_group_id = null;
                            $reservation->created_by = Auth::user()->id;
                            $reservation->updated_by = Auth::user()->id;
                            $reservation->save();
                        }                        
                    }
                    break;
                default:
                    return response()->json([
                        'message' => 'Reservation type '.$res_type.', will be handled soon',
                    ], 422);
            }

            Reservation::where('reservation_code', $reservation_code)->update([
                'updated_by' => Auth::user()->id
            ]);

            DB::commit();

            switch ($doc_type) {
                case 'R':
                    $reservation_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->where('reservation_code', $header->reservation_code)
                    ->whereNull('material_id')
                    ->first();

                    $reservation_item_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->with('material.images')
                    ->with('material.base_uom')
                    ->whereNotNull('material_id')
                    ->where('reservation_code', $header->reservation_code)
                    ->orderBy('reservation_item', 'asc')
                    ->get();

                    $price = $reservation_item_saved->map(function($data){
                        // get material price from stock
                        $stock_inout = StockMain::where('material_id', $data->material_id)
                            ->where('storage_id', $data->inout_storage_id)
                            ->first();

                        // get price from info record
                        $info_record = MaterialVendor::where('material_id', $data->material_id)
                                ->where('storage_id', $data->inout_storage_id)
                                ->first();

                        if ($stock_inout) {//get price from stock
                            // $batch = $stock_inout->batch_stock->first();
                            $batch = StockBatch::where('stock_main_id', $stock_inout->id)
                                ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                                ->orderBy('id', 'asc')
                                ->first();
                            if ($batch) {
                                $unit_cost = $batch->unit_cost;
                            } else{
                                $unit_cost = 0;
                            }
                        } else if ($info_record) {//get price from info record
                            if ($info_record) {
                                $unit_cost = $info_record->unit_price;
                            } else {
                                $unit_cost = 0;
                            }
                        } else {//get price from material account
                            $mat_account = MaterialAccount::where('material_id', $data->material_id)
                                ->where('plant_id', $data->inout_plant_id)
                                ->first();

                            if ($mat_account) {
                                // cek material account type
                                if ($mat_account->price_controll) {
                                    $unit_cost = $mat_account->standart_price;
                                } else {
                                    $unit_cost = $mat_account->moving_price;
                                }
                            } else {
                                $unit_cost = 0;
                            }
                        }

                        $price = $unit_cost ? $unit_cost : 0;
                        $data->price = $price * $data->quantity_base_unit;

                        return $data;
                    });

                    $reservation_saved->total_amount = collect($price)->sum('price');

                    $saveitem = [
                        'reservation' => $reservation_saved,
                        'reservation_item' => $reservation_item_saved
                    ];

                    return $saveitem;
                    
                    break;
                case 'A':
                    $reservation_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->where('reservation_code', $header->reservation_code)
                    ->whereNull('material_short_text')
                    ->first();

                    $reservation_item_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->with('material.images')
                    ->with('material.base_uom')
                    ->whereNotNull('material_short_text')
                    ->where('reservation_code', $header->reservation_code)
                    ->orderBy('reservation_item', 'asc')
                    ->get();

                    $saveitem = [
                        'reservation' => $reservation_saved,
                        'reservation_item' => $reservation_item_saved
                    ];

                    return $saveitem;
                    
                    break;            
            }
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while creating '.$res_type,
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function updateItemLine($reservation_code, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        $header = Reservation::where('reservation_code', $reservation_code)
            ->whereNull('material_id')
            ->first();

        if (!$header) {
            return response()->json([
                'message' => 'Reservation '.$reservation_code.', Not Found',
            ], 404);
        }

        $doc_type = $header->document_type;
        
        if ($doc_type == 'R') {
            $res_type = 'Reservation';
        } elseif ($doc_type == 'A') {
            $res_type = 'Purchase Requisition';
        }

        // cek status must draft
        if ($header->status != 0) {
            return response()->json([
                'message' => 'Can\'t edit '.$res_type.', status not draft',
            ], 422);
        }

        switch ($doc_type) {
            case 'R':
                // validation
                $this->validate(request(), [
                    'item' => 'array',
                    'item.*.requirement_date' => 'nullable|date',
                    'item.*.item_type' => 'nullable|boolean',
                    'item.*.material_id' => 'required|exists:materials,id',
                    'item.*.material_short_text' => 'nullable',
                    'item.*.note_item' => 'nullable',
                    'item.*.batch' => 'nullable',
                    'item.*.quantity' => 'required|numeric|min:1',
                    'item.*.uom_id' => 'required|exists:uoms,id',
                    'item.*.unit_cost' => 'nullable',
                    'item.*.inout_plant_id' => 'required|exists:plants,id',
                    'item.*.inout_storage_id' => 'required|exists:storages,id',
                ]);

                // validate duplicate material
                foreach ($request->item as $current_key => $current_array) {
                    foreach ($request->item as $search_key => $search_array) {
                        if ($search_array['material_id'] == $current_array['material_id']) {
                            if ($search_key != $current_key) {
                                return response()->json([
                                    'message'   => 'Data invalid',
                                    'errors'    => [
                                        'item.'.$search_key.'.material_id'  => ['Can not enter the same material code more than once']
                                    ]
                                ], 422);
                            }
                        }
                    }
                }

                // validate material plant
                foreach ($request->item as $key => $val) {
                    $material = Material::find($val['material_id']);

                    $material_plant = MaterialPlant::where('plant_id', $header->plant_id)
                    ->where('material_id', $val['material_id'])->whereNotNull('proc_group_id')->first();

                    $plant = Plant::find($header->plant_id);
                    
                    if (!$material_plant) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.material_id'  => ['Material '.$material->description.' not have material plant '.$plant->code.'']
                            ]
                        ], 422);
                    }

                    // validate requirement date > global config PROPOSE_DELIV_DATE
                    $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
                    if (isset($val['requirement_date'])) {
                        $date_req = Carbon::parse($val['requirement_date'])->format('Y-m-d');
                    } else {
                        $date_req = Carbon::parse($header->requirement_date)->format('Y-m-d');
                    }

                    if ($date_req < $date_config) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                            ]
                        ], 422);
                    }
                }

                // get mateial from selected reservation
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNotNull('material_id')
                    ->pluck('material_id')
                    ->toArray();

                // get material from request
                $req_mat = collect($request->item)->pluck('material_id')->toArray();

                // compare material id with material id in this reservation
                $unused_mat = array_diff($reservation, $req_mat);
                $new_mat = array_diff($req_mat, $reservation);

                break;
            case 'A':
                // validation
                // item type
                // 0. material for stock
                // 1. free text / material for cost center, jika tipe ini wajib mengisi cost center pada
                // 2. material for asset
                $this->validate(request(), [
                    'item' => 'array',
                    'item.*.requirement_date' => 'nullable|date',
                    'item.*.item_type' => 'nullable|numeric|min:0|max:2',
                    'item.*.note_item' => 'nullable',
                    'item.*.batch' => 'nullable|exists:stock_batches,batch_number',
                    'item.*.quantity' => 'required|numeric|min:1',
                    'item.*.unit_cost' => 'nullable',
                    'item.*.inout_plant_id' => 'nullable|exists:plants,id',
                    'item.*.inout_storage_id' => 'nullable|exists:storages,id',
                ]);

                $mat_text = [];

                foreach ($request->item as $key => $val) {
                    // validate free material
                    if ($val['item_type'] == 1) {
                        if (!isset($val['material_id'])) {
                            $this->validate(request(), [
                                'item.'.$key.'.material_short_text' => 'required',
                                'item.'.$key.'.uom_id' => 'required|exists:uoms,id',
                            ]);
                        }

                        $mat_text[] = isset($val['material_short_text']) ? $val['material_short_text'] : Material::find($val['material_id'])->description;
                    }

                    if ($val['item_type'] == 0 | $val['item_type'] == '' | $val['item_type'] == 2) {
                        $this->validate(request(), [
                            'item.'.$key.'.material_id' => 'required|exists:materials,id',
                            'item.'.$key.'.uom_id' => 'required|exists:uoms,id',
                        ]);

                        // $mat_text[] = Material::find($val['material_id'])->description;
                        $mat_text[] = isset($val['material_short_text']) ? $val['material_short_text'] : Material::find($val['material_id'])->description;
                    }

                    // validate requirement date > global config PROPOSE_DELIV_DATE
                    $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
                    if (isset($val['requirement_date'])) {
                        $date_req = Carbon::parse($val['requirement_date'])->format('Y-m-d');
                    } else {
                        $date_req = Carbon::parse($header->requirement_date)->format('Y-m-d');
                    }

                    if ($date_req < $date_config) {
                        return response()->json([
                            'message'   => 'Data invalid',
                            'errors'    => [
                                'item.'.$key.'.requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                            ]
                        ], 422);
                    }
                }

                // get mateial from selected reservation
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNotNull('material_short_text')
                    ->pluck('material_short_text')
                    ->toArray();

                // get material from request
                $req_mat = collect($mat_text)->toArray();

                // validate duplicate material
                if (count($req_mat) != count(array_unique($req_mat))) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'material_id'  => ['Can not enter the same material description more than once']
                        ]
                    ], 422);
                }

                // compare material id with material id in this reservation
                $unused_mat = array_diff($reservation, $req_mat);
                $new_mat = array_diff($req_mat, $reservation);

                break;
            default:
                return response()->json([
                    'message' => 'Reservation type '.$res_type.', will be handled soon',
                ], 422);
        }

        try {
            DB::beginTransaction();

            switch ($doc_type) {
                case 'R':
                    // update or create reservation item
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        // find material plant                    
                        $mrp = MaterialPlant::where('material_id', $val['material_id'])
                            ->where('plant_id', $header->plant_id)
                            ->whereNotNull('proc_group_id')
                            ->first();

                        // find material
                        $mat = Material::find($val['material_id']);
                        $matgroup = GroupMaterial::find($mat->group_material_id);

                        // uom entry
                        $unit_entry = Uom::find($val['uom_id']);

                        // convert uom
                        if ($mat->uom_id == $val['uom_id']) {
                            $convert_qty = $val['quantity'];
                        } else {
                            $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity']);                

                            if (!$convert_qty) {
                                throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                            }
                        }

                        // update or create
                        $reservation = Reservation::updateOrCreate(
                            [
                                'reservation_code' => $header->reservation_code,
                                'material_id'   => $val['material_id']
                            ],
                            [
                                'material_short_text' => isset($val['material_short_text']) ? $val['material_short_text'] : $mat->description,
                                'reservation_code' => $header->reservation_code,
                                'reservation_item' => $item++,
                                'reservation_type' => $header->reservation_type,
                                'document_type' => $header->document_type,
                                'note_header' => $header->note_header,
                                'status' => $header->status,
                                'plant_id' => $header->plant_id,
                                'storage_id' => $header->storage_id,
                                'item_type' => isset($val['item_type']) ? $val['item_type'] : null,
                                'material_id' => $val['material_id'],
                                'note_item' => isset($val['note_item']) ? $val['note_item'] : null,
                                'batch' => isset($val['batch']) ? $val['batch'] : null,
                                'requirement_date' => isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date,
                                'quantity' => $val['quantity'],
                                'uom_id' => $val['uom_id'],
                                'quantity_base_unit' => $convert_qty,
                                'uom_base_id' => $mat->uom_id,
                                'group_material_id' => $matgroup ? $matgroup->id : null,
                                'account_id' => $matgroup ? $matgroup->account_id : null,
                                'unit_cost' => isset($val['unit_cost']) ? $val['unit_cost'] : null,
                                'inout_plant_id' => $val['inout_plant_id'],
                                'inout_storage_id' => $val['inout_storage_id'],
                                'material_plant_id' => $mrp ? $mrp->id : null,
                                'proc_type' => $mrp ? $mrp->proc_type : null,
                                'proc_group_id' => $mrp ? $mrp->proc_group_id : null,
                                'created_by' => Auth::user()->id,
                                'updated_by' => Auth::user()->id,
                            ]
                        );
                    }

                    // delete unsed item based unused material id
                    foreach ($unused_mat as $mat_id) {
                        $del_res = Reservation::where('material_id', $mat_id)
                            ->where('reservation_code', $header->reservation_code)
                            ->first();

                        if ($del_res) {
                            $del_res->delete();
                        }
                    }

                    break;
                case 'A':
                    // update or create reservation item
                    $item = 1;
                    foreach ($request->item as $key => $val) {
                        // for pr with material
                        if (isset($val['material_id'])) {
                            // find material plant                    
                            $mrp = MaterialPlant::where('material_id', $val['material_id'])
                                ->where('plant_id', $header->plant_id)
                                ->whereNotNull('proc_group_id')
                                ->first();

                            // find material
                            $mat = Material::find($val['material_id']);
                            $matgroup = GroupMaterial::find($mat->group_material_id);

                            // uom entry
                            $unit_entry = Uom::find($val['uom_id']);

                            // convert uom
                            if ($mat->uom_id == $val['uom_id']) {
                                $convert_qty = $val['quantity'];
                            } else {
                                $convert_qty = uomconversion($val['material_id'], $val['uom_id'], $val['quantity']);                

                                if (!$convert_qty) {
                                    throw new \Exception('Convert uom error, check uom '.$unit_entry->code.' exists on material '.$mat->code.' unit conversion');
                                }
                            }

                            // update or create
                            $reservation = Reservation::updateOrCreate(
                                [
                                    'reservation_code' => $header->reservation_code,
                                    'material_id' => $val['material_id'],
                                ],
                                [
                                    'material_short_text' => isset($val['material_short_text']) ? $val['material_short_text'] : $mat->description,
                                    'reservation_code' => $header->reservation_code,
                                    'reservation_item' => $item++,
                                    'reservation_type' => $header->reservation_type,
                                    'document_type' => $header->document_type,
                                    'note_header' => $header->note_header,
                                    'status' => $header->status,
                                    'plant_id' => $header->plant_id,
                                    'storage_id' => $header->storage_id,
                                    'item_type' => isset($val['item_type']) ? $val['item_type'] : null,
                                    'material_id' => $val['material_id'],
                                    'note_item' => isset($val['note_item']) ? $val['note_item'] : null,
                                    'batch' => isset($val['batch']) ? $val['batch'] : null,
                                    'requirement_date' => isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date,
                                    'quantity' => $val['quantity'],
                                    'uom_id' => $val['uom_id'],
                                    'quantity_base_unit' => $convert_qty,
                                    'uom_base_id' => $mat->uom_id,
                                    'group_material_id' => $matgroup ? $matgroup->id : null,
                                    'account_id' => $matgroup ? $matgroup->account_id : null,
                                    'inout_plant_id' => $header->plant_id,
                                    'inout_storage_id' => $header->storage_id,
                                    'material_plant_id' => $mrp ? $mrp->id : null,
                                    'proc_type' => 0,
                                    'proc_group_id' => $mrp ? $mrp->proc_group_id : null,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                ]
                            );
                        } else {// for pr with free text
                            // update or create
                            $reservation = Reservation::updateOrCreate(
                                [
                                    'reservation_code' => $header->reservation_code,
                                    'material_short_text' => $val['material_short_text'],
                                ],
                                [
                                    'reservation_code' => $header->reservation_code,
                                    'reservation_item' => $item++,
                                    'reservation_type' => $header->reservation_type,
                                    'document_type' => $header->document_type,
                                    'note_header' => $header->note_header,
                                    'status' => $header->status,
                                    'plant_id' => $header->plant_id,
                                    'storage_id' => $header->storage_id,
                                    'item_type' => isset($val['item_type']) ? $val['item_type'] : null,
                                    'material_id' => null,
                                    'note_item' => isset($val['note_item']) ? $val['note_item'] : null,
                                    'batch' => isset($val['batch']) ? $val['batch'] : null,
                                    'requirement_date' => isset($val['requirement_date']) ? $val['requirement_date'] : $header->requirement_date,
                                    'quantity' => $val['quantity'],
                                    'uom_id' => $val['uom_id'],
                                    'quantity_base_unit' => $val['quantity'],
                                    'uom_base_id' => $val['uom_id'],
                                    'inout_plant_id' => $header->plant_id,
                                    'inout_storage_id' => $header->storage_id,
                                    'created_by' => Auth::user()->id,
                                    'updated_by' => Auth::user()->id,
                                ]
                            );
                        }
                    }

                    // delete unsed item based unused material id
                    foreach ($unused_mat as $mat_id) {
                        $del_pr = Reservation::where('material_short_text', $mat_id)
                            ->where('reservation_code', $header->reservation_code)
                            ->first();

                        if ($del_pr) {
                            $del_pr->delete();
                        }
                    }

                    break;
                default:
                    return response()->json([
                        'message' => 'Reservation type '.$res_type.', will be handled soon',
                    ], 422);
            }

            Reservation::where('reservation_code', $reservation_code)->update([
                'updated_by' => Auth::user()->id
            ]);

            DB::commit();

            switch ($doc_type) {
                case 'R':
                    $reservation_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->where('reservation_code', $header->reservation_code)
                    ->whereNull('material_id')
                    ->first();

                    $reservation_item_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->with('material.images')
                    ->with('material.base_uom')
                    ->whereNotNull('material_id')
                    ->where('reservation_code', $header->reservation_code)
                    ->orderBy('reservation_item', 'asc')
                    ->get();

                    $price = $reservation_item_saved->map(function($data){
                        // get material price from stock
                        $stock_inout = StockMain::where('material_id', $data->material_id)
                            ->where('storage_id', $data->inout_storage_id)
                            ->first();

                        // get price from info record
                        $info_record = MaterialVendor::where('material_id', $data->material_id)
                                ->where('storage_id', $data->inout_storage_id)
                                ->first();

                        if ($stock_inout) {//get price from stock
                            // $batch = $stock_inout->batch_stock->first();
                            $batch = StockBatch::where('stock_main_id', $stock_inout->id)
                                ->where(DB::raw('CAST(qty_unrestricted AS double precision)'), '!=', 0)
                                ->orderBy('id', 'asc')
                                ->first();
                            if ($batch) {
                                $unit_cost = $batch->unit_cost;
                            } else{
                                $unit_cost = 0;
                            }
                        } else if ($info_record) {//get price from info record
                            if ($info_record) {
                                $unit_cost = $info_record->unit_price;
                            } else {
                                $unit_cost = 0;
                            }
                        } else {//get price from material account
                            $mat_account = MaterialAccount::where('material_id', $data->material_id)
                                ->where('plant_id', $data->inout_plant_id)
                                ->first();

                            if ($mat_account) {
                                // cek material account type
                                if ($mat_account->price_controll) {
                                    $unit_cost = $mat_account->standart_price;
                                } else {
                                    $unit_cost = $mat_account->moving_price;
                                }
                            } else {
                                $unit_cost = 0;
                            }
                        }

                        $price = $unit_cost ? $unit_cost : 0;
                        $data->price = $price * $data->quantity_base_unit;

                        return $data;
                    });

                    $reservation_saved->total_amount = collect($price)->sum('price');

                    $saveitem = [
                        'reservation' => $reservation_saved,
                        'reservation_item' => $reservation_item_saved
                    ];

                    return $saveitem;
                    
                    break;
                case 'A':
                    $reservation_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->where('reservation_code', $header->reservation_code)
                    ->whereNull('material_short_text')
                    ->first();

                    $reservation_item_saved = Reservation::with([
                        'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
                        'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'createdBy', 'updatedBy'
                    ])
                    ->with('material.images')
                    ->with('material.base_uom')
                    ->whereNotNull('material_short_text')
                    ->where('reservation_code', $header->reservation_code)
                    ->orderBy('reservation_item', 'asc')
                    ->get();

                    $saveitem = [
                        'reservation' => $reservation_saved,
                        'reservation_item' => $reservation_item_saved
                    ];

                    return $saveitem;
                    
                    break;            
            }
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while updating reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function reservationAccountAssignment($id, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-create']);

        // validation
        $this->validate(request(), [
            // 'profit_center_id' => 'nullable|exists:profit_centers,id',
            'cost_center_id' => 'nullable|exists:cost_centers,id',
            'project_id' => 'nullable|exists:projects,id',
            'commitment_item_id' => 'nullable|exists:budgets,id',
        ]);

        $reservation = Reservation::find($id);

        // cek if pr & cost center type
        if ($reservation->item_type == 1) {
            $this->validate(request(), [
                'cost_center_id' => 'required|exists:cost_centers,id',
                'group_material_id' => 'required|exists:group_materials,id',
            ]);
        }

        // $reservation->profit_center_id = $request->profit_center_id;
        $reservation->cost_center_id = $request->cost_center_id;
        $reservation->project_id = $request->project_id;
        $reservation->commitment_item_id = $request->commitment_item_id;
        $reservation->group_material_id = $request->group_material_id ? $request->group_material_id : $reservation->group_material_id;
        $reservation->updated_by = Auth::user()->id;
        $save = $reservation->save();

        Reservation::where('reservation_code', $reservation->reservation_code)->update([
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $reservation;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function reservationPurchasing($id, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-create']);

        $reservation = Reservation::find($id);

        $doc_type = $reservation->document_type;
        
        if ($doc_type == 'R') {
            $res_type = 'Reservation';
        } elseif ($doc_type == 'A') {
            $res_type = 'Purchase Requisition';
        }

        switch ($doc_type) {
            case 'R':
                // validation
                $this->validate(request(), [
                    'purc_req_number' => 'nullable|numeric',
                    'purc_req_item' => 'nullable|numeric',
                    'po_number' => 'nullable|numeric',
                    'po_item_no' => 'nullable|numeric',
                ]);

                $reservation->purc_req_number = $request->purc_req_number;
                $reservation->purc_req_item = $request->purc_req_item;
                $reservation->po_number = $request->po_number;
                $reservation->po_item_no = $request->po_item_no;
                $reservation->updated_by = Auth::user()->id;
                $save = $reservation->save();
                break;
            case 'A':
                // validation
                $this->validate(request(), [
                    // 'unit_cost' => 'required|numeric|min:1',
                    'currency' => 'nullable',
                    'prefered_vendor_id' => 'nullable|exists:suppliers,id',
                    'proc_type' => 'nullable|boolean',
                    'proc_group_id' => 'nullable|exists:procurement_groups,id',
                ]);

                // for pr free text
                if ($reservation->item_type == 1) {
                    $reservation->unit_cost = $request->unit_cost;
                    $reservation->currency = $request->currency;
                    $reservation->prefered_vendor_id = $request->prefered_vendor_id;
                    $reservation->proc_type = $request->proc_type;
                    $reservation->proc_group_id = $request->proc_group_id;
                    $reservation->updated_by = Auth::user()->id;
                    $save = $reservation->save();
                }

                // for pr with material, pr with cost center
                if ($reservation->item_type == 0 | $reservation->item_type == '' | $reservation->item_type == 2) {
                    $reservation->unit_cost = $request->unit_cost;
                    $reservation->currency = $request->currency;
                    $reservation->prefered_vendor_id = $request->prefered_vendor_id;
                    $reservation->proc_type = isset($request->proc_type) ? $request->proc_type : $reservation->proc_type;
                    $reservation->proc_group_id = $request->proc_group_id ? $request->proc_group_id : $reservation->proc_group_id;
                    $reservation->updated_by = Auth::user()->id;
                    $save = $reservation->save();
                }

                break;
            default:
                return response()->json([
                    'message' => 'Reservation type '.$res_type.', will be handled soon',
                ], 422);
        }

        Reservation::where('reservation_code', $reservation->reservation_code)->update([
            'updated_by' => Auth::user()->id
        ]);

        if ($save) {
            return $reservation;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function reservationProduction($id, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-create']);

        // validation
        $this->validate(request(), [
            'order_number' => 'nullable|numeric',
            'planned_order' => 'nullable|numeric',
            'planned_order_item' => 'nullable|numeric',
        ]);

        $reservation = Reservation::find($id);
        $reservation->order_number = $request->order_number;
        $reservation->planned_order = $request->planned_order;
        $reservation->planned_order_item = $request->planned_order_item;
        $reservation->updated_by = Auth::user()->id;
        $save = $reservation->save();

        if ($save) {
            return $reservation;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function updateReservationHeader($reservation_code, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        // validation
        $this->validate(request(), [
            // 'reservation_type' => 'required',
            'note_header' => 'nullable',
            'plant_id' => 'required|exists:plants,id',
            'storage_id' => 'required|exists:storages,id',
            'requirement_date' => 'required|date',
        ]);

        // validate requirement date > global config PROPOSE_DELIV_DATE
        $date_config = Carbon::now()->addDays(appsetting('PROPOSE_DELIV_DATE'))->format('Y-m-d');
        $date_req = Carbon::parse($request->requirement_date)->format('Y-m-d');

        if ($date_req < $date_config) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'requirement_date'  => ['requirement_date must greater than or equal with '.$date_config.'']
                ]
            ], 422);
        }

        $header = Reservation::where('reservation_code', $reservation_code)
            ->whereNull('material_id')
            ->first();

        if ($header->status == 1) {
            return response()->json([
                'message'   => 'Reservation Already Submited, cant edit anymore',
            ], 422);
        }

        // cek responsible person selected storage exist or not
        $storage = MasterStorage::find($request->storage_id);
        if (!$storage->user_id) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'storage_id'  => ['This Storage '.$storage->description.' doesn\'t have supervisor, 
                    Please contact your system administrator']
                ]
            ], 422);
        }

        // get all data with same reservation code
        $data = Reservation::where('reservation_code', $reservation_code)->pluck('id');

        // save
        try {
            DB::beginTransaction();

            $item = 1;
            foreach ($data as $id) {
                $reservation = Reservation::find($id);
                // $reservation->reservation_type = $request->reservation_type;
                $reservation->plant_id = $request->plant_id;
                $reservation->storage_id = $request->storage_id;
                $reservation->requirement_date = $request->requirement_date;
                $reservation->note_header = $request->note_header;
                $reservation->updated_by = Auth::user()->id;
                $reservation->save();
            }

            DB::commit();

            return Reservation::with('plant')->find($header->id);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while updating reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function showReservationItem($id)
    {
        Auth::user()->cekRoleModules(['reservation-view']);

        $reservation = Reservation::with([
            'material', 'plant', 'storage', 'uom', 'movement_type', 'inout_plant', 'inout_storage',
            'approvalBy', 'material_plant', 'cost_center', 'profit_center', 'group_material',
            'createdBy', 'updatedBy'
        ])
        ->with('group_material.account')
        ->find($id);

        return $reservation;
    }

    public function deleteReservationItem($id)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        $delete = Reservation::find($id)->delete();

        return response()->json($delete);
    }

    public function delete($reservation_code)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        $reservation_id = Reservation::where('reservation_code', $reservation_code)->pluck('id');

        try {
            DB::beginTransaction();
            
            foreach ($reservation_id as $id) {
                $delete = Reservation::find($id);

                if ($delete->status != 0) {
                    throw new \Exception('reservation can delete status must Draft');
                }

                // record reservation approval
                ReservationApproval::create([
                    'reservation_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $delete->status, // draft
                    'status_to' => 10 // 10 = Deleted
                ]);

                $delete->timestamps = false;
                $delete->status = 10;
                $delete->save();
            }

            DB::commit();

            return response()->json([
                'message' => 'success delete reservation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while delete reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function submit($reservation_code)
    {
        Auth::user()->cekRoleModules(['reservation-submit']);

        $exist = Reservation::where('reservation_code', $reservation_code)->whereNull('material_id')->first();

        if (!$exist) {
            return response()->json([
                'message' => 'Reservation Not Found',
            ], 404);
        }

        if ($exist->status == 1) {
            return response()->json([
                'message'   => 'Reservation Already Submited',
            ], 422);
        }

        // handel if null item reservation cant submit
        $res_item = Reservation::where('reservation_code', $reservation_code)->whereNotNull('material_short_text')->first();

        if (!$res_item) {
            return response()->json([
                'message'   => 'Reservation not have item, Can\'t submit'
            ], 422);
        }

        $reservation_id = Reservation::where('reservation_code', $reservation_code)->pluck('id');

        // cek unit cost for PR cant empty
        if ($exist->document_type == 'A') {
            // $not_header_empty_cost = Reservation::where('reservation_code', $reservation_code)
            //     ->whereNotNull('material_short_text')
            //     ->whereNull('unit_cost')
            //     ->orWhere('unit_cost', '<', 0)
            //     ->first();

            // if ($not_header_empty_cost) {
            //     return response()->json([
            //         'message'   => 'Reservation '.$reservation_code.'. line '.$not_header_empty_cost->material_short_text.', Valuation price cant empty',
            //     ], 422);
            // }

            // cek if pr item type is for cost center, cost center id is required
            $not_header_empty_cost_center = Reservation::where('reservation_code', $reservation_code)
                ->whereNotNull('material_short_text')
                ->where('item_type', 1)
                ->whereNull('cost_center_id')
                ->first();

            if ($not_header_empty_cost_center) {
                return response()->json([
                    'message'   => 'Reservation '.$reservation_code.'. line '.$not_header_empty_cost_center->material_short_text.', cost center cant empty',
                ], 422);
            }

            // cek if pr item type is for cost center, material group id is required
            $not_header_empty_group_material_id = Reservation::where('reservation_code', $reservation_code)
                ->whereNotNull('material_short_text')
                ->where('item_type', 1)
                ->whereNull('group_material_id')
                ->first();

            if ($not_header_empty_group_material_id) {
                return response()->json([
                    'message'   => 'Reservation '.$reservation_code.'. line '.$not_header_empty_cost_center->material_short_text.', group material cant empty',
                ], 422);
            }
        }

        try {
            DB::beginTransaction();

            foreach ($reservation_id as $id) {
                $submit = Reservation::find($id);

                // record reservation approval
                ReservationApproval::create([
                    'reservation_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $submit->status, // draft
                    'status_to' => 1 // pending approval
                ]);

                $submit->timestamps = false;
                $submit->status = 1;
                $submit->save();
            }

            $storage_requester = MasterStorage::find($exist->storage_id);

            // cek if requester storage have responsible person & mail to that responsible user
            if ($storage_requester->user_id) {
                $responsible_storage_requester = User::find($storage_requester->user_id);

                \Mail::send(new \App\Mail\SubmitReservation($responsible_storage_requester, $exist));
            }

            DB::commit();

            return response()->json([
                'message' => 'success submit reservation for approval'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while submit reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function cancel($reservation_code)
    {
        Auth::user()->cekRoleModules(['reservation-submit']);

        $exist = Reservation::where('reservation_code', $reservation_code)->first();

        if (!$exist) {
            return response()->json([
                'message' => 'Reservation Not Found',
            ], 404);
        }

        $reservation_id = Reservation::where('reservation_code', $reservation_code)->pluck('id');

        try {
            DB::beginTransaction();

            foreach ($reservation_id as $id) {
                $submit = Reservation::find($id);

                // record reservation approval
                ReservationApproval::create([
                    'reservation_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => $submit->status, // pending approval
                    'status_to' => 4, // canceled
                    'notes' => null
                ]);

                $submit->timestamps = false;
                $submit->status = 4;
                $submit->save();
            }

            // cek if PR
            if ($exist->document_type == 'A') {
                // get pr item number by pr code
                $pr_item_number = Reservation::where('reservation_code', $reservation_code)
                    ->whereNotNull('reservation_item')
                    ->pluck('reservation_item');

                // get reservation with purc_req_number = pr code, purc_req_item = $pr_item_number
                $cek_reservation_convert_to_pr = Reservation::where('purc_req_number', $reservation_code)
                    ->whereIn('purc_req_item', $pr_item_number)
                    ->get();

                // update status reservation to submited, fullfill 0, agar reservasi muncul lagi setelah pr dicancel
                foreach ($cek_reservation_convert_to_pr as $key => $value) {
                    $res = Reservation::find($value->id);

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $res->status,
                        'status_to' => 2, // approved
                        'notes' => 'PR '.$reservation_code.' cancelled'
                    ]);

                    $res->timestamps = false;
                    $res->fulfillment_status = 0;
                    $res->status = 2;
                    $res->save();
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'success canceling reservation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while canceling reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function approval($reservation_code, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-approval']);

        $this->validate(request(), [
            'id'     => 'required|array',
            'id.*'   => 'required|exists:reservations,id',
            'status' => 'required|numeric|max:3|min:2',
            'approval_note' => 'required',
        ]);

        // get total reservation item
        if ($reservation_code[0] == 'R') {
            $total = Reservation::whereNotNull('material_id')->where('reservation_code', $reservation_code)->count();
        } else {
            // get total pr item
            $total = Reservation::whereNotNull('material_short_text')->where('reservation_code', $reservation_code)->count();
        }

        foreach ($request->id as $id) {
            $exist = Reservation::find($id);

            if ($exist->status == $request->status) {
                if ($request->status == 2) {
                    $text = 'approved';
                } else {
                    $text = 'rejected';
                }
                
                return response()->json([
                    'message'   => 'Reservation Already '. $text .', cant '. $text .' again',
                ], 422);
            }
        }

        try {
            DB::beginTransaction();
            
            foreach ($request->id as $id) {
                $reservation = Reservation::find($id)->update([
                    'status'    => $request->status,
                    'approval_note' => $request->approval_note,
                    'approval_date' => now(),
                    'approval_by' => Auth::user()->id
                ]);

                // record reservation approval
                ReservationApproval::create([
                    'reservation_id' => $id,
                    'user_id' => Auth::user()->id,
                    'status_from' => 1, // pending approval
                    'status_to' => $request->status,
                    'notes' => $request->approval_note
                ]);
            }

            // for reservation
            if ($reservation_code[0] == 'R') {
                // get total reservation item approved
                $approved = Reservation::whereNotNull('material_id')
                    ->where('status', 2)
                    ->where('reservation_code', $reservation_code)
                    ->count();

                // get total reservation item rejected
                $rejected = Reservation::whereNotNull('material_id')
                    ->where('status', 3)
                    ->where('reservation_code', $reservation_code)
                    ->count();

                $all = $approved + $rejected;

                // if total item is approved update status header
                if ($total == $approved) {
                    $head = Reservation::whereNull('material_id')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 5,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 5,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                } elseif ($total == $rejected) {// if total item is rejected update status header
                    $head = Reservation::whereNull('material_id')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 3,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 3,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                } elseif ($total == $all) {// if total item is rejected or approved update status header
                    $head = Reservation::whereNull('material_id')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 5,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 5,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                }
            } else {
                //for PR
                 // get total pr item approved
                $approved = Reservation::whereNotNull('material_short_text')
                    ->where('status', 2)
                    ->where('reservation_code', $reservation_code)
                    ->count();

                // get total pr item rejected
                $rejected = Reservation::whereNotNull('material_short_text')
                    ->where('status', 3)
                    ->where('reservation_code', $reservation_code)
                    ->count();

                $all = $approved + $rejected;

                // if total item is approved update status header
                if ($total == $approved) {
                    $head = Reservation::whereNull('material_short_text')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 5,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 5,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                } elseif ($total == $rejected) {// if total item is rejected update status header
                    $head = Reservation::whereNull('material_short_text')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 3,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 3,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                } elseif ($total == $all) {// if total item is rejected or approved update status header
                    $head = Reservation::whereNull('material_short_text')
                        ->where('reservation_code', $reservation_code)
                        ->first();

                    // record reservation approval
                    ReservationApproval::create([
                        'reservation_id' => $head->id,
                        'user_id' => Auth::user()->id,
                        'status_from' => $head->status,
                        'status_to' => 5,
                        'notes' => $request->approval_note
                    ]);

                    $head->update([
                        'status' => 5,
                        'approval_note' => $request->approval_note,
                        'approval_date' => now(),
                        'approval_by' => Auth::user()->id
                    ]);
                }
            }

            $first_r = Reservation::whereIn('id', $request->id)->first();
            $reservation_creator = User::find($first_r->created_by);
            $approval_ = ReservationApproval::with('user')
                ->where('reservation_id', $first_r->id)
                ->where('status_from', 1)
                ->first();

            \Mail::send(new \App\Mail\ApprovalReservation($reservation_creator, $first_r, $approval_));

            DB::commit();

            return response()->json([
                'message' => 'Success submit approval Reservation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed submit approval Reservation',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    /**
     * Send Approved Reservation (Purchase Requistion) as PO For External Purchase Type
     *
     * @param  Request  $request
     * @return Purchase Requistion
     */
    public function convertAsPo(Request $request)
    {
        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservations,id'
        ]);

        $pr_code = Reservation::whereIn('id', $request->id)->pluck('reservation_code');

        // validate pr header status
        foreach ($request->id as $key => $value) {
            $pr_headers = Reservation::find($value);
            if (!in_array($pr_headers->status, [2,5])) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key.''  => ['the selected PR status must approved']
                    ]
                ], 422);
            }
        }

        $pr_item = Reservation::whereIn('reservation_code', $pr_code)
            ->whereNotNull('material_short_text')
            ->get();

        // validate pr item status
        foreach ($pr_item as $key => $value) {
            $pr_items = Reservation::find($value->id);
            if ($pr_items->status == 5) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id'  => ['the selected PR Already processed to PO']
                    ]
                ], 422);
            }
        }

        // validate proc type external
        /* cek proc type
        * 0 = internal
        * 1 = external
        */
        $proc_type1 = $pr_item[0]['proc_type'];
        $cekrproc_type = true;

        for ($i = 0; $i < count($pr_item); $i++) {
            $mrp = $pr_item[$i]['proc_type'];
            if ($mrp !== $proc_type1 | !$mrp) {
                $cekrproc_type = false;
            }
        }

        if (!$cekrproc_type) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'id'  => ['the selected PR item are not in external procurement group']
                ]
            ], 422);
        }

        // cek pr item material info record (Material vendor)
        foreach ($pr_item as $key => $value) {
            // cek if free material text
            if (!$value->material_id) {
                // cek if free material text not have prefered vendor
                if (!$value->prefered_vendor_id) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'id'  => ['Material '.$value->material_short_text.' has missing prefered vendor']
                        ]
                    ], 422);
                }
            } else {//for material master
                $vendor_mat = MaterialVendor::where('material_id', $value->material_id)->first();
                $mat = Material::find($value->material_id);
                if (!$vendor_mat) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'id'  => ['Material '.$mat->description.' has missing info records']
                        ]
                    ], 422);
                }

                Reservation::find($value->id)->update([
                    'prefered_vendor_id' => $vendor_mat->vendor_id
                ]);
            }
        }

        // grouping by storage
        $storage_grouping = $pr_item->groupBy(['storage_id', 'prefered_vendor_id']);

        $val = [];
        try {
            DB::beginTransaction();
            
            foreach ($storage_grouping as $key => $value) {
                foreach ($value as $k => $data) {
                    $code = $this->POCode();

                     // create new PO header
                    $po = PurchaseHeader::create([
                        'po_doc_no' => $code,
                        'po_doc_category' => 'B',
                        'po_doc_type' => 0,
                        'purc_doc_date' => now(),
                        'status' => 0,
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id,
                    ]);

                    // grouping PR item material by vendor
                    foreach ($data as $item) {
                        // insert PR item as PO item
                        $po_item = PurchaseItem::create([
                            'purchase_header_id' => $po->id,
                            'purchasing_item_no' => null,
                            'account_type' => $item->item_type ? $item->item_type : 0,//0. Material; 1. cost center; 2. asset
                            'material_id' => $item->material_id,
                            'short_text' => $item->material_short_text,
                            'long_text' => $item->note_item,
                            'plant_id' => $item->plant_id,
                            'storage_id' => $item->storage_id,
                            'target_qty' => null,
                            'order_qty' => $item->quantity_base_unit,
                            'order_unit_id' => $item->uom_base_id,
                            'price_unit' => $item->unit_cost,
                            'delivery_location' => null,
                            'gl_account_id' => $item->account_id,
                            'cost_center_id' => $item->cost_center_id,
                            'dlv_complete' => 0,
                            'fund_center' => null,
                            'commitment_item' => $item->commitment_item_id,
                            'reservation_id' => $item->id,
                            'pr_number' => null,
                            'pr_item_number' => null,
                            'purc_doc_number' => null,
                            'purc_item_number' => null,
                            'group_material_id' => $item->group_material_id,
                            'project_id' => $item->project_id,
                            'delivery_date' => $item->requirement_date,
                            'currency' => $item->currency,
                            'created_by' => Auth::user()->id,
                            'updated_by' => Auth::user()->id,
                        ]);

                        // find pr
                        $res_pr_item = Reservation::find($item->id);

                        // record pr approval
                        ReservationApproval::create([
                            'reservation_id' => $item->id,
                            'user_id' => Auth::user()->id,
                            'status_from' => $res_pr_item->status,
                            'status_to' => 8,
                            'notes' => 'Convert to PO'
                        ]);

                        // update pr status
                        $res_pr_item->update([
                            'status' => 8, //PO created
                            'fulfillment_status' => 1,//0.Open, 1.Completed
                            'updated_by' => Auth::user()->id
                        ]);

                        Reservation::where('reservation_code', $res_pr_item->reservation_code)
                            ->where('reservation_item', null)
                            ->first()
                            ->update([
                                'status' => 8, //PO created
                            ]);
                    }

                    // update po code
                    TransactionCode::where('code', 'B')->first()
                    ->update(['lastcode' => $code]);
                }
            }

            DB::commit();

            return response()->json([
                'message' => 'success converting PR as PO',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error while converting PR as PO',
                'detail' => $e->getMessage()
            ], 422);
        }
    }

    public function addAttachment($reservation_code, Request $request)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        // validation
        $this->validate(request(), [
            'file' => 'required|file|max:5192',
        ]);

        // get file
        $file = $request->file('file');

        // get file extension for validation
        $ext_file = $file->getClientOriginalExtension();

        $ext = ['png', 'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'zip', 'rar'];

        if (!in_array($ext_file, $ext)) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type support to upload is png, jpg, jpeg, pdf, doc, docx, ppt, pptx, xls, xlsx, zip, rar']
                ]
            ],422);
        }

        try {
            DB::beginTransaction();

            if ($reservation_code[0] === 'R') {
                // get Reservation by code
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNull('material_id')
                    ->first();
            } else {
                // get Reservation by code
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNull('material_short_text')
                    ->first();
            }

            if (!$reservation) {
                throw new \Exception('reservation not found');
            }

            // upload file
            $file_data = request()->file('file');
            $file_ext  = request()->file('file')->getClientOriginalExtension();
            $file_old  = request()->file('file')->getClientOriginalName();
            $file_name = $reservation_code . md5(time().$file_old). "." .$file_ext;
            $file_path = 'file/reservation/'.$reservation_code;

            $uploaded = Storage::disk('public')->putFileAs($file_path, $file_data, $file_name);

            // insert data attachment
            ReservationAttachment::create([
                'reservation_id' => $reservation->id,
                'name' => $uploaded,
                'created_by' => Auth::user()->id,
                'original_name' => $file_old
            ]);

            DB::commit();

            return ReservationAttachment::where('reservation_id', $reservation->id)->get();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error save attachment',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function deleteAttachment($reservation_code)
    {
        Auth::user()->cekRoleModules(['reservation-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:reservation_attachments,id',
        ]);

        try {
            DB::beginTransaction();

            if ($reservation_code[0] === 'R') {
                // get Reservation by code
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNull('material_id')
                    ->first();
            } else {
                // get Reservation by code
                $reservation = Reservation::where('reservation_code', $reservation_code)
                    ->whereNull('material_short_text')
                    ->first();
            }

            if (!$reservation) {
                throw new \Exception('reservation not found');
            }

            foreach (request()->id as $ids) {
                $file = ReservationAttachment::find($ids);

                // delete file
                if (Storage::disk('public')->exists($file->name)) {
                    Storage::delete($file->name);
                }

                // delete data
                $delete = $file->delete();
            }

            DB::commit();

            return ReservationAttachment::where('reservation_id', $reservation->id)->get();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete attachment',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
