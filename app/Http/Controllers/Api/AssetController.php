<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Storage;
use PDF;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\Helpers\HashId;
use App\Exports\AssetExport;
use App\Imports\AssetImport;
use App\Exports\AssetInvalidExport;
use App\Exports\ExportFromArray;

use App\Models\User;
use App\Models\Location;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\Material;
use App\Models\Asset;
use App\Models\AssetStatus;
use App\Models\AssetParameter;
use App\Models\AssetImage;
use App\Models\AssetImage360;
use App\Models\AssetArea;
use App\Models\ResponsiblePersonHistory;
use App\Models\LocationHistory;
use App\Models\DepreTypeName;
use App\Models\DepreTypePeriod;
use App\Models\AssetFailedUpload;
use App\Models\AssetDepreciation;
use App\Models\GroupMaterial;

class AssetController extends Controller
{
    public function oldasset()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->with([
            'createdBy', 'updatedBy', 'material', 'asset_type', 'location', 'supplier', 'straightline', 
            'doubledecline', 'valuationasset'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        // plant filter
        if (request()->has('plant')) {
            $asset->whereIn('plant_id', request()->input('plant'));
        }

        // location filter
        if (request()->has('location')) {
            $asset->whereIn('location_id', request()->input('location'));
        }

        // asset_type filter
        if (request()->has('asset_type')) {
            $asset->whereIn('asset_type_id', request()->input('asset_type'));
        }

        // location_type filter
        if (request()->has('location_type')) {
            $asset->whereHas('location.location_type', function ($query){            
                    
                $query->where(function($query){
                    
                    $query->whereIn('location_type_id', request()->input('location_type'));
                });
            });
        }

        // valuation_type filter
        if (request()->has('valuation_type')) {
            $asset->whereIn('valuation_type', request()->input('valuation_type'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            // return $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
            $asset = $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            // return $asset->paginate(20)->appends(Input::except('page'));
            $asset = $asset->paginate(20)->appends(Input::except('page'));
        }

        $choosen_depreciation_type = "straight_line";

        if(request()->has('depre_type')){
                
            $choosen_depreciation_type = request()->input('depre_type');
        }

        foreach($asset->items() as $data_asset){

            $calc_depre = [
                'nilai_awal_penyusutan' => 0,
                'nilai_penyusutan'      => 0,
                'akumulasi_penyusutan'  => 0,
                'nilai_buku'            => 0
            ];

            if(!empty($data_asset->purchase_date) && (!empty($data_asset->valuationasset) && $data_asset->valuationasset->is_depreciation == true)){
                if ($choosen_depreciation_type === "straight_line" ){

                    $calc_depre = $this->straight_line_calculation($data_asset);
    
                }else{
    
                    $calc_depre = $this->double_decline_calculation($data_asset);
                }    
            }
            
            $data_asset["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
            $data_asset["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
            $data_asset["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
            $data_asset["nilai_buku"] = $calc_depre["nilai_buku"];
        }

        return $asset;
    }

    public function asset()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->with([
            'createdBy', 'updatedBy', 'material', 'asset_type', 'location', 'supplier', 'straightline', 
            'doubledecline', 'valuationasset'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        // plant filter
        if (request()->has('plant')) {
            $asset->whereIn('plant_id', request()->input('plant'));
        }

        // location filter
        if (request()->has('location')) {
            $asset->whereIn('location_id', request()->input('location'));
        }

        // asset_type filter
        if (request()->has('asset_type')) {
            $asset->whereIn('asset_type_id', request()->input('asset_type'));
        }

        // location_type filter
        if (request()->has('location_type')) {
            $asset->whereHas('location.location_type', function ($query){            
                    
                $query->where(function($query){
                    
                    $query->whereIn('location_type_id', request()->input('location_type'));
                });
            });
        }

        // valuation_type filter
        if (request()->has('valuation_type')) {
            $asset->whereIn('valuation_type', request()->input('valuation_type'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            // return $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
            $asset = $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            // return $asset->paginate(20)->appends(Input::except('page'));
            $asset = $asset->paginate(20)->appends(Input::except('page'));
        }

        return $asset;
    }

    public function straight_line_calculation($data_asset){

        $id_depreciation = $data_asset['straight_line'];
        $data_depre_name = DepreTypeName::find($id_depreciation);
        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $id_depreciation)
                                                ->whereRaw("period <= '".$data_asset['purchase_date']."'")
                                                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')
                                                ->first();
        
        if(empty($data_depre_period)){
            $data_straight_line = [
                'nilai_awal_penyusutan' => 0,
                'nilai_penyusutan'      => 0,
                'akumulasi_penyusutan'  => 0,
                'nilai_buku'            => 0,
                'tipe_penyusutan'       => ($data_depre_name ? $data_depre_name->name : "-"),
                'besar_penyusutan_perbulan' => 0,
                'umur_ekonomis'         => 0
            ];

            return $data_straight_line;
        }
        
        $umur_ekonomis = $data_depre_period->eco_lifetime / 12;
        $harga_beli = $data_asset['purchase_cost'];
        $nilai_sisa = $data_asset['salvage_value'];
        $purchase_date = Carbon::createFromFormat('Y-m-d', $data_asset['purchase_date']);
        $purchase_year = $purchase_date->year;
        $purchase_month = $purchase_date->month;
        $purchase_day = $purchase_date->day;
        $monthObj = Carbon::createFromFormat('!m', $purchase_month);
        if($purchase_day<=15){

            $monthName = $monthObj->format('F');
            $calc_purchase_date = new Carbon('first day of '.$monthName.' '.$purchase_year);
            
        }else{
            
            $monthName = $monthObj->addMonths(1)->format('F');
            $calc_purchase_date = new Carbon('first day of '.$monthName.' '.$purchase_year);
        }
        
        /*
        |--------------------------------------------------------------------------
        | Straight Line Method
        |--------------------------------------------------------------------------
        |
        | Besar Penyusutan = (Harga Beli - Nilai Sisa KALO ADA) / Umur Ekonomis
        | Besar Penyusutan Tahun Beli = Bulan Beli sampai Akhir Tahun / 12 * (Harga Beli - Nilai Sisa KALO ADA) / Umur Ekonomis
        | Atau
        | Besar Penyusutan Tahun Beli = Bulan Beli sampai Akhir Tahun / 12 * Besar Penyusutan
        */
        
        if($nilai_sisa == null){
            $besar_penyusutan = ($harga_beli - 0) / $umur_ekonomis;//Pertahun
        }else{
            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;//Pertahun
        }
        $month_left = 13 - $purchase_date->month;//Mendapatkan total bulan dari beli hingga akhir tahun
        $end_month_left = 12 - $month_left;
        $besar_penyusutan_awal = $month_left / 12 * $besar_penyusutan;
        $besar_penyusutan_perbulan = $besar_penyusutan / 12;

        // $days_total = $purchase_date->isLeapYear() ? 366 : 365;
        $days_total = 360;
        $besar_penyusutan_perhari = $besar_penyusutan / $days_total;
        
        $selisih = date_diff($calc_purchase_date, Carbon::now())->days + 1;
        if(Carbon::now()->year > $purchase_year + $umur_ekonomis){
            $akumulasi_penyusutan = 0;//Perhari ini
            $nilai_buku_perhari = 0;
            $penyusutan_tahun_ini = 0;
                
        }else{
            $akumulasi_penyusutan = $besar_penyusutan_perhari * $selisih;//Perhari ini
            $nilai_buku_perhari = $harga_beli - $akumulasi_penyusutan;
            
            if($purchase_year == Carbon::now()->year){
                $penyusutan_tahun_ini = $besar_penyusutan_awal;
            }else{
                $penyusutan_tahun_ini = $besar_penyusutan;
            }
        }
        
        /*STRAIGHT LINE Update Perhari Ini*/

        /*STRAIGHT LINE Depreciation Table*/
        $cost = $data_asset['purchase_cost'];
        
        $key = 0;
        $arr_count = 1;

        // $sum_nilai_penyusutan = 0;

        // for($i=$cost; $i>0; $i=$i-($besar_penyusutan_perbulan)){
        //     // $i = $i - ($besar_penyusutan_perbulan);
        //     $date = $purchase_date->addMonth($key);
        //     $akumulasi = $besar_penyusutan_perbulan * ($key+1);
        //     if(($harga_beli - $akumulasi) < 0){
        //         break;
        //     }

        //     $date = $purchase_date->subMonth($key);
        //     //$cost = $cost - $besar_penyusutan_perbulan;
        //     $key++;
        //     $arr_count++;

        // }

        $now = Carbon::now();

        $month_diff = (($now->year - $calc_purchase_date->year) * 12) + ($now->month - $calc_purchase_date->month);
        //dump($depreciation_table);

        $nilai_awal_penyusutan = $besar_penyusutan_perbulan * $month_diff;
        $akumulasi_penyusutan = $nilai_awal_penyusutan + $besar_penyusutan_perbulan;

        $data_straight_line = [
            'nilai_awal_penyusutan' => $nilai_awal_penyusutan,
            'nilai_penyusutan'      => $besar_penyusutan,
            'akumulasi_penyusutan'  => $akumulasi_penyusutan,
            'nilai_buku'            => $harga_beli - $akumulasi_penyusutan,
            'tipe_penyusutan'       => $data_depre_name->name,
            'besar_penyusutan_perbulan' => $besar_penyusutan_perbulan,
            'umur_ekonomis'       => $umur_ekonomis
        ];

        return $data_straight_line;
    }

    public function double_decline_calculation($data_asset){
        
        $key = 0;
        $aku_penyusutan_tahunIni = 0;

        $cost = $data_asset['purchase_cost'];//harga
        $double_decline = $data_asset['double_decline'];
        $data_depre_name = DepreTypeName::find($double_decline);

        $purchase_date = Carbon::createFromFormat('Y-m-d', $data_asset['purchase_date']);
        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $double_decline)
                                                ->whereDate('period', '<=', $purchase_date)
                                                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')
                                                ->first();

        if(empty($data_depre_period)){
            $double_dec_table = [
                'nilai_awal_penyusutan' => 0,
                'nilai_penyusutan'      => 0,
                'akumulasi_penyusutan'  => 0,
                'nilai_buku'            => 0,
                'tipe_penyusutan'       => ($data_depre_name ? $data_depre_name->name : "-")
            ];

            return $double_dec_table;
        }

        $umur_ekonomis = $data_depre_period->eco_lifetime / 12;

        $harga_beli_awal = $cost;
        $rates = $data_depre_period->rates;
        $month_start = $purchase_date->month;
        $last_year = $purchase_date->year + $umur_ekonomis - 1;
        $last_month = $purchase_date->month;
        $nilai_buku = $harga_beli_awal;
        $now = Carbon::now();

        $nourut = 1;
        $aku_penyusutan_bulanIni = 0;
        $aku_penyusutan_bulanLalu = 0;
        $nilai_buku_bulanIni = $harga_beli_awal;
        $total_akumulasi_penyusutan = 0;
        $penyusutan_hariIni = 0;
        $nilai_buku_hariIni = 0;
        $penyusutan_perTahun_ini = 0;
        $nilai_buku_saat_ini = 0;

        for($i = 0; $i < $umur_ekonomis; $i++){
            
            /* ===== start perhituangan penyusutan pertahun ===== */
            
            // $jml_month = 13 - $month_start;
            // $tahun_ini = $purchase_date->year + $key;

            // if ($tahun_ini == $last_year && $last_month > 1){
            //     $jml_month = $last_month - 1;
            // }

            $harga_beli_tahunIni = $nilai_buku;

            $penyusutan_tahunIni = 0;

            if ($i == $umur_ekonomis){

                $penyusutan_tahunIni = $harga_beli_tahunIni;

            }else{
                $penyusutan_tahunIni = $harga_beli_tahunIni * ($rates / 100);// * ($jml_month / 12);
            }

            // if($now->year == $tahun_ini){
            //     $penyusutan_tahun_ini = $penyusutan_tahunIni;
            // }

            $aku_penyusutan_tahunIni = $aku_penyusutan_tahunIni + $penyusutan_tahunIni;
            $nilai_buku = $nilai_buku - $penyusutan_tahunIni;

            /* ===== end perhituangan penyusutan per tahun ===== */

            /* ===== start perhituangan penyusutan per bulan ===== */

            $penyusutan_perBulan = $penyusutan_tahunIni / 12;

            for($j = 1; $j <= 12; $j++){

                // if($tahun_ini == $last_year && $j >= $last_month ){
                //     break 2;
                // }

                $purchase_date->addMonths(1);

                $harga_beli_bulanIni = $nilai_buku_bulanIni;
                $nilai_buku_bulanIni = $nilai_buku_bulanIni - $penyusutan_perBulan;

                if($purchase_date->year == $now->year){
                    
                    $penyusutan_perTahun_ini = $penyusutan_tahunIni;

                    if($purchase_date->month == $now->month){
                        
                        $nilai_buku_saat_ini = $nilai_buku_bulanIni;
                        $penyusutan_perhari = $penyusutan_perBulan / $now->daysInMonth;
    
                        $penyusutan_hariIni = $penyusutan_perhari * $now->day;
                        $total_akumulasi_penyusutan = $aku_penyusutan_bulanIni + $penyusutan_perBulan; //($penyusutan_perhari * $now->day);
                        $nilai_buku_hariIni = $nilai_buku_bulanIni - $penyusutan_hariIni;
    
                        break 2;
                    }
                }
                
                $aku_penyusutan_bulanLalu = $aku_penyusutan_bulanIni;

                $aku_penyusutan_bulanIni = $aku_penyusutan_bulanIni + $penyusutan_perBulan;
                
                $nourut++;
            }

            /* ===== end perhituangan penyusutan per bulan ===== */
            $month_start = 1;

            $key++;
        }

        // echo json_encode($double_dec_table);
        // return;

        if($total_akumulasi_penyusutan == 0){
            $total_akumulasi_penyusutan = $aku_penyusutan_bulanIni;
        }

        $double_dec_table = [
            'nilai_awal_penyusutan' => $aku_penyusutan_bulanLalu,
            'nilai_penyusutan'      => $penyusutan_perTahun_ini,
            'akumulasi_penyusutan'  => $total_akumulasi_penyusutan,
            'nilai_buku'            => $nilai_buku_saat_ini,
            'tipe_penyusutan'       => $data_depre_name->name
        ];

        return $double_dec_table;
    }

    public function oldlist()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->with([
            'createdBy', 'updatedBy', 'material', 'asset_type', 'location', 'supplier', 'straightline', 
            'doubledecline', 'valuationasset', 'responsible_person', 'plant', 'status', 'groupmaterial'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        // plant filter
        if (request()->has('plant')) {
            $asset->whereIn('plant_id', request()->input('plant'));
        }

        // location filter
        if (request()->has('location')) {
            $asset->whereIn('location_id', request()->input('location'));
        }

        // asset_type filter
        if (request()->has('asset_type')) {
            $asset->whereIn('asset_type_id', request()->input('asset_type'));
        }

        // location_type filter
        if (request()->has('location_type')) {
            $asset->whereHas('location.location_type', function ($query){            
                    
                $query->where(function($query){
                    
                    $query->whereIn('location_type_id', request()->input('location_type'));
                });
            });
        }

        // valuation_type filter
        if (request()->has('valuation_type')) {
            $asset->whereIn('valuation_type', request()->input('valuation_type'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        $asset = $asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        $choosen_depreciation_type = "straight_line";

        if(request()->has('depre_type')){
                
            $choosen_depreciation_type = request()->input('depre_type');
        }

        foreach($asset['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);

                $calc_depre = [
                    'nilai_awal_penyusutan' => 0,
                    'nilai_penyusutan'      => 0,
                    'akumulasi_penyusutan'  => 0,
                    'nilai_buku'            => 0
                ];
    
                if(!empty($v['purchase_date']) && (!empty($v['valuationasset']) && $v['valuationasset']['is_depreciation'] == true)){
                    if ($choosen_depreciation_type === "straight_line" ){
    
                        $calc_depre = $this->straight_line_calculation($v);
        
                    }else{
        
                        $calc_depre = $this->double_decline_calculation($v);
                    }    
                }
                
                $v["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
                $v["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
                $v["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
                $v["nilai_buku"] = $calc_depre["nilai_buku"];

                $asset['data'][$k] = $v;

            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 422);
            }
        }

        return $asset;
    }

    public function list()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('assets.deleted', false)->with([
            'createdBy', 'updatedBy', 'material', 'asset_type', 'location.location_type', 'supplier', 'straightline', 
            'doubledecline', 'valuationasset', 'responsible_person', 'plant', 'status', 
            'groupmaterial.account.account_group.chart_of_account', 'material.classification',
            'retired_reason', 'valuation_group', 'cost_center'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                // $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                // $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        // plant filter
        if (request()->has('plant_id')) {
            $asset->whereIn('plant_id', request()->input('plant_id'));
        }

        // location filter
        if (request()->has('location_id')) {
            $asset->whereIn('location_id', request()->input('location_id'));
        }

        // valuation_group filter
        if (request()->has('valuation_group_id')) {
            $asset->whereIn('valuation_group_id', request()->input('valuation_group_id'));
        }

        // valuation_type filter
        if (request()->has('valuation_type')) {
            $asset->whereIn('valuation_type_id', request()->input('valuation_type'));
        }

        // group_material filter
        if (request()->has('group_material_id')) {
            $asset->whereIn('group_material_id', request()->input('group_material_id'));
        }

        // cost center filter
        if (request()->has('cost_center_id')) {
            $asset->whereIn('cost_center_id', request()->input('cost_center_id'));
        }

        // material filter
        if (request()->has('material_id')) {
            $asset->whereIn('material_id', request()->input('material_id'));
        }

        // code filter
        if (request()->has('code')) {
            $asset->whereIn('id', request()->input('code'));
        }

        // depre monthly filter
        // if (request()->has('depreciation_period')) {
        //     $month = request()->input('depreciation_period');
        //     $asset->where(function($q) use($month) {
        //         foreach ($month as $m) {
        //             $q->whereMonth('purchase_date', '=', $m, 'or');
        //         }
        //     });
        // }

        // status filter
        if (request()->has('status_id')) {
            $asset->whereIn('status_id', request()->input('status_id'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $asset->join('materials','assets.material_id','=','materials.id');
                    $asset->select('assets.*');
                    $asset->orderBy('materials.code',$sort_order);
                break;

                case 'status':
                    $asset->join('asset_statuses','assets.status_id','=','asset_statuses.id');
                    $asset->select('assets.*');
                    $asset->orderBy('asset_statuses.name',$sort_order);
                break;

                case 'groupmaterial':
                    $asset->leftJoin('group_materials','assets.group_material_id','=','group_materials.id');
                    $asset->select('assets.*');
                    $asset->orderBy('group_materials.material_group',$sort_order);
                break;

                case 'cost_center':
                    $asset->join('cost_centers','assets.cost_center_id','=','cost_centers.id');
                    $asset->orderBy('cost_centers.code',$sort_order);
                break;

                case 'valuation_group':
                    $asset->leftJoin('user_groups','assets.valuation_group_id','user_groups.id');
                    $asset->orderBy('user_groups.code',$sort_order);
                break;

                case 'plant':
                    $asset->join('plants','assets.plant_id','=','plants.id');
                    $asset->orderBy('plants.code',$sort_order);
                break;

                case 'location':
                    $asset->join('locations','assets.location_id','=','locations.id');
                    $asset->orderBy('locations.address',$sort_order);
                break;

                case 'retired_reason':
                    $asset->orderBy('retired_date',$sort_order);
                break;

                case 'nilai_penyusutan':
                    $asset->orderBy('nilai_penyusutan',$sort_order);
                break;
                
                default:
                    $asset->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $asset->orderBy('code', 'asc');
        }

        $asset = $asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        $list_asset = [];
        $nourut = 0;

        foreach($asset['data'] as $data_asset){
            try {
                $data_asset['id'] = HashId::encode($data_asset['id']);

                if(!empty($data_asset['purchase_date']) && (!empty($data_asset['valuationasset']) && $data_asset['valuationasset']['is_depreciation'] == true)){

                    $List_depre_type = [
                        "straight_line", "double_decline"
                    ];

                    if(request()->has('depre_type')){
                        
                        $List_depre_type = request()->input('depre_type');

                    }
                    
                    // foreach($List_depre_type as $depre_type){

                        $calc_depre = [
                            'nilai_awal_penyusutan' => 0,
                            'nilai_penyusutan'      => 0,
                            'akumulasi_penyusutan'  => 0,
                            'nilai_buku'            => 0,
                            'tipe_penyusutan'       => '-',
                            'besar_penyusutan_perbulan'  => 0,
                            'umur_ekonomis'         => 0,
                        ];
                        
                        // if ($depre_type === "straight_line" ){

                            $calc_depre = $this->straight_line_calculation($data_asset);
                        //     // $calc_depre["tipe_penyusutan"] = 'Straight Line';
            
                        // }else{
            
                        //     $calc_depre = $this->double_decline_calculation($data_asset);
                        //     // $calc_depre["tipe_penyusutan"] = 'Double Decline';
                        // }

                        $data_asset["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
                        $data_asset["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
                        $data_asset["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
                        $data_asset["nilai_buku"] = $calc_depre["nilai_buku"];
                        $data_asset["tipe_penyusutan"] = $calc_depre["tipe_penyusutan"];
                        $data_asset["nourut"] = $nourut;
                        $data_asset["kalkulasi"]["depre_monthly"] = $calc_depre["besar_penyusutan_perbulan"];
                        $data_asset["kalkulasi"]["uselife"] = $calc_depre["umur_ekonomis"];

                        $list_asset[] = $data_asset;
                        $nourut++;
                    // }
                }else{

                    $calc_depre = [
                        'nilai_awal_penyusutan' => 0,
                        'nilai_penyusutan'      => 0,
                        'akumulasi_penyusutan'  => 0,
                        'nilai_buku'            => 0,
                        'tipe_penyusutan'       => '-',
                        'besar_penyusutan_perbulan'  => 0,
                        'umur_ekonomis'         => 0,
                    ];

                    $data_asset["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
                    $data_asset["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
                    $data_asset["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
                    $data_asset["nilai_buku"] = $calc_depre["nilai_buku"];
                    $data_asset["tipe_penyusutan"] = $calc_depre["tipe_penyusutan"];
                    $data_asset["nourut"] = $nourut;
                    $data_asset["kalkulasi"]["depre_monthly"] = $calc_depre["besar_penyusutan_perbulan"];
                    $data_asset["kalkulasi"]["uselife"] = $calc_depre["umur_ekonomis"];

                    $list_asset[] = $data_asset;
                    $nourut++;
                }
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 422);
            }
        }

        $asset['data'] = $list_asset;

        // return json_encode($list_asset);

        return $asset;
    }

    public function report()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->with([
            'createdBy', 'updatedBy', 'material', 'location.location_type', 'valuationasset', 'cost_center',
            'groupmaterial.account.account_group.chart_of_account', 'material.classification',
            'depre_sl', 'depre_dd'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        // location filter
        if (request()->has('location_id')) {
            $asset->whereIn('location_id', request()->input('location_id'));
        }

        // depreciation period filter
        if (request()->has('depreciation_period') && request()->input('depreciation_period') != '' 
            && request()->input('depreciation_period') != null) {
            $period = str_replace('"', '', request()->input('depreciation_period'));
            $month = Carbon::parse($period)->format('M');
            $year = Carbon::parse($period)->format('Y');
            $asset->whereHas('depre_sl', function($a) use ($month, $year) {
                $a->where('month', $month)->where('year', $year);
            });
            $asset->whereHas('depre_dd', function($a) use ($month, $year) {
                $a->where('month', $month)->where('year', $year);
            });
        }

        // valuation_type filter
        if (request()->has('valuation_type')) {
            $asset->whereIn('valuation_type_id', request()->input('valuation_type'));
        }

        // cost center filter
        if (request()->has('cost_center_id')) {
            $asset->whereIn('cost_center_id', request()->input('cost_center_id'));
        }

        // code filter
        if (request()->has('code')) {
            $asset->whereIn('id', request()->input('code'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        $asset = $asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        $list_asset = [];
        $nourut = 0;

        foreach($asset['data'] as $data_asset){

            $purchase_date = Carbon::createFromFormat('Y-m-d', $data_asset['purchase_date']);

            //sl
            $straightline = $data_asset['straight_line'];
            $data_depre_period_sl = DepreTypePeriod::where('depre_type_name_id', $straightline)
                ->whereDate('period', '<=', $purchase_date)
                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();
            $umur_ekonomis_sl = isset($data_depre_period_sl) ? $data_depre_period_sl->eco_lifetime / 12 : 0;
            $data_asset['show_sl']['uselife_sl'] = $umur_ekonomis_sl;

            //dd
            $double_decline = $data_asset['double_decline'];
            $data_depre_period_dd = DepreTypePeriod::where('depre_type_name_id', $double_decline)
                ->whereDate('period', '<=', $purchase_date)
                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();
            $umur_ekonomis_dd = isset($data_depre_period_dd) ? $data_depre_period_dd->eco_lifetime / 12 : 0;
            $data_asset['show_dd']['uselife_dd'] = $umur_ekonomis_dd;

            if (count($data_asset['depre_sl']) > 1) {
                if (request()->has('depreciation_period') && request()->input('depreciation_period') != '' 
                    && request()->input('depreciation_period') != null) {

                    $period = str_replace('"', '', request()->input('depreciation_period'));
                    $month = Carbon::parse($period)->format('M');
                    $year = Carbon::parse($period)->format('Y');
                    foreach ($data_asset['depre_sl'] as $d) {
                        if ($d['month'] == $month && $d['year'] == $year) {
                            $day = Carbon::parse($data_asset['purchase_date'])->format('d');
                            $month = $d['month'];
                            $year = $d['year'];
                            $depre_date = strftime("%F", strtotime($year."-".$month."-".$day));

                            $data_asset['show_sl']['depre_date'] = $depre_date;
                            $data_asset['show_sl']['depre_first_month'] = $data_asset['depre_sl'][1]['depreciation'];
                            $data_asset['show_sl']['depre_monthly'] = $d['depreciation'];
                            $data_asset['show_sl']['total_depreciation'] = $d['total_depreciation'];
                            $data_asset['show_sl']['book_value'] = $d['book_value'];
                            break;

                        } else {
                            $data_asset['show_sl']['depre_date'] = null;
                            $data_asset['show_sl']['depre_first_month'] = '-';
                            $data_asset['show_sl']['depre_monthly'] = '-';
                            $data_asset['show_sl']['total_depreciation'] = '-';
                            $data_asset['show_sl']['book_value'] = '-';
                        }
                    }

                } else {
                    $day = Carbon::parse($data_asset['purchase_date'])->format('d');
                    $month = $data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['month'] ;
                    $year = $data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['year'] ;
                    $depre_date = strftime("%F", strtotime($year."-".$month."-".$day));

                    $data_asset['show_sl']['depre_date'] = $depre_date;
                    $data_asset['show_sl']['depre_first_month'] = $data_asset['depre_sl'][1]['depreciation'];
                    $data_asset['show_sl']['depre_monthly'] = 
                        isset($data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['depreciation']) ? 
                        $data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['depreciation'] 
                        : '-';
                    $data_asset['show_sl']['total_depreciation'] = 
                        isset($data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['total_depreciation']) ? 
                        $data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['total_depreciation'] 
                        : '-';
                    $data_asset['show_sl']['book_value'] = 
                        isset($data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['book_value']) ? 
                        $data_asset['depre_sl'][count($data_asset['depre_sl']) - 1]['book_value'] 
                        : '-';
                }
            } else {
                $data_asset['show_sl']['depre_date'] = null;
                $data_asset['show_sl']['depre_first_month'] = '-';
                $data_asset['show_sl']['depre_monthly'] = '-';
                $data_asset['show_sl']['total_depreciation'] = '-';
                $data_asset['show_sl']['book_value'] = '-';
            }

            if (count($data_asset['depre_dd']) > 1) {

                if (request()->has('depreciation_period') && request()->input('depreciation_period') != '' 
                    && request()->input('depreciation_period') != null) {

                    $period = str_replace('"', '', request()->input('depreciation_period'));
                    $month = Carbon::parse($period)->format('M');
                    $year = Carbon::parse($period)->format('Y');
                    foreach ($data_asset['depre_dd'] as $d) {
                        if ($d['month'] == $month && $d['year'] == $year) {
                            $day = Carbon::parse($data_asset['purchase_date'])->format('d');
                            $month = $d['month'];
                            $year = $d['year'];
                            $depre_date = strftime("%F", strtotime($year."-".$month."-".$day));

                            $data_asset['show_dd']['depre_date'] = $depre_date;
                            $data_asset['show_dd']['depre_first_month'] = $data_asset['depre_dd'][1]['depreciation'];
                            $data_asset['show_dd']['depre_monthly'] = $d['depreciation'];
                            $data_asset['show_dd']['total_depreciation'] = $d['total_depreciation'];
                            $data_asset['show_dd']['book_value'] = $d['book_value'];
                            break;

                        } else {
                            $data_asset['show_dd']['depre_date'] = null;
                            $data_asset['show_dd']['depre_first_month'] = '-';
                            $data_asset['show_dd']['depre_monthly'] = '-';
                            $data_asset['show_dd']['total_depreciation'] = '-';
                            $data_asset['show_dd']['book_value'] = '-';
                        }
                    }

                } else {
                    $day = Carbon::parse($data_asset['purchase_date'])->format('d');
                    $month = $data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['month'] ;
                    $year = $data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['year'] ;
                    $depre_date = strftime("%F", strtotime($year."-".$month."-".$day));

                    $data_asset['show_dd']['depre_date'] = $depre_date;
                    $data_asset['show_dd']['depre_first_month'] = $data_asset['depre_dd'][1]['depreciation'];
                    $data_asset['show_dd']['depre_monthly'] = 
                        isset($data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['depreciation']) ? 
                        $data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['depreciation'] 
                        : '-';
                    $data_asset['show_dd']['total_depreciation'] = 
                        isset($data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['total_depreciation']) ? 
                        $data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['total_depreciation'] 
                        : '-';
                    $data_asset['show_dd']['book_value'] = 
                        isset($data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['book_value']) ? 
                        $data_asset['depre_dd'][count($data_asset['depre_dd']) - 1]['book_value'] 
                        : '-';
                }
            } else {
                $data_asset['show_dd']['depre_date'] = null;
                $data_asset['show_dd']['depre_first_month'] = '-';
                $data_asset['show_dd']['depre_monthly'] = '-';
                $data_asset['show_dd']['total_depreciation'] = '-';
                $data_asset['show_dd']['book_value'] = '-';
            }

            $list_asset[] = $data_asset;
        }

        $asset['data'] = $list_asset;

        return $asset;
    }

    public function assetoldnumber()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->where('status_id', '!=', 1)->with([
            'createdBy', 'updatedBy'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        // // status filter
        // if (request()->has('status')) {
        //     $asset->whereIn('status_id', request()->input('status'));
        // }

        // // plant filter
        // if (request()->has('plant')) {
        //     $asset->whereIn('plant_id', request()->input('plant'));
        // }

        // // material filter
        // if (request()->has('material')) {
        //     $asset->whereIn('material_id', request()->input('material'));
        // }

        // // supplier filter
        // if (request()->has('supplier')) {
        //     $asset->whereIn('supplier_id', request()->input('supplier'));
        // }

        // // asset_type filter
        // if (request()->has('asset_type')) {
        //     $asset->whereIn('asset_type_id', request()->input('asset_type'));
        // }

        // // valuation_group filter
        // if (request()->has('valuation_group')) {
        //     $asset->whereIn('valuation_group_id', request()->input('valuation_group'));
        // }

        // // superior_asset filter
        // if (request()->has('superior_asset')) {
        //     $asset->whereIn('superior_asset_id', request()->input('superior_asset'));
        // }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $asset->paginate(20)->appends(Input::except('page'));
        }
    }

    public function assetnumber()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $asset = (new Asset)->newQuery();

        $asset->where('deleted', false)->with([
            'createdBy', 'updatedBy'
        ]);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $asset->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(retired_remarks)"), 'LIKE', "%".$q."%");
            });
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $asset->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $asset->orderBy('code', 'asc');
        }

        if (request()->has('per_page')) {
            return $asset->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $asset->paginate(20)->appends(Input::except('page'));
        }
    }

    public function storeAsset(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-create']);

        $this->validate(request(), [
            'code'                  => 'required|unique:assets,code,NULL,NULL,deleted,false',
            'description'           => 'nullable',
            'material_id'           => 'required|exists:materials,id',
            'cost_center_id'        => 'nullable|exists:cost_centers,id',
            'asset_type_id'         => 'nullable|exists:asset_types,id',
            'plant_id'              => 'required|exists:plants,id',
            'location_id'           => 'required|exists:locations,id',
            'supplier_id'           => 'nullable|exists:suppliers,id',
            'status_id'             => 'nullable|exists:asset_statuses,id',
            'valuation_group_id'    => 'required|nullable|exists:user_groups,id',
            'retired_reason_id'     => 'nullable|exists:retired_reasons,id',
            'responsible_person_id' => 'nullable|exists:users,id',
            'old_asset'             => 'nullable',
            'salvage_value'         => 'nullable|numeric',
            'purchase_date'         => 'required|nullable|date',
            'purchase_cost'         => 'required|nullable',
            'waranty_start'         => 'nullable|date',
            'waranty_finish'        => 'nullable|date',
            'serial_number'         => 'nullable',
            'superior_asset_id'     => 'nullable|exists:assets,id',
            'time_cycle'            => 'nullable|required_with:unit_cycle',
            'unit_cycle'            => 'nullable|required_with:time_cycle',
            'curency'               => 'nullable',
            'retired_date'          => 'nullable|date',
            'retired_remarks'       => 'nullable',
            'last_maintenance'      => 'nullable|date',             
            'next_maintenance'      => 'nullable|date',
            'valuation_type'        => 'required|exists:valuation_asset,id',
            'straight_line'         => 'required|exists:depre_type_name,id',
            'double_decline'        => 'required|exists:depre_type_name,id',
            'material_group'        => 'nullable|exists:group_materials,id',
            'qty_asset'             => 'nullable|numeric',
            // 'unit_duration'         => 'nullable',
            // 'cycle_schedule'        => 'nullable',
            // 'level_asset'       => 'nullable',
            // 'count_duration'    => 'nullable',
        ]);

        $match = Asset::where('id', $request->superior_asset_id)->first();
        
        if (!$match) {
            $level = 1;
        } else {
            $level = $match->level_asset + 1;
        }

        $time = $request->time_cycle;
        $unit = $request->unit_cycle;
        if ($unit==4) {//Day
            $time = $time;
        } elseif ($unit==5) {//Week
            $time = $time*7;
        } elseif ($unit==6) {//month
            $time = $time*30;
        } else {//Year
            $time = $time*365;
        }

        // $location = Location::findOrFail($request->location_id);

        $data_asset = Asset::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($data_asset) {

            if($data_asset->deleted){
                
                $save = $data_asset->update([
                    'code'                  => $request->code,
                    'description'           => $request->description,
                    'cost_center_id'        => $request->cost_center_id,
                    'material_id'           => $request->material_id,
                    'asset_type_id'         => $request->asset_type_id,
                    'plant_id'              => $request->plant_id,
                    'location_id'           => $request->location_id,
                    'supplier_id'           => $request->supplier_id,
                    'status_id'             => $request->status_id,
                    'valuation_group_id'    => $request->valuation_group_id,
                    'retired_reason_id'     => $request->retired_reason_id,
                    'responsible_person_id' => $request->responsible_person_id,
                    'old_asset'             => $request->old_asset,
                    'purchase_date'         => $request->purchase_date,
                    'purchase_cost'         => $request->purchase_cost,
                    'waranty_start'         => $request->waranty_start,
                    'waranty_finish'        => $request->waranty_finish,
                    'serial_number'         => $request->serial_number,
                    'level_asset'           => $level,
                    'superior_asset_id'     => $request->superior_asset_id,
                    'count_duration'        => $time,
                    'unit_duration'         => $request->unit_cycle,
                    'cycle_schedule'        => $request->time_cycle,
                    'curency'               => $request->curency,
                    'retired_date'          => $request->retired_date,
                    'retired_remarks'       => $request->retired_remarks,
                    'last_maintenance'      => $request->last_maintenance,
                    'next_maintenance'      => $request->next_maintenance,
                    'valuation_type_id'     => $request->valuation_type,
                    'straight_line'         => $request->straight_line,
                    'double_decline'        => $request->double_decline,
                    'group_material_id'     => $request->material_group,
                    'qty_asset'             => $request->qty_asset,
                    'salvage_value'         => $request->salvage_value,
                    'deleted'               => 0,
                    'updated_by'            => Auth::user()->id
                ]);

                if ($save) {

                    $datareturn = Asset::with([
                                    'createdBy', 'updatedBy', 'material', 'location', 'responsible_person', 
                                    'asset_parameters', 'asset_parameters.classification_parameter', 'cost_center'
                                ])->find($data_asset->id);

                    $datareturn->id_hash = HashId::encode($datareturn->id);
                    
                    return $datareturn;
                } else {
                    return response()->json([
                        'message' => 'Failed Insert data',
                    ], 422);
                }
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['asset number already taken']
                ]
            ],422);

        } else {

            $save = Asset::create([
                'code'                  => $request->code,
                'description'           => $request->description,
                'material_id'           => $request->material_id,
                'cost_center_id'        => $request->cost_center_id,
                'asset_type_id'         => $request->asset_type_id,
                'plant_id'              => $request->plant_id,
                'location_id'           => $request->location_id,
                'supplier_id'           => $request->supplier_id,
                'status_id'             => $request->status_id,
                'valuation_group_id'    => $request->valuation_group_id,
                'retired_reason_id'     => $request->retired_reason_id,
                'responsible_person_id' => $request->responsible_person_id,
                'old_asset'             => $request->old_asset,
                'purchase_date'         => $request->purchase_date,
                'purchase_cost'         => $request->purchase_cost,
                'waranty_start'         => $request->waranty_start,
                'waranty_finish'        => $request->waranty_finish,
                'serial_number'         => $request->serial_number,
                'level_asset'           => $level,
                'superior_asset_id'     => $request->superior_asset_id,
                'count_duration'        => $time,
                'unit_duration'         => $request->unit_cycle,
                'cycle_schedule'        => $request->time_cycle,
                'curency'               => $request->curency,
                'retired_date'          => $request->retired_date,
                'retired_remarks'       => $request->retired_remarks,
                'last_maintenance'      => $request->last_maintenance,
                'next_maintenance'      => $request->next_maintenance,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
                'valuation_type_id'     => $request->valuation_type,
                'straight_line'         => $request->straight_line,
                'double_decline'        => $request->double_decline,
                'group_material_id'     => $request->material_group,
                'qty_asset'             => $request->qty_asset,
                'salvage_value'         => $request->salvage_value,
            ]);

            if ($save) {
                $datareturn = Asset::with([
                                'createdBy', 'updatedBy', 'material', 'location', 'responsible_person', 
                                'asset_parameters', 'asset_parameters.classification_parameter', 'cost_center'
                            ])->find($save->id);

                $datareturn->id_hash = HashId::encode($datareturn->id);
                
                return $datareturn;
            } else {
                return response()->json([
                    'message' => 'Failed Insert data',
                ], 422);
            }
        }
    }

    public function showAsset($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        // return Asset::with([
        //     'createdBy', 'updatedBy', 'material', 'asset_type', 'plant', 'location', 'supplier', 'status', 
        //     'valuation_group', 'retired_reason', 'responsible_person', 'asset_parameters', 'parent', 'images', 
        //     'image360', 'area', 'straightline', 'doubledecline', 'valuationasset', 'groupmaterial', 
        //     'asset_parameters.classification_parameter'
        // ])->findOrFail($id);

        $asset = Asset::with([
            'createdBy', 'updatedBy', 'material', 'material.classification', 'location', 'responsible_person', 
            'asset_parameters', 'asset_parameters.classification_parameter', 'status', 'cost_center',
            'many_responsible_person_history', 'many_responsible_person_history.old_responsible_person', 
            'location_history', 'location_history.old_location', 'location_history.cost_center', 
            'wo_asset', 'wo_asset.work_order', 'wo_asset.work_order.wo_status'
        ])->findOrFail($id);

        if (count($asset->wo_asset) > 0) {
            foreach ($asset->wo_asset as $x) {
                $x->id_hash = HashId::encode($id);
            }
        }

        return $asset;
    }

    public function showAssetStraightLine($id){
        
        /*
        |--------------------------------------------------------------------------
        | Straight Line Method
        |--------------------------------------------------------------------------
        |
        | Besar Penyusutan = (Harga Beli - Nilai Sisa KALO ADA) / Umur Ekonomis
        | Besar Penyusutan Tahun Beli = Bulan Beli sampai Akhir Tahun / 12 * (Harga Beli - Nilai Sisa KALO ADA) / Umur Ekonomis
        | Atau
        | Besar Penyusutan Tahun Beli = Bulan Beli sampai Akhir Tahun / 12 * Besar Penyusutan
        */

        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $date_now = Carbon::now();
        $data_asset = Asset::with(['groupmaterial','groupmaterial.account'])->find($id);
        $id_depreciation = $data_asset->straight_line;
        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $id_depreciation)
                                                ->whereDate('period', '<=', $data_asset->purchase_date)
                                                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

        $last_cost_center_name = ($data_asset->cost_center ? $data_asset->cost_center->description : "");
        $group_material_name = "";
        $gl_account_name = "";

        if($data_asset->groupmaterial){

            $group_material = $data_asset->groupmaterial;
            $group_material_name = $group_material->description;
            $gl_account_name = ($group_material->account ? $group_material->account->description : "");
        }
        

        $listLocationHistory = LocationHistory::with(['cost_center'])->whereIn('id', function($query){
            $query->select('id')->whereIn('created_at', function($query){
                $query->select(DB::raw("MAX(created_at)"))->from(with(new LocationHistory)->getTable())
                        ->groupBy('asset_id', DB::raw("date_part('year', created_at)"), DB::raw("date_part('month', created_at)"));
            });
        })->where('asset_id', $id)->get();

        $listCostCenter = $listLocationHistory->map(function ($listLocationHistory){

            $new_tanggal = substr($listLocationHistory->created_at, 0, 10);
            $newObj = array(
                "tanggal" => Carbon::createFromFormat('Y-m-d', $new_tanggal),
                "cost_center" => ($listLocationHistory->cost_center ? $listLocationHistory->cost_center->description : "")
            );

            return $newObj;
        });

        /*Main Variable*/
        $umur_ekonomis = $data_depre_period->eco_lifetime / 12;
        $harga_beli = $data_asset->purchase_cost;
        $nilai_sisa = $data_asset->salvage_value;
        $purchase_date = Carbon::createFromFormat('Y-m-d', $data_asset->purchase_date);
        $purchase_year = $purchase_date->year;
        $purchase_month = $purchase_date->month;
        $purchase_day = $purchase_date->day;
        /*Main Variable*/

        /*STRAIGHT LINE Update Perhari Ini*/

        $monthObj = Carbon::createFromFormat('!m', $purchase_month);
        if($purchase_day<=15){
            //Memulai menghitung dari tanggal 1 bulan beli
            $monthName = $monthObj->format('F');
            $calc_purchase_date = new Carbon('first day of '.$monthName.' '.$purchase_year);
            
        }else{
            //Memulai menghitung dari tanggal 1 bulan berikutnya
            $monthName = $monthObj->addMonths(1)->format('F');
            $calc_purchase_date = new Carbon('first day of '.$monthName.' '.$purchase_year);
            
        }
        
        if($nilai_sisa == null){
            $besar_penyusutan = ($harga_beli - 0) / $umur_ekonomis;//Pertahun
        }else{
            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;//Pertahun
        }
        $month_left = 13 - $purchase_date->month;//Mendapatkan total bulan dari beli hingga akhir tahun
        $end_month_left = 12 - $month_left;
        $besar_penyusutan_awal = $month_left / 12 * $besar_penyusutan;
        $besar_penyusutan_perbulan = $besar_penyusutan / 12;

        $days_total = $purchase_date->isLeapYear() ? 366 : 365;
        $besar_penyusutan_perhari = $besar_penyusutan / $days_total;
        $selisih = date_diff($calc_purchase_date, Carbon::now())->days + 1;
        $penyusutan_tahun_ini = 0;

        if(Carbon::now()->year > $purchase_year + $umur_ekonomis){
            $akumulasi_penyusutan = 0;//Perhari ini
            $nilai_buku_perhari = 0;
            $penyusutan_tahun_ini = 0;
               
        }else{
            $akumulasi_penyusutan = $besar_penyusutan_perhari * $selisih;//Perhari ini
            $nilai_buku_perhari = $harga_beli - $akumulasi_penyusutan;
            
            if($purchase_year == Carbon::now()->year){
                // $penyusutan_tahun_ini = $besar_penyusutan_awal;
            }else{
                // $penyusutan_tahun_ini = $besar_penyusutan;
            }
        }
        /*STRAIGHT LINE Update Perhari Ini*/

        /*STRAIGHT LINE Depreciation Table*/
        $cost = $data_asset->purchase_cost;

        $depreciation_table[0] = [
            'id'                    => 0,
            'month'                 => "-",
            'year'                  => "-",
            'penyusutan'            => "-",
            'akumulasi_penyusutan'  => "-",
            'nilai_buku'            => number_format($harga_beli,2,".",","),
            'count'                  => 0
        ];

        $key = 0;
        $arr_count = 1;
        $nilai_buku_saat_ini = 0;
        $penyusutan_ditahun_ini = 0;

        for($i=$cost; $i>0; $i=$i-($besar_penyusutan_perbulan)){
            // $i = $i - ($besar_penyusutan_perbulan);
            $date = $purchase_date->addMonth($key);
            $akumulasi = $besar_penyusutan_perbulan * ($key+1);
            if(($harga_beli - $akumulasi) < 0){
                break;
            }

            if($date_now->year == $date->year){

                $penyusutan_ditahun_ini = $penyusutan_ditahun_ini + $besar_penyusutan_perbulan;

                if($date_now->month == $date->month){
                    $akumulasi_penyusutan = $akumulasi;
                    $nilai_buku_saat_ini = $harga_beli - $akumulasi;
                    $penyusutan_tahun_ini = $penyusutan_ditahun_ini;
                    $penyusutan_ditahun_ini = 0;
                }
            }
            
            $cost_center_name = $last_cost_center_name;

            if(count($listCostCenter) > 0){
                $filter_cost_center = $listCostCenter->filter(function ($value, $key) use ($purchase_date) {

                    return $value["tanggal"]->lessThanOrEqualTo($purchase_date);
                });

                if(count($filter_cost_center) > 0){
                    $cost_center_per_tahun = $filter_cost_center->sortByDesc(function($item) {
                        return $item["tanggal"];
                    })->first();

                    $cost_center_name = $cost_center_per_tahun["cost_center"];
                }
            }

            $depreciation_table[$arr_count] = [
                'id'                    => $arr_count,
                'month'                 => $date->format("M"),
                'year'                  => $date->format("Y"),
                'penyusutan'            => number_format($besar_penyusutan_perbulan,2,".",","),
                'akumulasi_penyusutan'  => number_format($akumulasi,2,".",","),
                'nilai_buku'            => number_format(($harga_beli - $akumulasi),2,".",","),
                'cost'                  => $i,
                'cost_center'           => $cost_center_name,
                'material_group'        => $group_material_name,
                'gl_account'            => $gl_account_name
            ];

            $date = $purchase_date->subMonth($key);
            $key++;
            $arr_count++;

        }

        /*STRAIGHT LINE Depreciation Table*/
        $data_straight_line = [
            'umur_ekonomis'         => number_format($umur_ekonomis,2,".",""),
            'harga_beli'            => number_format($harga_beli,2,".",""),
            'nilai_sisa'            => number_format($nilai_sisa,2,".",""),
            'akumulasi_penyusutan'  => number_format($akumulasi_penyusutan,2,".",""),
            'nilai_buku_perhari'    => number_format($nilai_buku_saat_ini,2,".",""),
            'penyusutan_pertahun'   => number_format($besar_penyusutan,2,".",""),
            'penyusutan_perbulan'   => number_format($besar_penyusutan_perbulan,2,".",""),
            'penyusutan_tahun_ini'  => number_format($penyusutan_tahun_ini,2,".",""),
            'purchase_date'         => $data_asset->purchase_date,
            'table_straightline'    => $depreciation_table
        ];

        return $data_straight_line;
    }

    public function showAssetDoubleDecline($id){

        /*
        |--------------------------------------------------------------------------
        | Double Declining Method
        |--------------------------------------------------------------------------
        |
        | Tarif = 2 * (100% / Umur Ekonomis)
        | Besar Penyusutan = Tarif * Nilai Buku
        | Nilai Buku = Harga Beli - Akumulasi Penyusutan
        |
        | $tarif = 2 * (100 /100 /$umur_ekonomis);
        | $nilai_buku = $harga_beli - $akumulasi_susut;
        | $besar_penyusutan = $tarif * $nilai_buku;
        */

        /* Depreciation Table*/

        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        $data_asset = Asset::find($id);
        // $key = 0;
        $aku_penyusutan_tahunIni = 0;

        $cost = $data_asset->purchase_cost;//harga
        $double_decline = $data_asset->double_decline;

        $purchase_date = Carbon::createFromFormat('Y-m-d', $data_asset->purchase_date);
        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $double_decline)
                                                ->whereDate('period', '<=', $purchase_date)
                                                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

        $last_cost_center_name = ($data_asset->cost_center ? $data_asset->cost_center->description : "");
        $group_material_name = "";
        $gl_account_name = "";

        if($data_asset->groupmaterial){

            $group_material = $data_asset->groupmaterial;
            $group_material_name = $group_material->description;
            $gl_account_name = ($group_material->account ? $group_material->account->description : "");
        }

        $listLocationHistory = LocationHistory::with(['cost_center'])->whereIn('id', function($query){
            $query->select('id')->whereIn('created_at', function($query){
                $query->select(DB::raw("MAX(created_at)"))->from(with(new LocationHistory)->getTable())
                        ->groupBy('asset_id', DB::raw("date_part('year', created_at)"), DB::raw("date_part('month', created_at)"));
            });
        })->where('asset_id', $id)->get();

        $listCostCenter = $listLocationHistory->map(function ($listLocationHistory){

            $new_tanggal = substr($listLocationHistory->created_at, 0, 10);
            $newObj = array(
                "tanggal" => Carbon::createFromFormat('Y-m-d', $new_tanggal),
                "cost_center" => ($listLocationHistory->cost_center ? $listLocationHistory->cost_center->description : "")
            );

            return $newObj;
        });

        $umur_ekonomis = $data_depre_period->eco_lifetime / 12;

        $harga_beli_awal = $cost;
        $rates = $data_depre_period->rates;
        $month_start = $purchase_date->month;
        $last_year = $purchase_date->year + $umur_ekonomis - 1;
        $last_month = $purchase_date->month;
        $nilai_buku = $harga_beli_awal;
        $now = Carbon::now();

        $double_dec_table[0] = [
            'id'                    => 0,
            'month'                 => "-",
            'year'                  => "-",
            'harga_beli'            => 0,
            'penyusutan'            => 0,
            'akumulasi_penyusutan'  => 0,
            'nilai_buku'            => number_format($nilai_buku,2,".",","),
            'count'                  => 0
        ];

        $nourut = 1;
        $aku_penyusutan_bulanIni = 0;
        $nilai_buku_bulanIni = $harga_beli_awal;
        $nilai_buku_saat_ini = 0;
        $aku_penyusutan_saat_ini = 0;
        $penyusutan_perBulan_saat_ini = 0;
        $penyusutan_ditahun_ini = 0;
        $penyusutan_perTahun_ini = 0;
        $penyusutan_tahun_ini = 0;
        
        for($i = 1; $i <= $umur_ekonomis; $i++){
            
            /* ===== start perhituangan penyusutan pertahun ===== */
            
            // $jml_month = 13 - $month_start;
            // $tahun_ini = $purchase_date->year + $key;

            $harga_beli_tahunIni = $nilai_buku;

            $penyusutan_tahunIni = 0;

            if ($i == $umur_ekonomis){

                $penyusutan_tahunIni = $harga_beli_tahunIni;

            }else{
                $penyusutan_tahunIni = $harga_beli_tahunIni * ($rates / 100); // * ($jml_month / 12);
            }

            // if($now->year == $tahun_ini){
            //     $penyusutan_tahun_ini = $penyusutan_tahunIni;
            // }

            $aku_penyusutan_tahunIni = $aku_penyusutan_tahunIni + $penyusutan_tahunIni;
            $nilai_buku = $nilai_buku - $penyusutan_tahunIni;

            /* ===== end perhituangan penyusutan per tahun ===== */

            /* ===== start perhituangan penyusutan per bulan ===== */

            $penyusutan_perBulan = $penyusutan_tahunIni / 12; //$jml_month;

            for($j = 1; $j <= 12; $j++){

                $purchase_date->addMonths(1);

                $harga_beli_bulanIni = $nilai_buku_bulanIni;
                $nilai_buku_bulanIni = $nilai_buku_bulanIni - $penyusutan_perBulan;

                if($now->year == $purchase_date->year){

                    $penyusutan_tahun_ini = $penyusutan_tahunIni;
                    // $penyusutan_ditahun_ini = $penyusutan_ditahun_ini + $penyusutan_perBulan;

                    if($now->month == $purchase_date->month){
                        $nilai_buku_saat_ini = $nilai_buku_bulanIni;
                        $aku_penyusutan_saat_ini = $aku_penyusutan_bulanIni + $penyusutan_perBulan;
                        $penyusutan_perBulan_saat_ini = $penyusutan_perBulan;
                        $penyusutan_perTahun_ini = $penyusutan_perBulan * $j; //$penyusutan_ditahun_ini;
                        // $penyusutan_ditahun_ini = 0;
                    }
                }

                $cost_center_name = $last_cost_center_name;

                if(count($listCostCenter) > 0){
                    $filter_cost_center = $listCostCenter->filter(function ($value, $key) use ($purchase_date) {
                        // echo json_encode($value);
                        // return $value["tahun"] <= $purchase_date->year && ($value["bulan"] <= $purchase_date->month);
                        return $value["tanggal"]->lessThanOrEqualTo($purchase_date);
                    });

                    if(count($filter_cost_center) > 0){
                        $cost_center_per_tahun = $filter_cost_center->sortByDesc(function($item) {
                            return $item["tanggal"];
                        })->first();

                        // return json_encode($cost_center_per_tahun);

                        $cost_center_name = $cost_center_per_tahun["cost_center"];
                    }
                }

                $double_dec_table[$nourut] = [
                    'id'                    => $nourut,
                    'month'                 => $purchase_date->format("M"),
                    'year'                  => $purchase_date->year,
                    'harga_beli'            => number_format($harga_beli_bulanIni,2,".",","),
                    'penyusutan'            => number_format($penyusutan_perBulan,2,".",","),
                    'akumulasi_penyusutan'  => number_format($aku_penyusutan_bulanIni,2,".",","),
                    'nilai_buku'            => number_format($nilai_buku_bulanIni,2,".",","),
                    'count'                 => $nourut,
                    'cost_center'           => $cost_center_name,
                    'material_group'        => $group_material_name,
                    'gl_account'            => $gl_account_name
                ];

                $aku_penyusutan_bulanIni = $aku_penyusutan_bulanIni + $penyusutan_perBulan;
                
                $nourut++;
            }

            /* ===== end perhituangan penyusutan per bulan ===== */
            // $month_start = 1;

            // $key++;
        }

        // echo json_encode($double_dec_table);
        // return;

        $data_double_dec = [
            'umur_ekonomis'             => number_format($umur_ekonomis,2,".",""),
            'harga_beli'                => number_format($cost,2,".",""),
            'nilai_sisa'                => number_format($nilai_buku,2,".",""),
            'akumulasi_penyusutan'      => number_format($aku_penyusutan_saat_ini,2,".",""),
            'nilai_buku_perhari'        => number_format($nilai_buku_saat_ini,2,".",""),
            'penyusutan_tahun_ini'      => number_format($penyusutan_tahun_ini,2,".",""),
            'penyusutan_bulan_ini'      => number_format($penyusutan_perBulan_saat_ini,2,".",""),
            'purchase_date'             => $data_asset->purchase_date,
            'penyusutan_perTahun_ini'   => number_format($penyusutan_perTahun_ini,2,".",""),
            'table_doubledecline'       => $double_dec_table
        ];

        return $data_double_dec;
    }

    public function updateAsset($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $asset = Asset::findOrFail($id);

        $this->validate(request(), [
            'code'                  => 'required|unique:assets,code,'. $id .'',
            'description'           => 'nullable',
            'material_id'           => 'required|exists:materials,id',
            'cost_center_id'        => 'nullable|exists:cost_centers,id',
            'asset_type_id'         => 'nullable|exists:asset_types,id',
            'plant_id'              => 'required|exists:plants,id',
            // 'location_id'           => 'nullable|exists:locations,id',
            'supplier_id'           => 'nullable|exists:suppliers,id',
            'status_id'             => 'nullable|exists:asset_statuses,id',
            'valuation_group_id'    => 'required|nullable|exists:user_groups,id',
            'retired_reason_id'     => 'nullable|exists:retired_reasons,id',
            // 'responsible_person_id' => 'nullable|exists:users,id',
            'old_asset'             => 'nullable',
            'salvage_value'         => 'nullable|numeric',
            'purchase_date'         => 'required|nullable|date',
            'purchase_cost'         => 'required|nullable',
            'waranty_start'         => 'nullable|date',
            'waranty_finish'        => 'nullable|date',
            'serial_number'         => 'nullable',
            'superior_asset_id'     => 'nullable|exists:assets,id',
            'time_cycle'            => 'nullable|required_with:unit_cycle',
            'unit_cycle'            => 'nullable|required_with:time_cycle',
            'curency'               => 'nullable',
            'retired_date'          => 'nullable|date',
            'retired_remarks'       => 'nullable',
            'last_maintenance'      => 'nullable|date',             
            'next_maintenance'      => 'nullable|date',
            'valuation_type'        => 'required|exists:valuation_asset,id',
            'straight_line'         => 'required|exists:depre_type_name,id',
            'double_decline'        => 'required|exists:depre_type_name,id',
            'material_group'        => 'nullable|exists:group_materials,id',
            'qty_asset'             => 'nullable|numeric',
            // 'unit_duration'         => 'nullable',
            // 'cycle_schedule'        => 'nullable',
            // 'level_asset'       => 'nullable',
            // 'count_duration'    => 'nullable',
        ]);

        $match = Asset::where('id', $request->superior_asset_id)->first();
        
        if (!$match) {
            $level = 1;
        } else {
            $level = $match->level_asset + 1;
        }

        $time = $request->time_cycle;
        $unit = $request->unit_cycle;
        if ($unit==4) {//Day
            $time = $time;
        } elseif ($unit==5) {//Week
            $time = $time*7;
        } elseif ($unit==6) {//month
            $time = $time*30;
        } else {//Year
            $time = $time*365;
        }

        $save = $asset->update([
            'code'                  => $request->code,
            'description'           => $request->description,
            'material_id'           => $request->material_id,
            'cost_center_id'        => $request->cost_center_id,
            'asset_type_id'         => $request->asset_type_id,
            'plant_id'              => $request->plant_id,
            // 'location_id'           => $request->location_id,
            'supplier_id'           => $request->supplier_id,
            'status_id'             => $request->status_id,
            'valuation_group_id'    => $request->valuation_group_id,
            'retired_reason_id'     => $request->retired_reason_id,
            // 'responsible_person_id' => $request->responsible_person_id,
            'old_asset'             => $request->old_asset,
            'purchase_date'         => $request->purchase_date,
            'purchase_cost'         => $request->purchase_cost,
            'waranty_start'         => $request->waranty_start,
            'waranty_finish'        => $request->waranty_finish,
            'serial_number'         => $request->serial_number,
            'level_asset'           => $level,
            'superior_asset_id'     => $request->superior_asset_id,
            'count_duration'        => $time,
            'unit_duration'         => $request->unit_cycle,
            'cycle_schedule'        => $request->time_cycle,
            'curency'               => $request->curency,
            'retired_date'          => $request->retired_date,
            'retired_remarks'       => $request->retired_remarks,
            'last_maintenance'      => $request->last_maintenance,
            'next_maintenance'      => $request->next_maintenance,
            'updated_by'            => Auth::user()->id,
            'valuation_type_id'     => $request->valuation_type,
            'straight_line'         => $request->straight_line,
            'double_decline'        => $request->double_decline,
            'group_material_id'     => $request->material_group,
            'qty_asset'             => $request->qty_asset,
            'salvage_value'         => $request->salvage_value,
        ]);

        if ($save) {

            $datareturn = Asset::with([
                'createdBy', 'updatedBy', 'material', 'location', 'responsible_person', 
                'asset_parameters', 'asset_parameters.classification_parameter', 'cost_center'
            ])->find($asset->id);

            $datareturn->id_hash = HashId::encode($datareturn->id);

            return $datareturn;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteAsset($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $delete = Asset::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }
    
    public function multipleDelete()
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:assets,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {
                $delete = Asset::findOrFail($ids)->update([
                    'deleted' => true, 'updated_by' => Auth::user()->id
                ]);
            }

            DB::commit();

            return response()->json([
                'message' => 'Success delete data'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'error delete data',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function classificationParametersByMaterial($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $material = Material::findOrFail($id);

        return ClassificationParameter::where('classification_id', $material->classification_material_id)
                ->where('deleted', false)->get();
    }

    public function storeAssetClassificationParameters($id,Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $parameter  = $request->input('parameter');
        $asset_id   = $id;
        
        if (is_array($parameter) || is_object($parameter))
        {
            foreach($parameter as $key => $value)
            {
                $id_parameter = explode("-",$key)[1];
                $value_parameter = $value;
                $insert_param = AssetParameter::updateOrCreate(
                    [
                        'asset_id'      => $asset_id,
                        'classification_parameter_id'  => $id_parameter,
                    ],
                    [
                        'value'         => $value_parameter,
                        'created_by'    => Auth::user()->id,
                        'updated_by'    => Auth::user()->id
                    ]
                );
            }
        }

        return response()->json([
            'message' => 'Success create data'
        ], 200);
    }

    public function responsiblePersonHistory($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);
        
        $history = ResponsiblePersonHistory::where('asset_id', $id)->orderBy('id', 'desc')->get();

        $data = $history->map(function($dataq){
            $dataq->person = User::findOrFail($dataq->old_responsible_person_id)->name;

            return $dataq;
        });

        return $data;
    }

    public function responsiblePersonHistoryList()
    {
        Auth::user()->cekRoleModules(['asset-view']);
        
        $history = ResponsiblePersonHistory::get();

        return $history;
    }

    public function responsiblePersonHistoryPagination($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        $history = (new ResponsiblePersonHistory)->newQuery();

        $history->where('asset_id', $id);
        $history->where('responsible_person_histories.deleted', false);
        $history->with(['old_responsible_person', 'createdBy']);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'created_by':
                    $history->leftJoin('users','users.id','=','responsible_person_histories.created_by');
                    $history->select('responsible_person_histories.*');
                    $history->orderBy('users.firstname',$sort_order);
                break;

                case 'old_responsible_person':
                    $history->leftJoin('users','users.id','=','responsible_person_histories.old_responsible_person_id');
                    $history->select('responsible_person_histories.*');
                    $history->orderBy('users.firstname',$sort_order);
                break;
                
                default:
                    $history->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $history->orderBy('created_at', 'desc');
        }

        $history = $history->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($history['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $history['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $history;
    }

    public function locationHistoryPagination($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        $history = (new LocationHistory)->newQuery();

        $history->where('asset_id', $id);
        $history->where('location_histories.deleted', false);
        $history->with(['old_location', 'cost_center', 'createdBy']);

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'created_by':
                    $history->join('users','users.id','=','location_histories.created_by');
                    $history->select('location_histories.*');
                    $history->orderBy('users.firstname',$sort_order);
                break;

                case 'cost_center':
                    $history->join('cost_centers','cost_centers.id','=','location_histories.cost_center_id');
                    $history->select('location_histories.*');
                    $history->orderBy('cost_centers.code',$sort_order);
                break;
                
                default:
                    $history->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $history->orderBy('created_at', 'desc');
        }

        $history = $history->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'))
            ->toArray();

        foreach($history['data'] as $k => $v) {
            try {
                $v['id'] = HashId::encode($v['id']);
                $history['data'][$k] = $v;
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 400);
            }
        }

        return $history;
    }

    public function updateEmptyResponsiblePerson(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);
        
        $this->validate(request(), [
            'id'       => 'required',
            'old'       => 'required|exists:users,id',
        ]);

        $save = ResponsiblePersonHistory::findOrFail($request->id)->update([
            'old_responsible_person_id' => $request->old,
        ]);

        if ($save) {

            return response()->json([
                'message' => 'Success',
            ], 200);

        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function locationHistory($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);
        
        return LocationHistory::where('asset_id', $id)->orderBy('id', 'desc')->get();
    }

    public function updateResponsiblePersonHistory($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        $this->validate(request(), [
            'old'       => 'nullable|exists:users,id',
            'new'       => 'required|exists:users,id'
        ]);

        $save = ResponsiblePersonHistory::create([
            'asset_id'  => $id,
            'old_responsible_person_id' => $request->old,
            'created_by'=> Auth::user()->id,
            'updated_by'=> Auth::user()->id
        ]);

        if ($save) {
            $update = Asset::findOrFail($id)->update([
                'responsible_person_id' => $request->new,
                'updated_by'            => Auth::user()->id
            ]);

            $datareturn = Asset::with(['responsible_person'])->find($id);

            return $datareturn;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function updateLocationHistory($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        $this->validate(request(), [
            'old_location_id'       => 'nullable|exists:locations,id',
            'new_location_id'       => 'required|exists:locations,id',
            'cost_center_id'        => 'nullable|exists:cost_centers,id'
        ]);

        $asset = Asset::findOrFail($id);

        $location = Location::findOrFail(request()->new_location_id);

        $old_location = Location::findOrFail(request()->old_location_id);

        $save = LocationHistory::create([
            'asset_id'          => $id,
            'old_location_id'   => $request->old_location_id,
            'cost_center_id'    => $asset->cost_center_id,
            'location_code'     => $old_location->code,
            'building'          => $asset->building,
            'unit'              => $asset->unit,
            'city'              => $asset->city,
            'province'          => $asset->province,
            'address_location'  => $old_location->address,
            'latitude'          => $asset->latitude,
            'longitude'         => $asset->longitude,
            'created_by'        => Auth::user()->id,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            $update = $asset->update([
                'location_id'           => $request->new_location_id,
                'cost_center_id'        => $request->cost_center_id,
                'building'              => $location->building,
                'unit'                  => $location->unit,
                'city'                  => $location->city,
                'province'              => $location->province,
                'address_location'      => $location->address,
                'latitude'              => $location->latitude,
                'longitude'             => $location->longitude,
                'updated_by'            => Auth::user()->id
            ]);

            $datareturn = Asset::with(['location', 'cost_center'])->find($id);

            return $datareturn;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function assetImageList($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        return AssetImage::where('deleted', false)->where('asset_id', $id)
                ->orderBy('id', 'desc')->get();   
    }

    // public function storeAssetImage(Request $request, $id)
    // {
    //     Auth::user()->cekRoleModules(['asset-update']);

    //     $this->validate(request(), [
    //         'images'             => 'required|image'
    //     ]);

    //     if ($request->has('images')) {
    //         $image_data = request()->file('images');
    //         $image_ext  = request()->file('images')->getClientOriginalExtension();
    //         $image_name = Auth::user()->id . md5(time()). "." .$image_ext;
    //         $image_path = 'images/asset/'.$id.'/image';

    //         $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
    //     }

    //     $save = AssetImage::create([
    //         'asset_id'       => $id,
    //         'image'             => $uploaded,
    //         // 'value'             => $request->value,
    //         'created_by'        => Auth::user()->id,
    //         'updated_by'        => Auth::user()->id
    //     ]);

    //     if ($save) {
    //         return $save;
    //     } else {
    //         return response()->json([
    //             'message' => 'Failed Insert data',
    //         ], 422);
    //     }
    // }

    public function storeAssetImage(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate(request(), [
            'images'    => 'required|image'
        ]);

        if ($request->has('images')) {

            try {
                DB::beginTransaction();
    
                // foreach(request()->file('images') as $key => $image){
                
                    $image_data = request()->file('images');
                    $image_ext  = $image_data->getClientOriginalExtension();
                    $image_old  = $image_data->getClientOriginalName();
                    $image_name = Auth::user()->id . md5(time().$image_old). "." .$image_ext;
                    $image_path = 'images/asset/'.$id.'/image';
    
                    $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
    
                    $save = AssetImage::create([
                        'asset_id'       => $id,
                        'image'             => $uploaded,
                        // 'value'             => $request->value,
                        'created_by'        => Auth::user()->id,
                        'updated_by'        => Auth::user()->id
                    ]);
                // }
    
                DB::commit();

                return AssetImage::where('asset_id', $id)->get();

            } catch (\Exception $e) {
                DB::rollback();
    
                return response()->json([
                    'message' => 'Error Adding Image',
                    'detail' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ], 422);
            }
        }
    }

    public function showAssetImage($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        return AssetImage::findOrFail($id);
    }

    public function updateAssetImage($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $assetImage = AssetImage::findOrFail($id);

        $this->validate(request(), [
            'asset_id'          => 'required|exists:assets,id',
            'images'             => 'required|image'
        ]);

        if (Storage::disk('public')->exists($assetImage->image)) {
            Storage::delete($assetImage->image);
        }

        if ($request->has('image')) {
            $image_data = request()->file('image');
            $image_ext  = $image_data->getClientOriginalExtension();
            $image_old  = $image_data->getClientOriginalName();
            $image_name = Auth::user()->id . md5(time().$image_old). "." .$image_ext;
            $image_path = 'images/asset/'.$request->asset_id.'/image';

            $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
        }

        $save = $assetImage->update([
            'asset_id'          => $request->asset_id,
            'image'             => $uploaded,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $assetImage;
        } else {
            return response()->json([
                'message' => 'Failed Update Image',
            ], 422);
        }
    }

    public function deleteAssetImage($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $image = AssetImage::findOrFail($id);       

        if (Storage::disk('public')->exists($image->image)) {
            Storage::delete($image->image);
        }

        $delete = $image->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Image',
            ], 422);
        }
    }

    public function assetImage360List($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        return AssetImage360::where('deleted', false)->where('asset_id', $id)
                ->orderBy('id', 'desc')->get();   
    }

    // public function storeAssetImage360(Request $request, $id)
    // {
    //     Auth::user()->cekRoleModules(['asset-update']);

    //     $this->validate(request(), [
    //         'image'             => 'required|image'
    //     ]);

    //     if ($request->has('image')) {
    //         $image_data = request()->file('image');
    //         $image_ext  = request()->file('image')->getClientOriginalExtension();
    //         $image_name = Auth::user()->id . md5(time()). "." .$image_ext;
    //         $image_path = 'images/asset/'.$id.'/image360';

    //         $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
    //     }

    //     $save = AssetImage360::create([
    //         'asset_id'          => $id,
    //         'image360'          => $uploaded,
    //         // 'value'             => $request->value,
    //         'created_by'        => Auth::user()->id,
    //         'updated_by'        => Auth::user()->id
    //     ]);

    //     if ($save) {
    //         return $save;
    //     } else {
    //         return response()->json([
    //             'message' => 'Failed Insert data',
    //         ], 422);
    //     }
    // }

    public function storeAssetImage360(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate(request(), [
            'images'    => 'required|image'
        ]);

        if ($request->has('images')) {
            
            try {
                DB::beginTransaction();
    
                // foreach(request()->file('images') as $key => $image){
                
                    $image_data = request()->file('images');
                    $image_ext  = $image_data->getClientOriginalExtension();
                    $image_old  = $image_data->getClientOriginalName();
                    $image_name = Auth::user()->id . md5(time().$image_old). "." .$image_ext;
                    $image_path = 'images/asset/'.$id.'/image360';
        
                    $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
    
                    $save = AssetImage360::create([
                        'asset_id'      => $id,
                        'image360'      => $uploaded,
                        // 'value'             => $request->value,
                        'created_by'    => Auth::user()->id,
                        'updated_by'    => Auth::user()->id
                    ]);
                // }
    
                DB::commit();

                return AssetImage360::where('asset_id', $id)->get();

            } catch (\Exception $e) {
                DB::rollback();
    
                return response()->json([
                    'message' => 'Error Adding Image',
                    'detail' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ], 422);
            }
        }
    }

    public function showAssetImage360($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        return AssetImage360::findOrFail($id);
    }

    public function updateAssetImage360($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $assetImage360 = AssetImage360::findOrFail($id);

        $this->validate(request(), [
            'asset_id'          => 'required|exists:assets,id',
            'images'             => 'required|image'
        ]);

        if (Storage::disk('public')->exists($assetImage360->image360)) {
            Storage::delete($assetImage360->image360);
        }

        if ($request->has('image')) {
            $image_data = request()->file('image');
            $image_ext  = $image_data->getClientOriginalExtension();
            $image_old  = $image_data->getClientOriginalName();
            $image_name = Auth::user()->id . md5(time().$image_old). "." .$image_ext;
            $image_path = 'images/asset/'.$request->asset_id.'/image360';

            $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
        }

        $save = $assetImage360->update([
            'asset_id'          => $request->asset_id,
            'image'             => $uploaded,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $assetImage360;
        } else {
            return response()->json([
                'message' => 'Failed Update Image',
            ], 422);
        }
    }

    public function deleteAssetImage360($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $image = AssetImage360::findOrFail($id);       

        if (Storage::disk('public')->exists($image->image360)) {
            Storage::delete($image->image360);
        }

        $delete = $image->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Image',
            ], 422);
        }
    }

    public function assetAreaList($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }
        
        return AssetArea::where('deleted', false)->where('asset_id', $id)
                ->orderBy('id', 'desc')->get();   
    }

    public function storeAssetArea(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate(request(), [
            'name'              => 'required',
            'large'             => 'required',
            'area'              => 'required'
        ]);

        $save = AssetArea::create([
            'asset_id'          => $id,
            'name'              => $request->name,
            'large'             => $request->large,
            'area'              => $request->area,
            'created_by'        => Auth::user()->id,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showAssetArea($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        return AssetArea::findOrFail($id);
    }

    public function updateAssetArea($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $assetArea = AssetArea::findOrFail($id);

        $this->validate(request(), [
            'asset_id'          => 'required|exists:assets,id',
            'name'              => 'required',
            'large'             => 'required',
            'area'              => 'required'
        ]);

        $save = $assetArea->update([
            'asset_id'          => $request->asset_id,
            'name'              => $request->name,
            'large'             => $request->large,
            'area'              => $request->area,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $assetArea;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteAssetArea($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        $image = AssetArea::findOrFail($id);

        $delete = $image->update([
            'deleted'       => 1,
            'updated_by'    => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    /**
     * Print PDF picking list
     *
     * @param  string  $do_code
     * @return \PDF
     */
    public function qrcode_pdf()
    {
        Auth::user()->cekRoleModules(['asset-pdf']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        $assets = Asset::find($data);

        // if (request()->has('sort_field')) {
        //     $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
        //     $asset->orderBy(request()->input('sort_field'), $sort_order);
        // } else {
        //     $asset->orderBy('code', 'asc');
        // }

        // $asset = $asset->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
        //     ->appends(Input::except('page'))
        //     ->toArray();

        $pdf = PDF::loadview('pdf.asset', compact('assets'))->setPaper('a4', 'potrait');
        $pdf->output();
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 10, array(0, 0, 0));
        
        return $pdf->download('asset_barcode.pdf');
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['asset-create']);

        // return Excel::download(new AssetExport, 'asset.xlsx');

        $file = public_path()."/assets/upload_asset.xlsx";
        $headers = array('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',);

        return response()->download($file, 'upload_asset.xlsx', $headers);
    }

    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new AssetImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }

    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['asset-create']);

        $data = AssetFailedUpload::where('uploaded_by', Auth::user()->id)
                ->orderBy('created_at', 'asc')
                ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
                ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['asset-create']);

        $data = AssetFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new AssetInvalidExport($data), 'invalid_asset_upload.xlsx');

        // delete failed upload data
        AssetFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['asset-create']);

        $data = AssetFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        AssetFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }

    public function exportAsset()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        $asset = Asset::with([
            'createdBy', 'updatedBy', 'material', 'asset_type', 'location.location_type', 'supplier', 'straightline', 
            'doubledecline', 'valuationasset', 'responsible_person', 'plant', 'status', 'groupmaterial'
        ])->find($data);

        $list_asset = [];
        $nourut = 0;

        foreach($asset as $data_asset){
            try {

                if(!empty($data_asset['purchase_date']) && (!empty($data_asset['valuationasset']) && $data_asset['valuationasset']['is_depreciation'] == true)){

                    $List_depre_type = [
                        "straight_line", "double_decline"
                    ];

                    if(request()->has('depre_type')){
                        
                        $List_depre_type = request()->input('depre_type');

                    }
                    
                    // foreach($List_depre_type as $depre_type){

                        $calc_depre = [
                            'nilai_awal_penyusutan' => 0,
                            'nilai_penyusutan'      => 0,
                            'akumulasi_penyusutan'  => 0,
                            'nilai_buku'            => 0,
                            'tipe_penyusutan'       => '-'
                        ];
                        
                        // if ($depre_type === "straight_line" ){

                            $calc_depre = $this->straight_line_calculation($data_asset);
                            // $calc_depre["tipe_penyusutan"] = 'Straight Line';
            
                        // }else{
            
                        //     $calc_depre = $this->double_decline_calculation($data_asset);
                        //     // $calc_depre["tipe_penyusutan"] = 'Double Decline';
                        // }

                        $data_asset["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
                        $data_asset["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
                        $data_asset["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
                        $data_asset["nilai_buku"] = $calc_depre["nilai_buku"];
                        $data_asset["tipe_penyusutan"] = $calc_depre["tipe_penyusutan"];
                        $data_asset["nourut"] = $nourut;

                        $list_asset[] = $data_asset;
                        $nourut++;
                    // }
                }else{

                    $calc_depre = [
                        'nilai_awal_penyusutan' => 0,
                        'nilai_penyusutan'      => 0,
                        'akumulasi_penyusutan'  => 0,
                        'nilai_buku'            => 0,
                        'tipe_penyusutan'       => '-'
                    ];

                    $data_asset["nilai_awal_penyusutan"] = $calc_depre["nilai_awal_penyusutan"];
                    $data_asset["nilai_penyusutan"] = $calc_depre["nilai_penyusutan"];
                    $data_asset["akumulasi_penyusutan"] = $calc_depre["akumulasi_penyusutan"];
                    $data_asset["nilai_buku"] = $calc_depre["nilai_buku"];
                    $data_asset["tipe_penyusutan"] = $calc_depre["tipe_penyusutan"];
                    $data_asset["nourut"] = $nourut;

                    $list_asset[] = $data_asset;
                    $nourut++;
                }
            } catch(\Exception $ex) {
                return response()->json([
                    'message' => 'ERROR : Cannot hash ID. '.$ex->getMessage(),
                ], 422);
            }
        }

        $header = array(
            [
                'No', 'Status', 'Asset Number','Description','Material', 'Serial', 'Material Group', 
                'Depreciation Date', 'Purchased Price', 'Depreciate (Yearly)', 'Total Depreciated Value', 
                'Book Value', 'Cost Center', 'Group Owner', 'Valuation Type', 'Plant', 'Location', 'Retired Date',
                'Retired Reason'
            ]
        );

        $i=1;
        foreach($list_asset as $asset) {
            $header[] = [
                $i++,
                $asset->status ? $asset->status->name : '',
                $asset->code,
                $asset->description,
                $asset->material ? $asset->material->description : '',
                $asset->serial_number,
                $asset->groupmaterial ? $asset->groupmaterial->description : '',
                $asset->purchase_date,
                number_format($asset->purchase_cost,2,".",","),
                number_format($asset->nilai_penyusutan,2,".",","),
                number_format($asset->akumulasi_penyusutan,2,".",","),
                number_format($asset->nilai_buku, 2,".",","),
                $asset->cost_center ? $asset->cost_center->description : '',
                $asset->valuation_group ? $asset->valuation_group->description : '',
                $asset->valuationasset ? $asset->valuationasset->name : '',
                $asset->plant ? $asset->plant->description : '',
                $asset->location ? $asset->location->address : '',
                $asset->retired_reason ? $asset->retired_reason->created_at : '',
                $asset->retired_reason ? $asset->retired_reason->name : ''
            ];
        }

        $export = new ExportFromArray($header);
        return Excel::download($export, 'Asset.xlsx');
    }

    public function assetRequestForRetired($id, Request $request)
    {
        Auth::user()->cekRoleModules(['asset-status-change']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $this->validate(request(), [
            'retired_reason_id'     => 'nullable|exists:retired_reasons,id',
            'retired_date'          => 'nullable|date',
            'retired_remarks'       => 'nullable',
        ]);

        $asset = Asset::findOrFail($id);

        if ($request->retired_date) {
            $retired_date = Carbon::parse($request->retired_date)->format('Y-m-d');
            $awal_bulan = Carbon::createFromFormat('Y-m-d', date('Y-m-01'));
            if ($retired_date < $awal_bulan) {
                return response()->json([
                    'message' => 'Retired date can not less than the start of this month. Because it will affect asset depreciation',
                ], 422);
            }
        }

        $asset_active = AssetStatus::whereRaw('LOWER(name) = ?', 'active')->first();
        if (!$asset_active) {
            return response()->json([
                'message' => 'Master asset with "active" status not found',
            ], 422);
        }

        $asset_pending = AssetStatus::whereRaw('LOWER(name) = ?', 'pending retired')->first();
        if (!$asset_pending) {
            return response()->json([
                'message' => 'Master asset with "pending retired" status not found',
            ], 422);
        }

        if ($asset->status_id != $asset_active->id) {
            return response()->json([
                'message' => 'Your current asset status is not "active". Please change it to active.',
            ], 422);
        }

        $save = $asset->update([
            'status_id'    => $asset_pending->id,
            'retired_reason_id'    => $request->retired_reason_id,
            'retired_date'    => isset($retired_date) ? $retired_date : null,
            'retired_remarks'    => $request->retired_remarks,
        ]);
        
        if ($save) {

            $asset->id_hash = HashId::encode($asset->id);

            return $asset;

        } else {
            return response()->json([
                'message' => 'Failed to change asset status',
            ], 422);
        }
    }

    public function assetRequestForActivation(Request $request)
    {
        Auth::user()->cekRoleModules(['asset-status-change']);

        $asset_draft = AssetStatus::whereRaw('LOWER(name) = ?', 'draft')->first();
        if (!$asset_draft) {
            return response()->json([
                'message' => 'Master asset with "draft" status not found',
            ], 422);
        }

        $asset_pending = AssetStatus::whereRaw('LOWER(name) = ?', 'pending activation')->first();
        if (!$asset_pending) {
            return response()->json([
                'message' => 'Master asset with "pending activation" status not found',
            ], 422);
        }

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:assets,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {

                $asset = Asset::findOrFail($ids);

                if ($asset->status_id != $asset_draft->id) {
                    return response()->json([
                        'message' => 'Your current asset status is not "draft". Please change it to draft.',
                    ], 422);
                }

                $save = $asset->update([
                    'status_id'    => $asset_pending->id,
                ]);

            }

            DB::commit();

            return response()->json([
                'message' => 'Success request for activation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed to request for activation',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
        
    }

    public function approvalAssetActivation($status)
    {
        Auth::user()->cekRoleModules(['asset-status-activation']);

        if ($status == 1) { 
            // approved, status change to active
            $option = 'approve';
            $status = AssetStatus::whereRaw('LOWER(name) = ?', 'active')->first();
            if (!$status) {
                return response()->json([
                    'message' => 'Master asset with "active" status not found',
                ], 422);
            }
        } else {
            // rejected, status change to draft
            $option = 'reject';
            $status = AssetStatus::whereRaw('LOWER(name) = ?', 'draft')->first();
            if (!$status) {
                return response()->json([
                    'message' => 'Master asset with "draft" status not found',
                ], 422);
            }
        }

        $asset_pending = AssetStatus::whereRaw('LOWER(name) = ?', 'pending activation')->first();
        if (!$asset_pending) {
            return response()->json([
                'message' => 'Master asset with "pending activation" status not found',
            ], 422);
        }

        $data = [];
        foreach (request()->id as $key => $ids) {
            try {
                $ids = HashId::decode($ids);
            } catch(\Exception $ex) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'id.'.$key  => ['id not found']
                    ]
                ], 422);
            }

            $data[] = $ids;
        }

        request()->merge(['id' => $data]);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:assets,id',
        ]);

        try {
            DB::beginTransaction();

            foreach (request()->id as $ids) {

                $asset = Asset::findOrFail($ids);

                if ($asset->status_id != $asset_pending->id) {
                    return response()->json([
                        'message' => 'Your current asset status is not "pending activation". Please request for activation first',
                    ], 422);
                }

                $save = $asset->update([
                    'status_id'    => $status->id,
                ]);
        
            }

            DB::commit();

            return response()->json([
                'message' => 'Success ' . $option . ' activation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Failed to request for activation',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function approvalAssetRetired($id, $status)
    {
        Auth::user()->cekRoleModules(['asset-status-approval']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $approve = 0;
        if ($status == 1) { 
            // approved, status change to retired
            $approve = 1;
            $status = AssetStatus::whereRaw('LOWER(name) = ?', 'retired')->first();
            if (!$status) {
                return response()->json([
                    'message' => 'Master asset with "retired" status not found',
                ], 422);
            }
        } else {
            // rejected, status change to active
            $status = AssetStatus::whereRaw('LOWER(name) = ?', 'active')->first();
            if (!$status) {
                return response()->json([
                    'message' => 'Master asset with "active" status not found',
                ], 422);
            }
        }

        $asset = Asset::findOrFail($id);

        $asset_pending = AssetStatus::whereRaw('LOWER(name) = ?', 'pending retired')->first();
        if (!$asset_pending) {
            return response()->json([
                'message' => 'Master asset with "pending retired" status not found',
            ], 422);
        }

        if ($asset->status_id != $asset_pending->id) {
            return response()->json([
                'message' => 'Your current asset status is is not "pending retired". Please request for retired first',
            ], 422);
        }

        if ($approve) {
            $save = $asset->update([
                'status_id'    => $status->id,
            ]);
        } else {
            $save = $asset->update([
                'status_id'    => $status->id,
                'retired_reason_id'    => null,
                'retired_date'    => null,
                'retired_remarks'    => null,
            ]);
        }
        
        if ($save) {

            $asset->id_hash = HashId::encode($asset->id);

            return $asset;

        } else {
            return response()->json([
                'message' => 'Failed to change asset status',
            ], 422);
        }
    }

    public function cronDepresiasi()
    {
        $active = AssetStatus::where(DB::raw("LOWER(name)"), '=', "active")->where('deleted', false)->first();
        $pending = AssetStatus::where(DB::raw("LOWER(name)"), '=', "pending retired")->where('deleted', false)->first();

        $asset = Asset::with('groupmaterial')->where('deleted', false)->whereIn('status_id', [$active->id, $pending->id])->get();

        $now = Carbon::now()->addMonth(-1);

        $insert_array = [];
        $depreciation_table = [];
        $double_dec_table = [];

        $temp = 0;

        foreach ($asset as $a) {
            $asset_depre = AssetDepreciation::where('asset_id', $a->id)->where('depre_type', 'sl')->first();

            //straightline

            $id_depreciation = $a->straight_line;
            if (!$id_depreciation) {
                continue;
            }

            if (!$a->purchase_date) {
                continue;
            }

            $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $id_depreciation)
            ->whereDate('period', '<=', $a->purchase_date)
            ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

            if (!$data_depre_period) {
                continue;
            }

            $nilai_sisa = $a->salvage_value;

            $harga_beli = $a->purchase_cost;
            if (!$harga_beli) {
                continue;
            }

            $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;
            if ($umur_ekonomis == 0 || $umur_ekonomis == null) {
                continue;
            }

            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;//Pertahun
            $besar_penyusutan_perbulan = $besar_penyusutan / 12;

            if (!$asset_depre) {

                $depreciation_table[$temp] = [
                    'asset_id'               => $a->id,
                    'number'                => 0,
                    'depre_type'            => 'sl',
                    'month'                 => "-",
                    'year'                  => "-",
                    'depreciation'          => 0,
                    'total_depreciation'    => 0,
                    'book_value'            => $harga_beli,
                    'material_group_id'     => $a->group_material_id,
                    'gl_account_id'         => isset($a->groupmaterial->account_id) ? $a->groupmaterial->account_id : null,
                    'cost_center_id'        => $a->cost_center_id,
                    'location_id'           => isset($a->location_id) ? $a->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];

                $temp++;

                $purchase_date = Carbon::parse($a->purchase_date);

                $i = 1;
                $id = 0;
                while ($purchase_date < $now) {
                    $akumulasi = $besar_penyusutan_perbulan * ($i);
                    $date = $purchase_date->addMonth(1);

                    $depreciation_table[$temp] = [
                        'asset_id'               => $a->id,
                        'number'                => $i,
                        'depre_type'            => 'sl',
                        'month'                 => $date->format("M"),
                        'year'                  => $date->format("Y"),
                        'depreciation'          => $besar_penyusutan_perbulan,
                        'total_depreciation'    => $akumulasi,
                        'book_value'            => $harga_beli - $akumulasi,
                        'material_group_id'     => $a->group_material_id,
                        'gl_account_id'         => isset($a->groupmaterial->account_id) ? $a->groupmaterial->account_id : null,
                        'cost_center_id'        => $a->cost_center_id,
                        'location_id'           => isset($a->location_id) ? $a->location_id : null,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                    ];

                    $i++;
                    $id++;
                    $temp++;
                }

            } else {
                $asset_exist = AssetDepreciation::where('asset_id', $a->id)->where('depre_type', 'sl')
                ->orderBy('number', 'desc')->first();
                $insert_sl = true;
                if (!$asset_exist) {
                    $insert_sl = false;
                }
                $day = Carbon::parse($a->purchase_date)->format("d");
                $month = $asset_exist->month == '-' ? date('M') : $asset_exist->month;
                $year = $asset_exist->year == '-' ? date('Y') : $asset_exist->year;
                $date = strftime("%F", strtotime($year."-".$month."-".$day));
                $insert_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
                $number = $asset_exist->number;
                $akumulasi = $asset_exist->total_depreciation;
                $new_akumulasi = $akumulasi + $besar_penyusutan_perbulan;
                $new_number = $number+1;
                if ($month === date('M') && $year === date('Y')) {
                    $insert_sl = false;
                }

                if ($insert_sl) {
                    $insert = [
                        'asset_id'              => $a->id, 
                        'number'                => $new_number,
                        'depre_type'            => 'sl',
                        'index_dd'              => null,
                        'month'                 => date('M', strtotime($insert_date)),
                        'year'                  => date('Y', strtotime($insert_date)),
                        'depreciation'          => $besar_penyusutan_perbulan,
                        'total_depreciation'    => $new_akumulasi,
                        'book_value'            => $harga_beli - $new_akumulasi,
                        'material_group_id'     => $a->group_material_id,
                        'gl_account_id'         => isset($a->groupmaterial->account->id) ? $a->groupmaterial->account->id : null,
                        'cost_center_id'        => $a->cost_center_id,
                        'location_id'           => isset($a->location_id) ? $a->location_id : null,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                    ];
                    array_push($insert_array, $insert);
                }
            }


        }

        $temp = 0;
        foreach ($asset as $data_asset) {
            // double decline
            $asset_depre = AssetDepreciation::where('asset_id', $data_asset->id)->where('depre_type', 'dd')->first();

            $double_decline = $data_asset->double_decline;
            if (!$double_decline) {
                continue;
            }

            if (!$data_asset->purchase_date) {
                continue;
            }

            $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $double_decline)
                ->whereDate('period', '<=', $data_asset->purchase_date)
                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

            if (!$data_depre_period) {
                continue;
            }

            $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;

            $harga_beli_awal = isset($data_asset->purchase_cost) ? $data_asset->purchase_cost : 0;
            if ($harga_beli_awal == 0) {
                continue;
            }
            $rates = $data_depre_period->rates;
            $nilai_buku = $harga_beli_awal;
            $now = Carbon::now()->addMonth(-1);

            if (!$asset_depre) {

                $double_dec_table[$temp] = [
                    'asset_id'              => $data_asset->id,
                    'number'                => 0,
                    'index_dd'              => 0,
                    'depre_type'            => 'dd',
                    'month'                 => "-",
                    'year'                  => "-",
                    'depreciation'          => 0,
                    'total_depreciation'    => 0,
                    'book_value'            => $nilai_buku,
                    'material_group_id'     => $data_asset->group_material_id,
                    'gl_account_id'         => isset($data_asset->groupmaterial->account_id) ? $data_asset->groupmaterial->account_id : null,
                    'cost_center_id'        => $data_asset->cost_center_id,
                    'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];

                $nourut = 1;
                $aku_penyusutan_bulanIni = 0;
                $nilai_buku_bulanIni = $harga_beli_awal;
                $purchase_date = Carbon::parse($data_asset->purchase_date);
                $temp++;

                $stop = false;

                for($i = 1; $i <= $umur_ekonomis; $i++){

                    if ($stop) {
                        continue;
                    }
                    
                    $harga_beli_tahunIni = $nilai_buku;

                    $penyusutan_tahunIni = 0;

                    if ($i == $umur_ekonomis) {
                        $penyusutan_tahunIni = $harga_beli_tahunIni;
                    } else {
                        $penyusutan_tahunIni = $harga_beli_tahunIni * ($rates / 100); // * ($jml_month / 12);
                    }

                    $nilai_buku = $nilai_buku - $penyusutan_tahunIni;

                    $penyusutan_perBulan = $penyusutan_tahunIni / 12; //$jml_month;

                    for($j = 1; $j <= 12; $j++){

                        if ($purchase_date > $now) {
                            $stop = true;
                        }

                        if ($stop) {
                            continue;
                        }

                        $purchase_date->addMonths(1);

                        $harga_beli_bulanIni = $nilai_buku_bulanIni;
                        $nilai_buku_bulanIni = $nilai_buku_bulanIni - $penyusutan_perBulan;

                        $double_dec_table[$temp] = [
                            'asset_id'              => $data_asset->id,
                            'number'                => $nourut,
                            'index_dd'              => $i,
                            'depre_type'            => 'dd',
                            'month'                 => $purchase_date->format("M"),
                            'year'                  => $purchase_date->year,
                            'depreciation'          => $penyusutan_perBulan,
                            'total_depreciation'    => $aku_penyusutan_bulanIni,
                            'book_value'            => $nilai_buku_bulanIni,
                            'material_group_id'     => $data_asset->group_material_id,
                            'gl_account_id'         => isset($data_asset->groupmaterial->account->id) ? $data_asset->groupmaterial->account->id : null,
                            'cost_center_id'        => $data_asset->cost_center_id,
                            'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                            'created_at'            => Carbon::now(),
                            'updated_at'            => Carbon::now(),
                        ];
                        
                        $aku_penyusutan_bulanIni = $aku_penyusutan_bulanIni + $penyusutan_perBulan;
                        $nourut++;
                        $temp++;
                    }

                }    
            } else {
                $asset_exist = AssetDepreciation::where('asset_id', $data_asset->id)->where('depre_type', 'dd')
                ->orderBy('number', 'desc')->first();
                if (!$asset_exist) {
                    continue;
                }
                $day = Carbon::parse($data_asset->purchase_date)->format("d");
                $month = $asset_exist->month == '-' ? date('M') : $asset_exist->month;
                $year = $asset_exist->year == '-' ? date('Y') : $asset_exist->year;
                $date = strftime("%F", strtotime($year."-".$month."-".$day));
                $insert_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
                $number = $asset_exist->number;
                $akumulasi = $asset_exist->total_depreciation;
                $book_value = $asset_exist->book_value;
                $index_dd = $asset_exist->index_dd;
                if ($index_dd == $umur_ekonomis) {
                    $besar_penyusutan_perbulan = $book_value / 12;
                } else {
                    $besar_penyusutan_perbulan = $asset_exist->depreciation;
                }
                $new_akumulasi = $akumulasi + $besar_penyusutan_perbulan;
                $new_number = $number+1;
                
                if ($month === date('M') && $year === date('Y')) {
                    continue;
                }

                $insert = [
                    'asset_id'              => $data_asset->id, 
                    'number'                => $new_number,
                    'depre_type'            => 'dd',
                    'index_dd'              => $index_dd,
                    'month'                 => date('M', strtotime($insert_date)),
                    'year'                  => date('Y', strtotime($insert_date)),
                    'depreciation'          => $besar_penyusutan_perbulan,
                    'total_depreciation'    => $new_akumulasi,
                    'book_value'            => $book_value - $besar_penyusutan_perbulan,
                    'material_group_id'     => $data_asset->group_material_id,
                    'gl_account_id'         => isset($data_asset->groupmaterial->account->id) ? $data_asset->groupmaterial->account->id : null,
                    'cost_center_id'        => $data_asset->cost_center_id,
                    'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];
                array_push($insert_array, $insert);
            }

        }

        try {
            DB::beginTransaction();

            if (!empty($insert_array)) {
                AssetDepreciation::insert($insert_array);
            }
            if (isset($double_dec_table)) {
                AssetDepreciation::insert($double_dec_table);
            }
            if (isset($depreciation_table)) {
                AssetDepreciation::insert($depreciation_table);
            }

            DB::commit();

            return response()->json([
                'insert_array' => $insert_array,
                'double_decline' => isset($double_dec_table) ? $double_dec_table : null,
                'straightline' => isset($depreciation_table) ? $depreciation_table : null,
                'message' => 'Success cron asset depreciation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Fail to cron asset depreciation',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }

    public function deleteAssetDepresiasi($id)
    {
        Auth::user()->cekRoleModules(['asset-update']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $delete = AssetDepreciation::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function showAssetDepresiasiSL($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $data_asset = Asset::findOrFail($id);

        $depresiasi = AssetDepreciation::with(['asset', 'material_group', 'gl_account.account_group', 'cost_center', 'location'])
        ->where('asset_id', $id)->where('depre_type', 'sl')->orderBy('number')->get();

        $latest = AssetDepreciation::where('asset_id', $id)->where('depre_type', 'sl')
        ->orderBy('number', 'desc')->first();

        $penyusutan_tahun_ini = AssetDepreciation::where('asset_id', $id)->where('depre_type', 'sl')
        ->where('year', date('Y'))->orderBy('number', 'desc')->get();

        $depre_this_year = 0;
        if (count($penyusutan_tahun_ini) > 0) {
            foreach ($penyusutan_tahun_ini as $p) {
                $depre_this_year += $p->depreciation;
            }
        }

        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $data_asset->straight_line)
        ->whereDate('period', '<=', $data_asset->purchase_date)
        ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

        $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;
        $harga_beli = $data_asset->purchase_cost;
        $nilai_sisa = $data_asset->salvage_value;
        if ($umur_ekonomis == 0) {
            $besar_penyusutan = 0;
        } else {
            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;
        }
        $besar_penyusutan_perbulan = $besar_penyusutan / 12;

        $data_straight_line = [
            'umur_ekonomis'         => $umur_ekonomis,
            'harga_beli'            => $harga_beli,
            'nilai_sisa'            => $nilai_sisa,
            'akumulasi_penyusutan'  => isset($latest->total_depreciation) ? $latest->total_depreciation : null,
            'nilai_buku_saat_ini'   => isset($latest->book_value) ? $latest->book_value : null,
            'penyusutan_pertahun'   => $besar_penyusutan,
            'penyusutan_perbulan'   => $besar_penyusutan_perbulan,
            'penyusutan_tahun_ini'  => $depre_this_year,
            'purchase_date'         => $data_asset->purchase_date,
            'table_straightline'    => $depresiasi,
        ];

        return $data_straight_line;
    }

    public function showAssetDepresiasiDD($id)
    {
        Auth::user()->cekRoleModules(['asset-view']);

        try {
            $id = HashId::decode($id);
        } catch(\Exception $ex) {
            return response()->json([
                'message' => 'ID is not valid. ERROR:'.$ex->getMessage(),
            ], 422);
        }

        $data_asset = Asset::findOrFail($id);

        $depresiasi = AssetDepreciation::with(['asset', 'material_group', 'gl_account.account_group', 'cost_center', 'location'])
        ->where('asset_id', $id)->where('depre_type', 'dd')->orderBy('number')->get();

        $latest = AssetDepreciation::where('asset_id', $id)->where('depre_type', 'dd')
        ->orderBy('number', 'desc')->first();

        $penyusutan_tahun_ini = AssetDepreciation::where('asset_id', $id)->where('depre_type', 'dd')
        ->where('year', date('Y'))->orderBy('number', 'desc')->get();

        $depre_this_year = 0;
        if (count($penyusutan_tahun_ini) > 0) {
            foreach ($penyusutan_tahun_ini as $p) {
                $depre_this_year += $p->depreciation;
            }
        }

        $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $data_asset->double_decline)
        ->whereDate('period', '<=', $data_asset->purchase_date)
        ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

        $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;
        $harga_beli = $data_asset->purchase_cost;
        $nilai_sisa = $data_asset->salvage_value;
        if ($umur_ekonomis == 0) {
            $besar_penyusutan = 0;
        } else {
            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;
        }
        $besar_penyusutan_perbulan = $besar_penyusutan / 12;

        $data_double_dec = [
            'umur_ekonomis'         => $umur_ekonomis,
            'harga_beli'            => $harga_beli,
            'nilai_sisa'            => $nilai_sisa,
            'akumulasi_penyusutan'  => isset($latest->total_depreciation) ? $latest->total_depreciation : null,
            'nilai_buku_saat_ini'   => isset($latest->book_value) ? $latest->book_value : null,
            'penyusutan_pertahun'   => $besar_penyusutan,
            'penyusutan_perbulan'   => $besar_penyusutan_perbulan,
            'penyusutan_tahun_ini'  => $depre_this_year,
            'purchase_date'         => $data_asset->purchase_date,
            'table_doubledecline'   => $depresiasi,
        ];

        return $data_double_dec;

    }

    public function showAllDepreciation()
    {
        Auth::user()->cekRoleModules(['asset-view']);

        $depre = (new AssetDepreciation)->newQuery();

        $depre = $depre->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return $depre;

    }

}
