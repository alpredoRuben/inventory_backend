<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Material\MaterialAccountTemplateExport;
use App\Exports\Material\MaterialAccountInvalidExport;
use App\Imports\Material\MaterialAccountImport;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;
use App\Models\MaterialAccountFailedUpload;

class MaterialAccountController extends Controller
{
	public function materialAccount()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = (new MaterialAccount)->newQuery();

        $material->with([
            'material', 'material.material_type', 'material.group_material', 'plant',
            'valuation_type', 'valuation_class'
        ]);

        // only material not softdeleted
        $material->whereHas('material', function ($q){
            $q->where('deleted', false);
        });

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->whereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        // group_material filter
        if (request()->has('group_material')) {
            $group_material = request()->input('group_material');
            $material->whereHas('material', function ($q) use ($group_material){
                $q->whereIn('materials.group_material_id', $group_material);
            });
        }

        // material_type filter
        if (request()->has('material_type')) {
            $material_type = request()->input('material_type');
            $material->whereHas('material', function ($q) use ($material_type){
                $q->whereIn('materials.material_type_id', $material_type);
            });
        }

        // material filter
        if (request()->has('material')) {
            $material->whereIn('material_id', request()->input('material'));
        }

        // plant filter
        if (request()->has('plant')) {
            $material->whereIn('plant_id', request()->input('plant'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $material->join('materials','material_accounts.material_id','=','materials.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('materials.code',$sort_order);
                break;

                case 'plant':
                    $material->join('plants','material_accounts.plant_id','=','plants.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('plants.code',$sort_order);
                break;

                case 'group_material':
                    $material->leftJoin('materials','material_accounts.material_id','=','materials.id');
                    $material->leftJoin('group_materials','materials.group_material_id','=','group_materials.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('group_materials.material_group',$sort_order);
                break;

                case 'description':
                    $material->join('materials','material_accounts.material_id','=','materials.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('materials.description',$sort_order);
                break;

                case 'material_type':
                    $material->leftJoin('materials','material_accounts.material_id','=','materials.id');
                    $material->leftJoin('material_types','materials.material_type_id','=','material_types.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('material_types.code',$sort_order);
                break;
                
                case 'valuation_type':
                    $material->leftJoin('valuation_types','material_accounts.valuation_type_id','=','valuation_types.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('valuation_types.code',$sort_order);
                break;

                case 'valuation_class':
                    $material->leftJoin('valuation_classes','material_accounts.valuation_class_id','=','valuation_classes.id');
                    $material->select('material_accounts.*');
                    $material->orderBy('valuation_classes.code',$sort_order);
                break;

                default:
                    $material->orderBy($sort_field, $sort_order);
                break;
            }
            
        } else {
            $material->orderBy('id', 'desc');
        }

        return $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }

    public function materialAccountList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialAccount::with(['material', 'plant', 'valuation_type', 'valuation_class'])
            ->where('material_id', $id)->orderBy('id', 'desc')->get();   
    }

    public function storeMaterialAccount(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'plant_id'          => 'required|exists:plants,id',
            'valuation_type_id' => 'nullable|exists:valuation_types,id',
            'valuation_class_id'=> 'required|exists:valuation_classes,id',
            'price_controll'    => 'required|numeric|min:0|max:2',//0. MAP, 1. Standard, 2. LIFO/FIFO
        ]);

        if ($request->price_controll == 0) {
            $this->validate(request(), [
                'moving_price'      => 'required|numeric',
            ]);
        } else if ($request->price_controll == 1) {
            $this->validate(request(), [
                'standart_price'      => 'required|numeric',
            ]);
        }

        $save = MaterialAccount::create([
            'material_id'       => $id,
            'plant_id'          => $request->plant_id,
            'valuation_type_id' => $request->valuation_type_id,
            'valuation_class_id'=> $request->valuation_class_id,
            'price_controll'    => $request->price_controll,//0. MAP, 1. Standard, 2. LIFO/FIFO
            'moving_price'      => $request->moving_price,
            'standart_price'    => $request->standart_price
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialAccount($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialAccount::with(['material', 'plant', 'valuation_type', 'valuation_class'])
                ->findOrFail($id);
    }

    public function updateMaterialAccount($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $materialAccount = MaterialAccount::findOrFail($id);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'plant_id'          => 'required|exists:plants,id',
            'valuation_type_id' => 'nullable|exists:valuation_types,id',
            'valuation_class_id'=> 'required|exists:valuation_classes,id',
            'price_controll'    => 'required|numeric|min:0|max:2',//0. MAP, 1. Standard, 2. LIFO/FIFO
        ]);

        if ($request->price_controll == 0) {
            $this->validate(request(), [
                'moving_price'      => 'required|numeric',
            ]);
        } else if ($request->price_controll == 1) {
            $this->validate(request(), [
                'standart_price'      => 'required|numeric',
            ]);
        }

        $save = $materialAccount->update([
            // 'material_id'       => $id,
            'plant_id'          => $request->plant_id,
            'valuation_type_id' => $request->valuation_type_id,
            'valuation_class_id'=> $request->valuation_class_id,
            'price_controll'    => $request->price_controll,//0. MAP, 1. Standard, 2. LIFO/FIFO
            'moving_price'      => $request->moving_price,
            'standart_price'    => $request->standart_price
        ]);

        if ($save) {
            return $materialAccount;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteMaterialAccount($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $delete = MaterialAccount::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteMaterialAccount()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_accounts,id',
        ]);

        foreach (request()->id as $id) {
            $delete = MaterialAccount::find($id)->delete();
        }

        if ($delete) {
            return response()->json([
                'message' => 'Success Delete Data',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['material-create']);

        return Excel::download(new MaterialAccountTemplateExport, 'template_material_account.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new MaterialAccountImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialAccountFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialAccountFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new MaterialAccountInvalidExport($data), 'invalid_material_account_upload.xlsx');

        // delete failed upload data
        MaterialAccountFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialAccountFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        MaterialAccountFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
