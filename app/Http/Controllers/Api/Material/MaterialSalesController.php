<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Material\MaterialSalesTemplateExport;
use App\Exports\Material\MaterialSalesInvalidExport;
use App\Imports\Material\MaterialSalesImport;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;
use App\Models\MaterialSalesFailedUpload;

class MaterialSalesController extends Controller
{
	public function materialSales()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = (new MaterialSales)->newQuery();

        $material->with([
            'material', 'material.material_type', 'material.group_material', 'sales_organization',
            'delivering_plant', 'account_assignment', 'item_category'
        ]);

        // only material not softdeleted
        $material->whereHas('material', function ($q){
            $q->where('deleted', false);
        });

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->whereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        // group_material filter
        if (request()->has('group_material')) {
            $group_material = request()->input('group_material');
            $material->whereHas('material', function ($q) use ($group_material){
                $q->whereIn('materials.group_material_id', $group_material);
            });
        }

        // material_type filter
        if (request()->has('material_type')) {
            $material_type = request()->input('material_type');
            $material->whereHas('material', function ($q) use ($material_type){
                $q->whereIn('materials.material_type_id', $material_type);
            });
        }

        // material filter
        if (request()->has('material')) {
            $material->whereIn('material_id', request()->input('material'));
        }

        // sales_organization filter
        if (request()->has('sales_organization')) {
            $material->whereIn('sales_organization_id', request()->input('sales_organization'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $material->join('materials','material_sales.material_id','=','materials.id');
                    $material->select('material_sales.*');
                    $material->orderBy('materials.code',$sort_order);
                break;

                case 'description':
                    $material->join('materials','material_sales.material_id','=','materials.id');
                    $material->select('material_sales.*');
                    $material->orderBy('materials.description',$sort_order);
                break;

                case 'material_type':
                    $material->join('materials','material_sales.material_id','=','materials.id');
                    $material->join('material_types','materials.material_type_id','=','material_types.id');
                    $material->select('material_sales.*');
                    $material->orderBy('material_types.code',$sort_order);
                break;

                case 'group_material':
                    $material->join('materials','material_sales.material_id','=','materials.id');
                    $material->leftJoin('group_materials','materials.group_material_id','=','group_materials.id');
                    $material->select('material_sales.*');
                    $material->orderBy('group_materials.material_group',$sort_order);
                break;

                case 'sales_organization':
                    $material->join('sales_organizations','material_sales.sales_organization_id','=','sales_organizations.id');
                    $material->select('material_sales.*');
                    $material->orderBy('sales_organizations.description',$sort_order);
                break;

                case 'delivering_plant':
                    $material->leftJoin('plants','material_sales.delivering_plant_id','=','plants.id');
                    $material->select('material_sales.*');
                    $material->orderBy('plants.code',$sort_order);
                break;

                case 'account_assignment':
                    $material->Join('account_assignments','material_sales.account_assignment_id','=','account_assignments.id');
                    $material->select('material_sales.*');
                    $material->orderBy('account_assignments.code',$sort_order);
                break;

                case 'item_category':
                    $material->Join('item_categories','material_sales.item_category_id','=','item_categories.id');
                    $material->select('material_sales.*');
                    $material->orderBy('item_categories.code',$sort_order);
                break;
                
                default:
                    $material->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $material->orderBy('id', 'desc');
        }

        return $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }
    
    public function materialSalesList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialSales::with([
            'material', 'sales_organization', 'delivering_plant',
            'account_assignment', 'item_category'
        ])->where('material_id', $id)->orderBy('id', 'desc')->get();   
    }

    public function storeMaterialSales(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'sales_organization_id' => 'required|exists:sales_organizations,id',
            'delivering_plant_id'   => 'nullable|exists:plants,id',
            'tax_class'             => 'nullable|boolean',//0: No tax; 1: PPN 10%
            'account_assignment_id' => 'required|exists:account_assignments,id',
            'item_category_id'      => 'required|exists:item_categories,id',
        ]);

        $save = MaterialSales::create([
            'material_id'           => $id,
            'sales_organization_id' => $request->sales_organization_id,
            'delivering_plant_id'   => $request->delivering_plant_id,
            'tax_class'             => $request->tax_class ? $request->tax_class : 0,
            'account_assignment_id' => $request->account_assignment_id,
            'item_category_id'      => $request->item_category_id
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialSales($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialSales::with([
            'material', 'sales_organization', 'delivering_plant',
            'account_assignment', 'item_category'
        ])->findOrFail($id);
    }

    public function updateMaterialSales($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $materialSales = MaterialSales::findOrFail($id);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'sales_organization_id' => 'required|exists:sales_organizations,id',
            'delivering_plant_id'   => 'nullable|exists:plants,id',
            'tax_class'             => 'nullable|boolean',//0: No tax; 1: PPN 10%
            'account_assignment_id' => 'required|exists:account_assignments,id',
            'item_category_id'      => 'required|exists:item_categories,id',
        ]);

        $save = $materialSales->update([
            // 'material_id'       => $id,
            'sales_organization_id' => $request->sales_organization_id,
            'delivering_plant_id'   => $request->delivering_plant_id,
            'tax_class'             => $request->tax_class ? $request->tax_class : 0,
            'account_assignment_id' => $request->account_assignment_id,
            'item_category_id'      => $request->item_category_id
        ]);

        if ($save) {
            return $materialSales;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteMaterialSales($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $delete = MaterialSales::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteMaterialSales()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_sales,id',
        ]);

        foreach (request()->id as $id) {
            $delete = MaterialSales::find($id)->delete();
        }

        if ($delete) {
            return response()->json([
                'message' => 'Success Delete Data',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['material-create']);

        return Excel::download(new MaterialSalesTemplateExport, 'template_material_sales.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new MaterialSalesImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialSalesFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialSalesFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new MaterialSalesInvalidExport($data), 'invalid_material_sales_upload.xlsx');

        // delete failed upload data
        MaterialSalesFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialSalesFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        MaterialSalesFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
