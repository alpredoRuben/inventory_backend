<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;

class MaterialController extends Controller
{    
    public function getClassificationForMaterial()
    {
        $type = ClassificationType::where('name', 'Material')->first();

        $classification = (new ClassificationMaterial)->newQuery();

        $classification->with(['parameters']);

        $classification->where('deleted', false)->where('classification_type_id', $type->id);

        if (request()->has('q')) {
            $classification->where(DB::raw("LOWER(name)"), 'LIKE', "%".strtolower(request()->input('q'))."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $classification->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $classification->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $classification->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $classification->paginate(20)->appends(Input::except('page'));
        }
    }

    public function getClassificationForQualityProfile()
    {
        $type = ClassificationType::where('name', 'Quality_Profile')->first();

        $classification = (new ClassificationMaterial)->newQuery();

        $classification->with(['parameters']);

        $classification->where('deleted', false)->where('classification_type_id', $type->id);

        if (request()->has('q')) {
            $classification->where(DB::raw("LOWER(name)"), 'LIKE', "%".strtolower(request()->input('q'))."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $classification->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $classification->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $classification->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $classification->paginate(20)->appends(Input::except('page'));
        }
    }

    public function getClassificationForWorkflow()
    {
        $type = ClassificationType::where('name', 'Workflow_Approval')->first();

        $classification = (new ClassificationMaterial)->newQuery();

        $classification->with(['parameters']);

        $classification->where('deleted', false)->where('classification_type_id', $type->id);

        if (request()->has('q')) {
            $classification->where(DB::raw("LOWER(name)"), 'LIKE', "%".strtolower(request()->input('q'))."%");
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $classification->orderBy(request()->input('sort_field'), $sort_order);
        } else {
            $classification->orderBy('id', 'desc');
        }

        if (request()->has('per_page')) {
            return $classification->paginate(request()->input('per_page'))->appends(Input::except('page'));
        } else {
            return $classification->paginate(20)->appends(Input::except('page'));
        }
    }

    public function material()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = (new Material)->newQuery();

        $material->where('materials.deleted', false)->with([
            'material_type', 'group_material', 'classification', 'base_uom', 'weight_unit_data',
            'volume_uom_data', 'base_mat', 'old_mat', 'createdBy', 'updatedBy', 'material_parameters',
            'images'
        ]);

        $material->with(['material_parameters.classification_parameter']);

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->where(function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(sku_code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(nett_weight)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(gross_weight)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(volume)"), 'LIKE', "%".$q."%");
            });
        }

        // classification filter
        if (request()->has('classification') && is_array(request()->input('classification'))) {
            $material->whereIn('classification_material_id', request()->input('classification'));
        }

        // group_material filter
        if (request()->has('group_material') && is_array(request()->input('group_material'))) {
            $material->whereIn('group_material_id', request()->input('group_material'));
        }

        // material_type filter
        if (request()->has('material_type') && is_array(request()->input('material_type'))) {
            $material->whereIn('material_type_id', request()->input('material_type'));
        }

        // uom filter
        if (request()->has('uom') && is_array(request()->input('uom'))) {
            $material->whereIn('uom_id', request()->input('uom'));
        }

        // material filter
        if (request()->has('material') && is_array(request()->input('material'))) {
            $material->whereIn('id', request()->input('material'));
        }

        // base material filter
        if (request()->has('base_material') && is_array(request()->input('base_material'))) {
            $material->whereIn('base_material', request()->input('base_material'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'group_material':
                    $material->leftJoin('group_materials','materials.group_material_id','=','group_materials.id');
                    $material->select('materials.*');
                    $material->orderBy('group_materials.material_group',$sort_order);
                break;
                
                case 'description':
                    $material->orderBy('description',$sort_order);
                break;

                case 'material_type':
                    $material->join('material_types','materials.material_type_id','=','material_types.id');
                    $material->select('materials.*');
                    $material->orderBy('material_types.code',$sort_order);
                break;

                case 'old_mat':
                    $material->orderBy('code',$sort_order);
                break;

                case 'base_mat':
                    $material->orderBy('code',$sort_order);
                break;

                case 'classification':
                    $material->leftJoin('classification_materials','materials.classification_material_id','=','classification_materials.id');
                    $material->select('materials.*');
                    $material->orderBy('classification_materials.name',$sort_order);
                break;

                default:
                    $material->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $material->orderBy('code', 'asc');
        }

        return $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }

    public function storeMaterial(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate(request(), [
            'material_type_id'  => 'required|exists:material_types,id',
            'code'              => 'required|max:20',
            'description'       => 'required|max:60'
        ]);

        // get material type
        $matType = MaterialType::find($request->material_type_id);

        // if material type external
        if ($matType->number_type) {
            // check lenght
            if (strlen($request->code) < strlen($matType->number_range_from) || strlen($request->code) > strlen($matType->number_range_to)) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code'  => ['Code lenght cant lower than number range from or greater than number range to']
                    ]
                ], 422);
            }

            // check must greater then number range from
            if ($request->code < $matType->number_range_from) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code'  => ['Code lenght cant lower than number range from']
                    ]
                ], 422);
            }

            // check must lower then number range to
            if ($request->code > $matType->number_range_to) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code' => ['Code lenght cant greater than number range to']
                    ]
                ], 422);
            }
        }

        $material = Material::whereRaw('LOWER(code) = ?', strtolower($request->code))->first();

        if ($material) {

            if($material->deleted){
                $save = $material->update([
                    'material_type_id'  => $request->material_type_id,
                    'code'              => $request->code,
                    'description'       => $request->description,
                    'deleted'           => 0,
                    'updated_by'        => Auth::user()->id
                ]);

                return Material::with(['material_type'])->find($material->id);
            }

            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'code' => ['Code already taken']
                ]
            ],422);
        } else {
            $save = Material::create([
                'material_type_id'  => $request->material_type_id,
                'code'              => $request->code,
                'description'       => $request->description,
                'created_by'        => Auth::user()->id,
                'updated_by'        => Auth::user()->id
            ]);

            return Material::with(['material_type'])->find($save->id);;
        }
    }

    public function showMaterial($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return Material::with([
            'material_type',
            'group_material',
            'classification',
            'base_uom',
            'weight_unit_data',
            'volume_uom_data',
            'images',
            'material_parameters',
            'uom_conversion',
            'base_mat',
            'old_mat',
            'createdBy',
            'updatedBy'
        ])
        ->with(['material_parameters.classification_parameter'])
        ->find($id);
    }

    public function showMaterialByCode($code)
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = Material::with([
            'material_type',
            'group_material',
            'classification',
            'base_uom',
            'weight_unit_data',
            'volume_uom_data',
            'images',
            'material_parameters',
            'uom_conversion',
            'base_mat',
            'old_mat',
            'createdBy',
            'updatedBy'
        ])
        ->with(['material_parameters.classification_parameter'])
        ->where('code', $code)
        ->first();

        if (!$material) {
            return response()->json([
                'message'   => 'Data Not Found',
            ], 404);
        }

        return $material;
    }

    public function updateMaterial($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $material = Material::findOrFail($id);

        $this->validate(request(), [
            'material_type_id'  => 'required|exists:material_types,id',
            'code'              => 'required|max:20|unique:materials,code,'. $id .'',
            'description'       => 'required|max:60',
        ]);

        // get material type
        $matType = MaterialType::find($request->material_type_id);

        // if material type external
        if ($matType->number_type) {
            // check lenght
            if (strlen($request->code) < strlen($matType->number_range_from) || strlen($request->code) > strlen($matType->number_range_to)) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code'  => ['Code lenght cant lower than number range from or greater than number range to']
                    ]
                ], 422);
            }

            // check must greater then number range from
            if ($request->code < $matType->number_range_from) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code'  => ['Code lenght cant lower than number range from']
                    ]
                ], 422);
            }

            // check must lower then number range to
            if ($request->code > $matType->number_range_to) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'code' => ['Code lenght cant greater than number range to']
                    ]
                ], 422);
            }
        }

        $save = $material->update([
            'material_type_id'  => $request->material_type_id,
            'code'              => $request->code,
            'description'       => $request->description,
            'updated_by'        => Auth::user()->id
        ]);        

        if ($save) {
            return Material::with(['material_type'])->find($material->id);
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteMaterial($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $delete = Material::findOrFail($id)->update([
            'deleted' => true, 'updated_by' => Auth::user()->id
        ]);

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteMaterial()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:materials,id',
        ]);

        foreach (request()->id as $id) {
            $delete = Material::find($id)->update([
                'deleted' => true, 'updated_by' => Auth::user()->id
            ]);
        }

        if ($delete) {
            return response()->json([
                'message' => 'Success Delete Data',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }
}
