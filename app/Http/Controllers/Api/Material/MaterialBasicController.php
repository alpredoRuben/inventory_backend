<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Material\MaterialBasicTemplateExport;
use App\Exports\Material\MaterialBasicInvalidExport;
use App\Imports\Material\MaterialBasicImport;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;
use App\Models\MaterialFailedUpload;

class MaterialBasicController extends Controller
{
    public function storeMaterialBasic($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'group_material_id' => 'required|exists:group_materials,id',
            // 'classification_material_id' => 'nullable|exists:classification_materials,id',
            'uom_id'            => 'required|exists:uoms,id',
            'sku_code'          => 'nullable|unique:materials,sku_code,'. $id .'',
            'old_material'      => 'nullable|exists:materials,id',
            'base_material'     => 'nullable|exists:materials,id',
            'nett_weight'       => 'nullable|between:0,99.99',
            'gross_weight'      => 'nullable|between:0,99.99',
            'weight_unit'       => 'nullable|exists:uoms,id',
            'volume'            => 'nullable|between:0,99.99',
            'volume_uom'        => 'nullable|exists:uoms,id',
            'serial_profile'    => 'nullable|boolean',
        ]);

        $material = Material::find($id);

        if ($request->old_material == $id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'old_material' => ['Old Material cant same with Material Code']
                ]
            ], 422);
        }

        if ($request->base_material == $id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'base_material' => ['Base Material cant same with Material Code']
                ]
            ], 422);
        }

        $stock = StockMain::where('material_id', $id)->first();

        if ($stock) {
            if ($material->serial_profile != $request->serial_profile) {
                return response()->json([
                    'message'   => 'Data invalid',
                    'errors'    => [
                        'serial_profile'  => ['this material has stock, cant change serial profile']
                    ]
                ], 422);
            }
        }

        $save = $material->update([
            'group_material_id' => $request->group_material_id,
            // 'classification_material_id' => $request->classification_material_id,
            'uom_id'            => $request->uom_id,
            'sku_code'          => $request->sku_code,
            'old_material'      => $request->old_material,
            'base_material'     => $request->base_material,
            'nett_weight'       => $request->nett_weight,
            'gross_weight'      => $request->gross_weight,
            'weight_unit'       => $request->weight_unit,
            'volume'            => $request->volume,
            'volume_uom'        => $request->volume_uom,
            'serial_profile'    => $request->serial_profile ? $request->serial_profile : false,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return Material::with(['base_uom'])->find($id);
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function storeMaterialClassification($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'classification_material_id' => 'nullable|exists:classification_materials,id',
            'parameter'     => 'nullable|array',
        ]);

        $material = Material::find($id);

        $save = $material->update([
            'classification_material_id'    => $request->classification_material_id,
            'updated_by'                    => Auth::user()->id
        ]);

        // Store Parameter if classification selected
        if ($request->classification_material_id) {
            $parameter = $request->input('parameter');
            $material_id = $id;
            
            if (is_array($parameter) || is_object($parameter))
            {
                foreach($parameter as $key => $value)
                {
                    $id_parameter = explode("-",$key)[1];
                    $value_parameter = $value;
                    $insert_param = MaterialParameter::updateOrCreate(
                        [
                            'material_id'   => $material_id,
                            'classification_parameter_id'  => $id_parameter,
                        ],
                        [
                            'value'         => $value_parameter,
                            'created_by'    => Auth::user()->id,
                            'updated_by'    => Auth::user()->id
                        ]
                    );
                }
            }
        }

        if ($save) {
            return Material::with(['material_parameters'])->find($id);
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function materialUomList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialUom::with(['uom'])
                ->where('deleted', false)->where('material_id', $id)
                ->orderBy('id', 'desc')->get();   
    }

    public function storeMaterialUom(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'uom_id'            => 'required|exists:uoms,id',
            'base_value'        => 'required|between:0,99.99',
            'value_conversion'  => 'required|between:0,99.99'
        ]);

        $uom = Uom::find($request->uom_id);
        $material = Material::find($id);
        $cek = MaterialUom::where('material_id', $id)->where('uom_id', $request->uom_id)->first();

        if ($material->uom_id == $request->uom_id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'uom_id' => ['Uom conversion cant same with base uom']
                ]
            ], 422);
        }

        if ($cek) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'uom_id' => ['Conversion using uom '.$uom->description. ' already exist']
                ]
            ], 422);
        }

        $save = MaterialUom::create([
            'material_id'       => $id,
            'uom_id'            => $request->uom_id,
            'base_value'        => $request->base_value,
            'value_conversion'  => $request->value_conversion,
            'created_by'        => Auth::user()->id,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialUom($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialUom::with(['uom'])->with('material.base_uom')->findOrFail($id);
    }

    public function updateMaterialUom($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $materialUom = MaterialUom::findOrFail($id);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'uom_id'            => 'required|exists:uoms,id',
            'base_value'        => 'required|between:0,99.99',
            'value_conversion'  => 'required|between:0,99.99'
        ]);

        $uom = Uom::find($request->uom_id);
        $material = Material::find($materialUom->material_id);
        $cek = MaterialUom::where('material_id', $materialUom->material_id)
        ->where('uom_id', $request->uom_id)
        ->where('id', '<>', $id)
        ->first();

        if ($material->uom_id == $request->uom_id) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'uom_id' => ['Uom conversion cant same with base uom']
                ]
            ], 422);
        }

        if ($cek) {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'uom_id' => ['Conversion using uom '.$uom->description. ' already exist']
                ]
            ], 422);
        }

        $save = $materialUom->update([
            // 'material_id'       => $request->material_id,
            'uom_id'            => $request->uom_id,
            'base_value'        => $request->base_value,
            'value_conversion'  => $request->value_conversion,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $materialUom;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteMaterialUom($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $delete = MaterialUom::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function materialImageList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialImage::where('deleted', false)->where('material_id', $id)
                ->orderBy('id', 'desc')->get();   
    }

    public function storeMaterialImage(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'image'             => 'required|image'
        ]);

        if ($request->has('image')) {
            $image_data = request()->file('image');
            $image_ext  = request()->file('image')->getClientOriginalExtension();
            $image_old  = request()->file('image')->getClientOriginalName();
            $image_name = Auth::user()->id . md5(time().$image_old). "." .$image_ext;
            $image_path = 'images/material/'.$id;

            $uploaded = Storage::disk('public')->putFileAs($image_path, $image_data, $image_name);
        }

        $save = MaterialImage::create([
            'material_id'       => $id,
            'image'             => $uploaded,
            'value'             => $request->value,
            'created_by'        => Auth::user()->id,
            'updated_by'        => Auth::user()->id
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialImage($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialImage::findOrFail($id);
    }

    public function deleteMaterialImage($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $image = MaterialImage::findOrFail($id);       

        if (Storage::disk('public')->exists($image->image)) {
            Storage::delete($image->image);
        }

        $delete = $image->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['material-create']);

        return Excel::download(new MaterialBasicTemplateExport, 'template_material_basic.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new MaterialBasicImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new MaterialBasicInvalidExport($data), 'invalid_material_basic_upload.xlsx');

        // delete failed upload data
        MaterialFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        MaterialFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
