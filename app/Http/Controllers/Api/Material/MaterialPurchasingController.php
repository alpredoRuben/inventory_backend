<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;

class MaterialPurchasingController extends Controller
{
    public function materialPurchasingList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialPlant::where('material_id', $id)
                ->whereNotNull('proc_group_id')
                ->with(['material', 'plant', 'procurement_group', 'unit_of_purchase'])
                ->orderBy('id', 'desc')->get();
    }

    public function storeMaterialPurchasing($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);
        
        $this->validate(request(), [
            'plant_id'          => 'required|exists:plants,id',
            'proc_type'         => 'required|boolean',
            'proc_group_id'     => 'required|exists:procurement_groups,id',
            'unit_of_purchase_id' => 'nullable|exists:uoms,id'
        ]);

        $save = MaterialPlant::updateOrCreate(
            [
                'material_id'       => $id,
                'plant_id'          => $request->plant_id
            ],
            [               
                'proc_type'         => $request->proc_type,
                'proc_group_id'     => $request->proc_group_id,
                'unit_of_purchase_id' => $request->unit_of_purchase_id
            ]
        );

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }   
    }

    public function showMaterialPurchasing($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialPlant::with([
            'material', 'plant', 'procurement_group', 'unit_of_purchase'
        ])->findOrFail($id);
    }

    public function deleteMaterialPurchasing($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $mrp = MaterialPlant::find($id);

        if (!$mrp) {
            return response()->json([
                'message' => 'Data Not Found',
            ], 404);
        }

        if (count($mrp->reservation)) {
            return response()->json([
                'message' => 'Data Used in reservation cant delete',
            ], 422);
        }

        $delete = $mrp->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }
}
