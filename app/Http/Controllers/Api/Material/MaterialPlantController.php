<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Material\MaterialPlantTemplateExport;
use App\Exports\Material\MaterialPlantInvalidExport;
use App\Imports\Material\MaterialPlantImport;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;
use App\Models\MaterialPlantFailedUpload;

class MaterialPlantController extends Controller
{
	public function materialPlant()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = (new MaterialPlant)->newQuery();

        $material->with([
            'material', 'material.material_type', 'material.group_material', 'plant', 'procurement_group',
            'mrp', 'profit_center', 'product_champion', 'quality_profile'
        ]);

        // only material not softdeleted
        $material->whereHas('material', function ($q){
            $q->where('deleted', false);
        });

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->whereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        // group_material filter
        if (request()->has('group_material')) {
            $group_material = request()->input('group_material');
            $material->whereHas('material', function ($q) use ($group_material){
                $q->whereIn('materials.group_material_id', $group_material);
            });
        }

        // material_type filter
        if (request()->has('material_type')) {
            $material_type = request()->input('material_type');
            $material->whereHas('material', function ($q) use ($material_type){
                $q->whereIn('materials.material_type_id', $material_type);
            });
        }

        // material filter
        if (request()->has('material')) {
            $material->whereIn('material_id', request()->input('material'));
        }

        // plant filter
        if (request()->has('plant')) {
            $material->whereIn('plant_id', request()->input('plant'));
        }

        // proc type filter
        if (request()->has('proc_type')) {
            $material->whereIn('proc_type', request()->input('proc_type'));
        }

        // proc group filter
        if (request()->has('procurement_group')) {
            $material->whereIn('proc_group_id', request()->input('procurement_group'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $material->join('materials','materials.id','=','material_plants.material_id');
                    $material->select('material_plants.*');
                    $material->orderBy('materials.code',$sort_order);
                break;

                case 'description':
                    $material->join('materials','materials.id','=','material_plants.material_id');
                    $material->select('material_plants.*');
                    $material->orderBy('materials.description',$sort_order);
                break;

                case 'material_type':
                    $material->join('materials','material_plants.material_id','=','materials.id');
                    $material->join('material_types','materials.material_type_id','=','material_types.id');
                    $material->select('material_plants.*');
                    $material->orderBy('material_types.code',$sort_order);
                break;
                
                case 'group_material':
                    //$material->join
                    $material->join('materials', function($joint) {
                        $joint->on('materials.id','=','material_plants.material_id');
                    });
                    $material->join('group_materials','group_materials.id','=','materials.group_material_id');
                    $material->select('material_plants.*');
                    $material->orderBy('group_materials.material_group',$sort_order);
                break;

                case 'plant':
                    $material->join('plants','plants.id','=','material_plants.plant_id');
                    $material->select('material_plants.*');
                    $material->orderBy('plants.code',$sort_order);
                break;

                case 'product_champion':
                    $material->leftJoin('product_champions','product_champions.id','=','material_plants.product_champion_id');
                    $material->select('material_plants.*');
                    $material->orderBy('product_champions.code',$sort_order);
                break;

                case 'procurement_group':
                    $material->leftJoin('procurement_groups','material_plants.proc_group_id','=','procurement_groups.id');
                    $material->select('material_plants.*');
                    $material->orderBy('procurement_groups.code',$sort_order);
                break;

                case 'quality_profile':
                    $material->leftJoin('classification_materials','classification_materials.id','=','material_plants.quality_profile_id');
                    $material->select('material_plants.*');
                    $material->orderBy('classification_materials.name',$sort_order);
                break;

                case 'profit_center':
                    $material->leftJoin('profit_centers','material_plants.profit_center_id','=','profit_centers.id');
                    $material->select('material_plants.*');
                    $material->orderBy('profit_centers.code',$sort_order);
                break;

                case 'mrp':
                    $material->leftJoin('mrps','material_plants.mrp_controller_id','=','mrps.id');
                    $material->select('material_plants.*');
                    $material->orderBy('mrps.code',$sort_order);
                break;

                default:
                    $material->orderBy($sort_field, $sort_order);
                break;
            }
        } else {
            $material->orderBy('id', 'desc');
        }

        return $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }
    
    public function materialMrpList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialPlant::where('material_id', $id)
                ->whereNotNull('mrp_group')
                ->with(['material', 'plant', 'mrp', 'profit_center', 'product_champion', 'quality_profile', 'production_storage'])
                ->orderBy('id', 'desc')->get();
    }

    public function storeMaterialMrp(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'plant_id'          => 'required|exists:plants,id',
            'production_storage_id' => 'nullable|exists:storages,id',
            'batch_ind'         => 'nullable|boolean',
            'avail_check'       => 'nullable|boolean',
            'profit_center_id'  => 'nullable|exists:profit_centers,id',
            'serial_profile'    => 'nullable',
            'source_list'       => 'nullable|boolean',
            'mrp_group'         => 'required',
            'mrp_controller_id' => 'nullable|exists:mrps,id',
            'planned_dlv_time'  => 'nullable',
            'product_champion_id'=> 'nullable|exists:product_champions,id',
            'hazardous_indicator'=> 'nullable|boolean',
            'stock_determination'=> 'nullable',
            'quality_profile_id' => 'nullable|exists:classification_materials,id'
        ]);

        $mrp = MaterialPlant::where('material_id', $id)->where('plant_id', $request->plant_id)->first();

        if ($mrp) {
            $stock = StockMain::where('material_id', $id)->where('plant_id', $request->plant_id)->first();

            if ($stock) {
                if ($mrp->batch_ind != $request->batch_ind) {
                    return response()->json([
                        'message'   => 'Data invalid',
                        'errors'    => [
                            'batch_ind'  => ['this material has stock, cant change batch indicator']
                        ]
                    ], 422);
                }
            }
        }

        $save = MaterialPlant::updateOrCreate(
            [
                'material_id'       => $id,
                'plant_id'          => $request->plant_id,
            ],
            [
                'batch_ind'         => $request->batch_ind ? $request->batch_ind : 0,
                'production_storage_id' => $request->production_storage_id ? $request->production_storage_id : 0,
                'avail_check'       => $request->avail_check ? $request->avail_check : 0,
                'profit_center_id'  => $request->profit_center_id,
                'serial_profile'    => $request->serial_profile,
                'source_list'       => $request->source_list ? $request->source_list : 0,
                'mrp_group'         => $request->mrp_group,
                'mrp_controller_id' => $request->mrp_controller_id,
                'planned_dlv_time'  => $request->planned_dlv_time,
                'product_champion_id'=> $request->product_champion_id,
                'hazardous_indicator'=> $request->hazardous_indicator ? $request->hazardous_indicator : 0,
                'stock_determination'=> $request->stock_determination,
                'quality_profile_id' => $request->quality_profile_id
            ]
        );

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialMrp($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialPlant::with([
            'material', 'plant', 'mrp', 'profit_center', 'product_champion', 'quality_profile', 'production_storage'
        ])->findOrFail($id);
    }

    public function deleteMaterialMrp($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $mrp = MaterialPlant::find($id);

        if (!$mrp) {
            return response()->json([
                'message' => 'Data Not Found',
            ], 404);
        }

        if (count($mrp->reservation)) {
            return response()->json([
                'message' => 'Data Used in reservation cant delete',
            ], 422);
        }

        $delete = $mrp->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteMaterialPlant()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_plants,id',
        ]);

        foreach (request()->id as $id) {
            $mrp = MaterialPlant::find($id);

            if (count($mrp->reservation)) {
                return response()->json([
                    'message' => 'Data Used in reservation cant delete',
                ], 422);
            } else {

                $delete = $mrp->delete();

                return response()->json([
                    'message' => 'Success Delete Data',
                ], 200);
            }
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['material-create']);

        return Excel::download(new MaterialPlantTemplateExport, 'template_material_plant.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new MaterialPlantImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialPlantFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialPlantFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new MaterialPlantInvalidExport($data), 'invalid_material_plant_upload.xlsx');

        // delete failed upload data
        MaterialPlantFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialPlantFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        MaterialPlantFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
