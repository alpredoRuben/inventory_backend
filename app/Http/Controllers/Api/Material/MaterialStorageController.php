<?php

namespace App\Http\Controllers\Api\Material;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Input;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Material\MaterialStorageTemplateExport;
use App\Exports\Material\MaterialStorageInvalidExport;
use App\Imports\Material\MaterialStorageImport;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationType;
use App\Models\ClassificationParameter;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialParameter;
use App\Models\MaterialUom;
use App\Models\MaterialImage;
use App\Models\MaterialPlant;
use App\Models\MaterialStorage;
use App\Models\MaterialAccount;
use App\Models\MaterialSales;
use App\Models\StockMain;
use App\Models\MaterialStorageFailedUpload;

class MaterialStorageController extends Controller
{
	public function materialStorage()
    {
        Auth::user()->cekRoleModules(['material-view']);

        $material = (new MaterialStorage)->newQuery();

        $material->with([
            'material', 'material.material_type', 'material.group_material', 'plant', 'storage',
            'storage_condition', 'storage_type'
        ]);

        // only material not softdeleted
        $material->whereHas('material', function ($q){
            $q->where('deleted', false);
        });

        if (request()->has('q')) {
            $q = strtolower(request()->input('q'));
            $material->whereHas('material', function($query) use ($q) {
                $query->where(DB::raw("LOWER(code)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description)"), 'LIKE', "%".$q."%");
            });
        }

        // group_material filter
        if (request()->has('group_material')) {
            $group_material = request()->input('group_material');
            $material->whereHas('material', function ($q) use ($group_material){
                $q->whereIn('materials.group_material_id', $group_material);
            });
        }

        // material_type filter
        if (request()->has('material_type')) {
            $material_type = request()->input('material_type');
            $material->whereHas('material', function ($q) use ($material_type){
                $q->whereIn('materials.material_type_id', $material_type);
            });
        }

        // material filter
        if (request()->has('material')) {
            $material->whereIn('material_id', request()->input('material'));
        }

        // plant filter
        if (request()->has('plant')) {
            $material->whereIn('plant_id', request()->input('plant'));
        }

        // storage filter
        if (request()->has('storage')) {
            $material->whereIn('storage_id', request()->input('storage'));
        }

        if (request()->has('sort_field')) {
            $sort_order = request()->input('sort_order') == 'asc' ? 'asc' : 'desc';
            $sort_field = request()->input('sort_field');
            switch ($sort_field) {
                case 'material':
                    $material->join('materials','materials.id','=','material_storages.material_id');
                    $material->select('material_storages.*');
                    $material->orderBy('materials.code',$sort_order);
                break;

                case 'group_material':
                    $material->join('materials','materials.id','=','material_storages.material_id');
                    $material->leftJoin('group_materials','group_materials.id','=','materials.group_material_id');
                    $material->select('material_storages.*');
                    $material->orderBy('group_materials.material_group',$sort_order);
                break;

                case 'material_type':
                    $material->join('materials','materials.id','=','material_storages.material_id');
                    $material->join('material_types','material_types.id','=','materials.material_type_id');
                    $material->select('material_storages.*');
                    $material->orderBy('material_types.code',$sort_order);
                break;

                case 'plant':
                    $material->with('plant')->join('plants','material_storages.plant_id','=','plants.id');
                    $material->select('material_storages.*');
                    $material->orderBy('plants.code',$sort_order);
                break;
                
                case 'storage':
                    $material->join('storages','material_storages.storage_id','=','storages.id');
                    $material->select('material_storages.*');
                    $material->orderBy('storages.code',$sort_order);
                break;

                case 'storage_condition':
                    $material->leftJoin('storage_conditions','material_storages.storage_condition_id','=','storage_conditions.id');
                    $material->select('material_storages.*');
                    $material->orderBy('storage_conditions.code',$sort_order);
                break;

                case 'storage_type':
                    $material->leftJoin('storage_types','material_storages.storage_type_id','=','storage_types.id');
                    $material->select('material_storages.*');
                    $material->orderBy('storage_types.code',$sort_order);
                break;

                case 'description':
                $material->join('materials','materials.id','=','material_storages.material_id');
                $material->select('material_storages.*');
                $material->orderBy('materials.description',$sort_order);
                break;

                default:
                    $material->orderBy(request()->input('sort_field'), $sort_order);
                break;
            }
        } else {
            $material->orderBy('id', 'desc');
        }

        return $material->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));
    }

    public function materialStorageList($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialStorage::with([
            'material', 'plant', 'storage', 'storage_condition', 'storage_type', 'temp_condition', 'le_unit'
        ])->where('material_id', $id)->orderBy('id', 'desc')->get();   
    }

    public function storeMaterialStorage(Request $request, $id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'plant_id'          => 'required|exists:plants,id',
            'storage_id'        => 'required|exists:storages,id',
            'min_stock'         => 'nullable|numeric',
            'max_stock'         => 'nullable|numeric',
            'bin_indicator'     => 'nullable|boolean',
            'storage_condition_id' => 'nullable|exists:storage_conditions,id',
            'storage_type_id'      => 'nullable|exists:storage_types,id',
            'temp_condition_id'    => 'nullable|exists:temp_conditions,id',
            'le_quantity'       => 'nullable',
            'le_unit_id'           => 'nullable|exists:uoms,id',
            'storage_bin'           => 'nullable|max:9'
        ]);

        $min_stock = $request->min_stock ? $request->min_stock : 0;
        $max_stock = $request->max_stock ? $request->max_stock : 0;

        if ($min_stock > $max_stock) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'max_stock'  => ['The max stock must be greater than or equal '.$min_stock.'']
                ]
            ], 422);
        }

        $save = MaterialStorage::create([
            'material_id'       => $id,
            'plant_id'          => $request->plant_id,
            'storage_id'        => $request->storage_id,
            'min_stock'         => $request->min_stock,
            'max_stock'         => $request->max_stock,
            'bin_indicator'     => $request->bin_indicator ? $request->bin_indicator : 0,
            'storage_condition_id' => $request->storage_condition_id,
            'storage_type_id'      => $request->storage_type_id,
            'temp_condition_id'    => $request->temp_condition_id,
            'le_quantity'       => $request->le_quantity,
            'le_unit_id'           => $request->le_unit_id,
            'storage_bin'           => $request->storage_bin
        ]);

        if ($save) {
            return $save;
        } else {
            return response()->json([
                'message' => 'Failed Insert data',
            ], 422);
        }
    }

    public function showMaterialStorage($id)
    {
        Auth::user()->cekRoleModules(['material-view']);

        return MaterialStorage::with([
            'material', 'plant', 'storage', 'storage_condition', 'storage_type', 'temp_condition', 'le_unit'
        ])->findOrFail($id);
    }

    public function updateMaterialStorage($id, Request $request)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $materialStorage = MaterialStorage::findOrFail($id);

        $this->validate(request(), [
            // 'material_id'       => 'required|exists:materials,id',
            'plant_id'          => 'required|exists:plants,id',
            'storage_id'        => 'required|exists:storages,id',
            'min_stock'         => 'nullable|numeric',
            'max_stock'         => 'nullable|numeric',
            'bin_indicator'     => 'nullable|boolean',
            'storage_condition_id' => 'nullable|exists:storage_conditions,id',
            'storage_type_id'      => 'nullable|exists:storage_types,id',
            'temp_condition_id'    => 'nullable|exists:temp_conditions,id',
            'le_quantity'       => 'nullable',
            'le_unit_id'           => 'nullable|exists:uoms,id',
            'storage_bin'           => 'nullable|max:9'
        ]);

        $min_stock = $request->min_stock ? $request->min_stock : 0;
        $max_stock = $request->max_stock ? $request->max_stock : 0;

        if ($min_stock > $max_stock) {
            return response()->json([
                'message'   => 'Data invalid',
                'errors'    => [
                    'max_stock'  => ['The max stock must be greater than or equal '.$min_stock.'']
                ]
            ], 422);
        }

        $save = $materialStorage->update([
            // 'material_id'       => $id,
            'plant_id'          => $request->plant_id,
            'storage_id'        => $request->storage_id,
            'min_stock'         => $request->min_stock,
            'max_stock'         => $request->max_stock,
            'bin_indicator'     => $request->bin_indicator ? $request->bin_indicator : 0,
            'storage_condition_id' => $request->storage_condition_id,
            'storage_type_id'      => $request->storage_type_id,
            'temp_condition_id'    => $request->temp_condition_id,
            'le_quantity'       => $request->le_quantity,
            'le_unit_id'           => $request->le_unit_id,
            'storage_bin'          => $request->storage_bin
        ]);

        if ($save) {
            return $materialStorage;
        } else {
            return response()->json([
                'message' => 'Failed Update Data',
            ], 422);
        }
    }

    public function deleteMaterialStorage($id)
    {
        Auth::user()->cekRoleModules(['material-update']);

        $delete = MaterialStorage::findOrFail($id)->delete();

        if ($delete) {
            return response()->json($delete);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function multipleDeleteMaterialStorage()
    {
        Auth::user()->cekRoleModules(['material-update']);

        $this->validate(request(), [
            'id'          => 'required|array',
            'id.*'        => 'required|exists:material_storages,id',
        ]);

        foreach (request()->id as $id) {
            $delete = MaterialStorage::find($id)->delete();
        }

        if ($delete) {
            return response()->json([
                'message' => 'Success Delete Data',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Failed Delete Data',
            ], 422);
        }
    }

    public function template()
    {
        Auth::user()->cekRoleModules(['material-create']);

        return Excel::download(new MaterialStorageTemplateExport, 'template_material_storage.xlsx');
    }
    
    public function upload(Request $request)
    {
        Auth::user()->cekRoleModules(['material-create']);

        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        // get file
        $file = $request->file('upload_file');

        // get file extension for validation
        $ext = $file->getClientOriginalExtension();

        if ($ext != 'xlsx') {
            return response()->json([
                'message' => 'Data invalid',
                'errors' => [
                    'upload_file' => ['File Type must xlsx']
                ]
            ],422);
        }

        $user = Auth::user();
        
        $import = new MaterialStorageImport($user);

        Excel::import($import, $file);

        return response()->json([
            'message' => 'upload is in progess'
        ], 200);
    }
    
    public function getInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialStorageFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->paginate(request()->has('per_page') ? request()->per_page : appsetting('PAGINATION_DEFAULT'))
            ->appends(Input::except('page'));

        return response()->json([
            'message' => 'invalid data',
            'data' => $data
        ], 200);
    }
    
    public function downloadInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialStorageFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        $excel = Excel::download(new MaterialStorageInvalidExport($data), 'invalid_material_storage_upload.xlsx');

        // delete failed upload data
        MaterialStorageFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return $excel;
    }
    
    public function deleteInvalidUpload()
    {
        Auth::user()->cekRoleModules(['material-create']);

        $data = MaterialStorageFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->get();

        if (count($data) == 0) {
            return response()->json([
                'message' => 'Data is empty',
            ],404);
        }

        // delete failed upload data
        MaterialStorageFailedUpload::where('uploaded_by', Auth::user()->id)
            ->orderBy('created_at', 'asc')
            ->delete();

        return response()->json([
            'message' => 'berhasil hapus data',
        ],200);
    }
}
