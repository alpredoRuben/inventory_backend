<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Plant;
use App\Models\Storage;
use App\Models\WorkCenter;
use App\Models\Material;
use App\Models\Uom;
use App\Models\Bom;
use App\Models\ProductionOrder;
use App\Models\ProductionOrderDetail;
use App\Models\TransactionCode;
use App\Models\MaterialPlant;
use App\Models\Mrp;
use App\Models\User;
use App\Models\ProductionOrderFailed;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class NewProductionScheduleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $column = [
            'plant', 'work_center', 'material', 'description', 'uom', 'month', 'year', '1 as c_1', '2 as c_2', '3 as c_3', '4 as c_4',
            '5 as c_5', '6 as c_6', '7 as c_7', '8 as c_8', '9 as c_9', '10 as c_10', '11 as c_11', '12 as c_12', '13 as c_13',
            '14 as c_14', '15 as c_15', '16 as c_16', '17 as c_17', '18 as c_18', '19 as c_19', '20 as c_20', '21 as c_21', '22 as c_22',
            '23 as c_23', '24 as c_24', '25 as c_25', '26 as c_26', '27 as c_27', '28 as c_28', '29 as c_29', '30 as c_30', '31 as c_31',
            'messages', 'created_by'
        ];

        $data = ProductionOrderFailed::where('created_by', $this->user->id)
            ->orderBy('created_at', 'asc')
            ->get($column);
        
        $validDataMap = $data->map(function ($row) {
            $datekey = collect($row)->keys()->all();
            $dates = [];
            foreach ($datekey as $v) {
                if (preg_match('/c_/', $v)) {
                    $dates[] = $v;
                }
            }

            foreach ($dates as $key => $date) {
                // \Log::info($date);
                if ($row->$date) {
                    $num = str_replace('c_','',$date);
                    $req_date = Carbon::createFromDate($row->year, $row->month, $num);
                    $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))->first();
                    $work_center = WorkCenter::where(DB::raw("LOWER(code)"), strtolower($row['work_center']))->first();
                    $material = Material::where(DB::raw("LOWER(code)"), strtolower($row['material']))->first();
                    $new_data[] = [
                        'date' => (int)$num,
                        'req_date' => $req_date,
                        'date_value' => (float)$row->$date,
                        'plant_id' => $plant->id,
                        'storage_id' => $work_center->storage_result_id,
                        'work_center_id' => $work_center->id,
                        'material_id' => $material->id,
                        'uom_id' => $material->uom_id,
                    ];
                }
            }

            return $new_data;
        });

        \Log::info($validDataMap);

        try {
            DB::beginTransaction();
            $tes = [];
            foreach ($validDataMap as $key => $value) {
                foreach ($value as $v) {
                    $tes[] = $v;
                    // get production order number
                    $code = TransactionCode::where('code', 'O')->first();

                    // if exist
                    if ($code->lastcode) {
                        // generate if this year is graether than year in code
                        $yearcode = substr($code->lastcode, 1, 2);
                        $increment = substr($code->lastcode, 3, 7) + 1;
                        $year = substr(date('Y'), 2);
                        if ($year > $yearcode) {
                            $lastcode = 'O';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= '0000001';
                        } else {
                            // increment if year in code is equal to this year
                            $lastcode = 'O';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= sprintf("%07d", $increment);
                        }
                    } else {//generate if not exist
                        $lastcode = 'O';
                        $lastcode .= substr(date('Y'), 2);
                        $lastcode .= '0000001';
                    }

                    $po_code = $lastcode;

                    $material_plant = MaterialPlant::where('material_id', $v['material_id'])
                        ->where('plant_id', $v['plant_id'])
                        ->first();

                    // insert here
                    $po = ProductionOrder::create([
                        'order_number' => $po_code,
                        'order_type' => 1,
                        'status' => 1,
                        'requirement_date' => $v['req_date'],
                        'req_source' => 2,
                        'production_date' => $v['req_date'],
                        'mrp_id' => $material_plant ? $material_plant->mrp_controller_id : null,
                        'plant_id' => $v['plant_id'],
                        'storage_id' => $v['storage_id'],
                        'work_center_id' => $v['work_center_id'],
                        'material_id' => $v['material_id'],
                        'quantity' => $v['date_value'],
                        'prod_uom_id' => $v['uom_id'],
                        'gr_quantity' => null,
                        'gr_uom_id' => null,
                        'gr_date' => null,
                        'material_doc' => null,
                        'material_doc_year' => null,
                        'material_doc_item' => null,
                        'reservation_id' => null,
                        'reservation_item' => null,
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => false
                    ]);

                    // update production order number
                    $code->update([
                        'lastcode' => $po_code
                    ]);

                    // get bom
                    $material = Material::find($v['material_id']);

                    $bom = Bom::where('material_id', $v['material_id'])
                        ->where('plant_id', $v['plant_id'])
                        ->first();

                    $bom_details = $bom->detail;

                    foreach ($bom_details as $bom_detail) {
                        // get qty
                        $qty = $bom_detail->quantity * $v['date_value'];
                        // insert bom detail
                        $detail = ProductionOrderDetail::create([
                            'production_order_id' => $po->id,
                            'bom_material_id' => $po->material_id,
                            'bom_qty' => $po->quantity,
                            'component_id' => $bom_detail->component_id,
                            'quantity' => $qty,
                            'base_uom_id' => $material->uom_id
                        ]);
                    }
                }
            }

            ProductionOrderFailed::where('created_by', $this->user->id)->delete();

            DB::commit();

            \Log::info($tes);
        } catch (\Exception $e) {
            DB::rollback();

            \Log::info('error insert Production schedule');
            \Log::error($e->getMessage());
            \Log::error($e->getTrace());
        }
    }
}
