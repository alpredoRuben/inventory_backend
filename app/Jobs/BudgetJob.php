<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Budget;
use App\Models\BudgetTemporary;
use App\Models\Company;
use App\Models\CostCenter;
use App\Models\Material;
use App\Models\Project;
use App\Models\Account;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class BudgetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = BudgetTemporary::where('created_by', $this->user->id)
            ->orderBy('created_at', 'asc')
            ->get();

        try {
            DB::beginTransaction();
            foreach ($data as $row) {
                $material = Material::where(DB::raw("LOWER(code)"), strtolower($row->material))->first();
                $project = Project::where(DB::raw("LOWER(code)"), strtolower($row->project_code))->first();
                $cost_center = CostCenter::where(DB::raw("LOWER(code)"), strtolower($row->cost_center))->first();
                $gl_account = Account::where(DB::raw("LOWER(description)"), strtolower($row->gl_account))->first();
                // insert here
                $po = Budget::updateOrcreate(
                    [
                        'fiscal_year' => $row->fiscal_year,
                        'material_id' => isset($material) ? $material->id : null,
                        'company_id' => isset($cost_center) ? $cost_center->company_id : null,
                        'project_id' => isset($project) ? $project->id : null,
                        'cost_center_id' => isset($cost_center) ? $cost_center->id : null,
                        'account_id' => isset($gl_account) ? $gl_account->id : null
                    ],
                    [
                        'fiscal_year' => $row->fiscal_year,
                        'material_id' => isset($material) ? $material->id : null,
                        'company_id' => isset($cost_center) ? $cost_center->company_id : null,
                        'project_id' => isset($project) ? $project->id : null,
                        'cost_center_id' => isset($cost_center) ? $cost_center->id : null,
                        'account_id' => isset($gl_account) ? $gl_account->id : null,
                        // 'commitment_item' => ,
                        'currency' => $row->currency,
                        'amount' => $row->amount,
                        'status' => 0,
                        'responsible_id' => $row->created_by,
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                        'description' => $row->description
                    ]
                );
            }

            BudgetTemporary::where('created_by', $this->user->id)->delete();

            DB::commit();

            // \Log::info($tes);
        } catch (\Exception $e) {
            DB::rollback();

            \Log::info('error insert Production schedule');
            \Log::error($e->getMessage());
            \Log::error($e->getTrace());
        }
    }
}
