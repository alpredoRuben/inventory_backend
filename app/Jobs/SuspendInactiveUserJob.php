<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;

use App\Models\User;
use App\Models\UserLoginHistory;

class SuspendInactiveUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::where('deleted', false)
            ->where('status', 1)
            ->where('email', '<>', 'admin@localhost.com')
            ->get();

        try {
            \DB::beginTransaction();

            foreach ($users as $user) {
                $last_login = UserLoginHistory::where('user_id', $user->id)->orderBy('id', 'desc')->first();

                if ($last_login) {
                    $last_login_time = $last_login->created_at;
                } else {
                    $last_login_time = $user->created_at;
                }

                // get USER_INACTIVE_LIMIT from global setting
                $day = appsetting('USER_INACTIVE_LIMIT') ? (int)appsetting('USER_INACTIVE_LIMIT') : 90;
                $added_day = Carbon::parse($last_login_time)->addDays($day)->format('Y-m-d');

                $now = Carbon::parse(now())->format('Y-m-d');

                // cek if $now > $added_day
                if ($now > $added_day) {
                    // update status
                    $code = str_random(30);

                    $user->update([
                        'api_token' => str_random(100),
                        'confirmation_code' => $code,
                        'status' => 3, //Suspend
                    ]);

                    // send email
                    \Mail::send(new \App\Mail\SuspendInactiveUsers($user, $code));
                }
            }

            \DB::commit();

            // $this->info('Success suspend inactive user');
        } catch (\Exception $e) {
            \DB::rollback();

            \Log::error($e->getMessage());

            // $this->info('Error suspend inactive user');
        }
    }
}
