<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Reservation;
use App\Models\ReservationApproval;

class AutoCancelReservationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // find reservation requirement date = today but status pending_approval (1)
        $reservations = Reservation::where('requirement_date', '<', now())
            ->where('status', 1)
            ->get();

        // update status to cancel (4) & record to approval table
        try {
            \DB::beginTransaction();
            
            foreach ($reservations as $r) {
                $reservation = Reservation::find($r->id)->update([
                    'status'    => 4,
                    'approval_note' => 'Auto Cancel because status is pending approval until requirement date end',
                    'approval_date' => now(),
                    'approval_by' => null
                ]);

                // record reservation approval
                ReservationApproval::create([
                    'reservation_id' => $r->id,
                    'user_id' => null,
                    'status_from' => 1, // pending approval
                    'status_to' => 4,
                    'notes' => 'Auto Cancel because status is pending approval until requirement date end',
                ]);
            }

            \DB::commit();

            // $this->info('Success cancel reservation expiry not Approved');
        } catch (\Exception $e) {
            \DB::rollback();

            \Log::error($e->getMessage());

            // $this->info('Error cancel reservation expiry not Approved');
        }
    }
}
