<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use DB;

use App\Models\Asset;
use App\Models\AssetStatus;
use App\Models\AssetDepreciation;
use App\Models\DepreTypePeriod;

class AssetDepreciationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $active = AssetStatus::where(DB::raw("LOWER(name)"), '=', "active")->where('deleted', false)->first();
        $pending = AssetStatus::where(DB::raw("LOWER(name)"), '=', "pending retired")->where('deleted', false)->first();

        $asset = Asset::with('groupmaterial')->where('deleted', false)->whereIn('status_id', [$active->id, $pending->id])->get();

        $now = Carbon::now()->addMonth(-1);

        $insert_array = [];
        $depreciation_table = [];
        $double_dec_table = [];

        $temp = 0;

        foreach ($asset as $a) {
            $asset_depre = AssetDepreciation::where('asset_id', $a->id)->where('depre_type', 'sl')->first();

            //straightline

            $id_depreciation = $a->straight_line;
            if (!$id_depreciation) {
                continue;
            }

            if (!$a->purchase_date) {
                continue;
            }

            $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $id_depreciation)
            ->whereDate('period', '<=', $a->purchase_date)
            ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

            if (!$data_depre_period) {
                continue;
            }

            $nilai_sisa = $a->salvage_value;

            $harga_beli = $a->purchase_cost;
            if (!$harga_beli) {
                continue;
            }

            $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;
            if ($umur_ekonomis == 0 || $umur_ekonomis == null) {
                continue;
            }

            $besar_penyusutan = ($harga_beli - $nilai_sisa) / $umur_ekonomis;//Pertahun
            $besar_penyusutan_perbulan = $besar_penyusutan / 12;

            if (!$asset_depre) {

                $depreciation_table[$temp] = [
                    'asset_id'               => $a->id,
                    'number'                => 0,
                    'depre_type'            => 'sl',
                    'month'                 => "-",
                    'year'                  => "-",
                    'depreciation'          => 0,
                    'total_depreciation'    => 0,
                    'book_value'            => $harga_beli,
                    'material_group_id'     => $a->group_material_id,
                    'gl_account_id'         => isset($a->groupmaterial->account_id) ? $a->groupmaterial->account_id : null,
                    'cost_center_id'        => $a->cost_center_id,
                    'location_id'           => isset($a->location_id) ? $a->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];

                $temp++;

                $purchase_date = Carbon::parse($a->purchase_date);

                $i = 1;
                $id = 0;
                while ($purchase_date < $now) {
                    $akumulasi = $besar_penyusutan_perbulan * ($i);
                    $date = $purchase_date->addMonth(1);

                    $depreciation_table[$temp] = [
                        'asset_id'               => $a->id,
                        'number'                => $i,
                        'depre_type'            => 'sl',
                        'month'                 => $date->format("M"),
                        'year'                  => $date->format("Y"),
                        'depreciation'          => $besar_penyusutan_perbulan,
                        'total_depreciation'    => $akumulasi,
                        'book_value'            => $harga_beli - $akumulasi,
                        'material_group_id'     => $a->group_material_id,
                        'gl_account_id'         => isset($a->groupmaterial->account_id) ? $a->groupmaterial->account_id : null,
                        'cost_center_id'        => $a->cost_center_id,
                        'location_id'           => isset($a->location_id) ? $a->location_id : null,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                    ];

                    $i++;
                    $id++;
                    $temp++;
                }

            } else {
                $asset_exist = AssetDepreciation::where('asset_id', $a->id)->where('depre_type', 'sl')
                ->orderBy('number', 'desc')->first();
                $insert_sl = true;
                if (!$asset_exist) {
                    $insert_sl = false;
                }
                $day = Carbon::parse($a->purchase_date)->format("d");
                $month = $asset_exist->month == '-' ? date('M') : $asset_exist->month;
                $year = $asset_exist->year == '-' ? date('Y') : $asset_exist->year;
                $date = strftime("%F", strtotime($year."-".$month."-".$day));
                $insert_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
                $number = $asset_exist->number;
                $akumulasi = $asset_exist->total_depreciation;
                $new_akumulasi = $akumulasi + $besar_penyusutan_perbulan;
                $new_number = $number+1;
                if ($month === date('M') && $year === date('Y')) {
                    $insert_sl = false;
                }

                if ($insert_sl) {
                    $insert = [
                        'asset_id'              => $a->id, 
                        'number'                => $new_number,
                        'depre_type'            => 'sl',
                        'index_dd'              => null,
                        'month'                 => date('M', strtotime($insert_date)),
                        'year'                  => date('Y', strtotime($insert_date)),
                        'depreciation'          => $besar_penyusutan_perbulan,
                        'total_depreciation'    => $new_akumulasi,
                        'book_value'            => $harga_beli - $new_akumulasi,
                        'material_group_id'     => $a->group_material_id,
                        'gl_account_id'         => isset($a->groupmaterial->account->id) ? $a->groupmaterial->account->id : null,
                        'cost_center_id'        => $a->cost_center_id,
                        'location_id'           => isset($a->location_id) ? $a->location_id : null,
                        'created_at'            => Carbon::now(),
                        'updated_at'            => Carbon::now(),
                    ];
                    array_push($insert_array, $insert);
                }
            }


        }

        $temp = 0;
        foreach ($asset as $data_asset) {
            // double decline
            $asset_depre = AssetDepreciation::where('asset_id', $data_asset->id)->where('depre_type', 'dd')->first();

            $double_decline = $data_asset->double_decline;
            if (!$double_decline) {
                continue;
            }

            if (!$data_asset->purchase_date) {
                continue;
            }

            $data_depre_period = DepreTypePeriod::where('depre_type_name_id', $double_decline)
                ->whereDate('period', '<=', $data_asset->purchase_date)
                ->orderBy('softdelete', 'ASC')->orderBy('period', 'desc')->first();

            if (!$data_depre_period) {
                continue;
            }

            $umur_ekonomis = isset($data_depre_period) ? $data_depre_period->eco_lifetime / 12 : 0;

            $harga_beli_awal = isset($data_asset->purchase_cost) ? $data_asset->purchase_cost : 0;
            if ($harga_beli_awal == 0) {
                continue;
            }
            $rates = $data_depre_period->rates;
            $nilai_buku = $harga_beli_awal;
            $now = Carbon::now()->addMonth(-1);

            if (!$asset_depre) {

                $double_dec_table[$temp] = [
                    'asset_id'              => $data_asset->id,
                    'number'                => 0,
                    'index_dd'              => 0,
                    'depre_type'            => 'dd',
                    'month'                 => "-",
                    'year'                  => "-",
                    'depreciation'          => 0,
                    'total_depreciation'    => 0,
                    'book_value'            => $nilai_buku,
                    'material_group_id'     => $data_asset->group_material_id,
                    'gl_account_id'         => isset($data_asset->groupmaterial->account_id) ? $data_asset->groupmaterial->account_id : null,
                    'cost_center_id'        => $data_asset->cost_center_id,
                    'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];

                $nourut = 1;
                $aku_penyusutan_bulanIni = 0;
                $nilai_buku_bulanIni = $harga_beli_awal;
                $purchase_date = Carbon::parse($data_asset->purchase_date);
                $temp++;

                $stop = false;

                for($i = 1; $i <= $umur_ekonomis; $i++){

                    if ($stop) {
                        continue;
                    }
                    
                    $harga_beli_tahunIni = $nilai_buku;

                    $penyusutan_tahunIni = 0;

                    if ($i == $umur_ekonomis) {
                        $penyusutan_tahunIni = $harga_beli_tahunIni;
                    } else {
                        $penyusutan_tahunIni = $harga_beli_tahunIni * ($rates / 100); // * ($jml_month / 12);
                    }

                    $nilai_buku = $nilai_buku - $penyusutan_tahunIni;

                    $penyusutan_perBulan = $penyusutan_tahunIni / 12; //$jml_month;

                    for($j = 1; $j <= 12; $j++){

                        if ($purchase_date > $now) {
                            $stop = true;
                        }

                        if ($stop) {
                            continue;
                        }

                        $purchase_date->addMonths(1);

                        $harga_beli_bulanIni = $nilai_buku_bulanIni;
                        $nilai_buku_bulanIni = $nilai_buku_bulanIni - $penyusutan_perBulan;

                        $double_dec_table[$temp] = [
                            'asset_id'              => $data_asset->id,
                            'number'                => $nourut,
                            'index_dd'              => $i,
                            'depre_type'            => 'dd',
                            'month'                 => $purchase_date->format("M"),
                            'year'                  => $purchase_date->year,
                            'depreciation'          => $penyusutan_perBulan,
                            'total_depreciation'    => $aku_penyusutan_bulanIni,
                            'book_value'            => $nilai_buku_bulanIni,
                            'material_group_id'     => $data_asset->group_material_id,
                            'gl_account_id'         => isset($data_asset->groupmaterial->account->id) ? $data_asset->groupmaterial->account->id : null,
                            'cost_center_id'        => $data_asset->cost_center_id,
                            'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                            'created_at'            => Carbon::now(),
                            'updated_at'            => Carbon::now(),
                        ];
                        
                        $aku_penyusutan_bulanIni = $aku_penyusutan_bulanIni + $penyusutan_perBulan;
                        $nourut++;
                        $temp++;
                    }

                }    
            } else {
                $asset_exist = AssetDepreciation::where('asset_id', $data_asset->id)->where('depre_type', 'dd')
                ->orderBy('number', 'desc')->first();
                if (!$asset_exist) {
                    continue;
                }
                $day = Carbon::parse($data_asset->purchase_date)->format("d");
                $month = $asset_exist->month == '-' ? date('M') : $asset_exist->month;
                $year = $asset_exist->year == '-' ? date('Y') : $asset_exist->year;
                $date = strftime("%F", strtotime($year."-".$month."-".$day));
                $insert_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
                $number = $asset_exist->number;
                $akumulasi = $asset_exist->total_depreciation;
                $book_value = $asset_exist->book_value;
                $index_dd = $asset_exist->index_dd;
                if ($index_dd == $umur_ekonomis) {
                    $besar_penyusutan_perbulan = $book_value / 12;
                } else {
                    $besar_penyusutan_perbulan = $asset_exist->depreciation;
                }
                $new_akumulasi = $akumulasi + $besar_penyusutan_perbulan;
                $new_number = $number+1;
                
                if ($month === date('M') && $year === date('Y')) {
                    continue;
                }

                $insert = [
                    'asset_id'              => $data_asset->id, 
                    'number'                => $new_number,
                    'depre_type'            => 'dd',
                    'index_dd'              => $index_dd,
                    'month'                 => date('M', strtotime($insert_date)),
                    'year'                  => date('Y', strtotime($insert_date)),
                    'depreciation'          => $besar_penyusutan_perbulan,
                    'total_depreciation'    => $new_akumulasi,
                    'book_value'            => $book_value - $besar_penyusutan_perbulan,
                    'material_group_id'     => $data_asset->group_material_id,
                    'gl_account_id'         => isset($data_asset->groupmaterial->account->id) ? $data_asset->groupmaterial->account->id : null,
                    'cost_center_id'        => $data_asset->cost_center_id,
                    'location_id'           => isset($data_asset->location_id) ? $data_asset->location_id : null,
                    'created_at'            => Carbon::now(),
                    'updated_at'            => Carbon::now(),
                ];
                array_push($insert_array, $insert);
            }

        }

        try {
            DB::beginTransaction();

            if (!empty($insert_array)) {
                AssetDepreciation::insert($insert_array);
            }
            if (isset($double_dec_table)) {
                AssetDepreciation::insert($double_dec_table);
            }
            if (isset($depreciation_table)) {
                AssetDepreciation::insert($depreciation_table);
            }

            DB::commit();

            return response()->json([
                'insert_array' => $insert_array,
                'double_decline' => isset($double_dec_table) ? $double_dec_table : null,
                'straightline' => isset($depreciation_table) ? $depreciation_table : null,
                'message' => 'Success cron asset depreciation'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'message' => 'Fail to cron asset depreciation',
                'detail' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 422);
        }
    }
}
