<?php

namespace App\Imports\Master;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Storage as MasterStorage;
use App\Models\Plant;
use App\Models\Location;
use App\Models\StorageFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class StorageImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi code
            if (!$row['code']) {
                $valid = false;
                array_push($messages, 'code is required');
            } else {
                $code = MasterStorage::where(DB::raw("LOWER(code)"), strtolower($row['code']))
                    ->where('deleted', false)
                    ->first();
                if ($code) {
                    $valid = false;
                    array_push($messages, 'code '.$row['code'].' Already taken');
                }

                // validasi max char code
                if (strlen($row['code']) > 4) {
                    $valid = false;
                    array_push($messages, 'code is max 4 char');
                }
            }

            // validasi description
            if (!$row['description']) {
                $valid = false;
                array_push($messages, 'description is required');
            } else {
                // validasi max char description
                if (strlen($row['description']) > 30) {
                    $valid = false;
                    array_push($messages, 'description is max 60 char');
                }
            }

            // validate plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))
                    ->where('deleted', false)
                    ->first();
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'plant '.$row['plant'].' Not Found');
                }
            }

            // validate location
            if (!$row['location']) {
                $valid = false;
                array_push($messages, 'location is required');
            } else {
                $location = Location::where(DB::raw("LOWER(code)"), strtolower($row['location']))
                    ->where('deleted', false)
                    ->first();
                if (!$location) {
                    $valid = false;
                    array_push($messages, 'location '.$row['location'].' Not Found');
                }
            }

            // validate responsible_user
            if ($row['responsible_user']) {
                $responsible_user = User::where(DB::raw("LOWER(email)"), strtolower($row['responsible_user']))
                    ->where('deleted', false)
                    ->first();
                if (!$responsible_user) {
                    $valid = false;
                    array_push($messages, 'responsible user '.$row['responsible_user'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'code' => $row['code'],
                'description' => $row['description'],
                'plant' => $row['plant'],
                'location' => $row['location'],
                'responsible_user' => $row['responsible_user'],
                'plant_id' => isset($plant) ? $plant->id : null,
                'location_id' => isset($location) ? $location->id : null,
                'responsible_user_id' => isset($responsible_user) ? $responsible_user->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                StorageFailedUpload::create([
                    'code' => $temp['code'],
                    'description' => $temp['description'],
                    'plant' => $temp['plant'],
                    'location' => $temp['location'],
                    'responsible_user' => $temp['responsible_user'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                MasterStorage::updateOrCreate(
                    [
                        'code' => $val['code'],
                    ],
                    [
                        'code' => $val['code'],
                        'description' => $val['description'],
                        'location_id'   => $val['location_id'],
                        'plant_id'      => $val['plant_id'],
                        'user_id'       => $val['responsible_user_id'],
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
