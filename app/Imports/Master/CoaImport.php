<?php

namespace App\Imports\Master;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\ChartOfAccount;
use App\Models\AccountGroup;
use App\Models\Account;
use App\Models\GroupType;
use App\Models\AccountType;
use App\Models\CoaFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CoaImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi coa_code
            if (!$row['coa_code']) {
                $valid = false;
                array_push($messages, 'CoA Code is required');
            } else {
                $coa = ChartOfAccount::where(DB::raw("LOWER(code)"), strtolower($row['coa_code']))->first();

                // validasi max char coa_code
                if (strlen($row['coa_code']) > 5) {
                    $valid = false;
                    array_push($messages, 'CoA Code is max 5 char');
                }
            }

            // validasi CoA description
            if (!$row['coa_description']) {
                $valid = false;
                array_push($messages, 'CoA description is required');
            } else {
                // validasi max char coa_description
                if (strlen($row['coa_description']) > 30) {
                    $valid = false;
                    array_push($messages, 'CoA description is max 30 char');
                }
            }

            // validasi Account Group Code
            if (!$row['account_group_code']) {
                $valid = false;
                array_push($messages, 'Account Group Code is required');
            } else {
                $account_group = ChartOfAccount::where(DB::raw("LOWER(code)"), strtolower($row['account_group_code']))->first();

                // validasi max char coa_code
                if (strlen($row['account_group_code']) > 20) {
                    $valid = false;
                    array_push($messages, 'Account Group Code is max 20 char');
                }
            }

            // validasi account_group description
            if (!$row['account_group_description']) {
                $valid = false;
                array_push($messages, 'Account Group description is required');
            } else {
                // validasi max char account_grou_description
                if (strlen($row['account_group_description']) > 30) {
                    $valid = false;
                    array_push($messages, 'Account Group description is max 30 char');
                }
            }

            // validate Group Type
            if (!$row['group_type']) {
                $valid = false;
                array_push($messages, 'Group Type is required');
            } else {
                if (!in_array(strtolower($row['group_type']), ['asset', 'liability', 'equity', 'revenue', 'expenses'])) {
                    $valid = false;
                    array_push($messages, 'Group Type invalid, Group Type value must asset, liability, equity, revenue or expenses');
                } else {
                    $group_type = GroupType::where(DB::raw("LOWER(code)"), strtolower($row['group_type']))->first();
                }
            }

            // validate Account Type
            if (!$row['account_type']) {
                $valid = false;
                array_push($messages, 'Account Type is required');
            } else {
                if (!in_array(strtolower($row['account_type']), ['assets', 'customers', 'vendors', 'materials', 'g/l accounts'])) {
                    $valid = false;
                    array_push($messages, 'Account Type invalid, Group Type value must assets, customers, vendors, materials or g/l accounts');
                } else {
                    $account_type = AccountType::where(DB::raw("LOWER(description)"), strtolower($row['account_type']))->first();
                }
            }

            // validasi Account Code
            if (!$row['account_code']) {
                $valid = false;
                array_push($messages, 'Account Code is required');
            } else {
                $gl_account = Account::where(DB::raw("LOWER(code)"), strtolower($row['account_code']))->first();

                // validasi max char account_code
                if (strlen($row['account_code']) > 20) {
                    $valid = false;
                    array_push($messages, 'Account Code is max 20 char');
                }
            }

            // validasi Account description
            if (!$row['account_description']) {
                $valid = false;
                array_push($messages, 'Account description is required');
            } else {
                // validasi max char account_description
                if (strlen($row['account_description']) > 50) {
                    $valid = false;
                    array_push($messages, 'Account description is max 50 char');
                }
            }

            // validate Cash
            if (!$row['cash']) {
                $cash_boolean = false;
            } else {
                if (!in_array(strtolower($row['cash']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Cash invalid, Cash value must Yes or No');
                } else {
                    if (strtolower($row['cash']) === 'no') {
                        $cash_boolean = false;
                    } else if (strtolower($row['cash']) === 'yes') {
                        $cash_boolean = true;
                    } else {
                        $cash_boolean = false;
                    }
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'coa_code' => $row['coa_code'],
                'coa_description' => $row['coa_description'],
                'account_group_code' => $row['account_group_code'],
                'account_group_description' => $row['account_group_description'],
                'group_type' => $row['group_type'],
                'account_type' => $row['account_type'],
                'account_code' => $row['account_code'],
                'account_description' => $row['account_description'],
                'cash' => $row['cash'],
                'coa_id' => isset($coa) ? $coa->id : null,
                'account_group_id' => isset($account_group) ? $account_group->id : null,
                'group_type_id' => isset($group_type) ? $group_type->id : null,
                'account_type_id' => isset($account_type) ? $account_type->id : null,
                'gl_account_id' => isset($gl_account) ? $gl_account->id : null,
                'cash_boolean' => $cash_boolean,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                CoaFailedUpload::create([
                    'coa_code' => $temp['coa_code'],
                    'coa_description' => $temp['coa_description'],
                    'account_group_code' => $temp['account_group_code'],
                    'account_group_description' => $temp['account_group_description'],
                    'group_type' => $temp['group_type'],
                    'account_type' => $temp['account_type'],
                    'gl_account' => $temp['account_code'],
                    'gl_description' => $temp['account_description'],
                    'cash' => $temp['cash'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                $new_coa = ChartOfAccount::updateOrCreate(
                    [
                        'code' => $val['coa_code'],
                    ],
                    [
                        'code' => $val['coa_code'],
                        'description' => $val['coa_description'],
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                    ]
                );

                $new_ag = AccountGroup::updateOrCreate(
                    [
                        'code' => $val['account_group_code'],
                        'chart_of_account_id' => $new_coa->id,
                    ],
                    [
                        'code' => $val['account_group_code'],
                        'description' => $val['account_group_description'],
                        'group_type_id' => $val['group_type_id'],
                        'chart_of_account_id' => $new_coa->id,
                        'account_type_id' => $val['account_type_id'],
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                    ]
                );

                $new_gl = Account::updateOrCreate(
                    [
                        'code' => $val['account_code'],
                        'account_group_id' => $new_ag->id,
                    ],
                    [
                        'code' => $val['account_code'],
                        'description' => $val['account_description'],
                        'is_cash' => $val['cash_boolean'],
                        'account_group_id' => $new_ag->id,
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}