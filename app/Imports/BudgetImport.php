<?php

namespace App\Imports;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Budget;
use App\Models\Account;
use App\Models\Company;
use App\Models\Project;
use App\Models\Material;
use App\Models\CostCenter;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class BudgetImport implements WithHeadingRow, ToCollection,  WithChunkReading
{
	use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    private $rows = [];

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validate year
            if (!$row['fiscal_year']) {
                $valid = false;
                array_push($messages, 'fiscal year is required');
            } else {
                // cek valid year
                $year_length = strlen($row['fiscal_year']);
                if ($year_length > 4) {
                    $valid = false;
                    array_push($messages, 'fiscal year max 4 char');
                }

                if ($year_length < 4) {
                    $valid = false;
                    array_push($messages, 'fiscal year min 4 char');
                }

                if (is_numeric($row['fiscal_year'])) {
                    $req_date = Carbon::createFromDate($row['fiscal_year'], 01, 1)->format('Y-m-d');
                    $now = new Carbon('first day of January ' . date('Y'));
                    $this_year = Carbon::parse($now)->format('Y-m-d');

                    if ($req_date < $this_year) {
                        $valid = false;
                        array_push($messages, 'fiscal year must greather than '.date('Y').'');
                    }
                } else {
                    $valid = false;
                    array_push($messages, 'fiscal year is invalid');
                }
            }

            // validate description
            if (!$row['description']) {
                $valid = false;
                array_push($messages, 'description is required');
            } else {
                // cek minim 10 char
                $description = strlen($row['description']);

                if ($description < 10) {
                    $valid = false;
                    array_push($messages, 'description min 10 char');
                }

                if ($description > 100) {
                    $valid = false;
                    array_push($messages, 'description max 100 char');
                }
            }

            // validate Amount
            if (!$row['amount']) {
                $valid = false;
                array_push($messages, 'amount is required');
            } else {
                // cek valid ammount
                if (!is_numeric($row['amount'])) {
                    $valid = false;
                    array_push($messages, 'ammount is invalid');
                }
            }

            // validate Currency
            if (!$row['currency']) {
                $valid = false;
                array_push($messages, 'ammount is required');
            }

            // cek logic
            if ($row['material']) {
                $material = Material::where(DB::raw("LOWER(code)"), strtolower($row['material']))->first();
                if (!$material) {
                    $valid = false;
                    array_push($messages, 'material '.$row['material'].' Not Found');
                } else {
                    // if (empty($row['company']) && empty($row['project_code']) && empty($row['cost_center']) && empty($row['gl_account'])) {
                    //     $valid = false;
                    //     array_push($messages, 'material must filled with minimum one of company / project_code / cost center / gl account');
                    // }
                    if (empty($row['project_code']) && empty($row['cost_center']) && empty($row['gl_account'])) {
                        $valid = false;
                        array_push($messages, 'material must filled with minimum one of project_code / cost center / gl account');
                    }
                }
            } else {
                // if (empty($row['company']) && empty($row['project_code']) && empty($row['cost_center']) && empty($row['gl_account'])) {
                //     $valid = false;
                //     array_push($messages, 'minimum one of company / project_code / cost center / gl account is required');
                // }
                if (empty($row['project_code']) && empty($row['cost_center']) && empty($row['gl_account'])) {
                    $valid = false;
                    array_push($messages, 'minimum one of project_code / cost center / gl account is required');
                }
            }

            // cek company
            // if ($row['company']) {
            //     $company = Company::where(DB::raw("LOWER(code)"), strtolower($row['company']))->first();
            //     if (!$company) {
            //         $valid = false;
            //         array_push($messages, 'company '.$row['company'].' Not Found');
            //     }
            // }

            // cek project
            if ($row['project_code']) {
                $project = Project::where(DB::raw("LOWER(code)"), strtolower($row['project_code']))->first();
                if (!$project) {
                    $valid = false;
                    array_push($messages, 'project_code '.$row['project_code'].' Not Found');
                }
            }

            // cek cost_center
            if ($row['cost_center']) {
                $cost_center = CostCenter::where(DB::raw("LOWER(code)"), strtolower($row['cost_center']))->first();
                if (!$cost_center) {
                    $valid = false;
                    array_push($messages, 'cost center '.$row['cost_center'].' Not Found');
                }
            }

            // cek gl_account
            if ($row['gl_account']) {
                $gl_account = Account::where(DB::raw("LOWER(description)"), strtolower($row['gl_account']))->first();
                if (!$gl_account) {
                    $valid = false;
                    array_push($messages, 'gl account description '.$row['gl_account'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'fiscal_year' => $row['fiscal_year'],
                'description' => $row['description'],
                // 'company' => $row['company'],
                'project_code' => $row['project_code'],
                'cost_center' => $row['cost_center'],
                'gl_account' => $row['gl_account'],
                'material' => $row['material'],
                'amount' => $row['amount'],
                'currency' => $row['currency'],
                // 'company_id' => isset($company) ? $company->id : null,
                'project_id' => isset($project) ? $project->id : null,
                'cost_center_id' => isset($cost_center) ? $cost_center->id : null,
                'gl_account_id' => isset($gl_account) ? $gl_account->id : null,
                'material_id' => isset($material) ? $material->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        $this->rows = $data->toArray();
    }

    public function getRow(): array
    {
        return $this->rows;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
