<?php

namespace App\Imports\Material;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Material;
use App\Models\Plant;
use App\Models\SalesOrganization;
use App\Models\AccountAssignment;
use App\Models\ItemCategory;
use App\Models\MaterialSales;
use App\Models\MaterialSalesFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MaterialSalesImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi Material Code
            if (!$row['material_code']) {
                $valid = false;
                array_push($messages, 'material code is required');
            } else {
                $material_code = Material::where(DB::raw("LOWER(code)"), strtolower($row['material_code']))
                    ->where('deleted', false)
                    ->first();
                if (!$material_code) {
                    $valid = false;
                    array_push($messages, 'material code '.$row['material_code'].' Not found');
                }
            }

            // validate Delivering Plant
            if ($row['delivering_plant']) {
                $delivering_plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['delivering_plant']))
                    ->where('deleted', false)
                    ->first();
                    
                if (!$delivering_plant) {
                    $valid = false;
                    array_push($messages, 'Delivering Plant '.$row['delivering_plant'].' Not Found');
                }
            }

            // validate Tax Class
            if (!$row['tax_class']) {
                $valid = false;
                array_push($messages, 'Tax Class is required');
            } else {
                if (!in_array(strtolower($row['tax_class']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Tax Class invalid, Tax Class value must YES or NO');
                }

                if (strtolower($row['tax_class']) === 'yes') {
                    $tax_class_boolean = true;
                } else if(strtolower($row['tax_class']) === 'no') {
                    $tax_class_boolean = false;
                } else {
                    $tax_class_boolean = false;
                }
            }

            // validate SalesOrganization
            if (!$row['sales_organization']) {
                $valid = false;
                array_push($messages, 'Sales Organization is required');
            } else {
                $sales_organization = SalesOrganization::where(DB::raw("LOWER(code)"), strtolower($row['sales_organization']))
                    ->where('deleted', false)
                    ->first();
                if (!$sales_organization) {
                    $valid = false;
                    array_push($messages, 'Sales Organization '.$row['sales_organization'].' Not Found');
                }
            }

            // validate Account Assignment
            if (!$row['account_assignment']) {
                $valid = false;
                array_push($messages, 'Account Assignment is required');
            } else {
                $account_assignment = AccountAssignment::where(DB::raw("LOWER(code)"), strtolower($row['account_assignment']))
                    ->where('deleted', false)
                    ->first();
                if (!$account_assignment) {
                    $valid = false;
                    array_push($messages, 'Account Assignment '.$row['account_assignment'].' Not Found');
                }
            }

            // validate Item Category
            if (!$row['item_category']) {
                $valid = false;
                array_push($messages, 'Item Category is required');
            } else {
                $item_category = ItemCategory::where(DB::raw("LOWER(code)"), strtolower($row['item_category']))
                    ->where('deleted', false)
                    ->first();
                if (!$item_category) {
                    $valid = false;
                    array_push($messages, 'Item Category '.$row['item_category'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'material_code' => $row['material_code'],
                'delivering_plant' => $row['delivering_plant'],
                'sales_organization' => $row['sales_organization'],
                'account_assignment' => $row['account_assignment'],
                'item_category' => $row['item_category'],
                'tax_class' => $row['tax_class'],
                'tax_class_boolean' => isset($tax_class_boolean) ? $tax_class_boolean : false,
                'material_code_id' => isset($material_code) ? $material_code->id : null,
                'account_assignment_id' => isset($account_assignment) ? $account_assignment->id : null,
                'delivering_plant_id' => isset($delivering_plant) ? $delivering_plant->id : null,
                'sales_organization_id' => isset($sales_organization) ? $sales_organization->id : null,
                'item_category_id' => isset($item_category) ? $item_category->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                MaterialSalesFailedUpload::create([
                    'material_code' => $temp['material_code'],
                    'sales_organization' => $temp['sales_organization'],
                    'delivering_plant' => $temp['delivering_plant'],
                    'tax_class' => $temp['tax_class'],
                    'account_assignment' => $temp['account_assignment'],
                    'item_category' => $temp['item_category'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                MaterialSales::updateOrCreate(
                    [
                        'material_id'          => $val['material_code_id'],
                        'sales_organization_id'=> $val['sales_organization_id'],
                    ],
                    [
                        'material_id' => $val['material_code_id'],
                        'material_id'           => $val['material_code_id'],
                        'sales_organization_id' => $val['sales_organization_id'],
                        'delivering_plant_id'   => $val['delivering_plant_id'],
                        'tax_class'             => $val['tax_class_boolean'],
                        'account_assignment_id' => $val['account_assignment_id'],
                        'item_category_id'      => $val['item_category_id']
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
