<?php

namespace App\Imports\Material;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Material;
use App\Models\Plant;
use App\Models\ValuationType;
use App\Models\ValuationClass;
use App\Models\MaterialAccount;
use App\Models\MaterialAccountFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MaterialAccountImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi Material Code
            if (!$row['material_code']) {
                $valid = false;
                array_push($messages, 'material code is required');
            } else {
                $material_code = Material::where(DB::raw("LOWER(code)"), strtolower($row['material_code']))
                    ->where('deleted', false)
                    ->first();
                if (!$material_code) {
                    $valid = false;
                    array_push($messages, 'material code '.$row['material_code'].' Not found');
                }
            }

            // validate Plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'Plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))
                    ->where('deleted', false)
                    ->first();
                    
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'Plant '.$row['plant'].' Not Found');
                }
            }

            // validate Price Controll
            if (!$row['price_controll']) {
                $valid = false;
                array_push($messages, 'Price Controll is required');
            } else {
                if (!in_array(strtolower($row['price_controll']), ['map', 'standart', 'lifo/fifo'])) {
                    $valid = false;
                    array_push($messages, 'Price Controll invalid, Tax Class value must map or standart or lifo/fifo');
                }

                //0. MAP, 1. Standard, 2. LIFO/FIFO
                if (strtolower($row['price_controll']) === 'map') {
                    $price_controll_value = 0;
                } else if(strtolower($row['price_controll']) === 'standart') {
                    $price_controll_value = 1;
                } else if(strtolower($row['price_controll']) === 'lifo/fifo') {
                    $price_controll_value = 2;
                } else {
                    $price_controll_value = 0;
                }
            }

            // validate Moving Price
            if ($price_controll_value == 0) {
                if (!$row['moving_price']) {
                    $valid = false;
                    array_push($messages, 'Moving Price is required');
                }
            }

            if ($row['moving_price']) {
                if (!is_numeric($row['moving_price'])) {
                    $valid = false;
                    array_push($messages, 'Moving Price is invalid');
                }
            }

            // validate standart Price
            if ($price_controll_value == 1) {
                if (!$row['standart_price']) {
                    $valid = false;
                    array_push($messages, 'standart Price is required');
                }
            }

            if ($row['standart_price']) {
                if (!is_numeric($row['standart_price'])) {
                    $valid = false;
                    array_push($messages, 'standart Price is invalid');
                }
            }

            // validate Valuation Type
            if ($row['valuation_type']) {
                $valuation_type = ValuationType::where(DB::raw("LOWER(code)"), strtolower($row['valuation_type']))
                    ->where('deleted', false)
                    ->first();
                if (!$valuation_type) {
                    $valid = false;
                    array_push($messages, 'Valuation Type '.$row['valuation_type'].' Not Found');
                }
            }

            // validate Valuation Class
            if (!$row['valuation_class']) {
                $valid = false;
                array_push($messages, 'Valuation Class is required');
            } else {
                $valuation_class = ValuationClass::where(DB::raw("LOWER(code)"), strtolower($row['valuation_class']))
                    ->where('deleted', false)
                    ->first();
                if (!$valuation_class) {
                    $valid = false;
                    array_push($messages, 'Valuation Class '.$row['valuation_class'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'material_code' => $row['material_code'],
                'plant' => $row['plant'],
                'valuation_type' => $row['valuation_type'],
                'valuation_class' => $row['valuation_class'],
                'price_controll' => $row['price_controll'],
                'moving_price' => $row['moving_price'],
                'standart_price' => $row['standart_price'],
                'price_controll_value' => isset($price_controll_value) ? $price_controll_value : 0,
                'material_code_id' => isset($material_code) ? $material_code->id : null,
                'plant_id' => isset($plant) ? $plant->id : null,
                'valuation_type_id' => isset($valuation_type) ? $valuation_type->id : null,
                'valuation_class_id' => isset($valuation_class) ? $valuation_class->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                MaterialAccountFailedUpload::create([
                    'material_code' => $temp['material_code'],
                    'plant' => $temp['plant'], 
                    'valuation_type' => $temp['valuation_type'],
                    'valuation_class' => $temp['valuation_class'], 
                    'price_controll' => $temp['price_controll'], 
                    'moving_price' => $temp['moving_price'], 
                    'standart_price' => $temp['standart_price'], 
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                MaterialAccount::updateOrCreate(
                    [
                        'material_id'          => $val['material_code_id'],
                        'plant_id'             => $val['plant_id'],
                    ],
                    [
                        'material_id' => $val['material_code_id'],
                        'plant_id'          => $val['plant_id'],
                        'valuation_type_id' => $val['valuation_type_id'],
                        'valuation_class_id'=> $val['valuation_class_id'],
                        'price_controll'    => $val['price_controll_value'],//0. MAP, 1. Standard, 2. LIFO/FIFO
                        'moving_price'      => $val['moving_price'],
                        'standart_price'    => $val['standart_price']
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
