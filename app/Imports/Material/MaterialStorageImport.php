<?php

namespace App\Imports\Material;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\StorageCondition;
use App\Models\StorageType;
use App\Models\TempCondition;
use App\Models\MaterialStorage;
use App\Models\MaterialStorageFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MaterialStorageImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi Material Code
            if (!$row['material_code']) {
                $valid = false;
                array_push($messages, 'material code is required');
            } else {
                $material_code = Material::where(DB::raw("LOWER(code)"), strtolower($row['material_code']))
                    ->where('deleted', false)
                    ->first();
                if (!$material_code) {
                    $valid = false;
                    array_push($messages, 'material code '.$row['material_code'].' Not found');
                }
            }

            // validate plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))
                    ->where('deleted', false)
                    ->first();
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'plant '.$row['plant'].' Not Found');
                }
            }

            // validate storage
            if (!$row['storage']) {
                $valid = false;
                array_push($messages, 'storage is required');
            } else {
                $storage = MasterStorage::where(DB::raw("LOWER(code)"), strtolower($row['storage']))
                    ->where('deleted', false)
                    ->first();
                if (!$storage) {
                    $valid = false;
                    array_push($messages, 'storage '.$row['storage'].' Not Found');
                }
            }

            // validate Minimum Stock 
            if ($row['minimum_stock']) {
                if (!is_numeric($row['minimum_stock'])) {
                    $valid = false;
                    array_push($messages, 'Minimum Stock must number');
                }
            }

            // validate Maximum Stock 
            if ($row['maximum_stock']) {
                if (!is_numeric($row['maximum_stock'])) {
                    $valid = false;
                    array_push($messages, 'Maximum Stock must number');
                }

                if ($row['minimum_stock']) {
                    if ($row['minimum_stock'] > $row['maximum_stock']) {
                        $valid = false;
                        array_push($messages, 'Minimum Stock cant grater than Maximum stock');
                    }
                }
            }

            // validate Storage Bin
            if ($row['storage_bin']) {
                // validasi max char storage_bin
                if (strlen($row['storage_bin']) > 191) {
                    $valid = false;
                    array_push($messages, 'Storage Bin is max 191 char');
                }
            }

            // validate Storage Condition
            if ($row['storage_condition']) {
                $storage_condition = StorageCondition::where(DB::raw("LOWER(code)"), strtolower($row['storage_condition']))
                    ->where('deleted', false)
                    ->first();
                if (!$storage_condition) {
                    $valid = false;
                    array_push($messages, 'Storage Condition '.$row['storage_condition'].' Not Found');
                }
            }

            // validate Storage Type
            if ($row['storage_type']) {
                $storage_type = StorageType::where(DB::raw("LOWER(code)"), strtolower($row['storage_type']))
                    ->where('deleted', false)
                    ->first();
                if (!$storage_type) {
                    $valid = false;
                    array_push($messages, 'Storage Type '.$row['storage_type'].' Not Found');
                }
            }

            // validate Temp Condition
            if ($row['temp_condition']) {
                $temp_condition = TempCondition::where(DB::raw("LOWER(code)"), strtolower($row['temp_condition']))
                    ->where('deleted', false)
                    ->first();
                if (!$temp_condition) {
                    $valid = false;
                    array_push($messages, 'Temp Condition '.$row['temp_condition'].' Not Found');
                }
            }

            // validate LE Quantity
            if ($row['le_quantity']) {
                if (!is_numeric($row['le_quantity'])) {
                    $valid = false;
                    array_push($messages, 'LE Quantity is must number');
                }
            }

            // validate LE Unit
            if ($row['le_unit']) {
                $le_unit = Uom::where(DB::raw("LOWER(code)"), strtolower($row['le_unit']))
                    ->where('deleted', false)
                    ->first();
                if (!$le_unit) {
                    $valid = false;
                    array_push($messages, 'LE Unit '.$row['le_unit'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'material_code' => $row['material_code'],
                'plant' => $row['plant'],
                'storage' => $row['storage'],
                'minimum_stock' => $row['minimum_stock'],
                'maximum_stock' => $row['maximum_stock'],
                'storage_bin' => $row['storage_bin'],
                'storage_condition' => $row['storage_condition'],
                'storage_type' => $row['storage_type'],
                'temp_condition' => $row['temp_condition'],
                'le_quantity' => $row['le_quantity'],
                'le_unit' => $row['le_unit'],
                'material_code_id' => isset($material_code) ? $material_code->id : null,
                'plant_id' => isset($plant) ? $plant->id : null,
                'storage_id' => isset($storage) ? $storage->id : null,
                'storage_condition_id' => isset($storage_condition) ? $storage_condition->id : null,
                'storage_type_id' => isset($storage_type) ? $storage_type->id : null,
                'temp_condition_id' => isset($temp_condition) ? $temp_condition->id : null,
                'le_unit_id' => isset($le_unit) ? $le_unit->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                MaterialStorageFailedUpload::create([
                    'material_code' => $temp['material_code'],
                    'plant' => $temp['plant'],
                    'storage' => $temp['storage'],
                    'minimum_stock' => $temp['minimum_stock'],
                    'maximum_stock' => $temp['maximum_stock'],
                    'storage_bin' => $temp['storage_bin'],
                    'storage_condition' => $temp['storage_condition'],
                    'storage_type' => $temp['storage_type'],
                    'temp_condition' => $temp['temp_condition'],
                    'le_quantity' => $temp['le_quantity'],
                    'le_unit' => $temp['le_unit'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                MaterialStorage::updateOrCreate(
                    [
                        'material_id'       => $val['material_code_id'],
                        'plant_id'          => $val['plant_id'],
                        'storage_id'        => $val['storage_id'],
                    ],
                    [
                        'material_id'       => $val['material_code_id'],
                        'plant_id'          => $val['plant_id'],
                        'storage_id'        => $val['storage_id'],
                        'min_stock'         => $val['minimum_stock'],
                        'max_stock'         => $val['maximum_stock'],
                        'bin_indicator'     => $val['storage_bin'] ? 1 : 0,
                        'storage_condition_id' => $val['storage_condition_id'],
                        'storage_type_id'      => $val['storage_type_id'],
                        'temp_condition_id'    => $val['temp_condition_id'],
                        'le_quantity'          => $val['le_quantity'],
                        'le_unit_id'           => $val['le_unit_id'],
                        'storage_bin'          => $val['storage_bin'],
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
