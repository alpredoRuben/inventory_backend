<?php

namespace App\Imports\Material;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\MaterialType;
use App\Models\ClassificationMaterial;
use App\Models\GroupMaterial;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MaterialBasicImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validate material_type
            if (!$row['material_type']) {
                $valid = false;
                array_push($messages, 'material type is required');
            } else {
                $material_type = MaterialType::where(DB::raw("LOWER(code)"), strtolower($row['material_type']))
                    ->where('deleted', false)
                    ->first();
                if (!$material_type) {
                    $valid = false;
                    array_push($messages, 'material type '.$row['material_type'].' Not Found');
                } else {
                    // if material type external
                    if ($material_type->number_type) {
                        // check lenght
                        if (strlen($row['code']) < strlen($material_type->number_range_from) || strlen($row['code']) > strlen($material_type->number_range_to)) {
                            $valid = false;
                            array_push($messages, 'Code lenght cant lower than material type number range from or greater than material type number range to');
                        }

                        // check must greater then number range from
                        if ($row['code'] < $material_type->number_range_from) {
                            $valid = false;
                            array_push($messages, 'Code lenght cant lower than material type number range from');
                        }

                        // check must lower then number range to
                        if ($row['code'] > $material_type->number_range_to) {
                            $valid = false;
                            array_push($messages, 'Code lenght cant greater than material type number range to');
                        }
                    }
                }
            }

            // validasi code
            if (!$row['code']) {
                $valid = false;
                array_push($messages, 'code is required');
            } else {
                $code = Material::where(DB::raw("LOWER(code)"), strtolower($row['code']))
                    ->where('deleted', false)
                    ->first();
                if ($code) {
                    $valid = false;
                    array_push($messages, 'code '.$row['code'].' Already taken');
                }

                // validasi max char code
                if (strlen($row['code']) > 20) {
                    $valid = false;
                    array_push($messages, 'code is max 20 char');
                }
            }

            // validasi description
            if (!$row['description']) {
                $valid = false;
                array_push($messages, 'description is required');
            } else {
                // validasi max char description
                if (strlen($row['description']) > 60) {
                    $valid = false;
                    array_push($messages, 'description is max 60 char');
                }
            }

            // validate group_material
            if (!$row['group_material']) {
                $valid = false;
                array_push($messages, 'group material is required');
            } else {
                $group_material = GroupMaterial::where(DB::raw("LOWER(material_group)"), strtolower($row['group_material']))
                    ->where('deleted', false)
                    ->first();
                if (!$group_material) {
                    $valid = false;
                    array_push($messages, 'group material '.$row['group_material'].' Not Found');
                }
            }

            // validate classification
            if (!$row['classification']) {
                $valid = false;
                array_push($messages, 'classification is required');
            } else {
                $classification = ClassificationMaterial::where(DB::raw("LOWER(name)"), strtolower($row['classification']))->first();
                if (!$classification) {
                    $valid = false;
                    array_push($messages, 'classification '.$row['classification'].' Not Found');
                }
            }

            // validate old_material
            if ($row['old_material']) {
                $old_material = Material::where(DB::raw("LOWER(code)"), strtolower($row['old_material']))
                    ->where('deleted', false)
                    ->first();
                if (!$old_material) {
                    $valid = false;
                    array_push($messages, 'old material '.$row['old_material'].' Not Found');
                }
            }

            // validate base_material
            if ($row['base_material']) {
                $base_material = Material::where(DB::raw("LOWER(code)"), strtolower($row['base_material']))
                    ->where('deleted', false)
                    ->first();
                if (!$base_material) {
                    $valid = false;
                    array_push($messages, 'base material '.$row['base_material'].' Not Found');
                }
            }

            // validate sku_code
            if ($row['sku_code']) {
                $sku_code = Material::where(DB::raw("LOWER(sku_code)"), strtolower($row['sku_code']))
                    ->where('deleted', false)
                    ->first();
                if (!$sku_code) {
                    $valid = false;
                    array_push($messages, 'sku code '.$row['sku_code'].' Not Found');
                }
            }

            // validate uom
            if (!$row['uom']) {
                $valid = false;
                array_push($messages, 'uom is required');
            } else {
                $uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row['uom']))
                    ->where('deleted', false)
                    ->first();
                if (!$uom) {
                    $valid = false;
                    array_push($messages, 'uom '.$row['uom'].' Not Found');
                }
            }

            // validate Nett Weight Value
            if ($row['nett_weight_value']) {
                // cek nett_weight_value
                if (!is_numeric($row['nett_weight_value'])) {
                    $valid = false;
                    array_push($messages, 'Nett Weight Value is invalid');
                }
            }

            // validate Nett Weight UoM
            if ($row['nett_weight_uom']) {
                $nett_weight_uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row['nett_weight_uom']))
                    ->where('deleted', false)
                    ->first();
                if (!$nett_weight_uom) {
                    $valid = false;
                    array_push($messages, 'Nett Weight UoM '.$row['nett_weight_uom'].' Not Found');
                }
            }

            // validate Gross Weight Value
            if ($row['gross_weight_value']) {
                // cek gross_weight_value
                if (!is_numeric($row['gross_weight_value'])) {
                    $valid = false;
                    array_push($messages, 'Gross Weight Value is invalid');
                }
            }

            // validate Gross Volume Value
            if ($row['volume_value']) {
                // cek volume_value
                if (!is_numeric($row['volume_value'])) {
                    $valid = false;
                    array_push($messages, 'Volume Value is invalid');
                }
            }

            // validate Volume UoM
            if ($row['volume_uom']) {
                $volume_uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row['volume_uom']))
                    ->where('deleted', false)
                    ->first();
                if (!$volume_uom) {
                    $valid = false;
                    array_push($messages, 'Volume UoM '.$row['volume_uom'].' Not Found');
                }
            }

            // validasi serial profile
            if ($row['serial_profile']) {
                if (!in_array(strtolower($row['serial_profile']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'serial profile invalid, serial profile value must yes, no or leave it blank');
                }

                if (strtoupper($row['serial_profile']) === 'YES') {
                    $serial_profile_boolean = true;
                } else if(strtoupper($row['serial_profile']) === 'NO') {
                    $serial_profile_boolean = false;
                } else if($row['serial_profile'] == '') {
                    $serial_profile_boolean = false;
                } else {
                    $serial_profile_boolean = false;
                }
            } else {
                $serial_profile_boolean = false;
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'material_type' => $row['material_type'],
                'code' => $row['code'],
                'description' => $row['description'],
                'group_material' => $row['group_material'],
                'classification' => $row['classification'],
                'old_material' => $row['old_material'],
                'base_material' => $row['base_material'],
                'sku_code' => $row['sku_code'],
                'uom' => $row['uom'],
                'nett_weight_value' => $row['nett_weight_value'],
                'nett_weight_uom' => $row['nett_weight_uom'],
                'gross_weight_value' => $row['gross_weight_value'],
                'volume_value' => $row['volume_value'],
                'volume_uom' => $row['volume_uom'],
                'serial_profile' => $row['serial_profile'],
                'serial_profile_boolean' => $serial_profile_boolean,
                'material_type_id' => isset($material_type) ? $material_type->id : null,
                'group_material_id' => isset($group_material) ? $group_material->id : null,
                'classification_id' => isset($classification) ? $classification->id : null,
                'old_material_id' => isset($old_material) ? $old_material->id : null,
                'base_material_id' => isset($base_material) ? $base_material->id : null,
                'uom_id' => isset($uom) ? $uom->id : null,
                'nett_weight_uom_id' => isset($nett_weight_uom) ? $nett_weight_uom->id : null,
                'volume_uom_id' => isset($volume_uom) ? $volume_uom->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                MaterialFailedUpload::create([
                    'material_type' => $temp['material_type'],
                    'code' => $temp['code'],
                    'description' => $temp['description'],
                    'group_material' => $temp['group_material'],
                    'classification' => $temp['classification'],
                    'old_material' => $temp['old_material'],
                    'base_material' => $temp['base_material'],
                    'sku_code' => $temp['sku_code'],
                    'uom' => $temp['uom'],
                    'nett_weight_value' => $temp['nett_weight_value'],
                    'nett_weight_uom' => $temp['nett_weight_uom'],
                    'gross_weight_value' => $temp['gross_weight_value'],
                    'volume_value' => $temp['volume_value'],
                    'volume_uom' => $temp['volume_uom'],
                    'serial_profile' => $temp['serial_profile'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                Material::updateOrCreate(
                    [
                        'code' => $val['code'],
                    ],
                    [
                        'code' => $val['code'],
                        'description' => $val['description'],
                        'classification_material_id' => $val['classification_id'],
                        'group_material_id' => $val['group_material_id'],
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => 0,
                        'serial_profile' => $val['serial_profile_boolean'],
                        'material_type_id' => $val['material_type_id'],
                        'sku_code' => $val['sku_code'],
                        'uom_id' => $val['uom_id'],
                        'old_material' => $val['old_material_id'],
                        'base_material' => $val['base_material_id'],
                        'nett_weight' => $val['nett_weight_value'],
                        'gross_weight' => $val['gross_weight_value'],
                        'weight_unit' => $val['nett_weight_uom_id'],
                        'volume' => $val['volume_value'],
                        'volume_uom' => $val['volume_uom_id']
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
