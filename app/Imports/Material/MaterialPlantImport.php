<?php

namespace App\Imports\Material;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Uom;
use App\Models\Material;
use App\Models\MaterialUom;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\ProcurementGroup;
use App\Models\ProfitCenter;
use App\Models\Mrp;
use App\Models\ProductChampion;
use App\Models\ClassificationType;
use App\Models\ClassificationMaterial;
use App\Models\MaterialPlant;
use App\Models\MaterialPlantFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class MaterialPlantImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validasi Material Code
            if (!$row['material_code']) {
                $valid = false;
                array_push($messages, 'material code is required');
            } else {
                $material_code = Material::where(DB::raw("LOWER(code)"), strtolower($row['material_code']))
                    ->where('deleted', false)
                    ->first();
                if (!$material_code) {
                    $valid = false;
                    array_push($messages, 'material code '.$row['material_code'].' Not found');
                }
            }

            // validate plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))
                    ->where('deleted', false)
                    ->first();
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'plant '.$row['plant'].' Not Found');
                }
            }

            // validate Procurement Type
            if (!$row['procurement_type']) {
                $valid = false;
                array_push($messages, 'Procurement Type is required');
            } else {
                if (!in_array(strtolower($row['procurement_type']), ['internal', 'external'])) {
                    $valid = false;
                    array_push($messages, 'Procurement Type invalid, Procurement Type value must internal or external');
                }

                if (strtolower($row['procurement_type']) === 'internal') {
                    $procurement_type_boolean = false;
                } else if(strtolower($row['procurement_type']) === 'external') {
                    $procurement_type_boolean = true;
                } else {
                    $procurement_type_boolean = false;
                }
            }

            // validate Procurement Group
            if (!$row['procurement_group']) {
                $valid = false;
                array_push($messages, 'Procurement Group is required');
            } else {
                $procurement_group = ProcurementGroup::where(DB::raw("LOWER(code)"), strtolower($row['procurement_group']))
                    ->where('deleted', false)
                    ->first();
                if (!$procurement_group) {
                    $valid = false;
                    array_push($messages, 'Procurement Group '.$row['procurement_group'].' Not Found');
                }
            }

            // validate UoP
            if ($row['uop']) {
                $uop = Uom::where(DB::raw("LOWER(code)"), strtolower($row['uop']))
                    ->where('deleted', false)
                    ->first();
                if (!$uop) {
                    $valid = false;
                    array_push($messages, 'UoP '.$row['uop'].' Not Found');
                } else {
                    // validasi uop exist in material uom conversion
                    if (isset($material_code)) {
                        $uop_exist_in_conv = MaterialUom::where('material_id', $material_code->id)
                            ->where('uom_id', $uop->id)
                            ->first();

                        if (!$uop_exist_in_conv) {
                            $valid = false;
                            array_push($messages, 'UoP '.$row['uop'].' must exists in material conversion');
                        }
                    }
                }
            }

            // validate Production Storage
            if (!$row['production_storage']) {
                $valid = false;
                array_push($messages, 'Production storage is required');
            } else {
                $production_storage = MasterStorage::where(DB::raw("LOWER(code)"), strtolower($row['production_storage']))
                    ->where('deleted', false)
                    ->first();
                if (!$production_storage) {
                    $valid = false;
                    array_push($messages, 'Production storage '.$row['production_storage'].' Not Found');
                }
            }

            // validasi Batch Indicator
            if ($row['batch_indicator']) {
                if (!in_array(strtolower($row['batch_indicator']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Batch Indicator invalid, Batch Indicator value must yes, no or leave it blank');
                }

                if (strtolower($row['batch_indicator']) === 'yes') {
                    $batch_indicator_boolean = true;
                } else if(strtolower($row['batch_indicator']) === 'no') {
                    $batch_indicator_boolean = false;
                } else if($row['batch_indicator'] == '') {
                    $batch_indicator_boolean = false;
                } else {
                    $batch_indicator_boolean = false;
                }
            } else {
                $batch_indicator_boolean = false;
            }

            // validasi Available Check
            if ($row['available_check']) {
                if (!in_array(strtolower($row['available_check']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Available Check invalid, Available Check value must yes, no or leave it blank');
                }

                if (strtolower($row['available_check']) === 'yes') {
                    $available_check_boolean = true;
                } else if(strtolower($row['available_check']) === 'no') {
                    $available_check_boolean = false;
                } else if($row['available_check'] == '') {
                    $available_check_boolean = false;
                } else {
                    $available_check_boolean = false;
                }
            } else {
                $available_check_boolean = false;
            }

            // validate Profit Center
            if ($row['profit_center']) {
                $profit_center = ProfitCenter::where(DB::raw("LOWER(code)"), strtolower($row['profit_center']))
                    ->where('deleted', false)
                    ->first();
                if (!$profit_center) {
                    $valid = false;
                    array_push($messages, 'Profit Center '.$row['profit_center'].' Not Found');
                }
            }

            // validasi Source List
            if ($row['source_list']) {
                if (!in_array(strtolower($row['source_list']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Source List invalid, Source List value must yes, no or leave it blank');
                }

                if (strtolower($row['source_list']) === 'yes') {
                    $source_list_boolean = true;
                } else if(strtolower($row['source_list']) === 'no') {
                    $source_list_boolean = false;
                } else if($row['source_list'] == '') {
                    $source_list_boolean = false;
                } else {
                    $source_list_boolean = false;
                }
            } else {
                $source_list_boolean = false;
            }

            // validasi MRP Group
            if ($row['mrp_group']) {
                if (!in_array(strtolower($row['mrp_group']), ['stock', 'order'])) {
                    $valid = false;
                    array_push($messages, 'MRP Group invalid, MRP Group value must stock, order');
                }

                if (strtolower($row['mrp_group']) === 'stock') {
                    $mrp_group_value = 1;
                } else if(strtolower($row['mrp_group']) === 'order') {
                    $mrp_group_value = 2;
                }
            }

            // validate MRP Controller
            if ($row['mrp_controller']) {
                $mrp = Mrp::where(DB::raw("LOWER(code)"), strtolower($row['mrp_controller']))
                    ->where('deleted', false)
                    ->first();
                if (!$mrp) {
                    $valid = false;
                    array_push($messages, 'MRP Controller '.$row['mrp_controller'].' Not Found');
                }
            }

            // validate Planned Delivery Time
            if ($row['planned_delivery_time']) {
                if (!is_numeric($row['planned_delivery_time'])) {
                    $valid = false;
                    array_push($messages, 'Planned Dilivery Time is invalid');
                }
            }

            // validate Product Champion
            if ($row['product_champion']) {
                $product_champion = ProductChampion::where(DB::raw("LOWER(code)"), strtolower($row['product_champion']))
                    ->where('deleted', false)
                    ->first();
                if (!$product_champion) {
                    $valid = false;
                    array_push($messages, 'Product Champion '.$row['product_champion'].' Not Found');
                }
            }

            // validasi Hazardous Indicator
            if ($row['hazardous_indicator']) {
                if (!in_array(strtolower($row['hazardous_indicator']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'Hazardous Indicator invalid, Hazardous Indicator value must yes, no or leave it blank');
                }

                if (strtolower($row['hazardous_indicator']) === 'yes') {
                    $hazardous_indicator_boolean = true;
                } else if(strtolower($row['hazardous_indicator']) === 'no') {
                    $hazardous_indicator_boolean = false;
                } else if($row['hazardous_indicator'] == '') {
                    $hazardous_indicator_boolean = false;
                } else {
                    $hazardous_indicator_boolean = false;
                }
            } else {
                $hazardous_indicator_boolean = false;
            }

            // validasi Stock Determination
            if ($row['stock_determination']) {
                // validasi max char Stock Determination
                if (strlen($row['stock_determination']) > 191) {
                    $valid = false;
                    array_push($messages, 'Stock Determination is max 191 char');
                }
            }

            // validate Quality Profile
            if ($row['quality_profile']) {
                $quality_profile_c_type = ClassificationType::where(DB::raw("LOWER(name)"), strtolower('Quality_Profile'))->first();
                $quality_profile = ClassificationMaterial::where(DB::raw("LOWER(name)"), strtolower($row['quality_profile']))
                    ->where('classification_type_id', $quality_profile_c_type->id)
                    ->where('deleted', false)
                    ->first();
                if (!$quality_profile) {
                    $valid = false;
                    array_push($messages, 'Quality Profile '.$row['quality_profile'].' Not Found');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'material_code' => $row['material_code'],
                'plant' => $row['plant'],
                'procurement_type' => $row['procurement_type'],
                'procurement_group' => $row['procurement_group'],
                'uop' => $row['uop'],
                'production_storage' => $row['production_storage'],
                'batch_indicator' => $row['batch_indicator'],
                'available_check' => $row['available_check'],
                'profit_center' => $row['profit_center'],
                'source_list' => $row['source_list'],
                'mrp_group' => $row['mrp_group'],
                'mrp' => $row['mrp_controller'],
                'planned_delivery_time' => $row['planned_delivery_time'],
                'product_champion' => $row['product_champion'],
                'hazardous_indicator' => $row['hazardous_indicator'],
                'stock_determination' => $row['stock_determination'],
                'quality_profile' => $row['quality_profile'],
                'material_code_id' => isset($material_code) ? $material_code->id : null,
                'plant_id' => isset($plant) ? $plant->id : null,
                'procurement_group_id' => isset($procurement_group) ? $procurement_group->id : null,
                'uop_id' => isset($uop) ? $uop->id : null,
                'production_storage_id' => isset($production_storage) ? $production_storage->id : null,
                'profit_center_id' => isset($profit_center) ? $profit_center->id : null,
                'mrp_id' => isset($mrp) ? $mrp->id : null,
                'product_champion_id' => isset($product_champion) ? $product_champion->id : null,
                'quality_profile_id' => isset($quality_profile) ? $quality_profile->id : null,
                'procurement_type_boolean' => isset($procurement_type_boolean) ? $procurement_type_boolean : false,
                'batch_indicator_boolean' => isset($batch_indicator_boolean) ? $batch_indicator_boolean : false,
                'available_check_boolean' => isset($available_check_boolean) ? $available_check_boolean : false,
                'source_list_boolean' => isset($source_list_boolean) ? $source_list_boolean : false,
                'hazardous_indicator_boolean' => isset($hazardous_indicator_boolean) ? $hazardous_indicator_boolean : false,
                'mrp_group_value' => isset($mrp_group_value) ? $mrp_group_value : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                MaterialPlantFailedUpload::create([
                    'material_code' => $temp['material_code'],
                    'plant' => $temp['plant'],
                    'procurement_type' => $temp['procurement_type'],
                    'procurement_group' => $temp['procurement_group'],
                    'uop' => $temp['uop'],
                    'production_storage' => $temp['production_storage'],
                    'batch_indicator' => $temp['batch_indicator'],
                    'available_check' => $temp['available_check'],
                    'profit_center' => $temp['profit_center'],
                    'source_list' => $temp['source_list'],
                    'mrp_group' => $temp['mrp_group'],
                    'mrp_controller' => $temp['mrp'],
                    'planned_delivery_time' => $temp['planned_delivery_time'],
                    'product_champion' => $temp['product_champion'],
                    'hazardous_indicator' => $temp['hazardous_indicator'],
                    'stock_determination' => $temp['stock_determination'],
                    'quality_profile' => $temp['quality_profile'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                MaterialPlant::updateOrCreate(
                    [
                        'material_id'       => $val['material_code_id'],
                        'plant_id'          => $val['plant_id'],
                    ],
                    [
                        'material_id' => $val['material_code_id'],
                        'plant_id' => $val['plant_id'],
                        'proc_type' => $val['procurement_type_boolean'],
                        'proc_group_id' => $val['procurement_group_id'],
                        'batch_ind' => $val['batch_indicator_boolean'],
                        'avail_check' => $val['available_check_boolean'],
                        'profit_center_id' => $val['profit_center_id'],
                        'serial_profile' => null,
                        'source_list' => $val['source_list_boolean'],
                        'mrp_group' => $val['mrp_group_value'],
                        'mrp_controller_id' => $val['mrp_id'],
                        'planned_dlv_time' => $val['planned_delivery_time'],
                        'product_champion_id' => $val['product_champion_id'],
                        'hazardous_indicator' => $val['hazardous_indicator_boolean'],
                        'stock_determination' => $val['stock_determination'],
                        'quality_profile_id' => $val['quality_profile_id'],
                        'production_storage_id' => $val['production_storage_id'],
                        'unit_of_purchase_id' => $val['uop_id']
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
