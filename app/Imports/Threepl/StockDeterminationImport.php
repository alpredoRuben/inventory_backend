<?php

namespace App\Imports\Threepl;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\Plant;
use App\Models\Storage as MasterStorage;
use App\Models\StockDetermination;
use App\Models\StockDeterminationFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class StockDeterminationImport implements  WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
    use Importable;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validate Plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'Plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))
                    ->where('deleted', false)
                    ->first();
                    
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'Plant '.$row['plant'].' Not Found');
                }
            }

            // validate storage
            if (!$row['storage']) {
                $valid = false;
                array_push($messages, 'storage is required');
            } else {
                $storage = MasterStorage::where(DB::raw("LOWER(code)"), strtolower($row['storage']))
                    ->where('deleted', false)
                    ->first();
                    
                if (!$storage) {
                    $valid = false;
                    array_push($messages, 'storage '.$row['storage'].' Not Found');
                } else {
                    // validate storage adn plant is related
                    if ($plant) {
                        if ($storage->plant_id != $plant->id) {
                            $valid = false;
                            array_push($messages, 'storage & plant must related');
                        }
                    }
                }
            }

            // validate Sequence
            if ($row['sequence']) {
                $seq = $row['sequence'];
                if (!is_numeric($row['sequence'])) {
                    $valid = false;
                    array_push($messages, 'Sequence must number');
                } else {
                    // cek sequence exist in storage reciever
                    if ($storage) {
                        $cek_seq = StockDetermination::where('storage_id', $storage->id)
                            ->where('sequence', $seq)
                            ->first();

                        if ($cek_seq) {
                            $valid = false;
                            array_push($messages, 'Sequence already taken for storage '.$row['storage'].'');
                        }
                    }
                }
            } else {
                $seq = 0;

                // cek sequence exist in storage reciever
                if ($storage) {
                    $cek_seq = StockDetermination::where('storage_id', $storage->id)
                        ->where('sequence', $seq)
                        ->first();

                    if ($cek_seq) {
                        $valid = false;
                        array_push($messages, 'Sequence already taken for storage '.$row['storage'].'');
                    }
                }
            }

            // validate External Purchase
            if (!$row['external_purchase']) {
                $valid = false;
                array_push($messages, 'External Purchase is required');
            } else {
                if (!in_array(strtolower($row['external_purchase']), ['yes', 'no'])) {
                    $valid = false;
                    array_push($messages, 'External Purchase invalid, External Purchase value must yes or no');
                }

                if (strtolower($row['external_purchase']) === 'no') {
                    $external_purchase_boolean = false;
                } else if(strtolower($row['external_purchase']) === 'yes') {
                    $external_purchase_boolean = true;
                } else {
                    $external_purchase_boolean = false;
                }
            }

            if (!$external_purchase_boolean) {
                // validate Supply Plant
                if (!$row['plant_supply']) {
                    $valid = false;
                    array_push($messages, 'Supply Plant is required');
                } else {
                    $plant_supply = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant_supply']))
                        ->where('deleted', false)
                        ->first();
                        
                    if (!$plant_supply) {
                        $valid = false;
                        array_push($messages, 'Supply Plant '.$row['plant_supply'].' Not Found');
                    }
                }

                // validate supply_ storage
                if (!$row['storage_supply']) {
                    $valid = false;
                    array_push($messages, 'Supply storage is required');
                } else {
                    $storage_supply = MasterStorage::where(DB::raw("LOWER(code)"), strtolower($row['storage_supply']))
                        ->where('deleted', false)
                        ->first();
                        
                    if (!$storage_supply) {
                        $valid = false;
                        array_push($messages, 'Supply storage '.$row['storage_supply'].' Not Found');
                    } else {
                        // validate storage adn plant is related
                        if ($plant_supply) {
                            if ($storage_supply->plant_id != $plant_supply->id) {
                                $valid = false;
                                array_push($messages, 'Supply storage & Supply plant must related');
                            }
                        }
                    }
                }
            }

            if (isset($storage) && isset($storage_supply)) {
                if ($row['storage'] === $row['storage_supply']) {
                    $valid = false;
                    array_push($messages, 'Storage & Storage Supply Cant Same');
                }

                $data_ada = StockDetermination::where('ext_purchase', $external_purchase_boolean)
                    ->where('storage_id', $storage->id)
                    ->where('supply_storage_id', $storage_supply->id)
                    ->first();

                if ($data_ada) {
                    $valid = false;
                    array_push($messages, 'Data Already Exist');
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'plant' => $row['plant'],
                'storage' => $row['storage'],
                'sequence' => $row['sequence'],
                'sequence_val' => $seq,
                'external_purchase' => $row['external_purchase'],
                'plant_supply' => $row['plant_supply'],
                'storage_supply' => $row['storage_supply'],
                'external_purchase_boolean' => isset($external_purchase_boolean) ? $external_purchase_boolean : 0,
                'plant_id' => isset($plant) ? $plant->id : null,
                'storage_id' => isset($storage) ? $storage->id : null,
                'plant_supply_id' => isset($plant_supply) ? $plant_supply->id : null,
                'storage_supply_id' => isset($storage_supply) ? $storage_supply->id : null,
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                StockDeterminationFailedUpload::create([
                    'plant' => $temp['plant'],
                    'storage' => $temp['storage'],
                    'sequence' => $temp['sequence'],
                    'external_purchase' => $temp['external_purchase'],
                    'plant_supply' => $temp['plant_supply'],
                    'storage_supply' => $temp['storage_supply'],
                    'message' => $temp['message'],  
                    'uploaded_by' => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            foreach ($data_valid as $val) {
                StockDetermination::create([
                    'plant_id'          => $val['plant_id'],
                    'storage_id'        => $val['storage_id'],
                    'sequence'          => $val['sequence_val'],
                    'supply_plant_id'   => $val['plant_supply_id'],
                    'supply_storage_id' => $val['storage_supply_id'],
                    'ext_purchase'      => $val['external_purchase_boolean'],
                    'created_by'        => $this->user->id,
                    'updated_by'        => $this->user->id
                ]);
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
