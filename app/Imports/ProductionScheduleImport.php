<?php

namespace App\Imports;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\Plant;
use App\Models\Storage;
use App\Models\WorkCenter;
use App\Models\Material;
use App\Models\Uom;
use App\Models\Bom;
use App\Models\ProductionOrder;
use App\Models\ProductionOrderDetail;
use App\Models\TransactionCode;
use App\Models\MaterialPlant;
use App\Models\Mrp;
use App\Models\User;
use App\Models\ProductionOrderFailed;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ProductionScheduleImport implements WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
	use Importable;

    public function  __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validate plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'Plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))->first();
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'Plant '.$row['plant'].' Not Found');
                }
            }

            // validate work center
            if (!$row['work_center']) {
                $valid = false;
                array_push($messages, 'work center is required');
            } else {
                $work_center = WorkCenter::where(DB::raw("LOWER(code)"), strtolower($row['work_center']))->first();
                if (!$work_center) {
                    $valid = false;
                    array_push($messages, 'work center '.$row['work_center'].' Not Found');
                } else {
                    $plant_data = $plant ? $plant->id : 0;
                    if ($plant_data != $work_center->plant_id) {
                        $valid = false;
                        array_push($messages, 'work center & plant not match');
                    }
                }
            }

            // validate material
            if (!$row['material']) {
                $valid = false;
                array_push($messages, 'material is required');
            } else {
                $material = Material::where(DB::raw("LOWER(code)"), strtolower($row['material']))->first();
                if (!$material) {
                    $valid = false;
                    array_push($messages, 'material '.$row['material'].' Not Found');
                } else {
                    $bom = Bom::where('material_id', $material->id)
                        ->where('plant_id', $plant ? $plant->id : null)
                        ->first();

                    if (!$bom) {
                        $valid = false;
                        array_push($messages, 'material '.$row['material'].' Not exist as Bill of material header in plant '.$row['plant'].'');
                    }
                }
            }

            // validate uom
            if (!$row['base_uom']) {
                $valid = false;
                array_push($messages, 'Base Uom is required');
            } else {
                $uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row['base_uom']))->first();
                if (!$uom) {
                    $valid = false;
                    array_push($messages, 'Base Uom '.$row['base_uom'].' Not Found');
                } else {
                    $uom_material_data = $material ? $material->uom_id : 0;
                    $uom_data = Uom::find($uom_material_data);
                    if (!$uom_data) {
                        $valid = false;
                        array_push($messages, 'material not have base uom');
                    } else {
                        if ($uom_material_data != $uom->id) {
                            $valid = false;
                            array_push($messages, 'Incorrect base uom used');
                        }
                    }
                }
            }

            // validate month
            if (!$row['month']) {
                $valid = false;
                array_push($messages, 'month is required');
            } else {
                if (!is_numeric($row['month'])) {
                    $valid = false;
                    array_push($messages, 'month invalid. month must numeric');
                } else {
                    if ($row['month'] < 1 | $row['month'] > 12 ) {
                        $valid = false;
                        array_push($messages, 'month invalid. min 1 max 12');
                    }
                }
            }

            // validate year
            if (!$row['year']) {
                $valid = false;
                array_push($messages, 'year is required');
            } else {
                if (!is_numeric($row['year'])) {
                    $valid = false;
                    array_push($messages, 'year invalid. year must numeric');
                }
            }

            // validate month & year
            if (is_numeric($row['year']) && is_numeric($row['month'])) {
                $req_date = Carbon::createFromDate($row['year'], $row['month'], 1);
                $now = new Carbon('first day of this month');

                if ($req_date < $now) {
                    $valid = false;
                    array_push($messages, 'month and year must greather than '.date('m-Y').'');
                }
            }

            // validate date value
            $j = 32;
            for ($i=1; $i < $j; $i++) { 
                if ($row[$i]) {
                    if (!is_numeric($row[$i])) {
                        $valid = false;
                        array_push($messages, 'value '.$i.' must number');
                    }
                }
            }

            // create new data
            $new_data = [
                'plant_id' => isset($plant) ? $plant->id : null,
                'work_center_id' => isset($work_center) ? $work_center->id : null,
                'storage_id' => isset($work_center) ? $work_center->storage_result_id : null,
                'material_id' => isset($material) ? $material->id : null,
                'uom_id' => isset($uom) ? $uom->id : null,
                'c_1' => $row[1],
                'c_2' => $row[2],
                'c_3' => $row[3],
                'c_4' => $row[4],
                'c_5' => $row[5],
                'c_6' => $row[6],
                'c_7' => $row[7],
                'c_8' => $row[8],
                'c_9' => $row[9],
                'c_10' => $row[10],
                'c_11' => $row[11],
                'c_12' => $row[12],
                'c_13' => $row[13],
                'c_14' => $row[14],
                'c_15' => $row[15],
                'c_16' => $row[16],
                'c_17' => $row[17],
                'c_18' => $row[18],
                'c_19' => $row[19],
                'c_20' => $row[20],
                'c_21' => $row[21],
                'c_22' => $row[22],
                'c_23' => $row[23],
                'c_24' => $row[24],
                'c_25' => $row[25],
                'c_26' => $row[26],
                'c_27' => $row[27],
                'c_28' => $row[28],
                'c_29' => $row[29],
                'c_30' => $row[30],
                'c_31' => $row[31],
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            $row = $row->toArray();

            return (object) array_merge($row,$new_data);
        });

        // \Log::info($data);

        // insert valid into production order table
        $validData = $data->whereIn('valid', true);
        // \Log::info($validData);
        $validDataMap = $validData->map(function ($row) {
            $datekey = collect($row)->keys()->all();
            // \Log::info($datekey);
            $dates = [];
            foreach ($datekey as $v) {
                if (preg_match('/c_/', $v)) {
                    $dates[] = $v;
                }
            }

            // \Log::info($dates);

            foreach ($dates as $key => $date) {
                // \Log::info($date);
                if ($row->$date) {
                    $num = str_replace('c_','',$date);
                    $req_date = Carbon::createFromDate($row->year, $row->month, $num);
                    $new_data[] = [
                        'date' => (int)$num,
                        'req_date' => $req_date,
                        'date_value' => (float)$row->$date,
                        'plant_id' => $row->plant_id,
                        'storage_id' => $row->storage_id,
                        'work_center_id' => $row->work_center_id,
                        'material_id' => $row->material_id,
                        'uom_id' => $row->uom_id,
                    ];
                }
            }

            return $new_data;
        });

        // \Log::info($validDataMap);

        // $tes = [];
        try {
            DB::beginTransaction();

            foreach ($validDataMap as $key => $value) {
                foreach ($value as $v) {
                    // $tes[] = $v;
                    // get production order number
                    $code = TransactionCode::where('code', 'O')->first();

                    // if exist
                    if ($code->lastcode) {
                        // generate if this year is graether than year in code
                        $yearcode = substr($code->lastcode, 1, 2);
                        $increment = substr($code->lastcode, 3, 7) + 1;
                        $year = substr(date('Y'), 2);
                        if ($year > $yearcode) {
                            $lastcode = 'O';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= '0000001';
                        } else {
                            // increment if year in code is equal to this year
                            $lastcode = 'O';
                            $lastcode .= substr(date('Y'), 2);
                            $lastcode .= sprintf("%07d", $increment);
                        }
                    } else {//generate if not exist
                        $lastcode = 'O';
                        $lastcode .= substr(date('Y'), 2);
                        $lastcode .= '0000001';
                    }

                    $po_code = $lastcode;

                    $material_plant = MaterialPlant::where('material_id', $v['material_id'])
                        ->where('plant_id', $v['plant_id'])
                        ->first();

                    // insert here
                    $po = ProductionOrder::create([
                        'order_number' => $po_code,
                        'order_type' => 1,
                        'status' => 1,
                        'requirement_date' => $v['req_date'],
                        'req_source' => 2,
                        'production_date' => $v['req_date'],
                        'mrp_id' => $material_plant ? $material_plant->mrp_controller_id : null,
                        'plant_id' => $v['plant_id'],
                        'storage_id' => $v['storage_id'],
                        'work_center_id' => $v['work_center_id'],
                        'material_id' => $v['material_id'],
                        'quantity' => $v['date_value'],
                        'prod_uom_id' => $v['uom_id'],
                        'gr_quantity' => null,
                        'gr_uom_id' => null,
                        'gr_date' => null,
                        'material_doc' => null,
                        'material_doc_year' => null,
                        'material_doc_item' => null,
                        'reservation_id' => null,
                        'reservation_item' => null,
                        'created_by' => $this->user->id,
                        'updated_by' => $this->user->id,
                        'deleted' => false
                    ]);

                    // update production order number
                    $code->update([
                        'lastcode' => $po_code
                    ]);

                    // get bom
                    $material = Material::find($v['material_id']);

                    $bom = Bom::where('material_id', $v['material_id'])
                        ->where('plant_id', $v['plant_id'])
                        ->first();

                    $bom_details = $bom->detail;

                    foreach ($bom_details as $bom_detail) {
                        // get qty
                        $qty = $bom_detail->quantity * $v['date_value'];
                        // insert bom detail
                        $detail = ProductionOrderDetail::create([
                            'production_order_id' => $po->id,
                            'bom_material_id' => $po->material_id,
                            'bom_qty' => $po->quantity,
                            'component_id' => $bom_detail->component_id,
                            'quantity' => $qty,
                            'base_uom_id' => $material->uom_id
                        ]);
                    }
                }
            }

            DB::commit();

            // \Log::info('succes insert Production schedule');
        } catch (\Exception $e) {
            DB::rollback();

            \Log::info('error insert Production schedule');
            \Log::error($e->getMessage());
            \Log::error($e->getTrace());
        }

        // \Log::info($tes);

        // insert invalid into temporary table
        $invalidData = $data->whereIn('valid', false)->values();
        // \Log::error($invalidData);
        // TODO insert to temporary table
        try {
            DB::beginTransaction();
            
            foreach ($invalidData as $inv) {
                ProductionOrderFailed::create([
                    'plant' => $inv->plant,
                    'work_center' => $inv->work_center,
                    'material' => $inv->material,
                    'description' => $inv->description,
                    'uom' => $inv->base_uom,
                    'month' => $inv->month,
                    'year' => $inv->year,
                    '1' => $inv->c_1,
                    '2' => $inv->c_2,
                    '3' => $inv->c_3,
                    '4' => $inv->c_4,
                    '5' => $inv->c_5,
                    '6' => $inv->c_6,
                    '7' => $inv->c_7,
                    '8' => $inv->c_8,
                    '9' => $inv->c_9,
                    '10' => $inv->c_10,
                    '11' => $inv->c_11,
                    '12' => $inv->c_12,
                    '13' => $inv->c_13,
                    '14' => $inv->c_14,
                    '15' => $inv->c_15,
                    '16' => $inv->c_16,
                    '17' => $inv->c_17,
                    '18' => $inv->c_18,
                    '19' => $inv->c_19,
                    '20' => $inv->c_20,
                    '21' => $inv->c_21,
                    '22' => $inv->c_22,
                    '23' => $inv->c_23,
                    '24' => $inv->c_24,
                    '25' => $inv->c_25,
                    '26' => $inv->c_26,
                    '27' => $inv->c_27,
                    '28' => $inv->c_28,
                    '29' => $inv->c_29,
                    '30' => $inv->c_30,
                    '31' => $inv->c_31,
                    'messages' => $inv->message,
                    'created_by' => $this->user->id
                ]);
            }

            DB::commit();

            // \Log::info('succes insert Production schedule');
        } catch (\Exception $e) {
            DB::rollback();

            \Log::info('error insert Production schedule');
            \Log::error($e->getMessage());
            \Log::error($e->getTrace());
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function headingRow(): int
    {
        return 6;
    }
}
