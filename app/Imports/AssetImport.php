<?php

namespace App\Imports;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\User;
use App\Models\ClassificationMaterial;
use App\Models\ClassificationParameter;
use App\Models\AssetParameter;
use App\Models\GroupMaterial;
use App\Models\Material;
use App\Models\Asset;
use App\Models\DepreTypeName;
use App\Models\Supplier;
use App\Models\Plant;
use App\Models\Location;
use App\Models\Role;
use App\Models\UserGroup;
use App\Models\ValuationAsset;
use App\Models\AssetStatus;
use App\Models\RetiredReason;
use App\Models\CostCenter;
use App\Models\AssetType;
use App\Models\AssetFailedUpload;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class AssetImport implements WithHeadingRow, ToCollection,  WithChunkReading, ShouldQueue
{
	use Importable;

    public function  __construct(User $user)
    {
        $this->user = $user;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $isValid = true;

            // cek material
            $material = null;
            if(!$row['material_code']){
                $isValid = false;
                array_push($messages,"Material is required");
            }else{
                $material = Material::with(['classification'])->where('code',$row['material_code'])->first();
                if( !$material ){
                    $isValid = false;
                    array_push($messages,"Material doesn't exists");
                }
            }

            //cek code
            $asset = null;
            if(!$row['asset_number']){
                $isValid = false;
                array_push($messages,"Asset Number is required");
            }else{
                $asset = Asset::where('code',$row['asset_number'])->where('deleted', false)->first();
                if( $asset ){
                    $isValid = false;
                    array_push($messages,"Asset Number ".$row['asset_number']." already exists");
                }
            }

            //cek superior field
            $level = 1;
            $superior = null;
            if($row['superior_asset']){
                $superior = Asset::where('code',$row['superior_asset'])->first();
                if (!empty($superior)){
                    $level = $superior->level_asset + 1;
                }else{
                    $isValid = false;
                    array_push($messages,"Superior Asset ".$row['superior_asset']." doesn't exists"); 
                }
            }

            //cek old asset number field
            $old_asset = null;
            if($row['old_asset']){
                $old_asset = Asset::where('code',$row['old_asset'])->first();
                if (empty($old_asset)){
                    $isValid = false;
                    array_push($messages,"Old Asset Number ".$row['old_asset']." doesn't exists"); 
                }
            }

            //cek Depreciation Type - Straight Line
            $straight_line = null;
            if(!$row['straight_line']){
                $isValid = false;
                array_push($messages,"Depreciation Type - Straight Line is required");
            }else{
                $straight_line = DepreTypeName::whereHas('depre_group', function ($query){
                                    $query->where('is_straight_line', '=', 1);
                                })->with(['depre_group' => function ($query) {
                                    $query->where('is_straight_line', '=', 1);
                                }])->whereRaw('LOWER(name) = ?', strtolower($row['straight_line']))
                                ->where('deleted', false)->first();

                if(!$straight_line){
                    $isValid = false;
                    array_push($messages,"Depreciation Type - Straight Line ".$row['straight_line']." doesn't exists");
                }
            }
            
            //cek Depreciation Type - Double Decline
            $double_decline = null;
            if(!$row['double_declining']){
                $isValid = false;
                array_push($messages,"Depreciation Type - Double Declining is required");
            }else{
                $double_decline = DepreTypeName::whereHas('depre_group', function ($query){
                                    $query->where('is_straight_line', '=', 0);
                                })->with(['depre_group' => function ($query) {
                                    $query->where('is_straight_line', '=', 0);
                                }])->whereRaw('LOWER(name) = ?', strtolower($row['double_declining']))
                                ->where('deleted', false)->first();

                if(!$double_decline){
                    $isValid = false;
                    array_push($messages,"Depreciation Type - Double Declining ".$row['double_declining']." doesn't exists");
                }
            }

            //cek material group field
            $material_group = null;
            if($row['material_group']){
                $material_group = GroupMaterial::where('material_group',$row['material_group'])->first();
                if (!$material_group){
                    $isValid = false;
                    array_push($messages,"Material Group ".$row['material_group']." doesn't exists"); 
                }
            }

            //cek vendor field
            $vendor = null;
            if($row['vendor_code']){
                $vendor = Supplier::where('code',$row['vendor_code'])->first();
                if(!$vendor){
                    $isValid = false;
                    array_push($messages,"Vendor ".$row['vendor_code']." doesn't exists");
                }
            }

            //cek plant / region field
            $plant = null;
            if(!$row['plant_code']){
                $isValid = false;
                array_push($messages,"Plant is required");
            }else{
                $plant = Plant::where('code',$row['plant_code'])->first();
                if(!$plant){
                    $isValid = false;
                    array_push($messages,"Plant ".$row['plant_code']." doesn't exists");
                }
            }

            //cek location
            $location = null;
            if(!$row['location']){
                $isValid = false;
                array_push($messages,"Location is required");
            }else{
                $location = Location::whereRaw('LOWER(code) = ?', strtolower($row['location']))->first();
                if(!$location){
                    $isValid = false;
                    array_push($messages,"Location ".$row['location']." doesn't exists");
                }
            }

            //cek status asset
            // $status = null;
            // if(!$row['status']){
            //     $isValid = false;
            //     array_push($messages,"Asset Status is required");
            // }else{
            //     $status = AssetStatus::whereRaw('LOWER(name) = ?', strtolower($row['status']))->first();
            //     if(!$status){
            //         $isValid = false;
            //         array_push($messages,"Status ".$row['status']." doesn't exists");
            //     }
            // }

            //cek cost center
            $cost_center = null;
            if(!$row['cost_center']){
                $isValid = false;
                array_push($messages,"Cost Center is required");
            }else{
                $cost_center = CostCenter::where('code',$row['cost_center'])->first();
                if(!$cost_center){
                    $isValid = false;
                    array_push($messages,"Cost Center ".$row['cost_center']." doesn't exists");
                }
            }

            //cek valuation group
            $valuation_group = null;
            if(!$row['group_owner']){
                $isValid = false;
                array_push($messages,"Valuation Group is required");
            }else{
                // $valuation_group = Role::whereRaw('LOWER(name) = ?', strtolower($row['group_owner']))->first();
                $valuation_group = UserGroup::whereRaw('LOWER(code) = ?', strtolower($row['group_owner']))->first();
                if(!$valuation_group){
                    $isValid = false;
                    array_push($messages,"Group Owner ".$row['group_owner']." doesn't exists");
                }

                // if(Role::find(Auth::user()->id_role)->special != 'all-access'){

                //     if ($valuation_group->id != Auth::user()->id_role){
                //         $isValid = false;
                //         array_push($messages,"Valuation Group is invalid");
                //     }
                // }
            }

            //cek asset type
            $asset_type = null;
            if($row['asset_type']){
                $asset_type = AssetType::whereRaw('LOWER(name) = ?', strtolower($row['asset_type']))->first();
                if(!$asset_type){
                    $isValid = false;
                    array_push($messages,"Asset Type ".$row['asset_type']." doesn't exists");
                }
            }

            //cek Valuation Type
            $valuation_type = null;
            if(!$row['valuation_type']){
                $isValid = false;
                array_push($messages,"Valuation Type is required");
            }else{
                $valuation_type = ValuationAsset::whereRaw('LOWER(name) = ?', strtolower($row['valuation_type']))->first();

                if(!$valuation_type){
                    $isValid = false;
                    array_push($messages,"Valuation Type ".$row['valuation_type']." doesn't exists");
                }
            }

            //cek responsible person
            $responsible_person = null;
            if($row['responsible_person']){
                $responsible_person = User::whereRaw('LOWER(email) = ?', strtolower($row['responsible_person']))->first();
                if(!$responsible_person){
                    $isValid = false;
                    array_push($messages,"Responsible Person ".$row['responsible_person']." doesn't exists");
                }
            }

            // Validate Purchase Date
            $purchase_date = null;
            if($row['depreciation_date']) {
                $purchase_date = Carbon::parse($this->formatDateExcel($row['depreciation_date']));
                // $purchase_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['depreciation_date']));
                if($purchase_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $isValid = false;
                    array_push($messages,"Purchase date is not valid");
                    $purchase_date = null;
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Purchase date is required");
                $purchase_date = null;
            }

            // Purchase Price
            if($row['purchase_price']){
                // echo $row['purchase_price'];
                // continue;
                if(is_numeric($row['purchase_price'])==false){
                        $isValid = false;
                        array_push($messages,$row['purchase_price']." is not a number");
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Purchase price is required");
            }

            if(!$row['currency']) {
                $isValid = false;
                array_push($messages,"Currency is required");
            }

            // Validate Warranty End Date
            $warranty_end_date = null;
            if($row['warranty_end']) {
                $warranty_end_date = Carbon::parse($this->formatDateExcel($row['warranty_end']));
                // $warranty_end_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['warranty_end']));
                if($warranty_end_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $warranty_end_date = null;
                }
            }
            else {
                $warranty_end_date = null;
            }

            //check maintenance cycle and mc unit
            $cycle = null;
            $unit = null;
            $count_duration = null;
            if($row['maintenance_cycle'] && $row['mc_unit']){
                $cycle = $row['maintenance_cycle'];
                $unit = $row['mc_unit'];

                switch($unit){
                    case 1: //days
                        $count_duration = $cycle * 1;
                        break;
                    case 2: //month
                        $count_duration = $cycle * 30;
                        break;
                    case 3: //year
                        $count_duration = $cycle * 365;         
                        break;
                }
            }

            // Validate Last Maintenance Date
            $last_maintenance_date = null;
            if($row['last_maintenance']) {
                $last_maintenance_date = Carbon::parse($this->formatDateExcel($row['last_maintenance']));
                // $last_maintenance_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['last_maintenance']));
                if($last_maintenance_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $last_maintenance_date = null;
                }
            }
            else {
                $last_maintenance_date = null;
            }

            // Validate Last Maintenance Date
            // $retired_date = null;
            // if($row['retired_date']) {
            //     $retired_date = Carbon::parse($this->formatDateExcel($row['retired_date']));
            //     // $retired_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['retired_date']));
            //     if($retired_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
            //         $retired_date = null;
            //     }
            // }
            // else {
            //     $retired_date = null;
            // }

            // //cek status retired reason
            // $retired_reason = null;
            // if($row['retired_reason']){
            //     $retired_reason = RetiredReason::whereRaw('LOWER(name) = ?', strtolower($row['retired_reason']))->first();
            //     if(!$retired_reason){
            //         $isValid = false;
            //         array_push($messages,"Retired Reason ".$row['retired_reason']." doesn't exists");
            //     }
            // }

            //fiter header c_ params
            // $headerkey = $row->keys()->all();
            
            // // \Log::info($headerkey);

            // $filtered = [];
            // foreach($headerkey as $value){
            //     if(preg_match('/c_/',$value)){
            //         $filtered[] = $value;
            //     }
            // }

            // // \Log::info($filtered);

            // $params = [];
            // if(!$material){
            //     $isValid = false;
            //     // array_push($messages,"Material Code doesn't exists");
            // } else {
            //     foreach($filtered as $keyparam){
            //         $where = str_replace('c_','',$keyparam);
            //         $wherespace = str_replace('_',' ',$where);
            //         $param = ClassificationParameter::whereRaw('LOWER(name) = ? ', strtolower($wherespace))
            //                     ->where('classification_id', $material->classification_material_id)
            //                     ->get();

            //         $lowkey = strtolower($keyparam);

            //         foreach($param as $v){
            //             $params[] = [
            //                 'id_parameter' => $v->id,
            //                 'value'        => $row[$lowkey],
            //             ];
                    
            //         }
            //     }
            //     // \Log::info($params);
            // }

            // create new data
            $new_data = [
                'material_id'           => ($material ? $material->id : null),
                'material'              => $row['material_code'],
                'description'           => $row['description'],
                'code'                  => $row['asset_number'],
                'serial_number'         => ($row['serial_number'] ? $row['serial_number'] : null),
                'superior_asset_id'     => ($superior ? $superior->id : null),
                'superior_asset'        => $row['superior_asset'],
                'level_asset'           => $level,
                'old_asset'             => ($old_asset ? $old_asset->id : null),
                'old_asset_name'        => $row['old_asset'],
                'straight_line'         => ($straight_line ? $straight_line->id : null),
                'straight_line_name'    => $row['straight_line'],
                'double_decline'        => ($double_decline ? $double_decline->id : null),
                'double_decline_name'   => $row['double_declining'],
                'group_material_id'     => ($material_group ? $material_group->id : null),
                'material_group'        => $row["material_group"],
                'supplier_id'           => ($vendor ? $vendor->id : null),
                'vendor'                => $row["vendor_code"],
                'plant_id'              => ($plant ? $plant->id : null),
                'plant'                 => $row["plant_code"],
                'location_id'           => ($location ? $location->id : null),
                'location'              => $row["location"],
                // 'status_id'             => ($status ? $status->id : null),
                // 'status'                => $row["status"],
                'cost_center_id'        => ($cost_center ? $cost_center->id : null),
                'cost_center'           => $row["cost_center"],
                'valuation_group_id'    => ($valuation_group ? $valuation_group->id : null),
                'group_owner'           => $row["group_owner"],
                'asset_type_id'         => ($asset_type ? $asset_type->id : null),
                'asset_type'            => $row["asset_type"],
                'valuation_type_id'     => ($valuation_type ? $valuation_type->id : null),
                'valuation_type'        => $row["valuation_type"],
                'responsible_person_id' => ($responsible_person ? $responsible_person->id : null),
                'responsible_person'    => $row["responsible_person"],
                'purchase_date'         => $purchase_date,
                'depreciation_date'     => $row['depreciation_date'],
                'purchase_cost'         => $row['purchase_price'],
                'curency'               => $row['currency'],
                'qty_asset'             => $row['quantity_asset'],
                'salvage_value'         => $row['salvage_value'],
                'waranty_finish'        => $warranty_end_date,
                'warranty_end'          => $row['warranty_end'],
                'count_duration'        => $count_duration,
                'maintenance_cycle'     => $row['maintenance_cycle'],
                'unit_duration'         => $cycle,
                'cycle_schedule'        => $unit,
                'maintenance_cycle_unit'=> $row['maintenance_cycle_unit'],
                'last_maintenance'      => $last_maintenance_date,
                'last_maintenance_date' => $row['last_maintenance'],
                // 'retired_date'          => $retired_date,
                // 'retired_date_name'     => $row['retired_date'],
                // 'retired_reason_id'     => ($retired_reason ? $retired_reason->id : null),
                // 'retired_reason'        => $row['retired_reason'],
                // 'retired_remarks'       => $row['retired_remarks'],
                'uploaded_by'           => $this->user->id,
                'valid'                 => $isValid,
                'message'               => implode(', ',$messages),
                // 'params'                => $params, //untuk insert parameter
            ];

            return $new_data;
        });

        // process invalid data
        $data_invalid = $data->whereIn('valid', false)->values();

        if ($data_invalid->count() > 0) {
            // \Log::info('invalid' .$data_invalid.'');
            foreach ($data_invalid as $temp) {
                AssetFailedUpload::create([
                    'material'                  => $temp['material'],
                    'description'               => $temp['description'],
                    'asset_number'              => $temp['code'],
                    'serial_number'             => $temp['serial_number'],
                    'superior_asset'            => $temp['superior_asset'],
                    'old_asset'                 => $temp['old_asset_name'],
                    'straight_line'             => $temp['straight_line_name'],
                    'double_decline'            => $temp['double_decline_name'],
                    'material_group'            => $temp["material_group"],
                    'vendor'                    => $temp["vendor"],
                    'plant'                     => $temp["plant"],
                    'location'                  => $temp["location"],
                    // 'status'                    => $temp["status"],
                    'cost_center'               => $temp["cost_center"],
                    'group_owner'               => $temp["group_owner"],
                    'asset_type'                => $temp["asset_type"],
                    'valuation_type'            => $temp["valuation_type"],
                    'responsible_person'        => $temp["responsible_person"],
                    'depreciation_date'         => $temp['purchase_date'],
                    'purchase_price'            => $temp['purchase_cost'],
                    'curency'                  => $temp['curency'],
                    'quantity_asset'            => $temp['qty_asset'],
                    'salvage_value'             => $temp['salvage_value'],
                    'warranty_end'              => $temp['waranty_finish'],
                    'maintenance_cycle'         => $temp['maintenance_cycle'],
                    'maintenance_cycle_unit'    => $temp['maintenance_cycle_unit'],
                    'last_maintenance'          => $temp['last_maintenance'],
                    // 'retired_date'              => $temp['retired_date'],
                    // 'retired_reason'            => $temp['retired_reason'],
                    // 'retired_remarks'           => $temp['retired_remarks'],
                    'message'                   => $temp['message'],  
                    'uploaded_by'               => $this->user->id
                ]);
            }
        }

        // process valid data
        $data_valid = $data->whereIn('valid', true)->values();

        if ($data_valid->count() > 0) {
            // \Log::info('valid' .$data_valid.'');
            $status_draft = AssetStatus::whereRaw('LOWER(name) = ?', 'draft')->first();
            foreach ($data_valid as $val) {
                $new_asset = Asset::create([
                    'material_id'           => $val['material_id'],
                    'description'           => $val['description'],
                    'code'                  => $val['code'],
                    'serial_number'         => $val['serial_number'],
                    'superior_asset_id'     => $val['superior_asset_id'],
                    'level_asset'           => $val['level_asset'],
                    'old_asset'             => $val['old_asset'],
                    'straight_line'         => $val['straight_line'],
                    'double_decline'        => $val['double_decline'],
                    'group_material_id'     => $val['group_material_id'],
                    'supplier_id'           => $val['supplier_id'],
                    'plant_id'              => $val['plant_id'],
                    'location_id'           => $val['location_id'],
                    'status_id'             => $status_draft->id,
                    'cost_center_id'        => $val['cost_center_id'],
                    'valuation_group_id'    => $val['valuation_group_id'],
                    'asset_type_id'         => $val['asset_type_id'],
                    'valuation_type_id'     => $val['valuation_type_id'],
                    'responsible_person_id' => $val['responsible_person_id'],
                    'purchase_date'         => $val['purchase_date'],
                    'purchase_cost'         => $val['purchase_cost'],
                    'curency'               => $val['curency'],
                    'qty_asset'             => $val['qty_asset'],
                    'salvage_value'         => $val['salvage_value'],
                    'waranty_finish'        => $val['waranty_finish'],
                    'count_duration'        => $val['count_duration'],
                    'unit_duration'         => $val['unit_duration'],
                    'cycle_schedule'        => $val['cycle_schedule'],
                    'last_maintenance'      => $val['last_maintenance'],
                    // 'retired_date'          => $val['retired_date'],
                    // 'retired_reason_id'     => $val['retired_reason_id'],
                    // 'retired_remarks'       => $val['retired_remarks'],
                    'created_by'            => $this->user->id,
                    'updated_by'            => $this->user->id,
                ]);

                // foreach($val['params'] as $dataparam){
                //     // check date type
                //     $c = ClassificationParameter::find($dataparam['id_parameter']);

                //     if ($c->type  == 2) {
                //         AssetParameter::updateOrCreate(
                //             [
                //                 'asset_id'  => $new_asset->id,
                //                 'classification_parameter_id' => $dataparam['id_parameter'],
                //             ],
                //             [
                //                 'value' => Carbon::parse($this->formatDateExcel($dataparam['value'])),
                //                 'created_by' => $this->user->id,
                //                 'updated_by' => $this->user->id,
                //                 'deleted' => 0
                //             ]
                //         );
                //     } else {
                //         AssetParameter::updateOrCreate(
                //             [
                //                 'asset_id'  => $new_asset->id,
                //                 'classification_parameter_id' => $dataparam['id_parameter'],
                //             ],
                //             [
                //                 'value' => $dataparam['value'],
                //                 'created_by' => $this->user->id,
                //                 'updated_by' => $this->user->id,
                //                 'deleted' => 0
                //             ]
                //         );
                //     }
                // }
            }
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    protected function formatDateExcel($date)
    {
        if (gettype($date) === 'double') {
            $birthday = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date); 
            return $birthday->format('n/j/Y');
        } 
        return $date;
    }
}
