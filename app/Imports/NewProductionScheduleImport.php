<?php

namespace App\Imports;

use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use App\Models\Plant;
use App\Models\Storage;
use App\Models\WorkCenter;
use App\Models\Material;
use App\Models\Uom;
use App\Models\Bom;
use App\Models\ProductionOrder;
use App\Models\ProductionOrderDetail;
use App\Models\TransactionCode;
use App\Models\MaterialPlant;
use App\Models\Mrp;
use App\Models\User;
use App\Models\ProductionOrderFailed;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Validators\Failure;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class NewProductionScheduleImport implements WithHeadingRow, ToCollection,  WithChunkReading
{
	use Importable;

    public function  __construct(User $user, $month, $year)
    {
        $this->user = $user;
        $this->year = $year;
        $this->month = $month;
    }

    private $rows = [];

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $data = $rows->map(function($row){
            $messages = [];
            $valid = true;

            // validate plant
            if (!$row['plant']) {
                $valid = false;
                array_push($messages, 'Plant is required');
            } else {
                $plant = Plant::where(DB::raw("LOWER(code)"), strtolower($row['plant']))->first();
                if (!$plant) {
                    $valid = false;
                    array_push($messages, 'Plant '.$row['plant'].' Not Found');
                }
            }

            // validate work center
            if (!$row['work_center']) {
                $valid = false;
                array_push($messages, 'work center is required');
            } else {
                $work_center = WorkCenter::where(DB::raw("LOWER(code)"), strtolower($row['work_center']))->first();
                if (!$work_center) {
                    $valid = false;
                    array_push($messages, 'work center '.$row['work_center'].' Not Found');
                } else {
                    $plant_data = $plant ? $plant->id : 0;
                    if ($plant_data != $work_center->plant_id) {
                        $valid = false;
                        array_push($messages, 'work center & plant not match');
                    }
                }
            }

            // validate material
            if (!$row['material']) {
                $valid = false;
                array_push($messages, 'material is required');
            } else {
                $material = Material::where(DB::raw("LOWER(code)"), strtolower($row['material']))->first();
                if (!$material) {
                    $valid = false;
                    array_push($messages, 'material '.$row['material'].' Not Found');
                } else {
                    $bom = Bom::where('material_id', $material->id)
                        ->where('plant_id', $plant ? $plant->id : null)
                        ->first();

                    if (!$bom) {
                        $valid = false;
                        array_push($messages, 'material '.$row['material'].' Not exist as Bill of material header in plant '.$row['plant'].'');
                    }
                }
            }

            // validate uom
            // if uom not exist get base uom from material
            if (!$row['base_uom']) {
                $uom_material_data = $material ? $material->uom_id : 0;
                $uom = Uom::find($uom_material_data);
            } else {
                $uom = Uom::where(DB::raw("LOWER(code)"), strtolower($row['base_uom']))->first();
                if (!$uom) {
                    $valid = false;
                    array_push($messages, 'Base Uom '.$row['base_uom'].' Not Found');
                } else {
                    $uom_material_data = $material ? $material->uom_id : 0;
                    $uom_data = Uom::find($uom_material_data);
                    if (!$uom_data) {
                        $valid = false;
                        array_push($messages, 'material not have base uom');
                    } else {
                        if ($uom_material_data != $uom->id) {
                            $valid = false;
                            array_push($messages, 'Incorrect base uom used');
                        }
                    }
                }
            }

            // validate month
            if (!$row['month']) {
                $valid = false;
                array_push($messages, 'month is required');
            } else {
                if (!is_numeric($row['month'])) {
                    $valid = false;
                    array_push($messages, 'month invalid. month must numeric');
                } else {
                    if ($row['month'] < 1 | $row['month'] > 12 ) {
                        $valid = false;
                        array_push($messages, 'month invalid. min 1 max 12');
                    }
                }
            }

            // validate year
            if (!$row['year']) {
                $valid = false;
                array_push($messages, 'year is required');
            } else {
                if (!is_numeric($row['year'])) {
                    $valid = false;
                    array_push($messages, 'year invalid. year must numeric');
                }
            }

            // validate date value
            $j = 32;
            for ($i=1; $i < $j; $i++) { 
                if ($row[$i]) {
                    if (!is_numeric($row[$i])) {
                        $valid = false;
                        array_push($messages, 'value date '.$i.' must number');
                    }                    

                    // validate month & year
                    if (is_numeric($row['year']) && is_numeric($row['month'])) {
                        $req_date = Carbon::createFromDate($row['year'], $row['month'], $i);
                        $req_date_formated = Carbon::parse($req_date)->format('Y-m-d');
                        // $now = new Carbon('first day of this month');
                        $now = Carbon::parse(now())->format('Y-m-d');

                        if ($req_date_formated < $now) {
                            $valid = false;
                            array_push($messages, 'production schedule date '.$req_date_formated.' invalid, must greather or equal than '.date('d-m-Y').'');
                        }

                        // validate year & month must equal to requested date
                        if ((int)$row['year'] != (int)$this->year) {
                            $valid = false;
                            array_push($messages, 'year must equal to production date');
                        }

                        if ((int)$row['month'] != (int)$this->month) {
                            $valid = false;
                            array_push($messages, 'month must equal to production date');
                        }
                    }
                }
            }

            // create new data
            $new_data = [
                'uploaded_by' => $this->user->id,
                'month' => $row['month'],
                'year' => $row['year'],
                'plant' => $row['plant'],
                'work_center' => $row['work_center'],
                'material' => $row['material'],
                'description' => $row['description'],
                'base_uom' => $row['base_uom'],
                'plant_id' => isset($plant) ? $plant->id : null,
                'work_center_id' => isset($work_center) ? $work_center->id : null,
                'storage_id' => isset($work_center) ? $work_center->storage_result_id : null,
                'material_id' => isset($material) ? $material->id : null,
                'uom_id' => isset($uom) ? $uom->id : null,
                'v_1' => $row[1],
                'v_2' => $row[2],
                'v_3' => $row[3],
                'v_4' => $row[4],
                'v_5' => $row[5],
                'v_6' => $row[6],
                'v_7' => $row[7],
                'v_8' => $row[8],
                'v_9' => $row[9],
                'v_10' => $row[10],
                'v_11' => $row[11],
                'v_12' => $row[12],
                'v_13' => $row[13],
                'v_14' => $row[14],
                'v_15' => $row[15],
                'v_16' => $row[16],
                'v_17' => $row[17],
                'v_18' => $row[18],
                'v_19' => $row[19],
                'v_20' => $row[20],
                'v_21' => $row[21],
                'v_22' => $row[22],
                'v_23' => $row[23],
                'v_24' => $row[24],
                'v_25' => $row[25],
                'v_26' => $row[26],
                'v_27' => $row[27],
                'v_28' => $row[28],
                'v_29' => $row[29],
                'v_30' => $row[30],
                'v_31' => $row[31],
                'valid' => $valid,
                'message' => implode(', ',$messages)
            ];

            return $new_data;
        });

        $this->rows = $data->toArray();

        // \Log::info($data);
        // \Log::info($this->rows);
        // \Log::info($this->year);
        // \Log::info($this->month);
    }

    public function getRow(): array
    {
        return $this->rows;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function headingRow(): int
    {
        return 6;
    }
}
