<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialParameter extends Model
{
    protected $fillable = [		
		'material_id', 'classification_parameter_id', 'value', 'created_by', 'updated_by', 'deleted'
	];

    public function classification_parameter()
    {
        return $this->belongsTo('App\Models\ClassificationParameter', 'classification_parameter_id');
    }

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'id', 'material_id');
    }
}
