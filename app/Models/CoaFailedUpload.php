<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoaFailedUpload extends Model
{
    protected $fillable = [
    	'coa_code', 'coa_description', 'account_group_code', 'account_group_description', 'group_type',
        'account_type', 'gl_account', 'gl_description', 'cash', 'message', 'uploaded_by'
    ];
}
