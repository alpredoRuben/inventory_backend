<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialVendor extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialVendor';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [		
		'material_id', 'vendor_id', 'proc_group_id', 'plant_id', 'storage_id', 'lead_time', 'unit_price',
        'currency', 'validity_from', 'std_qty', 'std_uom_id', 'min_qty', 'min_uom_id','created_by',
        'updated_by', 'deleted', 'vendor_material', 'purchase_header_id', 'purchase_item_id'
	];

    protected $casts = [
        'std_qty'  => 'float',
        'min_qty'  => 'float',
    ];

    public function history()
    {
        return $this->hasMany('App\Models\MaterialVendorHistory', 'material_vendor_id')->orderBy('id', 'desc');
    }

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function purchase_header()
    {
        return $this->belongsTo('App\Models\PurchaseHeader', 'purchase_header_id');
    }

    public function purchase_item()
    {
        return $this->belongsTo('App\Models\PurchaseItem', 'purchase_item_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'vendor_id');
    }

    public function proc_group()
    {
        return $this->belongsTo('App\Models\ProcurementGroup', 'proc_group_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function std_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'std_uom_id');
    }

    public function min_uom()
    {
    	return $this->belongsTo('App\Models\Uom', 'min_uom_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
