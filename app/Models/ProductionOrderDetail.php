<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProductionOrderDetail extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ProductionOrderDetail';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'production_order_id', 'bom_material_id', 'bom_qty', 'component_id' , 'quantity', 'base_uom_id'
	];

    public function production_order()
    {
        return $this->belongsTo('App\Models\ProductionOrder', 'production_order_id');
    }

    public function bom_material()
    {
        return $this->belongsTo('App\Models\Material', 'bom_material_id');
    }

    public function component()
    {
        return $this->belongsTo('App\Models\Material', 'component_id');
    }

    public function base_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'base_uom_id');
    }
}
