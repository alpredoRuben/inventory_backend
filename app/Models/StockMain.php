<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class StockMain extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'StockMain';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'material_id', 'plant_id', 'storage_id', 'period', 'qty_unrestricted', 'qty_blocked',
		'qty_transit', 'qty_qa', 'created_by', 'updated_by', 'deleted'
	];

    protected $casts = [
        'qty_unrestricted' => 'float',
        'qty_blocked' => 'float',
        'qty_transit' => 'float',
        'qty_qa' => 'float',
    ];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function batch_stock()
    {
        return $this->hasMany('App\Models\StockBatch')->where('deleted', false);
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
