<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ReservationAttachment extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ReservationAttachment';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [    
    	'reservation_id', 'name', 'created_by', 'original_name'
    ];

    protected $appends = [
        'url_full'
    ];

    public function getUrlFullAttribute()
    {
        if ($this->name && \Storage::disk('public')->exists($this->name)) {
            return \Storage::disk('public')->url($this->name);
        } else {
            return null;
        }
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
