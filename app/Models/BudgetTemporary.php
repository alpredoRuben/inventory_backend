<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetTemporary extends Model
{
    protected $fillable = [
    	'fiscal_year', 'description', 'project_code', 'cost_center', 'gl_account', 'material', 'amount', 'currency',
        'company', 'created_by'
    ];
}
