<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class WorkCenter extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'WorkCenter';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'code', 'description', 'cost_center_id', 'storage_supply_id', 'storage_result_id',
		'responsible_user_id', 'plant_id', 'created_by', 'updated_by', 'deleted'
	];

	public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function storage_supply()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_supply_id');
    }

    public function storage_result()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_result_id');
    }

    public function responsible_user()
    {
        return $this->belongsTo('App\Models\User', 'responsible_user_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function materials()
    {
        return $this->belongsToMany('App\Models\Material', 'material_work_centers', 'work_center_id', 'material_id')
                ->withPivot('id')
                ->withTimestamps();
    }

	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
