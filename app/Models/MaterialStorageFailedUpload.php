<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialStorageFailedUpload extends Model
{
    protected $fillable = [
        'material_code', 'plant', 'storage', 'minimum_stock', 'maximum_stock', 'storage_bin', 'storage_condition', 'storage_type',
        'temp_condition', 'le_quantity', 'le_unit', 'message', 'uploaded_by'
    ];
}
