<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountGroup extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AccountGroup';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'code', 'description', 'group_type_id', 'account_type_id', 'chart_of_account_id', 'parent_id', 'created_by', 'updated_by',
        'deleted'
    ];

    /* group type
    *1=Asset; 2=Liability; 3=Equity; 4=Profit; 5=Loss
    */

    public function chart_of_account()
    {
        return $this->belongsTo('App\Models\ChartOfAccount', 'chart_of_account_id');
    }

    public function accounts()
    {
        return $this->hasMany('App\Models\Account', 'account_group_id');
    }

    public function group_type()
    {
        return $this->belongsTo('App\Models\GroupType', 'group_type_id');
    }

    public function account_type()
    {
        return $this->belongsTo('App\Models\AccountType', 'account_type_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\AccountGroup', 'parent_id');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\AccountGroup', 'id', 'parent_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
