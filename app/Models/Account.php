<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Account extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'Account';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'code', 'description', 'is_cash', 'account_group_id', 'parent_id', 'created_by', 'updated_by',
        'deleted'
    ];

    public function account_group()
    {
        return $this->belongsTo('App\Models\AccountGroup', 'account_group_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\AccountGroup', 'parent_id');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\AccountGroup', 'id', 'parent_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
