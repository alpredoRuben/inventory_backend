<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ServiceRequest extends Model
{
    use SoftDeletes;
    
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ServiceRequest';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $fillable = [
		'service_id', 'prob_id', 'service_route_destination', 'service_request', 'service_creator', 'ftimestap', 
		'service_state', 'destination', 'plant_id', 'plant_name', 'prob_description', 'last_reply', 'target_time', 
		'service_duration', 'category_name', 'dispatch', 'service_info_pengecekan', 'service_customer_info_rel', 
		'service_info_indikasi', 'service_info_progres_noc', 'service_info_lokasi', 'service_tipe', 
		'status_service_create_wo', 'status_close_service', 'comment_service', 'created_by', 'updated_by', 
	];

    protected $dates = ['deleted_at'];

    protected $with = [
        'createdBy',
        'updatedBy',
    ];

    public function plant()
    {
        return $this->hasOne('App\Models\Plant', 'id', 'plant_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function work_order()
    {
        return $this->hasMany('App\Models\TrxWo', 'service_id', 'id');
    }
}
