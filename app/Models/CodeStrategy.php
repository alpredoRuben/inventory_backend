<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodeStrategy extends Model
{
    protected $table = 'code_strategies';
}
