<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PurchaseItem extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'PurchaseItem';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'purchase_header_id', 'purchasing_item_no', 'account_type', 'material_id', 'short_text',
        'long_text', 'plant_id', 'storage_id', 'target_qty', 'order_qty', 'order_unit_id',
        'price_unit', 'delivery_location', 'gl_account_id', 'cost_center_id', 'dlv_complete',
        'fund_center', 'commitment_item', 'reservation_id', 'pr_number', 'pr_item_number',
        'purc_doc_number', 'purc_item_number', 'created_by', 'updated_by', 'deleted',
        'group_material_id', 'project_id', 'delivery_date', 'currency', 'completed', 'tax', 'tax_item'
	];

	/*Account Type
	* 0. material for stock
    * 1. free text / material for cost center, jika tipe ini wajib mengisi cost center
    * 2. material for asset
	*/

    public function gl_account()
    {
        return $this->belongsTo('App\Models\Account', 'gl_account_id');
    }

    public function purchase_header()
    {
        return $this->belongsTo('App\Models\PurchaseHeader', 'purchase_header_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function group_material()
    {
        return $this->belongsTo('App\Models\GroupMaterial', 'group_material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function order_unit()
    {
        return $this->belongsTo('App\Models\Uom', 'order_unit_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function purchase_document_history()
    {
        return $this->hasMany('App\Models\PurchaseDocumentHistory', 'purchase_item_id');
    }

	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
