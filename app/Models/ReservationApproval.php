<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationApproval extends Model
{
    protected $fillable = [    
    	'reservation_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** Reservation Status
    *0 = draft
    *1 = pending_approval
    *2 = approved
    *3 = rejected
    *4 = canceled
    *5 = processed
    */

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
