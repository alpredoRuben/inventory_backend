<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetConsumption extends Model
{
    protected $fillable = [
    	'budget_id', 'doc_type', 'doc_number', 'doc_item', 'currency', 'amount', 'posting_date', 'reservation_id', 'purchase_item_id', 'created_by',
    ];

    protected $casts = [
        'amount' => 'float'
    ];

    public function budget()
    {
        return $this->belongsTo('App\Models\Budget', 'budget_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function purchase_item()
    {
        return $this->belongsTo('App\Models\PurchaseItem', 'purchase_item_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
