<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetImage extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AssetImage';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'asset_id', 'image', 'created_by', 'updated_by', 'deleted'
	];

	protected $appends = [
        'asset_image_url_full'
    ];

    public function getAssetImageUrlFullAttribute()
    {
        if ($this->image && \Storage::disk('public')->exists($this->image)) {
            return \Storage::disk('public')->url($this->image);
        } else {
            return null;
        }
    }

	public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }
}
