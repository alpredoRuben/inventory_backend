<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class TrxWo extends Model
{
    use SoftDeletes;

    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'TrxWo';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
	];

    public function service()
    {
        return $this->hasOne('App\Models\ServiceRequest', 'id', 'service_id');
    }

    public function plant()
    {
        return $this->hasOne('App\Models\Plant', 'id', 'plant_id');
    }

    public function wo_level()
    {
        return $this->hasOne('App\Models\WoLevel', 'id', 'wo_level_id');
    }

    public function wo_status()
    {
        return $this->hasOne('App\Models\WoStatus', 'id', 'wo_status_id');
    }

    public function wo_type()
    {
        return $this->hasOne('App\Models\WoType', 'id', 'wo_type_id');
    }

    public function created_by()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updated_by()
    {
        return $this->hasOne('App\Models\user', 'id', 'updated_by');
    }

    // public function serpo()
    // {
    //     return $this->hasOne('App\Models\User', 'id', 'service_point_id');
    // }

    // public function basecamp()
    // {
    //     return $this->hasOne('App\Models\Supplier', 'id', 'timserpo_id');
    // }

    public function history_status_wos()
    {
        return $this->hasMany('App\Models\HistoryStatusWo', 'trx_wo_id', 'id')->orderBy('id');
    }

    public function wo_asset()
    {
        return $this->hasMany('App\Models\WoAsset', 'trx_wo_id', 'id');
    }

    // public function schedule()
    // {
    //     return $this->belongsTo('App\Models\Schedule', 'schedule_id', 'id');
    // }
}
