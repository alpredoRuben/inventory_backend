<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class StockSerial extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'StockSerial';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'serial', 'material_id', 'plant_id', 'storage_id', 'stock_type', 'vendor_id', 'customer_id',
        'status', 'batch_number', 'created_by', 'updated_by', 'deleted'
	];

	/* stock type
	*1=unrest
	*2=in-transit
	*3=quality
	*4=blocked
	*/

	/*status
	*1 AVLB = Setelah GI, Atau nanti bs aja masukin data serial sebelum jd stock
	*2 ESTO = kalau ada isinya plant/storage
	*3 EQUI = kalau jadi asset status
	*/

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Supplier', 'customer_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'vendor_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
