<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DepreTypeName extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'DepreTypeName';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $table = 'depre_type_name';

    // public $primaryKey = 'id';

    protected $fillable = ['name','depre_type_group_id','deleted','created_by','updated_by'];

    public $timestamps = false;

    public function asset() {

        return $this->hasMany('App\Models\Asset', 'id_depreciation_type');
    }

    public function depre_group() {
        
        return $this->belongsTo('App\Models\DepreTypeGroup', 'depre_type_group_id', 'id');
    }

    public function depre_period() {
        
        return $this->hasMany('App\Models\DepreTypePeriod', 'depre_type_name_id', 'id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}