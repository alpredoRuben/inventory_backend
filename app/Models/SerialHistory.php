<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class SerialHistory extends Model
{
    protected $fillable = [
        'plant_id', 'storage_id', 'material_id', 'serial', 'movement_history_id', 'created_by', 'updated_by'
	];

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Stoage', 'storage_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function movement_history()
    {
        return $this->belongsTo('App\Models\MovementHistory', 'movement_history_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
