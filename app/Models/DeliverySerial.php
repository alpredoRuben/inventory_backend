<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DeliverySerial extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'DeliverySerial';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'delivery_header_id', 'delivery_item', 'sequence', 'serial', 'material_id',
    	'created_by', 'updated_by', 'deleted'
    ];

    public function delivery_header()
    {
        return $this->belongsTo('App\Models\DeliveryHeader', 'delivery_header_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
