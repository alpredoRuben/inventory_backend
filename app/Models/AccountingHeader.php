<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountingHeader extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AccountingHeader';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
        'company_id', 'document_no', 'year', 'document_type_id', 'doc_date', 'posting_date', 'fiscal_period', 'currency',
        'reference', 'header_text', 'status', 'created_by'
    ];

    /* Status
    * 0. Parked
    * 1. Posted
    * 2. Cleared
    */

    public function detail()
    {
        return $this->hasMany('App\Models\AccountingDetail', 'accounting_header_id');
    }

    public function attachment()
    {
        return $this->hasMany('App\Models\AccountingAttachment', 'accounting_header_id');
    }

    public function document_type()
    {
        return $this->belongsTo('App\Models\DocumentType', 'document_type_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
