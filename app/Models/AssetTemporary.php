<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetTemporary extends Model
{
    protected $fillable = [
        'material', 'description', 'asset_number', 'serial_number', 'superior_asset', 'old_asset', 'straight_line',
        'double_decline', 'material_group', 'vendor', 'plant', 'location', 'group_owner', 'asset_type', 
        'valuation_type', 'responsible_person', 'status', 'purchase_date', 'purchase_price', 'currency', 
        'quantity', 'salvage', 'warranty_finish', 'maintenance_cycle', 'unit_cycle', 'last_maintenance', 
        'created_by'
    ];
}
