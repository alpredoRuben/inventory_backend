<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupType extends Model
{
    protected $fillable = [
		'bs_pl', 'code'
	];
}
