<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ResponsiblePersonHistory extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ResponsiblePersonHistory';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'asset_id', 'old_responsible_person_id', 'created_by', 'deleted'
    ];

    public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }

    public function old()
    {
        return $this->belongsTo('App\Models\User', 'id', 'old_responsible_person_id');
    }

    public function old_responsible_person()
    {
        return $this->belongsTo('App\Models\User', 'old_responsible_person_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
