<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DeliveryItem extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'DeliveryItem';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'delivery_header_id', 'delivery_item', 'sequence', 'plant_id', 'storage_id', 'batch',
    	'reservation_id', 'material_id', 'uom_id', 'delivery_qty', 'pick_qty', 'sales_order',
    	'sales_order_item', 'stock_batch_id', 'created_by', 'updated_by', 'deleted'
    ];

    protected $appends = [
        'expiry_date', 'serials'
    ];

    protected $with = ['material'];

    protected $casts = [
        'pick_qty' => 'float'
    ];

    public function getExpiryDateAttribute()
    {
        $batch = StockBatch::where('batch_number', $this->batch)->first();

        if ($batch) {
            return $batch->sled_date;
        } else {
            return null;
        }
    }

    public function getSerialsAttribute()
    {
        $serials = DeliverySerial::where('delivery_header_id', $this->delivery_header_id)
            ->where('material_id', $this->material_id)
            ->where('sequence', $this->sequence)
            ->get();

        return $serials;
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function stock_batch()
    {
        return $this->belongsTo('App\Models\StockBatch', 'stock_batch_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function delivery_header()
    {
        return $this->belongsTo('App\Models\DeliveryHeader', 'delivery_header_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
