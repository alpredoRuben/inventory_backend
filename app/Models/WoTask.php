<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class WoTask extends Model
{
    use SoftDeletes;

    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'WoTask';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $guarded = [
		'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function assetWoTask()
    {
        return $this->belongsTo('App\Models\Asset', 'asset_id', 'id');
    }

    // public function taskImages(){
    //     return $this->hasMany('App\Models\TaskImage');
    // }
	
    // public function readingIndicatorHistories(){
    //     return $this->hasMany('App\Models\ReadingIndicatorHistory');
    // }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function plant()
    {
        return $this->hasOne('App\Models\Plant', 'id', 'plant_id');
    }

    public function classification()
    {
        return $this->hasOne('App\Models\ClassificationMaterial', 'id', 'classification_id');
    }
}
