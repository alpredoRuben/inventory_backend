<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LocationHistory extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'LocationHistory';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'asset_id', 'old_location_id', 'address_location', 'latitude', 'longitude', 'province', 'city',
        'building', 'unit', 'created_by', 'deleted', 'cost_center_id', 'location_code'
    ];

    public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }

    public function old()
    {
        return $this->belongsTo('App\Models\Location', 'id', 'old_location_id');
    }

    public function old_location()
    {
        return $this->belongsTo('App\Models\Location', 'old_location_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }
}
