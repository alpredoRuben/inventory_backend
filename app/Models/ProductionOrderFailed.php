<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionOrderFailed extends Model
{
    protected $fillable = [
		'plant', 'work_center', 'material', 'description', 'uom', 'month', 'year', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29',
        '30', '31', 'messages', 'created_by'
	];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
