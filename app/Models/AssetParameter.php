<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetParameter extends Model
{
    protected $fillable = [		
		'asset_id', 'classification_parameter_id', 'value', 'created_by', 'updated_by', 'deleted'
	];

	public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }

    public function classification_parameter()
    {
    	return $this->belongsTo('App\Models\ClassificationParameter', 'classification_parameter_id', 'id');
    }
}
