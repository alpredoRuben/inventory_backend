<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialUom extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialUom';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [		
		'material_id', 'base_value', 'uom_id', 'value_conversion', 'created_by', 'updated_by', 'deleted'
	];

    protected $casts = [
        'base_value'        => 'float',
        'value_conversion'  => 'float',
    ];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function uom()
    {
    	return $this->belongsTo('App\Models\Uom', 'uom_id');
    }
}
