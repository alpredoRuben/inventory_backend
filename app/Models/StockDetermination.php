<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class StockDetermination extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'StockDetermination';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'plant_id',	'storage_id', 'sequence', 'supply_plant_id', 'supply_storage_id',
		'ext_purchase', 'created_by', 'updated_by',
	];

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function supply_plant()
    {
        return $this->belongsTo('App\Models\Plant', 'supply_plant_id');
    }

    public function supply_storage()
    {
        return $this->belongsTo('App\Models\Storage', 'supply_storage_id');
    }

	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
