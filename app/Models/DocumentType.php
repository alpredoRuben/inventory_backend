<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $fillable = [
		'code', 'description', 'account_type', 'active'
    ];
    
    /* Account Type
    * A=Assets, D=Customers, K=Vendors, M=Material, S=G/L accounts
    */
}
