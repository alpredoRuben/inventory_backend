<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialSalesFailedUpload extends Model
{
    protected $fillable = [
        'material_code', 'sales_organization', 'delivering_plant', 'tax_class', 'account_assignment', 'item_category',
        'message', 'uploaded_by'
    ];
}
