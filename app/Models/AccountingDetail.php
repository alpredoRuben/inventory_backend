<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountingDetail extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AccountingDetail';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'accounting_header_id', 'seq_number', 'account_type', 'posting_key_id', 'd_c', 'clearing_date', 'clearing_created_time',
        'clearning_doc', 'tax_code', 'tax_type', 'amount', 'currency', 'amount_lc', 'assignment', 'co_area', 'cost_center_id',
        'purchasing_doc_no', 'purchasing_doc_item', 'billing_doc', 'sales_order', 'item_no', 'asset_id', 'account_id',
        'vendor_id', 'customer_id', 'bl_pl', 'baseline_date', 'term_payment_id', 'due_date'
    ];
    
    public function header()
    {
        return $this->belongsTo('App\Models\AccountingHeader', 'accounting_header_id');
    }

    public function posting_key()
    {
        return $this->belongsTo('App\Models\PostingKey', 'posting_key_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'asset_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'vendor_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Supplier', 'customer_id');
    }

    public function term_payment()
    {
        return $this->belongsTo('App\Models\TermPayment', 'term_payment_id');
    }
}
