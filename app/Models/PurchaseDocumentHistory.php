<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseDocumentHistory extends Model
{
    protected $fillable = [
		'purchase_header_id', 'purchase_item_id', 'transaction', 'material_year', 'material_doc', 'material_doc_item',
        'de_ce', 'quantity', 'amount_lc', 'quantity_order', 'amount', 'reference_doc'
	];

	/** transaction
	* 1. Goods Reciept
	* 2. Invoice Reciept
	* 3. Down Payment
	*/

    public function purchase_header()
    {
        return $this->belongsTo('App\Models\PurchaseHeader', 'purchase_header_id');
    }

    public function purchase_item()
    {
        return $this->belongsTo('App\Models\PurchaseItem', 'purchase_item_id');
    }
}
