<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProductionOrder extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ProductionOrder';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'order_number', 'order_type', 'status', 'requirement_date', 'req_source', 'production_date', 'mrp_id', 'plant_id', 'storage_id',
        'work_center_id', 'material_id', 'quantity', 'prod_uom_id', 'gr_quantity', 'gr_uom_id', 'gr_date', 'material_doc',
        'material_doc_year', 'material_doc_item', 'reservation_id', 'reservation_item', 'created_by', 'updated_by', 'deleted'
	];

    /**
    * 1. Production Order
    **/

    /** Status
    * 1. Draft, masih dalam rencana produksi
	* 2. Confirmed, dokumen digunakan dalam proses produksi
	* 3. Completed, proses produksi selesai, sdh GR from Produksi
    **/

    /** Req source
    * 1. Reservation
	* 2. Independent (dari upload data produksi)
    **/

    public function detail()
    {
        return $this->hasMany('App\Models\ProductionOrderDetail', 'production_order_id');
    }

	public function mrp()
    {
        return $this->belongsTo('App\Models\Mrp', 'mrp_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function work_center()
    {
        return $this->belongsTo('App\Models\WorkCenter', 'work_center_id');
    }

    public function prod_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'prod_uom_id');
    }

    public function gr_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'gr_uom_id');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Models\Reservation', 'reservation_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
