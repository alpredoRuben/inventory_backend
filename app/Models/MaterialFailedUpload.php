<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialFailedUpload extends Model
{
    protected $fillable = [
    	'material_type', 'code', 'description', 'group_material', 'classification', 'old_material', 'base_material', 'sku_code',
        'uom', 'nett_weight_value', 'nett_weight_uom', 'gross_weight_value', 'volume_value', 'volume_uom', 'serial_profile',
        'message', 'uploaded_by'
    ];
}
