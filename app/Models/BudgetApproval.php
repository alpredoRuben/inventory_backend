<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BudgetApproval extends Model
{
	protected $fillable = [    
    	'budget_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** Status
    *0.Draft
    *1.Submitted
    *2.Active
    *3.Completed
    *4.Hold
    *5.Cancelled
    **/

    public function budget()
    {
        return $this->belongsTo('App\Models\Budget', 'budget_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
