<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MovingPriceHistory extends Model
{
    protected $fillable = [
        'plant_id', 'material_id', 'movement_history_id', 'gr_date', 'gr_price', 'gr_quantity', 'moving_price'
    ];

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function movement_history()
    {
        return $this->belongsTo('App\Models\MovementHistory', 'movement_history_id');
    }
}
