<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class TrxTaskList extends Model
{
    use SoftDeletes;
    
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'TrxTaskList';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $fillable = [
		'tasklist_id', 'name', 'material_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 
	];

    protected $dates = ['deleted_at'];

    protected $with = [
        'createdBy',
        'updatedBy',
    ];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function material()
    {
        return $this->hasOne('App\Models\Material', 'id', 'material_id');
    }

    public function item_list()
    {
        return $this->hasMany('App\Models\TrxTaskListItem', 'tasklist_id', 'id');
    }
}
