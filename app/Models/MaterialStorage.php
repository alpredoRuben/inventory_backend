<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialStorage extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialStorage';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'material_id', 'plant_id', 'storage_id', 'min_stock', 'max_stock', 'bin_indicator',
		'storage_condition_id', 'storage_type_id', 'temp_condition_id', 'le_quantity', 'le_unit_id',
        'storage_bin'
	];

    protected $casts = [
        'min_stock' => 'float',
        'max_stock' => 'float'
    ];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function storage_condition()
    {
        return $this->belongsTo('App\Models\StorageCondition', 'storage_condition_id');
    }

    public function storage_type()
    {
        return $this->belongsTo('App\Models\StorageType', 'storage_type_id');
    }

    public function temp_condition()
    {
        return $this->belongsTo('App\Models\TempCondition', 'temp_condition_id');
    }

    public function le_unit()
    {
        return $this->belongsTo('App\Models\Uom', 'le_unit_id');
    }
}
