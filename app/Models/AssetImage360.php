<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetImage360 extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AssetImage360';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'asset_id', 'image360', 'created_by', 'updated_by', 'deleted'
	];

	protected $appends = [
        'asset_image_360_url_full'
    ];

    public function getAssetImage360UrlFullAttribute()
    {
        if ($this->image360 && \Storage::disk('public')->exists($this->image360)) {
            return \Storage::disk('public')->url($this->image360);
        } else {
            return null;
        }
    }

	public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }
}
