<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialAccount extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialAccount';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

     protected $fillable = [
		'material_id', 'plant_id', 'valuation_type_id', 'valuation_class_id', 'price_controll',
        'moving_price', 'standart_price'
    ];
    
    // price controll
    // 0. MAP, 1. Standard, 2. LIFO/FIFO

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function valuation_type()
    {
        return $this->belongsTo('App\Models\ValuationType', 'valuation_type_id');
    }

    public function valuation_class()
    {
        return $this->belongsTo('App\Models\ValuationClass', 'valuation_class_id');
    }
}
