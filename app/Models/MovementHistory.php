<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MovementHistory extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MovementHistory';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $fillable = [
		'material_doc', 'doc_year', 'item_no', 'posting_date', 'movement_type_id', 'movement_type_reason_id',
        'plant_id', 'storage_id', 'material_id', 'batch_no', 'quantity', 'base_uom_id', 'quantity_entry',
        'entry_uom_id', 'de_ce', 'amount', 'currency', 'document_date', 'doc_header_text', 'material_slip',
        'supplier_id', 'customer_id', 'cost_center_id', 'work_center_id', 'po_number', 'po_item', 'work_order',
        'project', 'delivery_no', 'delivery_item', 'accounting_no', 'accounting_line', 'created_by', 'updated_by',
        'material_description'
	];

    protected $casts = [
        'quantity_entry' => 'float'
    ];

    public function serial()
    {
        return $this->hasMany('App\Models\SerialHistory', 'movement_history_id');
    }

    public function movement_type()
    {
        return $this->belongsTo('App\Models\MovementType', 'movement_type_id');
    }

    public function movement_type_reason()
    {
        return $this->belongsTo('App\Models\MovementTypeReason', 'movement_type_reason_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function base_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'base_uom_id');
    }

    public function entry_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'entry_uom_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Supplier', 'customer_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function work_center()
    {
        return $this->belongsTo('App\Models\WorkCenter', 'work_center_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
