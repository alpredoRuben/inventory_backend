<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\DepreTypePeriod;

class Asset extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'Asset';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
        'code', 'description', 'material_id', 'asset_type_id', 'plant_id', 'location_id', 'supplier_id', 
        'status_id', 'valuation_group_id', 'retired_reason_id', 'responsible_person_id', 'old_asset', 
        'purchase_date', 'purchase_cost', 'waranty_start', 'waranty_finish', 'serial_number', 'level_asset', 
        'superior_asset_id', 'count_duration', 'unit_duration', 'cycle_schedule', 'curency', 'retired_date', 
        'retired_remarks', 'last_maintenance', 'next_maintenance', 'created_by', 'updated_by', 'deleted', 
        'straight_line', 'double_decline', 'valuation_type_id', 'group_material_id', 'qty_asset', 'salvage_value',
        'cost_center_id'
    ];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function asset_type()
    {
        return $this->belongsTo('App\Models\AssetType', 'asset_type_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location', 'location_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\AssetStatus', 'status_id');
    }

    public function valuation_group()
    {
        return $this->belongsTo('App\Models\UserGroup', 'valuation_group_id');
    }

    public function retired_reason()
    {
        return $this->belongsTo('App\Models\RetiredReason', 'retired_reason_id');
    }

    public function responsible_person()
    {
        return $this->belongsTo('App\Models\User', 'responsible_person_id');
    }

    public function responsible_person_history()
    {
        return $this->belongsTo('App\Models\ResponsiblePersonHistory', 'asset_id');
    }

    public function many_responsible_person_history()
    {
        return $this->hasMany('App\Models\ResponsiblePersonHistory', 'asset_id')->orderBy('created_at', 'desc');
    }

    public function location_history()
    {
        return $this->hasMany('App\Models\LocationHistory', 'asset_id')->orderBy('created_at', 'desc');
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\Asset', 'superior_asset_id');
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'superior_asset_id');
    }

    public function asset_parameters()
    {
        return $this->hasMany('App\Models\AssetParameter', 'asset_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\AssetImage', 'asset_id');
    }

    public function image360()
    {
        return $this->hasMany('App\Models\AssetImage360', 'asset_id');
    }

    public function area()
    {
        return $this->hasMany('App\Models\AssetArea', 'asset_id');
    }

    public function straightline(){
        return $this->belongsTo('App\Models\DepreTypeName','straight_line');
    }

    public function doubledecline(){
        return $this->belongsTo('App\Models\DepreTypeName','double_decline');
    }

    public function valuationasset(){
        return $this->belongsTo('App\Models\ValuationAsset','valuation_type_id','id');
    }

    public function groupmaterial(){
        return $this->belongsTo('App\Models\GroupMaterial','group_material_id','id');
    }

    public function wo_asset()
    {
        return $this->hasMany('App\Models\WoAsset', 'asset_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function depreciation()
    {
        return $this->hasMany('App\Models\AssetDepreciation', 'asset_id');
    }

    public function depre_sl()
    {
        return $this->depreciation()->where('depre_type', 'sl');
    }

    public function depre_dd() {
        return $this->depreciation()->where('depre_type', 'dd');
    }
}
