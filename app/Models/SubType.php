<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubType extends Model
{
    protected $fillable = [
		'code', 'description'
	];
}
