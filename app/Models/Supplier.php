<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Supplier extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'Supplier';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'type', 'code', 'description', 'location_id', 'halal_certified', 'halal_license_number',
        'halal_valid_to', 'created_by', 'updated_by', 'deleted', 'vendor_group_id', 'vat_no', 'tax_indicator',
        'email', 'gl_account_id'
	];

    /* Vendor type
    * 1 = vendor
    * 2 = manufacturer
    * 3 = customer
    * 4 = vendor one time
    */

    /* tax_indicator
    * 0 = no ppn
    * 10 = ppn 10%
    */

	public function location()
    {
        return $this->belongsTo('App\Models\Location', 'location_id');
    }

    public function gl_account()
    {
        return $this->belongsTo('App\Models\Account', 'gl_account_id');
    }

    public function vendor_group()
    {
        return $this->belongsTo('App\Models\VendorGroup', 'vendor_group_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
