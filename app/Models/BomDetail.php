<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BomDetail extends Model
{
    protected $fillable = [
    	'bom_id', 'component_id', 'quantity', 'uom_id', 'quantity_entry', 'uom_entry_id', 'valid_from', 'valid_to',
    	'remarks', 'created_by', 'updated_by', 'deleted', 'item'
    ];

    protected $casts = [
        'quantity' => 'float',
        'quantity_entry' => 'float',
    ];

    public function bom()
    {
        return $this->belongsTo('App\Models\Bom', 'bom_id');
    }

    public function component()
    {
        return $this->belongsTo('App\Models\Material', 'component_id');
    }

    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id');
    }

    public function uom_entry()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_entry_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
