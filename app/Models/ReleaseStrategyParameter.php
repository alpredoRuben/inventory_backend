<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ReleaseStrategyParameter extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'ReleaseStrategyParameter';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'release_strategy_id', 'classification_parameter_id', 'value', 'created_by', 'updated_by', 'deleted'
    ];

    protected $appends = ['values'];

    public function getValuesAttribute()
    {
        return json_decode($this->value);
    }

    public function classification_parameter()
    {
        return $this->belongsTo('App\Models\ClassificationParameter', 'classification_parameter_id');
    }

	public function release_strategy()
    {
        return $this->belongsTo('App\Models\ReleaseStrategy', 'id', 'release_strategy_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
