<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DeliveryHeader extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'DeliveryHeader';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $fillable = [
    	'delivery_code', 'delivery_types', 'sales_org', 'planned_gi', 'delivery_date',
        'picking_date', 'customer_id', 'dlv_to_plant_id', 'dlv_to_storage_id', 'shipping_point_location_id',
        'delivery_note', 'transport_type_id', 'transport_id', 'courier_name', 'status', 'created_by', 'updated_by',
        'deleted', 'sent_by', 'sent_date', 'recived_by', 'recived_date'
    ];

    /** Delivery Type
    *0. Stock Transfer Order
    *2. Customer Delivery
    *3. Vendor Return
    **/
		
    /** Status Delivery
    *0. Open
    *1. Picking/Packing
    *2. Sent
    *3. Received
    **/

    public function item()
    {
        return $this->hasMany('App\Models\DeliveryItem', 'delivery_header_id');
    }

    public function serial_item()
    {
        return $this->hasMany('App\Models\DeliverySerial', 'delivery_header_id');
    }

    public function dlv_to_plant()
    {
        return $this->belongsTo('App\Models\Plant', 'dlv_to_plant_id');
    }

    public function dlv_to_storage()
    {
        return $this->belongsTo('App\Models\Storage', 'dlv_to_storage_id');
    }

    public function shipping_point_location()
    {
        return $this->belongsTo('App\Models\Storage', 'shipping_point_location_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Supplier', 'customer_id');
    }

    public function transport_type()
    {
        return $this->belongsTo('App\Models\TransportType', 'transport_type_id');
    }

    public function approval()
    {
        return $this->hasMany('App\Models\DeliveryApproval', 'delivery_header_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function sentBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'sent_by');
    }

    public function recivedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'recived_by');
    }
}
