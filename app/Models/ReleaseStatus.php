<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReleaseStatus extends Model
{
    protected $fillable = [
		'release_strategy_id', 'value', 'created_by', 'updated_by'
	];

	public function release_strategy()
    {
        return $this->belongsTo('App\Models\ReleaseStrategy', 'id', 'release_strategy_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
