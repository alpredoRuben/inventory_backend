<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockDeterminationFailedUpload extends Model
{
    protected $fillable = [
        'plant', 'storage', 'sequence', 'external_purchase', 'plant_supply', 'storage_supply', 'message', 'uploaded_by'
    ];
}
