<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class WoAsset extends Model
{
    use SoftDeletes;

    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'WoAsset';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }
    
	protected $guarded = [
		'id', 'deleted_at', 'created_at', 'updated_at'
	];
	
    public function asset()
    {
        return $this->hasOne('App\Models\Asset', 'id', 'asset_id');
    }
	
    public function work_order()
    {
        return $this->hasOne('App\Models\TrxWo', 'id', 'trx_wo_id');
    }
	
    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
