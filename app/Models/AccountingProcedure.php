<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountingProcedure extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AccountingProcedure';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'transaction_type_id', 'sub_type_id', 'valuation_class_id', 'gl_account_id'
    ];

    public function transaction_type()
    {
        return $this->belongsTo('App\Models\TransactionType', 'transaction_type_id');
    }

    public function sub_type()
    {
        return $this->belongsTo('App\Models\SubType', 'sub_type_id');
    }

    public function valuation_class()
    {
        return $this->belongsTo('App\Models\ValuationClass', 'valuation_class_id');
    }

    public function gl_account()
    {
        return $this->belongsTo('App\Models\Account', 'gl_account_id');
    }
}
