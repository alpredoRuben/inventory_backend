<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Material extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'Material';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'code', 'description', 'classification_material_id', 'group_material_id',
		'created_by', 'updated_by', 'deleted', 'serial_profile', 'material_type_id', 'sku_code',
        'uom_id', 'old_material', 'base_material', 'nett_weight', 'gross_weight', 'weight_unit',
        'volume', 'volume_uom'
	];

    protected $with = ['base_uom'];

    protected $casts = [
        'nett_weight'   => 'float',
        'gross_weight'  => 'float',
        'volume'        => 'float',
    ];

	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function material_type()
    {
        return $this->belongsTo('App\Models\MaterialType', 'material_type_id');
    }

    public function base_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id');
    }

    public function group_material()
    {
        return $this->belongsTo('App\Models\GroupMaterial', 'group_material_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\MaterialImage', 'material_id');
    }

    public function classification()
    {
        return $this->belongsTo('App\Models\ClassificationMaterial', 'classification_material_id');
    }

    public function material_parameters()
    {
        return $this->hasMany('App\Models\MaterialParameter', 'material_id');
    }

    public function uom_conversion()
    {
        return $this->belongsToMany('App\Models\Uom', 'material_uoms', 'material_id', 'uom_id')
                ->withPivot('base_value', 'value_conversion')
                ->withTimestamps();
    }

    public function old_mat()
    {
        return $this->belongsTo('App\Models\Material', 'old_material');
    }

    public function base_mat()
    {
        return $this->belongsTo('App\Models\Material', 'base_material');
    }

    public function weight_unit_data()
    {
        return $this->belongsTo('App\Models\Uom', 'weight_unit');
    }

    public function volume_uom_data()
    {
        return $this->belongsTo('App\Models\Uom', 'volume_uom');
    }

    public function plants()
    {
        return $this->hasMany('App\Models\MaterialPlant');
    }
}
