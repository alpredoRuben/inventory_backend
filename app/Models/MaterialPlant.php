<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialPlant extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialPlant';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'material_id', 'plant_id', 'proc_type', 'proc_group_id', 'batch_ind', 'avail_check', 'profit_center_id',
		'serial_profile', 'source_list', 'mrp_group', 'mrp_controller_id', 'planned_dlv_time', 'product_champion_id',
		'hazardous_indicator', 'stock_determination', 'quality_profile_id', 'production_storage_id', 'unit_of_purchase_id'
	];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function reservation()
    {
        return $this->hasMany('App\Models\Reservation', 'material_plant_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function production_storage()
    {
        return $this->belongsTo('App\Models\Storage', 'production_storage_id');
    }

    public function unit_of_purchase()
    {
        return $this->belongsTo('App\Models\Uom', 'unit_of_purchase_id');
    }

    public function procurement_group()
    {
        return $this->belongsTo('App\Models\ProcurementGroup', 'proc_group_id');
    }

    public function mrp()
    {
        return $this->belongsTo('App\Models\Mrp', 'mrp_controller_id');
    }

    public function profit_center()
    {
        return $this->belongsTo('App\Models\ProfitCenter', 'profit_center_id');
    }

    public function product_champion()
    {
        return $this->belongsTo('App\Models\ProductChampion', 'product_champion_id');
    }

    public function quality_profile()
    {
        return $this->belongsTo('App\Models\ClassificationMaterial', 'quality_profile_id');   
    }
}
