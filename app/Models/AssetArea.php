<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetArea extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AssetArea';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'asset_id', 'name', 'large', 'area', 'created_by', 'updated_by', 'deleted'
    ];

    public function asset()
    {
        return $this->belongsTo('App\Models\Asset', 'id', 'asset_id');
    }
}
