<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Budget extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'BUDGET';
    protected static $logOnlyDirty = true;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
    	'fiscal_year', 'material_id', 'company_id', 'project_id', 'cost_center_id', 'account_id',
        'commitment_item', 'currency', 'amount', 'status', 'responsible_id', 'created_by',
        'updated_by', 'deleted', 'release_group_id', 'release_strategy_id', 'release_indicator', 'release_state',
        'approved_by', 'approved_at', 'description'
    ];

    protected $casts = [
        'amount' => 'float'
    ];

    public function consumption()
    {
        return $this->hasMany('App\Models\BudgetConsumption', 'budget_id');
    }

    public function approval()
    {
        return $this->hasMany('App\Models\BudgetApproval', 'budget_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function release_group()
    {
        return $this->belongsTo('App\Models\ReleaseGroup', 'release_group_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function release_strategy()
    {
        return $this->belongsTo('App\Models\ReleaseStrategy', 'release_strategy_id');
    }

    public function approved_by()
    {
        return $this->belongsTo('App\Models\User', 'approved_by');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\User', 'responsible_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
