<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialImage extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialImage';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'material_id', 'image', 'created_by', 'updated_by', 'deleted'
	];

	protected $appends = [
        'image_url_full'
    ];

    public function getImageUrlFullAttribute()
    {
        if ($this->image && \Storage::disk('public')->exists($this->image)) {
            return \Storage::disk('public')->url($this->image);
        } else {
            return null;
        }
    }

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'id', 'material_id');
    }
}
