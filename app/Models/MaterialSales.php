<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialSales extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialSales';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'material_id', 'sales_organization_id', 'delivering_plant_id', 'tax_class',
		'account_assignment_id', 'item_category_id'
	];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function delivering_plant()
    {
        return $this->belongsTo('App\Models\Plant', 'delivering_plant_id');
    }

    public function sales_organization()
    {
        return $this->belongsTo('App\Models\SalesOrganization', 'sales_organization_id');
    }

    public function account_assignment()
    {
        return $this->belongsTo('App\Models\AccountAssignment', 'account_assignment_id');
    }

    public function item_category()
    {
        return $this->belongsTo('App\Models\ItemCategory', 'item_category_id');
    }
}
