<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class StockBatch extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'StockBatch';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'stock_main_id', 'batch_number', 'gr_date', 'sled_date', 'manuf_date', 'vendor_id', 'vendor_batch',
		'license', 'unit_cost', 'po_number', 'po_item_number', 'prod_doc_no', 'prod_item_no', 'created_by',
		'updated_by', 'deleted', 'qty_unrestricted', 'qty_blocked', 'qty_transit', 'qty_qa'
	];

    protected $casts = [
        'qty_unrestricted' => 'float',
        'qty_blocked' => 'float',
        'qty_transit' => 'float',
        'qty_qa' => 'float',
    ];

	public function stock_main()
    {
        return $this->belongsTo('App\Models\StockMain', 'stock_main_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'vendor_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
