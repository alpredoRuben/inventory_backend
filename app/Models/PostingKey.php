<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostingKey extends Model
{
    protected $fillable = [
    	'code', 'description', 'd_c', 'account_type', 'sales_rel', 'payment_rel', 'special_gl', 'reversal'
    ];

    /* Account Type
    * A=Assets, D=Customers, K=Vendors, M=Material, S=G/L accounts
    */
}
