<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class TrxTaskListItem extends Model
{
    use SoftDeletes;
    
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'TrxTaskListItem';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

	protected $fillable = [
		'tasklist_id', 'plant_id', 'classification_id', 'operation_no', 'description', 'estimated', 'estimated_unit', 
		'created_at', 'updated_at', 'created_by', 'updated_by', 
	];

	//estimated unit. 1 = minute, 2 = hour, 3 = day

    protected $dates = ['deleted_at'];

    protected $with = [
        'createdBy',
        'updatedBy',
    ];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function tasklist()
    {
        return $this->belongsTo('App\Models\TrxTaskList');
    }

    public function plant()
    {
        return $this->hasOne('App\Models\Plant', 'id', 'plant_id');
    }

    public function classification()
    {
        return $this->hasOne('App\Models\ClassificationMaterial', 'id', 'classification_id');
    }
}
