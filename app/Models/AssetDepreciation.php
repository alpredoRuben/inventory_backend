<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetDepreciation extends Model
{
	use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AssetDepreciation';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    /*
    depre type:
    1 = straight line
    2 = double decline
    */

    protected $fillable = [
        'asset_id', 'depre_type', 'number', 'index_dd', 'month', 'year', 'depreciation', 'total_depreciation', 'book_value', 
        'material_group_id', 'location_id', 'gl_account_id', 'cost_center_id', 'created_by', 'updated_by',
    ];

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function asset(){
        return $this->belongsTo('App\Models\Asset','asset_id','id');
    }

    public function material_group(){
        return $this->belongsTo('App\Models\GroupMaterial','material_group_id','id');
    }

    public function gl_account(){
        return $this->belongsTo('App\Models\Account','gl_account_id','id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location', 'location_id');
    }
}
