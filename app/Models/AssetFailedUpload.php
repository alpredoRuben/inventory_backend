<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetFailedUpload extends Model
{
    protected $fillable = [
        'material', 'description', 'asset_number', 'serial_number', 'superior_asset', 'old_asset', 'straight_line',
        'double_decline', 'material_group', 'vendor', 'plant', 'location', 'status', 'cost_center', 'group_owner', 
        'asset_type', 'valuation_type', 'responsible_person', 'depreciation_date', 'purchase_price', 'curency', 
        'quantity_asset', 'salvage_value', 'warranty_end', 'maintenance_cycle', 'maintenance_cycle_unit', 
        'last_maintenance', 'retired_date', 'retired_reason', 'retired_remarks', 'message', 'uploaded_by'
    ];
}
