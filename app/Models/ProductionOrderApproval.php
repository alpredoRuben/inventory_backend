<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionOrderApproval extends Model
{
    protected $fillable = [    
    	'production_order_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** Status
    * 1. Draft, masih dalam rencana produksi
	* 2. Confirmed, dokumen digunakan dalam proses produksi
	* 3. Completed, proses produksi selesai, sdh GR from Produksi
    **/

    public function production_order()
    {
        return $this->belongsTo('App\Models\ProductionOrder', 'production_order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
