<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Reservation extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'Reservation';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [    
    	'reservation_code', 'reservation_item', 'reservation_type', 'note_header', 'material_id',
        'note_item', 'plant_id', 'storage_id', 'batch', 'requirement_date', 'quantity', 'uom_id',
        'dc', 'movement_type_id', 'unit_cost', 'inout_plant_id', 'inout_storage_id', 'status',
        'approval_by', 'approval_date', 'material_plant_id', 'fulfillment_status', 'cost_center_id',
        'profit_center_id', 'order_number', 'planned_order', 'planned_order_item', 'purc_req_number',
        'purc_req_item', 'po_number', 'po_item_no', 'created_by', 'updated_by', 'approval_note',
        'material_short_text', 'document_type', 'item_type', 'currency', 'prefered_vendor_id', 'release_group',
        'release_strategy', 'approval_level', 'project_id', 'account_id', 'commitment_item_id', 'proc_type',
        'proc_group_id', 'group_material_id', 'quantity_base_unit', 'uom_base_id'
    ];

    /** Type
    *1. Replenishment
    *2. Consumption
    *3. Purchase
    */

    /** Reservation Status
    *0 = draft
    *1 = pending_approval
    *2 = approved
    *3 = rejected
    *4 = canceled
    *5 = processed
    *6 = DO created
    *7 = PR created
    *8 = PO created
    *9 = RFQ created
    *10 = Deleted
    */

    /* fullfilment status
    * 0 = Open
    * 1 = Completed
    */

    /* item type
    * 0. material for stock
    * 1. free text / material for cost center, jika tipe ini wajib mengisi cost center
    * 2. material for asset
    */

    public function delivery_orders()
    {
        return $this->hasMany('App\Models\DeliveryItem', 'reservation_id');
    }

    public function attachment()
    {
        return $this->hasMany('App\Models\ReservationAttachment', 'reservation_id');
    }

    public function group_material()
    {
        return $this->belongsTo('App\Models\GroupMaterial', 'group_material_id');
    }

    public function procurement_group()
    {
        return $this->belongsTo('App\Models\ProcurementGroup', 'proc_group_id');
    }
    
    public function commitment_item()
    {
        return $this->belongsTo('App\Models\Budget', 'commitment_item_id');
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Account', 'account_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function prefered_vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'prefered_vendor_id');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id');
    }

    public function uom_base()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_base_id');
    }

    public function movement_type()
    {
        return $this->belongsTo('App\Models\MovementType', 'movement_type_id');
    }

    public function inout_plant()
    {
        return $this->belongsTo('App\Models\Plant', 'inout_plant_id');
    }

    public function inout_storage()
    {
        return $this->belongsTo('App\Models\Storage', 'inout_storage_id');
    }

    public function approvalBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'approval_by');
    }

    public function material_plant()
    {
        return $this->belongsTo('App\Models\MaterialPlant', 'material_plant_id');
    }

    public function cost_center()
    {
        return $this->belongsTo('App\Models\CostCenter', 'cost_center_id');
    }

    public function profit_center()
    {
        return $this->belongsTo('App\Models\ProfitCenter', 'profit_center_id');
    }

    public function approval()
    {
        return $this->hasMany('App\Models\ReservationApproval', 'reservation_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
