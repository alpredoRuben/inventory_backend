<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DepreTypePeriod extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'DepreTypePeriod';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $table = 'depre_type_period';

    // public $primaryKey = 'id';

    protected $fillable = ['depre_type_name_id','period','rates','eco_lifetime','softdelete','deleted','created_by','updated_by'];

    public $timestamps = false;

    public function depre_name() {
        
        return $this->belongsTo('App\Models\DepreTypeName', 'depre_type_name_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
