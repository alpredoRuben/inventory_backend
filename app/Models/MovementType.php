<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MovementType extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MovementType';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'code', 'description', 'reason_ind', 'dc_ind', 'rev_code_id', 'batch_ind',
        'qi_ind', 'created_by', 'updated_by', 'deleted', 'batch_sort', 'sub_type_id'
	];

    public function rev_code()
    {
        return $this->belongsTo('App\Models\MovementType', 'rev_code_id');
    }

	public function reasons()
    {
        return $this->hasMany('App\Models\MovementTypeReason', 'movement_type_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }

    public function sub_type()
    {
        return $this->belongsTo('App\Models\SubType', 'sub_type_id');
    }
}
