<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class StockOpname extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'StockOpname';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'stock_opname_doc', 'item_number', 'type', 'document_date', 'material_id', 'plant_id', 'storage_id',
		'stock_batch_id', 'zero_count', 'quantity', 'base_uom_id', 'quantity_entry', 'entry_uom_id', 'status',
		'created_by', 'updated_by', 'deleted'
	];

    protected $casts = [
        'quantity' => 'float',
        'quantity_entry' => 'float',
    ];

    /** Status
    * 0 = open
    * 1 = submit
    * 2 = approved
    * 3 = rejected
    **/

    public function serial()
    {
        return $this->hasMany('App\Models\StockOpnameSerial', 'stock_opname_id');
    }

    public function approval()
    {
        // get approval where status approved/rejected
        return $this->hasOne('App\Models\StockOpnameApproval', 'stock_opname_id')->where('status_to', 2);
    }

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function plant()
    {
        return $this->belongsTo('App\Models\Plant', 'plant_id');
    }

    public function storage()
    {
        return $this->belongsTo('App\Models\Storage', 'storage_id');
    }

    public function stock_batch()
    {
        return $this->belongsTo('App\Models\StockBatch', 'stock_batch_id');
    }

    public function base_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'base_uom_id');
    }

    public function entry_uom()
    {
        return $this->belongsTo('App\Models\Uom', 'entry_uom_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
