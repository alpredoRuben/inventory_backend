<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MaterialWorkCenter extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'MaterialWorkCenter';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [		
		'material_id', 'work_center_id'
	];

	public function material()
    {
        return $this->belongsTo('App\Models\Material', 'material_id');
    }

    public function work_center()
    {
        return $this->belongsTo('App\Models\WorkCenter', 'work_center_id');
    }
}
