<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialPlantFailedUpload extends Model
{
    protected $fillable = [
        'material_code', 'plant', 'procurement_type', 'procurement_group', 'uop', 'production_storage', 'batch_indicator',
        'available_check', 'profit_center', 'source_list', 'mrp_group', 'mrp_controller', 'planned_delivery_time',
        'product_champion', 'hazardous_indicator', 'stock_determination', 'quality_profile', 'message', 'uploaded_by'
    ];
}
