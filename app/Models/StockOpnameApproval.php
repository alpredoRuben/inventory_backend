<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOpnameApproval extends Model
{
    protected $fillable = [    
    	'stock_opname_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** SO status
	* 0. Open
	* 1. Submitted
	* 2. Approved
	* 3. Rejected
	*/

    public function stock_opname()
    {
        return $this->belongsTo('App\Models\StockOpname', 'stock_opname_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
