<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialVendorHistory extends Model
{
    protected $fillable = [
        'material_vendor_id', 'price', 'currency', 'purchase_header_id', 'purchase_item_id', 'created_by'
    ];

    public function material_vendor()
    {
        return $this->belongsTo('App\Models\MaterialVendor', 'material_vendor_id');
    }

    public function purchase_header()
    {
        return $this->belongsTo('App\Models\PurchaseHeader', 'purchase_header_id');
    }

    public function purchase_item()
    {
        return $this->belongsTo('App\Models\PurchaseItem', 'purchase_item_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
