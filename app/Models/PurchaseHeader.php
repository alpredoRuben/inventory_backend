<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PurchaseHeader extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'PurchaseHeader';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [
		'po_doc_no', 'po_doc_category', 'po_doc_type', 'vendor_id', 'term_payment_id', 'purc_group_id',
	    'doc_currency', 'exchange_rate', 'purc_doc_date', 'valid_from', 'valid_to', 'deadline_date',
	    'bid_number', 'quotation_number', 'your_reference', 'incoterm_id', 'incoterm2', 'collective_number',
        'release_state', 'status', 'created_by', 'updated_by', 'notes', 'vendor_description',
        'vendor_address', 'release_strategy_id'
	];

	/** Document Category
	* B (Purchase Order)
	* F (RFQ)
	* K (Contract)
	*/

	/** PO status
	* 0. Open
	* 1. Submitted
    * 2. Approved
    * 3. Rejected
    * 4. Closed
    * 5. Printed GR
    * 6. Partial GR
	* 9. Deleted
	*/

    /** RFQ status
    * 0. Open
    * 1. Sent
    * 2. Submitted
    * 3. Awarded
    * 4. Rejected
    * 5. Closed
    * 6. Printed
    * 9. Deleted
    **/

    public function release_strategy()
    {
        return $this->belongsTo('App\Models\ReleaseStrategy', 'release_strategy_id');
    }

    public function item()
    {
        return $this->hasMany('App\Models\PurchaseItem', 'purchase_header_id')->orderBy('id', 'asc');
    }

    public function attachment()
    {
        return $this->hasMany('App\Models\PurchaseAttachment', 'purchase_header_id');
    }

    public function condition()
    {
        return $this->hasMany('App\Models\PurchaseCondition', 'purchase_header_id');
    }

    public function approval()
    {
        return $this->hasMany('App\Models\PurchaseApproval', 'purchase_header_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Supplier', 'vendor_id');
    }

    public function term_payment()
    {
        return $this->belongsTo('App\Models\TermPayment', 'term_payment_id');
    }

    public function purc_group()
    {
        return $this->belongsTo('App\Models\ProcurementGroup', 'purc_group_id');
    }

    public function incoterm()
    {
        return $this->belongsTo('App\Models\Incoterm', 'incoterm_id');
    }

	public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function updatedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'updated_by');
    }
}
