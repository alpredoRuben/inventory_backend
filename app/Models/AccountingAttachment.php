<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AccountingAttachment extends Model
{
    use LogsActivity;

    /**
     * Enable logging all changes in this model
     *
     * @var boolean
     */
    protected static $logFillable = true;
    protected static $logName = 'AccountingAttachment';
    protected static $logOnlyDirty = false;
    
    public function getDescriptionForEvent(string $eventName): string {
        return "Table \"{$this->table}\" is {$eventName}";
    }

    protected $fillable = [    
    	'accounting_header_id', 'original_name', 'name', 'created_by'
    ];

    protected $appends = [
        'url_full'
    ];

    public function getUrlFullAttribute()
    {
        if ($this->name && \Storage::disk('public')->exists($this->name)) {
            return \Storage::disk('public')->url($this->name);
        } else {
            return null;
        }
    }

    public function accounting_header()
    {
        return $this->belongsTo('App\Models\AccountingHeader', 'accounting_header_id');
    }

    public function createdBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }
}
