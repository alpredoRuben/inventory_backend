<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseApproval extends Model
{
    protected $fillable = [    
    	'purchase_header_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** PO status
	* 0. Open
	* 1. Submitted
	* 2. Approved
	* 9. Deleted
	*/

    public function purchase_header()
    {
        return $this->belongsTo('App\Models\PurchaseHeader', 'purchase_header_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
