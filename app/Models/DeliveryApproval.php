<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryApproval extends Model
{
    protected $fillable = [    
    	'delivery_header_id', 'user_id', 'status_from', 'status_to', 'notes'
    ];

    /** Status Delivery
    *0 = open
    *1 = pick/pack
    *2 = Good Issued
    **/

    public function delivery_header()
    {
        return $this->belongsTo('App\Models\Reservation', 'delivery_header_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
