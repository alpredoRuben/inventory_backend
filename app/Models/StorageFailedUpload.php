<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StorageFailedUpload extends Model
{
    protected $fillable = [
        'code', 'description', 'plant', 'location', 'responsible_user','message', 'uploaded_by'
    ];
}
