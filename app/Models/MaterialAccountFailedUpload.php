<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialAccountFailedUpload extends Model
{
    protected $fillable = [
        'material_code', 'plant', 'valuation_type', 'valuation_class', 'price_controll', 'moving_price',
        'standart_price', 'message', 'uploaded_by'
    ];
}
