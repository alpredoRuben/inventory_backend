<?php

namespace App\Exports\Master;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

class CoaTemplateExport implements FromView, WithTitle, ShouldAutoSize, WithColumnFormatting, WithEvents
{
    private $chart;

    public function __construct($chart)
    {
        $this->chart = $chart;
    }

    public function view(): View
    {
        return view('exports.master.coa2', [
            'chart' =>  $this->chart
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class  => function(AfterSheet $event) {
                $cellRange = 'A1:J1';
                $num = 2;
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle(
                    'D2:D'.$event->sheet->getDelegate()->getHighestRow()
                )->getNumberFormat()->setFormatCode('#,##0');
                $event->sheet->getDelegate()->getStyle(
                    'H2:H'.$event->sheet->getDelegate()->getHighestRow()
                )->getNumberFormat()->setFormatCode('#,##0');
                //$event->sheet->getDelegate()->getCell('H2')->setValue(str_replace(",","","2,01"));
            },
        ];
    }

    public function title(): string
	{
	    return 'Chart Of Accounts';
	}
}
