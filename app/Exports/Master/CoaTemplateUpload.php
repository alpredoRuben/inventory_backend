<?php

namespace App\Exports\Master;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CoaTemplateUpload implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'CoA Code' => 'BSIM',
                'CoA Description' => 'COA Bank Sinarmas Tbk',
                'Account Group Code' => '12.1',
                'Account Group Description' => 'Tanah',
                'Group Type' => 'Asset',
                'Account Type' => 'Assets',
                'Account Code' => '30.1',
                'Account Description' => 'Tanah Rumah',
                'Cash' => 'Yes',
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No', 'CoA Code', 'CoA Description', 'Account Group Code', 'Account Group Description', 'Group Type',
            'Account Type', 'Account Code', 'Account Description', 'Cash'
        ];
    }
}
