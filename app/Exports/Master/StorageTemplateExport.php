<?php

namespace App\Exports\Master;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StorageTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Plant' => 'SMG',
                'Code' => 'PKW',
                'Description' => 'KK Kedungwuni',
                'Location' => 'PKW',
                'Responsible User' => 'admin@Localhost.coml',
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Plant',
            'Code',
            'Description',
            'Location',
            'Responsible User',
        ];
    }
}
