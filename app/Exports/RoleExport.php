<?php

namespace App\Exports;

use App\Models\Role;
use App\Models\Modules;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class RoleExport implements FromView, WithTitle, ShouldAutoSize
{
    public function view(): View
    {
        return view('exports.role', [
            'roles' => Role::orderBy('id', 'asc')->get(),
            'modules' => Modules::orderby('object', 'asc')->get()
        ]);
    }

    public function title(): string
	{
	    return 'Role';
	}
}
