<?php

namespace App\Exports\Threepl;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StockDeterminationInvalidExport implements FromView, WithTitle, ShouldAutoSize
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.threepl.stock_determination_failed', [
            'data' => $this->data
        ]);
    }

    public function title(): string
	{
	    return 'Stock Determination';
	}
}