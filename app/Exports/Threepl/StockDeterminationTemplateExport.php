<?php

namespace App\Exports\Threepl;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class StockDeterminationTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Plant' => 'WH',
                'Storage' => 'WH',
                'Sequence' => '0',
                'External Purchase' => 'Yes',
                'Plant Supply' => 'WO',
                'Storage Supply' => 'WO'
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Plant',
            'Storage',
            'Sequence',
            'External Purchase',
            'Plant Supply',
            'Storage Supply'
        ];
    }
}