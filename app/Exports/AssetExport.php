<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AssetExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1', 
                'Material Code' => '40', 
                'Description' => '',
                'Asset Number' => '123', 
                'Serial Number' => '', 
                'Superior Asset' => '', 
                'Old Asset' => '', 
                'Straight Line' => 'Bangunan Permanen',
                'Double Declining' => 'kelompok 1', 
                'Material Group' => '', 
                'Vendor Code' => '', 
                'Plant Code' => 'WH', 
                'Location' => 'HO123', 
                // 'Status' => 'Active',  
                'Cost Center' => 'CC123',  
                'Group Owner' => 'HRD', 
                'Asset Type' => '', 
                'Valuation Type' => 'Sewa', 
                'Responsible Person' => 'admin@gmail.com',
                'Depreciation Date' => '01/01/2019', 
                'Purchase Price' => '10000000', 
                'Currency' => 'IDR', 
                'Quantity Asset' => '', 
                'Salvage Value' => '', 
                'Warranty End' => '', 
                'Maintenance Cycle' => '', 
                'Maintenance Cycle Unit' => '', 
                'Last Maintenance' => '', 
                // 'Retired Date' => '', 
                // 'Retired Reason' => '', 
                // 'Retired Remarks' => '', 
                // 'C_merek' => '', 
                // 'C_spec' => ''
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No', 'Material Code', 'Description', 'Asset Number', 'Serial Number', 'Superior Asset', 'Old Asset', 
            'Straight Line', 'Double Declining', 'Material Group', 'Vendor Code', 'Plant Code', 'Location', 
            'Cost Center', 'Group Owner', 'Asset Type', 'Valuation Type', 'Responsible Person', 
            'Depreciation Date', 'Purchase Price', 'Currency', 'Quantity Asset', 'Salvage Value', 'Warranty End', 
            'Maintenance Cycle', 'Maintenance Cycle Unit', 'Last Maintenance'
        ];
    }
}
