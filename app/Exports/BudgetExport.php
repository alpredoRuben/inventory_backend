<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BudgetExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'Fiscal Year' => '2019',
                'Description' => 'Budget 2019',
                'Project Code' => 'Project 1',
                'Cost Center' => 'Cost Center 1',
                'GL Account' => 'Account 1',
                'Material' => 'Material 1',
                'Amount' => '3000000',
                'Currency' => 'Rp'
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
    		'Fiscal Year',
    		'Description',
    		'Project Code',
    		'Cost Center',
    		'GL Account',
    		'Material',
    		'Amount',
    		'Currency'
        ];
    }
}
