<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

// class ProductionScheduleExport implements FromCollection , WithHeadings, ShouldAutoSize
class ProductionScheduleExport implements FromCollection, ShouldAutoSize
{
	use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
            ],
            [
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
            ],
            [
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
            ],
            [
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
            ],
            [
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
                '' => '',
            ],
            [
                'No' => 'No',
                'Year' => 'Year',
                'Month' => 'Month',
                'Plant' => 'Plant',
	            'Work Center' => 'Work Center',
	            'Material' => 'Material',
	            'Description' => 'Description',
	            'Base Uom' => 'Base Uom',
	            '1' => '1',
	            '2' => '2',
	            '3' => '3',
	            '4' => '4',
	            '5' => '5',
	            '6' => '6',
	            '7' => '7',
	            '8' => '8',
	            '9' => '9',
	            '10' => '10',
	            '11' => '11',
	            '12' => '12',
	            '13' => '13',
	            '14' => '14',
	            '15' => '15',
	            '16' => '16',
	            '17' => '17',
	            '18' => '18',
	            '19' => '19',
	            '20' => '20',
	            '21' => '21',
	            '22' => '22',
	            '23' => '23',
	            '24' => '24',
	            '25' => '25',
	            '26' => '26',
	            '27' => '27',
	            '28' => '28',
	            '29' => '29',
	            '30' => '30',
	            '31' => '31'
            ],
            [
                'No' => '1',
                'Year' => '2019',
                'Month' => '6',
                'Plant' => 'PJKT',
                'Work Center' => 'WJKT',
                'Material' => 'L0001',
                'Description' => 'Californian Sushi',
                'Base Uom' => 'PC',
                '1' => '1',
                '2' => '',
                '3' => '2',
                '4' => '',
                '5' => '',
                '6' => '',
                '7' => '2',
                '8' => '',
                '9' => '',
                '10' => '',
                '11' => '',
                '12' => '',
                '13' => '',
                '14' => '',
                '15' => '1',
                '16' => '',
                '17' => '',
                '18' => '',
                '19' => '',
                '20' => '',
                '21' => '',
                '22' => '',
                '23' => '',
                '24' => '',
                '25' => '',
                '26' => '',
                '27' => '',
                '28' => '',
                '29' => '',
                '30' => '',
                '31' => ''
            ],
            [
                'No' => '2',
                'Year' => '2019',
                'Month' => '6',
                'Plant' => 'PLT4',
                'Work Center' => 'WC14',
                'Material' => '1002',
                'Description' => 'Vegetarian Sushi',
                'Base Uom' => 'PC',
                '1' => '1',
                '2' => '',
                '3' => '2',
                '4' => '',
                '5' => '',
                '6' => '',
                '7' => '2',
                '8' => '',
                '9' => '',
                '10' => '',
                '11' => '',
                '12' => '',
                '13' => '',
                '14' => '',
                '15' => '1',
                '16' => '',
                '17' => '',
                '18' => '',
                '19' => '',
                '20' => '',
                '21' => '',
                '22' => '',
                '23' => '',
                '24' => '',
                '25' => '',
                '26' => '',
                '27' => '',
                '28' => '',
                '29' => '',
                '30' => '',
                '31' => ''
            ]
        ]);
    }

    // public function headings(): array
    // {
    // 	return [
    //         'No',
    //         'Year',
    //         'Month',
    //         'Plant',
    //         'Work Center',
    //         'Material',
    //         'Description',
    //         'Base Uom',
    //         '1',
    //         '2',
    //         '3',
    //         '4',
    //         '5',
    //         '6',
    //         '7',
    //         '8',
    //         '9',
    //         '10',
    //         '11',
    //         '12',
    //         '13',
    //         '14',
    //         '15',
    //         '16',
    //         '17',
    //         '18',
    //         '19',
    //         '20',
    //         '21',
    //         '22',
    //         '23',
    //         '24',
    //         '25',
    //         '26',
    //         '27',
    //         '28',
    //         '29',
    //         '30',
    //         '31'
    //     ];
    // }
}
