<?php

namespace App\Exports;

use App\Models\MovementHistory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MonthlyConsumptionExport implements FromView, WithTitle, ShouldAutoSize
{
    private $map_data;
    private $filter;

    public function __construct($map_data, $filter)
    {
        $this->map_data = $map_data;
        $this->filter = $filter;
    }

    public function view(): View
    {
        return view('exports.report_monthly_consumption', [
            'map_data' => $this->map_data,
            'filter' => $this->filter
        ]);
    }

    public function title(): string
	{
	    return 'Monthly Consumption';
	}
}
