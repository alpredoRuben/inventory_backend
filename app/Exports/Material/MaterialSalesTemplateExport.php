<?php

namespace App\Exports\Material;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialSalesTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Material Code' => 'L121',
                'Sales Organization' => 'SO11',
                'Delivering Plant' => 'WH',
                'Tax Class' => 'YES',
                'Account Assignment' => 'A1',
                'Item Category' => 'IC1'
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Material Code',
            'Sales Organization',
            'Delivering Plant',
            'Tax Class',
            'Account Assignment',
            'Item Category'
        ];
    }
}