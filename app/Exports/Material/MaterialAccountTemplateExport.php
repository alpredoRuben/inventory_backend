<?php

namespace App\Exports\Material;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialAccountTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Material Code' => 'L121',
                'Plant' => 'WH',
                'Valuation Type' => 'VT1',
                'Valuation Class' => 'VC1',
                'Price Controll' => 'MAP',
                'Moving Price' => '1000000',
                'Standart Price' => ''
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Material Code',
            'Plant',
            'Valuation Type',
            'Valuation Class',
            'Price Controll',
            'Moving Price',
            'Standart Price'
        ];
    }
}