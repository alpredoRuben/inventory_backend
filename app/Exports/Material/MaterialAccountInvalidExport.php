<?php

namespace App\Exports\Material;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialAccountInvalidExport implements FromView, WithTitle, ShouldAutoSize
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.material.account_failed', [
            'data' => $this->data
        ]);
    }

    public function title(): string
	{
	    return 'Material Account';
	}
}