<?php

namespace App\Exports\Material;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialPlantTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Material Code' => 'L121',
                'Plant' => 'WH',
                'Procurement Type' => 'Internal',
                'Procurement Group' => 'GA',
                'UoP' => 'PCS',
                'Production Storage' => 'WH',
                'Batch Indicator' => 'Yes',
                'Available Check' => 'No',
                'Profit Center' => 'PC1',
                'Source List' => 'No',
                'MRP Group' => 'stock',
                'MRP Controller' => 'MRP1',
                'Planned Delivery Time' => '',
                'Product Champion' => '',
                'Hazardous Indicator' => 'No',
                'Stock Determination' => '',
                'Quality Profile' => '',
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Material Code',
            'Plant',
            'Procurement Type',
            'Procurement Group',
            'UoP',
            'Production Storage',
            'Batch Indicator',
            'Available Check',
            'Profit Center',
            'Source List',
            'MRP Group',
            'MRP Controller',
            'Planned Delivery Time',
            'Product Champion',
            'Hazardous Indicator',
            'Stock Determination',
            'Quality Profile',
        ];
    }
}