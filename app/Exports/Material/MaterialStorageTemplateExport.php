<?php

namespace App\Exports\Material;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialStorageTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Material Code' => 'L121',
                'Plant' => 'WH',
                'Storage' => 'WH',
                'Minimum Stock' => '1',
                'Maximum Stock' => '100',
                'Storage Bin' => 'Rak A',
                'Storage Condition' => '',
                'Storage Type' => '',
                'Temp Condition' => '',
                'LE Quantity' => '',
                'LE Unit' => ''
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
            'Material Code',
            'Plant',
            'Storage',
            'Minimum Stock',
            'Maximum Stock',
            'Storage Bin',
            'Storage Condition',
            'Storage Type',
            'Temp Condition',
            'LE Quantity',
            'LE Unit'
        ];
    }
}