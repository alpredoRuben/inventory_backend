<?php

namespace App\Exports\Material;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MaterialBasicTemplateExport implements FromCollection , WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'No' => '1',
                'Material Type' => 'OP',
                'Code' => '40',
                'Description' => 'Notebook',
                'Group Material' => '12135',
                'Classification' => 'PROCUREMENT',
                'Old Material' => '',
                'Base Material' => '',
                'SKU Code' => '',
                'UoM' => 'PCS',
                'Nett Weight Value' => '',
                'Nett Weight UoM' => '',
                'Gross Weight Value' => '',
                'Volume Value' => '',
                'Volume UoM' => '',
                'Serial Profile' => 'Yes',
            ]
        ]);
    }

    public function headings(): array
    {
    	return [
            'No',
    		'Material Type',
            'Code',
            'Description',
            'Group Material',
            'Classification',
            'Old Material',
            'Base Material',
            'SKU Code',
            'UoM',
            'Nett Weight Value',
            'Nett Weight UoM',
            'Gross Weight Value',
            'Volume Value',
            'Volume UoM',
            'Serial Profile'
        ];
    }
}
