<?php 

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class ExportFromArray implements FromArray {
    protected $exports;

    public function __construct(array $exports) {
        $this->exports = $exports;
    }

    public function array(): array {
        return $this->exports;
    }
}

?>