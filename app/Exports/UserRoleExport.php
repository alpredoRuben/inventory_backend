<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Role;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UserRoleExport implements FromView, WithTitle, ShouldAutoSize
{
    public function view(): View
    {
        return view('exports.user', [
            'users' => User::orderBy('username', 'asc')->get(),
            'roles' => Role::orderBy('id', 'asc')->get(),
        ]);
    }

    public function title(): string
	{
	    return 'User Role';
	}
}
