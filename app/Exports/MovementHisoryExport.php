<?php

namespace App\Exports;

use App\Models\MovementHistory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MovementHisoryExport implements FromView, WithTitle, ShouldAutoSize
{
    private $history;

    public function __construct($history)
    {
        $this->history = $history;
    }

    public function view(): View
    {
        return view('exports.report_history_movement', [
            'history' => $this->history
        ]);
    }

    public function title(): string
	{
	    return 'Hstory Movement';
	}
}
