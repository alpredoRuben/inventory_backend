<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SuspendInactiveUserJob;

class SuspendInactiveUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'suspend:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Suspend Inactive User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SuspendInactiveUserJob::dispatch();
    }
}
