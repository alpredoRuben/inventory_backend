<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\AssetDepreciationJob;

class AssetDepreciationMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'depreciation:asset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly Asset Depreciation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AssetDepreciationJob::dispatch();
    }
}
