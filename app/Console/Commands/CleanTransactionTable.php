<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Reservation;
use App\Models\ReservationApproval;
use App\Models\ReservationAttachment;

use App\Models\DeliveryHeader;
use App\Models\DeliveryItem;
use App\Models\DeliverySerial;
use App\Models\DeliveryApproval;

use App\Models\MovementHistory;
use App\Models\Serial;
use App\Models\SerialHistory;
use App\Models\MovingPriceHistory;

use App\Models\PurchaseHeader;
use App\Models\PurchaseItem;
use App\Models\PurchaseApproval;
use App\Models\PurchaseCondition;
use App\Models\PurchaseAttachment;

use App\Models\StockBatch;
use App\Models\StockMain;
use App\Models\StockSerial;

use App\Models\TransactionCode;

use App\Models\StockOpname;
use App\Models\StockOpnameApproval;
use App\Models\StockOpnameSerial;

class CleanTransactionTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:transactiontable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Transaction Table Inventory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // clear reservation table
        Reservation::whereNotNull('id')->delete();
        ReservationApproval::whereNotNull('id')->delete();
        ReservationAttachment::whereNotNull('id')->delete();

        // clear do table
        DeliveryHeader::whereNotNull('id')->delete();
        DeliveryItem::whereNotNull('id')->delete();
        DeliverySerial::whereNotNull('id')->delete();
        DeliveryApproval::whereNotNull('id')->delete();

        // clear movement history table
        MovementHistory::whereNotNull('id')->delete();
        Serial::whereNotNull('id')->delete();
        SerialHistory::whereNotNull('id')->delete();
        MovingPriceHistory::whereNotNull('id')->delete();

        // clear po table
        PurchaseHeader::whereNotNull('id')->delete();
        PurchaseItem::whereNotNull('id')->delete();
        PurchaseApproval::whereNotNull('id')->delete();
        PurchaseCondition::whereNotNull('id')->delete();
        PurchaseAttachment::whereNotNull('id')->delete();

        // clear stock table
        StockMain::whereNotNull('id')->delete();
        StockBatch::whereNotNull('id')->delete();
        StockSerial::whereNotNull('id')->delete();

        // clear stock opname
        StockOpname::whereNotNull('id')->delete();
        StockOpnameApproval::whereNotNull('id')->delete();
        StockOpnameSerial::whereNotNull('id')->delete();

        // update transaction code
        TransactionCode::whereNotNull('id')->update([
            'lastcode' => null
        ]);

        $this->info('Success clean transaction table');
    }
}
