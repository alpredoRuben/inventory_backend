<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CleanTransactionTable::class,
        Commands\AutoCancelReservations::class,
        Commands\SuspendInactiveUsers::class,
        Commands\AssetDepreciationMonthly::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cancel:reservation')->dailyAt('00:00');
        $schedule->command('suspend:user')->dailyAt('00:00');
        $date = appsetting('ASSET_DEPRECIATION_DATE') ? (int)appsetting('ASSET_DEPRECIATION_DATE') : 1;
        $schedule->command('depreciation:asset')->monthlyOn($date, '01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
