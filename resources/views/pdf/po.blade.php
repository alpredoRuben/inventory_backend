<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Purchase Orders</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 2em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 10px;
		}

		header {
			position: fixed; left: 0px; top: 0px; right: 0px;
			width:100%;
			display: table;
			margin-left: 9px;
		}

		#id-header {
			display: table-cell;
			width:40%;
		}

		#id-header1 {
			display: table-cell;
			width:41%;
		}

		#id-header img {
			height: 40px;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}

		#empty-space {
			display: table-cell;
			width:20%;
		}

		#empty-space1 {
			display: table-cell;
			width:19%;
		}

		h2.nametitle {
			font-size: 14px;
			text-decoration: underline;
			margin: 0;
		}

		table#t01 {
			position: relative; left: 0px; top: 5px; right: 0px;
			width: 100%;
			border: none;
			font-size: 10px;
		}

		table#t01 th,
		table#t01 td {
			padding: 0px 0px;
		}

		#header-sub {
			position: relative; left: 0px; top: 130px; right: 0px;
			width:100%;
			display: table;
		}

		h2.name1 {
			font-size: 1em;
			font-weight: normal;
			margin-bottom: 40px;
		}

		h2.to {
			font-size: 10px;
			margin: 0;
		}
		h2.note {
			font-size: 1em;
			font-weight: normal;
			position: relative; left: 0px; top: 120px; right: 0px;
			text-align: left;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			/* border-color: #f5f5f5; */
			position: relative; left: 0px; top: 120px; right: 0px;
			/* border-spacing: 1px; */
			border: 1px solid #ddd;
			font-size: 10px;
			margin-bottom: 120px;
		}

		table th,
		table td {
			padding: 1px 5px;
		}

		table th {
			white-space: nowrap;        
			font-weight: normal;
			text-align: center;
			padding: 5px;
		}

		table td {
			text-align: left;
			/* border: 1px solid #ddd; */
		}

		table .no {
			color: #FFFFFF;
			background: #57B223;
		}

		table .desc {
			text-align: left;
		}

		table .desc-note {
			text-align: left;
			font-size: 8px;
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			/*background: #57B223;*/
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {
			/*font-size: 1.2em;*/
		}

		table tbody tr:last-child td {
			/*border: none;*/
		}

		table tfoot td {
			/*padding: 10px 20px;*/
			/*background: #FFFFFF;*/
			/*border-bottom: none;*/
			/*font-size: 1.2em;*/
			white-space: nowrap;
			/*border-top: 1px solid #AAAAAA;*/
		}

		table tfoot tr:first-child td {
			/*border-top: none;*/
		}

		table tfoot tr td:first-child {
			border: none;
		}

		footer {			
			position: absolute; left: 0px; top: 935px; right: 0px;
			width:100%;
			display: table;
		}

		table#table-footer {
			position: absolute; left: 0px; top: 850px; right: 0px;
			border: none;
		}

		#footer-left {
			margin-top: 8px;
			display: table-cell;
			width:70%;
			text-align: left;
		}

		#footer-right {
			margin-top: 8px;
			display: table-cell;
			width:30%;
			text-align: center;
		}

		#qr img {
			height: 60px;
		}
		
	</style>
</head>
<body>
	<header>
		<div id="id-header">
			<img src="{{ \Storage::disk('public')->url(appsetting('COMPANY_LOGO')) }}">
			<h2 class="name">{{ appsetting('COMPANY_NAME') }}</h2>
				<div>{{ appsetting('COMPANY_ADDRESS') }}</div>
				<div>Telp: {{ appsetting('COMPANY_PHONE') }}</div>
				<div>Fax: {{ appsetting('COMPANY_FAX') }}</div>
		</div>
		<div id="empty-space">
		</div>
		<div id="id-header">
			<h2 class="nametitle"><ins>PURCHASE ORDER</ins></h2>
			<table id="t01">
				<tr>
					<td>No. Document</td>
					<td>:</td>
					<td>{{$po->po_doc_no}}</td>
					<td rowspan="4"><div id="qr"><img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(120)->generate($po->po_doc_no)) }}"></div></td>
				</tr>
				<tr>
					<td>Tanggal</td>
					<td>:</td>
					<td>{{ Carbon\Carbon::parse($po->purc_doc_date)->format('d-M-Y') }}</td>
				</tr>
				<tr>
					<td>No. Penawaran</td>
					<td>:</td>
					<td>{{$po->your_reference}}</td>
				</tr>
				@if($po->term_payment)
					<tr>
						<td>Payment Term</td>
						<td>:</td>
						<td>{{ $po->term_payment->description }}</td>
					</tr>
				@else
					<tr>
						<td>Payment Term</td>
						<td>:</td>
						<td></td>
					</tr>
				@endif

				@if($po->incoterm)
					<tr>
						<td>Incoterm</td>
						<td>:</td>
						<td>{{ $po->incoterm->description }} / {{ $po->incoterm2 }}</td>
					</tr>
				@else
					<tr>
						<td>Incoterm</td>
						<td>:</td>
						<td> / {{ $po->incoterm2 }}</td>
					</tr>
				@endif
				<tr>
					<td>Dicetak pada</td>
					<td>:</td>
					<td colspan="2">{{ Carbon\Carbon::now()->format('d-M-Y H:i:s') }}</td>
				</tr>
			</table>
		</div>
	</header>
	<main>
		<div id="header-sub">
			<div id="id-header">
				<h2 class="to">Kepada :</h2>
				@if($po->vendor_description)
					<h2 class="name">{{$po->vendor_description}}</h2>
					<div class="name">{{$po->vendor_address}}</div>
				@else
					<h2 class="name">{{$po->vendor->description}}</h2>
					<div class="name">{{$po->vendor->location->address}}</div>
				@endif
				<div class="name">Telp :</div>
				<div class="name">Fax :</div>
				<div class="name">Email :</div>
				<div class="name">Up :</div>				
			</div>
			<div id="empty-space1">
				
			</div>
			<div id="id-header1">
				<h2 class="to">Alamat Pengiriman Invoice :</h2>
				@php
					$storage = App\Models\Storage::find($po->item[0]['storage_id']);
					$responsible = App\Models\User::find($storage->user_id);
				@endphp
				<h2 class="name">{{ $storage->code }} - {{ $storage->description }}</h2>
				<div class="name">{{ $storage->location->address }}</div>
				<div class="name">Telp : {{ $storage->location->phone }}</div>	
				<div class="name">Fax : </div>
				<div class="name">Up : {{ $responsible->name }}</div>
			</div>
		</div>

		<table frame="box" cellspacing="0" cellpadding="0">
			<thead>
				<tr style="background-color:#d9d9d9;">
					<th width="3%">No</th>
					<th width="20%">Kode Barang</th>
					<th width="32%">Deskripsi</th>
					<th width="15%">Jumlah</th>
					<th width="15%">Harga/Unit</th>
					<th width="15%">Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($po->item as $item)
					<tr>
						<td class="number">{{ $loop->iteration }}</td>
						@if($item->material_id)
							<td class="desc">{{ $item->material->code }}</td>
							<td class="desc">{{ $item->material->description }}</td>
						@else
							<td class="desc"></td>
							<td class="desc">{{ $item->short_text }}</td>
						@endif
						<td class="number">{{ number_format($item->order_qty, $item->order_unit->decimal,',', '.') }} {{ $item->order_unit->code }}</td>
						<td class="number">{{ number_format($item->price_unit,  2, ',', '.') }}</td>
						@php
							$price_total = $item->order_qty * $item->price_unit				
						@endphp				
						<td class="number">{{ number_format($price_total,  2, ',', '.') }}</td>
					</tr>

					@if($item->long_text)
					<tr>
						<td class="number"></td>
						<td class="desc-note" colspan="2">{!! $item->long_text !!}</td>
						<td class="number" colspan="3"></td>
					</tr>
					@endif
				@endforeach
			</tbody>
		</table>
	</main>
	<footer>
		<table id="table-footer" frame="box" cellspacing="0" cellpadding="0">
			<tbody>
				<tr style="background-color:#f5f5f5;">
						<td colspan="4" width="70%"></td>
						<td class="desc" width="15%">Sub Total</td>
						<td class="number" width="15%">{{ number_format($po->subtotal,  2, ',', '.') }}</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="4"></td>
						<td class="desc">PPN</td>
						<td class="number">{{ number_format($po->tax,  2, ',', '.') }}</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="4"></td>
						<td class="desc">Discount</td>
						<td class="number">-{{ number_format($po->diskon,  2, ',', '.') }}</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="4"></td>
						<td class="desc">Ongkos Kirim</td>
						<td class="number">{{ number_format($po->ongkir,  2, ',', '.') }}</td>
					</tr>
					<tr style="background-color:#f5f5f5;">
						<td colspan="4"></td>
						<td class="desc">Grand Total</td>
						<td class="number">{{ number_format($po->ammount,  2, ',', '.') }}</td>
					</tr>
			</tbody>
		</table>
		<div id="footer-left">
			{{--<h2 class="name">Note :</h2>
			<h2 class="name">{!! $po->notes !!}</h2>--}}
		</div>
		<div id="footer-right">
			<h2 class="name1">Hormat kami,</h2>
			<h2 class="name">Procurement Dept. Head</h2>	
		</div>
	</footer>
	<div style="page-break-before: always;"></div>
	<div style="position: relative; left: 0px; top: 110px; right: 0px; width:100%;">
		<div>
			<p>Note :</p>
			{!! $po->notes !!}
		</div>
	</div>
</body>
</html>