<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Asset</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        /* margin-top: 15em;
	        margin-bottom: 5em; */
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: #000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 14px;
		}

		header {
			border-bottom: 1px solid #AAAAAA;
            margin-right: -20px;
			position: fixed; left: 0px; top: -210px; right: 0px; height: 80px;
			width:100%;
			display: table;
		}

		#qr {
			float: left;
			margin-top: 8px;
			display: table-cell;
			width:30%;
		}

		#qr img {
			height: 70px;
		}

		#title {
			display: table-cell;
			text-align: center;
			width:40%;
		}

		#company {
			text-align: right;
			display: table-cell;
			width:30%;
		}

		#company img {
			height: 40px;
		}

		#details {
			margin-bottom: 50px;
			display: table;
			width: 100%;
			margin-top: -100px;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #0087C3;
			float: left;
			display: table-cell;
			width: 50%;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}

		h2.nametitle {
			font-size: 18px;
			font-weight: normal;
			margin: 0;
		}

		#invoice {
			text-align: right;
			display: table-cell;
			width: 50%;
		}

		#invoice h1 {
			color: #0087C3;
			font-size: 1em;
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1em;
			color: #777777;
		}

		#thanks{
			font-size: 2em;
			margin-bottom: 50px;
		}

		#notices{
			padding-left: 6px;
			border-left: 6px solid #0087C3;
		}

		#notices .notice {
			font-size: 1.2em;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 60px;
			position: absolute;
			bottom: 30;
			padding: 8px 0;
			text-align: center;
		}

		.center{
			
			text-align: center;
			width: 100%;
		}
	</style>
</head>
<body>
	<header>
	</header>
	<main>
		<div>
			@foreach($assets as $item)
				<div>
					<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(200)->generate($item->code)) }}"><br>
					<div>{{$item->code}}</div>
				</div>
			@endforeach
		</div>
	</main>
	<footer></footer>
</body>
</html>