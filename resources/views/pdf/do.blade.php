<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Delivery Order (STO)</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 2em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 10px;
		}

		header {
			position: fixed; left: 0px; top: 0px; right: 0px;
			width:100%;
			display: table;
			margin-left: 9px;
		}

		#id-header {
			display: table-cell;
			width:40%;
		}

		#id-header1 {
			display: table-cell;
			width:41%;
		}

		#id-header img {
			height: 40px;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}

		#empty-space {
			display: table-cell;
			width:20%;
		}

		#empty-space1 {
			display: table-cell;
			width:19%;
		}

		h2.nametitle {
			font-size: 14px;
			text-decoration: underline;
			margin: 0;
		}

		table#t01 {
			position: relative; left: 0px; top: 5px; right: 0px;
			width: 100%;
			border: none;
			font-size: 10px;
		}

		table#t01 th,
		table#t01 td {
			padding: 0px 0px;
		}

		#header-sub {
			position: relative; left: 0px; top: 110px; right: 0px;
			width:100%;
			display: table;
		}

		h2.name1 {
			font-size: 1em;
			font-weight: normal;
			margin-bottom: 40px;
		}

		h2.to {
			font-size: 10px;
			margin: 0;
		}
		h2.note {
			font-size: 1em;
			font-weight: normal;
			position: relative; left: 0px; top: 120px; right: 0px;
			text-align: left;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			/* border-color: #f5f5f5; */
			position: relative; left: 0px; top: 120px; right: 0px;
			/* border-spacing: 1px; */
			border: 1px solid #ddd;
			font-size: 10px;
			margin-bottom: 120px;
		}

		table th,
		table td {
			padding: 1px 5px;
		}

		table th {
			white-space: nowrap;        
			font-weight: normal;
			text-align: center;
			padding: 5px;
		}

		table td {
			text-align: left;
			/* border: 1px solid #ddd; */
		}

		table .no {
			color: #FFFFFF;
			background: #57B223;
		}

		table .desc {
			text-align: left;
		}

		table .desc-note {
			text-align: left;
			font-size: 8px;
		}

		table .desc-center {
			text-align: center;
		}

		table .desc-total {
			vertical-align: "top";
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			/*background: #57B223;*/
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {
			/*font-size: 1.2em;*/
		}

		table tbody tr:last-child td {
			/*border: none;*/
		}

		table tfoot td {
			/*padding: 10px 20px;*/
			/*background: #FFFFFF;*/
			/*border-bottom: none;*/
			/*font-size: 1.2em;*/
			white-space: nowrap;
			/*border-top: 1px solid #AAAAAA;*/
		}

		table tfoot tr:first-child td {
			/*border-top: none;*/
		}

		table tfoot tr td:first-child {
			border: none;
		}

		footer {			
			position: absolute; left: 0px; top: 935px; right: 0px;
			width:100%;
			display: table;
		}

		#footer-left {
			margin-top: 8px;
			display: table-cell;
			width:70%;
			text-align: left;
		}

		#footer-right {
			margin-top: 8px;
			display: table-cell;
			width:30%;
			text-align: center;
		}

		#qr img {
			height: 60px;
		}
		
	</style>
</head>
<body>
	<header>
		<div id="id-header">
			<img src="{{ \Storage::disk('public')->url(appsetting('COMPANY_LOGO')) }}">
			<h2 class="name">{{ appsetting('COMPANY_NAME') }}</h2>
				<div>{{ appsetting('COMPANY_ADDRESS') }}</div>
				<div>Telp: {{ appsetting('COMPANY_PHONE') }}</div>
				<div>Fax: {{ appsetting('COMPANY_FAX') }}</div>
		</div>
		<div id="empty-space">
		</div>
		<div id="id-header">
			<h2 class="nametitle"><ins>DELIVERY ORDER (STO)</ins></h2>
			<table id="t01">
				<tr>
					<td>No. Document</td>
					<td>:</td>
					<td>{{ $do->delivery_code }}</td>
					<td rowspan="4"><div id="qr"><img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(120)->generate($do->delivery_code)) }}"></div></td>
				</tr>
				<tr>
					<td>Tanggal</td>
					<td>:</td>
					<td>{{ Carbon\Carbon::parse($do->delivery_date)->format('d-M-Y') }}</td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td>Nama Kurir</td>
					<td>:</td>
					<td>{{ $do->sentBy->firstname }} {{ $do->sentBy->lastname }}</td>
				</tr>
				<tr>
					<td>Dicetak pada</td>
					<td>:</td>
					<td>{{ Carbon\Carbon::now()->format('d-M-Y H:i:s') }}</td>
				</tr>
			</table>
		</div>
	</header>
	<main>
		<div id="header-sub">
			<div id="id-header">
				@php
					$sender = $do->item->first();
				@endphp

				<h2 class="to">Pengirim :</h2>
				<h2 class="name">({{ $sender->plant->code }}) {{ $sender->plant->description }} / ({{ $sender->storage->code }}) {{ $sender->storage->description }}</h2>
				<div class="name">{{ $sender->storage->location->address }}</div>
				<div class="name">Telp :</div>
				<div class="name">Fax :</div>		
				<div class="name">Pengirim : {{ $do->sentBy->firstname }} {{ $do->sentBy->lastname }} ({{$do->sentBy->email}})</div>
			</div>
			<div id="empty-space1">
				
			</div>
			<div id="id-header1">
				<h2 class="to">Alamat Pengiriman Barang :</h2>
				<h2 class="name">({{ $do->dlv_to_plant->code }}) {{ $do->dlv_to_plant->description }} / ({{ $do->dlv_to_storage->code }}) {{ $do->dlv_to_storage->description }}</h2>
				<div class="name">{{ $do->dlv_to_storage->location->address }}</div>
				<div class="name">Telp : </div>
				<div class="name">Fax : </div>	
				<div class="name">Ditujukan : {{ $do->dlv_to_storage->user->firstname }} {{ $do->dlv_to_storage->user->lastname }} ({{$do->dlv_to_storage->user->email}})</div>
			</div>
		</div>

		<table frame="box" cellspacing="0" cellpadding="0">
			<thead>
				<tr style="background-color:#d9d9d9;">
					<th style="width: 20px">No</th>
					<th style="width: 100px">Material Code</th>
					<th style="width: 220px">Description</th>
					<th style="width: 30px">Quantity</th>
					<th style="width: 30px">UOM</th>
					<th style="width: 100px">Batch No</th>
					<th style="width: 100px">Expiry Date</th>
				</tr>
			</thead>
			<tbody>
				@php
					$total = 0
				@endphp
				@foreach($do->item as $item)
					<tr>
						<td class="number">{{ $loop->iteration }}</td>
						<td class="desc">{{ $item->material->code }}</td>
						<td class="desc">{{ $item->material->description }}</td>
						<td class="desc">
							{{ number_format($item->pick_qty, $item->material->base_uom->decimal,',', '.') }}
						</td>
						<td class="desc">
							{{ $item->material->base_uom->code }}
						</td>
						@if($item->stock_batch)
							<td class="desc-center">{{ $item->stock_batch->batch_number }} 
							@if($item->stock_batch->vendor_batch)
								({{ $item->stock_batch->vendor_batch }})
							@endif
							</td>
							<td class="desc-center">{{ Carbon\Carbon::parse($item->stock_batch->sled_date)->format('d-M-Y') }}</td>
						@else
							<td class="desc-center"></td>
							<td class="desc-center"></td>
						@endif
					</tr>

					@if($item->serials)
						@if(count($item->serials) > 0)
							<tr>
								<td class="number"></td>
								<td class="desc-note" colspan="6">
									Serial:
								</td>
							</tr>
							<tr>
								<td class="number"></td>
								<td class="desc-note" colspan="6">
									@php
										$i = 1;
									@endphp
									@foreach($item->serials as $serial)
										{{ $i }}. {{$serial->serial}}
										<span style="display:inline-block; width: 50px;"></span>
										@if($i%5==0)
											<br/>
										@endif
										@php
											$i++;
										@endphp
									@endforeach	
								</td>
							</tr>
							<tr>
								<td colspan="7"><div style="color: white">a</div></td>
							</tr>
						@endif
					@endif

				@endforeach
				
				<tr style="background-color:#f5f5f5;">
					<td colspan="5" width="70%"></td>
					<td class="desc-total" width="15%">Total Barang</td>
					<td class="number" width="15%">
					@foreach($do->total as $total_uom)
						{{ $total_uom }}
					@endforeach
					</td>
				</tr>
				
				<!-- <tr style="background-color:#f5f5f5;">
					<td colspan="4"></td>
					<td class="desc">Sub Total</td>
					<td class="number">{{ number_format($total,  2, ',', '.') }} {{ $item->currency }}</td>
				</tr>
				<tr style="background-color:#f5f5f5;">
					<td colspan="4"></td>
					<td class="desc">PPN</td>
					<td class="number"></td>
				</tr>
				<tr style="background-color:#f5f5f5;">
					<td colspan="4"></td>
					<td class="desc">Grand Total</td>
					<td class="number">{{ number_format($total,  2, ',', '.') }} {{ $item->currency }}</td>
				</tr> -->
			</tbody>
		</table>
	</main>
	<footer>
		<div id="footer-left">
			{{-- <h2 class="name">Note :</h2> --}}
			{{-- <h2 class="name">- {!! $po->notes !!}</h2> --}}
		</div>
		<div id="footer-right">
			<h2 class="name1">(Hormat kami)</h2>
			<h2 class="name">(.................................)</h2>	
		</div>
	</footer>
</body>
</html>