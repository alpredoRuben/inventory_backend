<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Picking</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 15em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 14px;
		}

		header {
			border-bottom: 1px solid #AAAAAA;
            margin-right: -20px;
			position: fixed; left: 0px; top: -210px; right: 0px; height: 80px;
			width:100%;
			display: table;
		}

		#qr {
			float: left;
			margin-top: 8px;
			display: table-cell;
			width:30%;
		}

		#qr img {
			height: 70px;
		}

		#title {
			display: table-cell;
			text-align: center;
			width:40%;
		}

		#company {
			text-align: right;
			display: table-cell;
			width:30%;
		}

		#company img {
			height: 40px;
		}

		#details {
			margin-bottom: 50px;
			display: table;
			width: 100%;
			margin-top: -100px;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #0087C3;
			float: left;
			display: table-cell;
			width: 50%;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}

		h2.nametitle {
			font-size: 18px;
			font-weight: normal;
			margin: 0;
		}

		#invoice {
			text-align: right;
			display: table-cell;
			width: 50%;
		}

		#invoice h1 {
			color: #0087C3;
			font-size: 1em;
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1em;
			color: #777777;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table th,
		table td {
			padding: 10px;
			text-align: center;
		}

		table th {
			white-space: nowrap;        
			font-weight: normal;
		}

		table td {
			text-align: right;
		}

		table td h3{
			font-weight: normal;
			margin: 0 0 0.2em 0;
		}

		table .no {
			color: #FFFFFF;
			background: #57B223;
		}

		table .desc {
			text-align: left;
		}

		table .mid {
			text-align: center;
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {

		}

		table tbody tr:last-child td {

		}

		table tfoot td {
			white-space: nowrap;
		}

		table tfoot tr:first-child td {

		}

		table tfoot tr td:first-child {
			border: none;
		}

		#thanks{
			font-size: 2em;
			margin-bottom: 50px;
		}

		#notices{
			padding-left: 6px;
			border-left: 6px solid #0087C3;
		}

		#notices .notice {
			font-size: 1.2em;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 60px;
			position: absolute;
			bottom: 30;
			padding: 8px 0;
			text-align: center;
		}
	</style>
</head>
<body>
	<header>
		<div id="qr">
            <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(120)->generate($do->delivery_code)) }}">
			<h2 class="name">{{$do->delivery_code}}</h2>
		</div>
		<div id="title">
			<h2 class="nametitle"><ins>PICKING FORM</ins></h2>
			<h2 class="name">{{$do->delivery_code}}</h2>
		</div>
		<div id="company">
			{{-- <img src="{{ public_path('assets/images/logost.jpg') }}"> --}}
			{{-- <img src="{{ public_path('assets/images/sinarmas.png') }}"> --}}
			<img src="{{ \Storage::disk('public')->url(appsetting('COMPANY_LOGO')) }}">
		</div>
	</header>
	<main>
		<div id="details" class="clearfix">
			<div id="client">
				<div class="to">Ship To:</div>
				{{-- <h2 class="name">PT. Sushi Tei Indonesia</h2>
				<div class="address">Jalan Wijaya 2, Grand Wijaya Center, E18 - 19, RT.6/RW.1, Pulo, Kebayoran Baru, Jakarta Selatan, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12160</div>
				<div class="email">(021) 72801149</div> --}}

				<h2 class="name">{{ $do->dlv_to_storage->code }} - {{ $do->dlv_to_storage->description }}</h2>
				<div class="address">{{ $do->dlv_to_storage->location->address }}</div>
				<div class="email"></div>
			</div>
			<div id="invoice">
				<div class="date">Request Delivery: {{ Carbon\Carbon::parse($do->planned_gi)->format('d M Y') }}</div>
                <div class="date" style="margin-top: 25px;">Printed by: {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
			</div>
		</div>
		<table border="1" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th class="">No</th>
					<th class="desc">Material</th>
					<th class="number">Quantity</th>
					<th class="desc">Uom</th>
                    <th class="desc">Plant</th>
                    <th class="desc">Storage</th>
                    <th class="desc">Bin Number</th>
                    <th class="desc">Pick Qty</th>
				</tr>
			</thead>
			<tbody>
				@php
					$total = 0
				@endphp
				@foreach($do->item as $item)
					<tr>
						<td class="">{{ $loop->iteration }}</td>
						<td class="desc">
							<h3>{{ $item->material->code }}</h3>{{ $item->material->description }}
						</td>
						<td class="number">{{ $item->pick_qty }}</td>
						<td class="desc">{{ $item->uom->code }}</td>
                        <td class="desc">{{ $item->plant->code }}</td>
                        <td class="desc">{{ $item->storage->code }}</td>
                        <td class="desc">{{ $item->storage_bin }}</td>
                        <td class="desc">{{ $item->pick_qty }}</td>
					</tr>			
				@endforeach
			</tbody>
		</table>
	</main>
	<footer>
		<table border="0" cellspacing="0" cellpadding="0" style="width: 100%">
        <thead>
				<tr>
					<th class="mid">Signature</th>
                    <th class="mid"></th>
                    <th class="mid"></th>
					<th class="mid"></th>
				</tr>
			</thead>
			<tbody>
				<tr><td colspan="4"></td></tr>
				<tr><td colspan="4"></td></tr>
				<tr>
					<td class="mid">(....................)</td>
                    <td class="mid"></td>
                    <td class="mid"></td>
					<td class="mid">(....................)</td>
				</tr>
                <tr>
					<td class="mid">Picker Staff</td>
                    <td class="mid"></td>
                    <td class="mid"></td>
					<td class="mid">Warehouse Admin</td>
				</tr>
			</tbody>
		</table>
	</footer>
</body>
</html>