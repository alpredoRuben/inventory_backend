<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Delivery Orders</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 15em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 14px;
		}

		header {
			/* padding: 10px 0; */
			/*margin-bottom: 20px;*/
			border-bottom: 1px solid #AAAAAA;
			/*position: fixed; top: -60px; left: 0px; right: 0px;*/
			position: fixed; left: 10px; top: -200px; right: 0px; height: 180px;
			width:100%;
			display: table;
		}

		#qr {
			float: left;
			margin-top: 8px;
			display: table-cell;
			width:35%;
		}

		#qr img {
			height: 70px;
		}

		#title {
			display: table-cell;
			text-align: center;
			width:30%;
		}

		#company {
			/* float: right; */
			text-align: right;
			display: table-cell;
			width:35%;
		}

		#company img {
			height: 70px;
		}

		#details {
			margin-bottom: 50px;
			margin-top: 0px;
			display: table;
			width: 100%;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #0087C3;
			float: left;
			display: table-cell;
			width: 50%;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}

		h2.nametitle {
			font-size: 18px;
			font-weight: normal;
			margin: 0;
		}

		#invoice {
			/*float: right;*/
			text-align: right;
			display: table-cell;
			width: 50%;
		}

		#invoice h1 {
			color: #0087C3;
			font-size: 1em;
			/*line-height: 1em;*/
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1em;
			color: #777777;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table th,
		table td {
			padding: 10px;
			/*background: #EEEEEE;*/
			text-align: center;
			border-bottom: 1px solid #FFFFFF;
		}

		table th {
			white-space: nowrap;        
			font-weight: normal;
			border: 1px solid;
		}

		table td {
			text-align: right;
		}

		table td h3{
			/*color: #57B223;*/
			/*font-size: 1.2em;*/
			font-weight: normal;
			margin: 0 0 0.2em 0;
		}

		table .no {
			color: #FFFFFF;
			/*font-size: 1.6em;*/
			background: #57B223;
		}

		table .desc {
			text-align: left;
		}

		table .mid {
			text-align: center;
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			/*background: #57B223;*/
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {
			/*font-size: 1.2em;*/
		}

		table tbody tr:last-child td {
			border: none;
		}

		table tfoot td {
			padding: 10px 20px;
			/*background: #FFFFFF;*/
			border-bottom: none;
			/*font-size: 1.2em;*/
			white-space: nowrap;
			border-top: 1px solid #AAAAAA;
		}

		table tfoot tr:first-child td {
			border-top: none;
		}

		table tfoot tr:last-child td {
			color: #57B223;
			/*font-size: 1.4em;*/
			border-top: 1px solid #57B223;
		}

		table tfoot tr td:first-child {
			border: none;
		}

		#thanks{
			font-size: 2em;
			margin-bottom: 50px;
		}

		#notices{
			padding-left: 6px;
			border-left: 6px solid #0087C3;
		}

		#notices .notice {
			font-size: 1.2em;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 60px;
			position: absolute;
			bottom: 30;
			/*border-top: 1px solid #AAAAAA;*/
			padding: 8px 0;
			text-align: center;
		}
	</style>
</head>
<body>
	<header>
		<div id="qr">
			<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(120)->generate($do->delivery_code)) }}">
		</div>
		<div id="title">
			<h2 class="nametitle"><ins>DELIVERY NOTE</ins></h2>
			<h2 class="name">{{$do->delivery_code}}</h2>
		</div>
		<div id="company">
			<img src="{{ public_path('assets/images/logost.jpg') }}">
			<h2 class="name">PT. SUSHI-TEI INDONESIA</h2>
			<div>Grand Wijaya Center Block E No 18-9</div>
			<div>Jl. Wijaya II-Kebayoran Baru</div>
			<div>Jakarta Selatan 12160 INDONESIA</div>
			<div>Telp. (62-21)-7281 1149</div>
			<div>Fax. (62-21)-7280 1151</div>
		</div>
	</header>
	<main>
		<div id="details" class="clearfix">
			<div id="client">
				<div class="to">Delivery From:</div>
				@php
					$sender = $do->item->first();
				@endphp
				<h2 class="name">{{$sender->plant->code}} / {{$sender->storage->code}}</h2>
				<div class="address">{{$sender->storage->location->address}}</div>
				{{--<div class="email"><a href="mailto:john@mindaperdana.com">john@mindaperdana.com</a></div>--}}

				<div class="to" style="margin-top: 25px;">Delivery To:</div>
				<h2 class="name">{{$do->dlv_to_plant->code}} / {{$do->dlv_to_storage->code}}</h2>
				<div class="address">{{$do->dlv_to_storage->location->address}}</div>
				{{--<div class="email"><a href="mailto:john@mindaperdana.com">john@mindaperdana.com</a></div>--}}
			</div>
			<div id="invoice">
				@php
					if($do->delivery_types === 0) {
						$type = 'Stock Transfer Order';
					} elseif ($do->delivery_types === 2) {
						$type = 'Customer Delivery';
					} else {
						$type = 'Vendor Return';
					}
				@endphp
				<div class="date">Delivery Type: {{ $type }}</div>
				<div class="date">Issued Date: {{ Carbon\Carbon::parse($do->sent_date)->format('d M Y') }}</div>
				<div class="date">Your Reference: {{ $sender->reservation->reservation_code}}</div>
				<br>
				<div class="date">Transport Type: {{ $do->transport_type->description }}</div>
				<div class="date">Transport ID: {{ $do->transport_id }}</div>
				<div class="date">Printed By: {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
				<div class="date">Printed Date: {{ Carbon\Carbon::now()->format('d M Y') }}</div>
			</div>
		</div>
		<table cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th class="">No</th>
					<th class="desc">
						Material code <br>
						Description
					</th>
					<th class="number">Quantity</th>
					<th class="desc">Batch</th>
					<th class="desc">Expiry Date</th>
				</tr>
			</thead>
			<tbody>
				@foreach($do->item as $item)
					<tr>
						<td class="">{{ $loop->iteration }}</td>
						<td class="desc">
							<h3>{{ $item->material->code }}</h3>{{ $item->material->description }}
						</td>
						<td class="number">{{ $item->pick_qty }} / {{ $item->uom->code }}</td>
						<td class="desc">{{ $item->batch }}</td>						
						<td class="desc">
							@if($item->stock_batch)
								{{ Carbon\Carbon::parse($item->stock_batch->sled_date)->format('d M Y') }}
							@endif
						</td>
					</tr>
					@if($item->serials)
						@if(count($item->serials) > 0)
							<tr>
								<td colspan="2"></td>
								<td class="desc">
								Serial: <br/>
									@php
										$i = 1;
									@endphp
									@foreach($item->serials as $serial)
										{{ $i++ }}. {{$serial->serial}} ,
									@endforeach
								</td>
							</tr>
						@endif
					@endif
				@endforeach
			</tbody>
			{{--<tfoot>
				<tr>
					<td colspan="1"></td>
					<td colspan="2">SUBTOTAL</td>
					<td>$5,200.00</td>
				</tr>
				<tr>
					<td colspan="1"></td>
					<td colspan="2">TAX 25%</td>
					<td>$1,300.00</td>
				</tr>
				<tr>
					<td colspan="1"></td>
					<td colspan="2">GRAND TOTAL</td>
					<td>$6,500.00</td>
				</tr>
			</tfoot>--}}
		</table>
	</main>
	<footer>
		<table border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th class="mid">Issued By</th>
					<th class="mid">Courier Name</th>
					<th class="mid">Security Check</th>
				</tr>
			</thead>
			<tbody>
				<tr><td colspan="3"></td></tr>
				<tr><td colspan="3"></td></tr>
				<tr>
					<td class="mid">( {{ $do->sentBy->firstname }} {{ $do->sentBy->lastname }} )</td>
					<td class="mid">( {{ $do->courier_name }} )</td>
					<td class="mid">(....................)</td>
				</tr>
			</tbody>
		</table>
	</footer>
</body>
</html>