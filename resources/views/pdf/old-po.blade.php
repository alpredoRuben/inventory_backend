<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Delivery Orders</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 15em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 14px;
		}

		header {
			/* padding: 10px 0; */
			/*margin-bottom: 20px;*/
			border-bottom: 1px solid #AAAAAA;
			margin-right: -20px;
			/*position: fixed; top: -60px; left: 0px; right: 0px;*/
			position: fixed; left: 0px; top: -210px; right: 0px; height: 80px;
			width:100%;
			display: table;
		}

		#qr {
			float: left;
			margin-top: 8px;
			display: table-cell;
			width:30%;
		}

		#qr img {
			height: 70px;
		}

		#title {
			display: table-cell;
			text-align: center;
			width:40%;
		}

		#company {
			/*float: right;*/
			text-align: right;
			display: table-cell;
			width:30%;
		}

		#company img {
			height: 40px;
		}

		#details {
			margin-bottom: 50px;
			display: table;
			width: 100%;
			margin-top: -100px;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #0087C3;
			float: left;
			display: table-cell;
			width: 50%;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}


		h2.nametitle {
			font-size: 18px;
			font-weight: normal;
			margin: 0;
		}

		#invoice {
			/*float: right;*/
			text-align: right;
			display: table-cell;
			width: 50%;
		}

		#invoice h1 {
			color: #0087C3;
			font-size: 1em;
			/*line-height: 1em;*/
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1em;
			color: #777777;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table th,
		table td {
			padding: 10px;
			/*background: #EEEEEE;*/
			text-align: center;
			/*border-bottom: 1px solid #FFFFFF;*/
		}

		table th {
			white-space: nowrap;        
			font-weight: normal;
		}

		table td {
			text-align: right;
		}

		table td h3{
			/*color: #57B223;*/
			/*font-size: 1.2em;*/
			font-weight: normal;
			margin: 0 0 0.2em 0;
		}

		table .no {
			color: #FFFFFF;
			/*font-size: 1.6em;*/
			background: #57B223;
		}

		table .desc {
			text-align: left;
		}

		table .mid {
			text-align: center;
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			/*background: #57B223;*/
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {
			/*font-size: 1.2em;*/
		}

		table tbody tr:last-child td {
			/*border: none;*/
		}

		table tfoot td {
			/*padding: 10px 20px;*/
			/*background: #FFFFFF;*/
			/*border-bottom: none;*/
			/*font-size: 1.2em;*/
			white-space: nowrap;
			/*border-top: 1px solid #AAAAAA;*/
		}

		table tfoot tr:first-child td {
			/*border-top: none;*/
		}

		/*table tfoot tr:last-child td {
			color: #57B223;
			font-size: 1.4em;
			border-top: 1px solid #57B223;
		}*/

		table tfoot tr td:first-child {
			border: none;
		}

		#thanks{
			font-size: 2em;
			margin-bottom: 50px;
		}

		#notices{
			padding-left: 6px;
			border-left: 6px solid #0087C3;
		}

		#notices .notice {
			font-size: 1.2em;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 60px;
			position: absolute;
			bottom: 30;
			/*border-top: 1px solid #AAAAAA;*/
			padding: 8px 0;
			text-align: center;
		}
	</style>
</head>
<body>
	<header>
		<div id="qr">
			<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(120)->generate($po->po_doc_no)) }}">
			<h2 class="name">{{$po->po_doc_no}}</h2>
		</div>
		<div id="title">
			<h2 class="nametitle"><ins>Purchase Order</ins></h2>
			<h2 class="name">{{$po->po_doc_no}}</h2>
		</div>
		<div id="company">
			{{-- <img src="{{ public_path('assets/images/logost.jpg') }}"> --}}
			{{-- <img src="{{ public_path('assets/images/sinarmas.png') }}"> --}}
			<img src="{{ \Storage::disk('public')->url(appsetting('COMPANY_LOGO')) }}">
			<!-- <h2 class="name">PT. Minda Perdana Indonesia</h2>
			<div>M-Gold Tower Lt 12D, Jl KH Noer Ali, Bekasi, Jawa Barat – Indonesia</div>
			<div>021-28087100</div>
			<div><a href="mailto:sales@mindaperdana.com">sales@mindaperdana.com</a></div> -->
		</div>
	</header>
	<main>
		<div id="details" class="clearfix">
			<div id="client">
				<div class="to">Vendor:</div>
				<h2 class="name">{{$po->vendor->description}}</h2>
				<div class="address">{{$po->vendor->location->address}}</div>

				<div class="to" style="margin-top: 10px;">Ship To:</div>
				{{-- <h2 class="name">PT. Sushi Tei Indonesia</h2>
				<div class="address">Jalan Wijaya 2, Grand Wijaya Center, E18 - 19, RT.6/RW.1, Pulo, Kebayoran Baru, Jakarta Selatan, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12160</div>
				<div class="email">(021) 72801149</div> --}}

				<h2 class="name">{{ appsetting('COMPANY_NAME') }}</h2>
				<div class="address">{{ appsetting('COMPANY_ADDRESS') }}</div>
				<div class="email">{{ appsetting('COMPANY_PHONE') }}</div>
			</div>
			<div id="invoice">
				<div class="date">Date: 22 Mar 2019</div>
				<div class="date">Quotation No: {{ $po->quotation_number }}</div>
				<div class="date">Payment Terms: {{ $po->term_payment->description }}</div>
				<div class="date">Incoterms: {{ $po->incoterm->description }}</div>
				<div class="date">Request for delivery: {{ Carbon\Carbon::now()->format('d M Y') }}</div>
			</div>
		</div>
		<table border="1" cellspacing="0" cellpadding="0" style="width: 50%">
			<thead>
				<tr>
					<th class="desc">Remarks</th>
				</tr>
				<tr style="min-height: 50px">
					<th class="desc">{!! $po->notes !!}</th>
				</tr>
			</thead>
		</table>
		<table border="1" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th class="">No</th>
					<th class="desc">Material</th>
					<th class="number">Quantity</th>
					<th class="desc">UoM</th>
					<th class="desc">Price</th>
					<th></th>
					<th class="desc">Total Price</th>
				</tr>
			</thead>
			<tbody>
				@php
					$total = 0
				@endphp
				@foreach($po->item as $item)
					<tr>
						<td class="">{{ $loop->iteration }}</td>
						<td class="desc"><h3>{{ $item->material->code }}</h3></td>
						<td class="number">{{ $item->order_qty }}</td>
						<td class="desc">{{ $item->order_unit->code }}</td>
						<td class="number">{{ number_format($item->price_unit,  2, ',', '.') }} {{ $item->currency }}</td>
						<td></td>
						@php
							$price_total = $item->order_qty * $item->price_unit				
						@endphp				
						<td class="number">{{ number_format($price_total,  2, ',', '.') }} {{ $item->currency }}</td>
					</tr>
					<tr>
						<td></td>
						<td class="desc">{{ $item->material->description }}</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					@if($item->long_text)
					<tr>
						<td></td>
						<td class="desc">{{ $item->long_text }}</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					@endif

					@php
						$total += $item->order_qty * $item->price_unit
					@endphp
				@endforeach
				<tr>
					<td colspan="4"></td>
					<td class="desc">Subtotal</td>
					<td></td>
					<td class="number">{{ number_format($total,  2, ',', '.') }} {{ $item->currency }}</td>
				</tr>
				<tr>
					<td colspan="4"></td>
					<td class="desc">Tax</td>
					<td></td>
					<td class="number">0</td>
				</tr>
				<tr style="border-top: 1px solid black">
					<td colspan="4"></td>
					<td class="desc">Total</td>
					<td></td>
					<td class="number">{{ number_format($total,  2, ',', '.') }} {{ $item->currency }}</td>
				</tr>
			</tbody>
		</table>
	</main>
	<footer>
		<table border="0" cellspacing="0" cellpadding="0" style="width: 50%">
			<thead>
				<tr>
					<th class="desc">Signature</th>
				</tr>
				<tr><th></th></tr>
				<tr><th></th></tr>
				<tr>
					<th class="desc">(....................)</th>
				</tr>
			</thead>
		</table>
	</footer>
</body>
</html>