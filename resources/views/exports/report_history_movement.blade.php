<table>
    <thead>
        <tr>
            <th>Plant</th>
            <th>Plant description</th>
            <th>Storage</th>
            <th>Storage description</th>
            <th>Material Code</th>
            <th>Description</th>
            <th>Batch no</th>
            <th>Serial</th>
            <th>Movement type</th>
            <th>Cost center</th>
            <th>Material group</th>
            <th>GL account</th>
            <th>Quantity</th>
            <th>Base Uom</th>
            <th>Stock type</th>
            <th>Currency</th>
            <th>Unit Price</th>
            <th>Total Amount</th>
            <th>Created Date</th>
            <th>Created By</th>
        </tr>
    </thead>
    <tbody>
        @php
            $sum = 0;
        @endphp
        @foreach($history as $item)
            @if(count($item->serial) > 0)
                @foreach($item->serial as $serial)
                    <tr>
                        <td>{{ $item->plant->code }}</td>
                        <td>{{ $item->plant->description }}</td>
                        <td>{{ $item->storage->code }}</td>
                        <td>{{ $item->storage->description }}</td>
                        @if($item->material_id)
                            <td>{{ $item->material->code }}</td>
                            <td>{{ $item->material->description }}</td>
                        @else
                            <td></td>
                            <td>{{ $item->material_description }}</td>
                        @endif
                        <td>{{ $item->batch_no }}</td>
                        <td> {{ $serial->serial }} </td>
                        <td>{{ $item->movement_type->code }} - {{ $item->movement_type->description }}</td>
                        @if($item->cost_center)
                            <td>{{ $item->cost_center->code }} - {{ $item->cost_center->description }}</td>
                        @else
                            <td></td>
                        @endif
                        @if($item->material_id)
                            <td>{{ $item->material->group_material->material_group }} - {{ $item->material->group_material->description }}</td>
                            <td>{{ $item->material->group_material->account->code }} - {{ $item->material->group_material->account->description }}</td>
                        @else
                            <td>{{ $item->mat_group['material_group'] }} - {{ $item->mat_group['description'] }}</td>
                            <td>{{ $item->gl_account['code'] }} - {{ $item->gl_account['description'] }}</td>
                        @endif
                        @php
                            if($item->de_ce === 'c') {
                                $x = -1;
                            } else {
                                $x = 1;
                            }
                            $total_ammount = $item->amount * 1 * $x;
                            $sum += $total_ammount;
                        @endphp
                        <td>{{ 1 * $x }}</td>
                        <td>{{ $item->base_uom->code }}</td>
                        <td>{{ $item->stock_type }} </td>
                        <td>{{ $item->currency }}</td>
                        <td>{{ number_format(($item->amount * $x),  2, ',', '.') }}</td>
                        <td>{{ number_format($total_ammount,  2, ',', '.') }}</td>
                        <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-M-Y H:i:s') }}</td>
                        <td>{{ $item->createdBy->name }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td>{{ $item->plant->code }}</td>
                    <td>{{ $item->plant->description }}</td>
                    <td>{{ $item->storage->code }}</td>
                    <td>{{ $item->storage->description }}</td>
                    @if($item->material_id)
                        <td>{{ $item->material->code }}</td>
                        <td>{{ $item->material->description }}</td>
                    @else
                        <td></td>
                        <td>{{ $item->material_description }}</td>
                    @endif
                    <td>{{ $item->batch_no }}</td>
                    @if($item->serial)
                        <td>
                        @foreach($item->serial as $serial)
                            {{ $serial->serial }},
                        @endforeach
                        </td>
                    @else
                        <td></td>
                    @endif
                    <td>{{ $item->movement_type->code }} - {{ $item->movement_type->description }}</td>
                    @if($item->cost_center)
                        <td>{{ $item->cost_center->code }} - {{ $item->cost_center->description }}</td>
                    @else
                        <td></td>
                    @endif
                    @if($item->material_id)
                        <td>{{ $item->material->group_material->material_group }} - {{ $item->material->group_material->description }}</td>
                        <td>{{ $item->material->group_material->account->code }} - {{ $item->material->group_material->account->description }}</td>
                    @else
                        <td>{{ $item->mat_group['material_group'] }} - {{ $item->mat_group['description'] }}</td>
                        <td>{{ $item->gl_account['code'] }} - {{ $item->gl_account['description'] }}</td>
                    @endif
                    @php
                        if($item->de_ce === 'c') {
                            $x = -1;
                        } else {
                            $x = 1;
                        }
                        $total_ammount = $item->amount * $item->quantity * $x;
                        $sum += $total_ammount;
                    @endphp
                    <td>{{ $item->quantity * $x }}</td>
                    <td>{{ $item->base_uom->code }}</td>
                    <td>{{ $item->stock_type }} </td>
                    <td>{{ $item->currency }}</td>
                    <td>{{ number_format(($item->amount * $x),  2, ',', '.') }}</td>
                    <td>{{ number_format($total_ammount,  2, ',', '.') }}</td>
                    <td>{{ Carbon\Carbon::parse($item->created_at)->format('d-M-Y H:i:s') }}</td>
                    <td>{{ $item->createdBy->name }}</td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td colspan='17'>Grand Total</td>
            <td>{{ number_format($sum,  2, ',', '.') }}</td>
        </tr>
    </tbody>
</table>