<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Plant</th>
            <th>Storage</th>
            <th>Sequence</th>
            <th>External Purchase</th>
            <th>Plant Supply</th>
            <th>Storage Supply</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->plant }}</td>
                <td>{{ $data->storage }}</td>
                <td>{{ $data->sequence }}</td>
                <td>{{ $data->external_purchase }}</td>
                <td>{{ $data->plant_supply }}</td>
                <td>{{ $data->storage_supply }}</td>
                <td>{{ $data->message }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>