<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Material Code</th>
            <th>Plant</th>
            <th>Procurement Type</th>
            <th>Procurement Group</th>
            <th>UoP</th>
            <th>Production Storage</th>
            <th>Batch Indicator</th>
            <th>Available Check</th>
            <th>Profit Center</th>
            <th>Source List</th>
            <th>MRP Group</th>
            <th>MRP Controller</th>
            <th>Planned Delivery Time</th>
            <th>Product Champion</th>
            <th>Hazardous Indicator</th>
            <th>Stock Determination</th>
            <th>Quality Profile</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->material_code }}</td>
                <td>{{ $data->plant }}</td>
                <td>{{ $data->procurement_type }}</td>
                <td>{{ $data->procurement_group }}</td>
                <td>{{ $data->uop }}</td>
                <td>{{ $data->production_storage }}</td>
                <td>{{ $data->batch_indicator }}</td>
                <td>{{ $data->available_check }}</td>
                <td>{{ $data->profit_center }}</td>
                <td>{{ $data->source_list }}</td>
                <td>{{ $data->mrp_group }}</td>
                <td>{{ $data->mrp_controller }}</td>
                <td>{{ $data->planned_delivery_time }}</td>
                <td>{{ $data->product_champion }}</td>
                <td>{{ $data->hazardous_indicator }}</td>
                <td>{{ $data->stock_determination }}</td>
                <td>{{ $data->quality_profile }}</td>
                <td>{{ $data->message }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>