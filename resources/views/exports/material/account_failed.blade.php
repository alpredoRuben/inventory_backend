<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Material Code</th>
            <th>Plant</th>
            <th>Valuation Type</th>
            <th>Valuation Class</th>
            <th>Price Controll</th>
            <th>Moving Price</th>
            <th>Standart Price</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->material_code }}</td>
                <td>{{ $data->plant }}</td>
                <td>{{ $data->valuation_type }}</td>
                <td>{{ $data->valuation_class }}</td>
                <td>{{ $data->price_controll }}</td>
                <td>{{ $data->moving_price }}</td>
                <td>{{ $data->standart_price }}</td>
                <td>{{ $data->message }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>