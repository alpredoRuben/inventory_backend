<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Material Type</th>
            <th>Code</th>
            <th>Description</th>
            <th>Group Material</th>
            <th>Classification</th>
            <th>Old Material</th>
            <th>Base Material</th>
            <th>SKU Code</th>
            <th>UoM</th>
            <th>Nett Weight Value</th>
            <th>Nett Weight UoM</th>
            <th>Gross Weight Value</th>
            <th>Volume Value</th>
            <th>Volume UoM</th>
            <th>Serial Profile</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td> {{ $data->material_type }} </td>
                <td> {{ $data->code }} </td>
                <td> {{ $data->description }} </td>
                <td> {{ $data->group_material }} </td>
                <td> {{ $data->classification }} </td>
                <td> {{ $data->old_material }} </td>
                <td> {{ $data->base_material }} </td>
                <td> {{ $data->sku_code }} </td>
                <td> {{ $data->uom }} </td>
                <td> {{ $data->nett_weight_value }} </td>
                <td> {{ $data->nett_weight_uom }} </td>
                <td> {{ $data->gross_weight_value }} </td>
                <td> {{ $data->volume_value }} </td>
                <td> {{ $data->volume_uom }} </td>
                <td> {{ $data->serial_profile }} </td>
                <td> {{ $data->message }} </td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>