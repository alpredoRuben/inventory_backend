<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Material Code</th>
            <th>Plant</th>
            <th>Storage</th>
            <th>Minimum Stock</th>
            <th>Maximum Stock</th>
            <th>Storage Bin</th>
            <th>Storage Condition</th>
            <th>Storage Type</th>
            <th>Temp Condition</th>
            <th>LE Quantity</th>
            <th>LE Unit</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td> {{ $data->material_code }} </td>
                <td> {{ $data->plant }} </td>
                <td> {{ $data->storage }} </td>
                <td> {{ $data->minimum_stock }} </td>
                <td> {{ $data->maximum_stock }} </td>
                <td> {{ $data->storage_bin }} </td>
                <td> {{ $data->storage_condition }} </td>
                <td> {{ $data->storage_type }} </td>
                <td> {{ $data->temp_condition }} </td>
                <td> {{ $data->le_quantity }} </td>
                <td> {{ $data->le_unit }} </td>
                <td> {{ $data->message }} </td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>