<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Material Code</th>
            <th>Sales Organization</th>
            <th>Delivering Plant</th>
            <th>Tax Class</th>
            <th>Account Assignment</th>
            <th>Item Category</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->material_code }}</td>
                <td>{{ $data->sales_organization }}</td>
                <td>{{ $data->delivering_plant }}</td>
                <td>{{ $data->tax_class }}</td>
                <td>{{ $data->account_assignment }}</td>
                <td>{{ $data->item_category }}</td>
                <td>{{ $data->message }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>