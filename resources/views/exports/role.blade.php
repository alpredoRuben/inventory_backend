<table>
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th>Roles</th>
        @foreach($roles as $role)
            <th>{{ $role->name }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th>Organization Parameter</th>
        <th>Plant</th>
        @foreach($roles as $key => $r)
            @php
                $plant_org = App\Models\OrganizationParameter::where('key', 'plant')
                    ->where('role_id', $r->id)
                    ->first();

                if ($plant_org) {
                    $plant_id = json_decode($plant_org->value);
                } else {
                    $plant_id = [];
                }

                $plant = App\Models\Plant::whereIn('id', $plant_id)->pluck('code');
            @endphp
            <th>{{ $plant->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Storage</th>
        @foreach($roles as $key => $r)
            @php
                $storage_org = App\Models\OrganizationParameter::where('key', 'storage')
                    ->where('role_id', $r->id)
                    ->first();

                if ($storage_org) {
                    $storage_id = json_decode($storage_org->value);
                } else {
                    $storage_id = [];
                }

                $storage = App\Models\Storage::whereIn('id', $storage_id)->pluck('code');
            @endphp
            <th>{{ $storage->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Procurement Group</th>
        @foreach($roles as $key => $r)
            @php
                $proc_org = App\Models\OrganizationParameter::where('key', 'procurement_group')
                    ->where('role_id', $r->id)
                    ->first();

                if ($proc_org) {
                    $proc_id = json_decode($proc_org->value);
                } else {
                    $proc_id = [];
                }

                $proc = App\Models\ProcurementGroup::whereIn('id', $proc_id)->pluck('code');
            @endphp
            <th>{{ $proc->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Cost Center</th>
        @foreach($roles as $key => $r)
            @php
                $cost_org = App\Models\OrganizationParameter::where('key', 'cost_center')
                    ->where('role_id', $r->id)
                    ->first();

                if ($cost_org) {
                    $cost_id = json_decode($cost_org->value);
                } else {
                    $cost_id = [];
                }

                $cost = App\Models\CostCenter::whereIn('id', $cost_id)->pluck('code');
            @endphp
            <th>{{ $cost->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Movement Type</th>
        @foreach($roles as $key => $r)
            @php
                $movement_type_org = App\Models\OrganizationParameter::where('key', 'movement_type')
                    ->where('role_id', $r->id)
                    ->first();

                if ($movement_type_org) {
                    $movement_type_id = json_decode($movement_type_org->value);
                } else {
                    $movement_type_id = [];
                }

                $movement_type = App\Models\MovementType::whereIn('id', $movement_type_id)->pluck('code');
            @endphp
            <th>{{ $movement_type->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    <tr>
        <th></th>
        <th></th>
        <th>Valuation Group</th>
        @foreach($roles as $key => $r)
            @php
                $valuation_group_org = App\Models\OrganizationParameter::where('key', 'valuation_group')
                    ->where('role_id', $r->id)
                    ->first();

                if ($valuation_group_org) {
                    $valuation_group_id = json_decode($valuation_group_org->value);
                } else {
                    $valuation_group_id = [];
                }

                $valuation_group = App\Models\ValuationGroup::whereIn('id', $valuation_group_id)->pluck('code');
            @endphp
            <th>{{ $valuation_group->implode(', ', ', ') }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        <tr></tr>
        <tr>
            <td>No</td>
            <td>Auth Object</td>
            <td>Description</td>
        </tr>
        @php
            $i = 1;
        @endphp
        @foreach($modules as $module)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $module->object }}</td>
                <td>{{ $module->description }}</td>
                @php
                    $module_roles = $module->roles->sortBy('id')->values()->pluck('id')->toArray();
                @endphp
                @foreach($roles as $key => $r)
                    @isset($module_roles)
                        @if(in_array($r->id, $module_roles))
                            <td>x</td>
                        @else
                            <td></td>
                        @endif
                    @endisset
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
