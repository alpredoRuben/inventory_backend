<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Plant</th>
            <th>Code</th>
            <th>Description</th>
            <th>Location</th>
            <th>Responsible User</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td> {{ $data->plant }} </td>
                <td> {{ $data->code }} </td>
                <td> {{ $data->description }} </td>
                <td> {{ $data->location }} </td>
                <td> {{ $data->responsible_user }} </td>
                <td> {{ $data->message }} </td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>