<table>
    <tbody>
        @foreach($chart as $coa)
            <tr>
                <td>{{$coa->code}} - {{$coa->description}}</td>
            </tr>
            <tr></tr>

            @foreach($coa->account_groups as $ag)
                <tr>
                    <td>{{$ag->code}}</td>
                    <td>{{$ag->description}}</td>
                
                    @if(!empty($ag->group_type))
                        <td>{{$ag->group_type->code}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(!empty($ag->account_type))
                        <td>{{$ag->account_type->description}}</td>
                    @else
                        <td></td>
                    @endif
                </tr>

                    @foreach($ag->accounts as $gl)
                        <tr>
                            <td>{{$ag->code}}.{{$gl->code}}</td>
                            <td>{{$gl->description}}</td>
                            @if($gl->is_cash)
                                <td>Cash</td>
                            @else
                                <td></td>
                            @endif
                        </tr>
                    @endforeach
            @endforeach

            <tr></tr>
        @endforeach
    </tbody>
</table>