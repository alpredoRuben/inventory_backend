<table>
    <thead>
        <tr>
            <th>No</th>
            <th>CoA Code</th>
            <th>CoA Description</th>
            <th>Account Group Code</th>
            <th>Account Group Description</th>
            <th>Group Type</th>
            <th>Account Type</th>
            <th>Account Code</th>
            <th>Account Description</th>
            <th>Cash</th>
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->coa_code }}</td>
                <td>{{ $data->coa_description }}</td>
                <td>{{ $data->account_group_code }}</td>
                <td>{{ $data->account_group_description }}</td>
                <td>{{ $data->group_type }}</td>
                <td>{{ $data->account_type }}</td>
                <td>{{ $data->gl_account }}</td>
                <td>{{ $data->gl_description }}</td>
                <td>{{ $data->cash }}</td>
                <td>{{ $data->message }}</td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>