<table>
    <thead>
        <tr>
            <th>No</th>
            <th>CoA Code</th>
            <th>CoA Description</th>
            <th>Account Group Code</th>
            <th>Account Group Description</th>
            <th>Group Type</th>
            <th>Account Type</th>
            <th>Account Code</th>
            <th>Account Description</th>
            <th>Cash</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($chart as $data)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $data->account_group->chart_of_account->code }}</td>
                <td>{{ $data->account_group->chart_of_account->description }}</td>
                <td>{!! str_replace('.',',',($data->account_group->code)) !!}</td>
                <td>{{ $data->account_group->description }}</td>
                @if($data->account_group->group_type)
                    <td>{{ $data->account_group->group_type->code }}</td>
                @else
                    <td></td>
                @endif
                @if($data->account_group->account_type)
                    <td>{{ $data->account_group->account_type->description }}</td>
                @else
                    <td></td>
                @endif
                <td>{!! str_replace('.',',',$data->code) !!}</td>
                <td>{{ $data->description }}</td>
                @if($data->is_cash)
                    <td>Yes</td>
                @else
                    <td>No</td>
                @endif
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>