<table>
    <thead>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>Roles</th>
        @foreach($roles as $role)
            <th>{{ $role->name}}</th>
        @endforeach
    </tr>
    <tr>
        <th>No</th>
        <th>Username</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1;
    @endphp
    @foreach($users as $user)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->firstname }}</td>
            <td>{{ $user->lastname }}</td>
            <td>{{ $user->email }}</td>
            @php
                $user_roles = $user->roles->sortBy('id')->values()->pluck('id')->toArray();
            @endphp
            @foreach($roles as $key => $r)
                @isset($user_roles)
                    @if(in_array($r->id, $user_roles))
                        <td>x</td>
                    @else
                        <td></td>
                    @endif
                @endisset
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
