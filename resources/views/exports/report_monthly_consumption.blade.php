<table>
    <thead>
        <tr></tr>
        <tr>
            <th></th>
            <th>Filter</th>
        </tr>
        <tr>
            <th></th>
            <th>Plant</th>
            <th>Storage</th>
            <th>Period</th>
            <th>Movement Type</th>
        </tr>
        <tr>
            <th></th>
            @if(count($filter['plant']) > 0)
                <th>{{ $filter['plant']->implode(', ', ', ') }}</th>
            @else
                <th></th>
            @endif
            @if(count($filter['storage']) > 0)
                <th>{{ $filter['storage']->implode(', ', ', ') }}</th>
            @else
                <th></th>
            @endif
            <th>{{ $filter['period'] }}</th>
            @if(count($filter['movement_type']) > 0)
                <th>{{ $filter['movement_type']->implode(', ', ', ') }}</th>
            @else
                <th></th>
            @endif
        </tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <th></th>
            <th>Plant</th>
            <th>Plant description</th>
            <th>Storage</th>
            <th>Storage description</th>
            <th>Period</th>
            <th>Currency</th>
            <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
        @php
            $sum = 0;
        @endphp
        @foreach($map_data as $data)
            <tr>
                <td></td>
                <td>{{ $data['plant']['code'] }}</td>
                <td>{{ $data['plant']['description'] }}</td>
                <td>{{ $data['storage']['code'] }}</td>
                <td>{{ $data['storage']['description'] }}</td>
                <td>{{ $data['period'] }}</td>
                <td>{{ $data['currency'] }}</td>
                <td>{{ number_format($data['amount'],  2, ',', '.') }}</td>
            </tr>
            @php
                $sum += $data['amount'];
            @endphp
        @endforeach
        <tr>
            <td></td>
            <td colspan='6'>Grand Total</td>
            <td>{{ number_format($sum,  2, ',', '.') }}</td>
        </tr>
    </tbody>
</table>