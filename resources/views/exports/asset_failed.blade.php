<table>
    <thead>
        <tr>
            <!--
            
            'No', 'Material', 'Description', 'Asset Number', 'Serial Number', 'Superior Asset', 'Old Asset', 
            'Straight Line', 'Double Declining', 'Material Group', 'Vendor', 'Plant', 'Location', 'Status', 
            'Cost Center', 'Group Owner', 'Asset Type', 'Valuation Type', 'Responsible Person', 
            'Depreciation Date', 'Purchase Price', 'Currency', 'Quantity Asset', 'Salvage Value', 'Warranty End', 
            'Maintenance Cycle', 'Maintenance Cycle Unit', 'Last Maintenance', 'Retired Date', 'Retired Reason', 
            'Retired Remarks', 'C_merek', 'C_spec'
            -->
            <th>No</th>
            <th>Material Code</th>
            <th>Description</th>
            <th>Asset Number</th>
            <th>Serial Number</th>
            <th>Superior Asset</th>
            <th>Old Asset</th>
            <th>Straight Line</th>
            <th>Double Declining</th>
            <th>Material Group</th>
            <th>Vendor Code</th>
            <th>Plant Code</th>
            <th>Location</th>
            <!-- <th>Status</th> -->
            <th>Cost Center</th>
            <th>Group Owner</th>
            <th>Asset Type</th>
            <th>Valuation Type</th>
            <th>Responsible Person</th>
            <th>Depreciation Date</th>
            <th>Purchase Price</th>
            <th>Currency</th>
            <th>Quantity Asset</th>
            <th>Salvage Value</th>
            <th>Warranty End</th>
            <th>Maintenance Cycle</th>
            <th>Maintenance Cycle Unit</th>
            <th>Last Maintenance</th>
            <!-- <th>Retired Date</th> -->
            <!-- <th>Retired Reason</th> -->
            <!-- <th>Retired Remarks</th> -->
            <th>Error</th>
        </tr>
    </thead>
    <tbody>
        @php
            $no = 1;
        @endphp
        @foreach($data as $data)
            <tr>
                <td> {{ $no }}</td>
                <td> {{ $data->material }} </td>
                <td> {{ $data->description }} </td>
                <td> {{ $data->asset_number }} </td>
                <td> {{ $data->serial_number }} </td>
                <td> {{ $data->superior_asset }} </td>
                <td> {{ $data->old_asset }} </td>
                <td> {{ $data->straight_line }} </td>
                <td> {{ $data->double_decline }} </td>
                <td> {{ $data->material_group }} </td>
                <td> {{ $data->vendor }} </td>
                <td> {{ $data->plant }} </td>
                <td> {{ $data->location }} </td>
                <!-- <td> {{ $data->status }} </td> -->
                <td> {{ $data->cost_center }} </td>
                <td> {{ $data->group_owner }} </td>
                <td> {{ $data->asset_type }} </td>
                <td> {{ $data->valuation_type }} </td>
                <td> {{ $data->responsible_person }} </td>
                <td> {{ $data->depreciation_date }} </td>
                <td> {{ $data->purchase_price }} </td>
                <td> {{ $data->curency }} </td>
                <td> {{ $data->quantity_asset }} </td>
                <td> {{ $data->salvage_value }} </td>
                <td> {{ $data->warranty_end }} </td>
                <td> {{ $data->maintenance_cycle }} </td>
                <td> {{ $data->maintenance_cycle_unit }} </td>
                <td> {{ $data->last_maintenance }} </td>
                <!-- <td> {{ $data->retired_date }} </td> -->
                <!-- <td> {{ $data->retired_reason }} </td> -->
                <!-- <td> {{ $data->retired_remarks }} </td> -->
                <td> {{ $data->message }} </td>
            </tr>
            @php
                $no++;
            @endphp
        @endforeach
    </tbody>
</table>